<?php

class Anime_model extends CI_Model 
{
    public $table_name = "fame_albums";
    function __construct() {
        parent::__construct(); 
    }
    
    function allposts_count()
    {   
        $query = $this
                ->db
                ->get($this->table_name);
    
        return $query->num_rows();  

    }
    
    function allposts($limit,$start,$col,$dir)
    {   
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get($this->table_name);
        
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
   
    function posts_search($limit,$start,$search,$col,$dir)
    {
        $query = $this
                ->db
                ->like('album_name',$search)
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get($this->table_name);
        
       
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count($search)
    {
        $query = $this
                ->db
                ->like('album_name',$search)
                ->get($this->table_name);
    
        return $query->num_rows();
    } 
   
}