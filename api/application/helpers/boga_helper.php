<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

if (!function_exists('asset_url')) {

    function asset_url($file) {
        return site_url('assets/frontend/' . $file);
    }

}

function order_code() {
    $CI = get_instance();
    $CI->load->database();
    $query = $CI->db->select('MAX(SUBSTR(code, 14)) code')
            ->where('YEAR(date_added)', date('Y'))
            ->where('MONTH(date_added)', date('m'))
            ->get('orders');
    if ($query->num_rows() > 0) {
        $increment = $query->row()->code;
    } else {
        $increment = 0;
    }
    $increment = ($increment == 0) ? 1 : $increment + 1;
    if ($increment < 10) {
        $increment = '000' . $increment;
    } elseif ($increment < 100) {
        $increment = '00' . $increment;
    } elseif ($increment < 1000) {
        $increment = '0' . $increment;
    }
    $code = 'ORDER/' . date('Ym') . '/' . $increment;
    return $code;
}

function invoice_code() {
    $CI = get_instance();
    $CI->load->database();
    $query = $CI->db->select('MAX(SUBSTR(code, 12)) code')
            ->where('YEAR(date_added)', date('Y'))
            ->where('MONTH(date_added)', date('m'))
            ->get('order_invoice');
    if ($query->num_rows() > 0) {
        $increment = $query->row()->code;
    } else {
        $increment = 0;
    }
    $increment = ($increment == 0) ? 1 : $increment + 1;
    if ($increment < 10) {
        $increment = '000' . $increment;
    } elseif ($increment < 100) {
        $increment = '00' . $increment;
    } elseif ($increment < 1000) {
        $increment = '0' . $increment;
    }
    $code = 'INV/' . date('Ym') . '/' . $increment;
    return $code;
}

function payment_code() {
    $CI = get_instance();
    $CI->load->database();
    $query = $CI->db->select('MAX(SUBSTR(code, 12)) code')
            ->where('YEAR(date_added)', date('Y'))
            ->where('MONTH(date_added)', date('m'))
            ->get('order_payment');
    if ($query->num_rows() > 0) {
        $increment = $query->row()->code;
    } else {
        $increment = 0;
    }
    $increment = ($increment == 0) ? 1 : $increment + 1;
    if ($increment < 10) {
        $increment = '000' . $increment;
    } elseif ($increment < 100) {
        $increment = '00' . $increment;
    } elseif ($increment < 1000) {
        $increment = '0' . $increment;
    }
    $code = 'PAY/' . date('Ym') . '/' . $increment;
    return $code;
}

function seo_url($url) {
    $CI = get_instance();
    $CI->load->database();
    $CI->load->model('main');
    $query = $CI->main->get('seo_url', array('query' => $url));
    $url = ($query) ? $query->keyword : $url;
    return site_url($url);
}

function get_thumbnail($image) {
//    $image = explode('/', $image);
//    $variable = substr(end($image), 0, strpos(end($image), "."));
//    $image = site_url('files/image_thumbs/' . $variable . '.png');
    $image = site_url('files/image_thumbs/' . $image);
    $file_headers = @get_headers($image);
    if ($file_headers[0] == 'HTTP/1.0 404 Not Found') {
        return false;
    } else if ($file_headers[0] == 'HTTP/1.0 302 Found' && $file_headers[7] == 'HTTP/1.0 404 Not Found') {
        return false;
    } else {
        return $image;
    }
}

function get_image($image) {
    $image = base_url('files/images/' . $image);
    $file_headers = @get_headers($image);
    if ($file_headers[0] == 'HTTP/1.0 404 Not Found') {
        return false;
    } else if ($file_headers[0] == 'HTTP/1.0 302 Found' && $file_headers[7] == 'HTTP/1.0 404 Not Found') {
        return false;
    } else {
        return $image;
    }
}

function price_discount($price, $discount) {
    return $price - ($price * $discount / 100);
}

function weight_rounding($weight, $max = 0.2) {
    if ($weight > 1) {
        if (strpos($weight, '.')) {
            $number = explode('.', $weight);
            $decimal = '0.' . $number[1];
            if ($decimal > $max) {
                $decimal = 1;
            } else {
                $decimal = 0;
            }
            $weight = $number[0] + $decimal;
        } else {
            $weight = $weight;
        }
    } else
        $weight = 1;
    return $weight;
}

function settings($key) {
    if ($key) {
        $CI = get_instance();
        // if($CI->session->has_userdata('tnkSettings')){
        //     return $CI->session->userdata('tnkSettings')[$key];
        // }else{
            $CI->load->database();
            $settings = $CI->db->get('settings');
            foreach ($settings->result() as $setting) {
                $data[$setting->key] = $setting->value;
            }
            //$CI->session->set_userdata('tnkSettings', $data);
            return $data[$key];
        //}
    } else {
        return false;
    }
}
function get_date_indo_full($fulldate) {
    $date = substr($fulldate, 8, 2);
    $month = get_month(substr($fulldate, 5, 2));
    $year = substr($fulldate, 0, 4);
    $time = substr($fulldate, 10);
    return $date . ' ' . $month . ' ' . $year . ', ' . $time;
}

if (!function_exists('get_month')) {

    function get_month($month) {
        $CI = get_instance();
        if (is_numeric($month)) {
            $month = date('F', strtotime('2017-' . $month . '-01 00:00:00'));
        }
        return $CI->lang->line($month);
    }

}