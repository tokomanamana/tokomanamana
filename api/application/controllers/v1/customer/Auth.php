<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Auth extends REST_Controller {
    
    public $table = "customers";
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $params = array('rounds' => 8);
        $params['salt_prefix'] = version_compare(PHP_VERSION, '5.3.7', '<') ? '$2a$' : '$2y$';
        $this->load->library('bcrypt', $params);
        $this->load->model('Model', 'm');
    }
    
    public function login_post()
    {
        $username = $this->post('username');
        $password = $this->post('password');
        $this->db->select('*');
        $this->db->where('username',$username);
        $data = $this->db->get($this->table)->result();
        if(count($data) > 0) {
            if ($this->bcrypt->verify($password, $data[0]->password)) {
                $res = array('msg' => 'Login Sukses','data'=>$data[0]);
                $this->response($res, 200);
            } else {
                $res = array('msg' => 'Email / Password tidak cocok!','data'=>NULL);
                $this->response($res, 401);
            }
            
        } else {
            $res = array('msg' => 'Email belum terdaftar!','data'=>NULL);
            $this->response($res, 400);
        }
    }

    public function register_post()
    {
        $fullname = $this->post('fullname');
        $email = $this->post('email');
        $no_hp = $this->post('no_hp');
        $password = $this->post('password');

        $this->db->select('*');
        $this->db->where('username',$email);
        $this->db->or_where('phone',$no_hp);
        $data = $this->db->get($this->table)->result();
        if(count($data) > 0) {
            $res = array('msg' => 'Email / Phone sudah terdaftar!','data'=>NULL);
            $this->response($res, 400);
        } else {
            // IP Address
            $ip_address = '127.0.0.1';
            $password = $this->bcrypt->hash($password);
            // Users table.
            $data = array(
                'username' => $email,
                'password' => $password,
                'email' => $email,
                'ip_address' => $ip_address,
                'created_on' => time(),
                'active' => 1
            );
            $this->db->insert($this->table, $data);
            $datax = array(
                'customer' => $this->db->insert_id(),
                'group' => 1,
            );
            $this->db->insert('customer_to_groups', $datax);
            $res = array('msg' => 'Registrasi berhasil!','data'=>NULL);
            $this->response($res, 200);
        }
    }

    public function update_profile_post() {
        $id = $this->post('id');
        if(isset($id)){
            $fullname = $this->post('fullname');
            $birthday = $this->post('birthday');
            $gender = $this->post('gender');
            $base64_img = $this->post('base64_img');
            // Users table.
            $data = array();
            if($fullname){
                $data['fullname'] = $fullname;
            }
            if($base64_img){
                $data['base64_img'] = $base64_img;
            }
            if($gender){
                $data['gender'] = $gender;
            }
            if($birthday){
                $data['birthday'] = $birthday;
            }
            $this->db->where('id',$id);
            $save = $this->db->update('customers', $data);
            if($save){
                $res = array('msg' => 'Update Profil berhasil!','data'=>$data);
                $this->response($res, 200);
            } else {
                $res = array('msg' => 'Update Profil gagal!','data'=>NULL);
                $this->response($res, 400);
            }
        } else {
            $res = array('msg' => 'ID User tidak boleh kosong!','data'=>NULL);
            $this->response($res, 400);
        }
        
    }

    public function ubah_password_post(){
        $m = $this->m;
        $id_cust = $this->post('id_customer');
        $pass_lama = $this->post('pass_lama');
        $pass_baru = $this->post('pass_baru');
        $pass_baru1 = $this->post('pass_baru1');

        $q = $m->get_data('', 'customers', null, array('id' => $id_cust));
        if($q->num_rows() == 0){
            $res = array('success' => 0, 'msg' => 'Customer Tidak Ada');
        }else{
            $r = $q->row();
            if($this->bcrypt->verify($pass_lama, $r->password)){
                if($pass_baru == $pass_baru1){
                    $password = $this->bcrypt->hash($pass_baru);
                    $m->update_data('customers', array('password' => $password), array('password' => $password));
                    $res = array('success' => 1);
                }else{
                    $res = array('success' => 0, 'msg' => 'Password Baru Tidak Sama');
                }
            }else{
                $res = array('success' => 0, 'msg' => 'Password Lama Salah');
            }
            
        }

        $this->response($res, 200);
    }

    public function get_data_diri_post(){
        $m = $this->m;

        $id_cust = $this->post('id_cust');

        $q = $m->get_data('', 'customers', null, array('id' => $id_cust, 'active' => '1'));
        if($q->num_rows() == 0){
            $res = array('msg' => 'Permintaan Tidak Dikenali', 'data' => NULL);
        }else{
            $r = $q->row();
            $data = array(
                'email' => $r->email,
                'nama' => $r->fullname,
                'gender' => $r->gender,
                'tgl_lahir' => $r->birthday,
                'hp' => $r->phone,
                'date_now' => date('Y-m-d')
            );
            $res = array('data' => $data);
        }

        $this->response($res, 200);
    }

    public function update_data_diri_post(){
        $m = $this->m;
        $id_cust = $this->post('id_cust');
        $email = $this->post('email');
        $nama = $this->post('nama');
        $gender = $this->post('gender');
        $tgl_lahir = $this->post('tgl_lahir');
        $hp = $this->post('hp');
        
        $cek_cust = $m->get_data('', 'customers', null, array('id' => $id_cust, 'active' => '1'));
        if($cek_cust->num_rows() > 0){
            $cek_email = $m->get_data('', 'customers', null, array('id !=' => $id_cust, 'email' => $email));
            if($cek_email->num_rows() == 0){
                $data = array(
                    'email' => $email,
                    'fullname' => $nama,
                    'gender' => $gender,
                    'birthday' => date('Y-m-d', strtotime($tgl_lahir)),
                    'phone' => $hp
                );

                $m->update_data('customers', $data, array('id' => $id_cust));

                $res = array('success' => 1, 'msg' => 'Data Diri Berhasil Diupdate');
            }else{
                $res = array('success' => 0, 'msg' => 'Email Sudah Dipakai');
            }
        }else{
            $res = array('success' => 0, 'msg' => 'Permintaan Tidak Dikenali');
        }

        $this->response($res, 200);
    }

}
