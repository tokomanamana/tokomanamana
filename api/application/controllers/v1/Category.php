<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Category extends REST_Controller {
    
    public $table = "categories";
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $params = array('rounds' => 8);
        $params['salt_prefix'] = version_compare(PHP_VERSION, '5.3.7', '<') ? '$2a$' : '$2y$';
        $this->load->library('bcrypt', $params);
        $this->load->model('Model', 'm');
    }
    
    public function list_category_get(){
        $m = $this->m;

        $q = $m->get_data('id, name, icon', 'categories', null, array('active' => 1, 'status_global' => 1, 'parent' => 0), '', 'name asc');
        $data = array();
        foreach($q->result() as $key){
            $q0 = $m->get_data('id, name', 'categories', null, array('active' => 1, 'status_global' => 1, 'parent' => $key->id), '', 'name asc');
            $key->child = $q0->result();
            $data[] = $key;
        }

        $this->response(array('success' => 1, 'data' => $data), 200);
    }
    public function all_get()
    {
        $limit = $this->get('limit');
        $parent = $this->get('parent');
        if($limit){
            $this->db->limit($limit);
        }
        if(isset($parent)){
            $this->db->where('parent',$parent);
            $this->db->order_by('home_sort_order','asc');
        } else {
            $this->db->order_by('name','asc');
        }
        $this->db->select('id,name');
        $this->db->where('active', '1');
        $q = $this->db->get($this->table)->result_array();
        $this->response($q, 200);
    }
}
