<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Merchants extends REST_Controller {
    
    public $table = "merchants";
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        
    }
    public function total_get()
    {
        $album = $this->db->count_all($this->table);
        $this->response($album, 200);
    }
    
    public function all_get()
    {
        $limit = $this->get('limit');
        if($limit){
            $this->db->limit($limit);
        }
        $this->db->select('a.id,a.type,a.name,a.group,b.name as zone_name');
        $this->db->join('merchant_groups as b','b.id = a.group','left');
        $this->db->order_by('a.name','ASC');
        $data = $this->db->get($this->table.' as a')->result();
        $this->response($data, 200);
    }

    public function groups_get()
    {
        $limit = $this->get('limit');
        if($limit){
            $this->db->limit($limit);
        }
        $this->db->select('a.id,a.name,a.branch as branch_id,b.name as branch_name');
        $this->db->join('merchants as b','b.id = a.branch','left');
        $this->db->order_by('a.name','ASC');
        $data = $this->db->get('merchant_groups as a')->result();
        $this->response($data, 200);
    }
    

}
