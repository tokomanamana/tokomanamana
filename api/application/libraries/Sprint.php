<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sprint {

    function __construct() {
        $this->app = & get_instance();
//        $this->app->load->model('transactions_model');
//        $this->app->load->model('transactions_meta_model');
//        $this->app->load->model('products_model');
    }

    function process($transaction, $vendor = false) {
        $this->app->db->where('name',$transaction->payment_method);
        $payment_method = $this->app->db->get('payment_methods');
        $payment_method = $payment_method->row(0) ;
        $setting = json_decode($payment_method->setting);
        $coupon = NULL;
        $payload = [
            'channelId' => $setting->channel_id,
            'currency' => 'IDR',
            'transactionNo' => $transaction->id,
            'transactionAmount' => $transaction->total,
            'transactionFee' => 0,
            'additionalFee' => 0,
            'transactionDate' => $transaction->date_added,
            'transactionExpire' => $transaction->due_date,
            'transactionFeature' => '',
            'callbackURL' => '',
            'description' => $transaction->code,
//            'customerAccount' => $customer_account,
            'customerName' => $transaction->customer_name,
            'customerEmail' => $transaction->customer_email,
            'customerPhone' => $transaction->customer_phone,
            'customerBillAddress' => $transaction->shipping_address,
            'customerBillCity' => $transaction->shipping_city_name,
            'customerBillState' => $transaction->shipping_province_name,
            'customerBillCountry' => 'ID',
            'customerBillZipCode' => $transaction->shipping_postcode,
            'authCode' => hash('sha256', $transaction->id . $transaction->total . $setting->channel_id . $setting->secret_key),
        ];
        if ($transaction->payment_method == 'bca_va') {
            $payload['customerAccount'] = $setting->code . preg_replace("/[^0-9]/", "", $transaction->code);
        } elseif ($transaction->payment_method == 'kredivo') {
            $products = $this->app->main->gets('order_product', array('order' => $transaction->id));
            $invoice = $this->app->main->gets('order_invoice', array('order' => $transaction->id));
            $invoice_res = $invoice->result();
            $total_fee = (2.3/100)*($invoice_res[0]->total + $invoice_res[0]->shipping_cost);
            //$total_fee = 1000;
            $items = array();
            foreach ($products->result() as $product) {
                $item = array(
                    'itemId' => $product->product,
                    'itemName' => $product->name,
                    'quantity' => $product->quantity,
                    'price' => $product->price,
                    'itemURL' => seo_url('catalog/products/view/' . $product->product), 
                    'itemType' => 'Product'
                );
                array_push($items, $item);
            }
            // $items[] = array(
            //     'itemId' => 'discount',
            //     'itemName' => 'Discount',
            //     'quantity' => '1',
            //     'price' => '-10000',
            //     'itemURL' => seo_url('catalog/products/view/discount'), 
            //     'itemType' => 'Discount'
            // );
            $payload['transactionAmount'] = sprintf("%.0f",$payload['transactionAmount'] + $total_fee);
            $payload['authCode'] =  hash('sha256', $transaction->id .$payload['transactionAmount'] . $setting->channel_id . $setting->secret_key);
            $string = sprintf("%.0f", round($total_fee,2));
            if($coupon){
                $string_disc = sprintf("%.0f", round($coupon->total_disc,2));
                $payload['transactionFee'] = json_encode(['shippingfee'=>$transaction->shipping_cost,'additionalfee'=>$string]);
                $payload['transactionFeature'] = json_encode(['discount'=>$string_disc]);
            } else {
                $string_disc = 0;
                $payload['transactionFee'] = json_encode(['shippingfee'=>$transaction->shipping_cost,'additionalfee'=>$string]);
            }
            
            $payload['itemDetails'] = json_encode($items);
        }elseif($transaction->payment_method=='creditcard' ||
            $transaction->payment_method=='creditcard01' || 
            $transaction->payment_method=='creditcard03' || 
            $transaction->payment_method=='creditcard06' || 
            $transaction->payment_method=='creditcard12'
        ){
            $payload['serviceCode'] = 2001;
        }
        $payment_request = $this->post($setting->payment_url, $payload);
        //var_dump($payload);exit();
//        $this->app->main->insert($transaction->id, 'sprint_response', $payment_request);
//
        //if ($payment_request['insertStatus'] == '00') {
        //return array('request' => $payload, 'response' => $payment_request);
        echo json_encode($payment_request);
        // print_r($payment_request); exit;
//            if ($transaction->payment_type == 'bca_klikpay') {
//                $this->bca_klikpay($payment_request['redirectURL'], $payment_request['redirectData']);
//            } elseif ($transaction->payment_type == 'kredivo') {
//                $this->kredivo($payment_request['redirectURL'], $payment_request['redirectData']);
//            } else {
//                $this->virtual_account($transaction, $payment_request);
//            }
        // } else {
        //     echo 'Shipping Fee : '.$transaction->shipping_cost .' / ';
        //     echo 'Additional Fee : '.$string.' / ';
        //     echo 'Discount : '.$string_disc;
        //     echo 'Total : '.$transaction->total.' / ';
        //     throw new Exception(json_encode($payment_request), 1);
        // }
    }

    function creditcard_charge($data) {
        return $this->post('/payment/creditcard_charge', $data);
    }

    function bca_klikpay($redirectURL, $redirectData) {
        echo '<form name="redirect" action="' . $redirectURL . '" method="POST">
        <input type="hidden" name="klikPayCode" value="' . $redirectData['klikPayCode'] . '">
        <input type="hidden" name="transactionNo" value="' . $redirectData['transactionNo'] . '">
        <input type="hidden" name="totalAmount" value="' . $redirectData['totalAmount'] . '">
        <input type="hidden" name="currency" value="' . $redirectData['currency'] . '">
        <input type="hidden" name="payType" value="' . $redirectData['payType'] . '">
        <input type="hidden" name="callback" value="' . $redirectData['callback'] . '">
        <input type="hidden" name="transactionDate" value="' . $redirectData['transactionDate'] . '">
        <input type="hidden" name="descp" value="' . $redirectData['descp'] . '">
        <input type="hidden" name="miscFee" value="' . $redirectData['miscFee'] . '">
        <input type="hidden" name="signature" value="' . $redirectData['signature'] . '">
        <input type="submit" style="visibility:hidden;">
        </form>
       <script type="text/javascript">document.redirect.submit();</script>';
    }

    private function get($path, $data = array()) {
        $curl = curl_init();

        $headers = array();
        $headers[] = 'apikey: ' . $this->secret_api_key;
        $headers[] = 'Content-Type: application/json';

        $end_point = $this->server_domain . $path . '?' . http_build_query($data, '', '&');

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_USERPWD, $this->secret_api_key . ":");
        curl_setopt($curl, CURLOPT_URL, $end_point);
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        curl_close($curl);

        $responseObject = json_decode($response, true);
        return $responseObject;
    }

    private function post($url, $data = array()) {
        $curl = curl_init();

        $headers = array();
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';

        $payload = json_encode($data);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        curl_close($curl);

        $responseObject = json_decode($response, true);
        return $responseObject;
    }

    function sms($url, $data = array()) {
        $curl = curl_init();

        $end_point = $url . '?' . http_build_query($data, '', '&');

        curl_setopt($curl, CURLOPT_URL, $end_point);
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }
}
