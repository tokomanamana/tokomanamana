<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>404 - Not Found</title>
</head>
<body style="vertical-align: middle; 
                margin: auto; 
                -webkit-text-size-adjust: 100%; 
                -ms-text-size-adjust: 100%; 
                height: 100% !important; 
                width: 100% !important;">
    <table width="100%" height="100%" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -mso-table-lspace: 0pt; -mso-table-rspace: 0pt; vertical-align: middle; border-collapse: collapse !important; height: 100% !important; width: 100% !important;">
        <tr>
            <td style="text-align: center;">
                <img src="<?php echo base_url('assets/frontend/images/404-notfound.jpeg'); ?>" style="margin: 0 10%; vertical-align: middle; cursor: pointer;" onclick="window.history.back()">
                <p style="font-family: cursive; color: #97C23C; margin-bottom: unset; font-weight: bolder;">Terjadi masalah !</p>
                <p style="font-family: cursive; color: #97C23C; margin-top: unset;">Halaman yang anda tuju tidak ditemukan</p>
            </td>
        </tr>
    </table>
</body>
</html>