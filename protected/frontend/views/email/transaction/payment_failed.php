<div bgcolor="#FFFFFF" style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;width:100%!important;height:100%;font-size:14px;color:#404040;margin:0;padding:0">
    <table style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:0" bgcolor="transparent">
        <tbody>
            <tr style="margin:0;padding:0">
                <td style="margin:0;padding:0"></td>
                <td style="display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0" bgcolor="#FFFFFF">
                    <div style="max-width:600px;display:block;border-collapse:collapse;margin:0 auto;border:1px solid #e7e7e7">
                        <table style="max-width:100%;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:20px;" bgcolor="transparent">
                            <tbody>
                                <tr style="margin:0;padding:0">
                                    <td>
                                        <img src="<?php echo site_url('../assets/frontend/images/logo.png'); ?>" alt="" title="" style="margin:10px 0;height: 35px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="<?php echo site_url('../assets/frontend/images/paymentgagal.png'); ?>" alt="" title="" style="margin-bottom: 16px;
                                            max-height: 128px;
                                            margin-left: auto;
                                            margin-right: auto;
                                            display: block;">
                                    </td>
                                </tr>

                                <tr style="margin:0;padding:0">
                                    <td style="margin:0;padding:0">
                                        <h5 style="line-height:32px;color:#666;font-weight:700;font-size:24px;margin:0 0 20px;padding:0">Pembayaran #<?php echo $order->code; ?> Gagal</h5>
                                        <p style="font-weight:normal;color:#999;font-size:14px;line-height:1.6;margin:0 0 20px;padding:0">
                                            Hai <?php echo $customer->fullname; ?>! Mohon maaf tagihan <b><?php echo $order->code; ?></b> dibatalkan karena pembayaran kamu gagal.
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table style="width:100%;margin-bottom:24px;padding:0 20px;">
                            <thead>
                                <tr><td style="width:50%">
                                        <span style="font-size:12px;color:#666;font-weight:600">Total Pembayaran</span>
                                    </td>
                                    <td style="width:50%">
                                        <span style="font-size:12px;color:#666;font-weight:600">Status Pembayaran</span>
                                    </td>
                                </tr></thead>
                            <tbody>
                                <tr>
                                    <td style="vertical-align:top;">

                                        <p style="font-size:16px;color:#999;margin:0 0 5px 0"><?php echo ($order->payment_method == 'transfer') ? rupiah($order->total_plus_kode) : rupiah($order->total); ?></p>

                                    </td>
                                    <td style="vertical-align:top;">
                                        <p style="font-size:16px;color:#999;margin:0 0 5px 0">Gagal</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table style="width:100%;margin-bottom:24px;padding:0 20px; ">
                            <thead>
                                <tr><td style="width:50% ">
                                        <span style="font-size:12px;color:#666;font-weight:600 ">Metode Pembayaran</span>
                                    </td>
                                    <td style="width:50% ">
                                        <span style="font-size:12px;color:#666;font-weight:600 ">Referensi Pembayaran</span>
                                    </td>
                                </tr></thead>
                            <tbody>
                                <tr>
                                    <td style="vertical-align:top">
                                        <p style="font-size:16px;color:#999;margin:5px 0"><?php echo $payment_method; ?></p>
                                    </td>
                                    <td style="vertical-align:top">
                                        <p style="font-size:16px;color:#999;"><?php echo $order->code; ?></p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="padding:0 20px">
                            <p style="font-size:14px;color:#999;padding:24px 0 10px;margin:0;border-top: 1px solid #E0E0E0;">
                                Email dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.
                            </p>
                        </div>
                    </div>
                </td>
                <td style="margin:0;padding:0"></td>
            </tr>
        </tbody>
    </table>
</div>