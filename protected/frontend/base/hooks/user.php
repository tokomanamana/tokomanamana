<?php

function get_user() {
    $CI = & get_instance();
    if ($CI->ion_auth->logged_in()) {
        if (!$CI->session->has_userdata('user')) {
            $user = $CI->ion_auth->user()->row();
            $CI->session->set_userdata('user', $user, 3600);
        }
        $CI->data['user'] = $CI->session->userdata('user');
    }
    print_r($CI->session->userdata('user'));
    print_r($CI->ion_auth->user()->row());
}
