<?php

function get_user() {
    $CI = & get_instance();
    $CI->data['categories'] = $CI->main->gets('categories', array('parent' => 0, 'active' => 1));
    if ($CI->ion_auth->logged_in()) {
        if (!$CI->session->has_userdata('user')) {
            $user = $CI->ion_auth->user()->row();
            $CI->session->set_userdata('user', $user);
        }
        $CI->data['user'] = $CI->session->userdata('user');
        $CI->data['orders_pending'] = $CI->main->gets('orders',array('customer'=>$CI->data['user']->id,'payment_status'=>'Pending'));
        $CI->data['total_active_order'] = $CI->db->where_not_in('order_status',array(settings('order_finish_status'), settings('order_cancel_status')))->where('customer',$CI->data['user']->id)->count_all_results('order_invoice');
        $CI->data['orders_pending'] = $CI->main->gets('orders',array('customer'=>$CI->data['user']->id,'payment_status'=>'Pending'));
        //$CI->data['complain_chat'] = $CI->db->where('type','merchant')->where('receiver_id',$CI->data['user']->id)->count_all_results('chat_complain');
        //$CI->data['total_notif'] =  $CI->data['total_active_order'] + $CI->data['complain_chat'];
        $CI->data['total_notif'] =  $CI->data['total_active_order'];
    }
}

function merchant_by_distance() {
    $CI = & get_instance();
//    log_message('debug', 'get-merchant');
    if (!$CI->session->has_userdata('list_merchant')) {
//        log_message('debug', 'sessino tidak ada');
        if ($CI->session->has_userdata('location')) {
            $location = $CI->session->userdata('location');
            $merchant = $CI->db->query("SELECT id, `name`, `group`, (6971 * acos(cos(radians($location[lat])) * cos(radians(lat)) * cos(radians(lng) - radians($location[lng])) + sin(radians($location[lat])) * sin(radians(lat)))) AS distance FROM merchants WHERE status = 1 HAVING distance < 80 ORDER BY distance");
            if ($merchant->num_rows() > 0) {
                $CI->session->set_userdata('list_merchant', $merchant->result_array());
            } else {
                $merchant = $CI->db->query("SELECT id, `name`, `group`, (6971 * acos(cos(radians($location[lat])) * cos(radians(lat)) * cos(radians(lng) - radians($location[lng])) + sin(radians($location[lat])) * sin(radians(lat)))) AS distance FROM merchants WHERE status = 1 ORDER BY distance LIMIT 5");
                $CI->session->set_userdata('list_merchant', $merchant->result_array());
            }
        }
    }
//    print_r($CI->session->userdata('list_merchant'));
}

function redirect_ssl() {
    $CI = & get_instance();

    if (isset($_SERVER['HTTPS']) and $_SERVER['HTTPS'] == 1) {
        
    } elseif (isset($_SERVER['HTTPS']) and $_SERVER['HTTPS'] == 'on') {
        
    } else {
        $CI->config->config['base_url'] = str_replace('http://', 'https://', $CI->config->config['base_url']);
        redirect($CI->uri->uri_string());
    }
}
