<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_methods extends MY_Model {

    public $table = 'payment_methods';
    public $primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
        $this->timestamps = false;
    }
}
