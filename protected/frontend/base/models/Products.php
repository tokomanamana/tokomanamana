<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends MY_Model {

    public $table = 'products';
    public $primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
        $this->timestamps = false;
    }
}
