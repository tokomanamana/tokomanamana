<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Model {

    public function get($id) {
        $this->db->select('o.*, cst.fullname customer_name, cst.email customer_email, cst.phone customer_phone, p.name shipping_province_name, c.name shipping_city_name, d.name shipping_district_name')
                ->join('customers cst', 'cst.id = o.customer', 'left')
                ->join('provincies p', 'p.id = o.shipping_province', 'left')
                ->join('cities c', 'c.id = o.shipping_city', 'left')
                ->join('districts d', 'd.id = o.shipping_district', 'left')
                ->where('o.id', $id);
        $query = $this->db->get('orders o');
        return ($query->num_rows()>0)?$query->row():false;
    }

    public function cctv_get($id) {
        $this->db->select('o.*')->where('o.id', $id);
        $query = $this->db->get('cctv_orders o');
        return ($query->num_rows()>0)?$query->row():false;
    }
    
    public function voucher_get($id){
        $this->db->select('o.*')->where('o.id', $id);
        $query = $this->db->get('app_voucher_orders o');
        return ($query->num_rows()>0)?$query->row():false;
    }
    
}
