<?php
class Ipaycallback {
    var $array, $merchantKey, $host;
    public function __construct() {
        if (isset($_REQUEST['TransId'])){
        $this->_ci = & get_instance();
        $this->_ci->load->config('ipay88', TRUE);
        $merchant_key = $this->_ci->config->item('merchant_key', 'ipay88');
        $merchant_code = $this->_ci->config->item('merchant_code', 'ipay88');
        $this->merchantKey = $merchant_key;
        $this->array = array(
            'MerchantCode' => $merchant_code,
            'PaymentId' => $_REQUEST['PaymentId'],
            'RefNo' => $_REQUEST['RefNo'],
            'Amount' => $_REQUEST['Amount'],
            'Currency' => $_REQUEST['Currency'],
            'Remark' => $_REQUEST['Remark'],
            'TransId' => $_REQUEST['TransId'],
            'AuthCode' => $_REQUEST['AuthCode'],
            'Status' => $_REQUEST['Status'],
            'ErrDesc' => $_REQUEST['ErrDesc'],
            'Signature' => $_REQUEST['Signature'],
        );
        } else {
            header("Location: ".site_url()."");
		    die();
        }
    }
    /*
     * Generate iPay88 Signature for Verification
     * Indirect Use
     * 
     */
    function iPay88_signature($source) {
        return base64_encode(hex2bin(sha1($source)));
    }
    function hex2bin($hexSource) {
        for ($i = 0; $i < strlen($hexSource); $i = $i + 2) {
            $bin .= chr(hexdec(substr($hexSource, $i, 2)));
        }
        return $bin;
    }
    /*
     * By default Signature verification is enough
     * But, if want extra verification, make extra call
     * 
     */
    public function verifySignature() {
        $amount = preg_replace("/[^0-9]/", "", $this->array['Amount']);
        $string = $this->iPay88_signature($this->merchantKey . $this->array['MerchantCode'] . $this->array['PaymentId'] . $this->array['RefNo'] . $amount . $this->array['Currency'] . $this->array['Status']);
        if ($string == $this->array['Signature']) {
            //print_r("RECEIVEOK");exit;	
            //echo 'RECEIVEOK';
            return $this;
        } else {
            var_dump($string);
            var_dump($this->array);
            exit('Signature Not Match');
        }
    }
    
    public function requeryStatus(){
        $host = 'https://www.mobile88.com/epayment/enquiry.asp';
        $host.= '?MerchantCode='.$this->array['MerchantCode'].'&RefNo='.$this->array['RefNo']. '&Amount='.$this->array['Amount'];
        $process = curl_init();
        curl_setopt($process, CURLOPT_URL, $host);
        curl_setopt($process, CURLOPT_HEADER, 0);
        curl_setopt($process, CURLOPT_TIMEOUT, 10);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($process, CURLOPT_POSTFIELDS, http_build_query($this->array));
        $return = curl_exec($process);
        curl_close($process);
        if ($return == '00'){
            return $this;
        }else {
            exit($return);
        }
    }
    
    /*
     *  Not used in callback
     */
    public function getData() {
        return $this->array;
    }
}