<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Merchant_model extends CI_Model {

    function get_categories($parent = 0) {
        $this->db->select('id, name, image, icon')
                ->where('parent', $parent)
                ->order_by('sort_order')
                ->where('active', 1);
        $query = $this->db->get('categories');
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    function get($id) {
        $this->db->select('merchants.id,
                            merchants.username,
                            merchants.type,
                            merchants.name,
                            merchants.short_description,
                            merchants.description,
                            merchants.address,
                            merchants.postcode,
                            merchants.image_profile,
                            merchants.image_cover,
                            provincies.name AS province_name,
                            cities.name AS city_name,
                            districts.name AS district_name', FALSE)
                ->join('provincies', 'merchants.province = provincies.id', 'LEFT')
                ->join('cities', 'merchants.city = cities.id', 'LEFT')
                ->join('districts', 'merchants.district = districts.id', 'LEFT')
                ->where('merchants.id', $id);
        
        $query = $this->db->get('merchants');

        return ($query->num_rows() > 0) ?$query->row() : FALSE;
        // $this->db->select("m.id, m.name, short_description, address, p.name province_name, c.name city_name", FALSE)
        //         ->join('provincies p', 'p.id = m.province', 'left')
        //         ->join('cities c', 'c.id = m.city', 'left')
        //         ->where('m.id', $id);
        // $query = $this->db->get('merchants m');
        // return ($query->num_rows() > 0) ? $query->row() : FALSE;
    }

    function getProducts($merchant, $sort = 'popular', $length = 20, $start = 0) {
        $query_order = 'ORDER BY ';
        switch ($sort) {
            default:
            case 'popular':
                $query_order .= 'products.viewed DESC';
                break;
            case 'newest' :
                $query_order .= 'products.date_added DESC';
                break;
            case 'lowprice' :
                $query_order .= 'products.price ASC';
                break;
            case 'highprice' :
                $query_order .= 'products.price DESC';
                break;
            case 'nameAZ' :
                $query_order .= 'products.name ASC';
                break;
            case 'nameZA' :
                $query_order .= 'products.name DESC';
                break;
        }
        $limit = '';
        if($length){
            $limit = 'LIMIT ';
            if($start){
                $limit .= $start.','.$length;
            }else{
                $limit .= $length;
            }
            
        }

        $query = $this->db->query("SELECT * FROM 
                                    (SELECT products_principal_stock.branch_id AS store_id,
                                            products_principal_stock.product_id AS product_id,
                                            products_principal_stock.quantity AS quantity,
                                            merchants.name AS merchant_name,
                                            products.name,
                                            products.merchant,
                                            products.price,
                                            products.price_old,
                                            products.discount,
                                            products.promo,
                                            products.preorder,
                                            products.promo_data,
                                            products.status,
                                            product_image.image,
                                            products.viewed,
                                            products.date_added
                                    FROM products_principal_stock
                                    LEFT JOIN products ON products_principal_stock.product_id = products.id
                                    LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id
                                    LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                    WHERE products_principal_stock.branch_id = $merchant
                                    
                                    UNION
                                    
                                    SELECT products_principal_stock.branch_id AS store_id,
                                            products_principal_stock.product_id AS product_id,
                                            products_principal_stock.quantity AS quantity,
                                            merchants.name AS merchant_name,
                                            products.name,
                                            products.merchant,
                                            products.price,
                                            products.price_old,
                                            products.discount,
                                            products.promo,
                                            products.preorder,
                                            products.promo_data,
                                            products.status,
                                            product_image.image,
                                            products.viewed,
                                            products.date_added
                                    FROM products_principal_stock
                                    LEFT JOIN product_merchant ON products_principal_stock.product_id = product_merchant.product
                                    LEFT JOIN products ON products_principal_stock.product_id = products.id
                                    LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id
                                    LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                    WHERE products_principal_stock.branch_id = $merchant)

                                    products
                                
                                
                                
                                WHERE products.status != 0
                                AND (products.quantity > 0 OR products.preorder = 1)
                                
                                $query_order
                                $limit");
        
        return ($query->num_rows() > 0) ? $query : FALSE;
        // return $this->db->query("SELECT p.*
        //                         FROM
        //                         (SELECT p.id, p.code, p.name, p.category, pi.image, p.discount, p.price, price_old, p.quantity, status, merchant, 'pm', p.viewed FROM products p LEFT JOIN product_image pi ON p.id = pi.product AND pi.primary = 1 WHERE p.merchant = " . $merchant . "
                        
        //                         UNION
                        
        //                         SELECT p.id, p.code, p.name, p.category, pi.image, pm.discount, pm.price, pm.price_old, pm.quantity, pm.status, p.merchant, pm.id, p.viewed FROM products p LEFT JOIN product_image pi ON p.id = pi.product AND pi.primary = 1
        //                         LEFT JOIN product_merchant pm ON p.id = pm.product
        //                         WHERE pm.merchant = " . $merchant . ") p
        //                         ORDER BY $order_by
        //                         $limit");
//        $this->db->select("p.id, p.price, p.price_old, p.discount, pi.image, p.name, p.short_description, p.merchant, (SELECT COUNT(id) FROM product_merchant pm WHERE pm.product = p.id) total_merchant", FALSE)
//                ->join('product_image pi', 'pi.product = p.id AND pi.primary = 1', 'left')
//                ->where("p.category IN (SELECT category FROM category_path WHERE path = $category)")
//                ->where('p.status', 1);
//        if ($limit)
//            $this->db->limit($limit, $start);
//        $query = $this->db->get('products p');
//        return $query;
    }

    function get_category_path($category) {
        $this->db->select("c.id, c.name", FALSE)
                ->join('categories c', 'pc.path = c.id', 'left')
                ->where('category', $category)
                ->order_by('level', 'ASC');
        $query = $this->db->get('category_path pc');
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

}
