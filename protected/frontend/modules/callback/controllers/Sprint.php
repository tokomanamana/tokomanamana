<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sprint extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        ini_set('display_errors', 0);
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');

        $data = $this->input->post(null, true);

        if (!$data) {
            echo 'Error. Data return NULL';
            exit;
        }

        $this->load->model('orders');
        $first_char = substr($data['transactionNo'], 0, 1);
        if ($first_char == 'C') {
            $type = "cctv";
            $order = str_replace('C', '', $data['transactionNo']);
            $order = $this->orders->cctv_get($order);
        } elseif ($first_char == 'V') {
            $type = 'voucher';
            $order = str_replace('V', '', $data['transactionNo']);
            $order = $this->orders->voucher_get($order);
        } else {
            $type = 'shop';
            $order = str_replace('M', '', $data['transactionNo']);
            $order = $this->orders->get($order);
        }

        if (!$order) {
            $paymentStatus = '01';
            $paymentMessage = 'Invalid transactionNo';
        } else {
            $order->payment_to = json_decode($order->payment_to);

            if ($type == 'shop') {
                $this->main->insert('order_meta', array('order_id' => $order->id, 'meta_name' => 'sprint_reponse_flag', 'meta_value' => json_encode($data)));
                $meta = $this->main->get('order_meta', array('order_id' => $order->id, 'meta_name' => 'sprint_response'));
            } elseif ($type == 'cctv') {
                $this->main->insert('cctv_order_meta', array('order_id' => $order->id, 'meta_name' => 'sprint_response_flag', 'meta_value' => json_encode($data)));
                $meta = $this->main->get('cctv_order_meta', array('order_id' => $order->id, 'meta_name' => 'sprint_response'));
            } elseif ($type == 'voucher') {
                $this->main->insert('app_voucher_order_meta', array('order_id' -> $order->id, 'meta_name' => 'sprint_response'));
            }

            // $meta = $this->transactions_meta_model->get_meta($transaction_data->id, 'sprint_response');
            $meta_data = json_decode($meta->meta_value);
            // print_r($meta_data);
            $payment_method = $this->main->get('payment_methods', array('name' => $order->payment_method));
            $payment_method->setting = json_decode($payment_method->setting);

            if ($type == 'shop') {
                $authCode = hash('sha256', 'M' . $order->id . $order->total . $data['channelId'] . $data['transactionStatus'] . $meta_data->insertId . $payment_method->setting->secret_key);
            } elseif ($type == 'cctv') {
                $authCode = hash('sha256', 'C' . $order->id . $order->total . $data['channelId'] . $data['transactionStatus'] . $meta_data->insertId . $payment_method->setting->secret_key);
            } elseif ($type == 'voucher') {
                $authCode = hash('sha256', 'V' . $order->id . $order->total . $data['channelId'] . $data['transactionStatus'] . $meta_data->insertId . $payment_method->setting->secret_key);
            }

            if ($data['channelId'] != $payment_method->setting->channel_id) {
                $paymentStatus = '01';
                $paymentMessage = 'Invalid channelID';
            } elseif ($data['currency'] != 'IDR') {
                $paymentStatus = '01';
                $paymentMessage = 'Invalid currency';
                // } elseif (strlen($data['transactionStatus']) != 2) {
            } elseif ($data['transactionStatus'] != '00') {
                $paymentStatus = '01';
                $paymentMessage = 'Invalid transactionStatus';
                // } elseif (isset($data['customerAccount']) && $data['customerAccount'] != ($payment_method->setting->code . $order->id)) {
            } elseif (isset($data['customerAccount']) && $data['customerAccount'] != ($order->payment_to->account_number)) {
                $paymentStatus = '01';
                $paymentMessage = 'Invalid customerAccount';
            } elseif ($data['transactionAmount'] != $order->total) {
                $paymentStatus = '01';
                $paymentMessage = 'Invalid transactionAmount';
            } elseif ($data['insertId'] != $meta_data->insertId) {
                $paymentStatus = '01';
                $paymentMessage = 'Invalid insertId';
            } elseif ($data['authCode'] != $authCode) {
                $paymentStatus = '01';
                $paymentMessage = 'Invalid authCode ' . $authCode;

                if ($type == 'shop') {
                    $this->main->update('orders', array('payment_status' => 'Failed'), array('id' => $order->id));
                    $this->main->update('order_invoice', array('order_status' => settings('order_cancel_status')), array('order' => $order->id));
                } elseif ($type == 'cctv') {
                    $this->main->update('cctv_orders', array('payment_status' => 'Failed'), array('id' => $order->id));
                    $this->main->update('cctv_order_invoice', array('order_status' => settings('order_cancel_status')), array('order' => $order->id));
                } elseif ($type == 'voucher') {
                    $this->main->update('app_voucher_orders', array('payment_status' => 'Failed'), array('id' => $order->id));
                    $this->main->update('app_voucher_order_invoice', array('order_status' => settings('order_cancel_status')), array('order' => $order->id));
                }
                
                if ($type == 'shop') {
                    $invoices = $this->main->gets('order_invoice', array('order' => $order->id));

                    if ($invoices) {
                        foreach ($invoices->result() as $invoice) {
                            $this->main->insert('order_history', array('order' => $order->id, 'invoice' => $invoice->id, 'order_status' => settings('order_cancel_status')));
                        }
                    }
                } elseif ($type == 'cctv') {
                    $invoice = $this->main->gets('cctv_order_invoice', array('order' => $order->id));

                    if ($invoices) {
                        foreach($invoices->result() as $invoice) {
                            $this->main->insert('cctv_order_history', array('order' => $order->id, 'invoice' => $invoice->id, 'order_status' => settings('order_cancel_status')));
                        }
                    }
                } elseif ($type == 'voucher') {
                    $invoices = $this->main->gets('app_voucher_order_invoice', array('order' => $order->id));

                    if ($invoices) {
                        foreach ($invoices->result() as $invoice) {
                            $this->main->insert('app_voucher_order_history', array('order' => $order->id, 'invoice' => $invoice->id, 'order_status' => settings('order_cancel_status')));
                        }
                    }
                }
            } elseif ($order->payment_status == 'Paid') {
                $paymentStatus = '02';
                $paymentMessage = 'Declined (Transaction has been paid)';
            } elseif (time() > strtotime($order->due_date)) {
                $paymentStatus = '04';
                $paymentMessage = 'Declined (Transaction has been expired)';
            } elseif ($order->payment_status == 'Cancel') {
                $paymentStatus = '05';
                $paymentMessage = 'Declined (Transaction has been canceled)';
            } else {
                $paymentStatus = '00';
                $paymentMessage = 'Success';


                if ($type == 'shop') {
                    $this->main->update('orders', array('payment_status' => 'Paid', 'payment_date' => date('Y-m-d H:i:s')), array('id' => $order->id));

                    $this->data['order'] = $order;
                    $this->data['customer'] = $this->main->get('customers', array('id' => $this->data['order']->customer));
                    $this->data['invoices'] = $this->main->gets('order_invoice', array('order' => $this->data['order']->id));

                    if ($data['invoices']) {
                        foreach ($this->data['invoices']->result() as $invoice) {
                            if ($invoice->shipping_courier == 'pickup') {
                                $due_date_inv = date('Y-m-d H:i:s', strtotime('+1 days'));
                            } else {
                                $due_date_inv = date('Y-m-d H:i:s', strtotime('+2 days'));
                            }

                            $this->main->update('order_invoice', array('order_status' => settings('order_payment_received_status'), 'due_date' => $due_date_inv), array('id' => $invoice->id));
                            $this->main->update('order_history', array('order' => $order->id, 'invoice' => $invoice->id, 'order_status' => settings('order_payment_received_status')));
                            $this->data['invoice'] = $invoice;
                            $this->data['merchant'] = $this->main->get('merchants', array('id' => $invoice->merchant));

                            // updating merchant stock
                            // if ($this->data['merchant']->type == 'merchant' || $this->data['merchant']->type == 'branch') {
                            //     $products = $this->main->gets('order_product', array('invoice' => $invoice->id));

                            //     if ($products) {
                            //         foreach ($products->result() as $product) {
                            //             $product_stock = $this->main->get('products_principal_stock', array('branch_id' => $invoice->merchant, 'product_id' => $product->product));
                            //             $this->main->update('products_principal_stock', array('quantity' => ($product_stock->quantity - $product->quantity)), array('id' => $product_stock->id));
                            //         }
                            //     }
                            // }

                            $merchant_user = $this->main->get('merchant_users', array('id' => $this->data['merchant']->auth));
                            $message = $this->load->view('email/merchant/new_order', $this->data, true);
                            $cronjob = array(
                                'from' => settings('send_email_from'),
                                'from_name' => settings('store_name'),
                                'to' => $merchant_user->email,
                                'subject' => 'Pesanan baru dari '. $this->data['customer']->fullname,
                                'message' => $message
                            );

                            $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));

                            if ($merchant_user->verification_phone) {
                                $sms = json_decode(settings('sprint_sms'), true);
                                $sms['m'] = 'Pesanan baru dari ' . $this->data['customer']->fullname . '. Segera proses sebelum ' . get_date_indo_full($invoice->due_date) . ' WIB.';
                                $sms['d'] = $merchant_user->phone;
                                $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                                 $this->main->insert('log_sms', array('content' => json_encode($sms), 'to' => $merchant_user->phone));
                            }
                        }
                    }
                    if($this->data['customer']->email){
                    $message = $this->load->view('email/transaction/payment_success', $this->data, true);
                    $cronjob = array(
                        'from' => settings('send_email_from'),
                        'from_name' => settings('store_name'),
                        'to' => $this->data['customer']->email,
                        'subject' => 'Pembayaran Sukses #'. $this->data['order']->code,
                        'message' => $message
                    );

                    $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));

                    if ($this->data['customer']->verification_phone) {
                        $sms = json_decode(settings('sprint_sms'), true);
                        if($this->data['order']->payment_method == 'transfer'){
                        $sms['m'] = 'Pembayaran sebesar '. rupiah($this->data['order']->total_plus_kode) . ' telah berhasil pada '. get_date_indo_full(date('Y-m-d H:i:s')) . ' WIB. Pesanan Anda akan diteruskan ke penjual.';
                         }else{
                              $sms['m'] = 'Pembayaran sebesar '. rupiah($this->data['order']->total) . ' telah berhasil pada '. get_date_indo_full(date('Y-m-d H:i:s')) . ' WIB. Pesanan Anda akan diteruskan ke penjual.';
                         }
                        $sms['d'] = $this->data['customer']->phone;
                        $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                         $this->main->insert('log_sms', array('content' => json_encode($sms), 'to' => $this->data['customer']->phone));
                    }
                } elseif ($type == 'cctv') {
                    $this->load->library('cctv_lib');
                    // Approved Transaction
                    $this->main->update('cctv_orders', array('payment_status' => 'Paid', 'payment_date' => date('Y-m-d H:i:s')), array('id' => $order->id));
                    $this->data['order'] = $order;
                    $this->data['invoices'] = $this->main->gets('cctv_order_invoice', array('order' => $this->data['order']->id));
                    if ($this->data['invoices'])
                        foreach ($this->data['invoices']->result() as $invoice) {
                            $due_date_inv = date('Y-m-d H:i:s', strtotime('+2 days'));
                            $this->main->update('cctv_order_invoice', array('order_status' => settings('order_payment_received_status'), 'due_date' => $due_date_inv), array('id' => $invoice->id));
                            $this->main->insert('cctv_order_history', array('order' => $order->id, 'invoice' => $invoice->id, 'order_status' => settings('order_payment_received_status')));
                    }
                    $array = array(
                        'req_time' => time(),
                        'cash_fee_type' => 'IDR',
                        'cash_fee' => (int)$this->data['order']->total,
                        'total_fee' => (int)$this->data['order']->total,
                        'trade_no' => $this->data['order']->code,
                        'trade_end' => time()+(60*60*24),
                        'trade_result' => 'SUCCESS',
                        'order_id' => $this->data['order']->code,
                        'order_info' => $this->data['order']->product_detail,
                    );
                    $this->main->insert('cctv_order_meta', array('order_id' => $order->id, 'meta_name' => 'cctv_request', 'meta_value' => json_encode($array)));
                    $setData = $this->cctv_lib->setData($array);
                    $this->main->insert('cctv_order_meta', array('order_id' => $order->id, 'meta_name' => 'cctv_response', 'meta_value' => json_encode($setData)));
                } elseif ($type == 'voucher') {
                    $this->load->library('tanaka_voucher');
                    // Approved Transaction
                    $this->main->update('app_voucher_orders', array('payment_status' => 'Paid', 'payment_date' => date('Y-m-d H:i:s')), array('id' => $order->id));
                    $this->data['order'] = $order;
                    $this->data['invoices'] = $this->main->gets('app_voucher_order_invoice', array('order' => $this->data['order']->id));
                    if ($this->data['invoices']) {
                        foreach ($this->data['invoices']->result() as $invoice) {
                            $due_date_inv = date('Y-m-d H:i:s', strtotime('+2 days'));
                            $this->main->update('app_voucher_order_invoice', array('order_status' => settings('order_payment_received_status'), 'due_date' => $due_date_inv), array('id' => $invoice->id));
                            $this->main->insert('app_voucher_order_history', array('order' => $order->id, 'invoice' => $invoice->id, 'order_status' => settings('order_payment_received_status')));
                        }
                    }
                    $product_detail = json_decode($order->product_detail);
                    $array = array(
                        'Produk' => $product_detail->Produk,
                        'Chipid' => $product_detail->Chipid,
                        'PartnerTrxID' => $product_detail->PartnerTrxID
                    );
                    $this->main->insert('app_voucher_order_meta', array('order_id' => $order->id, 'meta_name' => 'voucher_request', 'meta_value' => json_encode($product_detail)));
                    $activation = $this->tanaka_voucher->process('Trx', $array);
                    $this->main->insert('app_voucher_order_meta', array('order_id' => $order->id, 'meta_name' => 'voucher_response', 'meta_value' => json_encode($activation)));
                }
            }
        }

        echo json_encode([
            'channelId' => $data['channelId'],
            'currency' => 'IDR',
            'paymentStatus' => $paymentStatus,
            'paymentMessage' => $paymentMessage,
            'flagType' => '11',
            'paymentReffId' => $data['paymentReffId']
        ]);
    }

    public function pay() {
        $order = 61;
        $invoice = 34;
        $this->load->model('orders');
        $this->data['order'] = $this->orders->get($order);
        $this->data['invoice'] = $this->main->get('order_invoice', array('order' => $this->data['order']->id));
        $this->data['customer'] = $this->main->get('customers', array('id' => $this->data['order']->customer));
        $this->data['merchant'] = $this->main->get('merchants', array('id' => $this->data['invoice']->merchant));
        $merchant_user = $this->main->get('merchant_users', array('id' => $this->data['merchant']->auth));
        $message = $this->load->view('email/merchant/new_order', $this->data, true);
        $cronjob = array(
            'from' => settings('send_email_from'),
            'from_name' => settings('store_name'),
            'to' => $merchant_user->email,
            'subject' => 'Pesanan baru dari ' . $this->data['customer']->fullname,
            'message' => $message
        );
        $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
    }

}
