<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Xendit extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('orders');
        $this->load->model('order_meta');
    }

    public function index() {
        $data = file_get_contents("php://input");
        $data = json_decode($data);

        if (is_null($data)) {
            echo 'Error. Data return NULL';
            exit;
        }

        $invoice = $data->external_id;
        $first_char = substr($data->external_id, 0, 1);

        if ($first_char == 'C') {
            $type = 'cctv';
            $order = $this->main->get('cctv_orders', array('code' => $invoice));
        } elseif ($first_char == 'V') {
            $type = 'voucher';
            $order = $this->main->get('app_voucher_orders', array('code' => $invoice));
        } else {
            $type = 'shop';
            $order = $this->main->get('orders', array('code' => $invoice));
        }

        if ($order) {
            if (strtoupper($data->status) == 'PAID') {
                if ($type == 'shop') {
                    $this->main_update('orders', array('payment_status' => 'Paid', 'payment_to' => 'Xendit', 'payment_date' => date('Y-m-d'), 'payment_total' => $data->amount), array('id' => $order->id));

                    $this->data['order'] = $this->orders->get($order->id);
                    $this->data['customer'] = $this->main->get('customers', array('id' => $this->data['order']->customer));
                    $this->data['invoices'] = $this->main->gets('order_invoice', array('order' => $this->data['order']->id));

                    if ($this->data['invoices']) {
                        foreach ($this->data['invoices']->result() as $invoice) {
                            if ($invoice->shipping_courier == 'pickup') {
                                $due_date_inv = date('Y-m-d H:i:s', strtotime('+1 days'));
                            } else {
                                $due_date_inv = date('Y-m-d H:i:s', strtotime('+2 days'));
                            }

                            $this->main->update('order_invoice', array('order_status' => settings('order_payment_received_status'), 'due_date' => $due_date_inv), array('id' => $invoice->id));
                            $this->main->insert('order_hstory', array('order' => $order->id, 'invoice' => $invoice->id, 'order_status' => settings('order_payment_received_status')));

                            $this->data['invoice'] = $invoice;
                            $this->data['merchant'] = $this->main->get('merchants', array('id' => $invoice->merchant));

                            // updating merchant stock
                            // if ($this->data['merchant']->type == 'merchant' || $this->data['merchant']->type == 'branch') {
                            //     $products = $this->main->gets('order_product', array('invoice' => $invoice->id));

                            //     if ($products) {
                            //         foreach ($products->result() as $product) {
                            //             $product_stock = $this->main->get('products_principal_stock', array('branch_id' => $invoice->merchant, 'product_id' => $product->product));
                            //             $this->main->update('products_principal_stock', array('quantity' => ($product_stock->quantity - $product->quantity)), array('id' => $product_stock->id));
                            //         }
                            //     }
                            // }

                            $merchant_user = $this->main->get('merchant_users', array('id' => $this->data['merchant']->auth));
                            $message = $this->load->view('email/merchant/new_order', $this->data, true);
                            $cronjob = array(
                                'from' => settings('send_email_from'),
                                'from_name' => settings('store_name'),
                                'to' => $merchant_user->email,
                                'subject' => 'Pesanan baru dari '. $this->data['customer']->fullname,
                                'message' => $message
                            );

                            $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));

                            if ($merchant_user->verification_phone) {
                                $sms = json_decode(settings('sprint_sms'), true);
                                $sms['m'] = 'Pesanan baru dari '. $customer->fullname . '. Segera proses sebelum '. get_date_indo_full($invoice->due_date) . ' WIB.';
                                $sms['d'] = $merchant_user->phone;
                                $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                                 $this->main->insert('log_sms', array('content' => json_encode($sms), 'to' => $merchant_user->phone));
                            }
                        }
                    }
                    if($this->data['customer']->email){
                    $message = $this->load->view('email/transaction/payment_success', $this->data, true);
                    $cronjob = array(
                        'from' => settings('send_email_from'),
                        'from_name' => settings('store_name'),
                        'to' => $this->data['customer']->email,
                        'subject' => 'Pembayaran Sukses #' . $this->data['order']->code,
                        'message' => $message
                    );

                    $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
                    }
                    if ($this->data['customer']->verification_phone) {
                        $sms = json_decode(settings('sprint_sms'), true);
                        if($this->data['order']->payment_method == 'transfer'){
                             $sms['m'] = 'Pembayaran sebesar ' . rupiah($this->data['order']->total_plus_kode) . ' telah berhasil pada ' . get_date_indo_full(date('Y-m-d H:i:s')) . ' WIB. Pesanan Anda akan diteruskan ke penjual.';
                        }else{
                            $sms['m'] = 'Pembayaran sebesar ' . rupiah($this->data['order']->total) . ' telah berhasil pada ' . get_date_indo_full(date('Y-m-d H:i:s')) . ' WIB. Pesanan Anda akan diteruskan ke penjual.';
                        }
                        $sms['d'] = $this->data['customer']->phone;
                        $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                         $this->main->insert('log_sms', array('content' => json_encode($sms), 'to' => $this->data['customer']->phone));
                    }
                } elseif ($type == 'cctv') {
                    $this->main->update('cctv_orders', array('payment_status' => 'Paid', 'payment_to' => 'Xendit', 'payment_date' => date('Y-m-d'), 'payment_total' => $data->amount), array('id' => $order->id));
                    $this->data['order'] = $this->orders->get_cctv($order->id);
                    $this->data['invoices'] = $this->main->gets('cctv_order_invoice', array('order' => $this->data['order']->id));
    
                    if ($this->data['invoices']) {
                        foreach ($this->data['invoices']->result() as $invoice) {
                            $due_date_inv = date('Y-m-d H:i:s', strtotime('+2 days'));
                            $this->main->update('cctv_order_invoice', array('order_status' => settings('order_payment_received_status'), 'due_date' => $due_date_inv), array('id' => $invoice->id));
                            $this->main->insert('cctv_order_history', array('order' => $order->id, 'invoice' => $invoice->id, 'order_status' => settings('order_payment_received_status')));
                            $this->data['invoice'] = $invoice;
                            //$this->data['merchant'] = $this->main->get('merchants', array('id' => $invoice->merchant));
                        }
                    }    
                } elseif ($type == 'voucher') {
                    $data_order = array(
                        'payment_status' => 'Paid',
                        'payment_to' => 'Xendit',
                        'payment_date' => date('Y-m-d'),
                        'payment_total' => $data->amount
                    );
                    $this->main->update('app_voucher_orders', $data_order, ['id' => $order->id]);
                    $this->data['order'] = $this->orders->get_voucher($order->id);
                    $this->data['invoices'] = $this->main->gets('app_voucher_order_invoice', array('order' => $this->data['order']->id));

                    if ($this->data['invoices']) {
                        foreach ($this->data['invoices']->result() as $invoice) {
                            $due_date_inv = date('Y-m-d H:i:s', strtotime('+2 days'));
                            $this->main->update('app_voucher_order_invoice', array('order_status' => settings('order_payment_received_status'), 'due_date' => $due_date_inv), array('id' => $invoice->id));
                            $this->main->insert('app_voucher_order_history', array('order' => $order->id, 'invoice' => $invoice->id, 'order_status' => settings('order_payment_received_status')));
                            $this->data['invoice'] = $invoice;
                        }
                    }
                }
            } else {

            }

            if ($type == 'shop') {
                $this->order_meta->insert(array('order_id' => $order->id, 'meta_name' => 'xendir_response_callback', 'meta_value' => json_encode($data)));
            }  elseif ($type ==  'cctv') {
                $this->main->insert('cctv_order_meta', array('order_id' => $order->id, 'meta_name' => 'xendit_response_callback', 'meta_value' => json_encode($data)));
                $array = array(
                    'req_time' => time(),
                    'cash_fee_type' => 'IDR',
                    'cash_fee' => (int)$this->data['order']->total,
                    'total_fee' => (int)$this->data['order']->total,
                    'trade_no' => $this->data['order']->code,
                    'trade_end' => time()+(60*60*24),
                    'trade_result' => 'SUCCESS',
                    'order_id' => $this->data['order']->code,
                    'order_info' => $this->data['order']->product_detail,
                );
                $this->main->insert('cctv_order_meta', array('order' => $order->id, 'meta_name' => 'cctv_request', 'meta_value' => json_encode($array)));
                $setData = $this->cctv_lib->setData($array);
                $this->main->insert('cctv_order_meta', array('order' => $order->id, 'meta_name' => 'cctv_response', 'meta_value' => json_encode($setData)));
            } elseif ($type == 'voucher') {
                $this->load->library('tanaka_voucher');
                $data = json_decode($order->product_detail);
                $array = array(
                    'Produk' => $product_detail->Produk,
                    'Chipid' => $product_detail->Chipid,
                    'PartnerTrxID' => $product_detail->PartnerTrxID
                );

                $this->main->insert('app_voucher_order_meta', array('order_id' => $order->id, 'meta_name' => 'voucher_request', 'meta_value' => json_encode($data)));
                $activation = $this->tanaka_voucher->process('Trx', $array);
                $this->main->insert('app_voucher_order_meta', array('order_id' => $order->id, 'meta_name' => 'voucher_response', 'meta_value' => $activation));
            }
        }

        /*$order = $this->main->get('orders', ['code' => $invoice]);
        if ($order) {
            if (strtoupper($data->status) == 'PAID') {
                $this->main->update('orders', [
                    'payment_status' => 'Paid',
                    'payment_to' => 'Xendit',
                    'payment_date' => date('Y-m-d'),
                    'payment_total' => $data->amount
                        ], ['id' => $order->id]);

                //$this->main->update('order_invoice', array('order_status' => settings('order_payment_received_status')), array('order' => $order->id));
                $this->data['order'] = $this->orders->get($order->id);
                $this->data['customer'] = $this->main->get('customers', array('id' => $this->data['order']->customer));
                $this->data['invoices'] = $this->main->gets('order_invoice', array('order' => $this->data['order']->id));

                if ($this->data['invoices']) {
                    foreach ($this->data['invoices']->result() as $invoice) {
                        if($invoice->shipping_courier == 'pickup') {
                            $due_date_inv = date('Y-m-d H:i:s', strtotime('+1 days'));
                        } else {
                            $due_date_inv = date('Y-m-d H:i:s', strtotime('+2 days'));
                        }
                        $this->main->update('order_invoice', array('order_status' => settings('order_payment_received_status'), 'due_date' => $due_date_inv), array('id' => $invoice->id));
                        $this->main->insert('order_history', array('order' => $order->id, 'invoice' => $invoice->id, 'order_status' => settings('order_payment_received_status')));
                        $this->data['invoice'] = $invoice;
                        $this->data['merchant'] = $this->main->get('merchants', array('id' => $invoice->merchant));

                        //update stock merchant
                        if ($this->data['merchant']->type == 'merchant' || $this->data['merchant']->type == 'branch') {
                            $products = $this->main->gets('order_product', array('invoice' => $invoice->id));
                            if ($products) {
                                foreach ($products->result() as $product) {
                                    $product_detail = $this->db->query("select type, package_items from products where id = $product->product ");
                                    $row = $product_detail->row();
                                    if($row->type == 'p') {
                                        $package_items = json_decode($row->package_items, true);
                                        foreach ($package_items as $package) {
                                            $this->db->query("UPDATE product_merchant SET quantity = (quantity - $product->quantity) WHERE product = ".$package['product_id']." AND merchant = " . $this->data['merchant']->id); 
                                        }
                                    } else {
                                        $this->db->query("UPDATE product_merchant SET quantity = (quantity - $product->quantity) WHERE product = $product->product AND merchant = " . $this->data['merchant']->id);
                                    }
                                }
                            }
                        }

                        $merchant_user = $this->main->get('merchant_users', array('id' => $this->data['merchant']->auth));
                        $message = $this->load->view('email/merchant/new_order', $this->data, true);
                        $cronjob = array(
                            'from' => settings('send_email_from'),
                            'from_name' => settings('store_name'),
                            'to' => $merchant_user->email,
                            'subject' => 'Pesanan baru dari ' . $this->data['customer']->fullname,
                            'message' => $message
                        );
                        $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
                        if ($merchant_user->verification_phone) {
                            $sms = json_decode(settings('sprint_sms'), true);
                            $sms['m'] = 'Pesanan baru dari ' . $customer->fullname . '. Segera proses sebelum ' . get_date_indo_full($invoice->due_date) . ' WIB.';
                            $sms['d'] = $merchant_user->phone;
                            $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                        }
                    }
                }

                $message = $this->load->view('email/transaction/payment_success', $this->data, true);
                $cronjob = array(
                    'from' => settings('send_email_from'),
                    'from_name' => settings('store_name'),
                    'to' => $this->data['customer']->email,
                    'subject' => 'Pembayaran Sukses #' . $this->data['order']->code,
                    'message' => $message
                );
                $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
                if ($this->data['customer']->verification_phone) {
                    $sms = json_decode(settings('sprint_sms'), true);
                    $sms['m'] = 'Pembayaran sebesar ' . rupiah($this->data['order']->total) . ' telah berhasil pada ' . get_date_indo_full(date('Y-m-d H:i:s')) . ' WIB. Pesanan Anda akan diteruskan ke penjual.';
                    $sms['d'] = $this->data['customer']->phone;
                    $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                }
            } else {
                //    $this->main->update('orders',[
                //        'payment_status' => 'Failed',
                //        'payment_date' => date('Y-m-d')
                //            ], ['id' => $order->id]);
            }

            $this->order_meta->insert([
                'order_id' => $order->id,
                'meta_name' => 'xendit_response_callback',
                'meta_value' => json_encode($data)
            ]);
        }*/
    }

    public function send_email() {
        $this->load->library('email');
        $config['mailtype'] = 'html';
        $config['wordwrap'] = FALSE;
        $config['priority'] = 1;
        $config['protocol'] = 'sendmail';
        $this->email->initialize($config);
        $this->email->from('noreply@tokomanamana.com', 'TokoManaMana.com');
        $this->email->to('sandi4code@gmail.com');
        $this->email->subject('Menunggu Pembayaran Invoice ');
        $this->email->message('helooooo haaaiiii');
        $this->email->send();
        echo $this->email->print_debugger();
    }

}
