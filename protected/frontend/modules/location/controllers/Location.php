<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Location extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function set() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $this->session->set_userdata('location', $data);
        $location = $data;
        $merchant = $this->db->query("SELECT id, `name`, `group`, (6971 * acos(cos(radians($location[lat])) * cos(radians(lat)) * cos(radians(lng) - radians($location[lng])) + sin(radians($location[lat])) * sin(radians(lat)))) AS distance FROM merchants HAVING distance < 80 ORDER BY distance");
        if ($merchant->num_rows() > 0) {
            $this->session->set_userdata('list_merchant', $merchant->result_array());
        } else {
            $merchant = $this->db->query("SELECT id, `name`, `group`, (6971 * acos(cos(radians($location[lat])) * cos(radians(lat)) * cos(radians(lng) - radians($location[lng])) + sin(radians($location[lat])) * sin(radians(lat)))) AS distance FROM merchants ORDER BY distance LIMIT 5");
            $this->session->set_userdata('list_merchant', $merchant->result_array());
        }
        if ($this->cart->contents()) {
            $this->load->model('catalog/product_model', 'product');
            $merchant = $this->session->userdata('list_merchant')[0];
            foreach ($this->cart->contents() as $item) {
                $product = $this->product->get_product($item['id']);
                if ($product) {
                    $item['price'] = $product->price;
                    if ($product->merchant == 0) {
                        $price_level = $this->product->price_in_level($product->id, $merchant['group'], $item['qty']);
                        if ($price_level) { //jika price level ada, maka produk tsb masuk ke harga grosir
                            $item['price'] = $price_level;
                            $item['price_level'] = 1;
                        } else {
                            $price_group = $this->main->get('product_price', array('product' => $product->id, 'merchant_group' => $merchant['group']));
                            if ($price_group) {
                                $item['price'] = $price_group->price;
                            }
                        }
                    }
                    if ($item['options']) {
                        foreach ($item['options'] as $option) {
                            $item['price'] += $option['price'];
                        }
                    }
                    $this->cart->update($item);
                } else {
                    $this->cart->remove($item['rowid']);
                }
            }
        }
    }

    public function modal_location() {
        $this->load->library('googlemaps');
        $config['loadAsynchronously'] = TRUE;
        $config['map_height'] = '300px';
        if ($this->session->has_userdata('location')) {
            $config['center'] = $this->session->userdata('location')['lat'] . ',' . $this->session->userdata('location')['lng'];
        } else {
            $config['center'] = '-6.186486399999999,106.83409110000002';
//            $config['center'] = 'auto';
        }
        $config['onboundschanged'] = 'if (!centreGot) {
                var mapCentre = map.getCenter();
                marker_0.setOptions({
                        position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng()) 
                });
                save_lat_lng(mapCentre.lat(), mapCentre.lng());
                save_lat_lng_new(mapCentre.lat(), mapCentre.lng());
            }
            centreGot = true;';
        $config['disableFullscreenControl'] = TRUE;
        $config['disableMapTypeControl'] = TRUE;
        $config['disableStreetViewControl'] = TRUE;
        $config['places'] = TRUE;
        $config['placesAutocompleteInputID'] = 'search-location';
        $config['placesAutocompleteBoundsMap'] = TRUE; // set results biased towards the maps viewport
        $config['placesAutocompleteOnChange'] = 'map.setCenter(this.getPlace().geometry.location); 
            marker_0.setOptions({
                        position: new google.maps.LatLng(this.getPlace().geometry.location.lat(), this.getPlace().geometry.location.lng()) 
                });
            save_lat_lng(this.getPlace().geometry.location.lat(), this.getPlace().geometry.location.lng());
            save_lat_lng_new(this.getPlace().geometry.location.lat(), this.getPlace().geometry.location.lng());';
        $this->googlemaps->initialize($config);

        $marker = array();
        $marker['draggable'] = true;
        // $marker['ondragend'] = 'save_lat_lng(event.latLng.lat(), event.latLng.lng());';
        $marker['ondragend'] = 'save_lat_lng_new(event.latLng.lat(), event.latLng.lng());';
        $this->googlemaps->add_marker($marker);
        $this->data['map'] = $this->googlemaps->create_map();

        $this->load->view('modal_location', $this->data);
    }

}
