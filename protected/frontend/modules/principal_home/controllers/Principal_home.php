<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Principal_home extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->lang->load('catalog', settings('language'));
        $this->load->model('Principal_home_model', 'principal_home');
        $this->load->library('pagination');
        // $this->load->library('breadcrumb');
    }

    public function view($id) {
        $this->load->library('user_agent');
        $principal_home = $this->principal_home->get_principal_homepage($id);
        if (!$principal_home)
            redirect('error_404');

        $this->data['data'] = $principal_home;
        if($principal_home->province && $principal_home->city) {
            $this->data['data']->province_name = $this->main->get('provincies', ['id' => $principal_home->province])->name;
            $this->data['data']->city_type = $this->main->get('cities', ['id' => $principal_home->city])->type;
            $this->data['data']->city_name = $this->main->get('cities', ['id' => $principal_home->city])->name;
            $this->data['data']->district_name = $this->main->get('districts', ['id' => $principal_home->district])->name;
        }
        $this->data['brand'] = $this->main->get('brands', ['principle_id' => $principal_home->id]);
        // $ekspedisi = $this->principal_home->get_ekspedisi($id);
        // $this->data['ekspedisi'] = json_decode($ekspedisi->shipping);
        $this->data['rating'] = $this->principal_home->get_rating($id);
        $this->data['rating']->rating = round($this->data['rating']->rating,1);
        $this->data['rating_modal'] = $this->principal_home->get_rating($id);
        $this->data['rating_modal']->rating = round($this->data['rating_modal']->rating,1);
        // $this->data['total_transaction'] = $this->principal_home->get_total_transaction($id);

        //rating speed per rating
        $this->data['speed_rating5'] = $this->principal_home->count_row_speed($id,5);
        $this->data['speed_rating4'] = $this->principal_home->count_row_speed($id,4);
        $this->data['speed_rating3'] = $this->principal_home->count_row_speed($id,3);
        $this->data['speed_rating2'] = $this->principal_home->count_row_speed($id,2);
        $this->data['speed_rating1'] = $this->principal_home->count_row_speed($id,1);
        //rating service per rating
        $this->data['service_rating5'] = $this->principal_home->count_row_service($id,5);
        $this->data['service_rating4'] = $this->principal_home->count_row_service($id,4);
        $this->data['service_rating3'] = $this->principal_home->count_row_service($id,3);
        $this->data['service_rating2'] = $this->principal_home->count_row_service($id,2);
        $this->data['service_rating1'] = $this->principal_home->count_row_service($id,1);
        //rating accuracy per rating
        $this->data['accuracy_rating5'] = $this->principal_home->count_row_accuracy($id,5);
        $this->data['accuracy_rating4'] = $this->principal_home->count_row_accuracy($id,4);
        $this->data['accuracy_rating3'] = $this->principal_home->count_row_accuracy($id,3);
        $this->data['accuracy_rating2'] = $this->principal_home->count_row_accuracy($id,2);
        $this->data['accuracy_rating1'] = $this->principal_home->count_row_accuracy($id,1);

        // $start = ($this->input->get('page')) ? ($this->input->get('page') - 1) * 20 : 0;
        $start = 0;
        $this->data['products'] = $this->principal_home->get_products($id,($this->input->get('sort')) ? $this->input->get('sort') : 'popular',20,$start,'',NULL);
        $this->data['start'] = $start;
        $this->data['total_etalase'] = $this->principal_home->get_products($id, '', '', '','','')->num_rows();
        $this->data['no_etalase'] = $this->principal_home->get_no_etalase($id);
        $this->data['etalase_label'] = 'Semua Etalase';
        // $this->data['meta_description'] = ($principal_home->meta_description) ? $principal_home->meta_description : $principal_home->product_name . ', ' . settings('meta_title');
        $this->data['meta_description'] = $principal_home->meta_description . ', ' . settings('meta_title');
        $this->data['meta_keyword'] = ($principal_home->meta_keyword) ? $principal_home->meta_keyword : settings('meta_keyword');

        if($this->data['total_etalase'] > 20){
            //pagination
            // $config['base_url'] = current_url();
            // $config['total_rows'] = $this->data['total_etalase'];
            // $config['per_page'] = 20;
            // $this->pagination->initialize($config);

            // $this->data['pagination'] = $this->pagination->create_links();
            $this->data['next_button'] = true;
        }
        else{
            $this->data['next_button'] = false;
        }
        $this->data['data']->total_product = $this->data['total_etalase'];
        $this->output->set_title($principal_home->name  . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->js('assets/frontend/js/modules/principal_home.js');
        $this->load->js('assets/frontend/js/jssocials.min.js');
        $this->load->view('principal_home/view', $this->data);
    }

    public function ajax_filter(){
        $id = $this->input->get('m');
        $start = $this->input->get('start');
        if(empty($start) || $this->input->get('etalase-choice')){
            $start=0;
        }
        $start_end = $start + 20;
        $word = htmlspecialchars($this->input->get('search-specific-result'));
        $principal_home = $this->principal_home->get_principal_homepage($id);
        $this->data['data'] = $principal_home;
        $this->data['start'] = $start;
        $this->data['products'] = $this->principal_home->get_products($id,($this->input->get('sort')) ? $this->input->get('sort') : 'popular',20,$start,$word,$this->input->get('etalase-choice'));
        $count_products = $this->principal_home->get_products($id,($this->input->get('sort')) ? $this->input->get('sort') : 'popular','',0,$word,$this->input->get('etalase-choice'))->num_rows();
        $count_products_perpage = $this->principal_home->get_products($id,($this->input->get('sort')) ? $this->input->get('sort') : 'popular',20,$start,$word,$this->input->get('etalase-choice'))->num_rows();
        $this->data['total_etalase'] = $this->principal_home->get_products($id, '', '', '','','')->num_rows();
        $this->data['no_etalase'] = $this->principal_home->get_no_etalase($id);
        $this->data['etalase_label'] = ($this->input->get('etalase-label')) ? $this->input->get('etalase-label') : 'Semua Etalase';

        if($start - $count_products_perpage >=0){
            
            $this->data['prev_button'] = true;
        }
        else{
            $this->data['prev_button'] = false;
        }

        if($count_products_perpage < 20 || $count_products == $start_end  ){
            $this->data['next_button'] = false;
        }
        else{
            $this->data['next_button'] = true;
        }

        $this->load->view('principal_home/view_ajax', $this->data);
    }

}

/* End of file Principal_home.php */
/* Location: .//Applications/XAMPP/xamppfiles/htdocs/tokomanamana/protected/frontend/modules/principal_home/controllers/Principal_home.php */