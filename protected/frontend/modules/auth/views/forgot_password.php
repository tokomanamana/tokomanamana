<style>
    input{border-radius:3px}input:focus{border-color:#97c23c}input[type=submit]{border-radius:3px;background:#97c23c;border:1px solid #97c23c;transition:.3s}input[type=submit]:hover{background:#85a839;border:1px solid #85a839;color:#fff}#login-email a{transition:.3s}#login-email a:hover{color:#97c23c}#reset_password_form input,#reset_password_form select{background-image:none;background-color:transparent;-webkit-box-shadow:none;-moz-box-shadow:none;box-shadow:none;border:1px solid #ccc;border-radius:3px}#reset_password_form input:focus,#reset_password_form select:focus{border-color:#a7c22a;background-color:none!important;background-image:none}#reset_password_form .text_register{margin-top:10px;margin-bottom:10px}#reset_password_form .text_other{margin-top:10px}#reset_password_form a{color:#a7c22a}#reset_password_form a:hover{color:#e75a5f}#reset_password_form .action{margin-bottom:-20px}#reset_password_form .action button{width:auto;margin:10px 0}#reset_password_form .action button:focus,#reset_password_form .action button:focus i{outline:0;color:#fff}#reset_password_form .action button.btn_login{border-radius:3px;background:#a7c22a;border:1px solid #a7c22a;text-transform:capitalize;font-weight:700;font-size:14px}#reset_password_form .action button.btn_login:hover{color:#fff!important;background:#8ea623;border:1px solid #8ea623}#reset_password_form .action button.btn_register_facebook{background:#3b5998;border:1px solid #3b5998;border-radius:3px;text-transform:capitalize;font-weight:700;font-size:14px}#reset_password_form .action button.btn_register_facebook:hover{background:#2c4170!important;border:1px solid #2c4170!important;color:#fff}#reset_password_form .action button.btn_register_facebook:hover i{color:#fff!important}#reset_password_form .action button.btn_register_google{color:#444;background:#ccc;border:1px solid #ccc;border-radius:3px;text-transform:capitalize;font-weight:700;font-size:14px}#reset_password_form .action button.btn_register_google:hover{background:#b3b3b3;border:1px solid #b3b3b3}#reset_password_form .action button.btn_register_google i{color:#444}#reset_password_form .action .col-md-3{padding-left:0}#reset_password_form button#button_login_verification{height:34px;width:100%;border:1px solid #ccc;background:#ccc;border-radius:3px;font-weight:700;font-size:12px;color:#444}#reset_password_form button#button_login_verification:focus{outline:0}#reset_password_form button#button_login_verification:hover{background:#b3b3b3;border:1px solid #b3b3b3}#reset_password_form .error_validation_login .alert,#reset_password_form .send_otp_success .alert{border-radius:3px}#reset_password_form input:active,#reset_password_form input:focus,#reset_password_form select:active,#reset_password_form select:focus{border:1px solid #a7c22a!important}#reset_password_form .error_validation span{color:#d9534f;margin-top:-15px}.lds-ring{display:inline-block;position:relative;width:100%;height:100%;padding:9px 2px}.lds-ring div{box-sizing:border-box;display:inline-block;position:absolute;width:20px;height:20px;border:3px solid #000;border-radius:50%;animation:lds-ring 1.2s cubic-bezier(.5,0,.5,1) infinite;border-color:#000 transparent transparent transparent}.lds-ring div:nth-child(1){animation-delay:-.45s}.lds-ring div:nth-child(2){animation-delay:-.3s}.lds-ring div:nth-child(3){animation-delay:-.15s}@keyframes lds-ring{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}#reset_password_form .alert{border-radius:5px}#choose_validate .modal-content{border-radius:5px;border:none}#choose_validate .btn_choose_validate{width:100%;background:0 0;margin-bottom:20px;padding:20px;border:1px solid #ddd;border-radius:5px;font-size:14px;font-weight:400;box-shadow:0 0 5px rgba(0,0,0,.175);text-align:left}#choose_validate .btn_choose_validate:focus{outline:0;border:1px solid #ddd}#validate_phone input{background-image:none;background-color:transparent;-webkit-box-shadow:none;-moz-box-shadow:none;box-shadow:none;border:1px solid #ccc;border-radius:3px}#validate_phone input:focus{border-color:#a7c22a;background-color:none!important;background-image:none}#validate_phone .verification_code_phone{border-radius:3px;background:#ccc;border:1px solid #ccc;text-transform:capitalize;font-weight:700;height:34px;width:100%}#validate_phone .verification_code_phone{outline:0;border:1px solid #ddd}#validate_phone .verification_code_phone:hover{background:#b3b3b3;border:1px solid #b3b3b3}#validate_phone .btn_cancel{background:#fff!important;border:1px solid #ccc!important;color:#888!important}#validate_phone .btn_cancel:hover{background-color:#eee!important}#validate_phone .modal-footer .btn{border-radius:3px;font-weight:400}#validate_phone .modal-footer .btn:focus{outline:0;border:none}#validate_phone .modal-footer .btn_next{background:#a7c22a;border:1px solid #a7c22a;color:#fff}#validate_phone .modal-footer .btn_next:hover{background:#8ea623;border:1px solid #8ea623}#validate_phone .modal-content{border-radius:5px;border:none}#validate_phone_body .alert{border-radius:5px}#choose_validate .alert{border-radius:5px}#change_password_by_phone input{background-image:none;background-color:transparent;-webkit-box-shadow:none;-moz-box-shadow:none;box-shadow:none;border:1px solid #ccc;border-radius:3px}#change_password_by_phone input:focus{border-color:#a7c22a;background-color:none!important;background-image:none}#change_password_by_phone .btn_cancel{background:#fff!important;border:1px solid #ccc!important;color:#888!important}#change_password_by_phone .btn_cancel:hover{background-color:#eee!important}#change_password_by_phone .modal-footer .btn{border-radius:3px;font-weight:400}#change_password_by_phone .modal-footer .btn:focus{outline:0;border:none}#change_password_by_phone .modal-footer .btn_next{background:#a7c22a;border:1px solid #a7c22a;color:#fff}#change_password_by_phone .modal-footer .btn_next:hover{background:#8ea623;border:1px solid #8ea623}#change_password_by_phone .modal-content{border-radius:5px;border:none}#change_password_by_phone_body .alert{border-radius:5px}
</style>
<section class="login-content" id="login">
    <div class="login-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="panel">
                    <div class="tabbable" style="margin-top: 25px;">
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active"><a href="#login-email" data-toggle="tab">Lupa Password</a></li>
                        </ul>

                        <form action="<?php echo site_url('auth/check_account_reset') ?>" style="margin-top: 25px;" id="reset_password_form">
                            <div class="row">
                                <div class="">
                                    <div class="row">
                                        <div class="error_validation_reset col-md-12" style="padding: 0px;">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 form-group" style="padding: 0px;">
                                            <label for="phone_reset_password">No Handphone / Email</label>
                                            <input type="text" class="form-control" name="phone_reset_password" id="phone_reset_password" placeholder="Masukkan No Handphone / Email">
                                            <div id="error_phone_reset_password" class="error_validation"></div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2" style="padding: 0px;">
                                    <?php if($this->agent->is_mobile()) : ?>
                                        <a href="<?php echo site_url('member/login'); ?>">Masuk</a>
                                    <?php else : ?>
                                        <a href="javascript:void(0)" onclick="$('#modal_login').modal('show')">Masuk</a>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <!-- <input type="hidden" name="back" value="<?php //echo site_url('') ?>"> -->
                            <div class="row action">
                                <?php if($this->agent->is_mobile()) : ?>
                                    <div class="col-sm-12" style="padding: 0px;">
                                        <button class="btn btn_login" type="submit" style="width: 100%;">Reset Password</button>
                                    </div>
                                <?php else : ?>
                                    <div class="col-sm-2" style="padding: 0px;">
                                        <button class="btn btn_login" type="submit">Reset Password</button>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- modal -->
<div id="choose_validate" class="modal" role="dialog" aria-hidden="true" tabindex="-1" data-width="800" style="display: none;">
    <div class="modal-dialog fadeIn animated modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid #eee;padding-bottom: 0px;">
                <center><h1><b>Pilih Metode Verifikasi</b></h1></center>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -37px;" onclick="close_choose_validate();">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="choose_validate_body">
                <div class="row">
                   <div class="col-md-12">
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="validate_phone" class="modal" role="dialog" aria-hidden="true" tabindex="-1" data-width="800" style="display: none;">
    <div class="modal-dialog fadeIn animated modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid #eee;padding-bottom: 0px;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="close_validate_phone();">
                    <span aria-hidden="true">&times;</span>
                </button>
                <center><h1><b>Reset Password</b></h1><span style="margin-top: -10px;display: inherit;margin-bottom: 15px;">Kami telah mengirim kode verifikasi ke No Handphone anda</span></center>
            </div>
            <div id="validate_phone_body">
                <div class="modal-body">
                    <div class="row">
                       <div class="col-md-12">
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="change_password_by_phone" class="modal" role="dialog" aria-hidden="true" tabindex="-1" data-width="800" style="display: none;">
    <div class="modal-dialog fadeIn animated modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid #eee;padding-bottom: 0px;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="close_change_password_by_phone();">
                    <span aria-hidden="true">&times;</span>
                </button>
                <center><h1><b>Ubah Password</b></h1><span style="margin-top: -10px;display: inherit;margin-bottom: 15px;"></span></center>
            </div>
            <div id="change_password_by_phone_body">
                <div class="modal-body">
                    <div class="row">
                       <div class="col-md-12">
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>