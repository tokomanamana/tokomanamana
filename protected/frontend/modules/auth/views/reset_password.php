<style>
    input[type=submit]{border-radius:3px;background:#97c23c;border:1px solid #97c23c;transition:.3s}input[type=submit]:hover{background:#85a839;border:1px solid #85a839;color:#fff}#login-email a{transition:.3s}#login-email a:hover{color:#97c23c}#reset_password_form input,#reset_password_form select{background-image:none;background-color:transparent;-webkit-box-shadow:none;-moz-box-shadow:none;box-shadow:none;border:1px solid #ccc;border-radius:3px}#reset_password_form input:focus,#reset_password_form select:focus{border-color:#a7c22a;background-color:none!important;background-image:none}#reset_password_form .text_register{margin-top:10px;margin-bottom:10px}#reset_password_form .text_other{margin-top:10px}#reset_password_form a{color:#a7c22a}#reset_password_form a:hover{color:#e75a5f}#reset_password_form .action{margin-bottom:-20px}#reset_password_form .action button{width:auto;margin:10px 0}#reset_password_form .action button:focus,#reset_password_form .action button:focus i{outline:0;color:#fff}#reset_password_form .action button.btn_login{border-radius:3px;background:#a7c22a;border:1px solid #a7c22a;text-transform:capitalize;font-weight:700;font-size:14px}#reset_password_form .action button.btn_login:hover{color:#fff!important;background:#8ea623;border:1px solid #8ea623}#reset_password_form .action button.btn_register_facebook{background:#3b5998;border:1px solid #3b5998;border-radius:3px;text-transform:capitalize;font-weight:700;font-size:14px}#reset_password_form .action button.btn_register_facebook:hover{background:#2c4170!important;border:1px solid #2c4170!important;color:#fff}#reset_password_form .action button.btn_register_facebook:hover i{color:#fff!important}#reset_password_form .action button.btn_register_google{color:#444;background:#ccc;border:1px solid #ccc;border-radius:3px;text-transform:capitalize;font-weight:700;font-size:14px}#reset_password_form .action button.btn_register_google:hover{background:#b3b3b3;border:1px solid #b3b3b3}#reset_password_form .action button.btn_register_google i{color:#444}#reset_password_form .action .col-md-3{padding-left:0}#reset_password_form button#button_login_verification{height:34px;width:100%;border:1px solid #ccc;background:#ccc;border-radius:3px;font-weight:700;font-size:12px;color:#444}#reset_password_form button#button_login_verification:focus{outline:0}#reset_password_form button#button_login_verification:hover{background:#b3b3b3;border:1px solid #b3b3b3}#reset_password_form .error_validation_login .alert,#reset_password_form .send_otp_success .alert{border-radius:3px}#reset_password_form input:active,#reset_password_form input:focus,#reset_password_form select:active,#reset_password_form select:focus{border:1px solid #a7c22a!important}#reset_password_form .error_validation span{color:#d9534f;margin-top:-15px}.lds-ring{display:inline-block;position:relative;width:100%;height:100%;padding:9px 2px}.lds-ring div{box-sizing:border-box;display:inline-block;position:absolute;width:20px;height:20px;border:3px solid #000;border-radius:50%;animation:lds-ring 1.2s cubic-bezier(.5,0,.5,1) infinite;border-color:#000 transparent transparent transparent}.lds-ring div:nth-child(1){animation-delay:-.45s}.lds-ring div:nth-child(2){animation-delay:-.3s}.lds-ring div:nth-child(3){animation-delay:-.15s}@keyframes lds-ring{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}#reset_password_form .alert{border-radius:5px}
</style>
<!-- <section class="collection-heading heading-content ">
    <div class="container">
        <div class="row">
            <div class="collection-wrapper">
                <h1 class="collection-title"><span>Reset Password</span></h1>
            </div>
        </div>
    </div>
</section> -->
<!-- <section class="login-content">
    <div class="login-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="login-content-inner">
                    <div id="customer-login">
                        <div id="login" class="">
                            <?php if ($message) { ?>
                                <div class="alert alert-<?php echo $status; ?>">
                                    <?php echo $message; ?>
                                </div>
                            <?php }else{ ?>
                            <form method="post" accept-charset="UTF-8">
                                <label class="label">Password Baru</label>
                                <input type="password" value="" name="password" required="" class="text">
                                <label class="label">Komfirmasi Password Baru</label>
                                <input type="password" value="" name="confirm_password" required="" class="text">
                                <div class="action_bottom">
                                    <input class="btn" type="submit" value="Ubah Password">
                                </div>
                            </form>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> -->
<section class="login-content" id="login">
    <div class="login-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="panel">
                    <div class="tabbable" style="margin-top: 25px;">
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active"><a href="#login-email" data-toggle="tab">Reset Password</a></li>
                        </ul>

                        <form action="" style="margin-top: 25px;" id="reset_password_form">
                            <div class="row">
                                <div class="">
                                    <div class="row">
                                        <div class="message_validation col-md-12" style="padding: 0px;">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 form-group" style="padding: 0px;">
                                            <label for="password">Password Baru</label>
                                            <input type="password" class="form-control" name="password" id="password">
                                            <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"
                                            style="float: right;
                                            margin-top:-42px;
                                            margin-right:10px;
                                            position: relative;
                                            z-index: 2;
                                            cursor: pointer;" onclick="password_reset_show(this, 'password')"></span>
                                            <div class="error_validation" id="error_password"></div>
                                        </div>
                                        <div class="col-sm-12 form-group" style="padding: 0px;">
                                            <label for="confirm_password">Konfirmasi Password Baru</label>
                                            <input type="password" class="form-control" name="confirm_password" id="confirm_password">
                                            <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"
                                            style="float: right;
                                            margin-top:-42px;
                                            margin-right:10px;
                                            position: relative;
                                            z-index: 2;
                                            cursor: pointer;" onclick="password_reset_show(this, 'confirm_password')"></span>
                                            <div class="error_validation" id="error_confirm_password"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <input type="hidden" name="back" value="<?php //echo site_url('') ?>"> -->
                            <div class="row action">
                                <?php if($this->agent->is_mobile()) : ?>
                                    <div class="col-sm-12" style="padding: 0px;">
                                        <button class="btn btn_login" type="submit" style="width: 100%;">Ubah Password</button>
                                    </div>
                                <?php else : ?>
                                    <div class="col-sm-2" style="padding: 0px;">
                                        <button class="btn btn_login" type="submit">Ubah Password</button>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="<?php echo site_url('assets/frontend/js/modules/reset_password.js') ?>"></script>