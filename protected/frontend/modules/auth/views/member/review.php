<style type="text/css">
    .star-1 img,.star-2 img,.star-3 img,.star-4 img,.star-5 img{-webkit-transition:width .5s,height .5s,-webkit-transform .5s;transition:width .5s,height .5s,transform .5s}.star-1 img:hover,.star-2 img:hover,.star-3 img:hover,.star-4 img:hover,.star-5 img:hover{-webkit-transform:rotate(-40deg);transform:rotate(-40deg)}.collection-title{font-size:18px!important;color:#a7c22a!important;font-weight:700!important}.alert{border-radius:5px;font-size:15px}
</style>
<div class="my-account" id="review">
    <section class="collection-heading heading-content ">
        <div class="container">
            <div class="row">
                <div class="col-sm-9 col-sm-push-3" style="padding-left: 0px; padding-right: 0px;">
                    <div class="collection-wrapper block" style="padding: 10px 15px;background: none;">
                        <h1 class="collection-title"><span><?php echo lang('account_review'); ?></span></h1>
                        <?php echo $breadcrumb; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container" style="margin-bottom: 30px;">
        <div class="row">
            <?php echo $this->load->view('navigation'); ?>
            <div class="collection-mainarea col-md-9 col-sm-8 col-xs-12">
                <?php if ($orders->num_rows() > 0) { ?>
                    <?php foreach ($orders->result() as $order) { ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-sm-8 col-xs-12">
                                        <span class="order-header-code"><?php echo $order->code ?></span>
                                        <span class="text-muted"> <?php echo get_date($order->date_added, 'full'); ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body review" style="padding-bottom: 0;">
                                <?php
                                $invoice = explode(',', $order->invoice);
                                $products = $this->member->get_products_review($order->id, $user->id, $invoice);
                                if ($products->num_rows() > 0) {
                                    foreach ($products->result() as $product) {
                                        ?>
                                        <div id="review-<?php echo $product->id; ?>" class="row" style="background-color: #f5f5f5; margin-left: 0; margin-right: 0; padding: 10px; margin-bottom: 15px;">
                                            <div class="col-sm-5 col-xs-12">
                                                <a class="product-image" href="<?php echo seo_url('catalog/products/view/' . $product->product . '/' . $product->merchant); ?>" target="_blank">
                                                    <img src="<?php echo get_image($product->image); ?>">
                                                </a>
                                                <div style="float: left; width: 80%;">
                                                    <a href="<?php echo seo_url('catalog/products/view/' . $product->product . '/' . $product->merchant); ?>" target="_blank"><?php echo $product->name; ?></a>
                                                </div>
                                            </div>
                                            <div class="col-sm-7 col-xs-12">
                                                <?php if ($product->rating_speed && $product->rating_service && $product->rating_accuracy) { ?>
                                                    <div class="row">
                                                        <div class="col-sm-3">Kecepatan</div>
                                                        <div class="col-sm-9">
                                                            <span class="spr-starratings spr-review-header-starratings">
                                                                <?php for ($i = 1; $i <= $product->rating_speed; $i++) { ?>
                                                                    <i class="spr-icon spr-icon-star"></i>
                                                                <?php } ?>
                                                                <?php for ($i = $product->rating_speed + 1; $i <= 5; $i++) { ?>
                                                                    <i class="spr-icon spr-icon-star-empty"></i>
                                                                <?php } ?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-3">Pelayanan</div>
                                                        <div class="col-sm-9">
                                                            <span class="spr-starratings spr-review-header-starratings">
                                                                <?php for ($i = 1; $i <= $product->rating_service; $i++) { ?>
                                                                    <i class="spr-icon spr-icon-star"></i>
                                                                <?php } ?>
                                                                <?php for ($i = $product->rating_service + 1; $i <= 5; $i++) { ?>
                                                                    <i class="spr-icon spr-icon-star-empty"></i>
                                                                <?php } ?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-3">Akurasi</div>
                                                        <div class="col-sm-9">
                                                            <span class="spr-starratings spr-review-header-starratings">
                                                                <?php for ($i = 1; $i <= $product->rating_accuracy; $i++) { ?>
                                                                    <i class="spr-icon spr-icon-star"></i>
                                                                <?php } ?>
                                                                <?php for ($i = $product->rating_accuracy + 1; $i <= 5; $i++) { ?>
                                                                    <i class="spr-icon spr-icon-star-empty"></i>
                                                                <?php } ?>
                                                            </span>
                                                        </div>
                                                    </div>

                                                    <div class="spr-review-content">
                                                        <p class="spr-review-content-body"><?php echo $product->description; ?></p>
                                                    </div>
                                                <?php } else { ?>
                                                    <button class="btn btn-primary" onclick="add('<?php echo $product->id; ?>')">Tulis Ulasan</button>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                    <div style="display: block; text-align: center;">
                        <div class="pagi-bar">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                <?php } else {
                    ?>
                        <div class="<?php echo ($this->agent->is_mobile()) ? 'col-sm-12 col-md-12' : ''  ?>">
                            <div class="alert alert-warning">
                                <center><b>Tidak ada ulasan.</b></center>
                            </div>
                        </div>
                    <?php
                } ?>
            </div>
        </div>
    </div>
</div>
<div id="modal-review" class="modal" role="dialog" aria-hidden="true" tabindex="-1" data-width="800" style="display: none;">
    <div class="modal-dialog fadeIn animated">
        <div class="modal-content">
            <form action="#">
                <input type="hidden" name="id">
                <div class="modal-header">
                    <h1>Tulis Ulasan Anda</h1>
                </div>
                <div class="modal-body">
                    <div id="message"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="rating">
                                <input class="star star-1" id="star-speed-1" onclick="kecepatan(1)" type="radio" name="rating_speed" value="1"/>
                                <label class="star-1" id="icon-satu" for="star-speed-1"><img style="width:50px" src="<?php echo base_url('assets/frontend/images/smiley_blank.png'); ?>"></label>
                                <input class="star star-2" id="star-speed-2" onclick="kecepatan(2)" type="radio" name="rating_speed" value="2"/>
                                <label class="star-2" id="icon-dua" for="star-speed-2"><img style="width:50px" src="<?php echo base_url('assets/frontend/images/smiley_blank.png'); ?>"></label>
                                <input class="star star-3" id="star-speed-3" onclick="kecepatan(3)" type="radio" name="rating_speed" value="3"/>
                                <label class="star-3" id="icon-tiga" for="star-speed-3"><img style="width:50px" src="<?php echo base_url('assets/frontend/images/smiley_blank.png'); ?>"></label>
                                <input class="star star-4" id="star-speed-4" onclick="kecepatan(4)" type="radio" name="rating_speed" value="4"/>
                                <label class="star-4" id="icon-empat" for="star-speed-4"><img style="width:50px" src="<?php echo base_url('assets/frontend/images/smiley_blank.png'); ?>"></label>
                                <input class="star star-5" id="star-speed-5" onclick="kecepatan(5)" type="radio" name="rating_speed" value="5"/>
                                <label class="star-5" id="icon-lima" for="star-speed-5"><img style="width:50px" src="<?php echo base_url('assets/frontend/images/smiley_blank.png'); ?>"></label>
                            </div>
                            <p style="display: inline-block;
                            vertical-align: top;
                            position: relative;
                            left: 10px;
                            margin-top: 15px;"><b style="font-weight: bold;">Kecapatan</b><br><span id="star-speed-amount">0</span> bintang</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="rating">
                                <input class="star star-1" id="star-service-1" onclick="pelayanan(1)" type="radio" name="rating_service" value="1"/>
                                <label class="star-1" id="layan-satu" for="star-service-1"><img style="width:50px" src="<?php echo base_url('assets/frontend/images/smiley_blank.png'); ?>"></label>
                                <input class="star star-2" id="star-service-2" onclick="pelayanan(2)" type="radio" name="rating_service" value="2"/>
                                <label class="star-2" id="layan-dua" for="star-service-2"><img style="width:50px" src="<?php echo base_url('assets/frontend/images/smiley_blank.png'); ?>"></label>
                                <input class="star star-3" id="star-service-3" onclick="pelayanan(3)" type="radio" name="rating_service" value="3"/>
                                <label class="star-3" id="layan-tiga" for="star-service-3"><img style="width:50px" src="<?php echo base_url('assets/frontend/images/smiley_blank.png'); ?>"></label>
                                <input class="star star-4" id="star-service-4" onclick="pelayanan(4)" type="radio" name="rating_service" value="4"/>
                                <label class="star-4" id="layan-empat" for="star-service-4"><img style="width:50px" src="<?php echo base_url('assets/frontend/images/smiley_blank.png'); ?>"></label>
                                <input class="star star-5" id="star-service-5" onclick="pelayanan(5)" type="radio" name="rating_service" value="5"/>
                                <label class="star-5" id="layan-lima" for="star-service-5"><img style="width:50px" src="<?php echo base_url('assets/frontend/images/smiley_blank.png'); ?>"></label>
                            </div>
                            <p style="display: inline-block;
                            vertical-align: top;
                            position: relative;
                            left: 10px;
                            margin-top: 15px;"><b style="font-weight: bold;">Pelayanan</b><br><span id="star-service-amount">0</span> bintang</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="rating">
                                <input class="star star-1" id="star-accuracy-1" onclick="akurasi(1)" type="radio" name="rating_accuracy" value="1"/>
                                <label class="star-1" id="akur-satu" for="star-accuracy-1"><img style="width:50px" src="<?php echo base_url('assets/frontend/images/smiley_blank.png'); ?>"></label>
                                <input class="star star-2" id="star-accuracy-2" onclick="akurasi(2)" type="radio" name="rating_accuracy" value="2"/>
                                <label class="star-2" id="akur-dua" for="star-accuracy-2"><img style="width:50px" src="<?php echo base_url('assets/frontend/images/smiley_blank.png'); ?>"></label>
                                <input class="star star-3" id="star-accuracy-3" onclick="akurasi(3)" type="radio" name="rating_accuracy" value="3"/>
                                <label class="star-3" id="akur-tiga" for="star-accuracy-3"><img style="width:50px" src="<?php echo base_url('assets/frontend/images/smiley_blank.png'); ?>"></label>
                                <input class="star star-4" id="star-accuracy-4" onclick="akurasi(4)" type="radio" name="rating_accuracy" value="4"/>
                                <label class="star-4" id="akur-empat" for="star-accuracy-4"><img style="width:50px" src="<?php echo base_url('assets/frontend/images/smiley_blank.png'); ?>"></label>
                                <input class="star star-5" id="star-accuracy-5" onclick="akurasi(5)" type="radio" name="rating_accuracy" value="5"/>
                                <label class="star-5" id="akur-lima" for="star-accuracy-5"><img style="width:50px" src="<?php echo base_url('assets/frontend/images/smiley_blank.png'); ?>"></label>
                            </div>
                            <p style="display: inline-block;
                            vertical-align: top;
                            position: relative;
                            left: 10px;
                            margin-top: 15px;"><b style="font-weight: bold;">Akurasi</b><br><span id="star-accuracy-amount">0</span> bintang</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Isi Ulasan</label>
                                <textarea name="description" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" style="background-color: #FFF; color: #000;">Batal</button>
                    <button type="button" onclick="submit_review();" class="btn">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
