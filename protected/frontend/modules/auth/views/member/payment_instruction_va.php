<?php if ($data->payment_method == 'mandiri_va') { ?>
    <div class="panel-group" id="mandiri-va">
        <div class="panel payment-instruction-sub">

            <div class="panel-heading">
                <h6 class="panel-title">
                    <a data-toggle="collapse" data-parent="#mandiri-va" href="#mandiri-va-atm">ATM Mandiri</a>
                </h6>
            </div>
            <div id="mandiri-va-atm" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="payment-instruction-sub-container">
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">1</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Masukkan kartu ATM dan pilih "Bahasa Indonesia"</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">2</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Ketik nomor PIN kartu ATM</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">3</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Pilih menu BAYAR/BELI, kemudian pilih menu MULTI PAYMENT</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">4</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Ketik kode perusahaan, yaitu "88908" (88908 XENDIT), tekan "BENAR"</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">5</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">
                                Masukkan nomor Virtual Account 
                                <b class="account-number-medium"><?php echo $payment->account_number; ?></b>
                            </div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">6</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Isi NOMINAL, kemudian tekan "BENAR"</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">7</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Muncul konfirmasi data customer. Pilih Nomor 1 sesuai tagihan yang akan dibayar, kemudian tekan "YA"</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">8</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Muncul konfirmasi pembayaran. Tekan "YA" untuk melakukan pembayaran</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">9</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Bukti pembayaran dalam bentuk struk agar disimpan sebagai bukti pembayaran yang sah dari Bank Mandiri</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">10</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Transaksi Anda sudah selesai</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">11</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel payment-instruction-sub">
            <div class="panel-heading">
                <h6 class="panel-title">
                    <a data-toggle="collapse" data-parent="#mandiri-va" href="#mandiri-va-ib">Internet Banking Mandiri</a>
                </h6>
            </div>
            <div id="mandiri-va-ib" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="payment-instruction-sub-container">
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">1</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">
                                Kunjungi website Mandiri Internet Banking dengan alamat
                                <a href="https://ib.bankmandiri.co.id/" target="_blank"> https://ib.bankmandiri.co.id/</a>
                            </div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">2</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Login dengan memasukkan USER ID dan PIN</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">3</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Masuk ke halaman Beranda, lalu pilih "Bayar"</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">4</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Pilih "Multi Payment"</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">5</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Pilih "No Rekening Anda"</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">6</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">
                                Pilih Penyedia Jasa "88908 XENDIT"
                            </div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">7</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Pilih "No Virtual Account"</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">8</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">
                                Masukkan nomor Virtual Account
                                <b class="account-number-medium"><?php echo $payment->account_number; ?></b>
                            </div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">9</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Masuk ke halaman konfirmasi 1</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">10</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Apabila benar/sesuai, klik tombol tagihan TOTAL, kemudian klik "Lanjutkan"</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">11</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Masuk ke halaman konfirmasi 2</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">12</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Masukkan Challenge Code yang dikirimkan ke Token Internet Banking Anda, kemudian klik "Kirim"</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">13</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Masuk ke halaman konfirmasi pembayaran telah selesai</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">14</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit.</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
<?php } elseif ($data->payment_method == 'bni_va') { ?>
    <div class="panel-group" id="bni-va">
        <div class="panel payment-instruction-sub">
            <div class="panel-heading">
                <h6 class="panel-title">
                    <a data-toggle="collapse" data-parent="#bni-va" href="#bni-va-atm">ATM BNI</a>
                </h6>
            </div>
            <div id="bni-va-atm" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="payment-instruction-sub-container">
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">1</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Masukkan kartu, pilih bahasa kemudian masukkan PIN Anda</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">2</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Pilih "Menu Lainnya" lalu pilih "Transfer"</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">3</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Pilih "Tabungan" lalu "Rekening BNI Virtual Account"</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">4</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">
                                Masukkan nomor Virtual Account
                                <b class="account-number-medium"><?php echo $payment->account_number; ?></b>
                                dan nominal yang ingin Anda bayar
                            </div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">5</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Periksa kembali data transaksi kemudian tekan "YA"</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">6</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel payment-instruction-sub">
            <div class="panel-heading">
                <h6 class="panel-title">
                    <a data-toggle="collapse" data-parent="#bni-va" href="#bni-va-ib">Internet Banking BNI</a>
                </h6>
            </div>
            <div id="bni-va-ib" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="payment-instruction-sub-container">
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">1</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">
                                Login di 
                                <a href="https://ibank.bni.co.id" target="_blank"> https://ibank.bni.co.id</a>
                                , masukkan USER ID dan PASSWORD
                            </div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">2</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Pilih "TRANSFER" lalu pilih "Tambah Rekening Favorit"</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">3</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Jika Anda menggunakan desktop untuk menambah rekening, pilih "Transaksi", pilih "Info &amp; Administrasi Transfer" lalu pilih "Atur Rekening Tujuan" kemudian "Tambah Rekening Tujuan"</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">4</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">
                                Masukkan nama dan nomor Virtual Account Anda 
                                <b class="account-number-medium"><?php echo $bank->bank_account_number; ?></b>
                                , lalu masukkan Kode Otentikasi Token
                            </div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">5</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Jika Nomor rekening tujuan berhasil ditambahkan, kembali ke menu "TRANSFER"</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">6</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Pilih "TRANSFER ANTAR REKENING BNI", kemudian pilih rekening tujuan</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">7</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Pilih Rekening Debit dan ketik nominal, lalu masukkan kode otentikasi token</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">8</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel payment-instruction-sub">
            <div class="panel-heading">
                <h6 class="panel-title">
                    <a data-toggle="collapse" data-parent="#bni-va" href="#bni-va-mb">Mobile Banking BNI</a>
                </h6>
            </div>
            <div id="bni-va-mb" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="payment-instruction-sub-container">
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">1</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Login ke BNI Mobile Banking, masukkan USER ID dan MPIN</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">2</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Pilih menu "Transfer", lalu pilih "Antar Rekening BNI"</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">3</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Pilih "Input Rekening Baru"</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">4</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">
                                Masukkan "Rekening Debet", "Rekening Tujuan (<b class="account-number-medium"><?php echo $bank->bank_account_number; ?></b>)" dan "Nominal" kemudian klik "Lanjut"
                            </div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">5</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Periksa kembali data transaksi Anda, masukkan "Password Transaksi", kemudian klik "Lanjut"</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">6</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } elseif ($data->payment_method == 'bri_va') { ?>
    <div class="panel-group" id="bri-va">
        <div class="panel payment-instruction-sub">
            <div class="panel-heading">
                <h6 class="panel-title">
                    <a data-toggle="collapse" data-parent="#bri-va" href="#bri-va-atm">ATM BRI</a>
                </h6>
            </div>
            <div id="bri-va-atm" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="payment-instruction-sub-container">
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">1</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Masukkan kartu, pilih bahasa kemudian masukkan PIN Anda</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">2</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Pilih "Transaksi Lain" lalu pilih "Pembayaran"</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">3</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Pilih "Lainnya" lalu pilih "Briva"</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">4</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">
                                Masukkan nomor Virtual Account <b class="account-number-medium"><?php echo $bank->bank_account_number; ?></b> dan nominal yang ingin Anda bayar
                            </div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">5</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Periksa kembali data transaksi kemudian tekan "YA"</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">6</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel payment-instruction-sub">
            <div class="panel-heading">
                <h6 class="panel-title">
                    <a data-toggle="collapse" data-parent="#bri-va" href="#bri-va-ib">Internet Banking BRI</a>
                </h6>
            </div>
            <div id="bri-va-ib" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="payment-instruction-sub-container">
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">1</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">
                                Login di <a href="https://ib.bri.co.id" target="_blank"> https://ib.bri.co.id/</a>, masukkan USER ID dan PASSWORD
                            </div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">2</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Pilih "Pembayaran" lalu pilih "Briva"</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">3</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">
                                Masukkan nomor Virtual Account Anda <b class="account-number-medium"><?php echo $bank->bank_account_number; ?></b>, nominal yang akan dibayar, lalu klik kirim
                            </div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">4</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Masukkan kembali PASSWORD anda serta kode otentikasi mToken internet banking</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">5</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel payment-instruction-sub">
            <div class="panel-heading">
                <h6 class="panel-title">
                    <a data-toggle="collapse" data-parent="#bri-va" href="#bri-va-mb">Mobile Banking BRI</a>
                </h6>
            </div>
            <div id="bri-va-mb" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="payment-instruction-sub-container">
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">1</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Login ke BRI Mobile Banking, masukkan USER ID dan PIN anda</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">2</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Pilih "Pembayaran" lalu pilih "Briva"</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">3</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">
                                Masukkan nomor Virtual Account anda <b class="account-number-medium"><?php echo $bank->bank_account_number; ?></b>, serta nominal yang akan dibyar
                            </div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">4</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Masukkan nomor PIN anda dan klik "Kirim"</div>
                        </div>
                        <div class="clearance">&nbsp;</div>
                        <div class="step-info-column-number">
                            <div class="textbox-circle-outer-green">
                                <div class="textbox-circle-green">
                                    <div class="textbox-circle-num">5</div>
                                </div>
                            </div>
                        </div>
                        <div class="step-info-column-text">
                            <div class="description-step-info">Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>