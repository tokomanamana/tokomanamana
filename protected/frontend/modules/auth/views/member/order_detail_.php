<div class="my-account" id="detail-order">
    <section class="collection-heading heading-content ">
        <div class="container">
            <div class="row">
                <div class="col-sm-9 col-sm-push-3">
                    <div class="collection-wrapper">
                        <h1 class="collection-title"><span>Detail Pesanan</span></h1>
                        <?php echo $breadcrumb; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="container" style="margin-bottom: 30px;">
        <div class="row">
            <?php echo $this->load->view('navigation'); ?>

            <div class="collection-mainarea col-md-9 col-sm-8 col-xs-12" style="padding-left: 0;">
                <div class="row">
                    <div class="col-sm-5 col-sm-push-7 col-xs-12">
                        <div class="panel">
                            <div class="panel-body">
                                <div class="order-info-label">NO. TAGIHAN</div>
                                <div class="order-info-content"><?php echo $data->code; ?></div>
                                
                                <hr>
                                
                                <div class="order-info-label">STATUS TAGIHAN</div>
                                <div class="order-info-content"><?php echo lang($data->payment_status); ?></div>
                                
                                <hr>
                                
                                <div class="order-info-label">CARA PEMBAYARAN</div>
                                <div class="order-info-content"><?php echo $data->payment_method_name; ?></div>
                                
                                <hr>
                                
                                <?php
                                    if ($data->payment_status == 'Pending') {
                                ?>
                                        <div class="order-info-label">BATAS PEMBAYARAN</div>
                                        <div class="order-info-content"><?php echo get_date_indo_full($data->due_date); ?></div>
                                        <hr>
                                <?php
                                    }
                                ?>
                                <div class="order-info-label">RINCIAN PEMBAYARAN</div>
                                <div class="order-info-content">
                                    <dl>
                                        <dt>Harga Barang</dt>
                                        <dd><?php echo rupiah($this->member->get_subtotal($data->id)); ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Biaya Pengiriman</dt>
                                        <dd><?php echo rupiah($this->member->get_total_shipping_cost($data->id)); ?></dd>
                                    </dl>

                                    <?php
                                        if ($data->payment_method == 'transfer') {
                                    ?>
                                            <dl>
                                                <dt>Kode Unik</dt>
                                                <dd><?php echo rupiah($data->unique_code); ?></dd>
                                            </dl>
                                    <?php
                                        }
                                    ?>

                                    <dl style="margin-top: 15px;">
                                        <dt><strong>Total Pembayaran</strong></dt>

                                    <?php
                                        if ($data->payment_method == 'transfer') {
                                    ?>
                                            <dd><strong><?php echo rupiah($data->total+$data->unique_code); ?></strong></dd>
                                    <?php
                                        } else {
                                    ?>
                                            <dd><strong><?php echo rupiah($data->total); ?></strong></dd>
                                    <?php
                                        }
                                    ?>

                                    </dl>
                                </div>
                                <hr>
                                <div class="order-info-label">ALAMAT PENGIRIMAN</div>
                                <div class="order-info-content">
                                    <b><?php echo $data->shipping_name; ?></b><br>
                                    <?php echo $data->shipping_phone; ?><br>
                                    <?php echo nl2br($data->shipping_address); ?>,
                                    <?php echo $data->shipping_district_name; ?><br><?php echo $data->shipping_city_name; ?> - <?php echo $data->shipping_postcode; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-7 col-sm-pull-5 col-xs-12" style="padding-right: 0;">
                    <?php
                        if ($data->payment_status == 'Pending') {
                    ?>
                            <div class="panel">
                                <div class="panel-heading">
                                    <h2 class="panel-title">Petunjuk Pembayaran</h2>
                                </div>
                                <div class="panel-body">
                                <?php
                                    $payment_method = $this->main->get('payment_methods', array('name' => $data->payment_method));
                                    
                                    if (in_array($data->payment_method, ['transfer', 'bca_va', 'mandiri_va', 'bni_va', 'bri_va', 'credit_card'])) {
                                ?>
                                        <p>Lakukan pembayaran melalui <?php
                                            if ($data->payment_method == 'transfer') {
                                                echo 'ATM/transfer';
                                            } elseif ($data->payment_method == 'credit_card') {
                                                echo 'Visa/Master Card';
                                            } else {
                                                //echo $payment->account_name;
                                            }
                                            ?> dengan detail sebagai berikut:</p>
                                        <div class="panel payment-instruction">
                                            <div class="panel-heading">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-3">
                                                        <div class="payment-instruction-label">TOTAL</div>
                                                        <div class="payment-instruction-content">
                                                        
                                                        <?php 
                                                            if ($data->payment_method == 'transfer') { ?>
                                                                <strong><?php echo rupiah($data->total+$data->unique_code); ?></strong>
                                                        <?php
                                                            } else {
                                                        ?>
                                                                <strong><?php echo rupiah($data->total); ?></strong>
                                                        <?php
                                                            }
                                                        ?>

                                                        </div>
                                                    </div>

                                                    <?php
                                                        if ($data->payment_method == 'transfer') {
                                                    ?>
                                                            <div class="col-xs-12 col-sm-9">
                                                                <div class="alert alert-warning no-border">
                                                                    Transfer sesuai dengan total.
                                                                </div>
                                                            </div>
                                                    <?php
                                                        }
                                                    ?>
                                                </div>
                                            </div>
                                            <!-- end of panel-heading class -->

                                            <?php
                                                if ($data->payment_method == 'transfer') {
                                            ?>
                                                    <div class="panel-body">
                                                        <div class="payment-instruction-label">REKENING TUJUAN</div>
                                                        
                                                        <div class="list-bank">
                                                        <?php
                                                            $payment = json_decode($data->payment_to);
                                                            foreach ($payment as $pym) {
                                                        ?>
                                                                <div class="detail-bank">
                                                                    <img src="<?php echo base_url('files/images/' . $pym->image); ?>">
                                                                    <div><strong><?php echo $pym->account_number; ?></strong></div>
                                                                    <div><?php echo $pym->account_name; ?></div>
                                                                    <div><?php echo $pym->name . ' ' . $pym->branch; ?></div>
                                                                </div>
                                                        <?php
                                                            }
                                                        ?>
                                                        </div>

                                                        <div class="text-center" style="margin-top: 15px;"><button type="button" id="payment-confirmation" class="btn btn-primary">Konfirmasi Pembayaran</button></div>
                                                    </div>
                                            <?php
                                                } elseif ($data->payment_method == 'credit_card') {
                                            ?>
                                                    <div class="panel-body">
                                                        <div class="text-center">
                                                            <a class="btn" href="<?php echo site_url('checkout/credit_card?id=' . $data->code); ?>">Bayar Sekarang</a>
                                                        </div>
                                                    </div>
                                            <?php
                                                } else {
                                            ?>
                                                    <div class="panel-heading">
                                                        <div class="payment-instruction-label">NOMOR REKENING <?php echo $payment->account_name; ?></div>
                                                        <div class="payment-instruction-content"><strong><?php echo $payment->account_number; ?></strong></div>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="payment-instruction-label">PETUNJUK PEMBAYARAN MELALUI <?php echo $payment->account_name; ?></div>
                                                        <?php $this->load->view('member/payment_instruction_va'); ?>
                                                    </div>
                                            <?php
                                                }
                                            ?>
                                        </div>
                                <?php
                                    } elseif ($data->payment_method == 'alfamart') {
                                        $this->load->view('member/payment_instruction_alfamart');
                                    } elseif (in_array($data->payment_method, ['kredivo', 'bca_klikpay', 'bca_sakuku'])) {
                                ?>
                                        <p>Anda belum menyelesaikan pembayaran. Klik tombol "Lanjutkan Pembayaran" di bawah untuk melanjutkan pembayaran Anda.</p>
                                        <div class="text-center">
                                        <?php
                                            if ($data->payment_method == 'bca_klikpay') {
                                                $payment_data = (array) $payment->payment_data;
                                                echo '<form name="redirect" action="' . $payment->payment_url . '" method="POST">
                                                <input type="hidden" name="klikPayCode" value="' . $payment_data['klikPayCode'] . '">
                                                <input type="hidden" name="transactionNo" value="' . $payment_data['transactionNo'] . '">
                                                <input type="hidden" name="totalAmount" value="' . $payment_data['totalAmount'] . '">
                                                <input type="hidden" name="currency" value="' . $payment_data['currency'] . '">
                                                <input type="hidden" name="payType" value="' . $payment_data['payType'] . '">
                                                <input type="hidden" name="callback" value="' . $payment_data['callback'] . '">
                                                <input type="hidden" name="transactionDate" value="' . $payment_data['transactionDate'] . '">
                                                <input type="hidden" name="descp" value="' . $payment_data['descp'] . '">
                                                <input type="hidden" name="miscFee" value="' . $payment_data['miscFee'] . '">
                                                <input type="hidden" name="signature" value="' . $payment_data['signature'] . '">
                                                <button type="submit" class="btn">Lanjutkan Pembayaran</button>
                                                </form>';
                                            } else {
                                        ?>
                                                <a class="btn" href="<?php echo $payment->payment_url; ?>">Lanjutkan Pembayaran</a>
                                        <?php
                                            }
                                        ?>
                                        </div>
                                <?php
                                    }
                                ?>
                                </div>
                            </div>
                    <?php
                        }
                        /* end if payment status is pending */
                    ?>
                        <div class="panel">
                            <div class="panel-heading">
                                <h2 class="panel-title">Daftar Pembelian</h2>
                            </div>

                            <div class="panel-body">
                            <?php
                                $invoices = $this->member->get_order_invoice($data->id);
                                
                                if ($invoices->num_rows() > 0) foreach ($invoices->result() as $invoice) {
                            ?>
                                    <div class="order-invoice">
                                        <div class="order-invoice-header">
                                            <div class="order-invoice-header-cell">
                                                <div class="order-info-label">NO. INVOICE</div>
                                                <div class="order-info-content"><?php echo $invoice->code; ?></div>
                                            </div>
                                            <div class="order-invoice-header-cell">
                                                <div class="order-info-label">PENJUAL</div>
                                                <div class="order-info-content"><?php echo $invoice->merchant_name; ?></div>
                                            </div>
                                        </div>
                                        <div class="order-invoice-product">
                                        <?php
                                            $products = $this->member->get_order_products($invoice->id);
                                            
                                            if ($products->num_rows() > 0) foreach ($products->result() as $product) {
                                        ?>
                                                <div class="row">
                                                    <div class="col-xs-2" style="padding-right: 0;">
                                                        <a href="<?php echo seo_url('catalog/products/view/' . $product->product . '/' . $invoice->merchant); ?>" target="_blank">
                                                            <img class="product-image" src="<?php echo get_image($product->image); ?>">
                                                        </a>
                                                    </div>
                                                    <div class="col-sm-7 col-xs-10">
                                                        <div class="product-name">
                                                            <a href="<?php echo seo_url('catalog/products/view/' . $product->product . '/' . $invoice->merchant); ?>" target="_blank"><?php echo $product->name; ?></a>
                                                        </div>
                                                        <div class="product-detail">
                                                            <div class="product-info">
                                                            <?php
                                                                if ($product->options = json_decode($product->options)) {
                                                                    foreach ($product->options as $option) {
                                                            ?>
                                                                        <div class="product-option" style="height: 20px;"><?php echo $option->option_name; ?></div>
                                                            <?php
                                                                    }
                                                            ?>
                                                                    <div class="divider"></div>
                                                            <?php
                                                                }
                                                            ?>
                                                                <div><?php echo $product->code; ?></div>
                                                            </div>
                                                            <span>Jumlah: <?php echo $product->quantity; ?></span>
                                                            <span>Berat: <?php echo $product->weight . 'gram'; ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 col-xs-12" style="padding-left: 0;">
                                                        <div class="text-right"><?php echo rupiah($product->total); ?></div>
                                                    </div>
                                                </div>
                                        <?php
                                            }
                                            /* end foreach */
                                        ?>
                                        </div>
                                        <!-- end of order-invoice-product class -->
                    <?php
                        if ($data->payment_status == 'Paid' || $data->payment_status == 'Pending') {
                    ?>
                                    <div class="order-invoice-detail">
                                        <div class="order-info-label">STATUS PEMBELIAN</div>
                                        <div class="order-info-content"><?php echo $invoice->order_status_name; ?></div>
                                    </div>
                                    
                                    <div class="order-invoice-shipping">
                                    <?php
                                        if ($invoice->shipping_courier == 'pickup') {
                                    ?>
                                            <div style="padding: 12px;">
                                                <div class="order-info-label">JASA PENGIRIMAN</div>
                                                <div class="order-info-content">Ambil Sendiri</div>
                                            </div>
                                    <?php
                                        } else {
                                            $invoice->shipping_courier = explode('-', $invoice->shipping_courier);
                                            $invoice->shipping_courier = strtoupper($invoice->shipping_courier[0]) . ' ' . $invoice->shipping_courier[1];
                                    ?>
                                            <div class="order-invoice-shipping-cell">
                                                <div class="order-info-label">JASA PENGIRIMAN</div>
                                                <div class="order-info-content"><?php echo $invoice->shipping_courier; ?></div>
                                            </div>
                                            <div class="order-invoice-shipping-cell">
                                                <div class="order-info-label">NO. RESI</div>
                                                <div class="order-info-content"><?php echo ($invoice->tracking_number) ? $invoice->tracking_number : '-'; ?></div>
                                            </div>
                                            <?php
                                                if ($invoice->tracking_number && in_array($invoice->order_status, [settings('order_shipped_status'), settings('order_delivered_status')])) {
                                            ?>
                                                    <div class="order-invoice-shipping-cell" style="text-align: center; vertical-align: middle;">

                                                        <a href="javascript:void(0);" class="link tracking" data-id="<?php echo encode($invoice->id); ?>">Lacak Pengiriman</a>
                                                    </div>
                                    <?php
                                                }
                                        }
                                    ?>
                            </div>
                            <!-- end of order-invoice-shipping class -->
                            
                            <?php
                                if ($invoice->shipping_courier == 'pickup' && in_array($invoice->order_status, [settings('order_ready_pickup_status'), settings('order_complete_pickup_status')])) {
                            ?>
                                    <div class="order-invoice-detail">
                                        <div class="order-info-label">ALAMAT PENGAMBILAN</div>
                                        <div class="order-info-content">
                                            <?php $merchant = $this->member->merchant($invoice->merchant); ?>
                                            <?php echo '<strong>' . $merchant->name . '</strong>
                                        <br>' . $merchant->address . '<br>' . $merchant->district . ' - ' . $merchant->city . '<br>' . $merchant->province . ' ' . $merchant->postcode . '<br><i class="fa fa-phone"></i> ' . $merchant->telephone . '<br>
                                        <a style="color:blue;" href="https://www.google.com/maps/search/?api=1&query=' . $merchant->lat . ',' . $merchant->lng . '" target="_blank"><i class="fa fa-map-marker"></i> View on Google Maps</a>'; ?></p>
                                        </div>
                                    </div>
                            <?php
                                }
                                
                                if (in_array($invoice->order_status, [settings('order_complete_pickup_status'), settings('order_delivered_status'), settings('order_shipped_status')])) {
                            ?>
                                    <div class="order-invoice-detail text-center">
                                        <button class="btn" id="confirm" data-id="<?php echo encode($invoice->id); ?>" data-code="<?php echo $invoice->code; ?>">KONFIRMASI TERIMA BARANG</button>
                                    </div>
                    <?php
                                }
                        }
                        /* end if payment status is is paid or pending */
                    ?>
                                    </div>
                                    <!-- end of order-invoice class -->
                            <?php
                                }
                            ?>
                            </div>
                            <!-- end of panel-body -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end of container -->


</div>
<!-- end of detail-order id -->
<div id="modal-tracking" class="modal" role="dialog" aria-hidden="true" tabindex="-1" data-width="800" style="display: none;">
    <div class="modal-dialog fadeIn animated">
        <div class="modal-content">
            <form action="#">
                <input type="hidden" name="id">
                <div class="modal-header">
                    <h1 class="modal-title">Lacak Pesanan</h1>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" style="background-color: #FFF; color: #000;">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
    if ($data->payment_method == 'transfer') {
?>
        <div id="modal-payment" class="modal" role="dialog" aria-hidden="true" tabindex="-1" style="display: none;">
            <div class="modal-dialog fadeIn animated">
                <div class="modal-content">
                    <form action="#" class="form-horizontal">
                        <input type="hidden" name="id" value="<?php echo encode($data->id); ?>">
                        <div class="modal-header">
                            <h1 class="modal-title">Konfirmasi Pembayaran<br><small>Tagihan <?php echo $data->code; ?></small></h1>
                        </div>
                        <div class="modal-body">
                            <div id="message"></div>
                            <div class="form-group row">
                                <label class="control-label col-sm-3">Transfer ke</label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="to" required="">
                                    <?php
                                        foreach ($payment as $id_bank => $pym) {
                                    ?>
                                            <option value="<?php echo $id_bank; ?>"><?php echo $pym->name . ' (' . $pym->account_number . ')'; ?></option>
                                    <?php
                                        }
                                    ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-sm-3">Sebesar</label>
                                <div class="col-sm-9">
                                    <input name="amount" type="text" class="form-control" required="" value="<?php echo $data->total+$data->unique_code; ?>" placeholder="Total transfer, input hanya angka">
                                    Total Transaksi: <b><?php echo rupiah($data->total+$data->unique_code); ?></b>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-sm-3">Dari Rekening Atas Nama</label>
                                <div class="col-sm-9">
                                    <input name="from" type="text" class="form-control" required="" placeholder="Nama yang tertera di rekening pengirim">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-sm-3">Catatan</label>
                                <div class="col-sm-9">
                                    <textarea name="note" type="text" class="form-control" placeholder="Catatan khusus yang perlu kami perhatikan"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn" data-dismiss="modal" style="background-color: #FFF; color: #000;">Batal</button>
                            <button type="button" onclick="submit_payment();" class="btn">Konfirmasi</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
<?php
    }
?>
<div id="modal-confirm" class="modal" role="dialog" aria-hidden="true" tabindex="-1" style="display: none;">
    <div class="modal-dialog fadeIn animated">
        <div class="modal-content">
            <input type="hidden" name="id" value="">
            <div class="modal-header">
                <h1 class="modal-title">Apakah Anda yakin transaksi <small></small> sudah diterima?</h1>
            </div>
            <div class="modal-body">
                <p>Lakukan konfirmasi ini hanya jika pesanan Anda sudah benar-benar diterima dengan baik dan lengkap. Karena transaksi yang akan selesai dan dana Anda akan diteruskan ke Penjual.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal" style="background-color: #FFF; color: #000;">TIDAK</button>
                <button type="button" onclick="submit_confirm();" class="btn">YA</button>
            </div>
        </div>
    </div>
</div>
