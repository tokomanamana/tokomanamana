<link rel="stylesheet" href="<?php echo site_url('assets/frontend/css/minified/profile_min.css') ?>">
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-flat.css" />
<script>
    const referral_code = '<?= $user->referral_code ?>'
</script>
<?php 
function date_indonesian($tanggal){
    $bulan = array (
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $pecahkan = explode('-', $tanggal);
 
    return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}
?>
<div class="my-account">
    <section class="collection-heading heading-content ">
        <div class="container">
            <div class="row">
                <div class="col-sm-9 col-sm-push-3" style="padding-left: 0px; padding-right: 0px;">
                    <div class="collection-wrapper block" style="padding: 10px 15px;background: none;">
                        <h1 class="collection-title"><span><?php echo lang('account_profile'); ?></span></h1>
                        <?php echo $breadcrumb; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container" style="margin-bottom: 30px;">
        <div class="row">
            <?php echo $this->load->view('navigation'); ?>
            <div class="collection-mainarea col-md-9 col-sm-8 col-xs-12" style="padding-right: 0px;padding-left: 0px;">
                <div class="block" <?php echo ($this->agent->is_mobile()) ? 'style="padding: 0px 15px 25px 15px"' : 'style="padding-top:20px;"' ?>>
                            <?php
                            if (validation_errors()) {
                                echo $this->template->alert('danger', 'Profil gagal disimpan. Tinjau kembali isian Anda.');
                            }
                            if ($this->session->flashdata('error')) {
                                echo $this->template->alert('danger', $this->session->flashdata('error'));
                            }
                            if ($this->session->flashdata('success')) {
                                echo $this->template->alert('success', $this->session->flashdata('success'));
                            }
                            if ($this->session->flashdata('warning')) {
                                echo $this->template->alert('warning', $this->session->flashdata('warning'));
                            }
                            ?>
                            <?php 
                            $check_address = $this->main->gets('customer_address', ['customer' => $this->data['user']->id]);
                            if(!$check_address) :
                            ?>
                                <div class="alert alert-warning" role="alert">
                                    <b>Belum ada alamat!</b> <a href="<?php echo site_url('member/address') ?>" id="link_address">Tambah Alamat</a>
                                </div>
                            <?php endif; ?>
                            <div id="message_not_phone_">
                                <?php if($customer->phone == '') : ?>
                                    <div class="alert alert-danger">
                                        <b>Anda harus mengisi No Handphone agar dapat berbelanja di TokoManaMana!</b>
                                    </div>
                                <?php elseif(!$customer->verification_phone) : ?>
                                    <div class="alert alert-danger">
                                        <b>Segera lakukan verifikasi No Handphone agar anda dapat berbelanja di TokoManaMana!</b>
                                    </div>
                                <?php endif; ?>
                            </div>

                            <?php if($this->session->flashdata('email_verification_message')) : ?>
                                <div class="message_verification" role="alert">
                                    <?php echo $this->session->flashdata('email_verification_message') ?>
                                </div>
                            <?php endif; ?>

                            <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                                <ul id="myTab" class="nav nav-tabs nav-tabs-responsive" role="tablist">
                                    <li role="presentation" class="active">
                                        <a href="#biodata" class="tab-header" id="biodata-tab" role="tab" data-toggle="tab" aria-controls="biodata" aria-expanded="true">
                                            Data Diri
                                        </a>
                                    </li>
                                    <li role="presentation" class="next">
                                        <a href="#norek" class="tab-header" role="tab" id="norek-tab" data-toggle="tab" aria-controls="norek">
                                            Rekening Bank
                                        </a>
                                    </li>
                                </ul>
                                <div id="myTabContent" class="tab-content">
                                    <?php if($this->agent->is_mobile()) : ?>
                                        <div role="tabpanel" class="tab-pane fade in active nav-white" id="biodata" aria-labelledby="biodata-tab">
                                            <br>
                                            <div class="row div_mobile">
                                                <div class="profile-image-mobile">
                                                    <span class="title-profile-image">Foto Profil</span>
                                                    <div class="profile-image-left">
                                                        <?php if($customer->image) : ?>
                                                            <img src="<?php echo site_url('files/images/customers_image/' . $customer->image) ?>" style="border-radius: 3px;width: 150px;">
                                                        <?php else : ?>
                                                            <img src="<?php echo site_url('files/images/customers_image/default_image_profile.jpg') ?>" style="border-radius: 3px;width: 150px;">
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="profile-image-right">
                                                        <span class="btn btn-file">
                                                            Pilih Foto <input type="file" name="profile_picture" id="profile_picture" onchange="return change_picture('<?php echo encode($customer->id) ?>', this)" accept="image/*">
                                                        </span>
                                                        <span style="text-align: left;margin-top: 10px;color: #AAA;">Ukuran file maksimal: <b>4MB</b></span>
                                                        <span style="text-align: left;margin-top: 10px;color: #AAA;">Jenis ekstensi file yang diperbolehkan: <b>JPG, JPEG, PNG</b></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row div_mobile">
                                                <div class="left">
                                                    <span>Email
                                                    <?php if($customer->email) : ?>
                                                        <?php if($customer->email_verification == 1) : ?>
                                                            <div class="phone_verification_label_mobile"><i><i class="fa fa-check" aria-hidden="true"></i> Terverifikasi</i></div>
                                                        <?php elseif($customer->email_verification == 2) : ?>
                                                            <span id="wait_verification_email_mobile"><i><i class="fa fa-clock-o" aria-hidden="true"></i> Menunggu Konfirmasi</i></span>
                                                        <?php else : ?>
                                                            <a href="javascript:void(0)" id="not_verification_email_link_mobile" onclick="verification_email('<?php echo encode($customer->id) ?>', 'sending')">Verifikasi Sekarang!</a>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                    </span>
                                                    <span>
                                                    <?php if($customer->email) : ?>
                                                        <?php echo $customer->email ?>
                                                        <?php if($customer->email_verification == 2) : ?>
                                                            <?php
                                                            $now = time();
                                                            $email_verification_sent_time = strtotime($customer->email_verification_sent_time);
                                                            $range = $now - $email_verification_sent_time;
                                                            ?>
                                                            <?php if($range >= 120) : ?>
                                                                <br>
                                                                <a href="javascript:void(0)" id="not_verification_email_link" style="margin-left: 0px;" onclick="verification_email('<?php echo encode($customer->id) ?>', 'resending')">Verifikasi Ulang?</a>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    <?php else : ?>
                                                        <a href="javascript:void(0)" id="add_email" onclick="add_email()" style="color: #D9534F">Tambah Email</a>
                                                    <?php endif; ?>
                                                    </span>
                                                </div>
                                                <div class="right">
                                                    <?php if($customer->email) : ?>
                                                        <a href="javascript:void(0)" id="add_email" onclick="add_email()" style="margin-left: 10px;">Ubah</a>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <div class="row div_mobile">
                                                <div class="left">
                                                    <span>Nama Lengkap</span>
                                                    <span><?php echo $customer->fullname ?></span>
                                                </div>
                                                <div class="right">
                                                    <a href="javascript:void(0)" id="add_name" onclick="add_name()" style="margin-left: 10px;">Ubah</a>
                                                </div>
                                            </div>
                                            <div class="row div_mobile">
                                                <div class="left">
                                                    <span>No. Handphone 
                                                        <?php if($customer->phone == '') : ?>
                                                            <br>
                                                            <a href="javascript:void(0)" id="add_phone" onclick="add_phone()">Tambah No Handphone</a>
                                                        <?php elseif($customer->verification_phone != 1) : ?>
                                                            <a href="javascript:void(0)" id="verification_phone_link" onclick="add_phone('verif')" style="margin-left: 7px;">Verifikasi Sekarang!</a>
                                                        <?php else : ?>
                                                            <div class="phone_verification_label_mobile"><i><i class="fa fa-check" aria-hidden="true"></i> Terverifikasi</i></div>
                                                        <?php endif; ?>
                                                    </span>
                                                    <?php if($customer->phone) : ?>
                                                        <span>
                                                            <?php echo $customer->phone ?>
                                                        </span>
                                                    <?php else : ?>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <div class="row div_mobile">
                                                <div class="left">
                                                    <span>Tanggal Lahir</span>
                                                    <span>
                                                        <?php if($customer->birthday) : ?>
                                                            <?php if($customer->birthday == '0000-00-00') : ?>
                                                                <a href="javascript:void(0)" id="edit_birthday" onclick="birthday_modal()">Tambah Tanggal Lahir</a>
                                                            <?php else : ?>
                                                                <?php echo date_indonesian($customer->birthday) ?>
                                                            <?php endif; ?>
                                                        <?php else : ?>
                                                            <a href="javascript:void(0)" id="edit_birthday" onclick="birthday_modal()">Tambah Tanggal Lahir</a>
                                                        <?php endif; ?>
                                                    </span>
                                                </div>
                                                <div class="right">
                                                    <?php if($customer->birthday) : ?>
                                                        <?php if($customer->birthday != '0000-00-00') : ?>
                                                            <a href="javascript:void(0)" id="edit_birthday" onclick="birthday_modal()" style="margin-left: 10px;">Ubah</a>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <div class="row div_mobile">
                                                <div class="left">
                                                    <span>Jenis Kelamin</span>
                                                    <span>
                                                        <?php if($customer->gender) : ?>
                                                            <?php echo ($customer->gender == 'L') ? 'Laki - Laki' : 'Perempuan' ?>
                                                        <?php else : ?>
                                                            <a href="javascript:void(0)" id="gender_link" onclick="gender_modal()">Tambah Jenis Kelamin</a>
                                                        <?php endif; ?>
                                                    </span>
                                                </div>
                                                <div class="right">
                                                    <?php if($customer->gender) : ?>
                                                        <a href="javascript:void(0)" id="gender_link" onclick="gender_modal()" style="margin-left: 10px;">Ubah</a>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade nav-white" id="norek" aria-labelledby="norek-tab">
                                            <br>
                                            <div class="row div_mobile_bank">
                                                <h1 style="font-weight: bold;">Rekening Bank</h1>
                                                <!-- <div class="alert alert-info">
                                                    Catatan: <b>Rekening Bank digunakan untuk proses penarikan Saldo Tokomanamana.</b>
                                                    <ul>
                                                        <li style="list-style: disc;margin-left: 17px;">Anda dapat mengubah rekening bank melalui Admin kami.</li>
                                                        <li style="list-style: disc;margin-left: 17px;">Rekening Bank utama tidak bisa dihapus.</li>
                                                        <li style="list-style: disc;margin-left: 17px;">Anda hanya dapat memiliki 3 rekening bank.</li>
                                                    </ul>
                                                </div> -->
                                                <?php if($bank_accounts) : ?>
                                                    <?php if($bank_accounts->num_rows() > 0) : ?>
                                                        <ul class="list-group list-bank" style="margin-top: 20px;">
                                                            <?php foreach($bank_accounts->result() as $bank_account) : ?>
                                                                <li class="list-group-item">
                                                                    <div class="row">
                                                                        <div class="left">
                                                                            <div class="bank_name"><?php echo $bank_account->bank_name ?></div>
                                                                            <div class="bank_account_number"><?php echo $bank_account->bank_account_number ?> <?php echo ($bank_account->primary) ? '<span>Utama</span>' : '' ?></div>
                                                                            <div class="bank_account_name"><span>a.n</span> <?php echo $bank_account->bank_account_name ?></div>
                                                                        </div>
                                                                        <?php if(!$bank_account->primary) : ?>
                                                                            <div class="right">
                                                                                <button class="btn_delete_account_bank" onclick="delete_account_bank('<?php echo encode($bank_account->id) ?>')">Hapus</button>
                                                                            </div>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                </li>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                    <?php else : ?>
                                                        <div class="alert alert-warning">
                                                            <center><b>Anda belum menambah rekening bank. Silahkan tambah rekening untuk memudahkan anda berbelanja!</b></center>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php else : ?>
                                                    <div class="alert alert-warning">
                                                        <center><b>Anda belum menambah rekening bank. Silahkan tambah rekening untuk memudahkan anda berbelanja!</b></center>
                                                    </div>
                                                <?php endif; ?>
                                                <?php if($bank_accounts) : ?>
                                                    <?php if($bank_accounts->num_rows() < 3) : ?>
                                                        <div class="" style="margin: 10px 0px;">
                                                            <button type="button" class="btn btn-norek-mobile" onclick="addBank();">Tambah Rekening</button>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php else : ?>
                                                    <div class="" style="margin: 10px 0px;">
                                                        <button type="button" class="btn btn-norek-mobile" onclick="addBank();">Tambah Rekening</button>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    <?php else : ?>
                                        <div role="tabpanel" class="tab-pane fade in active nav-white" id="biodata" aria-labelledby="biodata-tab">
                                            <br>
                                            <div class="left-biodata">
                                                <div class="profile-image">
                                                    <?php if($customer->image) : ?>
                                                        <img src="<?php echo site_url('files/images/customers_image/' . $customer->image) ?>" style="border-radius: 3px;width: 150px;">
                                                    <?php else : ?>
                                                        <img src="<?php echo site_url('files/images/customers_image/default_image_profile.jpg') ?>" style="border-radius: 3px;width: 150px;">
                                                    <?php endif; ?>
                                                    <span class="btn btn-file">
                                                        Pilih Foto <input type="file" name="profile_picture" id="profile_picture" onchange="return change_picture('<?php echo encode($customer->id) ?>', this)" accept="image/*">
                                                    </span>
                                                    <span style="text-align: left;margin-top: 10px;color: #AAA;">Ukuran file maksimal: <b>4MB</b></span>
                                                    <span style="text-align: left;margin-top: 10px;color: #AAA;">Jenis ekstensi file yang diperbolehkan: <b>JPG, JPEG, PNG</b></span>
                                                </div>
                                            </div>
                                            <div class="right-biodata">
                                                <form action="#" method="post" class="form-horizontal" onsubmit="function(e){e.preventDefault();}">
                                                    <div class="form-group email">
                                                        <label class="col-sm-3 control-label">Email</label>
                                                        <div class="col-sm-9">
                                                            <?php $cus_email_2 = false; ?>
                                                            <?php if($customer->email) : ?>
                                                                <div class="form-control-static">
                                                                    <?php echo $customer->email; ?>
                                                                    <?php if($customer->email_verification == 1) : ?>
                                                                        <div class="phone_verification_label"><i><i class="fa fa-check" aria-hidden="true"></i> Terverifikasi</i></div>
                                                                        <a href="javascript:void(0)" id="add_email" class="link-attr" onclick="add_email()" style="margin-left: 5px;">Ubah</a>
                                                                    <?php elseif($customer->email_verification == 2) : ?>
                                                                        <span id="wait_verification_email"><i><i class="fa fa-clock-o" aria-hidden="true"></i> Menunggu Konfirmasi</i></span>
                                                                        <a href="javascript:void(0)" id="add_email" onclick="add_email()" class="link-attr" style="margin-left: 10px;">Ubah</a>
                                                                    <?php else : ?>
                                                                        <a href="javascript:void(0)" id="not_verification_email_link" onclick="verification_email('<?php echo encode($customer->id) ?>', 'sending')">Verifikasi Sekarang!</a>
                                                                        <a href="javascript:void(0)" id="add_email" class="link-attr" onclick="add_email()" style="margin-left: 10px;">Ubah</a>
                                                                    <?php endif; ?>
                                                                    <?php if($customer->email_verification == 2) : ?>
                                                                        <?php
                                                                        $now = time();
                                                                        $email_verification_sent_time = strtotime($customer->email_verification_sent_time);
                                                                        $range = $now - $email_verification_sent_time;
                                                                        ?>
                                                                        <?php if($range >= 120) : ?>
                                                                            <?php $cus_email_2 = true; ?>
                                                                            <br>
                                                                            <a href="javascript:void(0)" id="not_verification_email_link" style="margin-left: 0px;margin-right: 5px;" onclick="verification_email('<?php echo encode($customer->id) ?>', 'resending')">Verifikasi Ulang?</a>
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                    <?php if($customer->email_verification == 2) : ?>
                                                                        <?php if(!$cus_email_2) : ?>
                                                                            <br>
                                                                        <?php endif; ?>
                                                                        <a href="javascript:void(0)" id="add_email" class="link-attr" onclick="input_code_verification_email();">Masukkan Kode</a>
                                                                    <?php endif; ?>
                                                                </div>
                                                            <?php else : ?>
                                                                <!-- <input type="text" class="form-control" name="email" placeholder="Masukkan Email" style="border: 1px solid #D9534F"> -->
                                                                <a href="javascript:void(0)" id="add_email" onclick="add_email()" class="link-attr">Tambah Email</a>
                                                                <div class="alert alert-danger" style="margin-top: 10px;">
                                                                    <b>Email belum diisi!.</b>
                                                                </div>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                    <div class="form-group <?php echo (form_error('fullname')) ? 'has-error' : ''; ?>">
                                                        <label class="col-sm-3 control-label">Nama Lengkap</label>
                                                        <div class="col-sm-9">
                                                            <!-- <input name="fullname" type="text" required class="form-control" value="<?php //echo set_value('name', $customer->fullname); ?>"> -->
                                                            <div class="form-control-static">
                                                                <?php echo $customer->fullname ?>
                                                                <a href="javascript:void(0)" id="add_name" class="link-attr" onclick="add_name()" style="margin-left: 10px;">Ubah</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group <?php //echo (form_error('phone')) ? 'has-error' : ''; ?>">
                                                        <label class="col-sm-3 control-label">No. Handphone</label>
                                                        <div class="col-sm-9">
                                                            <div class="form-control-static">
                                                                <?php if($customer->phone) : ?>
                                                                    <!-- <input name="phone" type="text" required class="form-control" value="<?php //echo set_value('phone', $customer->phone); ?>"> -->
                                                                    <div class="phone_data" style="display: inline-block;">
                                                                        <?php echo $customer->phone ?>
                                                                    </div>
                                                                    <?php if($customer->verification_phone) : ?>
                                                                        <div class="phone_verification_label"><i><i class="fa fa-check" aria-hidden="true"></i> Terverifikasi</i></div>
                                                                    <?php else : ?>
                                                                        <a href="javascript:void(0)" id="verification_phone_link" onclick="add_phone('verif')" style="margin-left: 7px;">Verifikasi Sekarang!</a>
                                                                    <?php endif; ?>
                                                                <?php else : ?>
                                                                    <a href="javascript:void(0)" id="add_phone" onclick="add_phone()" class="link-attr">Tambah No Handphone</a>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group form-date <?php echo (form_error('birthday')) ? 'has-error' : ''; ?>">
                                                        <label class="col-sm-3 control-label">Tanggal Lahir</label>

                                                        <div class="col-sm-9">
                                                            <div class="form-control-static">
                                                                <?php if($customer->birthday) : ?>
                                                                    <?php if($customer->birthday == '0000-00-00') : ?>
                                                                        <a href="javascript:void(0)" id="edit_birthday" onclick="birthday_modal()">Tambah Tanggal Lahir</a>
                                                                    <?php else : ?>
                                                                        <?php echo date_indonesian($customer->birthday) ?>
                                                                        <a href="javascript:void(0)" id="edit_birthday" onclick="birthday_modal()" style="margin-left: 10px;" class="link-attr">Ubah</a>
                                                                    <?php endif; ?>
                                                                <?php else : ?>
                                                                    <a href="javascript:void(0)" id="edit_birthday" class="link-attr" onclick="birthday_modal()">Tambah Tanggal Lahir</a>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group <?php echo (form_error('gender')) ? 'has-error' : ''; ?>">
                                                        <label class="col-sm-3 control-label">Jenis Kelamin</label>
                                                        <div class="col-sm-9">
                                                            <div class="form-control-static">
                                                                <?php if($customer->gender) : ?>
                                                                    <?php echo ($customer->gender == 'L') ? 'Laki - Laki' : 'Perempuan' ?>
                                                                    <a href="javascript:void(0)" id="gender_link" class="link-attr" onclick="gender_modal()" style="margin-left: 10px;">Ubah</a>
                                                                <?php else : ?>
                                                                    <a href="javascript:void(0)" id="gender_link" class="link-attr" onclick="gender_modal()">Tambah Jenis Kelamin</a>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                        <!-- <div class="col-sm-6">
                                                            <label class="radio-inline">
                                                                <input type="radio" name="gender" value="L" <?php //echo set_radio('gender', 'L', ($customer->gender == 'L') ? true : false); ?>> Pria
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="gender" value="P" <?php //echo set_radio('gender', 'P', ($customer->gender == 'P') ? true : false); ?>> Wanita
                                                            </label>
                                                            <?php //echo form_error('gender'); ?>
                                                        </div> -->
                                                    </div>
                                                    <div class="form-group" style="margin-bottom: 0px;">
                                                        <label class="col-sm-3 control-label">Kode Referral</label>
                                                        <div class="col-sm-9">
                                                            <div class="form-control-static">
                                                                <b><?= $customer->referral_code ?></b>
                                                                <a href="javascript:void(0)" id="copy_code" style="margin-left: 10px;" class="link-attr" data-toggle="tooltip" data-placement="top" title="Salin" onclick="copy_code('<?= $customer->referral_code ?>', this)">Salin</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3"></label>
                                                        <div class="share-button-container col-sm-4">
                                                            <div class="share-button-title">Bagikan Kode Referral</div>
                                                            <div class="share-button-wrapper">
                                                                <a href="https://api.whatsapp.com/send?text=<?= 'Daftar Tokomanamana sekarang dan dapatkan voucher Rp50rb!' . '%0A%0A' . urlencode(site_url() . 'member/register?c=' . $user->referral_code) ?>" data-action="share/whatsapp/share" target="_blank" class="share-button" id="share-whatsapp">
                                                                    <i class="fa fa-whatsapp" aria-hidden="true"></i>
                                                                </a>
                                                                <div id="share-button-js-social"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- <div class="form-group">
                                                        <div class="col-sm-push-3 col-sm-6">
                                                            <button type="button" class="btn_save"><?php echo lang('button_save'); ?></button>
                                                        </div>
                                                    </div> -->
                                                </form>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade nav-white" id="norek" aria-labelledby="norek-tab">
                                            <div class="row" style="margin-top: 10px;">
                                                <!-- <div class="col-md-12" style="margin: 10px 0px;">
                                                    <span style="font-size: 18px; font-weight: bold;">Rekening Bank</span>
                                                    <div>Rekening Bank digunakan untuk proses penarikan Saldo Tokomanamana.</div>
                                                    <ul>
                                                        <li style="list-style: disc;margin-left: 17px;"> Anda hanya dapat mengubah rekening bank melalui Admin kami.</li>
                                                        <li style="list-style: disc;margin-left: 17px;"> Rekening Bank utama tidak bisa dihapus.</li>
                                                        <li style="list-style: disc;margin-left: 17px;"> Anda hanya dapat memiliki 3 rekening bank!.</li>
                                                    </ul>
                                                </div> -->
                                                <?php if($bank_accounts) : ?>
                                                    <?php if($bank_accounts->num_rows() < 3) : ?>
                                                        <div class="col-md-12" style="margin: 10px 0px;">
                                                            <button type="button" class="btn btn-norek" onclick="addBank();">Tambah Rekening</button>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                                <?php if($bank_accounts) : ?>
                                                    <?php if($bank_accounts->num_rows() > 0) : ?>
                                                        <div class="col-md-12">
                                                            <ul class="list-group" style="margin-top: 20px;">
                                                                <?php foreach($bank_accounts->result() as $bank_account) : ?>
                                                                    <li class="list-group-item">
                                                                        <div class="row">
                                                                            <div class="col-md-10">
                                                                                <div class="bank_name"><?php echo $bank_account->bank_name ?></div>
                                                                                <div class="bank_account_number"><?php echo $bank_account->bank_account_number ?> <?php echo ($bank_account->primary) ? '<span>Utama</span>' : '' ?></div>
                                                                                <div class="bank_account_name"><span>a.n</span> <?php echo $bank_account->bank_account_name ?></div>
                                                                            </div>
                                                                            <?php if(!$bank_account->primary) : ?>
                                                                                <div class="col-md-2">
                                                                                    <button class="btn_delete_account_bank" onclick="delete_account_bank('<?php echo encode($bank_account->id) ?>')">Hapus</button>
                                                                                </div>
                                                                            <?php endif; ?>
                                                                        </div>
                                                                    </li>
                                                                <?php endforeach; ?>
                                                            </ul>
                                                        </div>
                                                    <?php else : ?>
                                                        <div class="col-md-12">
                                                            <div class="alert alert-info">
                                                                <center><b>Anda belum menambah rekening bank. Silahkan tambah rekening untuk memudahkan anda berbelanja!</b></center>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php else : ?>
                                                    <div class="col-md-12">
                                                        <div class="alert alert-info">
                                                            <center><b>Anda belum menambah rekening bank. Silahkan tambah rekening untuk memudahkan anda berbelanja!</b></center>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>

                        </div>
            </div>
        </div>
    </div>
</div>
<!-- modal email -->
<div class="modal fade" id="add_email_modal" tabindex="-1" role="dialog" aria-labelledby="add_email_label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h1>Email</h1>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -37px;">
            <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="modal_email_body">
          <form action="<?php echo site_url('auth/member/save_email') ?>" id="save_email_customer">
              <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 message_edit">
                        <?php if($customer->phone == '') : ?>
                            <div class="alert alert-danger" id="message_edit_email">
                                <b>Tambah No Handphone terlebih dahulu!</b>
                            </div>
                        <?php elseif($customer->verification_phone != 1) : ?>
                            <div class="alert alert-danger" id="message_edit_email">
                                <b>Verifikasi No Handphone terlebih dahulu!</b>
                            </div>
                        <?php elseif($customer->email) : ?>
                            <div class="alert alert-warning" id="message_edit_email">
                                <b>Untuk mengganti alamat email, anda harus memverifikasinya terlebih dahulu dengan kode yang dikirim melalui sms!</b>
                            </div>
                        <?php endif; ?>
                    </div>
                    <?php if($customer->verification_phone == 1 && $customer->phone) : ?>
                        <div class="col-md-12 message_send_verification"></div>
                        <div class="col-md-12 message_error_email"></div>
                        <div class="col-md-12 form-group">
                            <input type="text" class="form-control" name="email" placeholder="Masukkan email...">
                            <div class="error_validation" id="error_email"></div>
                        </div>
                        <div class="col-md-7 form-group">
                            <!-- <input type="text" class="form-control" name="email" placeholder="Masukkan email...">
                            <div class="error_validation" id="error_email"></div> -->
                            <input type="text" class="form-control" name="code_verification" placeholder="Masukkan kode verifikasi no Handphone">
                            <div class="error_validation" id="error_code_verification"></div>
                        </div>
                        <div class="col-md-5 form-group" id="action_button">
                            <?php if($customer->verification_phone && $customer->phone) : ?>
                                <div id="button_request_verification">
                                    <button type="button" id="button_edit_email_verification" class="button_edit_email_verification" data-otp="true">Minta Kode Verifikasi</button>
                                </div>
                            <?php else : ?>
                                <div id="button_request_verification">
                                    <button type="button" id="button_email_disabled" class="button_email_disabled" data-otp="false" disabled="">Minta Kode Verifikasi</button>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                </div>
              </div>

              <div class="modal-footer">
                <?php if($customer->verification_phone == 1 && $customer->phone) : ?>
                    <button type="button" class="btn btn-secondary btn_cancel" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary btn_save">Simpan</button>
                <?php else : ?>
                    <button type="button" class="btn btn-secondary btn_cancel" data-dismiss="modal">Tutup</button>
                <?php endif; ?>
              </div>
          </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="input_code_verification_email_modal" tabindex="-1" role="dialog" aria-labelledby="input_code_verification_email_label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h1>Masukkan Kode Verifikasi Email</h1>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -37px;">
            <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo site_url('auth/member/input_code_verification_email') ?>" id="form_input_code_verification_email">
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12 message_email_verification"></div>
                <div class="col-md-12 form-group">
                    <input type="text" class="form-control" name="code_verification" placeholder="Masukkan kode verifikasi...">
                    <div class="error_validation" id="error_code_verification"></div>
                </div>
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn_cancel" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary btn_save">Kirim</button>
          </div>
      </form>
    </div>
  </div>
</div>

<!-- modal name -->
<div class="modal fade" id="add_name_modal" tabindex="-1" role="dialog" aria-labelledby="add_name_modal_label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h1>Nama</h1>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -37px;">
            <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo site_url('auth/member/save_name') ?>" id="save_name_customer">
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12 message_edit"></div>
                <div class="col-md-12 form-group">
                    <input type="text" class="form-control" name="name" placeholder="Masukkan nama...">
                    <div class="error_validation" id="error_name"></div>
                </div>         
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn_cancel" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary btn_save">Simpan</button>
          </div>
      </form>
    </div>
  </div>
</div>

<!-- birthday modal -->
<div class="modal fade" id="birthday_modal" tabindex="-1" role="dialog" aria-labelledby="birthday_modal_label" aria-hidden="true">
  <div class="modal-dialog" role="document" id="birthday_modal_dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1>Tanggal Lahir</h1>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -37px;">
            <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo site_url('auth/member/save_birthday') ?>" id="save_birthday" method="POST">
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12 message_edit"></div>
                <div class="col-md-12 message_error"></div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-2 col-xs-3" style="<?php echo ($this->agent->is_mobile()) ? 'padding: 0px 3px;' : '' ; ?>">
                            <select name="day" class="form-control">
                                <?php
                                for ($i = 1; $i <= 31; $i++) {
                                    echo '<option value="' . $i . '" ' . set_select('day', $i, (substr($customer->birthday, 8, 3)) == $i ? true : false) . '>' . $i . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-sm-5 col-xs-6" style="<?php echo ($this->agent->is_mobile()) ? 'padding: 0px 3px;' : '' ; ?>">
                            <select name="month" class="form-control">
                                <option value="01" <?php echo set_select('month', '01', (substr($customer->birthday, 5, 2)) == 1 ? true : false); ?>>Januari</option>
                                <option value="02" <?php echo set_select('month', '02', (substr($customer->birthday, 5, 2)) == 2 ? true : false); ?>>Februari</option>
                                <option value="03" <?php echo set_select('month', '03', (substr($customer->birthday, 5, 2)) == 3 ? true : false); ?>>Maret</option>
                                <option value="04" <?php echo set_select('month', '04', (substr($customer->birthday, 5, 2)) == 4 ? true : false); ?>>April</option>
                                <option value="05" <?php echo set_select('month', '05', (substr($customer->birthday, 5, 2)) == 5 ? true : false); ?>>Mei</option>
                                <option value="06" <?php echo set_select('month', '06', (substr($customer->birthday, 5, 2)) == 6 ? true : false); ?>>Juni</option>
                                <option value="07" <?php echo set_select('month', '07', (substr($customer->birthday, 5, 2)) == 7 ? true : false); ?>>Juli</option>
                                <option value="08" <?php echo set_select('month', '08', (substr($customer->birthday, 5, 2)) == 8 ? true : false); ?>>Agustus</option>
                                <option value="09" <?php echo set_select('month', '09', (substr($customer->birthday, 5, 2)) == 9 ? true : false); ?>>September</option>
                                <option value="10" <?php echo set_select('month', '10', (substr($customer->birthday, 5, 2)) == 10 ? true : false); ?>>Oktober</option>
                                <option value="11" <?php echo set_select('month', '11', (substr($customer->birthday, 5, 2)) == 11 ? true : false); ?>>November</option>
                                <option value="12" <?php echo set_select('month', '12', (substr($customer->birthday, 5, 2)) == 12 ? true : false); ?>>Desember</option>
                            </select>
                            <?php ?>
                        </div>
                        <div class="col-sm-3 col-xs-3" style="<?php echo ($this->agent->is_mobile()) ? 'padding: 0px 3px;' : '' ; ?>">
                            <select name="year" class="form-control">
                                <?php
                                for ($i = 1969; $i <= date('Y'); $i++) {
                                    echo '<option value="' . $i . '" ' . set_select('year', $i, (substr($customer->birthday, 0, 4)) == $i ? true : false) . '>' . $i . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn_cancel" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary btn_save">Simpan</button>
          </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Gender -->
<div class="modal fade" id="gender_modal" tabindex="-1" role="dialog" aria-labelledby="gender_modal_label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h1>Pilih Jenis Kelamin</h1>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -37px;">
            <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo site_url('auth/member/save_gender') ?>" id="save_gender">
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12 message_error"></div>
                <div class="col-md-3"></div>
                <div class="col-md-3 form-group">
                    <div class="radio">
                        <input type="radio" name="gender" id="man" value="L" <?php echo ($customer->gender == 'L') ? 'checked' : '' ?> ><label for="man">Laki - Laki</label>
                    </div>
                </div>
                <div class="col-md-3 form-group">
                    <div class="radio">
                        <input type="radio" name="gender" id="woman" value="P"><label for="woman" <?php echo ($customer->gender == 'P') ? 'checked' : '' ?> >Perempuan</label>
                    </div>
                </div>         
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn_cancel" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary btn_save">Simpan</button>
          </div>
      </form>
    </div>
  </div>
</div>

<!-- Bank Modal -->
<div class="modal fade" id="AddBank">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <br>
          <center><h4 class="modal-title" style="<?php echo ($this->agent->is_mobile()) ? 'font-size: 150%' : '' ; ?>">Tambah Rekening</h4>
          <div>Pastikan Nomor Rekening & Nama Pemilik Rekening Sesuai Buku Tabungan</div>
          </center>
        </div>
        <form action="#">
            <div class="modal-body" style="margin: 15px;">
              <div class="form-group">
                  <label>Nama Bank</label>
                  <select onchange="cek_norek()" id="bank_code" name="bank_code" style="width: 100%; color: #000" class="form-group js-example-basic-single">
                    <option value="" selected="" disabled="">Pilih Bank <i class="fa fa-home"></i></option>
                    <?php
                    foreach ($bank->result() as $b) { ?>
                        <option value="<?php echo $b->code ?>"><?= $b->name ?></option>
                    <?php } ?>
                  </select>
              </div>
              <div class="form-group">
                  <label>Cabang</label>
                  <input oninput="cek_norek()" id="bank_branch" name="bank_branch" style="width: 100%" type="text" name="bank_branch" class="form-group" required="">
              </div>
              <div class="form-group">
                  <label>Nomor Rekening</label>
                  <input oninput="cek_norek()" onkeypress="validate(event)" id="bank_account_number" name="bank_account_number" style="width: 100%" type="text" name="account_number" class="form-group" required="">
              </div>
              <div class="form-group">
                  <label>Nama Pemiliki Rekening</label>
                  <input oninput="cek_norek()" id="bank_account_name" name="bank_account_name" style="width: 100%" type="text" name="account_name" class="form-group" required="">
              </div>
            </div>
            <input type="hidden" id="cek_input">
            <div class="modal-footer">
              <button type="button" class="btn btn-cancel-bank" data-dismiss="modal">Batal</button>
              <button type="button" id="btn-submit-bank" onclick="submit_bank()" disabled="" class="btn btn-submit-bank" data-dismiss="modal">Simpan</button>
            </div>
        </form>
      </div>
    </div>
</div>

<!-- Phone Modal -->
<div class="modal fade" id="add_phone_modal" tabindex="-1" role="dialog" aria-labelledby="add_phone_modal_label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <?php if($customer->phone == '' || $customer->verification_phone == 0) : ?>
        <div class="modal-content">
          <div class="modal-header">
            <?php if($customer->phone == '') : ?>
                <h1>Tambah No Handphone</h1>
            <?php else : ?>
                <h1>Verifikasi No Handphone</h1>
            <?php endif; ?>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -37px;">
                <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form action="<?php echo site_url('auth/member/save_phone') ?>" id="save_phone_customer">
              <div class="modal-body">
                <div class="row">
                    <?php if($customer->phone == '') : ?>
                        <div class="col-md-12">
                            <div class="alert alert-warning">
                                <b>No Handphone yang sudah disimpan tidak dapat diubah lagi!</b>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="col-md-12 message_save_phone"></div>
                    <div class="col-md-12 form-group">
                        <?php if($customer->phone == '') : ?>
                            <input type="number" class="form-control" id="input_phone" name="phone" placeholder="Masukkan No Handphone...">
                            <div class="error_validation" id="error_phone"></div>
                        <?php else : ?>
                            <span class="form-control" style="border-radius: 3px;"><?php echo $customer->phone ?></span>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-7 form-group">
                        <input type="text" class="form-control" name="code_verification" placeholder="Masukkan kode verifikasi no Handphone">
                        <div class="error_validation" id="error_code_verification"></div>
                    </div>
                    <div class="col-md-5 form-group">
                        <?php if($customer->phone == '') : ?>
                            <button type="button" id="button_send_otp_no_phone" class="button_send_otp_no_phone" data-otp="true" onclick="request_verification_code(this)">Minta Kode Verifikasi</button>
                        <?php else : ?>
                            <button type="button" id="button_send_otp_no_phone" class="button_send_otp_no_phone" data-otp="true" onclick="request_verification_code(this, 'verif')">Minta Kode Verifikasi</button>
                        <?php endif; ?>
                    </div>
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn_cancel" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary btn_save">Simpan</button>
              </div>
          </form>
        </div>
    <?php endif; ?>
  </div>
</div>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();   
		$("#share-button-js-social").jsSocials({
			showLabel: false,
			showCount: false,
			shares: ["twitter", "facebook"],
			url: site_url + 'member/register?c=' + referral_code,
			text: 'Daftar Tokomanamana sekarang dan dapatkan voucher Rp50rb!',
			shareIn: "popup"
		});
	});
</script>