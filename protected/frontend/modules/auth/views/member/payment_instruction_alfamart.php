<p>Lakukan pembayaran melalui Alfamart dengan detail sebagai berikut:</p>
<div class="panel payment-instruction">
    <div class="panel-heading">
        <div class="row">
            <div class="col-xs-3">
                <div class="payment-instruction-label">TOTAL</div>
                <div class="payment-instruction-content"><strong><?php echo rupiah($data->total); ?></strong></div>
            </div>
        </div>
    </div>
    <div class="panel-heading">
        <div class="payment-instruction-label">KODE PEMBAYARAN</div>
        <div class="payment-instruction-content"><strong><?php echo $payment->account_number; ?></strong></div>
    </div>
    <div class="panel-body">
        <div class="payment-instruction-label">PETUNJUK PEMBAYARAN MELALUI ALFAMART</div>
        <div class="panel payment-instruction-sub">
            <div class="panel-body">
                <div class="payment-instruction-sub-container">
                    <div class="step-info-column-number">
                        <div class="textbox-circle-outer-green">
                            <div class="textbox-circle-green">
                                <div class="textbox-circle-num">1</div>
                            </div>
                        </div>
                    </div>
                    <div class="step-info-column-text">
                        <div class="description-step-info">Kunjungi Alfamart atau Alfamidi terdekat.</div>
                    </div>
                    <div class="clearance">&nbsp;</div>
                    <div class="step-info-column-number">
                        <div class="textbox-circle-outer-green">
                            <div class="textbox-circle-green">
                                <div class="textbox-circle-num">2</div>
                            </div>
                        </div>
                    </div>
                    <div class="step-info-column-text">
                        <div class="description-step-info">Beritahu kepada kasir bahwa anda ingin membayar ke "XENDIT".</div>
                    </div>
                    <div class="clearance">&nbsp;</div>
                    <div class="step-info-column-number">
                        <div class="textbox-circle-outer-green">
                            <div class="textbox-circle-green">
                                <div class="textbox-circle-num">3</div>
                            </div>
                        </div>
                    </div>
                    <div class="step-info-column-text">
                        <div class="description-step-info">Apabila kasir tidak mengenali nama "XENDIT", maka tunjukkanlah nama "XENDIT" kepada kasir tersebut.</div>
                    </div>
                    <div class="clearance">&nbsp;</div>
                    <div class="step-info-column-number">
                        <div class="textbox-circle-outer-green">
                            <div class="textbox-circle-green">
                                <div class="textbox-circle-num">4</div>
                            </div>
                        </div>
                    </div>
                    <div class="step-info-column-text">
                        <div class="description-step-info">Beritahu kode pembayaran <b><?php echo $payment->account_number; ?></b> kepada kasir.</div>
                    </div>
                    <div class="clearance">&nbsp;</div>
                    <div class="step-info-column-number">
                        <div class="textbox-circle-outer-green">
                            <div class="textbox-circle-green">
                                <div class="textbox-circle-num">5</div>
                            </div>
                        </div>
                    </div>
                    <div class="step-info-column-text">
                        <div class="description-step-info">Lakukanlah pembayaran Anda sesuai dengan jumlah yang tertera di invoice.</div>
                    </div>
                    <div class="clearance">&nbsp;</div>
                    <div class="step-info-column-number">
                        <div class="textbox-circle-outer-green">
                            <div class="textbox-circle-green">
                                <div class="textbox-circle-num">6</div>
                            </div>
                        </div>
                    </div>
                    <div class="step-info-column-text">
                        <div class="description-step-info">Terimalah tanda terima pembayaran dari Alfamart sebagai bukti pembayaran</div>
                    </div>
                    <div class="clearance">&nbsp;</div>
                    <div class="step-info-column-number">
                        <div class="textbox-circle-outer-green">
                            <div class="textbox-circle-green">
                                <div class="textbox-circle-num">7</div>
                            </div>
                        </div>
                    </div>
                    <div class="step-info-column-text">
                        <div class="description-step-info">Selamat! Pembayaran Anda sudah selesai.</div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>