<style>
    .collection-mainarea .add_address{border-radius:3px;background:#a7c22a;border:1px solid #a7c22a;text-transform:capitalize;font-weight:700;font-size:14px;color:#fff}.collection-mainarea .add_address:hover{color:#fff!important;background:#8ea623;border:1px solid #8ea623}.collection-mainarea .add_address:hover i{color:#fff!important}.collection-mainarea .add_address:focus{border-color:none;background-color:none!important;background-image:none;outline:0}.collection-mainarea .no_address{text-align:center;font-weight:700;font-size:15px}#modal-address .modal-header h1{font-weight:700;font-size:15px}#address .alert{border-radius:5px}#modal-address input,#modal-address select{background-image:none;background-color:transparent;-webkit-box-shadow:none;-moz-box-shadow:none;box-shadow:none;border:1px solid #ccc;border-radius:3px;outline:0}#modal-address input:focus,#modal-address select:focus{border-color:#a7c22a!important;background-color:none!important;background-image:none;outline:0;box-shadow:none}#modal-address .modal-footer button{border-radius:3px;background:#a7c22a;border:1px solid #a7c22a;text-transform:capitalize;font-weight:700;font-size:14px;color:#fff}#modal-address .modal-footer button:focus{border-color:none;background-color:none!important;background-image:none;outline:0}#modal-address button.btn_cancel{background:#fff!important;border:1px solid #ccc!important;color:#888!important;font-weight:400}#modal-address button.btn_cancel:hover{background-color:#eee!important}#modal-address button.btn_save:hover{color:#fff!important;background:#8ea623;border:1px solid #8ea623}.error_validation span{color:#d9534f}.my-account .address_list .panel .panel-footer a{border:1px solid #ccc!important;color:#888!important;padding:7px 15px;border-radius:3px}.my-account .address_list .panel .panel-footer a:hover{background-color:#eee!important}#modal-address .alert{border-radius:5px}#modal-address .modal-content{border-radius:5px;border:none}#primary_address{background:#a7c22a!important;border:1px solid #a7c22a!important;color:#fff!important;font-weight:700!important;cursor:default}.lds-ring{display:inline-block;position:relative;width:20px;height:20px;left:50%}.lds-ring div{box-sizing:border-box;display:inline-block;position:absolute;width:20px;height:20px;margin:auto;border:3px solid #a7c22a;border-radius:50%;animation:lds-ring 1.2s cubic-bezier(.5,0,.5,1) infinite;border-color:#a7c22a transparent transparent transparent;left:0;top:0}.lds-ring div:nth-child(1){animation-delay:-.45s}.lds-ring div:nth-child(2){animation-delay:-.3s}.lds-ring div:nth-child(3){animation-delay:-.15s}@keyframes lds-ring{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}@media (max-width:427px){#address .panel-footer a{margin-right:2px;padding:5px 10px}}@media (max-width:373px){#address .panel-footer a{margin-right:2px;padding:5px 7px}}@media (max-width:355px){#address .panel-footer a{margin-right:2px;padding:5px 5px}}@media (max-width:343px){#address .panel-footer a{margin-right:0;padding:5px 5px}}@media (max-width:337px){#address .panel-footer a{padding:5px 7px;font-size:10px}}.collection-title{font-size:18px!important;color:#a7c22a!important;font-weight:700!important}
</style>
<div class="my-account" id="address">
    <section class="collection-heading heading-content ">
        <div class="container">
            <div class="row">
                <div class="col-sm-9 col-sm-push-3" style="padding-left: 0px; padding-right: 0px;">
                    <div class="collection-wrapper block" style="padding: 10px 15px;background: none;">
                        <h1 class="collection-title"><span><?php echo lang('account_address'); ?></span></h1>
                        <?php echo $breadcrumb; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container" style="margin-bottom: 30px;">
        <div class="row">
            <?php echo $this->load->view('navigation'); ?>
            <div class="collection-mainarea col-md-9 col-sm-8 col-xs-12 address_list">
                <div class="col-md-12 col-sm-12" style="margin-bottom: 15px;<?php echo (!$this->agent->is_mobile()) ? 'padding-left: 0px;padding-right: 0px;' : '' ; ?>">
                    <button class="btn add_address" type="button" onclick="add()" style="<?php echo ($this->agent->is_mobile()) ? 'width: 100%;' : '' ; ?>"><i class="fa fa-plus-circle" aria-hidden="true"></i> Tambah Alamat</button>
                </div>
                <div id="address_list">
                    <div id="customer_address_list">
                        <?php if ($addresses->num_rows() > 0) foreach($addresses->result() as $address){ ?>
                            <div class="col-sm-12" id="address-<?php echo encode($address->id); ?>" <?php echo (!$this->agent->is_mobile()) ? 'style="padding-right: 0px; padding-left: 0px;"' : '' ; ?>>
                                <div class="panel">
                                    <div class="panel-heading">
                                        <span class="header-label"><?php echo $address->title; ?></span>
                                        <span><?php echo $address->name.' - '.$address->phone; ?></span>
                                    </div>
                                    <div class="panel-body">
                                        <?php echo nl2br($address->address); ?>
                                        <?php echo $address->province_name.', '.$address->city_name.', '.$address->district_name.' '.$address->postcode; ?>
                                    </div>
                                    <div class="panel-footer">
                                        <?php if($address->primary == 0) : ?>
                                            <a href="javascript:void(0)" onclick="primary_address('<?php echo encode($address->id) ?>')"><i class="fa fa-home" aria-hidden="true"></i> Jadikan Alamat Utama</a>
                                        <?php else : ?>
                                            <a href="javascript:void(0)" id="primary_address"><i class="fa fa-home" aria-hidden="true"></i> Alamat Utama</a>
                                        <?php endif; ?>
                                        <a href="javascript:void(0);" onclick="edit('<?php echo encode($address->id); ?>')"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Ubah</a>
                                        <a href="javascript:void(0);" onclick="remove('<?php echo encode($address->id); ?>')"><i class="fa fa-trash-o" aria-hidden="true"></i> Hapus</a>
                                    </div>
                                </div>
                            </div>
                        <?php } else {
                            ?>
                                <div class="col-sm-12 col-md-12 no_address" <?php echo (!$this->agent->is_mobile()) ? 'style="padding-right: 0px; padding-left: 0px;"' : '' ; ?>>
                                    <div class="alert alert-warning" role="alert">
                                        Tidak ada alamat!. Silahkan tambah alamat terlebih dahulu!
                                    </div>
                                </div>
                            <?php
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="modal-address" class="modal" role="dialog" aria-hidden="true" tabindex="-1" data-width="800" style="display: none;">
    <div class="modal-dialog fadeIn animated">
        <div class="modal-content">
            <form action="<?php echo site_url('auth/member/save_address') ?>" method="POST" id="form_address">
                <div class="modal-header">
                    <h1>Tambah Alamat</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -37px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="message"></div>
                    <?php 
                    //foreach($addresses->result() as $alamat){ ?>
                    <!--input type="hidden" name="id" value="<?php //echo $alamat->customer; ?>"-->
                    <?php //} ?>
                    <input type="hidden" name="id">
                    
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Simpan Alamat sebagai</label>
                                <input name="title" type="text" class="form-control" >
                                <div id="error_title" class="error_validation"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Nama Penerima</label>
                                <input name="name" type="text" class="form-control">
                                <div id="error_name" class="error_validation"></div>
                                <input type="hidden" id="temp-lat" name="latitude">
                                <input type="hidden" id="temp-lng" name="longitude">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">No. Handphone</label>
                                <input name="phone" type="text" class="form-control">
                                <div id="error_phone" class="error_validation"></div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Kode Pos</label>
                                <input name="postcode" id="postcode" type="text" onchange="test()" class="form-control">
                                <div id="error_postcode" class="error_validation"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Provinsi</label>
                                <select name="province" id="province" class="form-control">
                                    <option value="">Pilih provinsi</option>
                                    <?php if ($provincies) { ?>
                                        <?php foreach ($provincies->result() as $province) { ?>
                                            <option value="<?php echo $province->id; ?>"><?php echo $province->name; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                                <div id="error_province" class="error_validation"></div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Kota</label>
                                <select name="city" id="city" class="form-control">
                                    <option value="">Pilih kota</option>
                                </select>
                                <div class="error_validation" id="error_city"></div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Kecamatan</label>
                                <select name="district" id="district" class="form-control">
                                    <option value="">Pilih kecamatan</option>
                                </select>
                                <div class="error_validation" id="error_district"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Alamat</label>
                                <input name="address" type="text" class="form-control">
                                <div class="error_validation" id="error_address"></div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Pin Alamat (Optional)</label>
                                <input style="height:80px; background-image:url(<?php //echo base_url()?>assets/frontend/images/maps.jpg)" type="text" class="form-control">
                                <img src="<?php //echo base_url()?>assets/frontend/images/maps.jpg" style="width:100%; height:80px; ">
                                <a href="javscript:void(0);" onclick="add_pin()" style="margin-top:-60px; margin-right:15px; border-radius:3px; z-index:10000;" class="pull-right btn">Pin Point</a>
                            </div>
                        </div>
                    </div> -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn_cancel" data-dismiss="modal" style="background-color: #FFF; color: #000;">Batal</button>
                    <button type="submit" class="btn btn_save">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>