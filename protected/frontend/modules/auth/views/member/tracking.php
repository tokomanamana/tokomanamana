<?php
if ($tracking->status->code == 400) {
    echo '<p>' . $tracking->status->description . '</p>';
} else {
    $summary = $tracking->result->summary;
    $detail = $tracking->result->details;
    $delivery_status = $tracking->result->delivery_status;
    $manifest = $tracking->result->manifest;
    ?>
    <div class="row">
        <div class="col-sm-12">
            <ul class="list list-unstyled">
                <li><strong>No Resi : <?php echo $summary->waybill_number; ?></strong></li>
                <li>Tanggal di Kirim : <strong><?php echo $summary->waybill_date; ?></strong></li>
                <li>Service Code : <strong><?php echo $summary->service_code; ?></strong></li>
                <?php if ($tracking->result->delivered == true) { ?>
                    <li>Penerima : <span class="text-bold"><?php echo $delivery_status->pod_receiver; ?></span></li>
                <?php } ?>
            </ul>
            <div class="alert alert-<?php echo ($tracking->result->delivered) ? 'success' : 'warning'; ?> no-border">
                <span class="text-bold">Status : <?php echo $delivery_status->status; ?></span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <ul class="list list-unstyled">
                <li><strong>Nama Pengirim :</strong></li>
                <li><?php echo $detail->shippper_name; ?></li>
                <li><strong>Kota Pengirim :</strong></li>
                <li><?php echo $detail->shipper_city; ?></li>
            </ul>
        </div>
        <div class="col-sm-6">
            <ul class="list list-unstyled">
                <li><strong>Nama Penerima :</strong></li>
                <li><?php echo $detail->receiver_name; ?></li>
                <li><strong>Kota Penerima :</strong></li>
                <li><?php echo $detail->receiver_city; ?></li>
            </ul>
        </div>
    </div>
    <?php
    if (count($manifest) > 0) {
        krsort($manifest);
        ?>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th style="background: #d1ccc9;">Status</th>
                    <th style="background: #d1ccc9;">Waktu</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($manifest as $m) { ?>
                    <tr>
                        <td><?php echo $m->manifest_description; ?></td>
                        <td><?php echo $m->manifest_date . '<br>' . $m->manifest_time; ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    <?php } ?>
<?php } ?>