<style type="text/css">
    .left-tab{padding:4px 8px;font-size:14px}.left-tab:hover{background:#a7c22a;color:#fff;border-radius:5px}.active{background:#a7c22a;border-radius:5px}.active>a{color:#fff!important}.left-tab-out{padding:4px 8px;font-size:14px;color:#d9534f;font-weight:700}.left-tab-out:hover{background:#d9534f;color:#fff;border-radius:5px}
</style>
<div id="tags-load" style="display:none;"><i class="fa fa-spinner fa-pulse fa-2x"></i></div>
<div class="collection-leftsidebar sidebar col-sm-3 clearfix">
    <div class="sidebar-block collection-block">
        <div class="sidebar-title">
            <span>Akun Saya</span>
            <i class="fa fa-caret-down show_sidebar_content" aria-hidden="true"></i>
        </div>
        <div class="sidebar-content">
            <ul class="list-cat">
                <?php
                    $total_point = 0;
                    $this->load->model('Model');

                    $q_point = $this->Model->get_data('point', 'point', null, array('id_customer' => $this->session->userdata('user_id')));

                    if($q_point->num_rows() > 0){
                        $total_point = $q_point->row()->point;
                    }
                ?>
                <li class="<?php echo ($page == 'member/history') ? 'active' : ''; ?>"><a href="<?php echo site_url('member/history'); ?>"><div class="left-tab"><?php echo lang('account_order_history'); ?><?php echo ($total_active_order) ? '<span class="badge badge-notification" style="margin-top: 0;">' . $total_active_order . '</span>' : ''; ?></div></a></li>
                <!-- <li class="<?php echo ($page == 'member/point') ? 'active' : ''; ?>"><a href="<?php echo site_url('member/point'); ?>">Poin Saya</a> <?php echo ($total_point) ? '<span class="badge badge-notification" style="margin-top: 0;">' . $total_point . '</span>' : ''; ?></li> -->
                <li class="<?php echo ($page == 'member/balance') ? 'active' : ''; ?>"><a href="<?php echo site_url('member/balance'); ?>"><div class="left-tab"><?php echo lang('account_balance') ?></div></a></li>
                <li class="<?php echo ($page == 'member/profile') ? 'active' : ''; ?>"><a href="<?php echo site_url('member/profile'); ?>"><div class="left-tab"><?php echo lang('account_profile'); ?></div></a></li>
                <li class="<?php echo ($page == 'member/voucher-saya') ? 'active' : '' ?>"><a href="<?php echo site_url('member/voucher-saya') ?>"><div class="left-tab">Voucher Saya</div></a></li>
                <li class="<?php echo ($page == 'member/wishlist') ? 'active' : ''; ?>"><a href="<?php echo site_url('member/wishlist'); ?>"><div class="left-tab">Produk Favorit</div></a></li> 
                <li class="<?php echo ($page == 'member/favourite_shop') ? 'active' : ''; ?>"><a href="<?php echo site_url('member/favourite_shop'); ?>"><div class="left-tab">Toko Favorit</div></a></li> 
                <li class="<?php echo ($page == 'member/review') ? 'active' : ''; ?>"><a href="<?php echo site_url('member/review'); ?>"><div class="left-tab"><?php echo lang('account_review'); ?></div></a></li>
                <li class="<?php echo ($page == 'member/address') ? 'active' : ''; ?>"><a href="<?php echo site_url('member/address'); ?>"><div class="left-tab"><?php echo lang('account_address'); ?></div></a></li>
                <li class="<?php echo ($page == 'member/security') ? 'active' : ''; ?>"><a href="<?php echo site_url('member/security'); ?>"><div class="left-tab"><?php echo lang('account_security'); ?></div></a></li>
                <li><a href="<?php echo site_url('member/logout'); ?>"><div class="left-tab-out">Keluar</div></a></li>


<!--<li class="<?php echo ($page == 'member/payment_confirmation') ? 'active' : ''; ?>"><a href="<?php echo site_url('member/payment_confirmation'); ?>"><?php echo lang('account_text_payment_confirmation'); ?></a></li>-->
            </ul>
        </div>
        <select class="form-control sidebar-content-select" onchange="window.location.href = this.value">
            <option value="<?php echo site_url('member/history'); ?>" <?php echo ($page == 'member/history') ? 'selected' : ''; ?>><?php echo lang('account_order_history'); ?></option>
            <!-- <option value="<?php //echo site_url('member/point'); ?>" <?php //echo ($page == 'member/point') ? 'selected' : ''; ?>>Poin Saya</option> -->
            <option value="<?php echo site_url('member/balance'); ?>" <?php echo ($page == 'member/balance') ? 'selected' : ''; ?>><?php echo lang('account_balance'); ?></option>
            <option value="<?php echo site_url('member/profile'); ?>" <?php echo ($page == 'member/profile') ? 'selected' : ''; ?>><?php echo lang('account_profile'); ?></option>
            <option value="<?php echo site_url('member/wishlist'); ?>" <?php echo ($page == 'member/wishlist') ? 'selected' : ''; ?>>Produk Favorit</option>
            <option value="<?php echo site_url('member/review'); ?>" <?php echo ($page == 'member/review') ? 'selected' : ''; ?>><?php echo lang('account_review'); ?></option>
            <option value="<?php echo site_url('member/address'); ?>" <?php echo ($page == 'member/address') ? 'selected' : ''; ?>><?php echo lang('account_address'); ?></option>
            <option value="<?php echo site_url('member/security'); ?>" <?php echo ($page == 'member/security') ? 'selected' : ''; ?>><?php echo lang('account_security'); ?></option>
        </select>
    </div>										
</div>