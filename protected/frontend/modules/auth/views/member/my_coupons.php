<link rel="stylesheet" href="<?= site_url('assets/frontend/css/minified/my_coupons.min.css') ?>">
<?php 

    function tglIndo($tanggal){
        $bulan = array (
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
        );
        
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
        
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }

?>

<div class="my-account">
    <section class="collection-heading heading-content ">
        <div class="container">
            <div class="row">
                <div class="col-sm-9 col-sm-push-3">
                    <div class="collection-wrapper">
                        <h1 class="collection-title"><span>Voucher Saya</span></h1>
                        <?php echo $breadcrumb; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="container" style="margin-bottom: 30px;">
        <div class="row">
            <?php echo $this->load->view('navigation'); ?>
            <div class="collection-mainarea col-md-9 col-sm-8 col-xs-12">
                <div class="block">
                    <div class="row">
                        <?php if ($coupons && $coupons->num_rows() > 0) : ?>
                            <?php foreach($coupons->result() as $key => $coupon) : ?>
                                <div class="col-md-6 col-sm-12 card-wrapper" <?= ($this->agent->is_mobile()) ? 'style="margin-bottom: 20px;"' : '' ?>>
                                    <div class="card card-coupon">
                                        <img src="<?= base_url('assets/frontend/images/loading.gif') ?>" alt="voucher-pic-<?= $key ?>" class="card-img-top lazyload" data-src="<?= base_url('assets/frontend/images/voucher-pic.jpg') ?>" />
                                        <?php if($this->agent->is_mobile()) : ?>
                                            <span class="coupon-title-mobile"><?= $coupon->name ?> <br><span class="coupon-title-secondary">Diskon <?= rupiah($coupon->discount) ?></span></span>
                                        <?php else : ?>
                                            <span class="coupon-title"><?= $coupon->name ?> <br><span class="coupon-title-secondary">Diskon <?= rupiah($coupon->discount) ?></span></span>
                                        <?php endif; ?>
                                        <div class="card-body">
                                            <div class="row">
                                                <?php if($this->agent->is_mobile()) : ?>
                                                    <div class="col-md-6 card-body-mobile">
                                                        <span>Berlaku hingga</span>
                                                        <span class="date-end-coupon"><?= tglIndo(date('Y-m-d', strtotime($coupon->date_end))) ?></span>
                                                    </div>
                                                    <div class="col-md-6 card-body-mobile">
                                                        <span>Minimum Transaksi</span>
                                                        <span><?= ($coupon->min_order > 0) ? rupiah($coupon->min_order) : 'Tidak Ada' ?></span>
                                                    </div>
                                                <?php else : ?>
                                                    <div class="col-md-6 card-body-desktop">
                                                        <span>Berlaku hingga</span>
                                                        <span class="date-end-coupon"><?= tglIndo(date('Y-m-d', strtotime($coupon->date_end))) ?></span>
                                                    </div>
                                                    <div class="col-md-6 card-body-desktop">
                                                        <span>Minimum Transaksi</span>
                                                        <span><?= ($coupon->min_order > 0) ? rupiah($coupon->min_order) : 'Tidak Ada' ?></span>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                            <div class="row" style="margin-top: 10px;">
                                                <?php if($this->agent->is_mobile()) : ?>
                                                    <div class="col-md-12 copy-coupon" style="padding: 0;">
                                                        <span style="font-size: 11px;">Kode Voucher: <br> <b><?= $coupon->code ?></b></span>
                                                        <button onclick="copy_code('<?= $coupon->code ?>', this)" class="btn-copied btn-copied-mobile" style="font-size: 11px;right: 5px;">Salin Kode</button>
                                                    </div>
                                                <?php else : ?>
                                                    <div class="col-md-12 copy-coupon">
                                                        Kode Voucher: &nbsp; <b><?= $coupon->code ?></b>
                                                        <button onclick="copy_code('<?= $coupon->code ?>', this)" class="btn-copied" data-toggle="tooltip" data-placement="top" title="Salin Kode">Salin Kode</button>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="card-footer row">
                                            <?php if($this->agent->is_mobile()) : ?>
                                                <div class="col-md-12" style="padding: 0;">
                                                    <button onclick="openModal('<?= encode($coupon->id) ?>')" style="font-size: 13px;">Lihat Detail</button>
                                                </div>
                                            <?php else : ?>
                                                <div class="col-md-12">
                                                    <button onclick="openModal('<?= encode($coupon->id) ?>')">Lihat Detail</button>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php else : ?>
                            <div class="col-md-12">
                                <div class="text-left">
                                    <!-- <img src="<?php //echo base_url('assets/frontend/images/404-notfound.jpeg') ?>" alt="" style="width: 30em;"> -->
                                    Anda tidak memiliki voucher
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalCoupon" tabindex="-1" role="dialog" aria-labelledby="modalCouponLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <h5 class="modal-title" id="modalCouponLabel">Info Voucher</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div> -->
      <img src="<?= base_url('assets/frontend/images/voucher-pic.jpg') ?>" alt="voucher-pic-modal" />
      <?php if($this->agent->is_mobile()) : ?>
        <span class="title-coupon-modal-mobile"><span id="coupon_name_modal"></span> <br /><span class="title-coupon-secondary">Diskon <span class="total-discount"></span></span></span>
        <?php else : ?>
        <span class="title-coupon-modal"><span id="coupon_name_modal"></span> <br /><span class="title-coupon-secondary">Diskon <span class="total-discount"></span></span></span>
      <?php endif; ?>
      <div class="modal-body">
            <!-- <span class="coupon-description">Ini Deskripsi</span> -->
            <span class="date-end-coupon">Berlaku hingga : <span class="date-end-content"></span></span>
            <div class="coupon-modal-wrapper">
                <h3 style="font-size: 15px; font-weight: bold;margin-bottom: 10px;">Syarat dan Ketentuan</h3>
                <ol>
                    <li>Anda akan mendapatkan potongan sebesar <span class="total-discount" style="font-weight: bold"></span> </li>
                    <li id="coupon-app-info"></li>
                    <li>Voucher berlaku untuk <span id="list-category" style="font-weight: bold;"></span> </li>
                    <li>Voucher berlaku untuk <span id="list-city" style="font-weight: bold;"></span> </li>
                    <li>Minimal pembelian yang berlaku untuk voucher ini adalah <span class="coupon-min-order" style="font-weight: bold;"></span></li>
                    <li>Maksimal pembelian yang berlaku untuk voucher ini adalah <span class="coupon-max-amount" style="font-weight: bold;"></span></li>
                    <li>Kode Voucher tidak dapat digabung dengan voucher lainnya.</li>
                </ol>
                <?php if ($this->agent->is_mobile()) : ?>
                    <div class="modal-copy-coupon-mobile">
                        Kode: <br />
                        <!-- <strong class="coupon-code-modal"></strong> -->
                        <input type="text" id="code_coupon_modal" value="" readonly style="background: none; border: none;padding-left: 5px; font-weight: bold;">
                        <button id="button_copy_code" class="btn-copied" onclick="copy_code_modal(this)">Salin Kode</button>
                    </div>
                <?php else : ?>
                <div class="modal-copy-coupon">
                    Kode Voucher:
                    <!-- <strong class="coupon-code-modal"></strong> -->
                    <input type="text" id="code_coupon_modal" value="" readonly style="background: #F5F5F5; border: none;padding-left: 5px; font-weight: bold;">
                    <button id="button_copy_code" class="btn-copied" onclick="copy_code_modal(this)">Salin Kode</button>
                </div>
                <?php endif; ?>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>