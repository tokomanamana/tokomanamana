<div class="my-account" id="wishlist">
    <section class="collection-heading heading-content ">
        <div class="container">
            <div class="row">
                <div class="col-sm-9 col-sm-push-3">
                    <div class="collection-wrapper">
                        <h1 class="collection-title"><span>Poin Saya</span></h1>
                        <?php echo $breadcrumb; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php
        $user_id = $this->session->userdata('user_id');
        $total_point = 0;
        $m = $this->Model;
        $q_point = $m->get_data('point', 'point', null, array('id_customer' => $this->session->userdata('user_id')));

        if($q_point->num_rows() > 0){
            $total_point = $q_point->row()->point;
        }

        $tab_point = $this->session->userdata('tab_point');
        $t_point = array(
            0 => $tab_point == '0' ? "color: #97C23C" : "",
            1 => $tab_point == '1' ? "color: #97C23C" : "",
            2 => $tab_point == '2' ? "color: #97C23C" : ""
        );
    ?>
    <section class="collection-content">
      <div class="collection-wrapper">
        <div class="container" style="margin-bottom: 30px;">
          <div class="row">
            <?php echo $this->load->view('navigation'); ?>
            <div id="collection">
              <div class="collection-mainarea col-md-9 col-sm-8 col-xs-12">
                <?php if ($products->num_rows() > 0) { ?>
                  <div class="collection-items clearfix">
                    <div class="products">
                      <div class="col-xs-12 col-sm-6" style="padding: 4px;">
                        <div class="product">
                          <div class="row-right animMix">
                            <div class="grid-mode">
                              <div class="product-title">Poin Saya</div>
                              <div class="product-price" style="margin: 12px; font-size: 24px">
                                <h1><?=number_format($total_point)?></h1>
                              </div>
                            </div>
                            <!-- <div class="grid-mode">
                                <div class="product-title">Poin Saya</div>
                                <div class="product-price">
                                    <span class="price_sale"><span class="money">Price</span></span>
                                </div>
                            </div> -->
                          </div>
                          <div style="width: 100%; text-align: center;">
                            <?php
                              $periode = 'Belum Ada';

                              $qsettings = $m->get_data('', 'settings', null, array('key' => 'periode_tukar_point'));

                              if($qsettings->num_rows() > 0){
                                $exp = explode(';', $qsettings->row()->value);

                                if($exp[6] == '1'){
                                  $periode = date('d M Y', strtotime($exp[0].'/'.$exp[1].'/'.$exp[2])).' S/D '.date('d M Y', strtotime($exp[3].'/'.$exp[4].'/'.$exp[5]));
                                }
                              }
                            ?>
                            <p>Periode tukar point : </p><h3 style="color: red"><?=$periode?></h3>
                          </div>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6" style="padding: 4px;">
                        <div class="product">
                          <div class="row-right animMix">
                            <div class="grid-mode">
                              <div class="product-title">Poin Saya</div>
                              <div class="product-price" style="margin: 12px; font-size: 24px">
                                <h1><?=number_format($total_point)?></h1>
                              </div>
                            </div>
                            <!-- <div class="grid-mode">
                                <div class="product-title">Poin Saya</div>
                                <div class="product-price">
                                    <span class="price_sale"><span class="money">Price</span></span>
                                </div>
                            </div> -->
                          </div>
                          <div style="width: 100%; text-align: center;">
                            <a href="" style="background-color: #97C23C; border: none; color: white; padding: 8px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 14px; margin: 4px 2px; cursor: pointer;"  onclick="return confirm('Yakin hapus produk dari wishlist?');">Tukar Poin</a>
                          </div>
                        </div>
                      </div>

                      <div class="col-xs-12" style="padding: 4px;">
                        <div class="product">
                          <div class="row-right animMix">
                            <div class="grid-mode" style="padding-left: 8px">
                                <!-- <a href="#" style="font-size: 14px; margin-right: 16px; <?=$t_point[0]?>"><b>Tukar Poin</b></a>
                                <a href="#" style="font-size: 14px; margin-right: 16px; <?=$t_point[1]?>"><b>Voucher</b></a>
                                <a href="#" style="font-size: 14px; <?=$t_point[2]?>"><b>Riwayat Tukar Poin</b></a>
                                <div class="product-title">Poin Saya</div>
                                <div class="product-price" style="margin: 12px; font-size: 24px">
                                    <h1><?=number_format($total_point)?></h1>
                                </div> -->

                              <ul class="nav nav-tabs">
                                <li id="tukar_poin" class=""><a href="<?=base_url()?>member/point" onclick="changeTab(0)">Tukar Poin</a></li>
                                <li id="voucher"><a href="<?=base_url()?>member/coupons" onclick="changeTab(1)">Coupon</a></li>
                                <li id="riwayat_tukar_poin" class="active"><a href="#" onclick="changeTab(2)">Riwayat Tukar Poin</a></li>
                              </ul>

                              <hr>

                              <div class="container" style="margin-bottom: 30px;">
                                <div class="row">
                                  <div class="collection-mainarea col-md-9 col-sm-8 col-xs-12">
                                    <?php
                                      $join = array(
                                        0 => 'reward r-r.id=rr.id_reward'
                                      );
                                      $q_riwayat = $m->get_data('rr.periode, rr.id as rr_id, r.*', 'reward_claim_temp rr', $join, null, '', 'rr.periode desc, r.point desc', 0, 0, array(0 => "rr.id_customer LIKE '%,$user_id,%'"));

                                      foreach ($q_riwayat->result() as $key) {
                                        $gambar = $key->gambar == '' ? 'http://localhost/tmm/files/images/logo.png' : $key->gambar;
                                        $exp_periode = explode(';', $key->periode);

                                        $periode0 = date('d/m/Y', strtotime($exp_periode[0].'/'.$exp_periode[1].'/'.$exp_periode[2]));
                                        $periode1 = date('d/m/Y', strtotime($exp_periode[3].'/'.$exp_periode[4].'/'.$exp_periode[5]));

                                        $q_exchange = $m->get_data('', 'point_exchange_history', null, array('id_reward' => $key->id, 'id_customer' => $user_id), '', 'last_update desc')->row()->last_update;

                                        $last_update = date('d/m/Y H:i', strtotime($q_exchange));

                                        ?>
                                    <div class="panel">
                                      <div class="panel-heading">
                                        <div class="row">
                                          <div class="col-md-4 col-sm-6 col-xs-12">
                                            <span class="header-label">Reward</span>
                                            <span><?=$key->nama?></span>
                                            <span class="header-label"><?=$last_update?></span>
                                          </div>
                                          <div class="col-md-4 col-sm-6 col-xs-12">
                                            <span class="header-label">TOTAL POIN</span>
                                            <span><?=number_format($key->point)?></span>
                                          </div>
                                          <div class="col-md-3 col-xs-12">
                                            <div style="display: table;width: 100%;border-spacing: 0;">
                                              <div style="width: auto; display: table-cell; vertical-align: top;">
                                                <span class="header-label">PERIODE TUKAR POIN</span>
                                                <?=$periode0?><br>SAMPAI<br><?=$periode1?>
                                              </div>
                                              <div style="width: 33%; display: table-cell; vertical-align: middle;">
                                                <a href="<?php echo site_url('member/order_reward_detail/' . encode($key->rr_id)); ?>" class="detail">Lihat Detail</a>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                        <?php
                                      }
                                    ?>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          
                        </div>
                      </div>

                      <script type="text/javascript">
                        function tukar_point(id) {
                          $.ajax({
                            type: 'POST',
                            url: current_url1 + 'checkout/cart_point/add_cart_point',
                            data: {
                              id_reward: id,
                              email: email
                            },
                            dataType: "json",
                            success: function (data) {
                              if (data.success == 1) {
                                window.location.replace(current_url1 + 'cart_point');
                              }else if(data.success == '0'){
                                alert('Ooops.. ' + data.msg);
                              }
                            }
                          });
                        }

                        function changeTab(argument) {
                          if(argument == 0){
                            $('#tukar_poin').addClass('active');
                            $('#voucher').removeClass('active');
                            $('#riwayat_tukar_poin').removeClass('active');
                          }else if(argument == 1){
                            $('#tukar_poin').removeClass('active');
                            $('#voucher').addClass('active');
                            $('#riwayat_tukar_poin').removeClass('active');
                          }else{
                            $('#tukar_poin').removeClass('active');
                            $('#voucher').removeClass('active');
                            $('#riwayat_tukar_poin').addClass('active');
                          }
                        }

                        function changeTab1(argument) {
                          if(argument == 0){
                            $('#produk_tab').css('background-color', '#97C23C');
                            $('#pulsa_tab').css('background-color', '');
                            $('#voucher_tab').css('background-color', '');
                            $('#indomaret_tab').css('background-color', '');

                            $('#produk_tab_a').css('color', 'white');
                            $('#pulsa_tab_a').css('color', 'black');
                            $('#voucher_tab_a').css('color', 'black');
                            $('#indomaret_tab_a').css('color', 'black');
                          }else if(argument == 1){
                            $('#produk_tab').css('background-color', '');
                            $('#pulsa_tab').css('background-color', '#97C23C');
                            $('#voucher_tab').css('background-color', '');
                            $('#indomaret_tab').css('background-color', '');

                            $('#produk_tab_a').css('color', 'black');
                            $('#pulsa_tab_a').css('color', 'white');
                            $('#voucher_tab_a').css('color', 'black');
                            $('#indomaret_tab_a').css('color', 'black');
                          }else if(argument == 2){
                            $('#produk_tab').css('background-color', '');
                            $('#pulsa_tab').css('background-color', '');
                            $('#voucher_tab').css('background-color', '#97C23C');
                            $('#indomaret_tab').css('background-color', '');

                            $('#produk_tab_a').css('color', 'black');
                            $('#pulsa_tab_a').css('color', 'black');
                            $('#voucher_tab_a').css('color', 'white');
                            $('#indomaret_tab_a').css('color', 'black');
                          }else if(argument == 3){
                            $('#produk_tab').css('background-color', '');
                            $('#pulsa_tab').css('background-color', '');
                            $('#voucher_tab').css('background-color', '');
                            $('#indomaret_tab').css('background-color', '#97C23C');

                            $('#produk_tab_a').css('color', 'black');
                            $('#pulsa_tab_a').css('color', 'black');
                            $('#voucher_tab_a').css('color', 'black');
                            $('#indomaret_tab_a').css('color', 'white');
                          }
                        }
                      </script>

                      <?php if(false){?>
                      <?php foreach ($products->result() as $product) { ?>
                        <div class="product-item col-sm-3" style="padding: 1px;">
                          <div class="product">
                            <div class="row-left">
                              <a href="<?php echo seo_url('catalog/products/view/' . $product->id); ?>" class="hoverBorder container_item">
                                    <img src="<?php echo ($product->image) ? site_url('files/images/' . $product->image) : site_url('assets/frontend/images/noimage.jpg'); ?>" class="img-responsive" alt="<?php echo $product->name; ?>">
                              </a>
                            </div>
                            <div class="row-right animMix">
                              <div class="grid-mode">
                                <div class="product-title">
                                  <a href="<?php echo seo_url('catalog/products/view/' . $product->id); ?>"><?php echo $product->name; ?></a>
                                </div>
                                <div class="product-price">
                                  <span class="price_sale"><span class="money"><?php echo rupiah($product->price); ?></span></span>
                                </div>
                              </div>
                            </div>
                            <div class="delete">
                              <a href="<?php echo site_url('auth/member/delete_wishlist/' . encode($product->wishlist)); ?>" onclick="return confirm('Yakin hapus produk dari wishlist?');">Hapus</a>
                            </div>
                          </div>
                        </div>
                      <?php } ?>
                      <?php } ?>
                    </div>
                  </div>
                  <div style="display: block; text-align: center;">
                    <div class="pagi-bar">
                      <?php echo $pagination; ?>
                    </div>
                  </div>
                <?php } else { ?>
                  <div class="block">
                    <p>Belum ada produk di wishlist.</p>
                  </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
</div>

<script type="text/javascript">
  let current_url1 = '<?=base_url()?>';
  let email = '<?=$this->session->userdata("email")?>';
</script>