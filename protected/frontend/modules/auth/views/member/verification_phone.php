<section class="collection-heading heading-content ">
    <div class="container">
        <div class="row">
            <div class="collection-wrapper">
                <h1 class="collection-title"><span>Verifikasi Nomor Handphone</span></h1>
                <div class="breadcrumb-group">
                    <div class="breadcrumb clearfix">
                        <?php echo $breadcrumb; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="login-content">
    <div class="login-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="login-content-inner">
                    <?php if ($message) { ?>
                        <div class="alert alert-danger fade in alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <?php echo $message; ?>
                        </div>
                    <?php } ?>
                    <form method="post" accept-charset="UTF-8">
                        <label class="label">No. Handphone</label>
                        <input type="text" class="text" readonly="" value="<?php echo $user->phone; ?>">
                        <label class="label">Masukkan Kode Verifikasi</label>
                        <input type="text" class="text" name="token" autocomplete="off">
                        <p>Belum mendapatkan kode? <span id="wait" style="<?php echo (!$token_time_left) ? 'display:none;' : ''; ?>">Tunggu <span></span> detik</span><a id="request-token" href="javascript:void(0)" style="<?php echo ($token_time_left) ? 'display:none;' : ''; ?>">Kirim ulang</a></p>
                        <div class="action_bottom">
                            <input class="btn" type="submit" value="Verifikasi">
                            <a href="<?php echo site_url('member/profile'); ?>">Ubah Nomor Handphone</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $.fn.countdown = function (duration) {
        var countdown = setInterval(function () {
            if (--duration) {
                $('#wait span').html(duration);
            } else {
                clearInterval(countdown);
                $('#wait').hide();
                $('#request-token').show();
            }
        }, 1000);
    };
<?php if ($token_time_left) { ?>
        $(".countdown").countdown(<?php echo $token_time_left; ?>);
<?php } ?>
    $('#request-token').on('click', function () {
        $.ajax({
            url: site_url + 'auth/member/request_token',
            success: function (data) {
                $('#wait').show();
                $(".countdown").countdown(120);
                $('#request-token').hide();
            }
        })
    });
</script>