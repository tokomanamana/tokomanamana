<style>
    input{border-radius:3px}input:focus{border-color:#97c23c}input[type=submit]{border-radius:3px;background:#97c23c;border:1px solid #97c23c;transition:.3s}input[type=submit]:hover{background:#85a839;border:1px solid #85a839;color:#fff}.row select{margin-bottom:20px}#login-email a{transition:.3s}#login-email a:hover{color:#97c23c}#register_form input,#register_form select{background-image:none;background-color:transparent;-webkit-box-shadow:none;-moz-box-shadow:none;box-shadow:none;border:1px solid #ccc;border-radius:3px}#register_form input:focus,#register_form select:focus{border-color:#a7c22a;background-color:none!important;background-image:none}#register_form .text_login{margin-top:10px;margin-bottom:10px}#register_form .text_other{margin-top:20px}#register_form a{color:#a7c22a}#register_form a:hover{color:#e75a5f}#register_form .action{margin-bottom:-20px}#register_form .action button{width:100%;margin:10px 0}#register_form .action button:focus,#register_form .action button:focus i{outline:0;color:#fff}#register_form .action button.btn_login{border-radius:3px;background:#a7c22a;border:1px solid #a7c22a;text-transform:capitalize;font-weight:700;font-size:14px}#register_form .action button.btn_login:hover{color:#fff!important;background:#8ea623;border:1px solid #8ea623}#register_form .action button.btn_register_facebook{background:#3b5998;border:1px solid #3b5998;border-radius:3px;text-transform:capitalize;font-weight:700;font-size:14px}#register_form .action button.btn_register_facebook:hover{background:#2c4170!important;border:1px solid #2c4170!important;color:#fff}#register_form .action button.btn_register_facebook:hover i{color:#fff!important}#register_form .action button.btn_register_google{color:#444;background:#ccc;border:1px solid #ccc;border-radius:3px;text-transform:capitalize;font-weight:700;font-size:14px}#register_form .action button.btn_register_google:hover{background:#b3b3b3;border:1px solid #b3b3b3}#register_form .action button.btn_register_google i{color:#444}#register_form .action .col-md-3{padding-left:0}#register_form button#button_phone_verification{height:34px;width:100%;border:1px solid #ccc;background:#ccc;border-radius:3px;font-weight:700;font-size:12px;color:#444;margin-bottom:20px}#register_form button#button_phone_verification:focus{outline:0}#register_form button#button_phone_verification:hover{background:#b3b3b3;border:1px solid #b3b3b3}#register_form .error_validation_login .alert,#register_form .send_otp_success .alert{border-radius:3px}#register_form input:active,#register_form input:focus,#register_form select:active,#register_form select:focus{border:1px solid #a7c22a!important}#register_form .error_validation span{color:#d9534f;margin-top:-15px}.lds-ring{display:inline-block;position:relative;width:100%;height:100%;padding:9px 2px}.lds-ring div{box-sizing:border-box;display:inline-block;position:absolute;width:20px;height:20px;border:3px solid #000;border-radius:50%;animation:lds-ring 1.2s cubic-bezier(.5,0,.5,1) infinite;border-color:#000 transparent transparent transparent}.lds-ring div:nth-child(1){animation-delay:-.45s}.lds-ring div:nth-child(2){animation-delay:-.3s}.lds-ring div:nth-child(3){animation-delay:-.15s}@keyframes lds-ring{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}#register_form .otp_code .code_input{width:50%;display:inline-block;float:left;padding-right:0}#register_form .otp_code .code_button{width:48%;display:inline-block;float:right;padding-left:0}#register_form .captcha_div div.captcha_input{width:44%;display:inline-block;float:left;padding-right:0}#register_form .captcha_div div.captcha_image{width:53%;display:inline-block;float:right;padding-left:0}#register_form .btn_pin_address{border-radius:3px;background:#a7c22a;border:1px solid #a7c22a;text-transform:capitalize;font-weight:700;font-size:14px;display:inline-block;float:right}#register_form .btn_pin_address:focus{outline:0;color:#fff}#register_form .btn_pin_address:hover{background:#8ea623;color:#fff;border:1px solid #8ea623}#register_form .btn_pin_address:focus i,#register_form .btn_pin_address:hover i{color:#fff}#register_form .captcha_register_image img{width:100%;height:34px}@media(max-width:349px){#register_form .pin_label{font-size:11px;width:45%!important;line-height:40px!important}#register_form .btn_pin_address{font-size:12px;padding:0 15px}}@media(max-width:331px){#register_form .pin_label{width:50%!important}}#register_form .button_media_social div{display:inline-block;width:49%;padding:0!important}#register_form .action{margin-bottom:25px}#register_form .button_media_social div:last-child{margin-left:1%}#register_form .button_media_social{padding-right:15px;padding-left:15px}@media(max-width:326px){#register_form .button_media_social div:last-child{margin-left:0}}
</style>
<section class="login-content" id="login">
    <div class="login-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="panel">
                    <div class="tabbable">
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active"><a href="#login-email" data-toggle="tab">Daftar Akun Tokomanamana</a></li>
                        </ul>

                        <form action="<?php echo site_url('auth/save_register') ?>" style="margin-top: 25px;" id="register_form">
                            <div class="row">
                                <div class="">
                                    <div class="row">
                                        <div class="error_register_validation col-sm-12">
                                        </div>
                                        <div class="send_otp_success col-sm-12">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 form-group">
                                            <label for="no_handphone">No Handphone</label>
                                            <input type="number" class="form-control" placeholder="No Handphone" name="phone_register" id="phone_register">
                                            <div id="error_phone_register" class="error_validation"></div>
                                        </div>
                                        <div class="col-sm-12 form-group">
                                            <label for="full_name">Nama Lengkap</label>
                                            <input type="text" class="form-control" placeholder="Nama Lengkap" name="fullname" id="fullname">
                                            <div id="error_fullname" class="error_validation"></div>
                                        </div>
                                    </div>
                                    <div class="row otp_code">
                                        <div class="col-sm-12 form-group code_input">
                                            <label for="verification_phone_register">Kode OTP</label>
                                            <input type="text" class="form-control" placeholder="Kode OTP" name="verification_phone_register" id="verification_phone_register">
                                            <span id="error_verification_phone_register" class="error_validation"></span>
                                        </div>
                                        <div class="col-sm-12 form-group code_button">
                                            <label for="" style="color: white;">y</label>
                                            <button type="button" id="button_phone_verification" class="button_phone_verification" data-otp="true">
                                                Kirim OTP
                                            </button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 form-group">
                                            <label for="password_register">Kata Sandi</label>
                                            <input type="password" class="form-control" placeholder="Kata Sandi" name="password_register" id="password_register">
                                            <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"
                                            style="float: right;
                                            margin-top:-42px;
                                            margin-right:10px;
                                            position: relative;
                                            z-index: 2;
                                            cursor: pointer;" onclick="password_register_show(this)"></span>
                                            <div id="error_password_register" class="error_validation"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 form-group birtday">
                                            <div class="">
                                                <label for="birthdate_register">Tanggal Lahir</label>
                                            </div>
                                            <input type="date" class="form-control" placeholder="Tanggal Lahir" id="birthdate_register" name="birthdate_register">
                                        </div>
                                        <div class="col-sm-12 form-group">
                                            <label for="gender_register">Jenis Kelamin</label>
                                            <select name="gender_register" id="gender_register" class="form-control">
                                                <option disabled="" selected="" value="">Jenis Kelamin</option>
                                                <option value="L">Laki - Laki</option>
                                                <option value="P">Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row captcha_div">
                                        <div class="col-sm-12 form-group captcha_input">
                                            <label for="captcha_register">Kode Captcha</label>
                                            <input type="text" class="form-control" name="captcha_register" id="captcha_register" placeholder="Kode Captcha">
                                            <span id="error_captcha_register" class="error_validation"></span>
                                        </div>
                                        <div class="col-sm-12 form-group captcha_image">
                                            <label for="" style="text-align: right;"><a href="#" id="change_captcha_register">Ganti Captcha</a></label>
                                            <div class="captcha_register_image"><?php echo $image ?></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <label for="">Pin alamat untuk memudahkan anda dalam pengiriman Grab Express</label>
                                            <div class="square" style="width: 100%; border: 1px solid #EEE;vertical-align: middle;clear: both;float: left;padding: 20px;border-radius: 5px;">
                                                <div style="display: inline-block;width: 50%;float: left;line-height: 36px" class="pin_label">Pin Alamat (Opsional)</div>
                                                <button type="button" class="btn btn_pin_address" onclick="add_other_address();"><i class="fa fa-map-marker"></i> &nbsp;Pin Alamat</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="text_other">
                                                Dengan mendaftar, Anda menyetujui <a href="javascript:void(0)" data-toggle="modal" data-target="#lightbox">Syarat, Ketentuan dan Kebijakan dari Tokomanamana</a>
                                            </div>
                                            <div class="text_login">
                                                Sudah punya akun Tokomanamana ? <a href="<?php echo site_url('auth/login') ?>">Masuk Disini</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="back" value="<?php echo site_url('') ?>">
                            <input type="hidden" id="lat_register" name="lat">
                            <input type="hidden" id="lng_register" name="lng">
                            <div class="row action">
                                <div class="col-sm-12">
                                    <button class="btn btn_login" type="submit">Daftar Sekarang</button>
                                </div>
                                <div class="row button_media_social">
                                    <div class="col-sm-12">
                                        <button class="btn btn_register_facebook" type="button"><i class="fa fa-facebook" aria-hidden="true"></i> &nbsp;Facebook</button>
                                    </div>
                                    <div class="col-sm-12">
                                        <button class="btn btn_register_google" type="button"><i class="fa fa-google" aria-hidden="true"></i> &nbsp;Google</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="lightbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">×</button>
        <div class="modal-content">
            <div class="modal-body">
                <?php echo settings('term_customer_register'); ?>
            </div>
        </div>
    </div>
</div>
<script>
$(".toggle-password").click(function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $('#pass_member');
    if (input.attr("type") == "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
    }
    });
</script>