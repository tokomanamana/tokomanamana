<div bgcolor="#FFFFFF" style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;width:100%!important;height:100%;font-size:14px;color:#404040;margin:0;padding:0">
    <table style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:0" bgcolor="transparent">
        <tbody>
            <tr style="margin:0;padding:0">
                <td style="margin:0;padding:0"></td>
                <td style="display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0" bgcolor="#FFFFFF">
                    <div style="max-width:600px;display:block;border-collapse:collapse;margin:0 auto;border:1px solid #e7e7e7;border-radius: 5px;">
                        <table style="max-width:100%;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:20px;" bgcolor="transparent">
                            <tbody>
                                <tr style="margin:0;padding:0">
                                    <td style="text-align: center;">
                                        <img src="<?php echo site_url('/assets/frontend/images/logo.png'); ?>" alt="" title="" style="margin:10px 0; height: 30px;">
                                    </td>
                                </tr>
                                <tr style="margin: 0; padding: 0;">
                                    <td style="text-align: center;">
                                        <img src="<?php echo site_url('/assets/frontend/images/email1.png') ?>" alt="" title="" style="margin:10px 0;width: 170px;">
                                    </td>
                                </tr>

                                <tr style="margin:0;padding:0">
                                    <td style="margin:0;padding:0">
                                        <h5 style="line-height:32px;color:#666;font-weight:500;font-size:18px;margin: 25px 0;padding:0;text-align: center;">Verifikasi Alamat Email Anda</h5>
                                        <p style="font-weight:normal;color:#999;font-size:14px;line-height:1.6;margin:20px 0;padding:0;text-align: center;">
                                            Hai <?php echo $name; ?>, lakukan konfirmasi dengan memasukkan kode verifikasi berikut ini. <br>
                                            <!-- Anda kini telah terdaftar di sistem kami, namun akun Anda <b>belum aktif</b> dan akan segera di verifikasi oleh tim kami. -->
                                            <!-- <span style="display: block; text-align: center"><a href="<?php //echo site_url('auth/member/email_confirmation/' . $token); ?>" target="_blank" style="background-color: #97C23C; padding: 20px; text-decoration: none; color: #fff; font-weight: bold;border-radius:5px;">Konfirmasi</a></span> -->
                                        </p>
                                        <div style="display: block;text-align: center;">
                                            <div style="display: inline-block;font-weight:bold;color:#999;font-size:16px;border: 1px solid #999;padding: 10px 30px;border-radius: 5px;"><?php echo $code ?></div>
                                            <div style="font-weight:normal;color:#999;font-size:12px;margin: 10px 0;">*Kode verifikasi hanya berlaku selama 2 jam.</div>
                                        </div>
                                        <p style="font-weight:normal;color:#999;font-size:14px;line-height:1.6;margin:20px 0;padding:0;text-align: center;">
                                            <b>PERHATIAN: </b> Jangan sampai ada orang lain mengetahui kode verifikasi ini!.
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="padding:0 20px">
                            <p style="font-size:12px;color:#999;padding:20px 0;margin:0;border-top: 1px solid #E0E0E0;text-align: center;">
                                Email dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.
                            </p>
                        </div>
                    </div>
                </td>
                <td style="margin:0;padding:0"></td>
            </tr>
        </tbody>
    </table>
</div>