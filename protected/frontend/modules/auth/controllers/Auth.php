<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->lang->load('auth', settings('language'));
        $this->load->model('cart_model');
        
        $this->load->library('form_validation');
        $this->load->library('breadcrumb');
        //$this->load->helper('captcha');
        //$this->load->library('recaptcha');
    }

    public function ccc($value=''){
        $this->ion_auth->ccc($value);
    }

    // public function login() {
    //     if(!$this->agent->is_mobile()) {
    //         $this->session->set_flashdata('not_login_redirect', '<script>$("#modal_login").modal("show");</script>');
    //         redirect(site_url(''),'location');
    //     }
        
    //     $back = $this->input->get('back');
    //     $ip_address = $this->input->ip_address();
    //     if ($this->input->is_ajax_request()) {
    //         if ($this->input->post('login_type') == 'phone') {
    //             $this->form_validation->set_rules('phone', 'No. Handphone', 'required');
    //             $this->form_validation->set_rules('token', 'Kode Verifikasi', 'required');
    //         } else {
    //             $this->form_validation->set_rules('identity', 'lang:login_identity_label', 'required');
    //             $this->form_validation->set_rules('password', 'lang:login_password_label', 'required');
    //         }
    //         if ($this->form_validation->run() == true) {
    //             $data = $this->input->post(null, true);
    //             do {
    //                 $return = ['status' => 'error', 'message' => $this->template->alert('danger', 'Login gagal, silahkan coba lagi')];
    //                 if ($data['login_type'] == 'phone') {
    //                     $customer = $this->main->get('customers', array('phone' => $data['phone']));
    //                     if (!$customer) {
    //                         break;
    //                     }
    //                     if ($customer->verification_code != $data['token']) {
    //                         break;
    //                     }
    //                     $this->ion_auth_model->set_session($customer);
    //                     $this->ion_auth_model->update_last_login($customer->id);
    //                     $return = ['status' => 'success', 'redirect' => site_url($back)];
    //                 } elseif ($data['login_type'] == 'email') {
    //                     $this->db->where('email',$data['identity']);
    //                     $this->db->or_where('phone',$data['identity']);
    //                     $check_login = $this->db->get('customers');
    //                     if($check_login->num_rows() > 0){
    //                         $res = $check_login->result_array();
    //                         if ($this->ion_auth->login($res[0]['email'], $data['password'], false)) {
    //                             $return = ['status' => 'success', 'redirect' => site_url($back)];
    //                         }
    //                     } else {
    //                         break;
    //                     }
    //                 }
    //             } while (0);
    //         } else {
    //             $return = ['status' => 'error', 'message' => $this->template->alert('danger', validation_errors())];
    //         }
    //         echo json_encode($return);
    //     } else {
    //         if ($this->ion_auth->logged_in()) {
    //             redirect($back);
    //         }

    //         // $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
    //         // $this->breadcrumb->add('Member', site_url('member'));
    //         // $this->breadcrumb->add('Login', site_url('member/login'));
    //         // $this->data['breadcrumb'] = $this->breadcrumb->output();

    //         $this->template->_init();
    //         $this->output->set_title('Login');
    //         $this->data['meta_description'] = "Login Customer Tokomanamana";
    //         $this->load->js('assets/frontend/js/modules/login.js');
    //         $this->load->view('login', $this->data);
    //     }
    // }

    public function login() {
        if(!$this->agent->is_mobile()) {
            $this->session->set_flashdata('not_login_redirect', '<script>
            $(function(){
                $("#modal_login").modal("show")
            })
            </script>');
            redirect(site_url(''),'refresh');
        }

        if ($this->ion_auth->logged_in()) {
            if ($this->data['user']->type == 'g') {
                redirect('');
            } else {
                redirect(site_url('member'), 'refresh');
            }
        }

        $back = $this->input->get('back');
        $ip_address = $this->input->ip_address();
        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Member', site_url('member'));
        $this->breadcrumb->add('Login', site_url('member/login'));
        $this->data['breadcrumb'] = $this->breadcrumb->output();

        $this->template->_init();
        $this->output->set_title('Masuk');
        $this->data['meta_description'] = "Login Customer Tokomanamana";
        $this->load->js('assets/frontend/js/modules/login.js');
        $this->load->view('login', $this->data);
    }

//     public function register() {
//         $this->load->library('session');
//         $this->load->helper(array('captcha','url'));
//         if ($this->ion_auth->logged_in())
//             redirect('member');

//         $back = $this->input->get('back');

//         $this->data['message'] = '';

//         $this->form_validation->set_rules('name', 'lang:register_fullname_label', 'trim|required|callback__alpha_dash_space');
//         $this->form_validation->set_rules('phone', 'lang:register_phone_label', 'trim|required|numeric');
//         $this->form_validation->set_rules('email', 'lang:register_identity_label', 'trim|required|valid_email|callback__check_email');
//         $this->form_validation->set_rules('password', 'lang:register_password_label', 'trim|required|alpha_dash|min_length[6]');
//         $this->form_validation->set_rules( 'captcha', 'captcha', 'trim|callback_validate_captcha|required' );
//         //$this->form_validation->set_rules('captcha', 'lang:register_captcha_label', 'trim|required|callback_validate_captcha');

//         //$this->form_validation->set_rules('g-recaptcha-response', 'lang:register_recaptcha_label', 'trim|required|callback__check_recaptcha');

//        /* if ($this->input->post() && ($this->input->post('secutity_code') == $this->session->userdata('mycaptcha'))) {
//             //$this->load->view('berhasil.php');
//             //echo "OK";
//             redirect($back);
//         } 

//         else
//         {*/
//             // load codeigniter captcha helper

//         $this->load->helper('captcha');
 
//             $vals = array(
//                 'word' => '',
//                 'pool' => '0123456789',
//                 'word_length' => 4,
//                 'img_path'   => './assets/frontend/captcha/',
//                 'img_url'    => base_url().'assets/frontend/captcha/',
//                 'img_width'  => '200',
//                 'img_height' => 35,
//                 'border' => 0,
//                 'font_size' => 26, 
//                 'expiration' => 7200,
//                 'font_path' => FCPATH. 'assets/frontend/captcha/fonts/coolvetica_rg.ttf',
//                 'colors' => array(
//                     'background' => array(255, 255, 255),
//                     'border' => array(230, 230, 230),
//                     'text' => array(0, 0, 0),
//                     'grid' => array(255, 255, 255)
//                 )
//             );
 
//             // create captcha image
//             $cap = create_captcha($vals);
 
//             // store image html code in a variable
//             $this->data['image'] = $cap['image'];
 
//             //var_dump($this->session->userdata['mycaptcha']);
//         if ($this->form_validation->run() == true) {
//             // store the captcha word in a session
//             //var_dump('aaa');exit();
//             if ($register = $this->ion_auth->register($this->input->post('email'), $this->input->post('password'), $this->input->post('email'), array('fullname' => $this->input->post('name'), 'phone' => $this->input->post('phone')))) {
//                 $this->data['name'] = $this->input->post('name');
//                 $message = $this->load->view('email/member/register_confirmation', $this->data, TRUE);

//                 $cronjob = array(
//                     'from' => settings('send_email_from'),
//                     'from_name' => settings('store_name'),
//                     'to' => $this->input->post('email'),
//                     'subject' => 'Konfirmasi Pendaftaran ' . settings('store_name'),
//                     'message' => $message
//                 );
//                 $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
// //                send_mail('noreply@tokomanamana.com', $this->input->post('email'), 'Konfirmasi Pendaftaran ' . settings('store_name'), $message);
//                 if ($this->ion_auth->login($this->input->post('email'), $this->input->post('password'))) {
//                     redirect($back);
//                 }

//             } else {
//                 //var_dump('bbb');exit();
//                //  if (file_exists(BASEPATH . "/assets/frontend/captcha/" . $this->session->userdata['image']))
//                //     unlink(BASEPATH . "/assets/frontend/captcha/" . $this->session->userdata['image']);
//                // $this->session->unset_userdata('mycaptcha');
//                // $this->session->unset_userdata('image');

//                 $this->data['message'] = $this->ion_auth->errors();
//             } 
//         } 
//         else {

//             $this->session->set_userdata('mycaptcha', $cap['word']);
//                 //var_dump('ccc');exit();
//             $this->data['message'] = validation_errors();


//         }

//         $this->template->_init();
//         $this->output->set_title('Daftar Tokomanamana');
//         $this->data['meta_description'] = "Mendaftar sebagai pembeli ditokomanamana, dapatkan promo termurah setiap hari, ambil barang dekat dari lokasi anda.";

//         $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
//         $this->breadcrumb->add('Member', site_url('member'));
//         $this->breadcrumb->add('Register', site_url('member/register'));

//         //$this->data['widget'] = $this->recaptcha->getWidget();
//         //$this->data['script'] = $this->recaptcha->getScriptTag();
//         $this->data['breadcrumb'] = $this->breadcrumb->output();
//         $this->load->view('register', $this->data);
//     //}
//     }

    public function register() {
        $this->load->library('session');
        $this->load->helper(array('captcha','url'));
        $referral_code = $this->input->get('c');
        // var_dump($referral_code); die;
        if(!$this->agent->is_mobile()) {
            $this->session->set_flashdata('not_login_redirect', '<script>
            $(function() {
                const referral_code = "' . $referral_code . '"
                $("#modal_register").modal("show");
                $("#modal_register #from_code").val(referral_code)
                $.ajax({
                    url: site_url + "auth/get_captcha",
                    data: "",
                    success: function(data) {
                        data = JSON.parse(data)
                        $("#modal_register .captcha_register_image").html(data.image)
                    }
                })
            })
            </script>');
            redirect('','refresh');
        }
        if ($this->ion_auth->logged_in()) {
            if ($this->data['user']->type == 'g') {
                redirect('error_403');
            } else {
                redirect('member');
            }
        }

        $back = $this->input->get('back');

        $this->data['message'] = '';

        $this->load->helper('captcha');
 
        $vals = array(
            'word' => '',
            'pool' => '0123456789',
            'word_length' => 4,
            'img_path'   => './assets/frontend/captcha/',
            'img_url'    => base_url().'assets/frontend/captcha/',
            'img_width'  => '200',
            'img_height' => 35,
            'border' => 0,
            'font_size' => 26, 
            'expiration' => 7200,
            'font_path' => FCPATH. 'assets/frontend/captcha/fonts/coolvetica_rg.ttf',
            'colors' => array(
                'background' => array(255, 255, 255),
                'border' => array(230, 230, 230),
                'text' => array(0, 0, 0),
                'grid' => array(255, 255, 255)
            )
        );

        // create captcha image
        $cap = create_captcha($vals);

        // store image html code in a variable
        $this->data['image'] = $cap['image'];
        $this->session->set_userdata('captcha_register', $cap['word']);

        $this->template->_init();
        $this->output->set_title('Daftar Tokomanamana');
        $this->data['meta_description'] = "Mendaftar sebagai pembeli ditokomanamana, dapatkan promo termurah setiap hari, ambil barang dekat dari lokasi anda.";

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Member', site_url('member'));
        $this->breadcrumb->add('Register', site_url('member/register'));
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->load->js(site_url('assets/frontend/js/modules/register.js'));
        $this->load->view('register', $this->data);
    }

    public function validate_captcha() {
        $tt = $this->input->post('captcha');
        $rr = $this->session->userdata['mycaptcha'];
       if ($this->input->post('captcha') == $this->session->userdata['mycaptcha']) {
           //var_dump('sss');exit();
           $this->form_validation->set_message('validate_captcha', $rr);
           return true;
       } else {
            // var_dump($tt);
            // var_dump($rr);
            //exit();
           $this->form_validation->set_message('validate_captcha', 'Wrong captcha code');
           return false;
       }
    }     
 
    public function guest() {
        $back = $this->input->get('back');
        if(!$this->agent->is_mobile()) {
            $this->session->set_flashdata('not_login_redirect', '<script>
            $(function(){
                $("#modal-guest").modal("show")
                $("#modal-guest #back-modal").val("' . $back . '")
            })
            </script>');
            redirect(site_url(''),'refresh');
        } else {
            redirect(site_url('member/login'), 'location');
        }

        $back_modal = $this->input->post('back');
        if ($this->ion_auth->logged_in()) {
            redirect($back);
        }
        $this->data['message'] = '';
        if ($this->input->is_ajax_request()) {
            $return = ['status' => 'error', 'message' => $this->template->alert('danger', validation_errors())];
            echo json_encode($return);
        } else {
            $this->form_validation->set_rules('name', 'lang:register_fullname_label', 'trim|required|callback__alpha_dash_space');
            $this->form_validation->set_rules('phone', 'lang:register_phone_label', 'trim|required|numeric');
            $this->form_validation->set_rules('password', 'lang:register_password_label', 'trim|required|alpha_dash|min_length[6]');
            $this->form_validation->set_rules('email', 'lang:register_identity_label', 'trim|required|valid_email|callback__check_email');
            $this->form_validation->set_rules('address', 'lang:register_address_label', 'trim|required');
            $this->form_validation->set_rules('province', 'lang:register_province_label', 'trim|required');
            $this->form_validation->set_rules('city', 'lang:register_city_label', 'trim|required');
            $this->form_validation->set_rules('district', 'lang:register_district_label', 'trim|required');
            $this->form_validation->set_rules('postcode', 'lang:register_postcode_label', 'trim|required|numeric');
            if ($this->form_validation->run() == true) {
                //$password = $this->randomPassword();
                $password = $this->input->post('password');
                if ($register = $this->ion_auth->register($this->input->post('email'), $password, $this->input->post('email'), array('fullname' => $this->input->post('name'), 'phone' => $this->input->post('phone')))) {
                    $this->data['name'] = $this->input->post('name');
                    $this->data['password'] = $password;
                    $this->main->insert('customer_address', 
                                    array(
                                        'customer' => $register, 
                                        'title' => 'Utama',
                                        'name' => $this->input->post('name'),
                                        'phone' => $this->input->post('phone'),
                                        'address' => $this->input->post('address'),
                                        'city' => $this->input->post('city'),
                                        'postcode' => $this->input->post('postcode'),
                                        'province' => $this->input->post('province'),
                                        'district' => $this->input->post('district'),
                                        'primary' => 1
                                    ));
                    $message = $this->load->view('email/member/register_guest', $this->data, TRUE);
                    
                    // $cronjob = array(
                    //     'from' => settings('send_email_from'),
                    //     'from_name' => settings('store_name'),
                    //     'to' => $this->input->post('email'),
                    //     'subject' => 'Konfirmasi Pendaftaran ' . settings('store_name'),
                    //     'message' => $message
                    // );
                    // $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
    //                send_mail('noreply@tokomanamana.com', $this->input->post('email'), 'Konfirmasi Pendaftaran ' . settings('store_name'), $message);
                    send_mail(settings('send_email_from'), settings('store_name'), $this->input->post('email'), 'Konfirmasi Pendaftaran ' . settings('store_name'), $message); 
                    if ($this->ion_auth->login($this->input->post('email'), $password)) {
                        if($back_modal) {
                            redirect($back_modal);
                        } else {
                            redirect($back);
                        }
                    }
                } else {
                    $this->data['message'] = $this->ion_auth->errors();
                }
            } else {
                $this->data['message'] = validation_errors();
            }
            $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
            $this->breadcrumb->add('Member', site_url('member'));
            $this->breadcrumb->add('Login as guest', site_url('auth/guest'));
            $this->data['breadcrumb'] = $this->breadcrumb->output();
            $this->data['provincies'] = $this->main->gets('provincies', array(), 'name ASC');

            $this->template->_init();
            $this->output->set_title('Login sebagai tamu');
            $this->data['meta_description'] = "Login sebagai tamu";
            $this->load->js('assets/frontend/js/modules/guest.js');
            $this->load->view('guest', $this->data);
        }

       
    }

    public function save_guest() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        
        $this->form_validation->set_rules('email_guest', 'Email', 'trim|required|valid_email', [
            'required' => 'Email harus diisi!',
            'valid_email' => 'Email harus valid!'
        ]);
        $this->form_validation->set_rules('phone_guest', 'No Handphone', 'trim|required|numeric|min_length[10]', [
            'required' => 'No Handphone harus diisi!',
            'numeric' => 'Harus angka!'
        ]);
        $this->form_validation->set_rules('fullname_guest', 'Nama Lengkap', 'trim|required', [
            'required' => 'Nama Lengkap harus diisi!'
        ]);
        $this->form_validation->set_rules('verification_phone_guest', 'Kode OTP', 'trim|required', [
            'required' => 'Kode OTP harus diisi!'
        ]);
        $this->form_validation->set_rules('address', 'Alamat', 'trim|required', [
            'required' => 'Alamat harus diisi!'
        ]);
        $this->form_validation->set_rules('province', 'Provinsi', 'trim|required', [
            'required' => 'Provinsi harus diisi!'
        ]);
        $this->form_validation->set_rules('city', 'Kota', 'trim|required', [
            'required' => 'Kota harus diisi!'
        ]);
        $this->form_validation->set_rules('district', 'Kecamatan', 'trim|required', [
            'required' => 'Kecamatan harus diisi!'
        ]);
        $this->form_validation->set_rules('postcode', 'Kode POS', 'trim|required', [
            'required' => 'Kode POS harus diisi!'
        ]);
        $message = [];
        if ($this->form_validation->run() === TRUE) {
            $data = $this->input->post(null, true);

            $phone_verification = $this->main->get('phone_verification', ['phone' => $data['phone_guest'], 'verification_code' => $data['verification_phone_guest']]);
            if ($phone_verification) {

                $lat = '';
                $lng = '';

                $apiKey = 'AIzaSyCJMa9h-arzHI-UQh8IVLnLTk0XXhOsVi8';
                $district = $this->main->get('districts', array('id' => $data['district']));
                $city = $this->main->get('cities', array('id' => $data['city']));
                $province = $this->main->get('provincies', array('id' => $data['province']));
                $full_address = $data['address'] .' Kode Pos '. $data['postcode'] .' '. $district->name .' '. $city->name .' '. $province->name .' Indonesia';

                $geolocate = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($full_address) . '&sensor=false&key=' . $apiKey);

                if ($geolocate) {
                    $output_geolocate = json_decode($geolocate);
                    if ($output_geolocate->status == 'OK') {
                        $lng = $output_geolocate->results[0]->geometry->location->lng;
                        $lat = $output_geolocate->results[0]->geometry->location->lat;
                    }
                }

                $this->main->delete('phone_verification', ['id' => $phone_verification->id]);
                $register_data = [
                    'active' => 1,
                    'fullname' => $data['fullname_guest'],
                    'phone' => $data['phone_guest'],
                    'date_added' => date('Y-m-d H:i:s'),
                    'date_modified' => date('Y-m-d H:i:s'),
                    'verification_phone' => 1,
                    'verification_code' => $data['verification_phone_guest'],
                    'lat' => $lat,
                    'lng' => $lng,
                    'type' => 'g',
                    'email' => $data['email_guest']
                ];

                $password = settings('store_name');
                $register = $this->ion_auth->register($data['phone_guest'], $password, $data['email_guest'], $register_data);

                if ($register) {
                    $customer_address = [
                        'customer' => $register,
                        'title' => 'Utama',
                        'name' => $data['fullname_guest'],
                        'phone' => $data['phone_guest'],
                        'address' => $data['address'],
                        'city' => $data['city'],
                        'postcode' => $data['postcode'],
                        'province' => $data['province'],
                        'district' => $data['district'],
                        'primary' => 1,
                        'latitude' => $lat,
                        'longitude' => $lng
                    ];
                    $this->main->insert('customer_address', $customer_address);
                    $this->ion_auth->login($data['phone_guest'], $password);
                    $return = ['status' => 'success', 'message' => [
                        'lat' => $lat,
                        'lng' => $lng,
                        'back' => $data['back']
                    ]];
                } else {
                    $user_check = $this->main->get('customers', ['email' => $data['email_guest'], 'type' => 'g']);
                    if ($user_check) {
                        $customer_address = [
                            'customer' => $user_check->id,
                            'title' => 'Utama',
                            'name' => $data['fullname_guest'],
                            'phone' => $data['phone_guest'],
                            'address' => $data['address'],
                            'city' => $data['city'],
                            'postcode' => $data['postcode'],
                            'province' => $data['province'],
                            'district' => $data['district'],
                            'primary' => 1,
                            'latitude' => $lat,
                            'longitude' => $lng
                        ];
                        $register_data['password'] = $password;
                        $this->ion_auth_model->update($user_check->id, $register_data);
                        $this->main->update('customer_address', $customer_address, array('customer' => $user_check->id));
                        $this->ion_auth->login($data['phone_guest'], $password, false);
                        $return = array('status' => 'success', 'message' => [
                            'lat' => $lat,
                            'lng' => $lng,
                            'back' => $data['back']
                        ]);
                    } else {
                        array_push($message, [
                            'message' => '<span class="help-block">Email sudah terdaftar!</span>',
                            'type' => 'email_guest'
                        ]);
                        $return = [
                            'status' => 'error',
                            'message' => $message
                        ];
                    }
                }
            } else {
                array_push($message, [
                    'message' => '<span class="help-block">Kode OTP salah!</span>',
                    'type' => 'verification_phone_guest'
                ]);
                $return = [
                    'status' => 'error',
                    'message' => $message
                ];
            }
        } else {
            if (form_error('email_guest')) {
                array_push($message, [
                    'message' => form_error('email_guest'),
                    'type' => 'email_guest'
                ]);
            }
            if (form_error('phone_guest')) {
                array_push($message, [
                    'message' => form_error('phone_guest'),
                    'type' => "phone_guest"
                ]);
            }
            if (form_error('fullname_guest')) {
                array_push($message, [
                    'message' => form_error('fullname_guest'),
                    'type' => 'fullname_guest'
                ]);
            }
            if (form_error('verification_phone_guest')) {
                array_push($message, [
                    'message' => form_error('verification_phone_guest'),
                    'type' => 'verification_phone_guest'
                ]);
            }
            if (form_error('address')) {
                array_push($message, [
                    'message' => form_error('address'),
                    'type' => 'address'
                ]);
            }
            if (form_error('province')) {
                array_push($message, [
                    'message' => form_error('province'),
                    'type' => 'province'
                ]);
            }
            if (form_error('city')) {
                array_push($message, [
                    'message' => form_error('city'),
                    'type' => 'city'
                ]);
            }
            if (form_error('district')) {
                array_push($message, [
                    'message' => form_error('district'),
                    'type' => 'district'
                ]);
            }
            if (form_error('postcode')) {
                array_push($message, [
                    'message' => form_error('postcode'),
                    'type' => 'postcode'
                ]);
            }
            $return = [
                'status' => 'error',
                'message' => $message
            ];
        }
        echo json_encode($return);
    }

    public function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
    
    public function profile() {
        if (!$this->ion_auth->logged_in())
            redirect('auth/login');

        $this->template->_default();
        $this->template->form();

        $this->data['menu'] = array('menu' => 'profile', 'submenu' => '');

        $this->output->set_title(lang('account_setting_title'));
        $this->load->view('profile', $this->data);
    }

    public function update_profile() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->form_validation->set_rules('fullname', 'lang:account_fullname_label', 'trim|required');
        $this->form_validation->set_rules('phone', 'lang:account_phone_label', 'trim');
        $this->form_validation->set_rules('password_old', 'lang:account_old_password_label', 'trim');
        $this->form_validation->set_rules('password', 'lang:account_new_password_label', 'trim|min_length[6]');

        //validate the form
        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);

            $email = $this->data['user']->email;

            do {
                if ($data['password']) {
                    if (!$data['password_old']) {
                        $return = array('message' => lang('account_old_password_warning_message'), 'status' => 'danger');
                        break;
                    } else {
                        if (!$this->ion_auth->change_password($email, $data['password_old'], $data['password'])) {
                            $return = array('message' => lang('account_old_password_error_message'), 'status' => 'danger');
                            break;
                        }
                    }
                }
                unset($data['password']);
                unset($data['password_old']);
                $save = $this->main->update('auth_users', $data, array('id' => $this->ion_auth->get_user_id()));
                $this->data['user']->fullname = $data['fullname'];
                $this->data['user']->phone = $data['phone'];
                $return = array('message' => lang('account_update_success_message'), 'status' => 'success');
            } while (0);
        } else {
            $return = array('message' => validation_errors(), 'status' => 'danger');
        }
        echo json_encode($return);
    }

    // log the user out
    public function logout() {
        $this->ion_auth->logout();
        $this->session->unset_userdata('user');
        $this->session->set_flashdata('message', $this->ion_auth->messages());
        redirect('', 'refresh');
    }

//     public function forgot_password() {
//         if ($this->ion_auth->logged_in())
//             redirect();

//         $this->data['message'] = '';

//         $this->form_validation->set_rules('email', 'lang:login_identity_label', 'required|valid_email');
//         if ($this->form_validation->run() == true) {
//             $user = $this->ion_auth->where('email', $this->input->post('email'))->users()->row();
//             $forgotten = $this->ion_auth->forgotten_password($this->input->post('email'));
//             if ($forgotten) {
//                 $data['user'] = $user;
//                 $data['password_code'] = $forgotten['forgotten_password_code'];
                
//                 $message = $this->load->view('email/forgot_password', $data, TRUE);

//                 $cronjob = array(
//                     'from' => settings('send_email_from'),
//                     'from_name' => settings('store_name'),
//                     'to' => $user->email,
//                     'subject' => 'Reset Password ' . settings('store_name'),
//                     'message' => $message
//                 );
//                 $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
                
// //                send_mail('noreply@tokomanamana.com', $user->email, 'Reset Password TokoManaMana', $this->load->view('email/forgot_password', $data, true));
//                 $this->data['message'] = 'Silahkan cek email Anda untuk melakukan reset password.';
//                 $this->data['status'] = 'success';
//             } else {
//                 $this->data['message'] = 'Email yang Anda masukkan tidak terdaftar.';
//                 $this->data['status'] = 'danger';
//             }
//         } else {
//             $this->data['message'] = validation_errors();
//             $this->data['status'] = 'danger';
//         }

//         $this->template->_init();
//         $this->output->set_title('Lupa Password');

//         $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
//         $this->breadcrumb->add('Member', site_url('member'));
//         $this->breadcrumb->add('Login', site_url('member/forgot_password'));

//         $this->data['breadcrumb'] = $this->breadcrumb->output();
//         $this->load->view('forgot_password', $this->data);
//     }

    public function forgot_password() {
        if ($this->ion_auth->logged_in())
            redirect();

        $this->template->_init();
        $this->output->set_title('Lupa Password');

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Member', site_url('member'));
        $this->breadcrumb->add('Login', site_url('member/forgot_password'));
        $this->load->js(site_url('assets/frontend/js/modules/forgot_password.js'));

        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->load->view('forgot_password', $this->data);
    }

    public function reset_password($code) {
        if ($this->ion_auth->logged_in())
            redirect();

        $user = $this->ion_auth->forgotten_password_check($code);
        if (!$user)
            redirect();

        $this->data['message'] = '';

        if($this->input->is_ajax_request()) {
            $this->form_validation->set_rules('password', 'Password Baru', 'required|min_length[6]|alpha_dash');
            $this->form_validation->set_rules('confirm_password', 'Konfirmasi Password Baru', 'required|matches[password]');
            if ($this->form_validation->run() == true) {
                if ($this->ion_auth->reset_password($user->phone, $this->input->post('confirm_password'))) {
                    $return['message'] = 'Password berhasil diubah. Silahkan login kembali!';
                    $return['status'] = 'success';
                } else {
                    $return['message'] = $this->set_message_flashdata('danger', $this->ion_auth->errors());
                    $return['status'] = 'error';
                }
            } else {
                $return['message'] = $this->set_message_flashdata('danger', validation_errors());
                $return['status'] = 'error';
            }
            echo json_encode($return);
        } else {
            $this->template->_init();
            $this->output->set_title('Reset Password');
            $this->load->view('reset_password', $this->data);
        }
    }

    function _alpha_dash_space($fullname){
        if (! preg_match('/^[a-zA-Z\s]+$/', $fullname)) {
            $this->form_validation->set_message('_alpha_dash_space', 'Kolom %s hanya boleh berisi karakter & spasi');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function _check_recaptcha() {
        $recaptcha = $this->input->post('g-recaptcha-response');
        $response = $this->recaptcha->verifyResponse($recaptcha);
        if (isset($response['success']) and $response['success'] === true) {
            return true;
        } else {
            $this->form_validation->set_message('_check_recaptcha', 'Captcha gagal di verifikasi.');
            return false;
        }
    }

    function _check_email() {
        if (!$this->ion_auth->email_check($this->input->post('email')))
            return true;
        else {
            $this->form_validation->set_message('_check_email', 'Email sudah terdaftar.');
            return false;
        }
    }

    public function request_token() {
        $phone = $this->input->post('phone');
        $return = ['status' => 'error'];
        if ($customer = $this->main->get('customers', array('phone' => $phone))) {
            $this->load->library('sprint');
            $sms = json_decode(settings('sprint_sms'), true);
            $code = rand(1000, 9999);
            $sms['m'] = 'Hai, berikut kode verifikasi TokoManaMana Anda ' . $code;
            $sms['d'] = $phone;
            $url = $sms['url'];
            unset($sms['url']);
            $sprint_response = $this->sprint->sms($url, $sms);
            $this->main->update('customers', array('verification_code' => $code, 'verification_sent_time' => date('Y-m-d H:i:s')), array('id' => $customer->id));
            $return['status'] = 'success';
        }
        echo json_encode($return);
    }
    
    public function subscription(){
        $email = $this->input->post('email');
        if(!$this->main->get('subscriptions',array('email'=>$email))){
            $this->main->insert('subscriptions',array('email'=>$email));
        }
        echo $this->template->alert('success','Terimakasih telah berlangganan!');
    }

    public function getCities($province) {
        $cities = $this->main->gets('cities', array('province' => $province), 'name ASC');
        $output = '<option value="">Pilih kota</option>';
        if ($cities) {
            foreach ($cities->result() as $city) {
                $output .= '<option value="' . $city->id . '">' . $city->type . ' ' . $city->name . '</option>';
            }
        }
        echo $output;
    }

    public function getDistricts($city) {
        $districts = $this->main->gets('districts', array('city' => $city), 'name ASC');
        $output = '<option value="">Pilih kecamatan</option>';
        if ($districts) {
            foreach ($districts->result() as $district) {
                $output .= '<option value="' . $district->id . '">' . $district->name . '</option>';
            }
        }
        echo $output;
    }

    public function get_captcha() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->load->library('session');
        $this->load->helper(array('captcha','url'));

        $this->load->helper('captcha');

        // $grid1 = rand(1, 255);
 
        $vals = array(
            'word' => '',
            'pool' => '0123456789',
            'word_length' => 4,
            'img_path'   => './assets/frontend/captcha/',
            'img_url'    => base_url().'assets/frontend/captcha/',
            'img_width'  => '200',
            'img_height' => 35,
            'border' => 0,
            'font_size' => 26, 
            'expiration' => 7200,
            'font_path' => FCPATH. 'assets/frontend/captcha/fonts/coolvetica_rg.ttf',
            'colors' => array(
                'background' => array(255, 255, 255),
                'border' => array(230, 230, 230),
                'text' => array(0, 0, 0),
                'grid' => array(255, 255, 255)
            )
        );

        // create captcha image
        $cap = create_captcha($vals);
        $this->session->set_userdata('captcha_register', $cap['word']);

        // store image html code in a variable
        $return = [
            'image' => $cap['image']
        ];

        echo json_encode($return);
    }

    public function save_register() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('googlemaps');

        $this->form_validation->set_rules('phone_register', 'No Handphone', 'trim|required|numeric|min_length[10]', [
            'required' => 'No Handphone harus diisi!',
            'numeric' => 'Harus angka!'
        ]);
        $this->form_validation->set_rules('password_register', 'Kata Sandi', 'trim|required', [
            'required' => 'Kata Sandi harus diisi!'
        ]);
        $this->form_validation->set_rules('fullname', 'Nama Lengkap', 'trim|required', [
            'required' => 'Nama Lengkap harus diisi!'
        ]);
        $this->form_validation->set_rules('captcha_register', 'Kode Captcha', 'trim|required', [
            'required' => 'Kode Captcha harus diisi!'
        ]);
        $this->form_validation->set_rules('verification_phone_register', 'Kode OTP', 'trim|required', [
            'required' => 'Kode OTP harus diisi!'
        ]);
        $message = [];

        if ($this->form_validation->run() === TRUE) {
            $data = $this->input->post(null, true);
            $back = $data['back'];
            $check_phone = str_split($data['phone_register'], 2);
            if($check_phone[0] != '08') {
                array_push($message, [
                    'message' => '<span class="help-block">No Handphone tidak valid!</span>',
                    'type' => 'phone_register'
                ]);
                $return['status'] = 'error';
                $return['message'] = $message;
                echo json_encode($return);
                exit();
            }
            if(!isset($data['gender_register'])) {
                $data['gender_register'] = '';
            }
            $data['captcha_register'];
            $captcha = $this->session->userdata('captcha_register');
            
            if($data['captcha_register'] == $captcha) {

                $check_phone = $this->main->get('customers', ['phone' => $data['phone_register']]);
                if($check_phone) {
                    array_push($message, [
                        'message' => '<span class="help-block">No Handphone sudah digunakan!</span>',
                        'type' => 'phone_register'
                    ]);
                    $return = [
                        'status' => 'error',
                        'message' => $message
                    ];
                } else {
                    $check_phone_verification = $this->main->get('phone_verification', ['phone' => $data['phone_register'], 'verification_code' => $data['verification_phone_register']]);
                    if($check_phone_verification) {
                        $lat = '';
                        $lng = '';
                        $address = [];
                        if(isset($data['lat']) && isset($data['lng'])) {
                            if($data['lat'] != '' && $data['lng'] != '') {
                                $lat = $data['lat'];
                                $lng = $data['lng'];
                                $address = $this->get_address_by_maps($lat, $lng);
                                if(!$address) {
                                    $return = [
                                        'status' => 'error_login',
                                        'message' => $this->set_message_flashdata('danger', 'Pin Alamat tidak terdeteksi!')
                                    ];
                                    echo json_encode($return);
                                    exit();
                                }
                            }
                        }
                        do {
                            $referral = $this->generateReferralCode(12);
                        } while ($this->main->get('customers', ['referral_code' => $referral])) ;
                        $register_customer = $this->ion_auth->register($data['phone_register'], $data['password_register'], '', [
                            'fullname' => $data['fullname'],
                            'birthday' => $data['birthdate_register'],
                            'gender' => $data['gender_register'],
                            'verification_phone' => 1,
                            'lat' => $lat,
                            'lng' => $lng,
                            'referral_code' => $referral,
                            'from_code' => $data['from_code']
                        ]);
                        if($data['from_code']) {
                            $customerFrom = $this->main->get('customers', ['referral_code' => $data['from_code']]);
                            if ($customerFrom) {
                                $customerCurrent = $this->main->get('customers', ['phone' => $data['phone_register']]);
                                $couponEvent = $this->main->get('coupons', ['event' => 1, 'type_for' => 'customer']);
                                if ($couponEvent) {
                                    do {
                                        $couponCode = $this->generateReferralCode(12);
                                    } while($this->main->get('coupons', ['code' => $couponCode]));
                                    $couponSend = array(
                                        'coupon_type' => $couponEvent->coupon_type,
                                        'zonaIds'  => $couponEvent->zonaIds,
                                        'catIds' => $couponEvent->catIds,
                                        'product_id' => $couponEvent->product_id,
                                        'name' => $couponEvent->name,
                                        'code' => $couponCode,
                                        'type' => $couponEvent->type,
                                        'for_product' => $couponEvent->for_product,
                                        'discount' => $couponEvent->discount,
                                        'min_order' => $couponEvent->min_order,
                                        'max_amount' => $couponEvent->max_amount,
                                        'date_start' => $couponEvent->date_start,
                                        'date_end' => $couponEvent->date_end,
                                        'uses_total' => 1,
                                        'uses_customer' => 1,
                                        'gambar' => $couponEvent->gambar,
                                        'deskripsi' => $couponEvent->deskripsi,
                                        'show' => 1,
                                        'status' => 1,
                                        'date_added' => $couponEvent->date_added,
                                        'customer_id' => $customerCurrent->id,
                                        'autofill' => $couponEvent->autofill,
                                        'ekspedisi' => $couponEvent->ekspedisi,
                                        'apply' => $couponEvent->apply,
                                        'from_code' => $data['from_code'],
                                        'event' => 0,
                                        'cityIds' => $couponEvent->cityIds
                                    );
    
                                    $this->main->insert('coupons', $couponSend);
                                }
                            }
                        }
                        if(count($address) > 0) {
                            if($address['province'] != '' && $address['city'] != '' && $address['district'] != '' && $address['full_address'] != '' && $address['postcode'] != '') {
                                $temp_province = $address['province'];
                                $this->db->like('name', $temp_province);
                                $province = $this->db->get('provincies')->row();
                                if($province) {
                                    $id_province = $province->id;
                                    $temp_city = explode(' ', $address['city']);
                                    if(count($temp_city) > 1) {
                                        $type_city = $temp_city[0];
                                        $name_city = $temp_city[1];
                                    } else {
                                        $type_city = '';
                                        $name_city = $temp_city[0];
                                    }
                                    $this->db->like('name', $name_city);
                                    $this->db->like('type', $type_city);
                                    $this->db->where('province', $id_province);
                                    $city = $this->db->get('cities')->row();
                                    if($city) {
                                        $id_city = $city->id;
                                        $temp_disctrict = explode(' ', $address['district']);
                                        if(count($temp_disctrict) > 1) {
                                            $name_district = $temp_disctrict[1];
                                        } else {
                                            $name_district = $temp_disctrict[0];
                                        }
                                        $this->db->like('name', $name_district)->where('city', $id_city);
                                        $district = $this->db->get('districts')->row();
                                        if($district) {
                                            $id_disctrict = $district->id;
                                            $postcode = $address['postcode'];
                                            $full_address = $address['full_address'];
                                            $insert_address = [
                                                'customer' => $register_customer,
                                                'title' => 'Rumah',
                                                'phone' => $data['phone_register'],
                                                'address' => $full_address,
                                                'city' => $id_city,
                                                'postcode' => $postcode,
                                                'province' => $id_province,
                                                'district' => $id_disctrict,
                                                'primary' => 1,
                                                'latitude' => $lat,
                                                'longitude' => $lng,
                                                'name' => $data['fullname']
                                            ];
                                            $this->main->insert('customer_address', $insert_address);
                                        }
                                    }
                                }
                            }
                        }
                        if ($this->ion_auth->login($data['phone_register'], $data['password_register'])) {
                            $return = [
                                'status' => 'success',
                                'message' => $back
                            ]; 
                        } else {
                            $return = [
                                'status' => 'error_login',
                                'message' => $this->template->alert('danger', 'Gagal masuk, Silahkan masuk dengan mengklik tombol masuk!')
                            ];
                        }
                    } else {
                        array_push($message, [
                            'message' => '<span class="help-block">Kode OTP salah!</span>',
                            'type' => 'verification_phone_register'
                        ]);
                        $return = [
                            'status' => 'error',
                            'message' => $message
                        ];
                    }
                }

            } else {
                array_push($message, [
                    'message' => '<span class="help-block">Kode Captcha tidak sama!</span>',
                    'type' => 'captcha_register'
                ]);
                $return = [
                    'status' => 'error',
                    'message' => $message
                ];
            }

        } else {
            if(form_error('phone_register')) {
                array_push($message, [
                    'message' => form_error('phone_register'),
                    'type' => 'phone_register'
                ]);
            }
            if(form_error('password_register')) {
                array_push($message, [
                    'message' => form_error('password_register'),
                    'type' => 'password_register'
                ]);
            }
            if(form_error('fullname')) {
                array_push($message, [
                    'message' => form_error('fullname'),
                    'type' => 'fullname'
                ]);
            }
            if(form_error('captcha_register')) {
                array_push($message, [
                    'message' => form_error('captcha_register'),
                    'type' => 'captcha_register'
                ]);
            }
            if(form_error('verification_phone_register')) {
                array_push($message, [
                    'message' => form_error('verification_phone_register'),
                    'type' => 'verification_phone_register'
                ]);
            }
            $return = [
                'status' => 'error',
                'message' => $message
            ];   
        }

        echo json_encode($return);
        
    }

    private function get_address_by_maps($lat,$lng)
      {
         $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($lng).'&sensor=false&key=AIzaSyCJMa9h-arzHI-UQh8IVLnLTk0XXhOsVi8';
         $json = @file_get_contents($url);
         $data = json_decode($json);
         $status = $data->status;
         $return = [];
         if($status == "OK")
         {
           // return $data->results[0]->formatted_address;
            // $district = $data->results[0]->address_components[1]->long_name;
            // $city = $data->results[0]->address_components[2]->long_name;
            // $province = $data->results[0]->address_components[3]->long_name;
            // $postcode = $data->results[0]->address_components[5]->long_name;
            $full_address = $data->results[0]->formatted_address;
            $district = '';
            $city = '';
            $province = '';
            $postcode = '';
            foreach($data->results[0]->address_components as $data) {
                if($data->types[0] == 'administrative_area_level_3') {
                    $district = $data->long_name;
                }
                if($data->types[0] == 'administrative_area_level_2') {
                    $city = $data->long_name;
                }
                if($data->types[0] == 'administrative_area_level_1') {
                    $province = $data->long_name;
                }
                if($data->types[0] == 'postal_code') {
                    $postcode = $data->long_name;
                }
            }
            $return = [
                'district' => $district,
                'city' => $city,
                'province' => $province,
                'full_address' => $full_address,
                'postcode' => $postcode
            ];
            return $return;
         }
         else
         {
           return false;
         }
      }

    public function check_phone_register() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $data = $this->input->post(null, true);
        $phone = $data['phone'];
        $check_phone = str_split($phone, 2);
        if($check_phone[0] == '08') {
            if(strlen($phone) >= 10) {
                $check_phone = $this->main->get('customers', ['phone' => $phone]);
                $return = '';
                if(!$check_phone) {
                    $return = 'available';
                } else {
                    $return = 'not_available';
                }
            } else {
                $return = 'error';
            }
        } else {
            $return = 'error';
        }
        echo $return;
    }

    public function check_phone_guest() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);

        $check_user = $this->main->get('customers', ['email' => $data['email'], 'phone' => $data['phone'], 'type' => 'g']);
        if ($check_user) {
            $return = 'available';
        } else {
            $check_phone = $this->main->get('customers', ['phone' => $data['phone']]);
            if ($check_phone) {
                $return = 'not_available';
            } else {
                $return = 'available';
            }
        }
        echo $return;
    }

    public function send_otp_register() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $data = $this->input->post(null, true);
        $return = [];
        $phone = $data['phone'];
        $ip_address = $this->input->ip_address();
        $customer = $this->main->get('phone_verification', array('phone' => $phone));
        $this->load->library('sprint');
        $code = rand(1000, 9999);

        $customer_ip_address = $this->main->gets('phone_verification', ['ip_address' => $ip_address], 'verification_sent_time DESC');
        if($customer_ip_address) {
            $now = time();
            $verification_sent_time_ip_address = strtotime($customer_ip_address->row()->verification_sent_time);
            $range = $now - $verification_sent_time_ip_address;
            if($range < 60) {
                $return['status'] = 'error';
                $return['message'] = $this->set_message_flashdata('danger', 'Silahkan tunggu beberapa saat untuk mengirimkan ulang kode verifikasi!');
                echo json_encode($return);
                exit();
            }
        }

        if ($customer) {
            $now = time();
            $verification_sent_time = strtotime($customer->verification_sent_time);
            $range = $now - $verification_sent_time;
            if($range < 120) {
                $return['status'] = 'error';
                $return['message'] = $this->set_message_flashdata('danger', 'Silahkan tunggu beberapa saat untuk mengirimkan ulang kode verifikasi!');
            } else {
                $sms = json_decode(settings('sprint_sms'), true);
                $sms['m'] = 'Hai, berikut kode verifikasi TokoManaMana anda ' . $code . ' jangan sampai orang lain mengetahuinya!';
                $sms['d'] = $phone;
                // $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                $this->send_sms($sms);
                $this->main->update('phone_verification', array('verification_code' => $code, 'verification_sent_time' => date('Y-m-d H:i:s')), array('id' => $customer->id));
                $return['status'] = 'success';
                $return['message'] = $this->set_message_flashdata('success', 'Kode OTP Berhasil dikirim!');;
            }
        } else {
            $sms = json_decode(settings('sprint_sms'), true);
            $sms['m'] = 'Hai, berikut kode verifikasi TokoManaMana anda ' . $code . ' jangan sampai orang lain mengetahuinya!';
            $sms['d'] = $phone;
            // $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
            $this->send_sms($sms);
            $data_insert = [
                'phone' => $phone,
                'verification_code' => $code,
                'verification_sent_time' => date('Y-m-d H:i:s'),
                'ip_address' => $ip_address
            ];
            $this->main->insert('phone_verification', $data_insert);
            $return['status'] = 'success';
            $return['message'] = $this->set_message_flashdata('success', 'Kode OTP Berhasil dikirim!');
        }
        
        echo json_encode($return);
    }

    private function send_sms($sms) {
        $this->load->library('sprint');
        $url = $sms['url'];
        $to = $sms['d'];
        unset($sms['url']);
        $result = $this->sprint->sms($url, $sms);
        $result = explode('_', $result);
        if ($result[0] == 0) {
            $this->main->insert('log_sms', array('content' => json_encode($sms), 'to' => $to));
        }
    }

    // private function send_email($email) {
    //     $emails = $this->db->where('type', 'email')->get('cron_job');
    //     if (send_mail($email->from, $email->from_name, $email->to, $email->subject, $email->message)) {
    //         $this->main->delete('cron_job', array('id' => $id));
    //     }
    // }

    public function new_login() {
        $this->input->is_ajax_request() or redirect('');

        $this->form_validation->set_rules('identity_login', 'No Handphone / Email', 'trim|required', [
            'required' => 'No Handphone / Email harus diisi!'
        ]);
        $this->form_validation->set_rules('password_login', 'Kata Sandi', 'trim|required', [
            'required' => 'Kata Sandi harus diisi!'
        ]);
        $message = [];
        if ($this->form_validation->run() == TRUE) {
            
            $data = $this->input->post(null, true);
            if(isset($data['remember_me'])) {
                $remember = (bool) $data['remember_me'];
            } else {
                $remember = false;
            }
            if(filter_var($data['identity_login'], FILTER_VALIDATE_EMAIL)) {
                $customer_email = $this->main->get('customers', ['email' => $data['identity_login']]);
                if($customer_email) {
                    if($this->bcrypt->verify($data['password_login'], $customer_email->password)) {
                        if($customer_email->phone) {
                            $this->ion_auth_model->set_session($customer_email);
                            $this->ion_auth_model->update_last_login($customer_email->id);
                            if($remember) {
                                $remember_code = $this->ion_auth_model->salt();
                                $this->main->update('customers', ['remember_code' => $remember_code], ['id' => $customer_email->id]);
                                set_cookie('remember_me', $remember_code, 3600*24*7);
                            }
                            $return = [
                                'status' => 'success',
                                'message' => $data['back']
                            ];
                        } else {
                            $login_email_no_phone = $this->login_email_no_phone($customer_email);
                            if($login_email_no_phone) {
                                if($remember) {
                                    $remember_code = $this->ion_auth_model->salt();
                                    $this->main->update('customers', ['remember_code' => $remember_code], ['id' => $customer_email->id]);
                                    set_cookie('remember_me', $remember_code, 3600*24*7);
                                }
                                // $redirect_link = site_url('member/profile');
                                // $this->session->set_flashdata('login_email_not_phone', "<script>
                                //     $('body').load(window.location.href, function() {
                                //         Swal.fire({
                                //             title: 'Tidak ada No Handphone!',
                                //             type: 'error',
                                //             text: 'Anda harus mengisi No Handphone agar bisa berbelanja!',
                                //             confirmButtonText: 'Isi No Handphone',
                                //             cancelButtonText: 'Tutup',
                                //             showCancelButton: true,
                                //             cancelButtonColor: '#d33'
                                //         }).then((result) => {
                                //             if(result.value) {
                                //                 document.location.href = '$redirect_link';
                                //             }
                                //         });
                                //     });
                                // </script>");
                                $this->session->set_flashdata('login_email_not_phone', true);
                                $return = [
                                    'status' => 'success_no_phone',
                                    'message' => $data['back']
                                ];
                            } else {
                                $return = [
                                    'status' => 'error_validation',
                                    'message' => $this->set_message_flashdata('danger', 'Terjadi suatu kesalahan!')
                                ];
                            }
                        }
                    } else {
                        $return['status'] = 'error_validation';
                        $return['message'] = $this->set_message_flashdata('danger', 'Password salah!');
                    }
                } else {
                    $return['status'] = 'error_validation';
                    $return['message'] = $this->set_message_flashdata('danger', 'Email tidak terdaftar!. Silahkan daftar terlebih dahulu!');
                }
            } elseif (is_numeric($data['identity_login']) && strlen($data['identity_login']) >= 10) {
                $check_phone = str_split($data['identity_login'], 2);
                if($check_phone[0] != '08') {
                    array_push($message, [
                        'type' => 'identity_login',
                        'message' => '<span class="help-block">No Handphone tidak valid!</span>'
                    ]);
                    $return['status'] = 'error';
                    $return['message'] = $message;
                    echo json_encode($return);
                    exit();
                }
                $customer_phone = $this->main->get('customers', ['phone' => $data['identity_login']]);
                if($customer_phone) {
                    if($this->bcrypt->verify($data['password_login'], $customer_phone->password)) {
                        $check = $this->send_sms_verification($customer_phone->id);
                        if($check) {
                            if($remember) {
                                $remember_code = $this->ion_auth_model->salt();
                                $this->main->update('customers', ['remember_code' => $remember_code], ['id' => $customer_phone->id]);
                                set_cookie('remember_me', $remember_code, 3600*24*7);
                            }
                            $return['status'] = 'success_phone';
                            $return['id_customer'] = encode($customer_phone->id);
                            $return['message'] = $this->set_message_flashdata('success', 'Kode verifikasi berhasil dikirim!');
                        } else {
                            $return['status'] = 'error_phone';
                            $return['message'] = $this->set_message_flashdata('danger', 'Silahkan tunggu beberapa saat untuk mengirim ulang kode verifikasi!');
                            $return['id_customer'] = encode($customer_phone->id);
                            $return['duration'] = $check;
                        }
                    } else {
                        $return['status'] = 'error_validation';
                        $return['message'] = $this->set_message_flashdata('danger', 'Password salah!');
                    }
                } else {
                    $phone_else = substr($data['identity_login'], 1);
                    $customer_phone_else = $this->main->get('customers', ['phone' => $phone_else]);
                    if($customer_phone_else) {
                        $this->main->update('customers', ['phone' => $data['identity_login']], ['id' => $customer_phone_else->id]);
                        $check = $this->send_sms_verification($customer_phone_else->id);
                        if($check) {
                            if($remember) {
                                $remember_code = $this->ion_auth_model->salt();
                                $this->main->update('customers', ['remember_code' => $remember_code], ['id' => $customer_phone_else->id]);
                                set_cookie('remember_me', $remember_code, 3600*24*7);
                            }
                            $return['status'] = 'success_phone';
                            $return['id_customer'] = encode($customer_phone_else->id);
                            $return['message'] = $this->set_message_flashdata('success', 'Kode verifikasi berhasil dikirim!');
                        } else {
                            $return['status'] = 'error_phone';
                            $return['message'] = $this->set_message_flashdata('danger', 'Silahkan tunggu beberapa saat untuk mengirim ulang kode verifikasi!');
                            $return['id_customer'] = encode($customer_phone_else->id);
                            $return['duration'] = $check;
                        }
                    } else {
                        $return['status'] = 'error_validation';
                        $return['message'] = $this->set_message_flashdata('danger', 'No Handphone tidak terdaftar!. Silahkan daftar terlebih dahulu!');
                    }
                }
            } else {
                $return['status'] = 'error_validation';
                $return['message'] = $this->set_message_flashdata('danger', 'No Handphone atau Email tidak valid!');
            }

        } else {
            if(form_error('identity_login')) {
                array_push($message, [
                    'message' => form_error('identity_login'),
                    'type' => 'identity_login'
                ]);
            }
            if(form_error('password_login')) {
                array_push($message, [
                    'message' => form_error('password_login'),
                    'type' => 'password_login'
                ]);
            }
            if(form_error('verification_phone_login')) {
                array_push($message, [
                    'message' => form_error('verification_phone_login'),
                    'type' => 'verification_phone_login'
                ]);
            }
            $return = [
                'status' => 'error',
                'message' => $message
            ];
        }
        echo json_encode($return);
    }

    private function login_email_no_phone($user) {
        $this->ion_auth_model->trigger_events('pre_set_session');

        $session_data = array(
            'identity' => $user->email,
            $this->ion_auth_model->identity_column => $user->{$this->ion_auth_model->identity_column},
            'email' => $user->email,
            'user_id' => $user->id, //everyone likes to overwrite id so we'll use user_id
            'old_last_login' => $user->last_login,
            'last_check' => time(),
        );

        $this->session->set_userdata($session_data);

        $this->ion_auth_model->trigger_events('post_set_session');

        $this->ion_auth_model->update_last_login($user->id);

        return TRUE;
    }

    public function send_otp_login() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $data = $this->input->post(null, true);
        $return = [];
        if(isset($data['type']) && $data['type'] == 'forgot_password') {
            $phone = decode($data['phone']);
            $check = $this->main->get('customers', ['phone' => $phone]);
        } elseif(isset($data['id'])) {
            $id = decode($data['id']);
            $check = $this->main->get('customers', ['id' => $id]);
        } else {
            $phone = $data['phone'];
            $check = $this->main->get('customers', ['phone' => $phone]);
        }
        $code = rand(1000, 9999);
        if($check) {
            $phone = $check->phone;
            if($check->verification_code && $check->verification_sent_time) {
                $now = time();
                $verification_sent_time = strtotime($check->verification_sent_time);
                $range = $now - $verification_sent_time;
                if($range < 120) {
                    $return['status'] = 'error';
                    $return['message'] = $this->set_message_flashdata('danger', 'Silahkan tunggu beberapa saat untuk mengirim ulang kode verifikasi!');
                } else {
                    $this->load->library('sprint');
                    $sms = json_decode(settings('sprint_sms'), true);
                    $sms['m'] = 'Hai, berikut kode verifikasi TokoManaMana anda ' . $code . ' jangan sampai orang lain mengetahuinya!';
                    $sms['d'] = $phone;
                    // $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                    $this->send_sms($sms);
                    
                    $this->main->update('customers', ['verification_code' => $code, 'verification_sent_time' => date('Y-m-d H:i:s')], ['phone' => $phone]);
                    $return['message'] = $this->set_message_flashdata('success', 'Kode verifikasi berhasil dikirim!');
                }
            } else {
                $this->load->library('sprint');
                $sms = json_decode(settings('sprint_sms'), true);
                $sms['m'] = 'Hai, berikut kode verifikasi TokoManaMana anda ' . $code . ' jangan sampai orang lain mengetahuinya!';
                $sms['d'] = $phone;
                // $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                $this->send_sms($sms);
                
                $this->main->update('customers', ['verification_code' => $code, 'verification_sent_time' => date('Y-m-d H:i:s')], ['phone' => $phone]);
                $return['message'] = $this->set_message_flashdata('success', 'Kode verifikasi berhasil dikirim!');
            }
        } else {
            $return['status'] = 'error';
            $return['message'] = $this->set_message_flashdata('danger', 'Akun tidak ada!');
        }
        echo json_encode($return);
    }

    public function login_verification_phone() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $id = decode($data['id']);
        if($data['verification_code'] != '') {
            $customer = $this->main->get('customers', ['id' => $id, 'verification_code' => $data['verification_code']]);
            if($customer) {
                if($customer->verification_code == $data['verification_code']) {
                    $this->ion_auth_model->set_session($customer);
                    $this->ion_auth_model->update_last_login($customer->id);
                    $this->main->update('customers', ['verification_code' => '', 'verification_sent_time' => NULL], ['id' => $customer->id]);
                    if($data['id_merchant'] != "false") {
                        $return['status'] = 'success_merchant';
                        $return['message'] = $data['back'];
                        $return['id_merchant'] = $data['id_merchant'];
                    } else {
                        $return = [
                            'status' => 'success',
                            'message' => $data['back']
                        ];
                    }
                } else {
                    $return['status'] = 'error';
                    $return['message'] = $this->set_message_flashdata('danger', 'Kode verifikasi salah!');
                }
            } else {
                $return['status'] = 'error';
                $return['message'] = $this->set_message_flashdata('danger', 'Kode verifikasi salah!');
            }
        } else {
            $return['status'] = 'error';
            $return['message'] = $this->set_message_flashdata('danger', 'Kode verifikasi harus diisi!');
        }
        echo json_encode($return);
    }

    public function login_verification_phone_merchant() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $id = decode($data['id']);
        if($data['verification_code'] != '') {
            $customer = $this->main->get('customers', ['id' => $id, 'verification_code' => $data['verification_code']]);
            if($customer) {
                if($this->ion_auth->logged_in()) {
                    $this->ion_auth->logout();
                    $this->session->unset_userdata('user');
                }
                $this->ion_auth_model->set_session($customer);
                $this->ion_auth_model->update_last_login($customer->id);
                $this->main->update('customers', ['verification_code' => '', 'verification_sent_time' => NULL], ['id' => $customer->id]);
                $return['status'] = 'success_merchant';
                $return['message'] = $data['back'];
                $return['id_merchant'] = $data['id_merchant'];
            } else {
                $return['status'] = 'error';
                $return['message'] = $this->set_message_flashdata('danger', 'Kode verifikasi salah!');
            }
        } else {
            $return['status'] = 'error';
            $return['message'] = $this->set_message_flashdata('danger', 'Kode verifikasi harus diisi!');
        }
        echo json_encode($return);
    }

    private function set_message_flashdata($type, $message) {
        $return = '<div class="alert alert-' . $type . '" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <b>' . $message . '</b>
                  </div>';
        return $return;
    }

    public function check_account_reset() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('phone_reset_password', 'No Handphone / Email', 'trim|required');
        if ($this->form_validation->run() == TRUE) {
            $data = $this->input->post(null, true);
            $identity = $data['phone_reset_password'];
            if(filter_var($data['phone_reset_password'], FILTER_VALIDATE_EMAIL)) {
                $check_user = $this->main->get('customers', ['email' => $identity]);
                if($check_user) {
                    if($check_user->email_verification != 1) {
                        $return['status'] = 'error';
                        $return['message'] = $this->set_message_flashdata('danger', 'Email belum diverifikasi!. Silahkan gunakan No Handphone yang terdaftar!');
                        echo json_encode($return);
                        exit();
                    } 
                }
            } elseif (is_numeric($data['phone_reset_password'])) {
                $check_user = $this->main->get('customers', ['phone' => $identity]);
            } else {
                $return['status'] = 'error';
                $return['message'] = $this->set_message_flashdata('danger', 'Masukkan No Handphone / Email yang valid!');
                echo json_encode($return);
                exit();
            }
            $return = [];
            if($check_user) {
                $message = [];
                if($check_user->email_verification == 1) {
                    $len = strstr($check_user->email, '@', true);
                    $len2 = str_split($len);
                    $len3 = '';
                    for($i = 0; $i < count($len2); $i++) {
                        if($i == 0 || $i == count($len2) - 1) {
                            $len3 .= $len2[$i];
                        } else {
                            $len3 .= '*';
                        }
                    }
                    $domain = strstr($check_user->email, '@');
                    $domain2 = str_split($domain);
                    $domain3 = '';
                    for($i = 0; $i < count($domain2); $i++) {
                        if(($i >= 0 && $i <= 2) || $i == count($domain2) - 1) {
                            $domain3 .= $domain2[$i];
                        } else {
                            $domain3 .= '*';
                        }
                    }
                    $censor_email = $len3 . $domain3;
                    array_push($message, [
                        'type' => encode('email'),
                        'type2' => 'email',
                        'message' => $censor_email,
                        'id_customer' => encode($check_user->id)
                    ]);
                    array_push($message, [
                        'type' => encode('phone'),
                        'type2' => 'phone',
                        'message' => $check_user->phone,
                        'id_customer' => encode($check_user->id)
                    ]);
                } else {
                    array_push($message, [
                        'type' => encode('phone'),
                        'type2' => 'phone',
                        'message' => $check_user->phone,
                        'id_customer' => encode($check_user->id)
                    ]);
                }
                $return['message'] = $message;
                $return['status'] = 'success';
            } else {
                $return['status'] = 'error';
                $return['message'] = $this->set_message_flashdata('danger', 'Akun tidak terdaftar!');
            }
        } else {
            $return['status'] = 'error_validation';
            $return['message'] = form_error('phone_reset_password');
        }

        echo json_encode($return);
        
    }

    public function send_verification_reset_password() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $id = decode($this->input->post('id', true));
        $type = decode($this->input->post('type', true));
        $customer = $this->main->get('customers', ['id' => $id]);
        $return = [];
        if($customer) {
            if($type == 'phone') {
                $check = $this->send_sms_verification($id);
                if($check) {
                    $return['status'] = 'phone_success';
                    $return['phone'] = encode($customer->phone);
                } else {
                    $return['status'] = 'error_phone';
                    $return['phone'] = encode($customer->phone);
                    $return['message'] = $this->set_message_flashdata('danger', 'Silahkan tunggu beberapa saat untuk mengirim ulang kode verifikasi!');
                }
            } elseif($type == 'email') {
                $return['status'] = 'email_success';
                if($customer->email_verification) {
                    $send_email = $this->send_reset_password_by_email($id);
                    if($send_email) {
                        $return['status'] = 'email_success';
                        $return['message'] = 'Silahkan cek email untuk mengganti password anda!';
                    } else {
                        $return['status'] = 'error';
                        $return['message'] = $this->set_message_flashdata('danger', 'Silahkan tunggu beberapa saat!');
                    }
                } else {
                    $return['status'] = 'error';
                    $return['message'] = $this->set_message_flashdata('danger', 'Email belum diverifikasi!');
                }
            } else {
                $return['status'] = 'error';
                $return['message'] = $this->set_message_flashdata('danger', 'Terjadi suatu kesalahan!');
            }
        } else {
            $return['status'] = 'error';
            $return['message'] = $this->set_message_flashdata('danger', 'Terjadi suatu kesalahan!');
        }
        echo json_encode($return);
    }

    private function send_sms_verification($id = null) {
        $get_customer = $this->main->get('customers', ['id' => $id]);
        $code = rand(1000, 9999);
        date_default_timezone_set('Asia/Jakarta');
        
        if($get_customer->verification_sent_time && $get_customer->verification_code) {
            $now = time();
            $verification_sent_time = strtotime($get_customer->verification_sent_time);
            $range = $now - $verification_sent_time;
            if($range < 120) {
                return false;
            } else {
                $sms = json_decode(settings('sprint_sms'), true);
                $sms['m'] = 'Hai, berikut kode verifikasi TokoManaMana anda ' . $code . ' jangan sampai orang lain mengetahuinya!';
                $sms['d'] = $get_customer->phone;
                // $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                $this->send_sms($sms);
                $this->main->update('customers', ['verification_code' => $code, 'verification_sent_time' => date('Y-m-d H:i:s')], ['id' => $id]);
                return true;
            }
        } else {
            $sms = json_decode(settings('sprint_sms'), true);
            $sms['m'] = 'Hai, berikut kode verifikasi TokoManaMana anda ' . $code . ' jangan sampai orang lain mengetahuinya!';
            $sms['d'] = $get_customer->phone;
            // $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
            $this->send_sms($sms);
            $this->main->update('customers', ['verification_code' => $code, 'verification_sent_time' => date('Y-m-d H:i:s')], ['id' => $id]);
            return true;
        }
    }

    public function submit_phone_verification() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $phone = decode($data['phone']);
        $verification_code = $data['verification_code'];
        if($verification_code != '') {
            $customer = $this->main->get('customers', ['phone' => $phone]);
            if($customer) {
                if($customer->verification_code == $data['verification_code']) {
                    $return['status'] = 'success';
                    $return['id_customer'] = encode($customer->id);
                } else {
                    $return['status'] = 'error';
                    $return['message'] = $this->set_message_flashdata('danger', 'Kode verifikasi salah!');
                }
            } else {
                $return['status'] = 'error';
                $return['message'] = $this->set_message_flashdata('danger', 'No Handphone tidak terdaftar!');
            }
        } else {
            $return['status'] = 'error';
            $return['message'] = $this->set_message_flashdata('danger', 'Kode verifikasi harus diisi!');
        }

        echo json_encode($return);
    }

    public function change_password_by_phone() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $new_password = htmlspecialchars($data['new_password']);
        $confirm_new_password = htmlspecialchars($data['confirm_new_password']);
        $id_customer = decode($data['id_customer']);
        if($new_password != '' || $confirm_new_password != '') {
            if($new_password == $confirm_new_password) {
                if(strlen($new_password) >= 6) {
                    $customer = $this->main->get('customers', ['id' => $id_customer]);
                    if($customer) {
                        if ($this->ion_auth->reset_password($customer->phone, $confirm_new_password)) {
                            $return['message'] = 'Password berhasil diubah. Silahkan masuk!';
                            $return['status'] = 'success';
                            $this->main->update('customers', ['verification_code' => null, 'verification_sent_time' => NULL], ['id' => $id_customer]);
                        } else {
                            $return['message'] = $this->set_message_flashdata('danger', $this->ion_auth->errors());
                            $return['status'] = 'error';
                        }
                    } else {
                        $return['status'] = 'error';
                        $return['message'] = $this->set_message_flashdata('danger', 'User tidak ada!');
                    }
                } else {
                    $return['status'] = 'error';
                    $return['message'] = $this->set_message_flashdata('danger', 'Password minimal 6 karakter!');
                }
            } else {
                $return['status'] = 'error';
                $return['message'] = $this->set_message_flashdata('danger', 'Konfirmasi password tidak sama!');
            }
        } else {
            $return['status'] = 'error';
            $return['message'] = $this->set_message_flashdata('danger', 'Salah satu isian tidak boleh kosong!');
        }
        echo json_encode($return);
    }

    private function send_reset_password_by_email($id) {

        $customer = $this->main->get('customers', ['id' => $id, 'email_verification' => 1]);
        if($customer) {
            if($customer->forgotten_password_time) {
                $now = time();
                $forgotten_password_time = $customer->forgotten_password_time;
                $range = $now - $forgotten_password_time;
                if($range < 120) {
                    return false;
                    exit();
                }
            } 
            // $forgotten = $this->ion_auth->forgotten_password($customer->phone);
            $token = bin2hex(random_bytes(16));
            $data = [
                'user' => $customer,
                // 'password_code' => $forgotten['forgotten_password_code']
                'password_code' => $token
            ];
            $message = $this->load->view('email/forgot_password', $data, TRUE);
            $this->main->update('customers', ['forgotten_password_code' => $token, 'forgotten_password_time' => time()], ['id' => $id]);

            $cronjob = array(
                'from' => settings('send_email_from'),
                'from_name' => settings('store_name'),
                'to' => $customer->email,
                'subject' => 'Reset Password ' . settings('store_name'),
                'message' => $message
            );
            $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
            return true;
        } else {
            return false;
        }
    }

    public function new_login_merchant() {
        $this->input->is_ajax_request() or redirect('');

        $this->form_validation->set_rules('identity_login_merchant', 'No Handphone / Email', 'trim|required', [
            'required' => 'No Handphone / Email harus diisi!'
        ]);
        $this->form_validation->set_rules('password_login_merchant', 'Kata Sandi', 'trim|required', [
            'required' => 'Kata Sandi harus diisi!'
        ]);

        $message = [];
        if ($this->form_validation->run() == TRUE) {
            $data = $this->input->post(null, true);
            if($this->ion_auth->logged_in()) {
                $this->ion_auth->logout();
                $this->session->unset_userdata('user');
            }
            if(filter_var($data['identity_login_merchant'], FILTER_VALIDATE_EMAIL)) {
                $merchant_email = $this->main->get('merchant_users', ['email' => $data['identity_login_merchant']]);
                if($merchant_email) {
                    if($this->bcrypt->verify($data['password_login_merchant'], $merchant_email->password)) {
                        $check_phone_merchant = $this->main->get('customers', ['phone' => $merchant_email->phone]);
                        if($check_phone_merchant) {
                            $this->main->update('customers', ['type' => 'm'], ['id' => $check_phone_merchant->id]);
                            if ($this->ion_auth->login($merchant_email->phone, $data['password_login_merchant'])) {
                                $return = [
                                    'status' => 'success',
                                    'message' => $data['back_merchant'],
                                    'id_merchant' => encode($merchant_email->id)
                                ]; 
                            } else {
                                $return = [
                                    'status' => 'error_validation',
                                    'message' => $this->template->alert('danger', 'Gagal masuk!')
                                ];
                            }
                        } else {
                            $register_new_customer = $this->ion_auth->register($merchant_email->phone, $data['password_login_merchant'], $merchant_email->email, [
                                'fullname' => $merchant_email->fullname,
                                'birthday' => $merchant_email->birthday,
                                'verification_phone' => 1,
                                'type' => 'm',
                                'gender' => '',
                                'email_verification' => $merchant_email->token_confirm_status
                            ]);
                            if ($this->ion_auth->login($merchant_email->phone, $data['password_login_merchant'])) {
                                $return = [
                                    'status' => 'success',
                                    'message' => $data['back_merchant'],
                                    'id_merchant' => encode($merchant_email->id)
                                ]; 
                            } else {
                                $return = [
                                    'status' => 'error_validation',
                                    'message' => $this->template->alert('danger', 'Gagal masuk!')
                                ];
                            }
                        }
                    } else {
                        $return['status'] = 'error';
                        array_push($message, [
                            'type' => 'password_login_merchant',
                            'message' => '<span class="help-block">Password salah!</span>'
                        ]);
                        $return['message'] = $message;
                    }
                } else {
                    $return['status'] = 'error_validation';
                    $return['message'] = $this->set_message_flashdata('danger', 'Akun merchant tidak ada!. Silahkan daftar terlebih dahulu!');
                }
            } elseif (is_numeric($data['identity_login_merchant']) && strlen($data['identity_login_merchant']) >= 10) {
                $merchant_phone = $this->main->get('merchant_users', ['phone' => $data['identity_login_merchant']]);
                if($merchant_phone) {
                    if($this->bcrypt->verify($data['password_login_merchant'], $merchant_phone->password)) {
                        $check_phone_user = $this->main->get('customers', ['phone' => $merchant_phone->phone]);
                        if($check_phone_user) {
                            $this->main->update('customers', ['type' => 'm'], ['id' => $check_phone_user->id]);
                            $check = $this->send_sms_verification($check_phone_user->id);
                            if($check) {
                                $return['status'] = 'success_phone';
                                $return['id_customer'] = encode($check_phone_user->id);
                                $return['id_merchant'] = encode($merchant_phone->id);
                                $return['message'] = $this->set_message_flashdata('success', 'Kode verifikasi berhasil dikirim!');
                            } else {
                                $return['status'] = 'error_phone';
                                $return['message'] = $this->set_message_flashdata('danger', 'Silahkan tunggu beberapa saat untuk mengirim ulang kode verifikasi!');
                                $return['id_customer'] = encode($check_phone_user->id);
                                $return['id_merchant'] = encode($merchant_phone->id);
                                $return['duration'] = $check;
                            }
                        } else {
                            $register_new_customer = $this->ion_auth->register($merchant_phone->phone, $data['password_login_merchant'], $merchant_phone->email, [
                                'fullname' => $merchant_phone->fullname,
                                'birthday' => $merchant_phone->birthday,
                                'verification_phone' => 1,
                                'type' => 'm',
                                'gender' => '',
                                'email_verification' => $merchant_phone->token_confirm_status
                            ]);
                            $check = $this->send_sms_verification($register_new_customer);
                            if($check) {
                                $return['status'] = 'success_phone';
                                $return['id_customer'] = encode($register_new_customer);
                                $return['id_merchant'] = encode($merchant_phone->id);
                                $return['message'] = $this->set_message_flashdata('success', 'Kode verifikasi berhasil dikirim!');
                            } else {
                                $return['status'] = 'error_phone';
                                $return['message'] = $this->set_message_flashdata('danger', 'Silahkan tunggu beberapa saat untuk mengirim ulang kode verifikasi!');
                                $return['id_customer'] = encode($register_new_customer);
                                $return['id_merchant'] = encode($merchant_phone->id);
                                $return['duration'] = $check;
                            }
                        }
                    } else {
                        $return['status'] = 'error';
                        array_push($message, [
                            'type' => 'password_login_merchant',
                            'message' => '<span class="help-block">Password salah!</span>'
                        ]);
                        $return['message'] = $message;
                    }
                } else {
                    $return['status'] = 'error_validation';
                    $return['message'] = $this->set_message_flashdata('danger', 'Akun merchant tidak ada!. Silahkan daftar terlebih dahulu!');
                }
            } else {
                $return['status'] = 'error_validation';
                $return['message'] = $this->set_message_flashdata('danger', 'No Handphone / Email tidak valid!');
            }
        } else {
            if(form_error('identity_login_merchant')) {
                array_push($message, [
                    'type' => 'identity_login_merchant',
                    'message' => form_error('identity_login_merchant')
                ]);
            }
            if(form_error('password_login_merchant')) {
                array_push($message, [
                    'type' => 'password_login_merchant',
                    'message' => form_error('password_login_merchant')
                ]);
            }
            $return['status'] = 'error';
            $return['message'] = $message;
        }
        echo json_encode($return);
    }

    public function logout_merchant() {
        $this->input->is_ajax_request() or exit(redirect('error_403'));
        $data = $this->input->post(null, true);
        $back = $data['back'];
        $this->ion_auth->logout();
        $this->session->unset_userdata('user');
        echo $back;
    }

    private function generateReferralCode($length_of_string) { 
        $str_result = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'; 
        return substr(str_shuffle($str_result), 0, $length_of_string); 
    }

}
