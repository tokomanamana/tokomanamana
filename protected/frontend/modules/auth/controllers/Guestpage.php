<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Guestpage extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->helper(array('form', 'url'));
        $this->load->model('Model');


        $this->load->library('form_validation');
        $this->load->model('member_model', 'member');
        $this->load->model('orders');
        $this->load->library('breadcrumb');
        $this->load->language('member', settings('language'));
        $this->load->library('pagination');
     }

       public function guest_history($id,$tab = 'all') {
        //pagination
        $config['base_url'] = site_url('guestpage/guest_history/' . $id. '/' . $tab . '/');
        $id = decode($id);
        
        $config['total_rows'] = $this->member->getOrders($id, '', '')->num_rows();
        $config['per_page'] = 8;
        $this->pagination->initialize($config);
        $start = ($this->input->get('page')) ? ($this->input->get('page') - 1) * $config['per_page'] : 0;

        // $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        // $this->breadcrumb->add('Member', site_url('member'));
        // $this->breadcrumb->add(lang('account_order_history'), site_url('member/history'));
        // $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['orders'] = $this->member->getOrders($id, $config['per_page'], $start);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['meta_description'] = settings('meta_description');
        $this->data['page'] = 'guestpage/guest_history';

        $this->output->set_title(lang('account_order_history') . ' - ' . settings('meta_title'));
        $this->template->_init();
        // $this->load->js('assets/frontend/js/modules/member_history.js');
        $this->load->view('member/guest_history', $this->data);
    }

    public function guest_order_detail($id) {
        $data = $this->member->get_order(decode($id));
        // if ($this->input->post('cancel') && $data->payment_method == 'kredivo') {
        //     $this->main->update('orders', array('payment_status' => 'Cancel'), array('id' => $data->id));
        //     $this->main->update('order_invoice', array('order_status' => settings('order_cancel_status')), array('order' => $data->id));
        //     $data = $this->member->get_order(decode($id));
        // }
        $this->data['page'] = 'guestpage/guest_history';
        $this->data['data'] = $data;
        $this->data['data']->payment_method_name = $this->main->get('payment_methods', ['name' => $this->data['data']->payment_method])->title;
        $this->data['payment'] = json_decode($this->data['data']->payment_to);
        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add(lang('account_order_history'), site_url('guestpage/guest_history/'.encode($this->data['data']->customer)));
        $this->breadcrumb->add($this->data['data']->code, '#');
        $this->data['breadcrumb'] = $this->breadcrumb->output();

        $this->output->set_title('Pesanan:: ' . $this->data['data']->code . ' - ' . settings('meta_title'));
        $this->template->_init();
        // $this->load->js('assets/frontend/js/minified/member_order_detail.min.js');
        $this->load->js('assets/frontend/js/modules/member_order_detail.js');
        $this->load->view('member/guest_order_detail', $this->data);
    }

    public function do_upload() {
        $this->input->is_ajax_request();
        $config['upload_path']          = './files/images/confirm_payment/';
        $config['allowed_types']        = 'jpg|png|jpeg|JPG|PNG|JPEG';
        $config['max_size']             = 10240;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;
        $data = $this->input->post(null, true);
        $path = $_FILES['image']['name'];
        $tgl = date('Ymd_his');
        $config['file_name'] = str_replace('/', '-', $data['code_order']).'_'.$tgl;
        $order = $this->main->get('orders', array('id' => decode_id($data['id']), 'code' => $data['code_order']));
        $image_name = $config['file_name'].".".pathinfo($path, PATHINFO_EXTENSION);

        if ($order) {
            unset($data['id']);
            unset($data['code_order']);
            $payment_to = json_decode($order->payment_to, true);
            $data['to'] = $payment_to[$data['to']];
            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('image'))
            {
                $error = str_replace('<p>','',$this->upload->display_errors());
                $error = str_replace('</p>','',$error);
                // $this->data['title'] = '404';
                // $this->load->view('error', $this->data);
                $message = '<div class="alert alert-danger show" role="alert">
                        <center><strong>Upload Bukti Gagal! </strong>'.$error.'
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                        </center>
                    </div>';
                 $this->session->set_flashdata('fe_guest_doupload', $message);
                 redirect('guestpage/guest_order_detail/'. encode($order->id));
            }
            else
            {   
                $image_data = $this->upload->data();
                $config_img['image_library']='gd2';
                $config_img['source_image']= $image_data['full_path'];
                $config_img['maintain_ratio']= TRUE;
                $config_img['quality']= '60%';
                $config_img['width']= 700;
                $this->load->library('image_lib', $config_img);
                
                if( !$this->image_lib->resize()){
                    $error = str_replace('<p>','',$this->image_lib->display_errors());
                    $error = str_replace('</p>','',$error);
                    
                    $message = '<div class="alert alert-danger show" role="alert">
                        <center><strong>Upload Bukti Gagal! </strong>'.$error.'
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                        </center>
                    </div>';
                     $this->session->set_flashdata('fe_guest_doupload', $message);
                     redirect('guestpage/guest_order_detail/'. encode($order->id));
                }
                $this->image_lib->clear();
                $datas = array('upload_data' => $this->upload->data());
                $this->main->update('orders', array('payment_to' => json_encode($data), 'payment_status' => 'Confirmed', 'payment_date' => date('Y-m-d'), 'confirm_image' => $image_name), array('id' => $order->id));
                $this->main->update('order_invoice', array('order_status' => settings('order_payment_confirmed_status')), array('order' => $order->id));
                
                $message = $this->load->view('email/confirm_order', $order, TRUE);
                $cronjob = array(
                    'from' => settings('send_email_from'),
                    'from_name' => settings('store_name'),
                    'to' => settings('admin_email'),
                    'subject' => 'Konfirmasi Pesanan ' . $order->code,
                    'message' => $message
                );
                $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
    
                // $return = array('status' => 'success', 'message' => '<div class="alert alert-success">Konfirmasi pembayaran telah kami terima. Pembayaran akan segera kami verifikasi.</div>', 'id' => encode($order->id));
                $message = '<div class="alert alert-success show" role="alert">
                        <center><strong>Upload Bukti Berhasil! </strong>Konfirmasi pembayaran telah kami terima. Pembayaran akan segera kami verifikasi.<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                        </center>
                    </div>';
                $this->session->set_flashdata('fe_guest_doupload', $message);
                redirect('guestpage/guest_order_detail/'. encode($order->id));
                
            }
        } else {
            $message = '<div class="alert alert-danger show" role="alert">
                        <center><strong>Gagal! </strong>Tidak dapat menemukan pesanan. Konfirmasi pemesanan gagal<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                        </center>
                    </div>';
            $this->session->set_flashdata('fe_guest_doupload', $message);
            redirect('guestpage/guest_order_detail/'. encode($order->id));
        }
    }
}

   