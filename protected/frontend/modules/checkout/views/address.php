<link rel="stylesheet" type="text/css" href="../assets/frontend/css/address.css"/>
<div id="tags-load" style="display:none;"><i class="fa fa-spinner fa-pulse fa-2x"></i></div>
<section class="collection-heading heading-content ">
  <div class="container">
    <div class="cart-step">
      <ul style="font-size: 20px">
        <li style="width:80%" class="active"><i class="fa fa-shopping-cart"></i> Cart</li>
        <li style="width:5%"> <i style="color:#a7c22a" class="fa fa-arrow-right"></i> </li>
        <li style="width:80%" class="active"><i class="fa fa-credit-card"></i> Checkout</li>
        <li style="width:5%"> <i style="color:#a7c22a" class="fa fa-arrow-right"></i> </li>
        <li style="width:80%" class=""><i class="fa fa-money"></i> Payment</li>
        <li style="width:5%"> <i class="fa fa-arrow-right"></i> </li>
        <li style="width:80%"><i class="fa fa-check"></i> Finish</li>
      </ul>
    </div>
    <!-- <div class="cart-title"><i class="fa fa-credit-card"></i> Checkout</div> -->
  </div>
</section>
<section class="cart-content checkout">
    <div class="container">
        <?php if (isset($message)) { ?>
            <div class="alert alert-danger fade in alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                <?php echo $message; ?>
            </div>
        <?php } ?>
        <form class="form" action="" method="post">
            <input type="hidden" id="total" value="<?php echo $grand_total; ?>">
            <div class="address">
                <div class="left">
                    <div class="header">
                        <div class="title">Alamat Pengiriman</div>
                    </div>
                    <div class="body">
                        <div id="address-list">
                            <?php
                                if ($list_address->result()){
                                    $primary_address_ = $primary_address->row();
                                    if($primary_address_){ 
                                        $primary_address_ = $primary_address->row();
                                        $utama = '<span class="primary-address">Utama</span>';
                                    } else {
                                        $primary_address_ = $none_address->row();
                                        $utama = '';
                                    }
                            ?>
                                <div class="primary-div" id="primary_address" <?php echo ($this->data['type'] == 'g') ? 'style="border-bottom: none;"' : '' ?>>
                                    <?php if(isset($_SESSION['address_session'])) : ?>
                                        <?php $address_session_ = $this->checkout->get_address($_SESSION['address_session'])->row() ?>
                                        <?php if($address_session_) : ?>
                                            <div class="payment-method-detail">
                                                <label for="address-<?php echo $address_session_->id; ?>">
                                                    <div class="payment-method-title"><strong><?php echo $address_session_->name; ?></strong> (<?php echo $address_session_->title; ?>) <?php echo $utama; ?></div>
                                                    <div class="payment-method-subtitle"><?php echo $address_session_->phone .'<br>'. $address_session_->address .' '. $address_session_->district_name . ', ' . $address_session_->city_name . ', ' . $address_session_->postcode . ' ' . $address_session_->province_name; ?></div>
                                                </label>
                                            </div>
                                        <?php else : ?>
                                            <?php $_SESSION['address_session'] = $primary_address_->id; ?>
                                            <div class="payment-method-detail">
                                                <label for="address-<?php echo $primary_address_->id; ?>">
                                                    <div class="payment-method-title"><strong><?php echo $primary_address_->name; ?></strong> (<?php echo $primary_address_->title; ?>) <?php echo $utama; ?></div>
                                                    <div class="payment-method-subtitle"><?php echo $primary_address_->phone .'<br>'. $primary_address_->address .' '. $primary_address_->district_name . ', ' . $primary_address_->city_name . ', ' . $primary_address_->postcode . ' ' . $primary_address_->province_name; ?></div>
                                                </label>
                                            </div>
                                        <?php endif; ?>
                                    <?php else : ?>
                                        <div class="payment-method-detail">
                                            <label for="address-<?php echo $primary_address_->id; ?>">
                                                <div class="payment-method-title"><strong><?php echo $primary_address_->name; ?></strong> (<?php echo $primary_address_->title; ?>) <?php echo $utama; ?></div>
                                                <div class="payment-method-subtitle"><?php echo $primary_address_->phone .'<br>'. $primary_address_->address .' '. $primary_address_->district_name . ', ' . $primary_address_->city_name . ', ' . $primary_address_->postcode . ' ' . $primary_address_->province_name; ?></div>
                                            </label>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <input type="hidden" id="address-district" value="<?= $primary_address_->id; ?>">
                                <?php if ($this->data['user']->type != 'g') : ?>
                                    <br>
                                    <button class="btn btn-primary ganti-alamat" type="button" data-toggle="collapse" data-target="#otherAddress" aria-expanded="false" aria-controls="otherAddress">Ganti Alamat</button>
                                <?php endif; ?>
                            <?php
                                }
                            ?>

                            <?php if ($this->data['user']->type != 'g') : ?>
                                <button class="btn btn-default" type="button" id="add-address" onclick="add_address()">Tambah Alamat</button>
                            <?php endif; ?>
                            <div class="collapse" id="otherAddress" <?= ($this->data['user']->type == 'g') ? 'style="display:none;"' : ''  ?>>
                                <div class="card card-body">
                                    <?php
                                        if ($list_address):
                                            foreach ($list_address->result() as $address):
                                                $primary_address__ = $primary_address->row();
                                                $none_a = $none_address->row();
                                                foreach ($none_a as $non) {
                                                    $id_a = $none_a->id;
                                                }
                                                if($primary_address__){
                                                    if($address->primary == 1){
                                                    $primary = 'payment-method-block-active';
                                                    $check = 'checked';
                                                    } else {
                                                    $primary = '';
                                                    $check = '';
                                                    }
                                                }  else if($address->id == $id_a) {
                                                    $primary = 'payment-method-block-active';
                                                    $check = 'checked';
                                                } else {
                                                    $primary = '';
                                                    $check = '';
                                                }
                                    ?>
                                                <br>

                                                <?php if(isset($_SESSION['address_session'])) : ?>
                                                    <?php if($address->id == $_SESSION['address_session']) : ?>
                                                        <div class="payment-method-block payment-method-block-active" data-dismiss="collapse">
                                                            <div class="payment-method-detail">
                                                                <input type="radio" value="<?php echo $address->id; ?>" onclick="selectAddress(this, <?php echo $address->id; ?>, <?= $grand_total ?>);" name="address" required id="address-<?php echo $address->id; ?>" checked>
                                                                <label for="address-<?php echo $address->id; ?>">
                                                                    <div class="payment-method-title"><?php echo $address->name; ?> - <?php echo $address->title; ?></div>
                                                                    <div class="payment-method-subtitle"><?php echo $address->address . '<br>' . $address->district_name . ', ' . $address->city_name . ', ' . $address->postcode . '<br>' . $address->province_name; ?></div>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    <?php else : ?>
                                                        <div class="payment-method-block" data-dismiss="collapse">
                                                            <div class="payment-method-detail">
                                                            <input type="radio" value="<?php echo $address->id; ?>" onclick="selectAddress(this, <?php echo $address->id; ?>, <?= $grand_total ?>);" name="address" required id="address-<?php echo $address->id; ?>">
                                                            <label for="address-<?php echo $address->id; ?>">
                                                                <div class="payment-method-title"><?php echo $address->name; ?> - <?php echo $address->title; ?></div>
                                                                <div class="payment-method-subtitle"><?php echo $address->address . '<br>' . $address->district_name . ', ' . $address->city_name . ', ' . $address->postcode . '<br>' . $address->province_name; ?></div>
                                                            </label>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php else : ?>
                                                    <div class="payment-method-block <?php echo $primary; ?>" data-dismiss="collapse">
                                                        <div class="payment-method-detail">
                                                            <input type="radio" value="<?php echo $address->id; ?>" onclick="selectAddress(this, <?php echo $address->id; ?>, <?= $grand_total ?>);" name="address" required id="address-<?php echo $address->id; ?>" <?php echo $check; ?>>
                                                            <label for="address-<?php echo $address->id; ?>">
                                                                <div class="payment-method-title"><?php echo $address->name; ?> - <?php echo $address->title; ?></div>
                                                                <div class="payment-method-subtitle"><?php echo $address->address . '<br>' . $address->district_name . ', ' . $address->city_name . ', ' . $address->postcode . '<br>' . $address->province_name; ?></div>
                                                            </label>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                    <?php
                                            endforeach;
                                        endif;
                                    ?>
                                </div>
                            </div>
                            <?php if ($this->data['user']->type != 'g') : ?>
                            <br>
                            <br>
                            <?php endif; ?>

                            
                            <div class="info-shop">
                                <div id="info_product">
                                <br>
                                <div class="header">
                                    <div class="title">Ringkasan Belanja</div>
                                </div>

                                <!-- <?php //foreach ($this->cart->contents() as $item) { ?> -->
                                <?php foreach($cart_merchant as $cart_m) : ?>
                                    <?php $cart = $this->cart_model->get_all_cart_by_merchant($this->data['user']->id, $cart_m->merchant)->result(); ?>
                                    <input type="hidden" id="shipping-cost-<?php echo encode($cart_m->id); ?>" name="shipping_cost[<?php echo encode($cart_m->id); ?>]" value="0" class="shipping-cost-class">
                                    <div class="cart-item">
                                        <div class="cart-item-left" style="display: block !important;">
                                        <?php foreach($cart as $item) : ?>
                                            <?php if($item->option) : ?>
                                                <?php $options = json_decode($item->option); ?>
                                                <?php foreach($options as $option) : ?>
                                                    <div class="cart-item-detail" style="margin-bottom: 10px;">
                                                        <div class="item-image-">
                                                            <a href="<?php echo seo_url('catalog/products/view/' . $item->product_id . '/' . $item->merchant_id); ?>">
                                                                <img style="width:100px;" src="<?php echo get_image($option->image); ?>">
                                                            </a>
                                                        </div>

                                                        <div class="item-desc">
                                                            <div class="item-name">
                                                                <a href="<?php echo seo_url('catalog/products/view/' . $item->product_id . '/' . $item->merchant_id); ?>"><strong><?php echo $item->name; ?></strong></a>
                                                            </div>
                                                            <div class="cart-line">
                                                                <?php if ($option->data_option) { ?>
                                                                    <?php //foreach ($item-> as $option) { ?>
                                                                        <!-- <div class="option"><?php //echo $option['option_name']; ?></div> -->
                                                                    <?php //} ?>

                                                                    <?php $data_options = json_decode($option->data_option) ?>
                                                                    <?php foreach($data_options as $data_option) : ?>
                                                                        <div class="option" style="height: 20px;"><?php echo $data_option->value; ?></div>
                                                                        <div class="divider" style="margin-bottom: 0px;"></div>
                                                                    <?php endforeach; ?>
                                                                <?php } ?>
                                                                <div class="info"><?php echo $item->code; ?></div>
                                                                <div class="info"><?php echo $option->total_weight; ?> gram</div>
                                                            </div>
                                                            <div class="cart-line">
                                                                <span class="price" style="font-weight:bold; font-size: 16px; color: #a7c22a;"><?php echo rupiah($option->quantity * $option->price); ?> <div class="info">(<?php echo $option->quantity . ' x ' . rupiah($option->price); ?>)</div></span>
                                                            </div>
                                                            <?php //if ($item->merchant_id) { ?>
                                                                <!-- <div class="cart-line">
                                                                    <span class="merchant">Sold by: <?php echo $item->merchant_name; ?></span>
                                                                </div> -->
                                                            <?php //} ?>
                                                        </div>
                                                    </div>
                                                <?php endforeach; ?>
                                            <?php else : ?>
                                                <div class="cart-item-detail" style="margin-bottom: 10px;">
                                                    <div class="item-image-">
                                                        <a href="<?php echo seo_url('catalog/products/view/' . $item->product_id . '/' . $item->merchant_id); ?>">
                                                            <img style="width:100px;" src="<?php echo get_image($item->image); ?>">
                                                        </a>
                                                    </div>

                                                    <div class="item-desc">
                                                        <div class="item-name">
                                                            <a href="<?php echo seo_url('catalog/products/view/' . $item->product_id . '/' . $item->merchant_id); ?>"><strong><?php echo $item->name; ?></strong></a>
                                                        </div>
                                                        <div class="cart-line">
                                                            <?php if ($item->options) { ?>
                                                                <?php //foreach ($item-> as $option) { ?>
                                                                    <!-- <div class="option"><?php //echo $option['option_name']; ?></div> -->
                                                                <?php //} ?>

                                                                <?php $options = json_decode($item->options) ?>
                                                                <?php foreach($options as $option) : ?>
                                                                    <div class="option" style="height: 20px;"><?php echo $option->option_name; ?></div>
                                                                    <div class="divider"></div>
                                                                <?php endforeach; ?>
                                                            <?php } ?>
                                                            <div class="info"><?php echo $item->code; ?></div>
                                                            <div class="info"><?php echo $item->weight; ?> gram</div>
                                                        </div>
                                                        <div class="cart-line">
                                                            <span class="price" style="font-weight:bold; font-size: 16px; color: #a7c22a;"><?php echo rupiah($item->grand_total); ?> <div class="info">(<?php echo $item->quantity . ' x ' . rupiah($item->price); ?>)</div></span>
                                                        </div>
                                                        <?php //if ($item->merchant_id) { ?>
                                                            <!-- <div class="cart-line">
                                                                <span class="merchant">Sold by: <?php echo $item->merchant_name; ?></span>
                                                            </div> -->
                                                        <?php //} ?>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                        </div>
                                        <div class="cart-item-right-">
                                            <div class="shipment">
                                                <?php 
                                                $pickup = true;
                                                $merchant = $this->main->get('merchants', array('id' => $cart_m->merchant));
                                                ?>
                                                <span class="product-merchant">Dikirim oleh: <a href="<?php echo seo_url('merchant_home/view/' . $merchant->id); ?>" class="url-merchant"><b><?php echo $merchant->name; ?></b></a></span>
                                                <!-- <a href="javascript:void(0);" class="info-store" data-id="<?php echo encode($cart_m->id); ?>" data-toggle="popover" data-trigger="focus" data-html="true">Info Toko</a> -->

                                                <input type="hidden" id="shipping-<?php echo encode($cart_m->id); ?>" name="shipping[<?php echo encode($cart_m->id); ?>][merchant]" value="<?php echo encode($merchant->id); ?>">
                                                <?php if ($this->agent->is_mobile()) : ?>
                                                    <span class="product-merchant" style="display: inline-block; margin-top: 10px !important; ">Pilih Pengiriman : </span>
                                                    <div id="full-ekspedisi-mobile-<?= encode($cart_m->id) ?>" class="full-ekspedisi-mobile" style="margin-top: -10px;">
                                                        <select id="shipping-courier-mobile-<?= encode($cart_m->id) ?>" class="form-control choose-shipping-mobile" name="shipping[<?php echo encode($cart_m->id); ?>][service]" required onchange="ganti_pengiriman($(this).find(':selected').data('value'), $(this).find(':selected').data('id'));">
                                                            <?php if($pickup) : ?>
                                                            <option value="pickup" data-id="<?php echo encode($cart_m->id); ?>" data-value="0">Ambil Sendiri <span style="color:#d9534f;font-size:10px">* Tidak support kredivo</span></option>
                                                            <?php endif; ?>
                                                            <?php
                                                            $couriers = json_decode($merchant->shipping);
                                                            if($couriers) {
                                                                foreach($couriers as $courier) {
                                                                    if(isset($_COOKIE['district_add'])){
                                                                        $district_address = $_COOKIE['district_add'];
                                                                    // } elseif(isset($_SESSION['address_session'])) {
                                                                    //     $address_session_ = $this->checkout->get_address($_SESSION['address_session'])->row();
                                                                    //     $district_address = $address_session_->district;
                                                                    } else {
                                                                        $district_address = $primary_address_->district;
                                                                    }
                                                                    if(isset($_SESSION['session_merchant']) && $_SESSION != 0 && $_SESSION != '') {
                                                                        $services = $this->rajaongkir->cost($_SESSION['session_merchant'], 'subdistrict', $district_address, 'subdistrict', ($cart_m->total_weight), $courier);
                                                                    } else {
                                                                        $services = $this->rajaongkir->cost($merchant->district, 'subdistrict', $district_address, 'subdistrict', ($cart_m->total_weight), $courier);
                                                                    }
                                                                    $num_courier_mobile = 0;
                                                                    if ($services) {
                                                                        $services = json_decode($services);
                                                                        if ($services->rajaongkir->status->code == 200) {
                                                                            foreach ($services->rajaongkir->results as $service) {
                                                                                foreach ($service->costs as $cost) {
                                                                                    if (strpos($service->code, 'pos') !== false) {
                                                                                        $cost->cost[0]->value = $cost->cost[0]->value/100 * 10 + $cost->cost[0]->value;
                                                                                    }
                                                                                    if($cost->service == "CTC"){
                                                                                        $label_service = "REG";
                                                                                    } else if ($cost->service == "CTCYES"){
                                                                                        $label_service = "YES";
                                                                                    } else if ($cost->service == "POS"){
                                                                                        $label_service = str_replace('Barang', ' ', $cost->service);
                                                                                    } else {
                                                                                        $label_service = $cost->service;
                                                                                    }
                                                                                    if($service->code == "J&T"){
                                                                                        $service->code = "jnt";
                                                                                    }

                                                                                    $restricted_courier = array("jne", "tiki", "pos");
                                                                                    $restricted_service = array("OKE", "ECO", "Express Next Day Barang");

                                                                                    if (!in_array($cost->service, $restricted_service)) { 
                                                                                        $num_courier_mobile++;?>
                                                                                        <option value="<?php echo $service->code . '-' . $cost->service; ?>" data-id="<?php echo encode($cart_m->id); ?>" data-value="<?php echo $cost->cost[0]->value; ?>"><?php echo strtoupper($service->code) . ' ' . $label_service; ?> <span>(<?php echo rupiah($cost->cost[0]->value); ?>)</span></option>
                                                                                    <?php 
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                        <?php if(isset($num_courier_mobile) && $num_courier_mobile == 0) : ?>
                                                            <span style="display: block;font-size: 15px;text-align: center;color: white;background: #d9534f;margin-top: 5px;padding: 5px 10px;border-radius: 3px;font-weight:bold;">Jasa pengiriman tidak tersedia!</span>
                                                        <?php endif; ?>
                                                    </div>
                                                <?php else : ?>
                                                    <div class="collapse in" id="ekspedisi<?php echo encode($cart_m->id); ?>">
                                                        <span><b>Pilih Pengiriman: </b></span>
                                                        <?php 
                                                            if(isset($_SESSION['session_merchant'])) {
                                                                unset($_SESSION['session_merchant']);
                                                            }
                                                        ?>
                                                        <div id="full-ekspedisi-<?= encode($cart_m->id) ?>" class="full-ekspedisi">
                                                            <ul id="shipping-courier-<?php echo encode($cart_m->id); ?>">
                                                                <?php if($pickup) : ?>
                                                                    <li>
                                                                        <input id="<?php echo encode($cart_m->id); ?>-pickup" name="shipping[<?php echo encode($cart_m->id); ?>][service]" type="radio" value="pickup" class="choose-shipping" required data-id="<?php echo encode($cart_m->id); ?>" data-value="0" onclick="ganti_pengiriman($(this).data('value'), $(this).data('id'));">
                                                                        <label style="font-size: 11px;" for="<?php echo encode($cart_m->id); ?>-pickup">Ambil Sendiri <span style="color:#d9534f;font-size:10px">* Tidak support kredivo</span></label>
                                                                    </li>
                                                                    <!-- <li>
                                                                        <input type="radio" id="<?//= encode($cart_m->id) . 'pos-Paket Kilat Khusus' ?>" name="shipping[<?//= encode($cart_m->id) ?>][service]" value="pos-Paket Kilat Khusus" class="choose-shipping" required data-id="<?//= encode($cart_m->id) ?>" data-value="7700" onclick="ganti_pengiriman($(this).data('value'), $(this).data('id'))">
                                                                        <label for="<?//= encode($cart_m->id) . 'pos-Paket Kilat Khusus' ?>">POS Paket Kilat Khusus (Rp 7.700)</label>
                                                                    </li>
                                                                    <li>
                                                                        <input type="radio" id="<?//= encode($cart_m->id) . 'tiki-ONS' ?>" name="shipping[<?//= encode($cart_m->id) ?>][service]" value="tiki-ONS" class="choose-shipping" required data-id="<?//= encode($cart_m->id) ?>" data-value="18000" onclick="ganti_pengiriman($(this).data('value'), $(this).data('id'))">
                                                                        <label for="<?//= encode($cart_m->id) . 'tiki-ONS' ?>">Tiki ONS (Rp 18.000)</label>
                                                                    </li>
                                                                    <li>
                                                                        <input type="radio" id="<?//= encode($cart_m->id) . 'tiki-REG' ?>" name="shipping[<?//= encode($cart_m->id) ?>][service]" value="tiki-REG" class="choose-shipping" required data-id="<?//= encode($cart_m->id) ?>" data-value="9000" onclick="ganti_pengiriman($(this).data('value'), $(this).data('id'))">
                                                                        <label for="<?//= encode($cart_m->id) . 'tiki-REG' ?>">Tiki REG (Rp 9.000)</label>
                                                                    </li> -->
                                                                <?php endif; ?>
                                                                <div id="ekspedisi-">
                                                                    <?php 
                                                                    $couriers = json_decode($merchant->shipping);
                                                                    setcookie("district_add", "", time()-3600);
                                                                    if ($couriers) {
                                                                        foreach ($couriers as $courier) {
                                                                            if(isset($_COOKIE['district_add'])) {
                                                                                $district_address = $_COOKIE['district_add'];
                                                                            } elseif(isset($_SESSION['address_session'])) {
                                                                                $address_session_ = $this->checkout->get_address($_SESSION['address_session'])->row();
                                                                                $district_address = $address_session_->district;
                                                                            } else {
                                                                                $district_address = $primary_address_->district;
                                                                            }
                                                                            if(isset($_SESSION['session_merchant']) && $_SESSION != 0 && $_SESSION != '') {
                                                                                $services = $this->rajaongkir->cost($_SESSION['session_merchant'], 'subdistrict', $district_address, 'subdistrict', ($cart_m->total_weight), $courier);
                                                                            } else {
                                                                                $services = $this->rajaongkir->cost($merchant->district, 'subdistrict', $district_address, 'subdistrict', ($cart_m->total_weight), $courier);
                                                                            }
                                                                            if ($services) {
                                                                                // $num_courier = 0;
                                                                                $services = json_decode($services);
                                                                                if ($services->rajaongkir->status->code == 200) {
                                                                                    foreach ($services->rajaongkir->results as $service) {
                                                                                        foreach ($service->costs as $cost) {
                                                                                            if (strpos($service->code, 'pos') !== false) {
                                                                                                $cost->cost[0]->value = $cost->cost[0]->value/100 * 10 + $cost->cost[0]->value;
                                                                                            }
                                                                                            if($cost->service == "CTC"){
                                                                                                $label_service = "REG";
                                                                                            } else if ($cost->service == "CTCYES"){
                                                                                                $label_service = "YES";
                                                                                            } else {
                                                                                                $label_service = $cost->service;
                                                                                            }
                                                                                            if($service->code == "J&T"){
                                                                                                $service->code = "jnt";
                                                                                            }
                                                                                            $restricted_courier = array("jne", "tiki", "pos");
                                                                                            $restricted_service = array("OKE", "ECO", "Express Next Day Barang");
                                                                                            if (!in_array($cost->service, $restricted_service)) { 
                                                                                                //$num_courier++;?>
                                                                                                <li>
                                                                                                    <input name="shipping[<?php echo encode($cart_m->id); ?>][service]" type="radio" value="<?php echo $service->code . '-' . $cost->service; ?>" class="choose-shipping" id="<?php echo encode($cart_m->id) . $service->code . '-' . $cost->service; ?>" required data-id="<?php echo encode($cart_m->id); ?>" data-value="<?php echo $cost->cost[0]->value; ?>" onclick="ganti_pengiriman($(this).data('value'), $(this).data('id'))">
                                                                                                    <label style="font-size: 11px;" for="<?php echo encode($cart_m->id) . $service->code . '-' . $cost->service; ?>">
                                                                                                        <?php echo strtoupper($service->code) . ' ' . $label_service; ?> <span>(<?php echo rupiah($cost->cost[0]->value); ?>)</span>
                                                                                                    </label>
                                                                                                </li>
                                                                                            <?php 
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        //if(isset($num_courier) && $num_courier == 0) {
                                                                            ?>
                                                                            <!-- <span style="color: white;font-weight: bold;display: block;background: #d9534f;text-align: center;padding: 2px;border-radius: 2px;">Jasa pengiriman tidak tersedia!</span> -->
                                                                            <?php
                                                                        //}
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                                </div>    
                            </div>
                        </div>
                    </div>
                    <div style="border-bottom:1px solid #ddd"></div>
                    <br>
                    <div class="subtotal">
                        <span style="font-size: 14px; ">Estimasi Biaya Kirim</span>
                        <span id="shipping-cost-text" style="font-size: 14px; " class="pull-right"><?php echo rupiah(0); ?></span>
                    </div>
                    <br>
                    <div class="total">
                        <span style="font-size: 18px; color:#555;"><strong>Subtotal</strong></span>
                        <span style="font-size: 18px; color:#d9534f; font-weight: bold" class="pull-right" id="total-text"><?php echo rupiah($grand_total); ?></span>
                    </div>
                </div>
            </div>

            <div class="cart-footer">
                <div class="action">
                    <a href="<?php echo site_url('cart'); ?>"><!-- <i class="fa fa-arrow-left"></i> Kembali ke Cart --></a>
                    <button type="button" class="btn" id="button-lanjutkan" onclick="check_ongkir($('.shipping-cost-class').each(function() {}))">LANJUTKAN <i class="fa fa-arrow-right"></i></button>
                </div>
            </div>
        </form>
    </div>
</section>

<div id="modal-address" class="modal" role="dialog" aria-hidden="true" tabindex="-1" data-width="800">
    <div class="modal-dialog fadeIn animated">
        <div class="modal-content" style="border-radius: 5px;">
            <form action="#">
                <div class="modal-header">
                    <h1>Tambah Alamat</h1>
                </div>
                <div class="modal-body">
                    <div id="message"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Simpan Alamat sebagai</label>
                                <input name="title" type="text" class="form-control" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('checkout_shipping_field_name'); ?></label>
                                <input name="name" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('checkout_shipping_field_phone'); ?></label>
                                <input name="phone" type="text" class="form-control" onkeypress="return isNumberKey(event)">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('checkout_shipping_field_postcode'); ?></label>
                                <input name="postcode" type="text" class="form-control" onkeypress="return isNumberKey(event)">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('checkout_shipping_field_province'); ?></label>
                                <select name="province" id="province" class="form-control">
                                    <option value="">Pilih provinsi</option>
                                    <?php if ($provincies) { ?>
                                        <?php foreach ($provincies->result() as $province) { ?>
                                            <option value="<?php echo $province->id; ?>"><?php echo $province->name; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('checkout_shipping_field_city'); ?></label>
                                <select name="city" id="city" class="form-control">
                                    <option value="">Pilih kota</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('checkout_shipping_field_district'); ?></label>
                                <select name="district" id="district" class="form-control">
                                    <option value="">Pilih kecamatan</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('checkout_shipping_field_address'); ?></label>
                                <input name="address" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" style="background-color: #FFF; color: #000;">Batal</button>
                    <button type="button" onclick="submit_address();" class="btn">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>