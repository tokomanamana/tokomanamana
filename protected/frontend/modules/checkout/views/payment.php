<link rel="stylesheet" href="<?php echo site_url('assets/frontend/css/minified/payment_min.css') ?>">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" integrity="sha384-MUdXdzn1OB/0zkr4yGLnCqZ/n9ut5N7Ifes9RP2d5xKsTtcPiuiwthWczWuiqFOn" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css" integrity="sha384-3wRRg17hVCopINZVYCqnfbgXE7aFPSvawmLWNPSiUPVx+HxY+yxb5Cwp5mT7RXPD" crossorigin="anonymous">
<?php $total_price_order = $grand_total + $count_total_shipping - ($couponn?$total_disc:0) ?>
<section class="collection-heading heading-content " style="<?php echo ($this->agent->is_mobile()) ? 'padding: 0px 10px;' : '' ; ?>">
    <div class="container">
        <div class="cart-step">
            <ul style="font-size: 20px">
                <li style="width:80%" class="active"><i class="fa fa-shopping-cart"></i> Cart</li>
                <li style="width:5%"> <i style="color:#a7c22a" class="fa fa-arrow-right"></i> </li>
                <li style="width:80%" class="active"><i class="fa fa-credit-card"></i> Checkout</li>
                <li style="width:5%"> <i style="color:#a7c22a" class="fa fa-arrow-right"></i> </li>
                <li style="width:80%" class="active"><i class="fa fa-money"></i> Payment</li>
                <li style="width:5%"> <i style="color:#a7c22a" class="fa fa-arrow-right"></i> </li>
                <li style="width:80%"><i class="fa fa-check"></i> Finish</li>
            </ul>
        </div>
        <!-- <div class="cart-title"><i class="fa fa-money"></i> Payment</div> -->
    </div>
</section>
<section class="cart-content checkout" style="<?php echo ($this->agent->is_mobile()) ? 'padding: 0px 5px;' : '' ; ?>">
    <div class="container">
        <?php if (isset($message)) { ?>
            <div class="alert alert-danger fade in alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                <?php echo $message; ?>
            </div>
        <?php } ?>
        <form class="form" action="" method="post">
            <div class="payment">
                <div class="left">
                    <div class="header">
                        <div class="title" style="font-weight: bold;">Pilih Metode Pembayaran</div>
                    </div>
                    <div class="body">
                        <div id="payment-list">
                            <!-- <?php
                            if ($payment_methods) {
                                foreach ($payment_methods->result() as $payment_method) {
                                    if ($payment_method->name == 'alfamart' && ($this->cart->total() + $shipping_cost) > 5000000) {
                                        
                                    } else if($payment_method->name == 'kredivo' && $pickup_count > 0){

                                    } else {
                                        ?>
                                        <?php if($payment_method->vendor != 'parent_transfer') : ?>
                                        <div class="payment_method_block">
                                            <div class="payment_method_detail" <?php echo ($payment_method->name == 'transfer') ? 'onclick="selectTransfer()"' : '' ?>>
                                                <input type="radio" value="<?php echo $payment_method->name; ?>" onclick="selectPayment(this);" name="payment" required id="payment-<?php echo $payment_method->name; ?>">
                                                <label for="payment-<?php echo $payment_method->name; ?>" style="display: inline-grid;width: 90%;margin-left: 8px;">
                                                    <div class="payment-method-title" style="font-size: 15px;margin-bottom: 2px;font-weight: 600;"><?php echo $payment_method->title; ?> 
                                                        <?php if($payment_method->name != 'transfer') : ?>
                                                            <span class="recommended-payment">Rekomendasi</span>
                                                        <?php endif; ?>
                                                    </div>
                                                    <?php if($payment_method->name != 'transfer') : ?>
                                                        <div class="payment-method-subtitle"><?php echo $payment_method->description; ?></div>
                                                    <?php endif; ?>
                                                    <div class="payment-method-icon">
                                                        <?php if($payment_method->name != 'transfer') : ?>
                                                            <img src="<?php echo get_image($payment_method->image) ?>" alt="" style="width: 65px;">
                                                        <?php else : ?>
                                                            <?php $images = explode(' ', $payment_method->image); ?>
                                                            <?php foreach($images as $image) : ?>
                                                                <img src="<?php echo get_image($image) ?>" alt="" style="width: 65px;">
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </label>
                                                <div class="payment-method-subtitle" style="margin-left: 15px;"><?php //echo $payment_method->description; ?></div>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                    <?php } ?>
                                    <?php
                                }
                            }
                            ?> -->
                            <?php if($payment_methods_type) : ?>
                                <?php foreach($payment_methods_type->result() as $payment_method_type) : ?>
                                    <?php if($payment_method_type->name == 'Retail Outlet' && ($grand_total + $count_total_shipping) > 5000000) : ?>
                                    <?php elseif($payment_method_type->name == 'Cicilan Tanpa Kartu Kredit' && $pickup_count > 0) : ?>
                                    <?php elseif($total_price_order < 10000 && $payment_method_type->name != 'E-Wallet') : ?>
                                    <?php else : ?>
                                        <?php if($payment_method_type->name != 'Instant Payment') : ?>
                                        <div class="payment_method_block">
                                            <div class="payment_method_detail" data-toggle="collapse" href="#payment-<?= $payment_method_type->id ?>" role="button" aria-expanded="false" aria-controls="payment-<?= $payment_method_type->id ?>" data-id="<?= $payment_method_type->id ?>" data-icon="99" id="div-<?= $payment_method_type->id ?>">
                                                <input type="hidden" class="test" value="99" id="tes-<?= $payment_method_type->id ?>">
                                                <!-- <input type="radio" value="<?php //echo $payment_method_type->name; ?>" onclick="selectPayment(this);" name="payment" required id="payment-<?php //echo $payment_method_type->name; ?>"> -->
                                                <i class="fa fa-chevron-down" id="icon-<?= $payment_method_type->id ?>"></i>
                                                <label for="payment-<?php echo $payment_method_type->name; ?>" style="display: inline-grid;width: 90%;margin-left: 8px;">
                                                    <div class="payment-method-title" style="font-size: 15px;margin-bottom: 2px;font-weight: 600;"><?php echo $payment_method_type->name; ?> 
                                                        <?php if($payment_method_type->name != 'Transfer') : ?>
                                                            <span class="recommended-payment">Rekomendasi</span>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="payment-method-subtitle"><?php echo $payment_method_type->description; ?></div>
                                                    <div class="payment-method-icon">
                                                        <?php $payment_method = $this->main->gets('payment_methods', ['type' => $payment_method_type->id]) ?>
                                                        <?php foreach($payment_method->result() as $pym_m) : ?>
                                                            <?php if($pym_m->title == 'Credit Card') : ?>
                                                                <?php $images_credit_card = explode(' ', $pym_m->image) ?>
                                                                <?php foreach($images_credit_card as $image) : ?>
                                                                    <!-- <img src="<?php echo get_image($image) ?>" alt="" style="width: 50px;margin-right: 3px;"> -->
                                                                <?php endforeach; ?>
                                                            <?php else : ?>
                                                                <!-- <img src="<?php echo get_image($pym_m->image) ?>" alt="" style="width: 50px;margin-right: 3px;"> -->
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </div>
                                                </label>
                                                <!-- <div class="payment-method-subtitle" style="margin-left: 15px;"><?php //echo $payment_method->description; ?></div> -->
                                            </div>
                                            <div class="payment_methods_block collapse" id="payment-<?= $payment_method_type->id ?>">
                                                <?php $payment_methods_detail = $this->main->gets('payment_methods', ['type' => $payment_method_type->id, 'status' => 1]); ?>
                                                <?php if($payment_methods_detail) : ?>
                                                    <?php foreach($payment_methods_detail->result() as $payment_method) : ?>
                                                        <?php if($payment_method->name != 'transfer') : ?>
                                                            <?php 
                                                                if($payment_method->name == 'bca_sakuku') {
                                                                    $style_image = 'width: 20px;';
                                                                } elseif($payment_method->name == 'ovo') {
                                                                    $style_image = 'width: 35px;';
                                                                } elseif($payment_method->name == 'creditcard' || $payment_method->name == 'credit_card' || $payment_method->type == 5 || $payment_method->type == 6) {
                                                                    $style_image = 'width: 45px;';
                                                                } elseif($payment_method->name == 'bca_klikpay' || $payment_method->name == 'kredivo') {
                                                                    $style_image = 'width: 30px;';
                                                                } else {
                                                                    $style_image = 'width: 60px;';
                                                                }
                                                            ?>
                                                            <div class="payment_method_block_detail">
                                                                <input type="radio" name="payment" value="<?php echo $payment_method->name ?>" required id="payment-<?php echo $payment_method->name ?>" onclick="selectPayment(this);">
                                                                <label for="payment-<?= $payment_method->name ?>">
                                                                    <?php if($payment_method->type == 5 || $payment_method->type == 6) : ?>
                                                                        <?php $images = explode(' ', $payment_method->image); ?>
                                                                        <!-- <img src="<?php echo get_image($images[0]) ?>" alt="" style="<?php echo $style_image ?>"> -->
                                                                        <?php foreach($images as $image) : ?>
                                                                            <img src="<?php echo get_image($image) ?>" alt="" style="<?php echo $style_image ?>">
                                                                        <?php endforeach; ?>
                                                                    <?php else : ?>
                                                                        <img src="<?php echo get_image($payment_method->image) ?>" alt="" style="<?php echo $style_image ?>">
                                                                    <?php endif; ?>
                                                                    <span><?php echo $payment_method->title; ?></span>
                                                                </label>
                                                            </div>
                                                        <?php else : ?>
                                                            <div class="payment_method_block_detail">
                                                                <input type="radio" name="payment" value="<?php echo $payment_method->name ?>" id="payment-<?php echo $payment_method->title ?>" onclick="selectPayment(this);" data-nametransfer="<?php echo $payment_method->title ?>">
                                                                <label for="payment-<?= $payment_method->title ?>">
                                                                    <img src="<?php echo get_image($payment_method->image) ?>" alt="" style="width: 60px;">
                                                                    <span><?php echo $payment_method->title; ?></span>
                                                                </label>
                                                            </div>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                                <?php if($payment_method_type->name == 'Transfer') : ?>
                                                    <input type="hidden" name="parent_transfer" value="" id="parent_transfer">
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>

                        <!-- <div class="payment_transfer" style="height: 0px;opacity: 0;">
                            <?php foreach($payment_methods->result() as $payment_method) : ?>
                                <?php if($payment_method->vendor == 'parent_transfer') : ?>
                                    <div class="payment_method_transfer">
                                        <input type="radio" value="<?php echo $payment_method->title ?>" name="parent_transfer" id="transfer-<?= $payment_method->title ?>"><label for="transfer-<?= $payment_method->title ?>" style="margin-left: 8px;display: inline-block;width: 90%;"><?php echo $payment_method->description; ?></label>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div> -->
                        </div>
                    </div>
                </div>
                <?php if($this->agent->is_mobile()) : ?>
                    <div class="right-mobile">
                        <div class="body">
                            <div class="cart-summary">
                                <div class="header">
                                    <div class="title">Detail Pembelian</div>
                                </div>
                                <?php foreach($cart_merchant->result() as $cart_m) : ?>
                                    <?php $carts = $this->cart_model->get_all_cart_by_merchant($this->data['user']->id, $cart_m->merchant)->result_array(); ?>
                                    <?php
                                    foreach ($carts as $item) {
                                        ?>
                                        <?php if($item['option']) : ?>
                                            <?php $product_options = json_decode($item['option'], true) ?>
                                            <?php foreach($product_options as $product) : ?>
                                                <div class="cart-item" style="margin-bottom: 10px;">
                                                    <div class="cart-item-left">
                                                        <div class="item-image">
                                                            <a href="<?php echo seo_url('catalog/products/view/' . $item['product_id'] . '/' . $item['merchant_id']); ?>">
                                                                <img src="<?php echo get_image($product['image']); ?>">
                                                            </a>
                                                        </div>
                                                        <div class="item-desc">
                                                            <div class="item-name" style="font-size: 13px;font-weight: 600;"><a href="<?php echo seo_url('catalog/products/view/' . $item['product_id'] . '/' . $item['merchant_id']); ?>"><?php echo $item['name']; ?></a></div>
                                                            <div class="cart-line">
                                                                <?php if ($product['data_option']) { ?>
                                                                    <?php $options = json_decode($product['data_option']); ?>
                                                                    <?php foreach ($options as $option) { ?>
                                                                        <div class="option"><?php echo $option->value ?></div>
                                                                    <?php } ?>
                                                                    <div class="divider"></div>
                                                                <?php } ?>
                                                                <!-- <div class="option" style="height: 20px;"><?php //echo $product['value']; ?></div> -->
                                                                <!-- <div class="divider"></div> -->
                                                                <div class="info" style="font-size: 11px;"><?php echo $item['code']; ?></div>
                                                                <div class="info" style="font-size: 11px;"><?php echo $product['total_weight']; ?> gram</div>
                                                            </div>
                                                            <div class="cart-line" style="position: relative;">
                                                                <span class="quantity" style="font-size: 11px;color: #888;font-weight: bold;"><?php echo $product['quantity'] . ' x ' . rupiah($product['price']); ?></span>
                                                                <span class="price" style="position: absolute;right: 35px;font-size: 11px;color: #888; font-weight: bold;"><?php echo rupiah($product['quantity'] * $product['price']) ?></span>
                                                            </div>
                                                            <div class="cart-line">
                                                                <?php 
                                                                     // $list_merchant = $this->session->userdata('list_merchant')[0];
                                                                     // $merchant = $this->checkout->get_branch($list_merchant['group']);
                                                                ?>
                                                                <span class="info">Dikirim oleh: 
                                                                    <strong class="sent_merchant"><?php echo $item['merchant_name']; ?></strong>
                                                                    <!-- <strong class="sent_from" style="display:none"><?php //echo $merchant->name;?></strong> -->
                                                                </span>
                                                                
                                                            </div>
                                                            <div class="cart-line">
                                                                <span class="info">
                                                                    <?php
                                                                    $shipping = $cart_m->shipping_courier;
                                                                    if ($shipping == 'pickup') {
                                                                        echo '<strong>Diambil Sendiri</strong>';
                                                                    } else if ($shipping == 'grab') {
                                                                        echo '<strong>Grab Express</strong>';
                                                                    } else {
                                                                        echo 'Jasa Pengiriman: ';
                                                                        $shipping = explode('-', $shipping);
                                                                        echo '<strong>' . strtoupper($shipping[0]) . ' - ' . $shipping[1] . '</strong>';
                                                                    }
                                                                    ?>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php else : ?>
                                            <div class="cart-item" style="margin-bottom: 10px;">
                                                <div class="cart-item-left">
                                                    <div class="item-image">
                                                        <a href="<?php echo seo_url('catalog/products/view/' . $item['product_id'] . '/' . $item['merchant_id']); ?>">
                                                            <img src="<?php echo get_image($item['image']); ?>">
                                                        </a>
                                                    </div>
                                                    <div class="item-desc">
                                                        <div class="item-name" style="font-size: 13px;font-weight: 600;"><a href="<?php echo seo_url('catalog/products/view/' . $item['product_id'] . '/' . $item['merchant_id']); ?>"><?php echo $item['name']; ?></a></div>
                                                        <div class="cart-line">
                                                            <?php if ($item['options']) { ?>
                                                                <?php $options = json_decode($item['options']); ?>
                                                                <?php foreach ($options as $option) { ?>
                                                                    <div class="option"><?php echo $option->option_name ?></div>
                                                                <?php } ?>
                                                                <div class="divider"></div>
                                                            <?php } ?>
                                                            <div class="info" style="font-size: 11px;"><?php echo $item['code']; ?></div>
                                                            <div class="info" style="font-size: 11px;"><?php echo $item['weight']; ?> gram</div>
                                                        </div>
                                                        <div class="cart-line" style="position: relative;">
                                                            <span class="quantity" style="font-size: 11px;color: #888;font-weight: bold;"><?php echo $item['quantity'] . ' x ' . rupiah($item['price']); ?></span>
                                                            <span class="price" style="position: absolute;right: 35px;font-size: 11px;color: #888; font-weight: bold;"><?php echo rupiah($item['grand_total']) ?></span>
                                                        </div>
                                                        <div class="cart-line">
                                                            <?php 
                                                                 // $list_merchant = $this->session->userdata('list_merchant')[0];
                                                                 // $merchant = $this->checkout->get_branch($list_merchant['group']);
                                                            ?>
                                                            <span class="info">Dikirim oleh: 
                                                                <strong class="sent_merchant"><?php echo $item['merchant_name']; ?></strong>
                                                                <!-- <strong class="sent_from" style="display:none"><?php //echo $merchant->name;?></strong> -->
                                                            </span>
                                                            
                                                        </div>
                                                        <div class="cart-line">
                                                            <span class="info">
                                                                <?php
                                                                $shipping = $cart_m->shipping_courier;
                                                                if ($shipping == 'pickup') {
                                                                    echo '<strong>Diambil Sendiri</strong>';
                                                                } else if ($shipping == 'grab') {
                                                                    echo '<strong>Grab Express</strong>';
                                                                } else {
                                                                    echo 'Jasa Pengiriman: ';
                                                                    $shipping = explode('-', $shipping);
                                                                    echo '<strong>' . strtoupper($shipping[0]) . ' - ' . $shipping[1] . '</strong>';
                                                                }
                                                                ?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    <?php } ?>
                                <?php endforeach; ?>
                            </div>
                            <!-- <hr> -->
                            <ul class="purchase-summary">
                                <div class="address-summary">
                                    <li>
                                        <span class="summary-title"><b>Alamat Pengiriman</b></span><br><br>
                                            <div style="width:100%; display: table-cell;vertical-align: top;">
                                                <?php $address = $this->main->get('customer_address', array('id' => $this->session->userdata('checkout_address')));?>
                                                <!-- <p><?php //echo $address->phone; ?> <br>
                                                    <?php //echo $address->address; ?><br>
                                                    <?php //echo $this->main->get('districts', array('id' => $address->district))->name . ', ' . $this->main->get('cities', array('id' => $address->city))->name; ?><br>
                                                    <?php //echo $this->main->get('provincies', array('id' => $address->province))->name; ?>
                                                </p> -->
                                                <span style="font-weight: bold;margin-bottom: 8px;"><?php echo $address->name ?> (<?php echo $address->title; ?>)</span>
                                                <span style="margin-bottom: 8px;"><?php echo $address->phone ?></span>
                                                <span style="margin-bottom: 8px;"><?php echo $address->address ?></span>
                                                <span style="margin-bottom: 8px;"><?php echo $this->main->get('districts', array('id' => $address->district))->name . ', ' . $this->main->get('cities', array('id' => $address->city))->name; ?>,&nbsp;<?php echo $address->postcode ?></span>
                                            </div>
                                    </li>
                                </div>
                                <div class="voucher-summary">
                                    <li>
                                        <?php if($couponn) {
                                            if($couponn->type =='P') {
                                                $total_disc = $couponn->total_disc;
                                            } else {
                                                $total_disc = $couponn->discount;
                                            }
                                        }
                                        ?>
                                        <div id="coupon-input">
                                            <?php if($couponn) {
                                            ?>
                                            <div class="alert alert-success  alert-dismissable">
                                                <a href="javascript:void(0)" onclick="resetCoupon()" class="close" aria-label="close">×</a>
                                                <strong>Voucher "<?php echo $couponn->code ?>" sedang digunakan</strong>
                                            </div>
                                            <?php } else {?>
                                            <span class="voucher-summary-title">Kode Voucher</span>
                                            <input type="text" class="couponCode" name="couponCode" value="<?php echo $couponn ? $couponn->code:''?>" placeholder="Ketik kode voucher">
                                            <!-- <a href="javascript:void(0)" style="background: #a7c22a; border:none;" onclick="submitCoupon()" class="btn btn-default btn-sm">Gunakan</a> -->
                                            <?php } ?>
                                        </div>
                                        <a href="javascript:void(0)" onclick="submitCoupon()" class="btn btn-voucher">Gunakan</a>
                                        <div id="coupon-alert">
                                        </div>
                                    </li>
                                </div>
                                <div class="list-voucher-wrapper col-xs-12">
                                <?php if($vouchers) : ?>
                                    <div class="list-voucher">
                                        <div class="scrollmenu-mobile-nowrap">
                                            <?php foreach($vouchers->result() as $voucher) : ?>
                                                <div class="scrollmenu-item">
                                                    <div class="code_voucher">
                                                        Kode Voucher: <b><?php echo $voucher->code ?></b>
                                                        <a href="javascript:void(0)" type="button" class="btn-copied" onclick="copy_code('<?php echo $voucher->code ?>', this)">Salin</a>
                                                    </div>
                                                    <div class="description_voucher">
                                                        <?php if($voucher->deskripsi != '') : ?>
                                                            <?php if(strlen($voucher->deskripsi) > 50) : ?>
                                                                <?php $voucher_deskripsi = substr($voucher->deskripsi, 0, 50) . '...'; ?>
                                                                <?php echo $voucher_deskripsi ?>
                                                            <?php else : ?>
                                                                <?php echo $voucher->deskripsi ?>
                                                            <?php endif; ?>
                                                        <?php else : ?>
                                                            <span style="color: #f3f3f3">asd</span>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                </div>
                                <div class="billing_details">
                                    <span class="billing_details_title">Rincian Tagihan</span><br>
                                    <li>
                                        <!-- <span class="summary-title">Sub total (<b><?php //echo $this->cart->total_items(); ?></b> produk)</span> -->
                                        <span class="summary-title">Total Harga Barang</span>
                                        <span class="summary-amount"><?php echo rupiah($grand_total); ?></span>
                                    </li>
                                    <li>
                                        <span class="summary-title">Biaya pengiriman</span>
                                        <span id="shippingFee" class="summary-amount"><?php echo rupiah($count_total_shipping); ?></span>
                                    </li>
                                    <hr>
                                    <li>
                                        <span class="summary-title" style="font-weight: bold;">Subtotal</span>
                                        <span class="summary-amount" style="font-weight: bold;"><?php echo rupiah($grand_total + $count_total_shipping); ?></span>
                                    </li>
                                    <li id="showDisc" style="<?php echo $couponn ? '':'display:none;'?>">
                                        <span class="summary-title">Potongan</span>
                                        <span id="coupon_disc" class="summary-amount">- <?php echo $couponn ? rupiah($total_disc) :''?></span>
                                    </li>
                                    <hr>
                                    <li>
                                        <span class="summary-title"><b>Total pembayaran</b></span>
                                        <?php 
                                            $payment_total = $grand_total + $count_total_shipping - ($couponn?$total_disc:0);
                                            $fixed_total = $payment_total;
                                            if($fixed_total <= 0){
                                                $fixed_total = 0;
                                            }
                                        ?>
                                        <span style="color:red" class="summary-amount"><b id="totalShopping"><?php echo rupiah($fixed_total); ?></b></span>
                                    </li>
                                </div>
                                <button class="btn button-submit" onclick="submitPaymentForm()" type="button">Bayar Sekarang</button>
                                <!-- <hr> -->
                            </ul>
                        </div>
                    </div>
                <?php else : ?>
                    <div class="right">
                        <div class="body">
                            <div class="cart-summary">
                                <div class="header">
                                    <div class="title">Detail Pembelian</div>
                                </div>
                                <?php foreach($cart_merchant->result() as $cart_m) : ?>
                                    <?php $carts = $this->cart_model->get_all_cart_by_merchant($this->data['user']->id, $cart_m->merchant)->result_array(); ?>
                                    <?php
                                    foreach ($carts as $item) {
                                        ?>
                                        <?php if($item['option']) : ?>
                                            <?php $product_options = json_decode($item['option'], true); ?>
                                            <?php foreach($product_options as $product) : ?>
                                                <div class="cart-item" style="margin-bottom: 10px;">
                                                    <div class="cart-item-left">
                                                        <div class="item-image">
                                                            <a href="<?php echo seo_url('catalog/products/view/' . $item['product_id'] . '/' . $item['merchant_id']); ?>">
                                                                <img src="<?php echo get_image($product['image']); ?>">
                                                            </a>
                                                        </div>
                                                        <div class="item-desc">
                                                            <div class="item-name" style="font-size: 13px;font-weight: 600;"><a href="<?php echo seo_url('catalog/products/view/' . $item['product_id'] . '/' . $item['merchant_id']); ?>"><?php echo $item['name']; ?></a></div>
                                                            <div class="cart-line">
                                                                <?php if ($product['data_option']) { ?>
                                                                    <?php $options = json_decode($product['data_option']); ?>
                                                                    <?php foreach ($options as $option) { ?>
                                                                        <div class="option" style="height: 20px;"><?php echo $option->value; ?></div>
                                                                    <?php } ?>
                                                                    <div class="divider"></div>
                                                                <?php } ?>
                                                                <!-- <div class="option" style="height: 20px;"><?php //echo $product['value']; ?></div> -->
                                                                <!-- <div class="divider"></div> -->
                                                                <div class="info" style="font-size: 11px;"><?php echo $item['code']; ?></div>
                                                                <div class="info" style="font-size: 11px;"><?php echo $product['total_weight']; ?> gram</div>
                                                            </div>
                                                            <div class="cart-line" style="position: relative;">
                                                                <span class="quantity" style="font-size: 11px;color: #888;font-weight: bold;"><?php echo $product['quantity'] . ' x ' . rupiah($product['price']); ?></span>
                                                                <span class="price" style="position: absolute;right: 35px;font-size: 11px;color: #888; font-weight: bold;"><?php echo rupiah($product['quantity'] * $product['price']) ?></span>
                                                            </div>
                                                            <div class="cart-line">
                                                                <?php 
                                                                     // $list_merchant = $this->session->userdata('list_merchant')[0];
                                                                     // $merchant = $this->checkout->get_branch($list_merchant['group']);
                                                                ?>
                                                                <span class="info">Dikirim oleh: 
                                                                    <strong class="sent_merchant"><?php echo $item['merchant_name']; ?></strong>
                                                                    <!-- <strong class="sent_from" style="display:none"><?php //echo $merchant->name;?></strong> -->
                                                                </span>
                                                                
                                                            </div>
                                                            <div class="cart-line">
                                                                <span class="info">
                                                                    <?php
                                                                    $shipping = $cart_m->shipping_courier;
                                                                    if ($shipping == 'pickup') {
                                                                        echo '<strong>Diambil Sendiri</strong>';
                                                                    } else if ($shipping == 'grab') {
                                                                        echo '<strong>Grab Express</strong>';
                                                                    } else {
                                                                        echo 'Jasa Pengiriman: ';
                                                                        $shipping = explode('-', $shipping);
                                                                        echo '<strong>' . strtoupper($shipping[0]) . ' - ' . $shipping[1] . '</strong>';
                                                                    }
                                                                    ?>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php else : ?>
                                            <div class="cart-item" style="margin-bottom: 10px;">
                                                <div class="cart-item-left">
                                                    <div class="item-image">
                                                        <a href="<?php echo seo_url('catalog/products/view/' . $item['product_id'] . '/' . $item['merchant_id']); ?>">
                                                            <img src="<?php echo get_image($item['image']); ?>">
                                                        </a>
                                                    </div>
                                                    <div class="item-desc">
                                                        <div class="item-name" style="font-size: 13px;font-weight: 600;"><a href="<?php echo seo_url('catalog/products/view/' . $item['product_id'] . '/' . $item['merchant_id']); ?>"><?php echo $item['name']; ?></a></div>
                                                        <div class="cart-line">
                                                            <?php if ($item['options']) { ?>
                                                                <?php $options = json_decode($item['options']); ?>
                                                                <?php foreach ($options as $option) { ?>
                                                                    <div class="option" style="height: 20px;"><?php echo $option->option_name; ?></div>
                                                                <?php } ?>
                                                                <div class="divider"></div>
                                                            <?php } ?>
                                                            <div class="info" style="font-size: 11px;"><?php echo $item['code']; ?></div>
                                                            <div class="info" style="font-size: 11px;"><?php echo $item['weight']; ?> gram</div>
                                                        </div>
                                                        <div class="cart-line" style="position: relative;">
                                                            <span class="quantity" style="font-size: 11px;color: #888;font-weight: bold;"><?php echo $item['quantity'] . ' x ' . rupiah($item['price']); ?></span>
                                                            <span class="price" style="position: absolute;right: 35px;font-size: 11px;color: #888; font-weight: bold;"><?php echo rupiah($item['grand_total']) ?></span>
                                                        </div>
                                                        <div class="cart-line">
                                                            <?php 
                                                                 // $list_merchant = $this->session->userdata('list_merchant')[0];
                                                                 // $merchant = $this->checkout->get_branch($list_merchant['group']);
                                                            ?>
                                                            <span class="info">Dikirim oleh: 
                                                                <strong class="sent_merchant"><?php echo $item['merchant_name']; ?></strong>
                                                                <!-- <strong class="sent_from" style="display:none"><?php //echo $merchant->name;?></strong> -->
                                                            </span>
                                                            
                                                        </div>
                                                        <div class="cart-line">
                                                            <span class="info">
                                                                <?php
                                                                $shipping = $cart_m->shipping_courier;
                                                                if ($shipping == 'pickup') {
                                                                    echo '<strong>Diambil Sendiri</strong>';
                                                                } else if ($shipping == 'grab') {
                                                                    echo '<strong>Grab Express</strong>';
                                                                } else {
                                                                    echo 'Jasa Pengiriman: ';
                                                                    $shipping = explode('-', $shipping);
                                                                    echo '<strong>' . strtoupper($shipping[0]) . ' - ' . $shipping[1] . '</strong>';
                                                                }
                                                                ?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    <?php } ?>
                                <?php endforeach; ?>
                            </div>
                            <!-- <hr> -->
                            <ul class="purchase-summary">
                                <div class="address-summary">
                                    <li>
                                        <span class="summary-title"><b>Alamat Pengiriman</b></span><br><br>
                                            <div style="width:100%; display: table-cell;vertical-align: top;">
                                                <?php $address = $this->main->get('customer_address', array('id' => $this->session->userdata('checkout_address')));?>
                                                <!-- <p><?php //echo $address->phone; ?> <br>
                                                    <?php //echo $address->address; ?><br>
                                                    <?php //echo $this->main->get('districts', array('id' => $address->district))->name . ', ' . $this->main->get('cities', array('id' => $address->city))->name; ?><br>
                                                    <?php //echo $this->main->get('provincies', array('id' => $address->province))->name; ?>
                                                </p> -->
                                                <span style="font-weight: bold;margin-bottom: 8px;"><?php echo $address->name ?> (<?php echo $address->title; ?>)</span>
                                                <span style="margin-bottom: 8px;"><?php echo $address->phone ?></span>
                                                <span style="margin-bottom: 8px;"><?php echo $address->address ?></span>
                                                <span style="margin-bottom: 8px;"><?php echo $this->main->get('districts', array('id' => $address->district))->name . ', ' . $this->main->get('cities', array('id' => $address->city))->name; ?>,&nbsp;<?php echo $address->postcode ?></span>
                                            </div>
                                    </li>
                                </div>
                                <div class="voucher-summary">
                                    <li>
                                        <?php if($couponn) {
                                            if($couponn->type =='P') {
                                                $total_disc = $couponn->total_disc;
                                            } else {
                                                $total_disc = $couponn->total_disc;
                                            }
                                        }
                                        ?>
                                        <div id="coupon-input">
                                            <?php if($couponn) {
                                            ?>
                                            <div class="alert alert-success  alert-dismissable">
                                                <a href="javascript:void(0)" onclick="resetCoupon()" class="close" aria-label="close">×</a>
                                                <strong>Voucher "<?php echo $couponn->code ?>" sedang digunakan</strong>
                                            </div>
                                            <?php } else {?>
                                            <span class="voucher-summary-title">Kode Voucher</span>
                                            <input type="text" class="couponCode" name="couponCode" value="<?php echo $couponn ? $couponn->code:''?>" placeholder="Ketik kode voucher">
                                            <!-- <a href="javascript:void(0)" style="background: #a7c22a; border:none;" onclick="submitCoupon()" class="btn btn-default btn-sm">Gunakan</a> -->
                                            <?php } ?>
                                        </div>
                                        <a href="javascript:void(0)" onclick="submitCoupon()" class="btn btn-voucher">Gunakan</a>
                                        <div id="coupon-alert">
                                        </div>
                                    </li>
                                </div>
                                <?php if($vouchers) : ?>
                                    <div class="list-voucher">
                                        <div class="scrollmenu slick-vouchers">
                                            <?php foreach($vouchers->result() as $voucher) : ?>
                                                <div class="scrollmenu-item">
                                                    <div class="code_voucher">
                                                        Kode Voucher: <b><?php echo $voucher->code ?></b>
                                                        <button type="button" class="btn-copied" onclick="copy_code('<?php echo $voucher->code ?>', this)" data-toggle="tooltip" data-placement="top" title="Salin Kode" data-container="body">Salin</button>
                                                    </div>
                                                    <div class="description_voucher">
                                                        <?php if($voucher->deskripsi != '') : ?>
                                                            <?php if(strlen($voucher->deskripsi) > 50) : ?>
                                                                <?php $voucher_deskripsi = substr($voucher->deskripsi, 0, 50) . '...' ?>
                                                                <?php echo $voucher_deskripsi ?>
                                                            <?php else : ?>
                                                                <?php echo $voucher->deskripsi ?>
                                                            <?php endif; ?>
                                                        <?php else : ?>
                                                            <span style="color: #f3f3f3">a</span>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <div class="billing_details">
                                    <span class="billing_details_title">Rincian Tagihan</span><br>
                                    <li>
                                        <!-- <span class="summary-title">Sub total (<b><?php //echo $this->cart->total_items(); ?></b> produk)</span> -->
                                        <span class="summary-title">Total Harga Barang</span>
                                        <span class="summary-amount"><?php echo rupiah($grand_total); ?></span>
                                    </li>
                                    <li>
                                        <span class="summary-title">Biaya pengiriman</span>
                                        <span id="shippingFee" class="summary-amount"><?php echo rupiah($count_total_shipping); ?></span>
                                    </li>
                                    <hr>
                                    <li>
                                        <span class="summary-title" style="font-weight: bold;">Subtotal</span>
                                        <span class="summary-amount" style="font-weight: bold;"><?php echo rupiah($grand_total + $count_total_shipping); ?></span>
                                    </li>
                                    <li id="showDisc" style="<?php echo $couponn ? '':'display:none;'?>">
                                        <span class="summary-title">Potongan</span>
                                        <span id="coupon_disc" class="summary-amount">- <?php echo $couponn ? rupiah($total_disc) :''?></span>
                                    </li>
                                    <hr>
                                    <li>
                                        <span class="summary-title"><b>Total pembayaran</b></span>
                                        <?php 
                                            $payment_total = $grand_total + $count_total_shipping - ($couponn?$total_disc:0);
                                            $fixed_total = $payment_total;
                                            if($fixed_total <= 0){
                                                $fixed_total = 0;
                                            }
                                        ?>
                                        <span style="color:red" class="summary-amount"><b id="totalShopping"><?php echo rupiah($fixed_total); ?></b></span>
                                    </li>
                                </div>
                                <button class="btn button-submit" onclick="submitPaymentForm()" type="button">Bayar Sekarang</button>
                                <!-- <hr> -->
                            </ul>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
            <!-- <div class="cart-footer">
                <div class="action">
                    <a href="<?php echo site_url('cart'); ?>"><i class="fa fa-arrow-left"></i> Kembali ke Cart</a>
                    <a href="javascript:void(0);" class="btn" onclick="submitPaymentForm()">BAYAR SEKARANG</a>
                </div>
            </div> -->
        </form>
            <div class="modal fade" id="KredivoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <button type="button" class="close" 
                        data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">
                            Kredivo Agreement
                        </h4>
                    </div>
                    
                    <!-- Modal Body -->
                    <div class="modal-body">
                        <div>
                            <div>
                                <p>Anda akan diarahkan ke halaman kredivo</p>
                                <ol style="list-style-type: lower-alpha;">  
                                    <li style="list-style: inherit;margin-left:15px;"> Anda harus mempunyai akun Kredivo, atau registrasi ekspres pada halaman Kredivo setelah klik bayar </li>
                                    <li style="list-style: inherit;margin-left:15px;"> Siapkan dokumen berikut untuk diunggah (khusus pengguna baru Kredivo) </li>
                                    <li style="list-style: inherit;margin-left:15px;"> Kartu Identitas (KTP) </li>
                                    <li style="list-style: inherit;margin-left:15px;"> Bukti Tempat Tinggal (Akun E-commerce) </li>
                                    <li style="list-style: inherit;margin-left:15px;"> Bukti Penghasilan (Akun Internet Banking) </li>
                                    <li style="list-style: inherit;margin-left:15px;"> Berdomisili di Jabodetabek, Kota Bandung, Medan, Surabaya, Palembang, Semarang atau Denpasar </li>
                                    <li style="list-style: inherit;margin-left:15px;"> Berpenghasilan min. Rp 3.000.000 per bulan </li>
                                    <li style="list-style: inherit;margin-left:15px;"> Metode Tunda Pembayaran hanya untuk pembayaran maks. Rp 3.000.000 </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Modal Footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">
                                    Close
                        </button>
                        <a href="javascript:void(0)" class="btn btn-primary" onclick="agreeKredivo()">
                           Lanjutkan
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js" integrity="sha384-YGnnOBKslPJVs35GG0TtAZ4uO7BHpHlqJhs0XK3k6cuVb6EBtl+8xcvIIOKV5wB+" crossorigin="anonymous"></script>
<script>
    $(window).on('resize orientationchange', function() {
        $('.slick-vouchers').slick('resize');
    });
    $(function() {
        $('.slick-vouchers').slick({
            dots: false,
            arrow: false,
            // slidesToShow: 1,
            autoplay: false,
            infinite: false,
            // slidesToScroll: 1,
            // centerMode: false,
            variableWidth: true
        });
        $('[data-toggle="tooltip"]').tooltip();
    })
    function copy_code(val, e) {
        $('.btn-copied').text('Salin');
        $('.btn-copied').css('color', '#555');
        var tempInput = document.createElement("input");
        tempInput.style = "position: absolute; left: -10000px; top: -10000px";
        tempInput.value = val;
        document.body.appendChild(tempInput);
        tempInput.select();
        document.execCommand("copy");
        document.body.removeChild(tempInput);
        $(e).html('<i class="fa fa-check" aria-hidden="true"></i> Tersalin');
        $(e).css('color', '#5CB85C');
    }
</script>