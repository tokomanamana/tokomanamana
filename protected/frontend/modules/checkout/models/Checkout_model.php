<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Checkout_model extends CI_Model {

    function getShippings($district) {
        $this->db->select('s.*, sc.cost, sc.district, sc.estimate')
                ->join('shippings s', 's.id = sc.shipping', 'left');
        $query = $this->db->get_where('shipping_cost sc', array('district' => $district));
        return ($query->num_rows() > 0) ? $query : false;
    }

    function getOrderByCode($code) {
        $this->db->select("o.*, os.name order_status_name, os.color order_status_color, sd.name shipping_district_name, sc.name shipping_city_name, sp.name shipping_province_name, s.name shipping_courier_name", FALSE)
                ->join('order_status os', 'o.order_status = os.id', 'left')
                ->join('shipping_cities sc', 'sc.id = o.shipping_city', 'left')
                ->join('shipping_districts sd', 'sd.id = o.shipping_district', 'left')
                ->join('shipping_provincies sp', 'sp.id = o.shipping_province', 'left')
                ->join('shippings s', 's.id = o.shipping_courier', 'left')
                ->where('o.code', $code);
        $query = $this->db->get('orders o');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    function get_order_by_id($id) {
        $this->db->select("o.*, os.name order_status_name, os.color order_status_color, sd.name shipping_district_name, sc.name shipping_city_name, sp.name shipping_province_name, s.name shipping_courier_name", FALSE)
                ->join('order_status os', 'o.order_status = os.id', 'left')
                ->join('shipping_cities sc', 'sc.id = o.shipping_city', 'left')
                ->join('shipping_districts sd', 'sd.id = o.shipping_district', 'left')
                ->join('shipping_provincies sp', 'sp.id = o.shipping_province', 'left')
                ->join('shippings s', 's.id = o.shipping_courier', 'left')
                ->where('o.id', $id);
        $query = $this->db->get('orders o');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    function get_merchants($product, $quantity, $pricing_level = 1) {
        $location = $this->session->userdata('location');
        return $this->db->query("SELECT *, (6971 * acos(cos(radians($location[lat])) * cos(radians(lat)) * cos(radians(lng) - radians($location[lng])) + sin(radians($location[lat])) * sin(radians(lat)))) AS distance FROM merchants WHERE status = 1 AND (type = 'merchant' OR type = 'distributor' OR type = 'principle_branch') AND id IN (SELECT merchant FROM product_merchant WHERE quantity >= $quantity AND product = $product " . ($pricing_level ? "AND pricing_level = 1" : "") . ") HAVING distance < (CASE
        WHEN type='merchant' THEN 40
        ELSE 200
        END ) ORDER BY distance LIMIT 5");
//        return $this->db->get('orders o');
    }

    function get_merchants_with_favourite($product, $quantity, $pricing_level = 1) {
        $location = $this->session->userdata('location');
        return $this->db->query("
        SELECT m.*,(6971 * acos(cos(radians($location[lat])) * cos(radians(lat)) * cos(radians(lng) - radians($location[lng])) + sin(radians($location[lat])) * sin(radians(lat)))) AS distance FROM favourite_shop fs LEFT JOIN  merchants m ON m.id = fs.merchant_id
        WHERE m.status = 1 AND (m.type = 'merchant' OR type = 'm.distributor') AND m.id IN (SELECT merchant FROM product_merchant WHERE quantity >= $quantity AND product = $product " . ($pricing_level ? "AND pricing_level = 1" : "") . ")
        UNION
        SELECT *, (6971 * acos(cos(radians($location[lat])) * cos(radians(lat)) * cos(radians(lng) - radians($location[lng])) + sin(radians($location[lat])) * sin(radians(lat)))) AS distance FROM merchants WHERE status = 1 AND (type = 'merchant' OR type = 'distributor' OR type = 'principle_branch') AND id IN (SELECT merchant FROM product_merchant WHERE quantity >= $quantity AND product = $product " . ($pricing_level ? "AND pricing_level = 1" : "") . ") HAVING distance < (CASE
        WHEN type='merchant' THEN 40
        ELSE 200
        END ) ORDER BY distance LIMIT 5
        ");
//        return $this->db->get('orders o');
    }

    function get_merchant_stock($product, $store, $quantity) {
        return $this->db->query("SELECT * FROM products_principal_stock
                                    WHERE product_id = $product
                                    AND branch_id = $store
                                    AND quantity >= $quantity");
    }

    function get_favourite_merchants($product, $quantity, $pricing_level = 1, $customer_id) {
        $location = $this->session->userdata('location');
        return $this->db->query("
        SELECT m.*,(6971 * acos(cos(radians($location[lat])) * cos(radians(lat)) * cos(radians(lng) - radians($location[lng])) + sin(radians($location[lat])) * sin(radians(lat)))) AS distance FROM favourite_shop fs LEFT JOIN  merchants m ON m.id = fs.merchant_id
        WHERE m.status = 1 AND fs.customer_id = ".$customer_id." AND (m.type = 'merchant' OR type = 'm.distributor'  OR type = 'branch') AND m.id IN (SELECT merchant FROM product_merchant WHERE quantity >= $quantity AND product = $product " . ($pricing_level ? "AND pricing_level = 1" : "") . ")
        ");
//        return $this->db->get('orders o');
    }

    function get_merchant($id) {
        $location = $this->session->userdata('location');
        $this->db->select("m.id, m.name, c.name city, d.name district, IFNULL(r.rating,0) rating, m.username,(CASE WHEN lat != '' AND lng != '' THEN(6971 * acos(cos(radians($location[lat])) * cos(radians(lat)) * cos(radians(lng) - radians($location[lng])) + sin(radians($location[lat])) * sin(radians(lat)))) ELSE 0 END) AS distance")
                ->join('cities c', 'm.city = c.id', 'left')
                ->join('districts d', 'm.district = d.id', 'left')
                ->join('(SELECT merchant, ((SUM(rating_speed)+SUM(rating_service)+SUM(rating_accuracy))/(COUNT(id)*3)) rating FROM product_review
	WHERE merchant = ' . $id . ') r', 'r.merchant = m.id', 'left')
                ->where('m.id', $id)
                ->where('m.status', 1);
        return $this->db->get('merchants m')->row();
    }

    // function get_branch($group) {
    //     $this->db->join('merchants m', 'mg.branch = m.id', 'left')
    //             ->where('mg.id', $group)
    //             ->select('m.*');
    //     return $this->db->get('merchant_groups mg')->row();
    // }
    function check_branch_stock($merchant,$product, $quantity, $pricing_level = 1){
        $q = $this->db->query("SELECT * FROM product_merchant WHERE merchant = '$merchant' AND quantity >= '$quantity' AND product = '$product' ");
        return $q;
    }
    function customer_address($customer) {
        $this->db->select('ca.*, p.name province_name, c.name city_name, d.name district_name')
                ->join('provincies p', 'p.id = ca.province')
                ->join('cities c', 'c.id = ca.city')
                ->join('districts d', 'd.id = ca.district')
                ->where('ca.customer', $customer);
        return $this->db->get('customer_address ca');
    }

    function customer_address_primary($customer) {
        $this->db->select('ca.*, p.name province_name, c.name city_name, d.name district_name')
                ->join('provincies p', 'p.id = ca.province')
                ->join('cities c', 'c.id = ca.city')
                ->join('districts d', 'd.id = ca.district')
                ->where('ca.customer', $customer)
                ->where('ca.primary', 1);
        return $this->db->get('customer_address ca');
    }

    function customer_address_none($customer) {
        $this->db->select('ca.*, p.name province_name, c.name city_name, d.name district_name')
                ->join('provincies p', 'p.id = ca.province')
                ->join('cities c', 'c.id = ca.city')
                ->join('districts d', 'd.id = ca.district')
                ->where('ca.customer', $customer)
                ->order_by('id', 'DESC')
                ->limit(1);
        return $this->db->get('customer_address ca');
    }

    function get_address($id) {
        $this->db->select('ca.*, p.name province_name, c.name city_name, d.name district_name')
                ->join('provincies p', 'p.id = ca.province')
                ->join('cities c', 'c.id = ca.city')
                ->join('districts d', 'd.id = ca.district')
                ->where('ca.id', $id);
        return $this->db->get('customer_address ca');
    }

    function check_kode_unik($total, $kode){
        $ko = $this->db->query("SELECT * FROM orders WHERE unique_code = '$kode' AND total = '$total' AND payment_status = 'Pending' ");
        return $ko;
    }

    function get_preorder($id){
        return $this->db->query("SELECT max(preorder_time) as preorder_time FROM order_product WHERE `order` = '$id' ");
    }

    function check_kode_transfer($kode, $month){
        $ko = $this->db->query("SELECT * FROM orders WHERE transfer_code = '$kode' AND MONTH(date_added) = '$month' ");
        return $ko;
    }

    public function get_vouchers() {
        $customer = $this->data['user']->id;
        // $query = "SELECT * FROM `coupons` WHERE `status` = 1 AND (`date_start` <= NOW() AND `date_end` >= NOW()) AND `show` = 1 ORDER BY `date_added` DESC";
        $query = "SELECT * FROM `coupons` WHERE `status` = 1 AND CURDATE() BETWEEN `date_start` AND `date_end` AND `show` = 1 AND `event` = 0 AND (`customer_id` = 0 OR `customer_id` = $customer) ORDER BY customer_id DESC";
        return $this->db->query($query);
    }

}
