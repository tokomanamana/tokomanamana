<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cart_model extends CI_Model {

    function get_cart_products($auth) {
        $this->db->select('c.*, p.id, p.code, p.name, p.price_old, p.price, p.discount, p.weight, p.short_description, pi.image')
                ->join('products p', 'p.id = c.product', 'left')
                ->join('product_image pi', 'p.id = pi.product AND primary = 1', 'left');
        $query = $this->db->get_where('cart c', array('auth' => $auth));
        return ($query->num_rows() > 0) ? $query : false;
    }

    function get_total_cart() {
        $this->db->select('SUM(c.quantity) total');
        $query = $this->db->get_where('cart c', array('auth' => $this->data['user']->id))->row();
        return ($query->total > 0) ? number($query->total) : 0;
    }

    function getTotalWeightCart() {
        $this->db->select('ROUND(SUM(p.weight),2) total')
                ->join('products p', 'p.id = c.product', 'left');
        $query = $this->db->get_where('cart c', array('auth' => $this->data['user']->id))->row();
        return ($query->total > 0) ? $query->total : 0;
    }

    function get_product($id1, $id2, $id3) {
        $this->db->select('p.id, p.name, p.short_description, p.code, pi.image')
                ->join('product_image pi', 'pi.product = p.id', 'left')
                ->where('p.id', $id1);
        if ($id2) {
            $this->db->select('p.weight, p.price_old, p.price, p.discount, p.quantity, p.merchant, m.name merchant_name')
                    ->join('merchants m', 'm.id = p.merchant', 'left')
                    ->where('p.merchant', $id2);
        } else {
            $this->db->select('pm.weight, pm.price_old, pm.price, pm.discount, pm.quantity, pm.merchant, m.name merchant_name')
                    ->join('product_merchant pm', 'p.id = pm.product', 'left')
                    ->join('merchants m', 'm.id = pm.merchant', 'left')
                    ->where('p.merchant', 0)
                    ->where('pm.merchant', $id3);
        }
        $query = $this->db->get('products p');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    // function check_cart($product, $merchant, $id_customer, $ip_address, $id_option = null) {
    //     if($id_option) {
    //         return $this->db->query("SELECT * FROM cart WHERE product = $product AND merchant = $merchant AND id_customer = $id_customer AND id_option = $id_option AND ip_address = '$ip_address' AND status = 0")->row_array();
    //     } else {
    //         return $this->db->query("SELECT * FROM cart WHERE product = $product AND merchant = $merchant AND id_customer = $id_customer AND ip_address = '$ip_address' AND status = 0")->row_array();
    //     }
    // }
    function check_cart($product, $merchant, $id_customer, $ip_address) {
        return $this->db->query("SELECT * FROM cart WHERE product = $product AND merchant = $merchant AND id_customer = $id_customer AND ip_address = '$ip_address' AND status = 0")->row_array();
    }

    function check_cart_update($id_cart) {
        return $this->db->query("SELECT * FROM cart WHERE id = $id_cart AND status = 0")->row_array();
    }

    function get_cart($id) {
        return $this->db->query("SELECT * FROM cart WHERE id = $id AND status = 0")->row();
    }

    function get_all_cart($id_customer = null, $ip_address = null) {
        $this->db->select('
                    products.name,
                    products.type,
                    products.code,
                    products.category,
                    products.description,
                    products.preorder,
                    products.brand,
                    products.package_items,
                    merchants.name AS merchant_name,
                    merchants.group AS merchant_group,
                    cart.grand_total,
                    cart.quantity,
                    cart.total_weight AS weight,
                    cart.id AS id,
                    cart.image,
                    cart.product AS product_id,
                    cart.merchant AS merchant_id,
                    cart.price AS price,
                    cart.id_customer AS id_customer,
                    cart.options,
                    cart.promo_data,
                    cart.in_promo,
                    cart.old_price,
                    cart.disc_price,
                    cart.option
                ')
            ->join('products', 'cart.product = products.id', 'LEFT')
            ->join('product_image', 'product_image.product = products.id AND product_image.primary = 1', 'LEFT')
            ->join('merchants', 'cart.merchant = merchants.id', 'FALSE')
            ->where('cart.status', 0)
            ->where('id_customer', $id_customer);

        return $this->db->get('cart');
    }

    public function get_cart_by_merchant($id_customer, $id_merchant) {
        return $this->db->query("SELECT * FROM cart WHERE merchant = $id_merchant AND id_customer = $id_customer AND status = 0");
    }

    public function get_all_cart_by_merchant($id_customer, $id_merchant) {
        $this->db->select('
                products.name,
                products.type,
                products.code,
                products.category,
                products.description,
                products.preorder,
                products.brand,
                products.package_items,
                merchants.name AS merchant_name,
                merchants.group AS merchant_group,
                cart.grand_total,
                cart.quantity,
                cart.total_weight AS weight,
                cart.id AS id,
                cart.image,
                cart.product AS product_id,
                cart.merchant AS merchant_id,
                cart.price AS price,
                cart.id_customer AS id_customer,
                cart.options,
                cart.promo_data,
                cart.in_promo,
                cart.old_price,
                cart.disc_price,
                cart.option
            ')
        ->join('products', 'cart.product = products.id', 'LEFT')
        ->join('product_image', 'product_image.product = products.id AND product_image.primary = 1', 'LEFT')
        ->join('merchants', 'cart.merchant = merchants.id', 'FALSE')
        ->where('cart.status', 0)
        ->where('cart.merchant', $id_merchant)
        ->where('cart.id_customer', $id_customer);
        return $this->db->get('cart');
    }

    public function cart_merchant($id_customer, $id_merchant = null) {
        if($id_merchant) {
            return $this->db->query("SELECT * FROM cart_merchant WHERE merchant = $id_merchant AND id_customer = $id_customer");
        } else {
            return $this->db->query("SELECT * FROM cart_merchant WHERE id_customer = $id_customer");
        }
    }

}
