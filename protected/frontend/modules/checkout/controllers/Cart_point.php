<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cart_point extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->lang->load('checkout', settings('language'));
        $this->load->model('catalog/product_model', 'product');
        $this->load->model('catalog/product_price_model', 'product_prices');
        $this->load->model('cart_model');
        $this->load->model('Model');
    }

    public function index() {
        $param_redirect = $this->Model->cek_cart_detail($this->session->userdata('email'));
        if($param_redirect == ''){
            redirect(base_url('member/point'));
        }else if($param_redirect == '1'){
            redirect(base_url('checkout/checkout_reward'));
        }else if($param_redirect == '2'){
            redirect(base_url('checkout/checkout_reward/finish'));
        }
        $this->session->unset_userdata('coupon');
        $this->output->set_title(lang('cart_text') . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->js('assets/frontend/js/modules/cart.js');
        $this->data['page'] = 'cart';

        $this->load->view('cart_point', $this->data);
    }

    public function check_stock(){
        $email = $this->session->userdata('email');
        $m = $this->Model;

        $q_customer = $m->get_data('', 'customers', null, array('email' => $email));

        $q_reward_point = $m->get_data('prcd.id_reward, prcd.id as p_id, r.*, prcd.id_address', 'point_reward_cart_detail prcd', array(0 => 'point_reward_cart prc-prc.id=prcd.id_cart', 1 => 'reward r-r.id=prcd.id_reward'), array('prc.id_customer' => $q_customer->row()->id));

        //cek periode
        if(!$m->cek_periode_tukar_point()){
            $m->delete_data1('point_reward_cart_detail', array('id' => $q_reward_point->row()->p_id));
            die(json_encode(array('success' => '0', 'msg' => 'Periode Tukar Point Belum Dimulai')));
        }

        //cek point cust
        if(!$m->cek_point_customer('', $q_customer->row()->id, $q_reward_point->row()->id_reward)){
            $m->delete_data1('point_reward_cart_detail', array('id' => $q_reward_point->row()->p_id));
            die(json_encode(array('success' => '0', 'msg' => 'Poin Anda Tidak Cukup')));
        }

        //cek stok
        if(!$m->cek_stok_reward($q_reward_point->row()->id_reward)){
            $m->delete_data1('point_reward_cart_detail', array('id' => $q_reward_point->row()->p_id));
            die(json_encode(array('success' => '0', 'msg' => 'Reward yang Ingin Anda Tukar Telah Habis')));
        }

        if($q_reward_point->row()->id_product != 0 || $q_reward_point->row()->id_product_paket != 0){
            $m->update_data('point_reward_cart_detail', array('status' => '1'), array('id' => $q_reward_point->row()->p_id));
            $link = '/checkout_reward';
        }else{
            //ubah status cart detail jadi 2
            $m->update_data('point_reward_cart_detail', array('status' => '2'), array('id' => $q_reward_point->row()->p_id));

            //0 kan point customer
            $m->update_data('point', array('point' => 0, 'last_update' => date('Y/m/d H:i:s')), array('id_customer' => $q_customer->row()->id));

            $reward = $m->get_data('', 'reward', null, array('id' => $q_reward_point->row()->id_reward));
            //kurangi qty reward
            $m->update_data('reward', array('qty' => ($reward->row()->qty - 1)), array('id' => $q_reward_point->row()->id_reward));

            $q_reward_claim = $m->get_data('', 'reward_claim_temp', null, array('id_reward' => $q_reward_point->row()->id_reward, 'status_periode_aktif' => 1));

            $q_setting = $m->get_data('', 'settings', null, array('key' => 'periode_tukar_point'))->row()->value;
            $exp = explode(';', $q_setting);
            $periode_tukar_point = $exp[0].';'.$exp[1].';'.$exp[2].';'.$exp[3].';'.$exp[4].';'.$exp[5];

            //$m->delete_data1('point_reward_cart_detail', array('id' => $q_reward_point->row()->id));
            //insert/update reward_claim_temp
            if($q_reward_claim->num_rows() > 0){
                if($q_reward_claim->row()->id_customer != ''){
                    $add_['id_customer'] = $q_reward_claim->row()->id_customer.','.$q_customer->row()->id.',';
                }else{
                    $add_['id_customer'] = ','.$q_customer->row()->id.',';
                }
                $m->update_data('reward_claim_temp', $add_, array('id_reward' => $q_reward_point->row()->id_reward, 'status_periode_aktif' => 1));

                $id_reward_claim_ = $q_reward_claim->row()->id;
            }else{
                $m->insert_data('reward_claim_temp', array('id_reward' => $q_reward_point->row()->id_reward, 'id_customer' => ','.$q_customer->row()->id.',', 'id_merchant' => '', 'periode' => $periode_tukar_point, 'status_periode_aktif' => 1));

                $id_reward_claim_ = $m->get_data('', 'reward_claim_temp', null ,array('id_reward' => $q_reward_point->row()->id_reward, 'id_customer' => ','.$q_customer->row()->id.',', 'periode' => $periode_tukar_point))->row()->id;
            }

            $data = array(
                'id_merchant' => 0,
                'id_customer' => $q_customer->row()->id,
                'id_reward' => $q_reward_point->row()->id_reward,
                'last_update' => date('Y/m/d H:i:s'),
                'status_pengiriman' => 1,
                'id_reward_claim' => $id_reward_claim_,
                'kode' => $m->get_id('peh'),
                'id_address' => 0
            );
            //insert point_exchange_history
            $m->insert_data('point_exchange_history', $data);

            //daftarkan customer ke coupon
            if($reward->row()->id_coupon != 0){
                $q_coupon = $m->get_data('', 'coupons', null, array('id' => $reward->row()->id_coupon));
                if($q_coupon->num_rows() > 0){
                    if($q_coupon->row()->id_customer == ''){
                        $id_cust = ','.$this->session->userdata('user_id').',';
                    }else{
                        $id_cust = $q_coupon->row()->id_customer.','.$this->session->userdata('user_id').',';
                    }
                    $m->update_data('coupons', array('id_customer' => $id_cust), array('id' => $q_coupon->row()->id));
                }
            }
            $link = '/checkout_reward/finish';
        }

        die(json_encode(array('success' => '1', 'link' => $link)));
    }

    public function back(){
        $m = $this->Model;

        $q_cart = $m->get_data('', 'point_reward_cart', null, array('id_customer' => $this->session->userdata('user_id')));
        if($q_cart->num_rows() > 0){
            $m->delete_data1('point_reward_cart_detail', array('id_cart' => $q_cart->row()->id));
        }

        die(json_encode(array('success' => 1)));
    }

    //method ini dari auth/member/point
    public function add_cart_point(){
        $id_reward = $this->input->post('id_reward');
        $email = $this->input->post('email');
        $m = $this->Model;

        //cek periode tidak perlu

        $q_customer = $m->get_data('id', 'customers', null, array('email' => $email));
        //cek point cust
        if(!$m->cek_point_customer('', $q_customer->row()->id, $id_reward)){
            die(json_encode(array('success' => '0', 'msg' => 'Poin Anda Tidak Cukup')));
        }
        //cek stok
        if(!$m->cek_stok_reward($id_reward)){
            die(json_encode(array('success' => '0', 'msg' => 'Reward yang Ingin Anda Tukar Telah Habis')));
        }
        
        $q0 = $m->get_data('', 'point_reward_cart', null, array('id_customer' => $q_customer->row()->id));

        if($q0->num_rows() > 0){
            $data0['date_modified'] = date('Y/m/d H:i:s');
            $where0['id'] = $q0->row()->id;

            $m->update_data('point_reward_cart', $data0, $where0);

            $q1 = $m->get_data('', 'point_reward_cart_detail', null, array('id_cart' => $q0->row()->id, 'id_reward' => $id_reward));

            if($q1->num_rows() > 0){
                die(json_encode(array('success' => 1)));
            }else{

                $m->delete_data1('point_reward_cart_detail', array('id_cart' => $q0->row()->id));
                $data1 = array(
                    'id_cart' => $q0->row()->id,
                    'id_reward' => $id_reward,
                    'date_add' => date('Y/m/d H:i:s'),
                    'date_modified' => date('Y/m/d H:i:s'),
                    'qty' => 1,
                    'point' => $m->get_data('point', 'reward', null, array('id' => $id_reward), '', '', 1)->row()->point
                );

                $m->insert_data('point_reward_cart_detail', $data1);
                die(json_encode(array('success' => 1)));
            }
        }else{
            $data0 = array(
                'date_add' => date('Y/m/d H:i:s'),
                'date_modified' => date('Y/m/d H:i:s'),
                'id_customer' => $q_customer->row()->id,
                'id_merchant' => 0
            );

            $m->insert_data('point_reward_cart', $data0);

            $q1 = $m->get_data('id', 'point_reward_cart', null, array('id_customer' => $q_customer->row()->id));

            $data1 = array(
                'id_cart' => $q1->row()->id,
                'id_reward' => $id_reward,
                'date_add' => date('Y/m/d H:i:s'),
                'date_modified' => date('Y/m/d H:i:s'),
                'qty' => 1,
                'point' => $m->get_data('point', 'reward', null, array('id' => $id_reward), '', '', 1)->row()->point
            );

            $m->insert_data('point_reward_cart_detail', $data1);

            die(json_encode(array('success' => 1)));
        }
    }

    public function add() {
        $data = $this->input->post(null, true);
        $id = decode($data['product']);
        $qty = $data['qty'];
        $output = array('status' => 'error', 'message' => 'Qty minimum 1');
        if ($qty > 0) {
            $product = $this->product->get_product($id);
            if ($product) {
                do {
                    $product_option = 0;
                    if (isset($data['option'])) {
                        $product_option = $this->product->get_product_option_id($product->id, $data['option']);
                        if (!$product_option) {
                            $output['message'] = 'Kesalahan memilih variasi produk.';
                            break;
                        }
//                    $product_option = $this->main->get('product_option', array('id' => $current_product_option));
                        $options = $this->product->get_option_groups($product->id, $product_option);
                        $option = $options->row();
//                    print_r($option);
//                    $product_option = $product_options->row();
                        $image = $this->product->get_product_option_images($product_option);
                        if ($image->num_rows() > 0) {
                            $product->image = $image->row()->image;
                        }
                    }
                    if ($product->length && $product->height && $product->width) {
                        $weight_volume = ($product->length * $product->height * $product->width) / 6;
                        if ($weight_volume > $product->weight)
                            $product->weight = round($weight_volume);
                    }
                    $old_price = $product->price;
                    if($product->promo == 1){
                        $promo_data = json_decode($product->promo_data,true);
                        if($promo_data['type'] == 'P') {
                            $disc_value = $promo_data['discount'];
                            $disc_price = round(($product->price * $promo_data['discount']) / 100);
                        } else {
                            $disc_value = NULL;
                            $disc_price = $promo_data['discount'];
                        }
                        $end_price= $product->price - $disc_price;
                        $today = date('Y-m-d');
                        $today=date('Y-m-d', strtotime($today));
                        $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                        $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
            
                        if (($today >= $DateBegin) && ($today <= $DateEnd)){
                            $price = $end_price;
                            $inPromo = true;
                        } else {
                            $price = $product->price;
                            $inPromo = false;
                        }
                    } else {
                        $price = $product->price;
                        $inPromo = false;
                    }

                    $rowid = md5($id . ($product_option ? $product_option : 0));
                    $item = $this->cart->get_item($rowid);
                    $merchant = $this->session->userdata('list_merchant')[0];
                    //$price = $product->price;
                    //jika produk tanaka
                    if ($product->merchant == 0) {
                        $price_level = $this->product->price_in_level($product->id, $merchant['group'], ($item ? $item['qty'] + $qty : $qty));
                        if ($price_level) { //jika price level ada, maka produk tsb masuk ke harga grosir
                            $price = $price_level;
                            $price_level = 1;
                        } else {
                            $price_group = $this->main->get('product_price', array('product' => $product->id, 'merchant_group' => $merchant['group']));
                            if ($price_group) {
                                $old_price = $price_group->price;
                                if($product->promo == 1){
                                    if($promo_data['type'] == 'P') {
                                        $disc_price_group = round(($price_group->price * $promo_data['discount']) / 100);
                                    } else {
                                        $disc_price_group = $promo_data['discount'];
                                    }
                                    $end_price_group= $price_group->price - $disc_price_group;
                        
                                    if (($today >= $DateBegin) && ($today <= $DateEnd)){
                                        $price = $end_price_group;
                                    } else {
                                        $price = $price_group->price;
                                    }
                                } else {
                                    $price = $price_group->price;
                                }
                            }
                        }
                    } else {
                        $price_level = 0;
                    }
                    $price += ($product_option) ? $option->price : 0;
                    $weight = $product->weight + ($product_option ? $option->weight : 0);
                    if ($item) {
                        $item['qty'] += $qty;
                        $item['price'] = $price;
                        $item['price_level'] = $price_level;
                        $item['weight'] = $weight * $item['qty'];
                        $this->cart->update($item);
                    } else {
                        $item = array(
                            'id' => $id,
                            'name' => $product->name,
                            'type' => $product->type,
                            'image' => $product->image,
                            'code' => $product->code,
                            'category' => $product->category,
                            'description' => $product->short_description,
                            'qty' => $qty,
                            'price' => $price,
                            'price_level' => $price_level,
                            'merchant' => $product->merchant,
                            'merchant_name' => $product->merchant_name,
                            'merchant_group' => ($product->merchant) ? 0 : $merchant['group'],
                            'merchant_type' => '',
                            'weight' => $weight * $qty,
                            'shipping' => '',
                            'shipping_merchant' => $product->merchant,
                            'shipping_cost' => 0,
                            'options' => ($product_option) ? $options->result_array() : array()
                        );
                        if($product->type == 'p'){
                            $item['package_items'] = json_decode($product->package_items, true);
                        } else {
                            $item['package_items'] = array();
                        }
                        if($inPromo){
                            $item['inPromo'] = true;
                            $item['promo_data'] = $promo_data;
                            $item['disc_value'] = $disc_value;
                            $item['disc_price'] = $disc_price;
                            $item['old_price'] = $old_price;
                        } else {
                            $item['inPromo'] = false;
                            $item['promo_data'] = array();
                            $item['disc_value'] = NULL;
                            $item['disc_price'] = NULL;
                            $item['old_price'] = $price;
                        }
                        $this->cart->insert($item);
                    }
                    $output['status'] = 'success';
                } while (0);

//            $product_options = $this->product->get_product_options($id);
//            if ($product_options) {
//                foreach ($product_options->result() as $po) {
//                    if ($po->required && empty($options[$po->id])) {
//                        $output['error']['option'][] = (int) $po->id;
//                    }
//                }
//            }
//            if (!isset($output['error'])) {
//                $item_options = array();
//                if ($options) {
//                    foreach ($options as $option => $option_variant) {
//                        $option = $this->product->get_product_option_variant_full($option, $option_variant);
//                        if ($option) {
//                            $price_option += $option['price'];
//                            $weight_option += $option['weight'];
//                        }
//                        array_push($item_options, $option);
//                    }
//                }
//                $merchant = $this->session->userdata('list_merchant')[0];
//
//                $price = $price + $price_option;
//                $weight = $product->weight + $weight_option;
//            }
            }
        }
        echo json_encode($output);
    }

    public function update($rowid, $qty) {
        if ($qty > 0) {
            $item = $this->cart->get_item($rowid);
            $product = $this->product->get_product($item['id']);
            if ($product) {
                $old_price = $product->price;
                if($product->promo == 1){
                    $promo_data = json_decode($product->promo_data,true);
                    if($promo_data['type'] == 'P') {
                        $disc_value = $promo_data['discount'];
                        $disc_price = round(($product->price * $promo_data['discount']) / 100);
                    } else {
                        $disc_value = NULL;
                        $disc_price = $promo_data['discount'];
                    }
                    $end_price= $product->price - $disc_price;
                    $today = date('Y-m-d');
                    $today=date('Y-m-d', strtotime($today));
                    $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                    $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
        
                    if (($today >= $DateBegin) && ($today <= $DateEnd)){
                        $price = $end_price;
                        $inPromo = true;
                    } else {
                        $price = $product->price;
                        $inPromo = false;
                    }
                } else {
                    $price = $product->price;
                    $inPromo = false;
                }
                $item['price'] = $price;
                $item['qty'] = $qty;
                $item['rowid'] = $rowid;
                if ($product->merchant == 0) {
                    $merchant = $this->session->userdata('list_merchant')[0];
                    $price_level = $this->product->price_in_level($product->id, $merchant['group'], $qty);
                    if ($price_level) { //jika price level ada, maka produk tsb masuk ke harga grosir
                        $item['price'] = $price_level;
                        $item['price_level'] = 1;
                    } else {
                        $price_group = $this->main->get('product_price', array('product' => $product->id, 'merchant_group' => $merchant['group']));
                        if ($price_group) {
                            $old_price = $price_group->price;
                            if($product->promo == 1){
                                if($promo_data['type'] == 'P') {
                                    $disc_price_group = round(($price_group->price * $promo_data['discount']) / 100);
                                } else {
                                    $disc_price_group = $promo_data['discount'];
                                }
                                $end_price_group= $price_group->price - $disc_price_group;
                    
                                if (($today >= $DateBegin) && ($today <= $DateEnd)){
                                    $price = $end_price_group;
                                } else {
                                    $price = $price_group->price;
                                }
                            } else {
                                $price = $price_group->price;
                            }
                            $item['price'] = $price;
                        }
                    }
                }
                if ($item['options']) {
                    foreach ($item['options'] as $option) {
                        $item['price'] += $option['price'];
                    }
                }
                $this->cart->update($item);
            }
        }
        redirect('cart');
    }

    public function delete($rowid) {
        $this->cart->remove($rowid);
        redirect('cart');
    }

    public function destroy() {
        $this->cart->destroy();
        redirect('cart');
    }

}
