<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout_reward extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in())
        //redirect('member/login?back=' . uri_string());
        redirect('auth/guest?back=' . uri_string());
        $this->load->library('form_validation');
        $this->load->library('rajaongkir');
        $this->lang->load('checkout', settings('language'));
//        $this->load->model('catalog/product_model', 'product');
        $this->load->model('cart_model');
        $this->load->model('checkout_model', 'checkout');
        $this->load->model('Model');
//        $this->load->model('payment_methods');
    }

    public function index() {
        //redirect('checkout_reward/address');
        $param_redirect = $this->Model->cek_cart_detail($this->session->userdata('email'));
        if($param_redirect == ''){
            redirect(base_url('member/point'));
        }else if($param_redirect == '0'){
            redirect(base_url('checkout/cart_point'));
        }else if($param_redirect == '2'){
            redirect(base_url('checkout/checkout_reward/finish'));
        }
        $this->address();
    }

    public function address() {

        $this->form_validation->set_rules('address', 'Alamat', 'trim|required', array('required' => 'Alamat harus dipilih'));

        if ($this->form_validation->run() == true) {
            $data = $this->input->post(null, true);
            $this->session->set_userdata('checkout_address', $data['address']);
            redirect('checkout/shipping');
        } else {
            $this->data['messages'] = validation_errors();
        }
        $this->data['provincies'] = $this->main->gets('provincies', array(), 'name ASC');
        $this->data['list_address'] = $this->checkout->customer_address($this->data['user']->id);
        $this->data['shippings'] = array();
        $this->data['cities'] = array();
        $this->data['m'] = $this->Model;

        $this->output->set_title(lang('checkout_address_text') . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->js('assets/frontend/js/modules/checkout_reward.js');
        $this->load->view('address_reward', $this->data);
    }

    public function back(){
        $m = $this->Model;

        $q_cart = $m->get_data('', 'point_reward_cart', null, array('id_customer' => $this->session->userdata('user_id')));
        if($q_cart->num_rows() > 0){
            $m->update_data('point_reward_cart_detail', array('status' => 0), array('id_cart' => $q_cart->row()->id));
        }

        die(json_encode(array('success' => 1)));
    }

    public function save(){
        $m = $this->Model;
        $selected_address = $this->input->post('selected_address');

        $q_customer = $m->get_data('', 'customers', null, array('email' => $this->session->userdata('email')));

        $q_reward_point = $m->get_data('prcd.id_reward, prcd.id, prcd.point, prcd.qty, prcd.id_address', 'point_reward_cart_detail prcd', array(0 => 'point_reward_cart prc-prc.id=prcd.id_cart'), array('prc.id_customer' => $q_customer->row()->id));

        //cek periode
        if(!$m->cek_periode_tukar_point()){
            $m->delete_data1('point_reward_cart_detail', array('id' => $q_reward_point->row()->id));
            die(json_encode(array('success' => '0', 'msg' => 'Periode Tukar Point Belum Dimulai')));
        }

        //cek point cust tidak perlu

        //cek stok
        if(!$m->cek_stok_reward($q_reward_point->row()->id_reward)){
            $m->delete_data1('point_reward_cart_detail', array('id' => $q_reward_point->row()->id));
            die(json_encode(array('success' => '0', 'msg' => 'Reward yang Ingin Anda Tukar Telah Habis')));
        }
        //update status cart detail jadi 2
        $m->update_data('point_reward_cart_detail', array('status' => '2', 'id_address' => $selected_address), array('id' => $q_reward_point->row()->id));

        //0 kan point customer
        $m->update_data('point', array('point' => 0, 'last_update' => date('Y/m/d H:i:s')), array('id_customer' => $q_customer->row()->id));

        $reward = $m->get_data('', 'reward', null, array('id' => $q_reward_point->row()->id_reward));
        //kurangi qty reward
        $m->update_data('reward', array('qty' => ($reward->row()->qty - 1)), array('id' => $q_reward_point->row()->id_reward));

        $q_reward_claim = $m->get_data('', 'reward_claim_temp', null, array('id_reward' => $q_reward_point->row()->id_reward, 'status_periode_aktif' => 1));

        $q_setting = $m->get_data('', 'settings', null, array('key' => 'periode_tukar_point'))->row()->value;
        $exp = explode(';', $q_setting);
        $periode_tukar_point = $exp[0].';'.$exp[1].';'.$exp[2].';'.$exp[3].';'.$exp[4].';'.$exp[5];

        //insert/update reward_claim_temp
        if($q_reward_claim->num_rows() > 0){
            if($q_reward_claim->row()->id_customer != ''){
                $add_['id_customer'] = $q_reward_claim->row()->id_customer.','.$q_customer->row()->id.',';
            }else{
                $add_['id_customer'] = ','.$q_customer->row()->id.',';
            }
            $m->update_data('reward_claim_temp', $add_, array('id_reward' => $q_reward_point->row()->id_reward, 'status_periode_aktif' => 1));

            $id_reward_claim_ = $q_reward_claim->row()->id;
        }else{
            $m->insert_data('reward_claim_temp', array('id_reward' => $q_reward_point->row()->id_reward, 'id_customer' => ','.$q_customer->row()->id.',', 'id_merchant' => '', 'periode' => $periode_tukar_point, 'status_periode_aktif' => 1));

            $id_reward_claim_ = $m->get_data('', 'reward_claim_temp', null ,array('id_reward' => $q_reward_point->row()->id_reward, 'id_customer' => ','.$q_customer->row()->id.',', 'periode' => $periode_tukar_point))->row()->id;
        }

        $data = array(
            'id_merchant' => 0,
            'id_customer' => $q_customer->row()->id,
            'id_reward' => $q_reward_point->row()->id_reward,
            'last_update' => date('Y/m/d H:i:s'),
            'status_pengiriman' => 0,
            'id_reward_claim' => $id_reward_claim_,
            'kode' => $m->get_id('peh'),
            'id_address' => $selected_address
        );
        //insert point_exchange_history
        $m->insert_data('point_exchange_history', $data);

        //daftarkan customer ke coupon
        if($reward->row()->id_coupon != 0){
            $q_coupon = $m->get_data('', 'coupons', null, array('id' => $reward->row()->id_coupon));
            if($q_coupon->num_rows() > 0){
                if($q_coupon->row()->id_customer == ''){
                    $id_cust = ','.$this->session->userdata('user_id').',';
                }else{
                    $id_cust = $q_coupon->row()->id_customer.','.$this->session->userdata('user_id').',';
                }
                $m->update_data('coupons', array('id_customer' => $id_cust), array('id' => $q_coupon->row()->id));
            }
        }
        die(json_encode(array('success' => '1')));
    }

    public function finish() {
        $param_redirect = $this->Model->cek_cart_detail($this->session->userdata('email'));
        if($param_redirect == ''){
            redirect(base_url('member/point'));
        }else if($param_redirect == '0'){
            redirect(base_url('checkout/cart_point'));
        }else if($param_redirect == '1'){
            redirect(base_url('checkout/checkout_reward/address'));
        }
        //$this->data['order'] = $order;
        // $this->data['invoices'] = $this->main->gets('order_invoice', array('order' => $this->data['order']->id));
        // $this->data['customer'] = $this->main->get('customers', array('id' => $this->data['order']->customer));
        $this->data['customer'] = $this->Model->get_data('', 'customers', null, array('id' => $this->session->userdata('user_id')))->row();
        $this->output->set_title(lang('checkout_finish_text') . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->js('assets/frontend/js/modules/checkout_finish.js');
        $this->load->view('finish_reward', $this->data);
    }

    public function getCities($province) {
        $cities = $this->main->gets('cities', array('province' => $province), 'name ASC');
        $output = '<option value="">Pilih kota</option>';
        if ($cities) {
            foreach ($cities->result() as $city) {
                $output .= '<option value="' . $city->id . '">' . $city->type . ' ' . $city->name . '</option>';
            }
        }
        echo $output;
    }

    public function getDistricts($city) {
        $districts = $this->main->gets('districts', array('city' => $city), 'name ASC');
        $output = '<option value="">Pilih kecamatan</option>';
        if ($districts) {
            foreach ($districts->result() as $district) {
                $output .= '<option value="' . $district->id . '">' . $district->name . '</option>';
            }
        }
        echo $output;
    }

    public function getShippings() {
        $data = $this->input->post(null, TRUE);
//        $couriers = json_decode($data['courier']);
        $output = '';
        foreach ($data['courier'] as $courier) {
            if($courier == 'pickup') {
                $output .= '<li>
                <input id="' . $data['id'] . '-pickup" name="shipping[' . $data['id'] . '][service]" type="radio" value="pickup" class="shipping" required="" data-id="' . $data['id'] . '" data-value="0">
                <label for="' . $data['id'] . '-pickup">Ambil Sendiri <span style="color:red;font-size:10px">* Tidak support kredivo</span></label>
                </li>';
            } else {
                $services = $this->rajaongkir->cost($data['from'], 'subdistrict', $data['to'], 'subdistrict', $data['weight'], $courier);
                if ($services) {
                    $services = json_decode($services);
                    foreach ($services->rajaongkir->results as $service) {
                        foreach ($service->costs as $cost) {
                            if($cost->service == "CTC"){
                                $label_service = "REG";
                            } else if ($cost->service == "CTCYES"){
                                $label_service = "YES";
                            } else {
                                $label_service = $cost->service;
                            }
                            $output .= '<li>';
                            $output .= '<input name="shipping[' . $data['id'] . '][service]" type="radio" value="' . $service->code . '-' . $cost->service . '" class="shipping" id="' . $data['id'] . $service->code . '-' . $cost->service . '" required="" data-id="' . $data['id'] . '" data-value="' . $cost->cost[0]->value . '">';
                            $output .= '<label for="' . $data['id'] . $service->code . '-' . $cost->service . '">' . strtoupper($service->code) . ' ' . $label_service . ' <span>(' . rupiah($cost->cost[0]->value) . ')</span></label>';
                            $output .= '</li>';
                        }
                    }
                }
            }
        }
        echo $output;
    }

    public function add_address() {
        $this->form_validation->set_rules('title', 'Nama Alamat', 'trim|required');
        $this->form_validation->set_rules('name', 'lang:checkout_shipping_field_name', 'trim|required');
        $this->form_validation->set_rules('phone', 'lang:checkout_shipping_field_phone', 'trim|required');
        $this->form_validation->set_rules('address', 'lang:checkout_shipping_field_address', 'trim|required');
        $this->form_validation->set_rules('province', 'lang:checkout_shipping_field_province', 'required');
        $this->form_validation->set_rules('city', 'lang:checkout_shipping_field_city', 'required');
        $this->form_validation->set_rules('district', 'lang:checkout_shipping_field_district', 'required');
        $this->form_validation->set_rules('postcode', 'lang:checkout_shipping_field_postcode', 'trim|required');

        $return['status'] = 'error';
        if ($this->form_validation->run() == true) {
            $data = $this->input->post(null, true);
            $data['customer'] = $this->data['user']->id;
            $address = $this->main->insert('customer_address', $data);
            $province = $this->main->get('provincies', array('id' => $data['province']));
            $city = $this->main->get('cities', array('id' => $data['city']));
            $district = $this->main->get('districts', array('id' => $data['district']));
            $return['status'] = 'success';
            $return['html'] = '<div class="payment-method-block">
                               <div class="payment-method-detail">
                               <input type="radio" value="' . $address . '" onclick="selectPayment(this);" name="address" required id="address-' . $address . '">
                               <label for="address-' . $address . '">
                               <div class="payment-method-title">' . $data['name'] . ' - ' . $data['title'] . '</div>
                               <div class="payment-method-subtitle">' . $data['address'] . '<br>' . $district->name . ', ' . $city->name . ', ' . $data['postcode'] . '<br>' . $province->name . '</div>
                               </label>
                               </div>
                               </div>';
        } else {
            $return['message'] = '<div class="alert alert-danger fade in">' . validation_errors() . '</div>';
        }

        echo json_encode($return);
    }

    public function add_coupon() {
        $this->form_validation->set_rules('couponCode', 'Kode Kupon', 'trim|required');
        $return['status'] = 'error';
        $return['data'] = NULL;
        if ($this->form_validation->run() == true) {
            $coupon = $this->main->get('coupons', array('status' => 1,'code' => $this->input->post('couponCode')));
            if($coupon) {
                $today = strtotime(date("Y-m-d H:i:s"));
                $couponStart= strtotime($coupon->date_start);
                $couponEnd = strtotime($coupon->date_end);
                $sess_user = $this->session->userdata('user');
                $this->db->where('customer',$sess_user->user_id);
                $this->db->where('coupon',$coupon->id);
                $countCc = $this->db->get('coupon_history');
                $this->db->where('coupon',$coupon->id);
                $countC = $this->db->get('coupon_history');
                if($coupon->type =='P') {
                    $coupon->total_disc = $this->cart->total() * ($coupon->discount/100);
                } else {
                    $coupon->total_disc = $coupon->discount;
                }
                $list_shipping = array();
                $list_categories_invalid = array();
                $list_zona_invalid = array();
                $invalid_type = array();
                $branch_type = array();
                $merchant_type = array();
                $exp_ids = array();
                $exp_idsx = array();
                if($coupon->catIds){
                    $exp_ids = explode(',', $coupon->catIds);
                }
                if($coupon->zonaIds){
                    $exp_idsx = explode(',', $coupon->zonaIds);
                }
                foreach ($this->cart->contents() as $key => $item) {
                    array_push($list_shipping,$item['shipping']);
                    if (!in_array($item['category'], $exp_ids)){
                         array_push($list_categories_invalid,$item['category']);
                    }
                    if (!in_array($item['merchant_group'], $exp_idsx)){
                        array_push($list_zona_invalid,$item['merchant_group']);
                    }
                    if($item['merchant_type'] == 'branch') {
                        array_push($branch_type,$item['merchant_type']);
                    } else {
                        array_push($merchant_type,$item['merchant_type']);
                    }
                }
                
                if($coupon->coupon_type == 'branch'){
                    $invalid_type = $merchant_type;
                } else if($coupon->coupon_type == 'merchant'){
                    $invalid_type = $branch_type;
                }
                //var_dump($this->cart->contents());exit();
                $counts = array_count_values($list_shipping);
                $count_total_shipping = 0;
                $this->data['pickup_count'] = isset($counts['pickup']) ? $counts['pickup'] : 0;
                $shipping_group= array();
                foreach ($this->cart->contents() as $key => $item) {
                    $id = $item['shipping_merchant'] . '-' . $item['shipping'];
                    $shipping_group[$id]['merchant_id'] = $item['shipping_merchant'];
                    $shipping_group[$id]['shipping_type'] = $item['shipping'];
                    $shipping_group[$id]['shipping_cost'] = $item['shipping_cost'];
                    $shipping_group[$id]['weight'] = isset($shipping_group[$id]['weight']) ? ($shipping_group[$id]['weight'] + $item['weight']) / 1000 : $item['weight'] / 1000;
                    $shipping_group[$id]['shipping_total'] = $shipping_group[$id]['shipping_cost'];
                }
                foreach ($shipping_group as $key => $item){
                    $count_total_shipping = $count_total_shipping + $item['shipping_total'];
                }
                $coupon->total_shopping = $this->cart->total() + $count_total_shipping - ($this->session->userdata('coupon')?$coupon->total_disc:0);
                if(count($list_categories_invalid) > 0){
                    $return['message'] = '<div class="alert alert-danger fade in"> Kupon tidak berlaku !</div>';
                }
                if($today <= $couponStart || $today >= $couponEnd){
                    $return['message'] = '<div class="alert alert-danger fade in"> Kupon sudah tidak berlaku !</div>';
                } else if($this->cart->total() < $coupon->min_order){
                    $return['message'] = '<div class="alert alert-danger fade in">Minimal transaksi dibawah '.$coupon->min_order.'!</div>';
                } else if($coupon->uses_customer <= $countCc->num_rows() && $coupon->uses_customer != 0){
                    $return['message'] = '<div class="alert alert-danger fade in">Kupon sudah pernah digunakan !</div>';
                } else if($coupon->uses_total <= $countC->num_rows() && $coupon->uses_total != 0){
                    $return['message'] = '<div class="alert alert-danger fade in">Kupon sudah tidak berlaku !</div>';
                } else if(count($list_categories_invalid) > 0){
                    $return['message'] = '<div class="alert alert-danger fade in"> Kupon tidak berlaku di kategori ini!</div>';
                } else if(count($list_zona_invalid) > 0){
                    $return['message'] = '<div class="alert alert-danger fade in"> Kupon tidak berlaku di zona ini!</div>';
                } else if(count($invalid_type) > 0){
                    $return['message'] = '<div class="alert alert-danger fade in"> Kupon tipe salah!</div>';
                }
                else {
                    $this->session->set_userdata('coupon', $coupon);
                    $return['message'] = '<div class="alert alert-success fade in">Kode Kupon valid!</div>';
                    $return['data'] = $coupon;
                    $return['status'] = 'success';
                }
            } else {
                $return['message'] = '<div class="alert alert-danger fade in">Kode Kupon invalid!</div>';
            }
        } else {
            $return['message'] = '<div class="alert alert-danger fade in"> Kode Kupon tidak boleh kosong</div>';
        }
        echo json_encode($return);
    }

    public function reset_coupon() {
        $this->session->unset_userdata('coupon');
        $list_shipping = array();
        foreach ($this->cart->contents() as $key => $item) {
            array_push($list_shipping,$item['shipping']);
        }
        //var_dump($this->cart->contents());
        $counts = array_count_values($list_shipping);
        $count_total_shipping = 0;
        $this->data['pickup_count'] = isset($counts['pickup']) ? $counts['pickup'] : 0;
        $shipping_group= array();
        foreach ($this->cart->contents() as $key => $item) {
            $id = $item['shipping_merchant'] . '-' . $item['shipping'];
            $shipping_group[$id]['merchant_id'] = $item['shipping_merchant'];
            $shipping_group[$id]['shipping_type'] = $item['shipping'];
            $shipping_group[$id]['shipping_cost'] = $item['shipping_cost'];
            $shipping_group[$id]['weight'] = isset($shipping_group[$id]['weight']) ? ($shipping_group[$id]['weight'] + $item['weight']) / 1000 : $item['weight'] / 1000;
            $shipping_group[$id]['shipping_total'] = $shipping_group[$id]['shipping_cost'];
        }
        foreach ($shipping_group as $key => $item){
            $count_total_shipping = $count_total_shipping + $item['shipping_total'];
        }
        $return['totalShopping']= $this->cart->total() + $count_total_shipping;
        $return['status'] = 'success';
        $return['message'] = 'Sukses menghapus kupon';
        echo json_encode($return);
    }
//    public function email($id) {
//        $this->data['data'] = $this->main->get('order_payment', array('code' => $id));
//        $this->data['orders'] = $this->main->gets('orders', array('payment' => $this->data['data']->id));
//        $this->load->view('email/payment_order', $this->data);
//    }
    private function _payment_email($id) {
        $this->data['data'] = $this->main->get('order_payment', array('id' => $id));
        $this->data['orders'] = $this->main->gets('orders', array('payment' => $this->data['data']->id));
        $message = $this->load->view('email/payment_order', $this->data, TRUE);
        if (!send_mail('noreply@tokomanamana.com', $this->data['user']->email, 'Menunggu Pembayaran Transfer Bank untuk pembayaran ' . $this->data['data']->code, $message)) {
            log_message('error', 'send email failed');
        }
    }

    private function _invoice_email($id) {
//    public function invoice_email($id) {
        $this->load->library('email');
        $config['mailtype'] = 'html';
        $config['wordwrap'] = FALSE;
        $config['priority'] = 1;
        $config['protocol'] = 'sendmail';
        $this->email->initialize($config);
//        $this->data['order'] = $this->checkout->get_order_by_id($id);
//        $data['address'] = $this->main->get('address', array('auth' => $this->data['user']->id, 'primary' => 1));
//        $data['customer'] = $this->checkout->getProfile($data['order']->customer);
//        $this->data['products'] = $this->checkout->get_order_products($id);
        $this->data['order'] = $this->main->get('orders', array('id' => $id));
        $this->data['customer'] = $this->main->get('customers', array('id' => $this->data['order']->customer));
        $this->data['invoices'] = $this->main->gets('order_invoice', array('order' => $this->data['order']->id));
        $message = $this->load->view('email/invoice', $this->data, TRUE);
        // send_mail('noreply@tokomanamana.com', $this->data['user']->email, 'Menunggu Pembayaran Invoice ' . $this->data['order']->code, $message);
        $this->email->from('noreply@tokomanamana.com', 'TokoManaMana.com');
        $this->email->to($this->data['user']->email);
        $this->email->subject('Menunggu Pembayaran Invoice ' . $this->data['order']->code);
        $this->email->message($message);
        $this->email->send();
    }

    public function info_store($id) {
        $merchant = $this->checkout->get_merchant($id);
        $output = '<dl class="dl-horizontal">
                <p class="lead topbottom">' . $merchant->name . '</p>
                <hr class="topbottom">';
        if ($merchant->username) {
            $output .= '<dt class="listHeading">ID:</dt>
                <dd class="listText">' . $merchant->username . '</dd>';
        }
        $output .= '<dt class="listHeading">Kota:</dt>
                <dd class="listText">' . $merchant->city . '</dd>';
        $output .= '<dt class="listHeading">Kecamatan:</dt>
                <dd class="listText">' . $merchant->district . '</dd>';
        if ($merchant->distance) {
            $output .= '<dt class="listHeading">Jarak:</dt>
                <dd class="listText">' . number($merchant->distance) . ' km</dd>';
        }
        $merchant->rating = round($merchant->rating);
        $rating = '<span class="spr-starrating spr-badge-starrating">';
        for ($i = 1; $i <= $merchant->rating; $i++) {
            $rating .= '<i class="spr-icon spr-icon-star"></i>';
        }
        for ($i = $merchant->rating + 1; $i <= 5; $i++) {
            $rating .= '<i class="spr-icon spr-icon-star-empty"></i>';
        }
        $rating .= '</span>';
        $output .= '<dt class="listHeading">Rating:</dt>
                <dd class="listText">' . $rating . '</dd>
            </dl>';
        echo $output;
    }
    public function check_stock() {
        $this->load->model('checkout_model', 'checkout');
        // $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        if (!$this->cart->contents()){
            $return = array('message' => 'Tidak ada barang di dalam cart! ', 'status' => 'failed');
        } else {
            $not_avail = array();
            $merchant_stock = array();
            $count_valid = 0;
            foreach ($this->cart->contents() as $item) {
                if($item['type'] == 'p') {
                    $package_items = $item['package_items'];
                    $count_packages = count($package_items);
                    foreach ($package_items as $package) {
                        $merchants = $this->checkout->get_merchants($package['product_id'], $item['qty'] * $package['qty'], $item['price_level']);
                        if ($merchants->num_rows() > 0) {
                            foreach ($merchants->result() as $merchantx) {
                                //var_dump($merchant);
                                $merchant_stock[$merchantx->id][] = array('prod_id' => $package['product_id']);
                            }
                        }  
                    }
                    //exit();
                    if (count($merchant_stock) > 0) {
                        foreach ($merchant_stock as $mcn) {
                            if(count($mcn) == $count_packages) {
                                $count_valid++;
                            }
                        }
                    } 
                    if ($count_valid > 0){

                    } else {
                        $merchant_stock = array();
                        $list_merchant = $this->session->userdata('list_merchant')[0];
                        $merchant = $this->checkout->get_branch($list_merchant['group']);
                        foreach ($package_items as $package) {
                            $stock = $this->checkout->check_branch_stock($merchant ? $merchant->id : 0,$package['product_id'], $item['qty'] * $package['qty'], $item['price_level']); 
                            if ($stock->num_rows() > 0) {
                                foreach ($stock->result() as $merchantx) {
                                    $merchant_stock[$merchantx->id][] = array('prod_id' => $package['product_id']);
                                }
                            }  
                        }
                        if (count($merchant_stock) > 0) {
                            //foreach ($merchant_stock as $mcn) {
                                if(count($merchant_stock) == $count_packages) {
                                    $count_valid++;
                                }
                            //}
                        }
                        if ($count_valid == 0){
                            $not_avail[] = $item;
                        }
                    }
                } else {
                    $merchants = $this->checkout->get_merchants($item['id'], $item['qty'], $item['price_level']);
                    if ($merchants->num_rows() > 0) {
                        //$return = array('message' => 'Stok barang tersedia! ', 'status' => 'success'); 
                    } else { 
                        $list_merchant = $this->session->userdata('list_merchant')[0];
                        $merchant = $this->checkout->get_branch($list_merchant['group']);
                        $stock = $this->checkout->check_branch_stock($merchant ? $merchant->id : 0,$item['id'], $item['qty'], $item['price_level']);
                        if($stock->num_rows() == 0){
                            $not_avail[] = $item;
                        }
                    }
                }
                if(count($not_avail) > 0) {
                    $return = array('message' => 'Stok barang tidak tersedia! ', 'status' => 'failed','data' => $not_avail);
                } else {
                    $return = array('message' => 'Stok barang tersedia! ', 'status' => 'success');
                }
                
            }
            echo json_encode($return);
        }
        
    }
}
