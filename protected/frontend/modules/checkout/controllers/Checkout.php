<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in())
        //redirect('member/login?back=' . uri_string());
        redirect('auth/guest?back=' . uri_string());
        $this->load->library('form_validation');
        $this->load->library('rajaongkir');
        $this->lang->load('checkout', settings('language'));
//        $this->load->model('catalog/product_model', 'product');
        $this->load->model('cart_model');
        $this->load->model('checkout_model', 'checkout');
//        $this->load->model('payment_methods');
    }

    public function index() {
        redirect('checkout/address');
    }

    // public function address() {
    //     if (!$this->cart->contents())
    //         redirect('cart');

    //     $this->form_validation->set_rules('address', 'Alamat', 'trim|required', array('required' => 'Alamat harus dipilih'));

    //     if ($this->form_validation->run() == true) {
    //         $data = $this->input->post(null, true);
    //         $this->session->set_userdata('checkout_address', $data['address']);
    //         redirect('checkout/shipping');
    //     } else {
    //         $this->data['messages'] = validation_errors();
    //     }
    //     $this->data['provincies'] = $this->main->gets('provincies', array(), 'name ASC');
    //     $this->data['list_address'] = $this->checkout->customer_address($this->data['user']->id);
    //     $this->data['shippings'] = array();
    //     $this->data['cities'] = array();

    //     $this->output->set_title(lang('checkout_address_text') . ' - ' . settings('meta_title'));
    //     $this->template->_init();
    //     $this->load->js('assets/frontend/js/modules/checkout.js');
    //     $this->load->view('address', $this->data);
    // }

    public function address() {
        if (!$this->ion_auth->logged_in())
            redirect('auth/guest?back=checkout');

        // if (!$this->cart->contents())
        //     redirect('cart');

        if(!$this->cart_model->get_all_cart($this->data['user']->id, $this->input->ip_address())->result()) {
            redirect('cart');
        }

        $cek_verif_phone = $this->main->get('customers', array('id' => $this->data['user']->id));
        if($cek_verif_phone->verification_phone == 0 || $cek_verif_phone->phone == ''){
            redirect('cart');
        }

        $check_address = $this->main->get('customer_address', ['customer' => $this->data['user']->id]);
        if(!$check_address) {
            redirect('cart');
        }

        $this->form_validation->set_rules('address', 'Alamat', 'trim|required', array('required' => 'Alamat harus dipilih'));
        $this->form_validation->set_rules('shipping[]', 'lang:checkout_shipping_field_shipping', 'required');

        if ($this->form_validation->run() == true) {
            $data = $this->input->post(null, true);
            $this->session->set_userdata('checkout_address', $data['address']);
            // foreach ($data['shipping'] as $key => $shipping) {
            //     $x = $this->main->get('merchants',array('id'=>$shipping['merchant']));
            //     $cost = $data['shipping_cost'][$key];
            //     $item = array(
            //         'rowid' => $key,
            //         'shipping_merchant' => $shipping['merchant'],
            //         'shipping' => $shipping['service'],
            //         'shipping_cost' => $cost,
            //         'merchant_type' => $x->type
            //     );
            //     $this->cart->update($item);
            // }
            foreach($data['shipping'] as $key => $shipping) {
                $cart_by_merchant = $this->main->get('cart_merchant', ['id' => decode($key)]);
                $total_weight = $cart_by_merchant->total_weight;
                $get_merchant = $this->main->get('merchants', ['id' => decode($shipping['merchant'])]);
                $customer_address = $this->main->get('customer_address', ['id' => $data['address']]);
                $courier = explode('-', $shipping['service']);
                $exp = '';
                $servis = '';
                if($shipping['service'] != 'pickup') {
                    $servis = $courier[1];
                    $exp = $courier[0];
                }
                $total_cost = 0;
                $services = $this->rajaongkir->cost($get_merchant->district, 'subdistrict', $customer_address->district, 'subdistrict', $total_weight, $exp);
                if ($services) {
                    $services = json_decode($services);
                    if ($services->rajaongkir->status->code == 200) {
                        foreach ($services->rajaongkir->results as $service) {
                            foreach ($service->costs as $cost) {
                                if($cost->service == $servis){
                                    if (strpos($service->code, 'pos') !== false) {
                                        $cost->cost[0]->value = $cost->cost[0]->value/100 * 10 + $cost->cost[0]->value;
                                    }
                                    $total_cost += $cost->cost[0]->value;
                                }
                            }
                        }
                    }
                } else {
                    $total_cost += 0;
                }
                // $cost = $data['shipping_cost'][$key];
                $item = [
                    'shipping_courier' => $shipping['service'],
                    'shipping_cost' => $total_cost
                ];
                $this->db->where('id', decode($key));
                $this->db->update('cart_merchant', $item);
            };
            redirect('checkout/payment');
        } else {
            $this->data['messages'] = validation_errors();
        }
        $this->data['carts'] = $this->cart_model->get_all_cart($this->data['user']->id)->result();
        $grand_total = 0;
        foreach($this->data['carts'] as $cart) {
            $grand_total += $cart->grand_total;
        }
        $this->data['grand_total'] = $grand_total;
        $this->data['cart_merchant'] = $this->cart_model->cart_merchant($this->data['user']->id)->result();
        $this->data['provincies'] = $this->main->gets('provincies', array(), 'name ASC');
        $this->data['list_address'] = $this->checkout->customer_address($this->data['user']->id);
        $this->data['primary_address'] = $this->checkout->customer_address_primary($this->data['user']->id);
        $this->data['none_address'] = $this->checkout->customer_address_none($this->data['user']->id);
        if($this->data['primary_address']){
            $this->session->set_userdata('checkout_address', $this->data['primary_address']);
        } else {
            $this->session->set_userdata('checkout_address', $this->data['none_address']);
        }
        $this->session->unset_userdata('checkout_address');
        $this->session->unset_userdata('coupon');
        $this->data['shippings'] = array();
        $this->data['cities'] = array();
        $this->output->set_title(lang('checkout_address_text') . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->js('assets/frontend/js/modules/checkout.js');
        $this->load->js('assets/frontend_1/js/jquery-ui.min.js');
        $this->load->js('assets/frontend/js/sweetalert2.all.min.js');
        $this->load->view('address', $this->data);
    }

    // public function shipping() {
    //     if (!$this->cart->contents())
    //         redirect('cart');
    //     if (!$this->session->userdata('checkout_address'))
    //         redirect('checkout/address');

    //     $this->form_validation->set_rules('shipping[]', 'lang:checkout_shipping_field_shipping', 'required');

    //     if ($this->form_validation->run() == true) {
    //         $data = $this->input->post(null, true);
    //         foreach ($data['shipping'] as $key => $shipping) {
    //             $x = $this->main->get('merchants',array('id'=>$shipping['merchant']));
    //             $cost = $data['shipping_cost'][$key];
    //             $item = array(
    //                 'rowid' => $key,
    //                 'shipping_merchant' => $shipping['merchant'],
    //                 'shipping' => $shipping['service'],
    //                 'shipping_cost' => $cost,
    //                 'merchant_type' => $x->type
    //             );
    //             $this->cart->update($item);
    //         }
    //         redirect('checkout/payment');
    //     } else {
    //         $this->data['messages'] = validation_errors();
    //         $this->data['address'] = $this->main->get('customer_address', array('id' => $this->session->userdata('checkout_address'), 'customer' => $this->data['user']->id));
    //         $this->output->set_title(lang('checkout_shipping_text') . ' - ' . settings('meta_title'));
    //         $this->template->_init();
    //         $this->load->js('assets/frontend/js/modules/checkout.js');
    //         $this->load->view('shipping', $this->data);
    //     }
    // }

    public function payment() {
        if(!$this->cart_model->get_all_cart($this->data['user']->id, $this->input->ip_address())->result()) {
            redirect('cart');
        }
        if (!$this->session->userdata('checkout_address')){
            redirect('checkout/address');
        }
        $all_cart = $this->cart_model->get_all_cart($this->data['user']->id);
        $cart_merchant = $this->cart_model->cart_merchant($this->data['user']->id);

        $list_shipping = array();
        foreach ($cart_merchant->result() as $key => $item) {
            array_push($list_shipping,$item->shipping_courier);
        }
        $coupon = $this->session->userdata('coupon');
        // $this->session->unset_userdata('coupon');
        $counts = array_count_values($list_shipping);
        $count_total_shipping = 0;
        $this->data['pickup_count'] = isset($counts['pickup']) ? $counts['pickup'] : 0;
        $this->form_validation->set_rules('payment', 'lang:checkout_shipping_field_payment', 'required');
        $shipping_group= array();
        $list_merchant = $this->session->userdata('list_merchant')[0];
        // $merchant_ = $this->checkout->get_branch($list_merchant['group']);

        foreach ($cart_merchant->result_array() as $key => $item) {
            $id = $item['merchant'] . '-' . $item['shipping_courier'];
            $shipping_group[$id]['merchant'] = $item['merchant'];
            $shipping_group[$id]['shipping_type'] = $item['shipping_courier'];
            $shipping_group[$id]['shipping_cost'] = $item['shipping_cost'];
            // $shipping_group[$id]['weight'] = isset($shipping_group[$id]['weight']) ? ($shipping_group[$id]['weight'] + $item['weight']) / 1000 : $item['weight'] / 1000;
            $shipping_group[$id]['weight'] = $item['total_weight'];
            $shipping_group[$id]['shipping_total'] = $shipping_group[$id]['shipping_cost'];
        }
        $customer_address = $this->main->get('customer_address', ['id' => $this->session->userdata('checkout_address'), 'customer' => $this->data['user']->id]);
        foreach ($shipping_group as $key => $item){
            // $courier = explode('-', $item['shipping_type']);
            // $exp = '';
            // $servis = '';
            // if($item['shipping_type'] != 'pickup') {
            //     $servis = $courier[1];
            //     $exp = $courier[0];
            // }
            // $get_merchant = $this->main->get('merchants', ['id' => $item['merchant']]);
            // $services = $this->rajaongkir->cost($get_merchant->district, 'subdistrict', $customer_address->district, 'subdistrict', $item['weight'], $exp);
            // if ($services) {
            //     $services = json_decode($services);
            //     if ($services->rajaongkir->status->code == 200) {
            //         foreach ($services->rajaongkir->results as $service) {
            //             foreach ($service->costs as $cost) {
            //                 if($cost->service == $servis){
            //                     if (strpos($service->code, 'pos') !== false) {
            //                         $cost->cost[0]->value = $cost->cost[0]->value/100 * 10 + $cost->cost[0]->value;
            //                     }
            //                     $count_total_shipping += $cost->cost[0]->value;
            //                 }
            //             }
            //         }
            //     }
            // } else {
            //     $count_total_shipping += 0;
            // }
            $count_total_shipping += $item['shipping_total'];
        }
        $this->data['count_total_shipping'] = $count_total_shipping;
        $this->session->set_userdata('shipping', $count_total_shipping);
        if ($this->form_validation->run() == true) {
            $address = $this->main->get('customer_address', array('id' => $this->session->userdata('checkout_address'), 'customer' => $this->data['user']->id));
            $payment_method = $this->input->post('payment');
            // $coupon = $this->session->userdata('coupon');
            $this->session->unset_userdata('coupon');

            $today = date("y-m-d h:i:s");
            $month = date("m",strtotime($today));
            $customer = $this->main->get('customers', ['id' => $this->data['user']->id]);

           $data_order = array(
                'code' => order_code(),
                'customer' => $this->data['user']->id,
                'payment_method' => $payment_method,
                'due_date' => date('Y-m-d H:i:s', strtotime(' +1 day')),
//                'due_date' => date('Y-m-d H:i:s', strtotime(' +2 minutes')),
                'shipping_name' => $address->name,
                'shipping_address' => $address->address,
                // 'shipping_phone' => $address->phone,
                'shipping_phone' => $customer->phone,
                'shipping_province' => $address->province,
                'shipping_city' => $address->city,
                'shipping_district' => $address->district,
                'shipping_postcode' => $address->postcode,
            );
            $order = $this->main->insert('orders', $data_order);
            $last_insert_order = $this->db->insert_id();  
            // if($coupon) {
            //     $data_coupon = array(
            //         'coupon' => $coupon->id,
            //         'order' => $this->db->insert_id(),
            //         'customer' => $this->data['user']->id,
            //         'amount' => $coupon->total_disc,
            //     );
            //     $coupon_history = $this->main->insert('coupon_history', $data_coupon);  
            // }
            $data_invoice = array(
                'customer' => $this->data['user']->id,
                'order_status' => settings('order_new_status'),
                'order' => $order
            );
            $cart = array();
            // foreach ($all_cart->result_array() as $key => $item) {
            //     $id = $item['merchant_id'] . '-' . $item['shipping_courier'];
            //     $cart[$id]['merchant'] = $item['merchant_id'];
            //     $cart[$id]['shipping'] = $item['shipping_courier'];
            //     $cart[$id]['shipping_cost'] = isset($cart[$id]['shipping_cost']) ? $cart[$id]['shipping_cost'] + $item['shipping_cost'] : $item['shipping_cost'];
            //     //$cart[$id]['shipping_cost'] = isset($cart[$id]['shipping_cost']) && isset($cart[$id]['weight']) ? $cart[$id]['shipping_cost'] * ceil($cart[$id]['weight']) : 0;
            //     $cart[$id]['subtotal'] = isset($cart[$id]['subtotal']) ? $cart[$id]['subtotal'] + $item['grand_total'] : $item['grand_total'];
            //     $cart[$id]['weight'] = isset($cart[$id]['weight']) ? $cart[$id]['weight'] + $item['weight'] : $item['weight'];
            //     $cart[$id]['product'][$key] = $item;
            // }
            foreach($cart_merchant->result_array() as $key => $item) {
                $id = $item['merchant'] . '-' . $item['shipping_courier'];
                $cart[$id]['merchant'] = $item['merchant'];
                $cart[$id]['shipping'] = $item['shipping_courier'];
                $cart[$id]['shipping_cost'] = isset($cart[$id]['shipping_cost']) ? $cart[$id]['shipping_cost'] + $item['shipping_cost'] : $item['shipping_cost'];
                $cart[$id]['weight'] = isset($cart[$id]['weight']) ? $cart[$id]['weight'] + $item['total_weight'] : $item['total_weight'];
                $cart[$id]['subtotal'] = isset($cart[$id]['subtotal']) ? $cart[$id]['subtotal'] : 0;
                $carts = $this->cart_model->get_all_cart_by_merchant($this->data['user']->id, $item['merchant'])->result_array();
                foreach($carts as $i => $cart_detail) {
                    $cart[$id]['subtotal'] +=  $cart_detail['grand_total'];
                    $cart[$id]['product'][$key][$i] = $cart_detail;
                }
            }
            ksort($cart, SORT_NUMERIC);

            $order_total = 0;
            $order_shipping = 0;
            foreach ($cart as $merchant => $data) {
                $data_invoice['code'] = invoice_code();
                $data_invoice['merchant'] = $data['merchant'];
                $data_invoice['shipping_courier'] = $data['shipping'];
                $data_invoice['shipping_cost'] = $data['shipping_cost'];
                $data_invoice['shipping_weight'] = $data['weight'];
                $data_invoice['subtotal'] = $data['subtotal'];
                $data_invoice['total'] = $data['subtotal'] + $data['shipping_cost'];
                $order_total += $data_invoice['subtotal'];
                $order_shipping += $data['shipping_cost'];
                // var_dump($data['product']);
                //exit();
                $invoice = $this->main->insert('order_invoice', $data_invoice);
                foreach ($data['product'] as $product) {
                    foreach($product as $item) {
                        $data_product = array(
                            'order' => $order,
                            'invoice' => $invoice,
                            'product' => $item['product_id'],
                            'name' => $item['name'],
                            'code' => $item['code'],
                            'price' => $item['price'],
                            'quantity' => $item['quantity'],
                            'weight' => $item['weight'],
                            'total' => $item['grand_total'],
                            'options' => $item['option'],
                            'promo_data' => $item['promo_data']
                        );
                        if($item['in_promo'] == 1){
                            $data_product['price_old'] = $item['old_price'];
                            $data_product['discount'] = $item['quantity'] * $item['disc_price'];
                        }
                        $order_product = $this->main->insert('order_product', $data_product);
                    }
                }
                $this->main->insert('order_history', array('order' => $order, 'invoice' => $invoice, 'order_status' => settings('order_new_status')));
            }
            $subtotal = $order_total;
            $coupon_value = 0;
            if($coupon) {
                foreach ($cart_merchant->result_array() as $shipp) {
                    $shipping_ = $shipp['shipping_courier'];
                }
                $coupon_value = $coupon->total_disc;
                if($coupon->ekspedisi == 1){
                    if($shipping_ == 'pickup'){
                        $order_total = $order_total;
                    } else {
                        $order_shipping = $order_shipping - $coupon_value;
                        if($order_shipping <= 0){
                            $order_shipping = 0;
                        }
                        $order_total = $order_total + $order_shipping;
                    }
                } else {
                    $order_total = $order_total -  $coupon_value;
                }
                if($order_total <= 0){
                    $coupon_value = $subtotal;
                    $order_total = 0;
                    if($order_shipping == 0){
                        $payment_method = 'coupon';
                    }
                }
                $data_coupon = array(
                    'coupon' => $coupon->id,
                    'order' =>$order,
                    'customer' => $this->data['user']->id,
                    'amount' => $coupon_value,
                );
                $coupon_history = $this->main->insert('coupon_history', $data_coupon);
                $this->session->set_userdata('coupon_h', $data_coupon);
            }
            // var_dump($coupon_value); die;
            // if($coupon) {
            //     $order_total = $order_total -  $coupon->total_disc;
            // }
            // $kode_max = 3;
            // $kode_uniq = rand(pow(10, $kode_max-1), pow(10, $kode_max)-1);
            // $total = $order_total + $order_shipping;
            // $tot_kode = $total+$kode_uniq;
            // $cek_kode = $this->checkout->check_kode_unik($total, $kode_uniq)->num_rows();

            // if($cek_kode > 0){ //KODE UNIK SUDAH ADA
            //     $kode_unik = rand(pow(10, $kode_max-1), pow(10, $kode_max)-1);
            // } else {
            //     $kode_unik = $kode_uniq;
            // }

            // $tot_plus_kode_ = $total+$kode_unik;
            // $tot_plus_kode = sprintf('%0.2f', $tot_plus_kode_);

            // $this->main->update('orders', array('total' => $order_total + $order_shipping, 'subtotal' => $order_total, 'unique_code' => $kode_unik, 'total_plus_kode' => $tot_plus_kode, 'shipping_cost' => $order_shipping), array('id' => $order));
            $kode_transfer = '';
            $kode_uniq = 0;
            if($payment_method == 'transfer'){
                //KODE UNIK
                for(; $kode_uniq == 0; ){
                    $kode_unik = rand(99, 999);
                    $total_ = $order_total + $order_shipping + $kode_unik;
                    $total = sprintf('%0.2f', $total_);
                    $cek_kode = $this->checkout->check_kode_unik($total, $month)->num_rows();
                    if($cek_kode == 0){
                        $kode_uniq = $kode_unik;
                    }
                }

                for(; $kode_transfer == ''; ){
                    $kode_tf = 'TMN'.rand(1001, 9999);
                    $cek_kode_tf = $this->checkout->check_kode_transfer($kode_tf, $month)->num_rows();
                    // var_dump($kode_tf); exit();
                    if($cek_kode_tf == 0){
                        $kode_transfer = $kode_tf;
                    }
                }
            } 

            $tot_plus_kode_ = $order_total + $order_shipping + $kode_uniq;
            $tot_plus_kode = sprintf('%0.2f', $tot_plus_kode_);

            //PREORDER TIME
            $get_preorder = $this->checkout->get_preorder($order)->result();
            foreach ($get_preorder as $getpre) {
                if($get_preorder){
                    $preorder_time = $getpre->preorder_time;
                } else {
                    $preorder_time = '0';
                }
            }
            $coupon_code = '';
            if($coupon){
                $ortot = $coupon->total_shopping;
                $get_coupon = $this->main->get('coupons', ['id' => $coupon->id]);
                $code_coupon = $get_coupon->code;
                if ($ortot == 0) {
                    $tot_plus_kode = sprintf('%0.2f', 0);
                    $kode_uniq = 0;
                    $payment_method = 'coupon';
                }
            } else {
                $ortot = $order_total + $order_shipping;
            }
            $this->main->update('orders', 
            array(
                'total' => $ortot,
                'subtotal' => $subtotal,
                'unique_code' => $kode_uniq,
                'transfer_code' => $kode_transfer,
                'total_plus_kode' => $tot_plus_kode,
                'shipping_cost' => $order_shipping,
                'disc_value' => $coupon_value,
                'payment_method' => $payment_method,
                'preorder_time' => $preorder_time,
                'coupon_code' => $code_coupon
            ),
            array('id' => $order));
            // $this->cart->destroy();

            foreach($all_cart->result() as $cart) {
                $this->db->where('id', $cart->id);
                $this->db->delete('cart');
            }

            foreach($cart_merchant->result() as $cart_m) {
                $this->db->where('id', $cart_m->id);
                $this->db->delete('cart_merchant');
            }

            $this->load->model('orders');
            $data = $this->orders->get($order);

            $this->data['order'] = $data;
            $this->data['customer'] = $this->main->get('customers', array('id' => $this->data['order']->customer)); //data email
            $this->data['invoices'] = $this->main->gets('order_invoice', array('order' => $this->data['order']->id)); //data email
            $payment_method = $this->main->get('payment_methods', array('name' => $payment_method));
            $this->data['payment_method'] = $payment_method; //data email

            if ($payment_method->vendor == 'xendit') {
                if ($payment_method->name == 'credit_card') {
                    redirect('checkout/credit_card?id=' . $data_order['code']);
                } else {
                    $this->load->library('xendit');
                    $xendit_response = $this->xendit->createInvoice($data_order['code'], $order_total + $order_shipping, $this->data['customer']->email, 'Order #' . $order);
                    $payment = $xendit_response;
                    if (in_array($data->payment_method, array('mandiri_va', 'bni_va', 'bri_va'))) {
                        switch ($data->payment_method) {
                            case 'mandiri_va':
                                $va_name = 'Mandiri Virtual Account';
                                $va_code = 'MANDIRI';
                                break;
                            case 'bni_va':
                                $va_name = 'BNI Virtual Account';
                                $va_code = 'BNI';
                                break;
                            case 'bri_va':
                                $va_name = 'BRI Virtual Account';
                                $va_code = 'BRI';
                                break;
                        }
                        foreach ($payment['available_banks'] as $account) {
                            if ($account['bank_code'] == $va_code) {
                                $payment_detail = array(
                                    'vendor' => 'xendit',
                                    'account_code' => $account['bank_code'],
                                    'account_name' => $va_name,
                                    'account_holder_name' => $account['account_holder_name'],
                                    'account_branch' => $account['bank_branch'],
                                    'account_number' => $account['bank_account_number'],
                                    'amount' => $account['transfer_amount'],
                                );
                                break;
                            }
                        }
                    } elseif ($data->payment_method == 'alfamart') {
                        foreach ($payment['available_retail_outlets'] as $account) {
                            if ($account['retail_outlet_name'] == 'ALFAMART') {
                                $payment_detail = array(
                                    'vendor' => 'xendit',
                                    'account_code' => $account['retail_outlet_name'],
                                    'account_name' => $account['retail_outlet_name'],
                                    'account_holder_name' => 'XENDIT',
                                    'account_branch' => '',
                                    'account_number' => $account['payment_code'],
                                    'amount' => $account['transfer_amount'],
                                );
                                break;
                            }
                        }
                    }
                    $this->main->insert('order_meta', [
                        'order_id' => $order,
                        'meta_name' => 'xendit_response',
                        'meta_value' => json_encode($payment)
                    ]);
                }
            } elseif ($payment_method->vendor == 'sprint') {
                $this->load->library('sprint');
                $transaction = $this->data['order'];
                $response = $this->sprint->process($transaction);
                $this->main->insert('order_meta', [
                    'order_id' => $order,
                    'meta_name' => 'sprint_request',
                    'meta_value' => json_encode($response['request'])
                ]);
                $this->main->insert('order_meta', [
                    'order_id' => $order,
                    'meta_name' => 'sprint_response',
                    'meta_value' => json_encode($response['response'])
                ]);
                $payment_detail = array(
                    'vendor' => 'sprint',
                    'account_code' => $response['request']['channelId'],
                    'account_name' => $payment_method->title,
                    'account_holder_name' => isset($payment_method->merchant_name) ? $payment_method->merchant_name : '',
                    'account_branch' => '',
                    'account_number' => ($data->payment_method == 'bca_va') ? $response['request']['customerAccount'] : '',
                    'amount' => $response['request']['transactionAmount'],
                );
                if (in_array($data->payment_method, ['kredivo', 'bca_sakuku', 'creditcard','creditcard01','creditcard03','creditcard06','creditcard12'])) {
                    $payment_detail['payment_url'] = $response['response']['redirectURL'];
                } elseif ($data->payment_method == 'bca_klikpay') {
                    $payment_detail['payment_url'] = $response['response']['redirectURL'];
                    $payment_detail['payment_data'] = $response['response']['redirectData'];
                }
            } elseif ($payment_method->vendor == 'ipay88') {
                $this->load->library('ipay');
                $this->load->config('ipay88', TRUE);
                $transaction = $this->data['order'];
                $cust = $this->data['customer'];
                $merchant_code = $this->config->item('merchant_code', 'ipay88');
                $merchant_key = $this->config->item('merchant_key', 'ipay88');
                $responseUrl = $this->config->item('responseURL', 'ipay88');
                $backendUrl = $this->config->item('backendURL', 'ipay88');
                $payment_detail = array(
                    'MerchantCode' => $merchant_code,
                    'MerchantKey' => $merchant_key,
                    'RefNo' => $transaction->code,
                    'Amount' => $transaction->total,
                    'Currency' => 'IDR',
                    'ProdDesc' => $transaction->code,
                    'UserName' => $cust->fullname,
                    'UserEmail' => $cust->email,
                    'UserContact' => $cust->phone,
                    'Remark' => '',
                    'Lang' => 'UTF-8',
                    'ResponseURL' => $responseUrl,
                    'BackendURL' => $backendUrl,
                );
                if($data->payment_method == 'ovo'){
                    $payment_detail['PaymentId'] = 63;
                } elseif($data->payment_method == 'tcash'){
                    $payment_detail['PaymentId'] = 15;
                } else {
                    $payment_detail['PaymentId'] = 1;
                }
                //$payment_detail['xfield1'] = '||IPP:3||';
                $responseIpay = $this->ipay->setMerchantCode($payment_detail['MerchantCode'])
                ->setMerchantKey($payment_detail['MerchantKey'])
                ->setPaymentId($payment_detail['PaymentId'])
                ->setRefNo($payment_detail['RefNo'])
                ->setAmount($payment_detail['Amount'])
                ->setCurrency($payment_detail['Currency'])
                ->setProdDesc($payment_detail['ProdDesc'])
                ->setUsername($payment_detail['UserName'])
                ->setUserEmail($payment_detail['UserEmail'])
                ->setUserContact($payment_detail['UserContact'])
                ->setRemark($payment_detail['Remark'])
                ->setLang($payment_detail['Lang'])
                ->setResponseUrl($payment_detail['ResponseURL'])
                ->setBackendUrl($payment_detail['BackendURL'])
                ->prepare()
                ->process();
                // var_dump($responseIpay);die;
                // exit();
            } elseif ($payment_method->vendor == 'tmm') {
                if($data->payment_method == 'coupon'){
                    $payment_detail = array(
                        'CouponID' => $coupon->id,
                        'CouponName' => $coupon->name,
                        'CouponCode' => $coupon->code,
                        'CouponValue' => $coupon->total_disc
                    );
                }
            } else { //is transfer bank
                // $payment_detail = json_decode($payment_method->setting);
                $parent_transfer = $this->input->post('parent_transfer');
                $payment_method_transfer = $this->main->get('payment_methods', ['name' => 'transfer', 'title' => $parent_transfer]);
                $this->data['payment_method'] = $payment_method_transfer; //data email
                $payment_detail = json_decode($payment_method_transfer->setting,true);
            }

            if ($data->payment_method == 'coupon') {
                $this->main->update('orders', ['payment_to' => json_encode($payment_detail), 'payment_status' => 'Paid', 'payment_date' => date('Y-m-d H:i:s')], ['id' => $this->data['order']->id]);
                if ($this->data['invoices']) {
                    foreach ($this->data['invoices']->result() as $invoice) {
                        if($invoice->shipping_courier == 'pickup') {
                            $due_date_inv = date('Y-m-d H:i:s', strtotime('+1 days'));
                        } else {
                            $due_date_inv = date('Y-m-d H:i:s', strtotime('+2 days'));
                        }
                        $this->main->update('order_invoice', array('order_status' => settings('order_payment_received_status'), 'due_date' => $due_date_inv), array('id' => $invoice->id));
                        $this->main->insert('order_history', array('order' => $this->data['order']->id, 'invoice' => $invoice->id, 'order_status' => settings('order_payment_received_status')));
                        $this->data['invoice'] = $invoice;
                        $this->data['merchant'] = $this->main->get('merchants', array('id' => $invoice->merchant));

                        $merchant_user = $this->main->get('merchant_users', array('id' => $this->data['merchant']->auth));
                        $message = $this->load->view('email/merchant/new_order', $this->data, true);
                        $cronjob = array(
                            'from' => settings('send_email_from'),
                            'from_name' => settings('store_name'),
                            'to' => $merchant_user->email,
                            'subject' => 'Pesanan baru dari ' . $this->data['customer']->fullname,
                            'message' => $message
                        );
                        $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));

                        if ($merchant_user->verification_phone) {
                            $sms = json_decode(settings('sprint_sms'), true);
                            $sms['m'] = 'Pesanan baru dari ' . $this->data['customer']->fullname . '. Segera proses sebelum ' . get_date_indo_full($invoice->due_date) . ' WIB.';
                            $sms['d'] = $merchant_user->phone;
                            $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                        }
                    }
                }
                $message = $this->load->view('email/transaction/payment_success', $this->data, true);
                $cronjob = array(
                    'from' => settings('send_email_from'),
                    'from_name' => settings('store_name'),
                    'to' => $this->data['customer']->email,
                    'subject' => 'Pembayaran Sukses #' . $this->data['order']->code,
                    'message' => $message
                );
                $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
                if ($this->data['customer']->verification_phone) {
                    $sms = json_decode(settings('sprint_sms'), true);
                     if($this->data['customer']->type == 'g'){
                        if($this->data['order']->payment_method == 'transfer'){
                             $sms['m'] = 'Pembayaran sebesar ' . rupiah($this->data['order']->total_plus_kode) . ' telah berhasil pada ' . get_date_indo_full(date('Y-m-d H:i:s')) . ' WIB. Pesanan Anda akan diteruskan ke penjual. Klik disini untuk memantau pesanan anda ' . site_url('guestpage/guest_history/'. encode($this->data['customer']->id));
                        }else{
                             $sms['m'] = 'Pembayaran sebesar ' . rupiah($this->data['order']->total) . ' telah berhasil pada ' . get_date_indo_full(date('Y-m-d H:i:s')) . ' WIB. Pesanan Anda akan diteruskan ke penjual. Klik disini untuk memantau pesanan anda ' . site_url('guestpage/guest_history/'. encode($this->data['customer']->id));
                        }
                    }
                    else{
                        if($this->data['order']->payment_method == 'transfer'){
                             $sms['m'] = 'Pembayaran sebesar ' . rupiah($this->data['order']->total_plus_kode) . ' telah berhasil pada ' . get_date_indo_full(date('Y-m-d H:i:s')) . ' WIB. Pesanan Anda akan diteruskan ke penjual.';
                        }else{
                             $sms['m'] = 'Pembayaran sebesar ' . rupiah($this->data['order']->total) . ' telah berhasil pada ' . get_date_indo_full(date('Y-m-d H:i:s')) . ' WIB. Pesanan Anda akan diteruskan ke penjual.';
                        }

                    }
                    $sms['d'] = $this->data['customer']->phone;
                    $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                }
            } else {
                $this->main->update('orders', array('payment_to' => json_encode($payment_detail)), array('id' => $order));
    
                // $this->data['order'] = $this->main->get('orders',array('id' => $last_insert_order)); //data email
    
                if($this->data['customer']->email){
                    $message = $this->load->view('email/transaction/invoice', $this->data, TRUE);
                    $cronjob = array(
                        'from' => settings('send_email_from'),
                        'from_name' => settings('store_name'),
                        'to' => $this->data['customer']->email,
                        'subject' => 'Menunggu Pembayaran Invoice ' . $this->data['order']->code,
                        'message' => $message
                    );
                    $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));    
                }
                if ($this->data['user']->verification_phone) {
                    $sms = json_decode(settings('sprint_sms'), true);
                    if ($this->data['customer']->type == 'g') {
                        if ($this->data['order']->payment_method == 'transfer') {
                            $pym = json_decode($this->data['order']->payment_method);
                            foreach ($pym as $py) {
                                $tfCode = strtoupper($py->code);
                                $tfNumber = $py->account_number;
                            }
                            $sms['m'] = 'Segera lakukan pembayaran Transfer ' . $tfCode . ' Tokomanamana No Rek ' . $tfNumber . ' dengan nominal ' . rupiah($this->data['order']->total_plus_kode) . ' sebelum ' . get_date_indo_full($this->data['order']->due_date) . ' WIB. Klik disini untuk memantau pesanan anda ' . site_url('guestpage/guest_history/' . encode($this->data['customer']->id));
                        } else if ($this->data['order']->payment_method == 'alfamart') {
                            $sms['m'] .= PHP_EOL.'Segera lakukan pembayaran di Alfamart dengan Kode Bayar: '.$payment_detail['account_number'] . ', merchant Xendit dengan nominal' . rupiah($this->data['order']->total) . ' sebelum ' . get_date_indo_full($this->data['order']->due_date) . ' WIB. Klik disini untuk memantau pesanan anda ' . site_url('guestpage/guest_history/'. encode($this->data['customer']->id));
                        } else if ($this->data['order']->payment_method=='indomaret') {
                            $sms['m'] .= PHP_EOL.'Segera lakukan pembayaran di Indomaret dengan Kode Bayar: '.$payment_detail['account_number'] . ', merchant Tokomanamana dengan nominal' . rupiah($this->data['order']->total) . ' sebelum ' . get_date_indo_full($this->data['order']->due_date) . ' WIB. Klik disini untuk memantau pesanan anda ' . site_url('guestpage/guest_history/'. encode($this->data['customer']->id));
                        } else if (in_array($this->data['order']->payment_method, ['mandiri_va','bni_va','bri_va','bca_va'])) {
                            if($this->data['order']->payment_method == 'bca_va'){
                                $nameVa = 'BCA Virtual Account';
                            } else if($this->data['order']->payment_method == 'mandiri_va'){
                                $nameVa = 'Mandiri Virtual Account';
                            } else if($this->data['order']->payment_method == 'bni_va'){
                                $nameVa = 'BNI Virtual Account';
                            } else {
                                $nameVa = 'BRI Virtual Account';
                            }
                            $sms['m'] .= PHP_EOL.'Total belanja Anda senilai Rp. '. $payment_detail['amount'] . '. Lakukan pembayaran ke '.$nameVa.' berikut : '.$payment_detail['account_number'] . ' sebelum ' . get_date_indo_full($this->data['order']->due_date) . ' WIB. Klik disini untuk memantau pesanan anda ' . site_url('guestpage/guest_history/'. encode($this->data['customer']->id));
                        } else {
                            if (strpos($this->data['order']->payment_method, 'creditcard') !== FALSE) {
                                $ccName = 'Credit Card';
                            } else {
                                $ccName = $this->data['order']->payment_method;
                            }

                            $sms['m'] = 'Segera lakukan pembayaran dengan ' . $ccName . 'Anda nominal ' . rupiah($this->data['order']->total) . ' sebelum ' . get_date_indo_full($this->data['order']->due_date) . ' WIB. Klik disini untuk memantau pesanan anda ' . site_url('guestpage/guest_history/'. encode($this->data['customer']->id));
                        }
                    } else {
                        if ($this->data['order']->payment_method == 'transfer') {
                            $pym = json_decode($this->data['order']->payment_method);
                            foreach ($pym as $py) {
                                $tfCode = strtoupper($py->code); 
                                $tfNumber = $py->account_number;
                            }
                            $sms['m'] = 'Segera lakukan pembayaran Transfer ' . $tfCode . 'Tokomanamana No Rek ' .$tfNumber. ' dengan nominal ' . rupiah($this->data['order']->total_plus_kode) . ' sebelum ' . get_date_indo_full($this->data['order']->due_date) . ' WIB'; 
                        } else if ($this->data['order']->payment_method=='alfamart') {
                            $sms['m'] .= PHP_EOL.'Segera lakukan pembayaran di Alfamart dengan Kode Bayar: '.$payment_detail['account_number'] . ', merchant Xendit dengan nominal' . rupiah($this->data['order']->total) . ' sebelum ' . get_date_indo_full($this->data['order']->due_date) . ' WIB';
                        } else if ($this->data['order']->payment_method=='indomaret') {
                            $sms['m'] .= PHP_EOL.'Segera lakukan pembayaran di Indomaret dengan Kode Bayar: '.$payment_detail['account_number'] . ', merchant Tokomanamana dengan nominal' . rupiah($this->data['order']->total) . ' sebelum ' . get_date_indo_full($this->data['order']->due_date) . ' WIB';
                        } else if (in_array($this->data['order']->payment_method, ['mandiri_va','bni_va','bri_va','bca_va'])) {
                            if($this->data['order']->payment_method == 'bca_va'){
                                $nameVa = 'BCA Virtual Account';
                            } else if($this->data['order']->payment_method == 'mandiri_va'){
                                $nameVa = 'Mandiri Virtual Account';
                            } else if($this->data['order']->payment_method == 'bni_va'){
                                $nameVa = 'BNI Virtual Account';
                            } else {
                                $nameVa = 'BRI Virtual Account';
                            }
                            $sms['m'] = 'Total belanja Anda senilai Rp. '. $payment_detail['amount'] . '. Lakukan pembayaran ke '.$nameVa.' berikut : '.$payment_detail['account_number']. ' sebelum ' . get_date_indo_full($this->data['order']->due_date) . ' WIB';
                        } else {
                            if (strpos($this->data['order']->payment_method, 'creditcard') !== FALSE) {
                                $ccName = 'Credit Card';
                            } else {
                                $ccName = $this->data['order']->payment_method;
                            }

                            $sms['m'] = 'Segera lakukan pembayaran dengan ' . $ccName . 'Anda nominal ' . rupiah($this->data['order']->total) . ' sebelum ' . get_date_indo_full($this->data['order']->due_date) . ' WIB';
                        }
                    }
                    $sms['d'] = $this->data['customer']->phone;
                    $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));




                    // if($this->data['order']->payment_method == 'transfer'){
                    //    $sms['m'] = 'Segera lakukan pembayaran melalui ' . strtoupper($this->data['order']->payment_method) . ' dengan nominal ' . rupiah($this->data['order']->total_plus_kode) . ' sebelum ' . get_date_indo_full($this->data['order']->due_date) . ' WIB'; 
                    // } else {
                    // $sms['m'] = 'Segera lakukan pembayaran melalui ' . strtoupper($this->data['order']->payment_method) . ' dengan nominal ' . rupiah($this->data['order']->total) . ' sebelum ' . get_date_indo_full($this->data['order']->due_date) . ' WIB';
                    // }
                    // if($this->data['order']->payment_method=='alfamart'){
                    //     $sms['m'] .= PHP_EOL.'Kode Bayar: '.$payment_detail['account_number'];
                    // }elseif(in_array($this->data['order']->payment_method, ['mandiri_va','bni_va','bri_va','bca_va'])){
                    //     $sms['m'] .= PHP_EOL.'Rek. Virtual: '.$payment_detail['account_number'];
                    // }
                    // $sms['d'] = $this->data['user']->phone;
                    // $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                    $this->main->insert('log_sms', array('content' => json_encode($sms), 'to' => $this->data['user']->phone));
                }
            }


//            if ($transaction->payment_type == 'bca_klikpay') {
//                $this->bca_klikpay($payment_request['redirectURL'], $payment_request['redirectData']);
//            } elseif ($transaction->payment_type == 'kredivo') {
//                $this->kredivo($payment_request['redirectURL'], $payment_request['redirectData']);
//            } else {
//                $this->virtual_account($transaction, $payment_rezquest);
//            }
            if ($coupon) {
                $id_customer = $this->data['user']->id;
                $get_user_coupon = $this->main->get('coupons', ['id' => $coupon->id, 'customer_id' => $id_customer]);
                if ($get_user_coupon) {
                    $this->main->update('coupons', ['status' => 0, 'show' => 0], ['id' => $get_user_coupon->id]);
                }
            }
            $this->session->unset_userdata('checkout_address');
            if (in_array($data->payment_method, ['kredivo', 'bca_sakuku', 'creditcard','creditcard01','creditcard03','creditcard06','creditcard12'])) {
                redirect($response['response']['redirectURL']);
            } elseif ($data->payment_method == 'bca_klikpay') {
                $this->sprint->bca_klikpay($response['response']['redirectURL'], $response['response']['redirectData']);
            } else if (in_array($data->payment_method, ['ovo'])) {
                echo $responseIpay;
            } else {
                redirect('checkout/finish?id=' . $data_order['code']);
                // redirect('member/order_detail/' . encode($order));
            }
        } else {
            $this->data['messages'] = validation_errors();
            $shipping_cost = 0;
            $grand_total = 0;
            $total_qty = 0;
            foreach ($all_cart->result_array() as $item) {
                // $shipping_cost += $item['shipping_cost'];
                $grand_total += $item['grand_total'];
                $total_qty += $item['quantity'];
            }
            $this->data['couponn'] = $coupon;
            $this->data['total_qty'] = $total_qty;
            $this->data['grand_total'] = $grand_total;
            // $this->data['all_cart'] = $all_cart;
            $this->data['cart_merchant'] = $cart_merchant;
            $this->data['vouchers'] = $this->checkout->get_vouchers();
            $this->data['address'] = $this->main->get('customer_address', array('customer' => $this->data['user']->id, 'primary' => 1));
            $this->data['shipping_cost'] = $shipping_cost;
            $this->data['payment_methods'] = $this->main->gets('payment_methods', ['status' => 1], 'sort_order asc');
            $this->data['payment_methods_type'] = $this->main->gets('payment_method_type', ['status' => 1], 'sort asc');
            $this->output->set_title(lang('checkout_payment_text') . ' - ' . settings('meta_title'));
            $this->template->_init();
            $this->load->js('assets/frontend/js/modules/checkout.js');
            $this->load->view('payment', $this->data);
        }
    }

    public function credit_card() {
//        $order = $this->session->flashdata('order_id');
        $order = $this->input->get('id');
        if (!$order)
            redirect('cart');
//        $this->data['order'] = $this->checkout->get_orders($order);
        $this->data['order'] = $this->main->get('orders', array('code' => $order));
        if (!$this->data['order'])
            redirect('cart');

        $this->data['invoices'] = $this->main->gets('order_invoice', array('order' => $this->data['order']->id));
        $this->data['customer'] = $this->main->get('customers', array('id' => $this->data['order']->customer));

        if ($this->data['order']->payment_method != 'transfer') {
            $this->load->model('order_meta');
            $this->data['order_meta'] = $this->order_meta->get(['order_id' => $this->data['order']->id, 'meta_name' => 'xendit_response']);
        }

        $this->output->set_title(lang('checkout_finish_text') . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->view('credit_card', $this->data);
    }

    public function finish() {
        $this->session->unset_userdata('coupon');
        $order = $this->input->get('id');
        if (!$order)
            redirect('cart');

        $order = $this->main->get('orders', array('code' => $order, 'customer' => $this->data['user']->id));
        // redirect('member/order_detail/' . encode($order->id));
        if (!$order)
            redirect('cart');

        if ($order->payment_status == 'Pending' && in_array($order->payment_method, ['bca_sakuku', 'bca_klikpay', 'kredivo', 'creditcard','creditcard01','creditcard03','creditcard06','creditcard12'])) {
            $order->payment_status = 'Failed';
            $this->main->update('orders', array('payment_status' => 'Failed'), array('id' => $order->id));
            $this->main->update('order_invoice', array('order_status' => settings('order_cancel_status')), array('order' => $order->id));
            $invoices = $this->main->gets('order_invoice', array('order' => $order->id));
            if ($invoices) {
                foreach ($invoices->result() as $invoice) {
                    $this->main->insert('order_history', array('order' => $order->id, 'invoice' => $invoice->id, 'order_status' => settings('order_cancel_status')));
                }
            }
            $this->data['order'] = $order;
            $this->data['customer'] = $this->main->get('customers', array('id' => $order->customer));
            $this->data['payment_method'] = $this->main->get('payment_methods', array('name' => $order->payment_method))->title;
            if($this->data['customer']->email){

                $message = $this->load->view('email/transaction/payment_failed', $this->data, TRUE);
                $cronjob = array(
                    'from' => settings('send_email_from'),
                    'from_name' => settings('store_name'),
                    'to' => $this->data['customer']->email,
                    'subject' => 'Pembayaran Tagihan ' . $this->data['order']->code.' Gagal',
                    'message' => $message
                );
                $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
            }
            else{
                if($this->data['customer']->verification_phone){
                     $this->load->library('sprint');
                    $sms = json_decode(settings('sprint_sms'), true);
                    $sms['m'] = 'Hai '. strtoupper($this->data['customer']->fullname).', pembayaran #' . strtoupper($this->data['order']->code) . ' gagal! Segera check di Tokomanamana.com' ;
                    $sms['d'] = $this->data['customer']->phone;
                    $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                    $this->main->insert('log_sms', array('content' => json_encode($sms), 'to' => $this->data['customer']->phone));
                }
            }
            
        }
        $this->data['order'] = $order;
        $this->data['invoices'] = $this->main->gets('order_invoice', array('order' => $this->data['order']->id));
        $this->data['customer'] = $this->main->get('customers', array('id' => $this->data['order']->customer));

        $this->output->set_title(lang('checkout_finish_text') . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->js('assets/frontend/js/modules/checkout_finish.js');
        $this->load->view('finish', $this->data);
    }

    public function getCities($province) {
        $cities = $this->main->gets('cities', array('province' => $province), 'name ASC');
        $output = '<option value="">Pilih kota</option>';
        if ($cities) {
            foreach ($cities->result() as $city) {
                $output .= '<option value="' . $city->id . '">' . $city->type . ' ' . $city->name . '</option>';
            }
        }
        echo $output;
    }

    public function getDistricts($city) {
        $districts = $this->main->gets('districts', array('city' => $city), 'name ASC');
        $output = '<option value="">Pilih kecamatan</option>';
        if ($districts) {
            foreach ($districts->result() as $district) {
                $output .= '<option value="' . $district->id . '">' . $district->name . '</option>';
            }
        }
        echo $output;
    }

    public function getShippings() {
        $data = $this->input->post(null, TRUE);
//        $couriers = json_decode($data['courier']);
        $output = '';
        foreach ($data['courier'] as $courier) {
            if($courier == 'pickup') {
                $output .= '<li>
                <input id="' . $data['id'] . '-pickup" name="shipping[' . $data['id'] . '][service]" type="radio" value="pickup" class="shipping" required="" data-id="' . $data['id'] . '" data-value="0">
                <label for="' . $data['id'] . '-pickup">Ambil Sendiri <span style="color:red;font-size:10px">* Tidak support kredivo</span></label>
                </li>';
            } else {
                $services = $this->rajaongkir->cost($data['from'], 'subdistrict', $data['to'], 'subdistrict', $data['weight'], $courier);
                if ($services) {
                    $services = json_decode($services);
                    foreach ($services->rajaongkir->results as $service) {
                        foreach ($service->costs as $cost) {
                            if($cost->service == "CTC"){
                                $label_service = "REG";
                            } else if ($cost->service == "CTCYES"){
                                $label_service = "YES";
                            } else {
                                $label_service = $cost->service;
                            }
                            $output .= '<li>';
                            $output .= '<input name="shipping[' . $data['id'] . '][service]" type="radio" value="' . $service->code . '-' . $cost->service . '" class="shipping" id="' . $data['id'] . $service->code . '-' . $cost->service . '" required="" data-id="' . $data['id'] . '" data-value="' . $cost->cost[0]->value . '">';
                            $output .= '<label for="' . $data['id'] . $service->code . '-' . $cost->service . '">' . strtoupper($service->code) . ' ' . $label_service . ' <span>(' . rupiah($cost->cost[0]->value) . ')</span></label>';
                            $output .= '</li>';
                        }
                    }
                }
            }
        }
        echo $output;
    }

    public function add_address() {
        $this->form_validation->set_rules('title', 'Nama Alamat', 'trim|required');
        $this->form_validation->set_rules('name', 'lang:checkout_shipping_field_name', 'trim|required');
        $this->form_validation->set_rules('phone', 'lang:checkout_shipping_field_phone', 'trim|required');
        $this->form_validation->set_rules('address', 'lang:checkout_shipping_field_address', 'trim|required');
        $this->form_validation->set_rules('province', 'lang:checkout_shipping_field_province', 'required');
        $this->form_validation->set_rules('city', 'lang:checkout_shipping_field_city', 'required');
        $this->form_validation->set_rules('district', 'lang:checkout_shipping_field_district', 'required');
        $this->form_validation->set_rules('postcode', 'lang:checkout_shipping_field_postcode', 'trim|required');

        $return['status'] = 'error';
        if ($this->form_validation->run() == true) {
            $data = $this->input->post(null, true);
            unset($data['shipping']);
            $data['customer'] = $this->data['user']->id;
            $data['primary'] = 0;
            $address = $this->main->insert('customer_address', $data);
            $province = $this->main->get('provincies', array('id' => $data['province']));
            $city = $this->main->get('cities', array('id' => $data['city']));
            $district = $this->main->get('districts', array('id' => $data['district']));
            $return['status'] = 'success';
            // $return['html'] = '<div class="payment-method-block">
            //                    <div class="payment-method-detail">
            //                    <input type="radio" value="' . $address . '" onclick="selectPayment(this);" name="address" required id="address-' . $address . '">
            //                    <label for="address-' . $address . '">
            //                    <div class="payment-method-title">' . $data['name'] . ' - ' . $data['title'] . '</div>
            //                    <div class="payment-method-subtitle">' . $data['address'] . '<br>' . $district->name . ', ' . $city->name . ', ' . $data['postcode'] . '<br>' . $province->name . '</div>
            //                    </label>
            //                    </div>
            //                    </div>';
            $return['html'] = '<script>
                                $( "#address-list" ).load(window.location.href + " #address-list", function() {getCheapestService();$("#shipping-cost-text").text("Rp 0");$(".shipping-cost-class").val(0);});
                                
                            </script>';
            $return['id_address'] = $address;
        } else {
            $return['message'] = '<div class="alert alert-danger fade in">' . validation_errors() . '</div>';
        }

        echo json_encode($return);
    }

    public function add_coupon() {
        $this->form_validation->set_rules('couponCode', 'Kode Kupon', 'trim|required');
        $return['status'] = 'error';
        $return['data'] = NULL;
        if ($this->form_validation->run() == true) {
            $codec = $this->input->post('couponCode');
            if($codec == 'autofill') {
                $return['autofill'] = 1;
                $coupon = $this->main->get('coupons', array('status' => 1,'autofill' => 1));
                $codec = 'autofill';
            } else {
                $return['autofill'] = 0;
                $coupon = $this->main->get('coupons', array('status' => 1,'code' => $codec));
            }
            if($coupon) {
                unset($coupon->gambar);
                unset($coupon->deskripsi);
                $coupon_type = ($coupon->catIds != '') ? 'category' : 'product';
                $today = strtotime(date("Y-m-d H:i:s"));
                $couponStart= strtotime($coupon->date_start);
                $couponEnd = strtotime($coupon->date_end);
                $sess_user = $this->session->userdata('user');
                $id_address = $this->session->userdata('checkout_address');
                $customer_address = $this->main->get('customer_address', ['id' => $id_address]);
                $all_cart = $this->main->gets('cart', ['id_customer' => $sess_user->user_id]);
                $cart_merchant = $this->cart_model->cart_merchant($sess_user->user_id);
                $this->db->where('customer',$sess_user->user_id);
                $this->db->where('coupon',$coupon->id);
                $countCc = $this->db->get('coupon_history');
                $this->db->where('coupon',$coupon->id);
                $countC = $this->db->get('coupon_history');
                $coupon->total_disc = 0;
                $count_total_cart = 0;
                $shipping_cost = 0;
                foreach ($cart_merchant->result() as $ship) {
                    $shipping_cost += $ship->shipping_cost;
                    $shipping = $ship->shipping_courier;
                }
                foreach ($all_cart->result() as $item) {
                    $count_total_cart += $item->grand_total;
                }
                if($coupon->for_product == 0){
                    if($coupon->type =='P') {
                        $coupon->total_disc = $count_total_cart * ($coupon->discount/100);
                        if($coupon->max_amount > 0){
                            if($coupon->total_disc > $coupon->max_amount){
                                $coupon->total_disc = $coupon->max_amount;
                            } else {
                                $coupon->total_disc = $coupon->total_disc;
                            }
                        } else {
                            $coupon->total_disc = $coupon->total_disc;
                        }
                    } else { //fix amount
                        if($coupon->discount == 0){
                            $coupon->total_disc = $shipping_cost;
                        } else {
                            if($coupon->ekspedisi == 1){
                                if($shipping == 'pickup'){
                                    $coupon->total_disc = 0;
                                } else {
                                    $coupon->total_disc = $coupon->discount;
                                }
                            } else {
                                $coupon->total_disc = $coupon->discount;
                            }
                        }
                    }
                }
                $list_shipping = array();
                $list_categories_invalid = array();
                $list_zona_invalid = array();
                $list_city_invalid = array();
                $invalid_type = array();
                $branch_type = array();
                $merchant_type = array();
                $exp_ids = array();
                $exp_idsx = array();
                $exp_city = array();
                $valid_city = 0;
                $exp_productIds = [];
                $valid_product = [];
                if($coupon->catIds){
                    $exp_ids = explode(',', $coupon->catIds);
                }
                if($coupon->zonaIds){
                    $exp_idsx = explode(',', $coupon->zonaIds);
                }
                if($coupon->cityIds) {
                    $exp_city = explode(',', $coupon->cityIds);
                }
                if(count($exp_city) > 0) {
                    if (in_array($customer_address->city, $exp_city)) {
                        $valid_city = 1;
                    }
                }
                if($coupon->product_id) {
                    $exp_productIds = explode(',', $coupon->product_id);
                }
                foreach ($all_cart->result() as $key => $item) {
                    // array_push($list_shipping,$item['shipping']);
                    // if (!in_array($item['category'], $exp_ids)){
                    //      array_push($list_categories_invalid,$item['category']);
                    // }
                    // if (!in_array($item['merchant_group'], $exp_idsx)){
                    //     array_push($list_zona_invalid,$item['merchant_group']);
                    // }
                    // if($item['merchant_type'] == 'branch') {
                    //     array_push($branch_type,$item['merchant_type']);
                    // } else {
                    //     array_push($merchant_type,$item['merchant_type']);
                    // }
                    $validCategory = 0;
                    $product = $this->main->get('products', ['id' => $item->product]);
                    $merchant = $this->main->get('merchants', ['id' => $item->merchant]);
                    // if (!in_array($product->category, $exp_ids)) {
                    //     array_push($list_categories_invalid, $product->category);
                    // }
                    if ($coupon_type == 'category') {
                        if(in_array($product->category, $exp_ids)) {
                            $validCategory = 1;
                        }
                        $valid_product = [1];
                    } else {
                        if(in_array($item->product, $exp_productIds)) {
                            array_push($valid_product, $item->product);
                        }
                        $validCategory = 1;
                    }
                    // if ($merchant->type == 'merchant') {
                    //     array_push($merchant_type, $merchant->type);
                    // } else {
                    //     array_push($branch_type, $merchant->type);
                    // }
                }

                foreach($cart_merchant->result_array() as $key => $item) {
                    array_push($list_shipping, $item['shipping_courier']);
                }
                
                if($coupon->coupon_type == 'branch'){
                    // $invalid_type = $merchant_type;
                } else if($coupon->coupon_type == 'merchant'){
                    $invalid_type = $branch_type;
                }
                //var_dump($this->cart->contents());exit();
                $counts = array_count_values($list_shipping);
                $count_total_shipping = 0;
                $this->data['pickup_count'] = isset($counts['pickup']) ? $counts['pickup'] : 0;
                $shipping_group= array();
                foreach ($cart_merchant->result_array() as $key => $item) {
                    $id = $item['merchant'] . '-' . $item['shipping_courier'];
                    $shipping_group[$id]['merchant_id'] = $item['merchant'];
                    $shipping_group[$id]['shipping_type'] = $item['shipping_courier'];
                    $shipping_group[$id]['shipping_cost'] = $item['shipping_cost'];
                    $shipping_group[$id]['weight'] = isset($shipping_group[$id]['weight']) ? ($shipping_group[$id]['weight'] + $item['total_weight']) / 1000 : $item['total_weight'] / 1000;
                    $shipping_group[$id]['shipping_total'] = $shipping_group[$id]['shipping_cost'];
                    if($coupon->for_product) {
                        $cart_details = $this->main->gets('cart', ['id_customer' => $sess_user->user_id, 'merchant' => $item['merchant']]);
                        foreach($cart_details->result_array() as $cart) {
                            if ($coupon_type == 'product') {
                                if (in_array($cart['product'], $exp_productIds)) {
                                    if ($coupon->type == 'P') {
                                        $coupon->total_disc = $coupon->total_disc + (($cart['price'] * $cart['quantity']) * ($coupon->discount / 100));
                                    } else {
                                        if ($coupon->discount == 0) {
                                            $coupon->total_disc = $shipping_group[$id]['shipping_total'];
                                        } else {
                                            if ($coupon->ekspedisi == 1) {
                                                if ($shipping == 'pickup') {
                                                    $coupon->total_disc = 0;
                                                } else {
                                                    $coupon->total_disc = $coupon->total_disc + ($coupon->discount * $cart['quantity']);
                                                }
                                            } else {
                                                $coupon->total_disc = $coupon->total_disc + ($coupon->discount * $cart['quantity']);
                                            }
                                        }
                                    }
                                }
                            } else {
                                if($coupon->type =='P') {
                                    $coupon->total_disc = $coupon->total_disc + (($cart['price']*$cart['quantity']) * ($coupon->discount/100));
                                } else {
                                    if($coupon->discount == 0){
                                        $coupon->total_disc = $shipping_group[$id]['shipping_total'];
                                    } else {
                                        if($coupon->ekspedisi == 1){
                                            if($shipping == 'pickup'){
                                                $coupon->total_disc = 0;
                                            } else {
                                                $coupon->total_disc = $coupon->total_disc + ($coupon->discount * $cart['quantity']);
                                            }
                                        } else {
                                            $coupon->total_disc = $coupon->total_disc + ($coupon->discount * $cart['quantity']);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // foreach ($shipping_group as $key => $item){
                //     $count_total_shipping = $count_total_shipping + $item['shipping_total'];
                // }
                $count_total_shipping = $this->session->userdata('shipping');
                if($codec == 'autofill'){
                    if($coupon->ekspedisi == 1){
                        if($shipping == 'pickup'){
                            $count_total_shipping = 0;
                        } else {
                            $count_total_shipping = $count_total_shipping - $coupon->total_disc;
                            if($count_total_shipping <= 0){
                                $count_total_shipping = 0;
                            }
                        } 
                    } else {
                        $coupon->total_shopping = $count_total_cart - $coupon->total_disc;
                    }
                } else {
                    if($coupon->ekspedisi == 1){
                        if($shipping == 'pickup'){
                            $coupon->total_shopping = $count_total_cart;
                           
                        } else {
                            $dss = $count_total_shipping - $coupon->total_disc;
                            if($dss <= 0){
                                $dss = 0;
                            }
                            $coupon->total_shopping = $count_total_cart + $dss;
                        }
                    } else {
                        $coupon->total_shopping = $count_total_cart + $count_total_shipping - $coupon->total_disc;
                    }
                }
                // $coupon->total_shopping = $count_total_cart + $count_total_shipping - ($this->session->userdata('coupon')?$coupon->total_disc:0);
                if(count($list_categories_invalid) > 0){
                    $return['message'] = '<div class="alert alert-danger fade in"> Kupon tidak berlaku !</div>';
                }
                if($today <= $couponStart || $today >= $couponEnd){
                    $return['message'] = '<div class="alert alert-danger fade in"> Kupon sudah tidak berlaku !</div>';
                } else if($count_total_cart < $coupon->min_order){
                    $return['message'] = '<div class="alert alert-danger fade in">Minimal transaksi dibawah '.$coupon->min_order.'!</div>';
                } else if($coupon->uses_customer <= $countCc->num_rows() && $coupon->uses_customer != 0){
                    $return['message'] = '<div class="alert alert-danger fade in">Kupon sudah pernah digunakan !</div>';
                } else if($coupon->uses_total <= $countC->num_rows() && $coupon->uses_total != 0){
                    $return['message'] = '<div class="alert alert-danger fade in">Kupon sudah tidak berlaku !</div>';
                } else if(count($list_categories_invalid) > 0){
                    $return['message'] = '<div class="alert alert-danger fade in"> Kupon tidak berlaku di kategori ini!</div>';
                } else if(count($list_zona_invalid) > 0){
                    $return['message'] = '<div class="alert alert-danger fade in"> Kupon tidak berlaku di zona ini!</div>';
                } else if(count($invalid_type) > 0){
                    $return['message'] = '<div class="alert alert-danger fade in"> Kupon tipe salah!</div>';
                } else if ($validCategory == 0) {
                    $return['message'] = '<div class="alert alert-danger fade in"> Voucher tidak berlaku di kategori ini!</div>';
                } else if ($valid_city == 0) {
                    $return['message'] = '<div class="alert alert-danger fade in"> Voucher tidak berlaku di kota ini!</div>';
                } else if (count($valid_product) == 0) {
                    $return['message'] = '<div class="alert alert-danger fade in">Voucher tidak berlaku untuk produk ini!</div>';
                }
                else {
                    $this->session->set_userdata('coupon', $coupon);
                    $return['message'] = '<div class="alert alert-success fade in">Kode Kupon valid!</div>';
                    $return['data'] = $coupon;
                    $return['status'] = 'success';
                }
            } else {
                $return['message'] = '<div class="alert alert-danger fade in">Kode Kupon invalid!</div>';
            }
        } else {
            $return['message'] = '<div class="alert alert-danger fade in"> Kode Kupon tidak boleh kosong</div>';
        }
        echo json_encode($return);
    }

    public function reset_coupon() {
        $this->session->unset_userdata('coupon');
        $list_shipping = array();
        $count_price_cart = 0;
        $all_cart = $this->main->gets('cart', ['id_customer' => $this->data['user']->id]);
        foreach($all_cart->result() as $item) {
            $count_price_cart += $item->grand_total;
        }
        $cart_merchant = $this->cart_model->cart_merchant($this->data['user']->id);
        foreach ($cart_merchant->result_array() as $key => $item) {
            array_push($list_shipping,$item['shipping_courier']);
        }
        //var_dump($this->cart->contents());
        $counts = array_count_values($list_shipping);
        $count_total_shipping = 0;
        $this->data['pickup_count'] = isset($counts['pickup']) ? $counts['pickup'] : 0;
        $shipping_group= array();
        foreach ($cart_merchant->result_array() as $key => $item) {
            $id = $item['merchant'] . '-' . $item['shipping_courier'];
            $shipping_group[$id]['merchant_id'] = $item['merchant'];
            $shipping_group[$id]['shipping_type'] = $item['shipping_courier'];
            $shipping_group[$id]['shipping_cost'] = $item['shipping_cost'];
            $shipping_group[$id]['weight'] = isset($shipping_group[$id]['weight']) ? ($shipping_group[$id]['weight'] + $item['total_weight']) / 1000 : $item['total_weight'] / 1000;
            $shipping_group[$id]['shipping_total'] = $shipping_group[$id]['shipping_cost'];
        }
        foreach ($shipping_group as $key => $item){
            $count_total_shipping = $count_total_shipping + $item['shipping_total'];
        }
        $return['totalShopping']= $count_price_cart + $count_total_shipping;
        $return['status'] = 'success';
        $return['message'] = 'Sukses menghapus kupon';
        echo json_encode($return);
    }
//    public function email($id) {
//        $this->data['data'] = $this->main->get('order_payment', array('code' => $id));
//        $this->data['orders'] = $this->main->gets('orders', array('payment' => $this->data['data']->id));
//        $this->load->view('email/payment_order', $this->data);
//    }
    private function _payment_email($id) {
        $this->data['data'] = $this->main->get('order_payment', array('id' => $id));
        $this->data['orders'] = $this->main->gets('orders', array('payment' => $this->data['data']->id));
        $message = $this->load->view('email/payment_order', $this->data, TRUE);
        if (!send_mail('noreply@tokomanamana.com', $this->data['user']->email, 'Menunggu Pembayaran Transfer Bank untuk pembayaran ' . $this->data['data']->code, $message)) {
            log_message('error', 'send email failed');
        }
    }

    private function _invoice_email($id) {
//    public function invoice_email($id) {
        $this->load->library('email');
        $config['mailtype'] = 'html';
        $config['wordwrap'] = FALSE;
        $config['priority'] = 1;
        $config['protocol'] = 'sendmail';
        $this->email->initialize($config);
//        $this->data['order'] = $this->checkout->get_order_by_id($id);
//        $data['address'] = $this->main->get('address', array('auth' => $this->data['user']->id, 'primary' => 1));
//        $data['customer'] = $this->checkout->getProfile($data['order']->customer);
//        $this->data['products'] = $this->checkout->get_order_products($id);
        $this->data['order'] = $this->main->get('orders', array('id' => $id));
        $this->data['customer'] = $this->main->get('customers', array('id' => $this->data['order']->customer));
        $this->data['invoices'] = $this->main->gets('order_invoice', array('order' => $this->data['order']->id));
        $message = $this->load->view('email/invoice', $this->data, TRUE);
        // send_mail('noreply@tokomanamana.com', $this->data['user']->email, 'Menunggu Pembayaran Invoice ' . $this->data['order']->code, $message);
        $this->email->from('noreply@tokomanamana.com', 'TokoManaMana.com');
        $this->email->to($this->data['customer']->email);
        $this->email->subject('Menunggu Pembayaran Invoice ' . $this->data['order']->code);
        $this->email->message($message);
        $this->email->send();
    }

    public function info_store() {
        $id = $this->input->post('id');
        $merchant = $this->checkout->get_merchant($id);
        if($merchant == null) {
            $output = '<dl class="dl-horizontal">
                        <p class="lead topbottom">Toko Tidak Terjangkau</p>
                        </dl>';
        } else {
            $output = '<dl class="dl-horizontal">
                    <p class="lead topbottom">' . $merchant->name . '</p>
                    <hr class="topbottom">';
            if ($merchant->username) {
                $output .= '<dt class="listHeading">ID:</dt>
                    <dd class="listText">' . $merchant->username . '</dd>';
            }
            $output .= '<dt class="listHeading">Kota:</dt>
                    <dd class="listText">' . $merchant->city . '</dd>';
            $output .= '<dt class="listHeading">Kecamatan:</dt>
                    <dd class="listText">' . $merchant->district . '</dd>';
            if ($merchant->distance) {
                $output .= '<dt class="listHeading">Jarak:</dt>
                    <dd class="listText">' . number($merchant->distance) . ' km</dd>';
            }
            $merchant->rating = round($merchant->rating);
            $rating = '<span class="spr-starrating spr-badge-starrating">';
            for ($i = 1; $i <= $merchant->rating; $i++) {
                $rating .= '<i class="spr-icon spr-icon-star"></i>';
            }
            for ($i = $merchant->rating + 1; $i <= 5; $i++) {
                $rating .= '<i class="spr-icon spr-icon-star-empty"></i>';
            }
            $rating .= '</span>';
            $output .= '<dt class="listHeading">Rating:</dt>
                    <dd class="listText">' . $rating . '</dd>
                </dl>';
        }
        echo $output;
    }


    // public function check_stock() {
    //     $this->load->model('checkout_model', 'checkout');
    //     // $this->input->is_ajax_request() or exit('No direct post submit allowed!');
    //     if (!$this->cart->contents()){
    //         $return = array('message' => 'Tidak ada barang di dalam cart! ', 'status' => 'failed');
    //     } else {
    //         $not_avail = array();
    //         $merchant_stock = array();
    //         $count_valid = 0;

    //         foreach ($this->cart->contents() as $item) {

    //             /* Every single ID on cart session will be separated into 2 variable,
    //             ** product ID and store ID */
    //             list($product_id, $store_id) = explode('-', $item['id']);

    //             $item['store'] = $store_id;

    //             /* Checking if the product is package product or a single item product */
    //             if($item['type'] == 'p') {
    //                 $package_items = $item['package_items'];
    //                 $count_packages = count($package_items);
            
    //                 foreach ($package_items as $package) {
    //                     # code...
    //                     /* Checking every product stock in package */
    //                     $check_stock = $this->checkout->get_merchant_stock($package['product_id'], $item['store'], ($item['qty'] * $package['qty']));

    //                     if ($check_stock->num_rows() > 0) {
    //                         # code...
    //                         foreach ($check_stock->result() as $cs) {
    //                             $merchant_stock[$cs->branch_id][] = array('prod_id' => $package['product_id']);
    //                         }
    //                     }
    //                 }

    //                 if (count($merchant_stock) > 0) {
    //                     # code...
    //                     foreach ($merchant_stock as $mcn) {
    //                         # code...
    //                         if (count($mcn) == $count_packages) {
    //                             # code...
    //                             $count_valid++;
    //                         }
    //                     }
    //                 }

    //                 /* If the product is available, the cart will be procced
    //                 ** but if it doesnt, the product data will be send back to
    //                 ** the cart page and user will see the warning */
    //                 if ($count_valid > 0) {
    //                     # code...
    //                 }
    //                 else {
    //                     $not_avail[] = $item;
    //                 }
    //             }
    //             else {
    //                 if ($item['preorder'] == 1) {
    //                     // exit();
    //                 }
    //                 else {
    //                     /* Checking every stock of product in the cart session */
    //                     $check_stock = $this->checkout->get_merchant_stock($product_id, $item['store'], $item['qty']);
                        
    //                     /* If the product is available, the cart will be procced
    //                     ** but if it doesnt, the product data will be send back to
    //                     ** the cart page and user will see the warning */
    //                     if ($check_stock->num_rows() > 0) {
    //                         // $return = array('message' => 'Stok barang tersedia! ', 'status' => 'success');
    //                     }
    //                     else {
    //                         $not_avail[] = $item;
    //                     }
    //                 }
    //             }

    //             if(count($not_avail) > 0) {
    //                 $return = array('message' => 'Stok barang tidak tersedia! ', 'status' => 'failed','data' => $not_avail);
    //             } else {
    //                 $return = array('message' => 'Stok barang tersedia! ', 'status' => 'success');
    //             }
                
    //         }
            
    //     }

    //     echo json_encode($return);
    // }

    public function check_stock() {
        $this->load->model('cart_model');
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $id_customer = $this->data['user']->id;
        $carts = $this->main->gets('cart', ['id_customer' => $id_customer]);

        $return = ['status' => 'success', 'data' => []];

        foreach($carts->result() as $cart) {
            $product = $this->main->get('products', ['id' => $cart->product]);
            if($product->min_buy) {
                $product->min_buy = $product->min_buy;
            } else {
                $product->min_buy = 1;
            }
            if($product->store_type == 'principal') {
                $product_principal_stock = $this->main->get('products_principal_stock', ['product_id' => $product->id, 'branch_id' => $cart->merchant]);
                if($cart->option) {
                    $options = json_decode($cart->option);
                    foreach($options as $option) {
                        $product_option = $option->id;
                        if($option->product_quantity) {
                            if($option->product_quantity < $option->quantity) {
                                $return['status'] = 'failed';
                                array_push($return['data'], [
                                    'id' => encode($cart->id * $product_option),
                                    'min_buy' => $product->min_buy,
                                    'option' => 'true',
                                    'status' => 'failed'
                                ]);
                            }
                            if($option->quantity < $product->min_buy) {
                                $return['status'] = 'failed';
                                array_push($return['data'], [
                                    'id' => encode($cart->id * $product_option),
                                    'min_buy' => $product->min_buy,
                                    'option' => 'true',
                                    'status' => 'failed_min_buy'
                                ]);
                            }
                        } else {
                            if($product->quantity < $option->quantity) {
                                $return['status'] = 'failed';
                                array_push($return['data'], [
                                    'id' => encode($cart->id * $product_option),
                                    'min_buy' => $product->min_buy,
                                    'option' => 'true',
                                    'status' => 'failed'
                                ]);
                            }
                            if($option->quantity < $product->min_buy) {
                                $return['status'] = 'failed';
                                array_push($return['data'], [
                                    'id' => encode($cart->id * $product_option),
                                    'min_buy' => $product->min_buy,
                                    'option' => 'true',
                                    'status' => 'failed_min_buy'
                                ]);
                            }
                        }
                    }
                } else {
                    if($product_principal_stock->quantity < $cart->quantity) {
                        $return['status'] = 'failed';
                        array_push($return['data'], [
                            'id' => encode($cart->id),
                            'min_buy' => $product->min_buy,
                            'option'=> 'false',
                            'status' => 'failed'
                        ]);
                    }
                    if($cart->quantity < $product->min_buy) {
                        $return['status'] = 'failed';
                        array_push($return['data'], [
                            'id' => encode($cart->id),
                            'min_buy' => $product->min_buy,
                            'option'=> 'false',
                            'status' => 'failed_min_buy'
                        ]);
                    }
                }
            } else {
                if($cart->option) {
                    $options = json_decode($cart->option);
                    foreach($options as $option) {
                        $product_option = $option->id;
                        if($option->product_quantity) {
                            if($option->product_quantity < $option->quantity) {
                                $return['status'] = 'failed';
                                // $return['data'][] = encode($cart->id * $product_option);
                                array_push($return['data'], [
                                    'id' => encode($cart->id * $product_option),
                                    'min_buy' => $product->min_buy,
                                    'status' => 'failed',
                                    'option' => 'true'
                                ]);
                            }
                            if($option->quantity < $product->min_buy) {
                                $return['status'] = 'failed';
                                // $return['data']['id'] = encode($cart->id * $product_option);
                                // $return['data']['min_buy'] = $product->min_buy;
                                array_push($return['data'], [
                                    'id' => encode($cart->id * $product_option),
                                    'min_buy' => $product->min_buy,
                                    'status' => 'failed_min_buy',
                                    'option' => 'true'
                                ]);
                            }
                        } else {
                            if($option->quantity < $product->min_buy) {
                                $return['status'] = 'failed';
                                array_push($return['data'], [
                                    'id' => encode($cart->id * $product_option),
                                    'min_buy' => $product->min_buy,
                                    'status' => 'failed_min_buy',
                                    'option' => 'true'
                                ]);
                            }
                            if($product->quantity < $option->quantity) {
                                $return['status'] = 'failed';
                                // $return['data']['id'] = encode($cart->id * $product_option);
                                // $return['data']['min_buy'] = $product->min_buy;
                                array_push($return['data'], [
                                    'id' => encode($cart->id * $product_option),
                                    'min_buy' => $product->min_buy,
                                    'status' => 'failed',
                                    'option' => 'true'
                                ]);
                            }
                        }
                    }
                } else {
                    if($cart->quantity < $product->min_buy) {
                        $return['status'] = 'failed';
                        // $return['data']['id'] = encode($cart->id);
                        // $return['data']['min_buy'] = $product->min_buy;
                        array_push($return['data'], [
                            'id' => encode($cart->id),
                            'min_buy' => $product->min_buy,
                            'status' => 'failed_min_buy',
                            'option' => 'false'
                        ]);
                    }
                    if($product->quantity < $cart->quantity) {
                        $return['status'] = 'failed';
                        // $return['data']['id'] = encode($cart->id);
                        // $return['data']['min_buy'] = $product->min_buy;
                        array_push($return['data'], [
                            'id' => encode($cart->id),
                            'min_buy' => $product->min_buy,
                            'status' => 'failed',
                            'option' => 'false'
                        ]);
                    }
                }
            }
        }

        echo json_encode($return);

    }

    public function check_verifikasi() {
        $cek_verif_phone = $this->main->get('customers', array('id' => $this->data['user']->id));
        if($cek_verif_phone->phone == '') {
            $status = 'error_not_phone';
        } elseif($cek_verif_phone->verification_phone == 0) {
            $status = 'error';
        } else {
            $status = 'success';
        }
        echo $status;
    }

    public function check_address() {
        $check_address = $this->main->get('customer_address', ['customer' => $this->data['user']->id]);
        if(!$check_address) {
            $status = 'error';
        } else {
            $status = 'success';
        }
        echo $status;
    }

    public function change_address(){
        $data = $this->input->post(null, TRUE);
        $id = $data['id'];
        $address = $this->checkout->get_address($id);
        $this->session->set_userdata('checkout_address', $address);
        $this->session->set_userdata('address_session', $id);
        $cart_merchant = $this->cart_model->cart_merchant($this->data['user']->id)->result_array();
        if($address){
            $primary_address_ = $address->row();
            $return['new_district'] = $primary_address_->district;
            $return['new_address'] = '<div class="payment-method-detail">
                                        <label for="address-'.$primary_address_->id.'">
                                            <div class="payment-method-title"><strong>'.$primary_address_->name.'</strong> ('.$primary_address_->title.') 
                                            </div>
                                            <div class="payment-method-subtitle">'.$primary_address_->phone.'<br>'. $primary_address_->address .' '. $primary_address_->district_name . ', ' . $primary_address_->city_name . ', ' . $primary_address_->postcode . ' ' . $primary_address_->province_name.'
                                        </label>
                                    </div>';
            $return['status'] = 'success';
            $return['html'] = '<div class="loading">Loading&#8230;</div>
                               <script>$( "#address-list" ).load(window.location.href + " #address-list" );
                               $( "#modal-address" ).load(window.location.href + " #modal-address" );</script>';
            // $return['reload_ekspedisi'] = '<script>$( "#ekspedisi-" ).load(window.location.href + " #ekspedisi-");</script>';
            // $return['reload_full_ekspedisi'] = '<script>$("#full-ekspedisi").load(window.location.href + " #full-ekspedisi");</script>';
            $return['reload_full_ekspedisi'] = [];
            foreach($cart_merchant as $item) {
                $return['ekspedisi'] = '<script>$("#full-ekspedisi-' . $item['id'] . '").load(window.location.href + " #shipping-courier-' . $item['id'] . '", function() {getCheapestService()});</script>';
                $return['reload_full_ekspedisi'][] = $return['ekspedisi'];
            }
            $return['reload_full_ekspedisi_mobile'] = [];
            foreach($cart_merchant as $item) {
                $return['ekspedisi_mobile'] = '<script>$("#full-ekspedisi-mobile-' . $item['id'] . '").load(window.location.href + " #shipping-courier-mobile-' . $item['id'] . '", function() {getCheapestService()});</script>';
                $return['reload_full_ekspedisi_mobile'][] = $return['ekspedisi_mobile'];
            }
            $return['load_info_product'] = '<script>$(".info-shop").load(window.location.href + " #info_product", function() {getCheapestService();});</script>';
        } else {
            $return['status'] = 'gagal';
            $return['html'] = '<p>Something went wrong !</p>';
        }

        echo json_encode($return);
    }

}