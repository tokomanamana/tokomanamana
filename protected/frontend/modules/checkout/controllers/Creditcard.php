<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Creditcard extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in())
            redirect('member/login?back=' . uri_string());
        $this->load->library('form_validation');
        $this->lang->load('checkout', settings('language'));
        $this->load->model('catalog/product_model', 'product');
        $this->load->model('orders');
        $this->load->model('order_meta');
        $this->load->model('payment_methods');
    }

    public function index() {
        redirect('checkout/address');
    }

    public function charge()
    {
        $this->load->library('xendit');

        $form_data = $this->input->post('data', true);
        $data = [
            'token_id' => $this->input->post('token_id', true),
            'authentication_id' => $this->input->post('authentication_id', true),
            'amount' => $form_data['amount'],
            'card_number' => $form_data['card_number'],
            'card_exp_month' => $form_data['card_exp_month'],
            'card_exp_year' => $form_data['card_exp_year'],
            'card_cvn' => $form_data['card_cvn'],
            'id' => $this->input->post('id', true)
        ];

        try {
            $order = $this->orders->get($data['id']);
            $response = $this->xendit->captureCreditCardPayment($order->code, $data['token_id'], $data['amount'], ['authentication_id' => $data['authentication_id']]);

            if ( ! $response ) {
                throw new Exception( "Error in Processing Request to Xendit. Response return null", 1 );
            }

            $this->order_meta->insert(['order_id' => $data['id'], 'meta_name' => 'xendit_response', 'meta_value' => json_encode($response)]);

            if  (isset($response['status']) && $response['status'] == "CAPTURED") {
                $update = $this->orders->update([
                    'payment_status' => 'Paid',
                    'payment_date' => date('Y-m-d'),
                ], ['id' => $data['id']]);
                $this->main->update('order_invoice', array('order_status' => settings('order_payment_received_status')), array('order' => $order->id));
                $invoices = $this->main->gets('order_invoice', array('order' => $order->id));
                if ($invoices)
                    foreach ($invoices->result() as $invoice) {
                        $this->main->insert('order_history', array('order' => $order->id, 'invoice' => $invoice->id, 'order_status' => settings('order_payment_received_status')));
                    }

                echo 'finish?id='.$order->code;
            } else {
                $update = $this->orders->update(['payment_status' => 'Failed', 'payment_date' => date('Y-m-d')], ['id' => $data['id']]);
                echo 'failed?id='.$order->code;
            }

        } catch ( Exception $e ) {
            log_message( 'error', $e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine() );
        }
    }
}
