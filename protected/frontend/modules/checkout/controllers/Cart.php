<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->lang->load('checkout', settings('language'));
        $this->load->model('catalog/product_model', 'product');
        $this->load->model('catalog/product_price_model', 'product_prices');
        $this->load->model('cart_model');
    }

    public function index() {
        $this->session->unset_userdata('coupon');
        $this->output->set_title(lang('cart_text') . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->js('assets/frontend/js/modules/cart.js');
        $this->data['page'] = 'cart';
        if($this->ion_auth->logged_in()) {
            $id_customer = $this->data['user']->id;
            $this->data['cart_item'] = $this->cart_model->get_all_cart($id_customer, $this->input->ip_address())->result();
        } else {
            $this->data['cart_item'] = NULL;
        }

        $this->load->view('cart', $this->data);
    }

    public function add() {
        $data = $this->input->post(null, true);
        $id = decode($data['product']);
        $store = $data['store'];
        $qty = $data['qty'];
        $output = array('status' => 'error', 'message' => 'Qty minimum 1');
        if ($qty > 0) {
            $product = $this->product->get_detail_products($id, $store);
            if ($product) {
                do {
                    $product_option = 0;
                    if (isset($data['option'])) {
                        $product_option = $this->product->get_product_option_id($product->product_id, $data['option']);

                        if (!$product_option) {
                            $output['message'] = 'Kesalahan memilih variasi produk.';
                            break;
                        }

                        //$product_option = $this->main->get('product_option', array('id' => $current_product_option));
                        $options = $this->product->get_option_groups($product->id, $product_option);
                        $option = $options->row();
                        //print_r($option);
                        //$product_option = $product_options->row();
                        $image = $this->product->get_product_option_images($product_option);
                        if ($image->num_rows() > 0) {
                            $product->image = $image->row()->image;
                        }
                    }
                    if ($product->length && $product->height && $product->width) {
                        $weight_volume = ($product->length * $product->height * $product->width) / 6;
                        if ($weight_volume > $product->weight)
                            $product->weight = round($weight_volume);
                    }

                    $old_price = $product->price;
                    if($product->promo == 1){
                        $promo_data = json_decode($product->promo_data,true);
                        if($promo_data['type'] == 'P') {
                            $disc_value = $promo_data['discount'];
                            $disc_price = round(($product->price * $promo_data['discount']) / 100);
                        } else {
                            $disc_value = NULL;
                            $disc_price = $promo_data['discount'];
                        }
                        $end_price= $product->price - $disc_price;
                        $today = date('Y-m-d');
                        $today=date('Y-m-d', strtotime($today));
                        $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                        $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
            
                        if (($today >= $DateBegin) && ($today <= $DateEnd)){
                            $price = $end_price;
                            $inPromo = true;
                        } else {
                            $price = $product->price;
                            $inPromo = false;
                        }
                    } else {
                        $price = $product->price;
                        $inPromo = false;
                    }

                    $rowid = md5($id . ($product_option ? $product_option : 0));
                    $item = $this->cart->get_item($rowid);
                    $merchant = $this->session->userdata('list_merchant')[0];
                    //$price = $product->price;
                    //jika produk tanaka
                    if ($product->merchant == 0) {
                        $price_level = $this->product->price_in_level($product->product_id, $merchant['group'], ($item ? $item['qty'] + $qty : $qty));
                        if ($price_level) { //jika price level ada, maka produk tsb masuk ke harga grosir
                            $price = $price_level;
                            $price_level = 1;
                        } else {
                            $price_group = $this->main->get('product_price', array('product' => $product->product_id, 'merchant_group' => $merchant['group']));
                            if ($price_group) {
                                $old_price = $price_group->price;
                                if($product->promo == 1){
                                    if($promo_data['type'] == 'P') {
                                        $disc_price_group = round(($price_group->price * $promo_data['discount']) / 100);
                                    } else {
                                        $disc_price_group = $promo_data['discount'];
                                    }
                                    $end_price_group= $price_group->price - $disc_price_group;
                        
                                    if (($today >= $DateBegin) && ($today <= $DateEnd)){
                                        $price = $end_price_group;
                                    } else {
                                        $price = $price_group->price;
                                    }
                                } else {
                                    $price = $price_group->price;
                                }
                            }
                        }
                    } else {
                        $price_level = 0;
                    }
                    $price += ($product_option) ? $option->price : 0;
                    $weight = $product->weight + ($product_option ? $option->weight : 0);
                    if ($item) {
                        $item['qty'] += $qty;
                        $item['price'] = $price;
                        $item['price_level'] = $price_level;
                        $item['weight'] = $weight * $item['qty'];
                        $this->cart->update($item);
                    } else {
                        $item = array(
                            'id' => $id . '-' . $store,
                            'name' => $product->name,
                            'type' => $product->type,
                            'image' => $product->image,
                            'code' => $product->code,
                            'category' => $product->category,
                            'description' => $product->short_description,
                            'qty' => $qty,
                            'price' => $price,
                            'price_level' => $price_level,
                            'preorder' => $product->preorder,
                            'merchant' => $product->merchant,
                            'merchant_name' => $product->merchant_name,
                            'merchant_group' => ($product->merchant) ? 0 : $merchant['group'],
                            'weight' => $weight * $qty,
                            'shipping' => '',
                            'shipping_merchant' => $product->merchant,
                            'shipping_cost' => 0,
                            'options' => ($product_option) ? $options->result_array() : array()
                        );
                        if($product->type == 'p'){
                            $item['package_items'] = json_decode($product->package_items, true);
                        } else {
                            $item['package_items'] = array();
                        }
                        if($inPromo){
                            $item['inPromo'] = true;
                            $item['promo_data'] = $promo_data;
                            $item['disc_value'] = $disc_value;
                            $item['disc_price'] = $disc_price;
                            $item['old_price'] = $old_price;
                        } else {
                            $item['inPromo'] = false;
                            $item['promo_data'] = array();
                            $item['disc_value'] = NULL;
                            $item['disc_price'] = NULL;
                            $item['old_price'] = $price;
                        }
                        $this->cart->insert($item);
                    }
                    $output['status'] = 'success';
                } while (0);

//            $product_options = $this->product->get_product_options($id);
//            if ($product_options) {
//                foreach ($product_options->result() as $po) {
//                    if ($po->required && empty($options[$po->id])) {
//                        $output['error']['option'][] = (int) $po->id;
//                    }
//                }
//            }
//            if (!isset($output['error'])) {
//                $item_options = array();
//                if ($options) {
//                    foreach ($options as $option => $option_variant) {
//                        $option = $this->product->get_product_option_variant_full($option, $option_variant);
//                        if ($option) {
//                            $price_option += $option['price'];
//                            $weight_option += $option['weight'];
//                        }
//                        array_push($item_options, $option);
//                    }
//                }
//                $merchant = $this->session->userdata('list_merchant')[0];
//
//                $price = $price + $price_option;
//                $weight = $product->weight + $weight_option;
//            }
            }
        }
        echo json_encode($output);
    }

    // public function update_cart_ajax() {
    //     $qty = $this->input->post('qty', true);
    //     $id_customer = $this->input->post('id_customer', true);
    //     $product_id = $this->input->post('product', true);
    //     $merchant_id = $this->input->post('store', true);
    //     $id_cart = $this->input->post('id_cart', true);

    //     $item = $this->cart_model->check_cart_update($id_cart);

    //     if($item) {
    //         $product = $this->product->get_detail_products($product_id, $merchant_id);
    //         if($product) {
    //             $old_price = $item->old_price;
    //             if($item['in_promo'] == 1) {
    //                 $promo_data = json_decode($item['promo_data'], true);
    //                 if($promo_data['type'] == 'P') {
    //                     $disc_value = $promo_data['discount'];
    //                     $disc_price = round(($product->price * $promo_data['discount']) / 100);
    //                 } else {
    //                     $disc_value = NULL;
    //                     $disc_price = $promo_data['discount'];
    //                 }
    //                 $end_price= $product->price - $disc_price;
    //                 $today = date('Y-m-d');
    //                 $today=date('Y-m-d', strtotime($today));
    //                 $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
    //                 $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
        
    //                 if (($today >= $DateBegin) && ($today <= $DateEnd)){
    //                     $price = $end_price;
    //                     $inPromo = true;
    //                     $promo_type = $promo_data['type'];
    //                 } else {
    //                     $price = $product->price;
    //                     $inPromo = false;
    //                     $promo_type = false;
    //                 }
    //             } else {
    //                 $price = $product->price;
    //                 $inPromo = false;
    //                 $promo_type = false;
    //                 $disc_value = null;
    //                 $disc_price = null;
    //             }
    //             $merchant = $this->session->userdata('list_merchant')[0];
    //             $price_level = $this->product->price_in_level($product->product_id, $merchant['group'], ($item ? $item['quantity'] + $qty : $qty));
    //             if($price_level) {
    //                 $old_price = $price_level;
    //                 if($product->promo == 1) {
    //                     $price = $price_level - $disc_price;
    //                 } else {
    //                     $price = $price_level;
    //                 }
    //                 $price_level = 1;
    //             } else {
    //                 $price_group = $this->main->get('product_price', array('product' => $product->product_id, 'merchant_group' => $merchant['group']));
    //                 if ($price_group) {
    //                     $old_price = $price_group->price;
    //                     if($item['in_promo'] == 1){
    //                         if($promo_data['type'] == 'P') {
    //                             $disc_price_group = round(($price_group->price * $promo_data['discount']) / 100);
    //                         } else {
    //                             $disc_price_group = $promo_data['discount'];
    //                         }
    //                         $end_price_group= $price_group->price - $disc_price_group;
                
    //                         if (($today >= $DateBegin) && ($today <= $DateEnd)){
    //                             $price = $end_price_group;
    //                         } else {
    //                             $price = $price_group->price;
    //                         }
    //                     } else {
    //                         $price = $price_group->price;
    //                     }
    //                 } else {
    //                     $price_level = 0;
    //                 }
    //             }
    //             $item['quantity'] += $qty;
    //             $item['price'] = $price;
    //             $item['total_weight'] += $total_weight;
    //             $item['grand_total'] = $price * $item['quantity'];
    //             $item['price_level'] = $price_level;
    //             $item['in_promo'] = $inPromo;
    //             $item['promo_type'] = $promo_type;
    //             $item['disc_value'] = $disc_value;
    //             $item['disc_price'] = $disc_price;
    //             $item['old_price'] = $old_price;

    //             $this->db->where('id', $item['id']);
    //             $this->db->update('cart', $item);

    //             $output['items'] = $this->cart_model->get_cart($id_cart);
    //             $output['status'] = 'success';
    //             $output['message'] = 'Keranjang Berhasil Diupdate!';

    //         } else {
    //             $output['status'] = 'error';
    //             $output['message'] = 'Barang Kosong!';
    //         }
    //     } else {
    //         $output['status'] = 'error';
    //         $output['message'] = 'Terjadi Kesalahan!';
    //     }
    //     json_encode($output);
    // }

    public function update($rowid, $qty) {
        // $this->load->model('checkout_model', 'checkout');
        if ($qty > 0) {
            $item = $this->cart->get_item($rowid);
            
            list($product_id, $store_id) = explode('-', $item['id']);

            $item['store'] = $store_id;

            $product = $this->product->get_detail_products($product_id, $item['store']);
            if ($product) {
                $old_price = $product->price;
                if($product->promo == 1){
                    $promo_data = json_decode($product->promo_data,true);
                    if($promo_data['type'] == 'p') {
                        $disc_value = $promo_data['discount'];
                        $disc_price = round(($product->price * $promo_data['discount']) / 100);
                    } else {
                        $disc_value = NULL;
                        $disc_price = $promo_data['discount'];
                    }
                    $end_price= $product->price - $disc_price;
                    $today = date('Y-m-d');
                    $today=date('Y-m-d', strtotime($today));
                    $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                    $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                    if (($today >= $DateBegin) && ($today <= $DateEnd)){
                        $price = $end_price;
                        $inPromo = true;
                    } else {
                        $price = $product->price;
                        $inPromo = false;
                    }
                } else {
                    $price = $product->price;
                    $inPromo = false;
                }
                $item['price'] = $price;
                $item['qty'] = $qty;
                $item['rowid'] = $rowid;
                if ($product->merchant == 0) {
                    $merchant = $this->session->userdata('list_merchant')[0];
                    $price_level = $this->product->price_in_level($product->product_id, $merchant['group'], $qty);
                    if ($price_level) { //jika price level ada, maka produk tsb masuk ke harga grosir
                        $item['price'] = $price_level;
                        $item['price_level'] = 1;
                    } else {
                        $price_group = $this->main->get('product_price', array('product' => $product->product_id, 'merchant_group' => $merchant['group']));
                        if ($price_group) {
                            $old_price = $price_group->price;
                            if($product->promo == 1){
                                if($promo_data['type'] == 'p') {
                                    $disc_price_group = round(($price_group->price * $promo_data['discount']) / 100);
                                } else {
                                    $disc_price_group = $promo_data['discount'];
                                }
                                $end_price_group= $price_group->price - $disc_price_group;
                                if (($today >= $DateBegin) && ($today <= $DateEnd)){
                                    $price = $end_price_group;
                                } else {
                                    $price = $price_group->price;
                                }
                            } else {
                                $price = $price_group->price;
                            }
                            $item['price'] = $price;
                        }
                    }
                }
                if ($item['options']) {
                    foreach ($item['options'] as $option) {
                        $item['price'] += $option['price'];
                    }
                }
                
                // $cek_stock = $this->checkout->get_merchant_stock($product_id, $store_id, $qty);
    
                // if ($cek_stock->num_rows() > 0) {
                //     # code...
                //     $this->cart->update($item);
                //     $return = array('message' => 'Stok barang tersedia! ', 'status' => 'success');
                // } else {
                    //     # code...
                    //     $return = array('message' => 'Stok barang tidak tersedia! ', 'status' => 'failed','data' => $item);
                    // }
                }
                
                $this->cart->update($item);
            }
        // echo json_encode($return);
        
        redirect('cart');
    }

    public function delete($rowid) {
        $this->db->where('id', $rowid);
        $this->db->delete('cart');
        redirect('cart');
    }

    public function destroy() {
        $this->cart->destroy();
        redirect('cart');
    }

}
