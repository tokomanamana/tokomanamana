<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('breadcrumb');
        $this->load->library('pagination');
        $this->load->helper('text');
    }

    public function view($id) {
        $data = $this->main->get('blog_categories', array('id' => $id));

        $config['base_url'] = current_url();
        $config['total_rows'] = $this->main->gets('blogs', array('status' => 1))->num_rows();
        $config['per_page'] = ($this->input->get('show')) ? $this->input->get('show') : 8;
        $this->pagination->initialize($config);
        $start = ($this->input->get('page')) ? ($this->input->get('page') - 1) * $config['per_page'] : 0;
        $this->data['pagination'] = $this->pagination->create_links();

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Blogs', site_url('blogs'));
        $this->breadcrumb->add($data->name, site_url('blogs/categories/view/' . $data->id));
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['data'] = $data;
        $this->data['posts'] = $this->main->gets_paging('blogs', $start, $config['per_page'], array('status' => 1), 'date_added DESC');
        $this->data['categories'] = $this->main->gets('blog_categories', array(), 'name asc');

        $this->template->_init();
        $this->output->set_title('Blogs - ' . settings('meta_title'));
        $this->load->view('categories', $this->data);
    }

}
