<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Posts extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('breadcrumb');
    }

    public function view($id) {
        $data = $this->main->get('blogs', array('id' => $id));
        if (!$data)
            show_404();

        $category = $this->main->get('blog_categories', array('id' => $data->category));
        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Blogs', site_url('blogs'));
        $this->breadcrumb->add($category->name, seo_url('blogs/categories/view/' . $category->id));
        $this->breadcrumb->add($data->title, seo_url('blogs/posts/view/' . $data->id));

        $this->data['data'] = $data;
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['posts'] = $this->main->gets_paging('blogs', 0, 6, array('status' => 1), 'date_added DESC');
        $this->data['blog_categories'] = $this->main->gets('blog_categories', array(), 'name asc');

        $this->template->_init();
        $this->output->set_title((($data->meta_title) ? $data->meta_title : $data->title) . ' - ' . settings('meta_title'));
        $this->load->view('post', $this->data);
    }

}
