<section class="collection-heading heading-content ">
    <div class="container">
        <div class="row">
            <div class="collection-wrapper">
                <div class="collection-title"><span>Blogs</span></div>
                <div class="breadcrumb-group">
                    <div class="breadcrumb clearfix">
                        <?php echo $breadcrumb; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="article-content">
    <div class="article-wrapper">
        <div class="container">
            <div class="row">
                <div id="shopify-section-article-template" class="shopify-section">
                    <div class="article-inner" itemscope="" itemtype="http://schema.org/NewsArticle">
                        <div id="article">
                            <div class="col-sm-3 sidebar">
                                <div class="sidebar-block blogs-recent">
                                    <h3 class="sidebar-title"><span>Recent posts</span></h3>
                                    <div class="sidebar-content recent-article">
                                        <div class="ra-item-inner">
                                            <?php if ($posts) foreach($posts->result() as $post) { ?>
                                                <div class="article clearfix">
                                                    <div class="article-image">
                                                        <a href="<?php echo seo_url('blogs/posts/view/'.$post->id); ?>"><img src="<?php echo get_image($post->image); ?>" alt=""></a>
                                                    </div>
                                                    <div class="articleinfo_group">
                                                        <div class="article-title">
                                                            <h2 class="article-name"><a href="<?php echo seo_url('blogs/posts/view/'.$post->id); ?>"><?php echo $post->title; ?></a></h2>
                                                        </div>
                                                        <ul class="article-info list-inline">
                                                            <li class="article-date"><?php echo get_date($post->date_added); ?></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="sidebar-block blog-category">
                                    <h3 class="sidebar-title"><span>Category</span></h3>
                                    <div class="sidebar-content">
                                        <ul class="category">
                                            <?php if ($categories) foreach ($categories->result() as $category) { ?>
                                                    <li class="nav-item active">
                                                        <a href="<?php echo seo_url('blogs/categories/view/' . $category->id); ?>"><?php echo $category->name; ?></a>
                                                    </li>
                                                <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-9 article">
                                <div class="article-body clearfix" style="margin-bottom: 20px;">
                                    <div class="group-blog-top">
                                        <ul class="article-info list-inline">
                                            <li class="article-date"><?php echo get_date_time($data->date_added); ?></li>
                                        </ul>
                                        <div class="article-title">
                                            <h1 class="article-name" itemprop="headline"><?php echo $data->title; ?></h1>
                                        </div>
                                        <div class="top-banner article_banner_show article-image" itemprop="image" itemscope="" itemtype="https://schema.org/ImageObject">
                                            <img src="<?php echo get_image($data->image); ?>" alt="">
                                        </div>
                                    </div>
                                    <div class="articleinfo_group">
                                        <div  class="article-content" itemprop="description">
                                            <?php echo htmlspecialchars_decode($data->content); ?>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
