<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Blogs_model extends CI_Model {

    function getProducts($category, $merchant, $merchant_group, $sort = 'popular', $limit = 20, $start = 0) {
        $query_order = 'ORDER BY ';
        switch ($sort) {
            default:
            case 'popular':
                $query_order .= 'p.viewed DESC';
                break;
            case 'newest' :
                $query_order .= 'p.date_added DESC';
                break;
            case 'lowprice' :
                $query_order .= 'p.price ASC';
                break;
            case 'highprice' :
                $query_order .= 'p.price DESC';
                break;
            case 'nameAZ' :
                $query_order .= 'p.name ASC';
                break;
            case 'nameZA' :
                $query_order .= 'p.name DESC';
                break;
        }
//        $this->db->select("p.id, p.price, p.price_old, p.discount, pi.image, p.name, p.short_description, p.merchant, (SELECT COUNT(id) FROM product_merchant pm WHERE pm.product = p.id) total_merchant", FALSE)
//                ->join('product_image pi', 'pi.product = p.id AND pi.primary = 1', 'left')
//                ->where("p.category IN (SELECT category FROM category_path WHERE path = $category)")
//                ->where('p.status', 1);
        $query_limit = '';
        if ($limit) {
            $query_limit = 'LIMIT ' . $start . ',' . $limit;
        }
//            $this->db->limit($limit, $start);
//        $query = $this->db->get('products p');
//        return $query;
        $merchant = implode(',', $merchant);
        return $this->db->query("SELECT * FROM (
                    SELECT p.id, p.merchant, p.name, p.short_description, p.code, p.category, p.brand, pp.price, pi.image, pp.id ppid, p.viewed, p.date_added
                    FROM products p
                    LEFT JOIN product_price pp ON pp.product = p.id AND pp.merchant_group = $merchant_group
                    LEFT JOIN product_image pi ON pi.product = p.id AND pi.primary = 1
                    WHERE p.merchant = 0 AND p.status = 1
                    UNION
                    SELECT p.id, p.merchant, p.name, p.short_description, p.code, p.category, p.brand, p.price, pi.image, 0, p.viewed, p.date_added
                    FROM products p
                    LEFT JOIN product_image pi ON pi.product = p.id AND pi.primary = 1
                    WHERE merchant IN ($merchant) AND p.status = 1
                    ) p
                 WHERE p.category IN (SELECT category FROM category_path WHERE path = $category) $query_order $query_limit");
    }

//    function getProducts($category, $sort = 'popular', $limit = 20, $start = 0) {
//        switch ($sort) {
//            default:
//            case 'popular':
//                $this->db->order_by('p.viewed', 'DESC');
//                break;
//            case 'newest' :
//                $this->db->order_by('p.date_added', 'DESC');
//                break;
//            case 'lowprice' :
//                $this->db->order_by('p.price', 'ASC');
//                break;
//            case 'highprice' :
//                $this->db->order_by('p.price', 'DESC');
//                break;
//            case 'nameAZ' :
//                $this->db->order_by('p.name', 'ASC');
//                break;
//            case 'nameZA' :
//                $this->db->order_by('p.name', 'DESC');
//                break;
//        }
//        $this->db->select("p.id, p.price, p.price_old, p.discount, pi.image, p.name, p.short_description, p.merchant, (SELECT COUNT(id) FROM product_merchant pm WHERE pm.product = p.id) total_merchant", FALSE)
//                ->join('product_image pi', 'pi.product = p.id AND pi.primary = 1', 'left')
//                ->where("p.category IN (SELECT category FROM category_path WHERE path = $category)")
//                ->where('p.status', 1);
//        if ($limit)
//            $this->db->limit($limit, $start);
//        $query = $this->db->get('products p');
//        return $query;
//    }

    function get_category_path($category) {
        $this->db->select("c.id, c.name", FALSE)
                ->join('categories c', 'pc.path = c.id', 'left')
                ->where('category', $category)
                ->order_by('level', 'ASC');
        $query = $this->db->get('category_path pc');
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

}
