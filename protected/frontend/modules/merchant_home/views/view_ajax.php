
  <?php if ($products->num_rows() > 0) : ?>
    <input type="hidden" id="hidden-start" name="hidden-start" value="<?php echo $start?>">
    <div class="etalase-title"><span><?php echo $etalase_label; ?></span></div>
    <div class="products" style="margin-left: 5px;">
      <?php  foreach ($products->result() as $key => $product) { 
        if ($this->ion_auth->logged_in()) {
          $id_customer = $this->data['user']->id;
          $get_wishlist = $this->main->get('wishlist', ['product' => $product->product_id, 'branch_id' => $product->store_id, 'customer' => $id_customer]);
          if ($get_wishlist) {
            $product_wishlist = true;
          } else {
            $product_wishlist = false;
          }
        } else {
          $product_wishlist = false;
        }

        $sale_on = FALSE;

        if($product->promo == 1) {
                            // if ($product->merchant == 0) {
                            //   $merchant = $this->session->userdata('list_merchant')[0];
                            //   $price_group = $this->main->get('product_price', array('product' => $product->product_id, 'merchant_group' => $merchant['group']));
                            //   if ($price_group) {
                            //     $product->price = $price_group->price;
                            //   }
                            // }
          $promo_data = json_decode($product->promo_data,true);
          if($promo_data['type'] == 'P') {
            $disc_price = round(($product->price * $promo_data['discount']) / 100);
            $label_disc = $promo_data['discount'].'% off';
          } else {
            $disc_price = $promo_data['discount'];
            $label_disc = 'SALE';
          }
          $end_price= intval($product->price) - intval($disc_price);
          $today = date('Y-m-d');
          $today=date('Y-m-d', strtotime($today));
          $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
          $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

          if (($today >= $DateBegin) && ($today <= $DateEnd)){
            $sale_on = TRUE;
          }
          else {
            $sale_on = false;
          }
        }    
        ?>
        <div class="product-item col-sm-3" style="border: none;">
          <div class="product" onmouseover="change_store(<?= $key ?>, 'over')" onmouseout="change_store(<?= $key ?>, 'out')">
            <?php if($sale_on) : ?>
              <span class="product-discount-label" ><?php echo $label_disc;?></span>
            <?php endif; ?>
            <?php if ($this->ion_auth->logged_in()) : ?>
              <a href="javascript:void(0);" class="btn-add-to-wishlist" style="<?= ($product_wishlist) ? 'color:#d9534f;' : 'color:#BBB'  ?>" onclick="add_wishlist(this)" data-product="<?= encode($product->product_id) ?>" data-store="<?= encode($product->store_id) ?>" data-customer="<?= encode($id_customer) ?>">
                <i class="fa fa-heart" aria-hidden="true"></i>
              </a>
              <?php else : ?>
                <a href="javascript:void(0);" class="btn-add-to-wishlist" style="color:#BBB" data-toggle="modal" data-target="#modal-guest" data-back="<?= site_url() ?>">
                  <i class="fa fa-heart" aria-hidden="true"></i>
                </a>
              <?php endif; ?>

              <div class="row-left">
                <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="hoverBorder container_item" style="min-height: 180px;">
                  <img src="
                   <?php 
                        if($product->image_thumb)echo site_url('files/image_thumbs/'. $product->image_thumb);
                        else{ 
                            if($product->image)echo site_url('files/images/' . $product->image);
                            else echo site_url('assets/frontend/images/noimage.jpg');
                        } 
                    ?>"
                  class="img-responsive img-middle" alt="<?php echo $product->name; ?>">
                  <!-- <img src="<?php// echo ($product->image) ? site_url('files/images/' . $product->image) : site_url('assets/frontend/images/noimage.jpg'); ?>" class="img-responsive" alt="<?php// echo $product->name; ?>"> -->
                </a>


              </div>

              <div class="garis-tengah"></div>
              <div class="tombol-cart">
                <?php if ($this->ion_auth->logged_in()) : ?>
                  <?php if ($this->main->gets('product_option', array('product' => $product->product_id))) : ?>
                          <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="icon-cart">
                              <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                          </a>
                      <?php else : ?>
                          <button type="button" data-id="<?php echo encode($product->product_id); ?>" data-storeid="<?= encode($product->store_id) ?>" onclick="addtoCart($(this).data('id'), $(this).data('storeid'))" class="btn-add-to-cart icon-cart">
                              <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                          </button>
                      <?php endif; ?>
                  <?php else : ?>
                      <button type="button" class="icon-cart" data-toggle="modal" data-target="#modal-guest">
                          <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                      </button>
                  <?php endif; ?>
              </div>

                  <div class="row-right animMix">
                    <div class="grid-mode">
                      <div class="product-title">
                        <a class="title-5" style="font-weight: bold; text-transform: uppercase;width: 75%;" href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>"><?php echo $product->name; ?></a>
                      </div>

                      <div class="product-price">
                        <?php if($sale_on) {?>
                          <span class="money" style="text-decoration: line-through;color:#8b8f8b"><?php echo rupiah($product->price); ?></span>
                          <span class="money" style="color: #a7c22a"><?php echo rupiah($end_price); ?></span>
                        <?php } else { ?>
                          <span class="price_sale" style="color: #a7c22a"><span class="money"><?php echo rupiah($product->price); ?></span></span>
                        <?php } ?>
                      </div>

                      <?php
                      $merchant = $this->main->get('merchants', ['id' => $product->store_id]);
                      $city = $this->main->get('cities', ['id' => $merchant->city]);
                      ?>
                      <div style="height: 15px;overflow: hidden;">
                        <span id="text-ellipsis-city-<?= $key ?>" style="display: block;"><?= ($city->type == 'Kabupaten') ? 'Kab. ' . $city->name : $city->name ?></span>
                        <span id="text-ellipsis-store-<?= $key ?>" style="display: block;"><?= $merchant->name ?></span>
                      </div>

                      <div class="rating-star">
                        <span class="spr-badge" data-rating="0.0">
                          <span class="spr-starrating spr-badge-starrating">
                            <?php
                            $rating = $this->merchant_home->get_rating ($product->product_id, $product->store_id);

                            if (round($rating->rating) > 0) {
                              for ($i = 1; $i <= round($rating->rating); $i++) {
                                ?>
                                <i class="spr-icon spr-icon-star"></i>
                                <?php
                              }
                              for ($i = round($rating->rating) + 1; $i <= 5; $i++) {
                                ?>
                                <i class="spr-icon spr-icon-star-empty"></i>
                                <?php
                              }
                            }
                            ?>
                          </span>
                        </span>
                      </div>
                      <div class="product-footer">
                        &nbsp;
                        <i class="fa fa-eye" aria-hidden="true"></i> <?= $product->viewed; ?>
                      </div>
                      <?php
                      $preorder = $product->preorder;
                      $free_ongkir = $product->free_ongkir;

                      if ($preorder == 1) {
                        echo '<div class="pre-order-label">Preorder</div>';
                      }
                      if ($free_ongkir == 1) {
                        echo '<div class="free-ongkir-label">Free Ongkir</div>';
                      } ?>


                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>

        <div class="collection-bottom-toolbar">
          <div class="product-pagination">
            <div class="pagination_group">
              <?php if($prev_button){ ?>
                <button id='prev_button'><i class="fa fa-angle-left" aria-hidden="true"></i></button>
              <?php } ?>
               <?php if($next_button){ ?>
              <button id='next_button'><i class="fa fa-angle-right" aria-hidden="true"></i></button>
              <?php } ?>
            </div>
          </div>
        </div>


        <?php  else: ?>
          <p style="text-align: center;">Tidak ada produk.</p>

        <?php endif; ?>


      
