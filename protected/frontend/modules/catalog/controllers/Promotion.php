<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Promotion extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->lang->load('catalog', settings('language'));
        $this->load->model('promo_model', 'promo');
        $this->load->model('products');
        $this->load->library('pagination');
        $this->load->library('breadcrumb');
    }

    // public function index() {
    //     $config['base_url'] = site_url('promotion');

    //     //pagination
    //     $config['total_rows'] = $this->main->gets('products', array('promo' => 1))->num_rows();
    //     $config['per_page'] = ($this->input->get('show')) ? $this->input->get('show') : 20;
    //     $this->pagination->initialize($config);
    //     $start = $this->input->get('page') ? ($this->input->get('page') - 1) * $config['per_page'] : 0;

    //     $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
    //     $this->breadcrumb->add('Promo Bulan Ini', site_url('promotion'));
        
    //     $this->data['products'] = $this->promo->get_all_promo((($this->input->get('sort')) ? $this->input->get('sort') : 'random'), $config['per_page'], $start);
    //     $this->data['pagination'] = $this->pagination->create_links();
    //     $this->data['breadcrumb'] = $this->breadcrumb->output();
    //     $this->data['meta_description'] = 'Promo Bulan Ini di Tokomanamana.com';
    //     $this->data['total_products'] = $config['total_rows'];
    //     $this->data['start'] = $start + 1;
    //     $this->data['to'] = ($this->data['total_products'] < ($this->data['start'] * $config['per_page'])) ? $this->data['total_products'] : ($this->data['start'] * $config['per_page']);
    //     $this->output->set_title('Promo Bulan Ini - ' . settings('meta_title'));
    //     $this->template->_init();

    //     $this->load->view('promotion', $this->data);
    // }

    // NEW METHOD
    public function index() {
        $config['base_url'] = site_url('promotion');

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Promo Hari Ini', site_url('promotion'));

        $product_num = 0;
        $products = $this->promo->get_all_promo(24);

        foreach($products->result() as $product) {
            $promo_data = json_decode($product->promo_data, true);
            $today = date('Y-m-d');
            $today = date('Y-m-d', strtotime($today));
            $date_begin = date('Y-m-d', strtotime($promo_data['date_start']));
            $date_end = date('Y-m-d', strtotime($promo_data['date_end']));
            if(($today >= $date_begin) && ($today <= $date_end)) {
                $product_num++;
            } else {
                continue;
            }
        }

        $this->data['products_num'] = $product_num;
        
        $this->data['products'] = $this->promo->get_all_promo(24);
        
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['meta_description'] = 'Promo Bulan Ini di Tokomanamana.com';
        $this->output->set_title('Promo Hari Ini - ' . settings('meta_title'));
        $this->template->_init();

        $this->load->view('promotion', $this->data);
    }
    
    public function load_more_promotion() {
        $last_modified = $this->input->post('date_modified');
        $last_key = $this->input->post('last_key');

        $where = 'products.viewed < ' . "'" . $last_modified . "'";
        $limit = 12;

        $product_num = 0;
        $products = $this->promo->get_more_all_promo($where, $limit);

        foreach($products->result() as $product) {
            $promo_data = json_decode($product->promo_data, true);
            $today = date('Y-m-d');
            $today = date('Y-m-d', strtotime($today));
            $date_begin = date('Y-m-d', strtotime($promo_data['date_start']));
            $date_end = date('Y-m-d', strtotime($promo_data['date_end']));
            if(($today >= $date_begin) && ($today <= $date_end)) {
                $product_num++;
            } else {
                continue;
            }
        }

        $this->data['products'] = $this->promo->get_more_all_promo($where, $limit);
        $this->data['products_num'] = $product_num;
        $this->data['products_limit'] = 12;
        $this->data['key'] = $last_key;

        $this->load->view('more_promotions', $this->data, false);
    }

}
