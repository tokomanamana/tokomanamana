<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Promo_model extends CI_Model {

    // function get_all_promo ($sort = 'random', $limit = NULL, $start = NULL) {
    //     $this->db->select('products.name,
    //                         products.id AS product_id,
    //                         products.price,
    //                         products.discount,
    //                         products.promo,
    //                         products.promo_data,
    //                         products.merchant,
    //                         products.viewed,
    //                         products.free_ongkir,
    //                         products.preorder,
    //                         merchants.name AS store_name,
    //                         merchants.id AS store_id,
    //                         product_image.image')
    //             ->join('products', 'products_principal_stock.product_id = products.id', 'LEFT')
    //             ->join('merchants', 'products_principal_stock.branch_id = merchants.id', 'LEFT')
    //             ->join('product_image', 'product_image.product = products.id AND product_image.primary = 1', 'LEFT')
    //             ->where('products.promo', 1)
    //             ->where('products.status != ', 0)
    //             ->where('products_principal_stock.quantity > ', 0);

    //     switch ($sort) {
    //         default:
    //         case 'random':
    //             $this->db->order_by('RAND()');
    //             break;
    //         case 'popular' :
    //             $this->db->order_by('products.viewed', 'DESC');
    //             break;
    //         case 'newest' :
    //             $this->db->order_by('products.date_added', 'DESC');
    //             break;
    //         case 'lowprice' :
    //             $this->db->order_by('products.price', 'ASC');
    //             break;
    //         case 'highprice' :
    //             $this->db->order_by('products.price', 'DESC');
    //             break;
    //         case 'nameAZ' :
    //             $this->db->order_by('products.name', 'ASC');
    //             break;
    //         case 'nameZA' :
    //             $this->db->order_by('products.name', 'DESC');
    //             break;
    //     }

    //     if ($limit) {
    //         $this->db->limit($limit, $start);
    //     }
        
    //     $query = $this->db->get('products_principal_stock');

    //     return ($query->num_rows() > 0) ? $query : FALSE;
    // }

    // NEW METHOD
    function get_all_promo ($limit = null,$category=null) {
        $query = $this->db->query("
                (SELECT DISTINCT products.name AS name,
                        products.id AS product_id,
                        (CASE WHEN products_principal_stock.price != 0 THEN products_principal_stock.price ELSE (CASE WHEN products.variation = 1 THEN (CASE WHEN products.price_grosir = 0 OR products.price_reseller = 0 THEN product_option.price ELSE products.price END) ELSE products.price END) END) as price,
                        products.discount,
                        products.preorder,
                        products.promo,
                        (CASE WHEN products_principal_stock.promo_data != '' THEN products_principal_stock.promo_data ELSE products.promo_data END) as promo_data,
                        products.merchant,
                        product_image.image,
                        merchants.name AS store_name,
                        products_principal_stock.branch_id AS store_id,
                        products.store_type AS store_type,
                        products.viewed AS viewed,
                        products.free_ongkir,
                        products.date_modified AS date_modified,
                        products.variation,
                        products.price_grosir,
                        products_principal_stock.id_option,
                        products.price_reseller,
                        (CASE WHEN products.min_buy THEN products.min_buy ELSE 1 END) as min_buy,
                        (CASE WHEN products.variation = 1 THEN product_option.default ELSE 0 END) as product_option_default

                        FROM products_principal_stock
                        LEFT JOIN products ON products.id = products_principal_stock.product_id
                        LEFT JOIN merchants ON merchants.id = products_principal_stock.branch_id
                        LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                        LEFT JOIN principles ON principles.id = products.store_id
                        LEFT JOIN product_option ON CASE WHEN products.variation = 1 THEN product_option.id = products_principal_stock.id_option AND product_option.default = 1 ELSE 0 END
                        WHERE products.status != 0
                        AND (principles.status = 1 AND merchants.status = 1)
                        AND (products.promo = 1 OR products_principal_stock.promo_data != '')
                        AND products.store_type = 'principal'
                        AND (products_principal_stock.quantity > 0 OR products.preorder = 1 OR products.variation = 1)
                        GROUP BY products_principal_stock.product_id, products_principal_stock.branch_id HAVING (CASE WHEN products.variation = 1 THEN product_option_default = 1 ELSE product_option_default = 0 END))
                UNION
                (SELECT DISTINCT products.name AS name,
                    products.id AS product_id,
                    (CASE WHEN products.variation = 1 THEN product_option.price ELSE products.price END) as price,
                    products.discount,
                    products.preorder,
                    products.promo,
                    products.promo_data,
                    products.merchant,
                    product_image.image,
                    merchants.name AS store_name,
                    products.store_id AS store_id,
                    products.store_type AS store_type,
                    products.viewed AS viewed,
                    products.free_ongkir,
                    products.date_modified AS date_modified,
                    products.variation,
                    products.price_grosir,
                    product_option.id as id_option,
                    products.price_reseller,
                    (CASE WHEN products.min_buy THEN products.min_buy ELSE 1 END) as min_buy,
                    (CASE WHEN products.variation = 1 THEN product_option.default ELSE 0 END) as product_option_default
                    FROM products
                    LEFT JOIN merchants ON merchants.id = products.store_id
                    LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                    LEFT JOIN product_option ON CASE WHEN products.variation = 1 THEN product_option.product = products.id AND product_option.default = 1 ELSE 0 END
                    WHERE products.status != 0
                    AND merchants.status = 1
                    AND products.promo = 1
                    AND products.store_type = 'merchant'
                    AND (products.quantity > 0 OR products.preorder = 1 OR products.variation = 1))
                    
                    ORDER BY viewed DESC

                    LIMIT $limit
        ");
        return $query;
    }

    function get_more_all_promo($where, $limit = null,$category=null) {
        if($limit) {
            $query_limit = "LIMIT $limit";
        } else {
            $query_limit = NULL;
        }

        $query = $this->db->query("
        (SELECT DISTINCT products.name AS name,
                        products.id AS product_id,
                        (CASE WHEN products_principal_stock.price != 0 THEN products_principal_stock.price ELSE (CASE WHEN products.variation = 1 THEN (CASE WHEN products.price_grosir = 0 OR products.price_reseller = 0 THEN product_option.price ELSE products.price END) ELSE products.price END) END) as price,
                        products.discount,
                        products.preorder,
                        products.promo,
                        (CASE WHEN products_principal_stock.promo_data != '' THEN products_principal_stock.promo_data ELSE products.promo_data END) as promo_data,
                        products.merchant,
                        product_image.image,
                        merchants.name AS store_name,
                        products_principal_stock.branch_id AS store_id,
                        products.store_type AS store_type,
                        products.viewed AS viewed,
                        products.free_ongkir,
                        products.date_modified AS date_modified,
                        products.variation,
                        products.price_grosir,
                        products_principal_stock.id_option,
                        products.price_reseller,
                        (CASE WHEN products.min_buy THEN products.min_buy ELSE 1 END) as min_buy,
                        (CASE WHEN products.variation = 1 THEN product_option.default ELSE 0 END) as product_option_default

                        FROM products_principal_stock
                        LEFT JOIN products ON products.id = products_principal_stock.product_id
                        LEFT JOIN merchants ON merchants.id = products_principal_stock.branch_id
                        LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                        LEFT JOIN principles ON principles.id = products.store_id
                        LEFT JOIN product_option ON CASE WHEN products.variation = 1 THEN product_option.id = products_principal_stock.id_option AND product_option.default = 1 ELSE 0 END
                        WHERE products.status != 0
                        AND (principles.status = 1 AND merchants.status = 1)
                        AND (products.promo = 1 OR products_principal_stock.promo_data != '')
                        AND products.store_type = 'principal'
                        AND (products_principal_stock.quantity > 0 OR products.preorder = 1 OR products.variation = 1)
                        AND $where
                        GROUP BY products_principal_stock.product_id, products_principal_stock.branch_id HAVING (CASE WHEN products.variation = 1 THEN product_option_default = 1 ELSE product_option_default = 0 END))
                UNION
                (SELECT DISTINCT products.name AS name,
                    products.id AS product_id,
                    (CASE WHEN products.variation = 1 THEN product_option.price ELSE products.price END) as price,
                    products.discount,
                    products.preorder,
                    products.promo,
                    products.promo_data,
                    products.merchant,
                    product_image.image,
                    merchants.name AS store_name,
                    products.store_id AS store_id,
                    products.store_type AS store_type,
                    products.viewed AS viewed,
                    products.free_ongkir,
                    products.date_modified AS date_modified,
                    products.variation,
                    products.price_grosir,
                    product_option.id as id_option,
                    products.price_reseller,
                    (CASE WHEN products.min_buy THEN products.min_buy ELSE 1 END) as min_buy,
                    (CASE WHEN products.variation = 1 THEN product_option.default ELSE 0 END) as product_option_default
                    FROM products
                    LEFT JOIN merchants ON merchants.id = products.store_id
                    LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                    LEFT JOIN product_option ON CASE WHEN products.variation = 1 THEN product_option.product = products.id AND product_option.default = 1 ELSE 0 END
                    WHERE products.status != 0
                    AND merchants.status = 1
                    AND products.promo = 1
                    AND products.store_type = 'merchant'
                    AND (products.quantity > 0 OR products.preorder = 1 OR products.variation = 1)
                    AND $where)
                    
                    ORDER BY viewed DESC

            $query_limit
        ");

        return $query;
    }

    function get_rating ($product, $store) {
        $this->db->select('IFNULL((SUM(rating_speed) + SUM(rating_service) + SUM(rating_accuracy)) / (COUNT(id) * 3), 0) AS rating,
                            IFNULL(COUNT(id), 0) AS review')
                ->where('product', $product)
                ->where('merchant', $store)
                ->where('status', 1);

        $query = $this->db->get('product_review');
        return ($query->result() > 0) ? $query->row() : FALSE;
    }
}