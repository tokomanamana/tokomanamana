<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Recomendation_model extends CI_Model {

    // function get_recomended_keyword($id, $ip) {
    //     if ($id) {
    //         $auth = 'product_last_review.user_id = ' . $id;
    //     }
    //     elseif ($ip) {
    //         $auth = 'product_last_review.ip_address = "' . $ip . '"';
    //     }
    //     $query = $this->db->query("SELECT DISTINCT SUBSTR(products.name, 1, INSTR(products.name, ' ')-1) AS keyword
    //                                 FROM product_last_review
    //                                 LEFT JOIN products_principal_stock
    //                                 ON product_last_review.product_id = products_principal_stock.id
    //                                 LEFT JOIN products
    //                                 ON products_principal_stock.product_id = products.id
    //                                 WHERE $auth
    //                                 AND products.status = 1
    //                                 AND products_principal_stock.quantity > 0
    //                                 ORDER BY product_last_review.modified_time");
        
    //     return ($query->num_rows() > 0) ? $query : FALSE;
    // }

    // NEW METHOD
    function get_recomended_keyword ($id, $ip) {
        if ($id) {
            $auth = 'product_last_review.user_id = ' . $id;
        }
        elseif ($ip) {
            $auth = 'product_last_review.ip_address = "' . $ip . '"';
        }
        $query = $this->db->query("(SELECT DISTINCT SUBSTR(products.name, 1, INSTR(products.name, ' ')-1) AS keyword, product_last_review.modified_time
                                    FROM product_last_review
                                    LEFT JOIN products_principal_stock
                                    ON product_last_review.product_id = products_principal_stock.id
                                    LEFT JOIN products
                                    ON products_principal_stock.product_id = products.id
                                    WHERE $auth
                                    AND products.status = 1)

                                    UNION

                                    (SELECT DISTINCT SUBSTR(products.name, 1, INSTR(products.name, ' ')-1) AS keyword, product_last_review.modified_time
                                    FROM product_last_review
                                    LEFT JOIN products ON product_last_review.product_id = products.id
                                    WHERE $auth
                                    AND products.status = 1)
                                    
                                    ORDER BY modified_time");
        
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    // function get_recomendations ($string, $sort = 'random', $limit = NULL, $start = NULL) {
    //     $query_order = 'ORDER BY ';
    //     switch ($sort) {
    //         default:
    //         case 'random':
    //             $query_order .= 'RAND()';
    //             break;
    //         case 'popular':
    //             $query_order .= 'products.viewed DESC';
    //             break;
    //         case 'newest' :
    //             $query_order .= 'products.date_added DESC';
    //             break;
    //         case 'lowprice' :
    //             $query_order .= 'products.price ASC';
    //             break;
    //         case 'highprice' :
    //             $query_order .= 'products.price DESC';
    //             break;
    //         case 'nameAZ' :
    //             $query_order .= 'products.name ASC';
    //             break;
    //         case 'nameZA' :
    //             $query_order .= 'products.name DESC';
    //             break;
    //     }

    //     $query_limit = '';
    //     if ($limit) {
    //         $query_limit = 'LIMIT ' . $start . ', ' . $limit;
    //     }

    //     $query = $this->db->query("SELECT DISTINCT products.name,
    //                                                 products.id AS product_id,
    //                                                 products.price,
    //                                                 products.discount,
    //                                                 products.preorder,
    //                                                 products.promo,
    //                                                 products.promo_data,
    //                                                 products.merchant,
    //                                                 product_image.image,
    //                                                 merchants.name AS merchant_name,
    //                                                 merchants.id AS store_id
    //                                 FROM products LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
    //                                 LEFT JOIN products_principal_stock ON products.id = products_principal_stock.product_id
    //                                 LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id
    //                                 WHERE products.status = 1
    //                                 AND products_principal_stock.quantity > 0
    //                                 AND products.name REGEXP '($string)'
    //                                 $query_order
    //                                 $query_limit");
        
    //     return ($query->num_rows() > 0) ? $query : FALSE;
    // }

    // NEW METHOD
    function get_recomendations ($string, $sort = 'random', $limit = NULL, $start = NULL) {
        $query_order = 'ORDER BY ';
        switch ($sort) {
            default:
            case 'random':
                $query_order .= 'RAND()';
                break;
            case 'popular':
                $query_order .= 'viewed DESC';
                break;
            case 'newest' :
                $query_order .= 'date_added DESC';
                break;
            case 'lowprice' :
                $query_order .= 'price ASC';
                break;
            case 'highprice' :
                $query_order .= 'price DESC';
                break;
            case 'nameAZ' :
                $query_order .= 'name ASC';
                break;
            case 'nameZA' :
                $query_order .= 'name DESC';
                break;
        }

        $query_limit = '';
        if ($limit) {
            $query_limit = 'LIMIT ' . $start . ', ' . $limit;
        }

        $query = $this->db->query("(SELECT DISTINCT products.name, 
        products.id AS product_id, 
        (CASE WHEN products_principal_stock.price != 0 THEN products_principal_stock.price ELSE (CASE WHEN products.variation = 1 THEN (CASE WHEN products.price_grosir = 0 OR products.price_reseller = 0 THEN product_option.price ELSE products.price END) ELSE products.price END) END) as price, 
        products.discount, 
        products.promo, 
        products.preorder, 
        (CASE WHEN products_principal_stock.promo_data != '' THEN products_principal_stock.promo_data ELSE products.promo_data END) as promo_data, 
        products.viewed AS viewed, 
        products.free_ongkir, 
        product_image.image, 
        merchants.name AS merchant_name, 
        products_principal_stock.branch_id AS store_id,
        products.store_type AS store_type, 
        products_principal_stock.quantity AS quantity,
        products.date_modified AS date_modified,
        products.variation,
        products.price_grosir,
        products_principal_stock.id_option,
        products.price_reseller,
        (CASE WHEN products.min_buy THEN products.min_buy ELSE 1 END) as min_buy,
        products.merchant,
        (CASE WHEN products.variation = 1 THEN product_option.default ELSE 0 END) as product_option_default
        FROM products_principal_stock 
        LEFT JOIN products ON products_principal_stock.product_id = products.id
        LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
        LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id 
        LEFT JOIN principles ON principles.id = products.store_id
        LEFT JOIN product_option ON CASE WHEN products.variation = 1 THEN product_option.id = products_principal_stock.id_option AND product_option.default = 1 ELSE 0 END
        WHERE products.status = 1 
        AND (principles.status = 1 AND merchants.status = 1)
        AND products.store_type = 'principal'
        AND (products_principal_stock.quantity > 0 OR products.preorder = 1 OR products.variation = 1)
        AND products.store_id != 0
        AND products.name REGEXP '($string)'
        GROUP BY products_principal_stock.product_id, products_principal_stock.branch_id HAVING (CASE WHEN products.variation = 1 THEN product_option_default = 1 ELSE product_option_default = 0 END))

        UNION

        (SELECT DISTINCT products.name, 
        products.id AS product_id, 
        (CASE WHEN products.variation = 1 THEN product_option.price ELSE products.price END) as price, 
        products.discount, 
        products.promo, 
        products.preorder, 
        products.promo_data, 
        products.viewed AS viewed, 
        products.free_ongkir, 
        product_image.image, 
        merchants.name AS merchant_name, 
        products.store_id AS store_id, 
        products.store_type AS store_type, 
        (CASE WHEN products.variation = 1 THEN product_option.quantity ELSE products.quantity END) as quantity,
        products.date_modified AS date_modified,
        products.variation,
        products.price_grosir,
        product_option.id as id_option,
        products.price_reseller,
        (CASE WHEN products.min_buy THEN products.min_buy ELSE 1 END) as min_buy,
        products.merchant,
        (CASE WHEN products.variation = 1 THEN product_option.default ELSE 0 END) as product_option_default
        FROM products 
        LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
        LEFT JOIN merchants ON products.store_id = merchants.id
        LEFT JOIN product_option ON CASE WHEN products.variation = 1 THEN product_option.product = products.id AND product_option.default = 1 ELSE 0 END
        WHERE products.status = 1 
        AND merchants.status = 1
        AND products.store_type = 'merchant'
        AND (products.quantity > 0 OR products.preorder = 1 OR products.variation = 1)
        AND products.store_id != 0
        AND products.name REGEXP '($string)')

        ORDER BY date_modified DESC
        $query_limit");
        
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    function get_more_recomendations($string, $params = []) {

        if($params) {
            if(array_key_exists('where', $params)) {
                if($params['where']) {
                    $where = "AND " . $params['where'] ;
                } else {
                    $where = null;
                }
            } else {
                $where = null;
            }

            if(array_key_exists('limit', $params) && $params['return_type'] == '') {
                $limit = "LIMIT " . $params['limit'];
            } else {
                $limit = NULL;
            }
        }

        $query = $this->db->query("(SELECT DISTINCT products.name, 
        products.id AS product_id, 
        (CASE WHEN products_principal_stock.price != 0 THEN products_principal_stock.price ELSE (CASE WHEN products.variation = 1 THEN (CASE WHEN products.price_grosir = 0 OR products.price_reseller = 0 THEN product_option.price ELSE products.price END) ELSE products.price END) END) as price, 
        products.discount, 
        products.promo, 
        products.preorder, 
        (CASE WHEN products_principal_stock.promo_data != '' THEN products_principal_stock.promo_data ELSE products.promo_data END) as promo_data, 
        products.viewed AS viewed, 
        products.free_ongkir, 
        product_image.image, 
        merchants.name AS merchant_name, 
        products_principal_stock.branch_id AS store_id,
        products.store_type AS store_type, 
        products_principal_stock.quantity AS quantity,
        products.date_modified AS date_modified,
        products.variation,
        products.price_grosir,
        products_principal_stock.id_option,
        products.price_reseller,
        (CASE WHEN products.min_buy THEN products.min_buy ELSE 1 END) as min_buy,
        products.merchant,
        (CASE WHEN products.variation = 1 THEN product_option.default ELSE 0 END) as product_option_default
        FROM products_principal_stock 
        LEFT JOIN products ON products_principal_stock.product_id = products.id
        LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
        LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id 
        LEFT JOIN principles ON principles.id = products.store_id
        LEFT JOIN product_option ON CASE WHEN products.variation = 1 THEN product_option.id = products_principal_stock.id_option AND product_option.default = 1 ELSE 0 END
        WHERE products.status = 1 
        AND (principles.status = 1 AND merchants.status = 1)
        AND products.store_type = 'principal'
        AND (products_principal_stock.quantity > 0 OR products.preorder = 1 OR products.variation = 1)
        AND products.store_id != 0
        AND products.name REGEXP '($string)'
        $where
        GROUP BY products_principal_stock.product_id, products_principal_stock.branch_id HAVING (CASE WHEN products.variation = 1 THEN product_option_default = 1 ELSE product_option_default = 0 END))

        UNION

        (SELECT DISTINCT products.name, 
        products.id AS product_id, 
        (CASE WHEN products.variation = 1 THEN product_option.price ELSE products.price END) as price, 
        products.discount, 
        products.promo, 
        products.preorder, 
        products.promo_data, 
        products.viewed AS viewed, 
        products.free_ongkir, 
        product_image.image, 
        merchants.name AS merchant_name, 
        products.store_id AS store_id, 
        products.store_type AS store_type, 
        (CASE WHEN products.variation = 1 THEN product_option.quantity ELSE products.quantity END) as quantity,
        products.date_modified AS date_modified,
        products.variation,
        products.price_grosir,
        product_option.id as id_option,
        products.price_reseller,
        (CASE WHEN products.min_buy THEN products.min_buy ELSE 1 END) as min_buy,
        products.merchant,
        (CASE WHEN products.variation = 1 THEN product_option.default ELSE 0 END) as product_option_default
        FROM products 
        LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
        LEFT JOIN merchants ON products.store_id = merchants.id
        LEFT JOIN product_option ON CASE WHEN products.variation = 1 THEN product_option.product = products.id AND product_option.default = 1 ELSE 0 END
        WHERE products.status = 1 
        AND merchants.status = 1
        AND products.store_type = 'merchant'
        AND (products.quantity > 0 OR products.preorder = 1 OR products.variation = 1)
        AND products.store_id != 0
        AND products.name REGEXP '($string)'
        $where)

        ORDER BY date_modified DESC
        $limit");
        
        if(array_key_exists('return_type', $params)) {
            if($params['return_type'] == 'count') {
                $result = $query->num_rows();
            } else {
                $result = $query->result();
            }
        }
        return $result;
    }

    function get_more_all_product($params = []) {
        if($params) {
            if(array_key_exists('where', $params)) {
                $where = "AND " . $params['where'] ;
            } else {
                $where = null;
            }

            if(array_key_exists('limit', $params) && $params['return_type'] == '') {
                $limit = "LIMIT " . $params['limit'];
            } else {
                $limit = NULL;
            }
        }

        $query = $this->db->query("(SELECT DISTINCT products.name, 
        products.id AS product_id, 
        (CASE WHEN products_principal_stock.price != 0 THEN products_principal_stock.price ELSE (CASE WHEN products.variation = 1 THEN (CASE WHEN products.price_grosir = 0 OR products.price_reseller = 0 THEN product_option.price ELSE products.price END) ELSE products.price END) END) as price, 
        products.discount, 
        products.promo, 
        products.preorder, 
        (CASE WHEN products_principal_stock.promo_data != '' THEN products_principal_stock.promo_data ELSE products.promo_data END) as promo_data, 
        products.viewed, 
        products.free_ongkir, 
        product_image.image, 
        merchants.name AS merchant_name, 
        products_principal_stock.branch_id AS store_id , 
        products.store_type AS store_type, 
        products_principal_stock.quantity AS quantity, 
        products.date_modified AS date_modified, 
        products.variation, 
        products.price_grosir,
        products_principal_stock.id_option,
        products.price_reseller,
        (CASE WHEN products.min_buy THEN products.min_buy ELSE 1 END) as min_buy,
        products.merchant,
        (CASE WHEN products.variation = 1 THEN product_option.default ELSE 0 END) as product_option_default

            FROM products_principal_stock 
            LEFT JOIN products ON products_principal_stock.product_id = products.id 
            LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id 
            LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1 
            LEFT JOIN principles ON principles.id = products.store_id
            LEFT JOIN product_option ON CASE WHEN products.variation = 1 THEN product_option.id = products_principal_stock.id_option AND product_option.default = 1 ELSE 0 END
            WHERE products.status !=0 
            AND (principles.status = 1 AND merchants.status = 1)
            AND products.store_type = 'principal'
            AND (products_principal_stock.quantity >0 OR products.preorder = 1 OR products.variation = 1)
            $where
            GROUP BY products_principal_stock.product_id, products_principal_stock.branch_id HAVING (CASE WHEN products.variation = 1 THEN product_option_default = 1 ELSE product_option_default = 0 END))

            UNION

        (SELECT DISTINCT products.name, 
        products.id AS product_id, 
        (CASE WHEN products.variation = 1 THEN product_option.price ELSE products.price END) as price, 
        products.discount, 
        products.promo, 
        products.preorder, 
        products.promo_data, 
        products.viewed, 
        products.free_ongkir, 
        product_image.image, 
        merchants.name AS merchant_name, 
        products.store_id AS store_id, 
        products.store_type AS store_type, 
        (CASE WHEN products.variation = 1 THEN product_option.quantity ELSE products.quantity END) as quantity, 
        products.date_modified AS date_modified, 
        products.variation, 
        products.price_grosir,
        product_option.id as id_option,
        products.price_reseller,
        (CASE WHEN products.min_buy THEN products.min_buy ELSE 1 END) as min_buy,
        products.merchant,
        (CASE WHEN products.variation = 1 THEN product_option.default ELSE 0 END) as product_option_default

            FROM products 
            LEFT JOIN merchants ON products.store_id = merchants.id 
            LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1 
            LEFT JOIN product_option ON CASE WHEN products.variation = 1 THEN product_option.product = products.id AND product_option.default = 1 ELSE 0 END
            WHERE products.status !=0 
            AND merchants.status = 1
            AND products.store_type = 'merchant'
            AND (products.quantity >0 OR products.preorder = 1 OR products.variation = 1)
            $where)

            ORDER BY date_modified DESC
            $limit");
        
        if(array_key_exists('return_type', $params)) {
            if($params['return_type'] == 'count') {
                $result = $query->num_rows();
            } else {
                $result = $query->result();
            }
        }
        return $result;
    }

    // function get_all_products($sort = 'random', $limit = NULL, $start = NULL) {
    //     $this->db->distinct()
    //             ->select('products.name,
    //                         products.id AS product_id,
    //                         products.price,
    //                         products.discount,
    //                         products.preorder,
    //                         products.promo,
    //                         products.promo_data,
    //                         products.merchant,
    //                         merchants.name AS store_name,
    //                         merchants.id AS store_id,
    //                         product_image.image')
    //             ->join('products', 'products_principal_stock.product_id = products.id', 'LEFT')
    //             ->join('merchants', 'products_principal_stock.branch_id = merchants.id', 'LEFT')
    //             ->join('product_image', 'product_image.product = products.id AND product_image.primary = 1', 'LEFT')
    //             ->where('products.status != ', 0)
    //             ->where('products_principal_stock.quantity > ', 0);
    //     switch ($sort) {
    //         default:
    //         case 'random':
    //             $this->db->order_by('RAND()');
    //             break;
    //         case 'popular' :
    //             $this->db->order_by('products.viewed', 'DESC');
    //             break;
    //         case 'newest' :
    //             $this->db->order_by('products.date_added', 'DESC');
    //             break;
    //         case 'lowprice' :
    //             $this->db->order_by('products.price', 'ASC');
    //             break;
    //         case 'highprice' :
    //             $this->db->order_by('products.price', 'DESC');
    //             break;
    //         case 'nameAZ' :
    //             $this->db->order_by('products.name', 'ASC');
    //             break;
    //         case 'nameZA' :
    //             $this->db->order_by('products.name', 'DESC');
    //             break;
    //     }

    //     if ($limit) {
    //         $this->db->limit($limit, $start);
    //     }
    //     $query = $this->db->get('products_principal_stock');

    //     return ($query->num_rows() > 0) ? $query : FALSE;
    // }

    // NEW METHOD
    function get_all_products($sort = 'random', $limit = NULL, $start = NULL) {
        $query_order = 'ORDER BY ';
        switch ($sort) {
            default:
            case 'random':
                $query_order .= 'RAND()';
                break;
            case 'popular':
                $query_order .= 'viewed DESC';
                break;
            case 'newest' :
                $query_order .= 'date_added DESC';
                break;
            case 'lowprice' :
                $query_order .= 'price ASC';
                break;
            case 'highprice' :
                $query_order .= 'price DESC';
                break;
            case 'nameAZ' :
                $query_order .= 'name ASC';
                break;
            case 'nameZA' :
                $query_order .= 'name DESC';
                break;
        }

        $query_limit = '';
        if ($limit) {
            $query_limit = 'LIMIT ' . $start . ', ' . $limit;
        }

        $query = $this->db->query("(SELECT DISTINCT products.name, 
        products.id AS product_id, 
        (CASE WHEN products_principal_stock.price != 0 THEN products_principal_stock.price ELSE (CASE WHEN products.variation = 1 THEN (CASE WHEN products.price_grosir = 0 OR products.price_reseller = 0 THEN product_option.price ELSE products.price END) ELSE products.price END) END) as price, 
        products.discount, 
        products.promo, 
        products.preorder, 
        (CASE WHEN products_principal_stock.promo_data != '' THEN products_principal_stock.promo_data ELSE products.promo_data END) as promo_data, 
        products.viewed, 
        products.free_ongkir, 
        product_image.image, 
        merchants.name AS merchant_name, 
        products_principal_stock.branch_id AS store_id , 
        products.store_type AS store_type, 
        products_principal_stock.quantity AS quantity, 
        products.date_modified AS date_modified, 
        products.variation, 
        products.price_grosir,
        products_principal_stock.id_option,
        products.price_reseller,
        (CASE WHEN products.min_buy THEN products.min_buy ELSE 1 END) as min_buy,
        products.merchant,
        (CASE WHEN products.variation = 1 THEN product_option.default ELSE 0 END) as product_option_default

            FROM products_principal_stock 
            LEFT JOIN products ON products_principal_stock.product_id = products.id 
            LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id 
            LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1 
            LEFT JOIN principles ON principles.id = products.store_id
            LEFT JOIN product_option ON CASE WHEN products.variation = 1 THEN product_option.id = products_principal_stock.id_option AND product_option.default = 1 ELSE 0 END
            WHERE products.status !=0 
            AND (principles.status = 1 AND merchants.status = 1)
            AND products.store_type = 'principal'
            AND (products_principal_stock.quantity >0 OR products.preorder = 1 OR products.variation = 1)
            GROUP BY products_principal_stock.product_id, products_principal_stock.branch_id HAVING (CASE WHEN products.variation = 1 THEN product_option_default = 1 ELSE product_option_default = 0 END))

            UNION

        (SELECT DISTINCT products.name, 
        products.id AS product_id, 
        (CASE WHEN products.variation = 1 THEN product_option.price ELSE products.price END) as price, 
        products.discount, 
        products.promo, 
        products.preorder, 
        products.promo_data, 
        products.viewed, 
        products.free_ongkir, 
        product_image.image, 
        merchants.name AS merchant_name, 
        products.store_id AS store_id, 
        products.store_type AS store_type, 
        (CASE WHEN products.variation = 1 THEN product_option.quantity ELSE products.quantity END) as quantity, 
        products.date_modified AS date_modified, 
        products.variation, 
        products.price_grosir,
        product_option.id as id_option,
        products.price_reseller,
        (CASE WHEN products.min_buy THEN products.min_buy ELSE 1 END) as min_buy,
        products.merchant,
        (CASE WHEN products.variation = 1 THEN product_option.default ELSE 0 END) as product_option_default

            FROM products 
            LEFT JOIN merchants ON products.store_id = merchants.id 
            LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1 
            LEFT JOIN product_option ON CASE WHEN products.variation = 1 THEN product_option.product = products.id AND product_option.default = 1 ELSE 0 END
            WHERE products.status !=0 
            AND merchants.status = 1
            AND products.store_type = 'merchant'
            AND (products.quantity >0 OR products.preorder = 1 OR products.variation = 1)
            )

            ORDER BY date_modified DESC
            $query_limit");
        
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    function get_rating ($product, $store) {
        $this->db->select('IFNULL((SUM(rating_speed) + SUM(rating_service) + SUM(rating_accuracy)) / (COUNT(id) * 3), 0) AS rating,
                            IFNULL(COUNT(id), 0) AS review')
                ->where('product', $product)
                ->where('merchant', $store)
                ->where('status', 1);

        $query = $this->db->get('product_review');
        return ($query->result() > 0) ? $query->row() : FALSE;
    }
}