<?php
    $product = $m->get_data('', 'coupons', null, array('id' => $id))->row();
    $data = $product;
    

?>
<section class="product-detail-content" style="margin-bottom: 20px">
    <div class="detail-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="shopify-section">
                    <div class="detail-content-inner">
                        <div id="product" class="neque-porro-quisquam-est-qui-dolor-ipsum-quia-9 detail-content">
                            <div class="col-md-12 info-detail-pro clearfix">
                                <div class="col-md-5" id="product-image">
                                    <div class="show-image-load" style="display: none;">
                                        <div class="show-image-load-inner">
                                            <i class="fa fa-spinner fa-pulse fa-2x"></i>
                                        </div>
                                    </div>
                                    <?php
                                        $img = get_image('logo.png');
                                        if($data->gambar != ''){
                                            $img = $data->gambar;
                                        }
                                    ?>
                                    <img src="<?php echo $img; ?>" alt="<?php echo $data->name; ?>"  />
                                </div>
                                <div class="col-md-7" id="product-information">
                                    <h1 itemprop="name" class="title"><?php echo $data->name; ?></h1>
                                    <div class="description" itemprop="description">
                                        <?=$data->deskripsi?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
