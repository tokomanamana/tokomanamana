<?php
    //$product = $this->product->get_product($id);
    $join = array(
        0 => 'brands b-b.id=p.brand'
    );
    $product = $m->get_data('p.*, b.name as brand_name', 'products p', $join, array('p.id' => $id))->row();
    $data = $product;
    
    $images = $this->product->get_images($id);
    if($images){
        $meta_image = $images->row(0)->image;
    }
    //echo $id;

?>
<section class="product-detail-content" style="margin-bottom: 20px">
    <div class="detail-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="shopify-section">
                    <div class="detail-content-inner">
                        <div id="product" class="neque-porro-quisquam-est-qui-dolor-ipsum-quia-9 detail-content">
                            <div class="col-md-12 info-detail-pro clearfix">
                                <div class="col-md-5" id="product-image">
                                    <div class="show-image-load" style="display: none;">
                                        <div class="show-image-load-inner">
                                            <i class="fa fa-spinner fa-pulse fa-2x"></i>
                                        </div>
                                    </div>
                                    <?php
                                        $img = $m->get_data('', 'product_image', null, array('product' => $data->id), '', 'primary desc');
                                        if($img->num_rows() > 0){
                                            $gbr = $img->row()->image;
                                        }else{
                                            $gbr = 'logo.png';
                                        }
                                    ?>
                                    <img src="<?php echo get_image($gbr); ?>" alt="<?php echo $data->name; ?>"  />
                                </div>
                                <div class="col-md-7" id="product-information">
                                    <h1 itemprop="name" class="title"><?php echo $data->name; ?></h1>
                                    <div class="description" itemprop="description"><?php echo $data->short_description; ?></div>
                                    <div class="variants">
                                        <div class="product-options ">
                                            <div class="vendor-type">
                                                <?php if ($data->brand) { ?>
                                                    <span class="product_vendor"><span class="_title">Merek:</span> <?php echo $data->brand_name; ?></span>
                                                <?php } ?>
                                                <?php if ($data->merchant) { ?>
                                                    <span class="product_type"><span class="_title">Penjual:</span> <a href="<?php echo site_url('merchants/view/' . $data->merchant); ?>"><?php echo $data->merchant_name; ?></a></span>
                                                <?php } ?>
                                                <span class="product_sku"><span class="_title">Kode Produk: </span><?php echo $data->code; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
