<section class="collection-heading heading-content">
    <div class="container">
        <div class="row">
            <div class="collection-wrapper">
                <h1 class="collection-title"></h1>
                <div class="breadcrumb-group">
                    <div class="breadcrumb clearfix">
                        <?php echo $breadcrumb; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="product-detail-content" style="margin-bottom: 20px">
    <div class="detail-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="shopify-section">
                    <div class="detail-content-inner">
                        <div id="product" class="neque-porro-quisquam-est-qui-dolor-ipsum-quia-9 detail-content">
                            <div class="col-md-12 info-detail-pro clearfix">
                                <div class="col-md-5" id="product-image">
                                    <div class="show-image-load" style="display: none;">
                                        <div class="show-image-load-inner">
                                            <i class="fa fa-spinner fa-pulse fa-2x"></i>
                                        </div>
                                    </div>
                                    <div id="featuted-image" class="image featured">
                                        <div class="image-item">
                                            <a href="<?php echo $image; ?>" class="thumbnail" data-fancybox="gallery" data-options='{"loop":true,"smallBtn":false,"toolbar":false}'>
                                                <img src="<?php echo $image; ?>" alt="<?php echo $data->nama; ?>" data-item="<?php echo $data->id; ?>" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7" id="product-information">
                                    <h1 itemprop="name" class="title"><?php echo $data->nama; ?></h1>
                                    <p>
                                        <?=$data->deskripsi?>
                                    </p>
                                    <div class="description" itemprop="description"></div>
                                    <div class="variants">
                                        <div class="product-options ">
                                            <div class="product-price">
                                                <h2 class="price">Minimal <?php echo number_format($data->point); ?> Poin</h2>
                                                
                                            </div>
                                            <div class="purchase-section multiple">
                                                <?php if ($data->qty < 1) { ?>
                                                    <p>Stok habis</p>
                                                <?php } else { ?>
                                                <p>Tersisa <?=$data->qty?></p>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="supports-fontface">
                                        <div class="social-sharing is-clean">
                                            <div class="addthis_inline_share_toolbox_su95"></div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
    if($data->id_product != 0){
        $this->load->view('rewards/view_product', array('id' => $data->id_product));
    }
?>

<?php
    if($data->id_product_paket != 0){
        $this->load->view('rewards/view_product', array('id' => $data->id_product_paket));
    }
?>

<?php
    if($data->id_coupon != 0){
        $this->load->view('rewards/view_coupon', array('id' => $data->id_coupon));
    }
?>