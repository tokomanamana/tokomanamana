<?php if($this->agent->is_mobile()) {
    $width_lds = '46%';
} else {
    $width_lds = '48%';
} ?>
<style>
    .lds-ring {
        left: <?php echo $width_lds ?>;
        display: inline-block;
        position: relative;
        width: 64px !important;
        height: 64px !important;
        display: none;
        margin-bottom: 25px
    }

    .lds-ring div {
        box-sizing: border-box;
        display: block;
        position: absolute;
        width: 51px;
        height: 51px;
        margin: 6px;
        border-radius: 50%;
        animation: lds-ring .8s cubic-bezier(.5, 0, .5, 1) infinite;
        border-color: #a7c22a transparent transparent transparent!important
    }

    .lds-ring div:nth-child(1) {
        animation-delay: 0s
    }

    .lds-ring div:nth-child(2) {
        animation-delay: -80ms
    }

    .lds-ring div:nth-child(3) {
        animation-delay: -.1s
    }

    @keyframes lds-ring {
        0% {
            transform: rotate(0)
        }
        100% {
            transform: rotate(360deg)
        }
    }

    .lds-ring {
        display: inline-block;
        position: relative;
        height: 100%;
        padding: 9px 2px
    }

    .lds-ring div {
        box-sizing: border-box;
        display: inline-block;
        position: absolute;
        width: 20px;
        height: 20px;
        border: 3px solid #000;
        border-radius: 50%;
        animation: lds-ring 1.2s cubic-bezier(.5, 0, .5, 1) infinite;
        border-color: #000 transparent transparent transparent
    }

    .lds-ring div:nth-child(1) {
        animation-delay: -.45s
    }

    .lds-ring div:nth-child(2) {
        animation-delay: -.3s
    }

    .lds-ring div:nth-child(3) {
        animation-delay: -.15s
    }

    @keyframes lds-ring {
        0% {
            transform: rotate(0)
        }
        100% {
            transform: rotate(360deg)
        }
    }

    .slick-next:before,
    .slick-prev:before {
        color: #97c23c
    }
</style>
<link rel="stylesheet" href="<?= site_url('assets/frontend/css/minified/home_min.css') ?>">
<section class="collection-heading heading-content ">
    <div class="container">
        <div class="row" style="padding-left: 10px; padding-right: 10px;">
            <div class="collection-wrapper" style="margin-top: 20px;">
                <h1 class="collection-title">
                    <span class="title-segment">Semua Produk Rekomendasi</span>
                </h1>
                <?php echo $breadcrumb; ?>
            </div>
        </div>
    </div>
</section>

<section class="collection-content" style="margin-bottom:<?php echo ($this->agent->is_mobile()) ? '70px;' : '0px;' ; ?>">
    <div class="collection-wrapper">
        <div class="container">
            <div class="row">
                <div id="shopify-section-collection-template" class="shopify-section">
                    <div class="collection-inner">
                        <div id="tags-load" style="display:none;">
                            <i class="fa fa-spinner fa-pulse fa-2x"></i>
                        </div>
                        <div id="collection">
                            <div class="collection_inner">
                                <div class="collection-mainarea col-sm-12 clearfix">
                                    <?php if ($products->num_rows() > 0) { ?>
                                        <div class="collection_toolbar">
                                            <!-- <div class="toolbar_left">
                                                Item <?php //echo number($start); ?> s/d <?php //echo number($to); ?> dari <?php //echo number($total_products); ?> Total Item
                                            </div> -->

                                            <!-- <div class="toolbar_right">
                                                <div class="group_toolbar">
                                                    <form id="form-filter">
                                                        <div class="sortBy">
                                                            <span class="toolbar_title">Urut Berdasarkan:</span>
                                                            <div class="control-container">
                                                                <select name="sort" class="select-bootstrap" tabindex="-1" onchange="$('#form-filter').submit();">
                                                                    <option value="random" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'random') ? 'selected' : ''; ?>>Acak</option>
                                                                    <option value="popular" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'popular') ? 'selected' : ''; ?>>Terpopuler</option>
                                                                    <option value="nameAZ" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'nameAZ') ? 'selected' : ''; ?>>Nama (A-Z)</option>
                                                                    <option value="nameZA" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'nameZA') ? 'selected' : ''; ?>>Nama (Z-A)</option>
                                                                    <option value="lowprice" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'lowprice') ? 'selected' : ''; ?>>Harga Termurah</option>
                                                                    <option value="highprice" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'highprice') ? 'selected' : ''; ?>>Harga Termahal</option>
                                                                    <option value="newest" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'newest') ? 'selected' : ''; ?>>Terbaru</option>
                                                                </select>
                                                            </div>

                                                            <span class="toolbar_title">Tampilkan:</span>
                                                            <div class="control-container">
                                                                <select name="show" tabindex="-1" onchange="$('#form-filter').submit();">
                                                                    <option value="20" <?php //echo ($this->input->get('show') && $this->input->get('show') == '20') ? 'selected' : ''; ?>>20</option>
                                                                    <option value="40" <?php //echo ($this->input->get('show') && $this->input->get('show') == '40') ? 'selected' : ''; ?>>40</option>
                                                                    <option value="80" <?php //echo ($this->input->get('show') && $this->input->get('show') == '80') ? 'selected' : ''; ?>>80</option>
                                                                </select>
                                                            </div>
                                                            <span class="toolbar_title">Item Per Halaman</span>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div> -->
                                        </div>

                                        <div class="collection-items clearfix" style="border:none;">
                                            <div class="products" id="product_recommended" style="border:none;">
                                                <?php
                                                    foreach ($products->result() as $key => $product) {

                                                        if ($product->merchant == 0) {
                                                            $merchant = $this->session->userdata('list_merchant')[0];
                                                            $price_group = $this->main->get('product_price', array('product' => $product->product_id, 'merchant_group' => $merchant['group']));
                                                            if ($price_group) {
                                                                $product->price = $price_group->price;
                                                            }
                                                        }

                                                        if ($this->ion_auth->logged_in()) {
                                                            $id_customer = $this->data['user']->id;
                                                            $get_wishlist = $this->main->get('wishlist', ['product' => $product->product_id, 'branch_id' => $product->store_id, 'customer' => $id_customer]);
                                                            if ($get_wishlist) {
                                                                $product_wishlist = true;
                                                            } else {
                                                                $product_wishlist = false;
                                                            }
                                                        } else {
                                                            $product_wishlist = false;
                                                        }

                                                        if($product->variation) {
                                                            $product_variation = $this->main->get('product_option', ['product' => $product->product_id, 'default' => 1]);
                                                            if($product->store_type == 'principal') {
                                                                $product_variation_branch = $this->main->get('products_principal_stock', ['product_id' => $product->product_id, 'branch_id' => $product->store_id, 'id_option' => $product->id_option]);
                                                                if($product_variation_branch) {
                                                                    if($product_variation_branch->price != 0) {
                                                                        $product->price = $product_variation_branch->price;
                                                                        $product->quantity = $product_variation_branch->quantity;
                                                                    } else {
                                                                        $product->price = $product_variation->price;
                                                                        $product->quantity = $product_variation_branch->quantity;
                                                                    }
                                                                } else {
                                                                    $product->price = $product_variation->price;
                                                                    $product->quantity = $product->quantity;
                                                                }
                                                            } else {
                                                                if($product->price_grosir) {
                                                                    if($product_variation) {
                                                                        $product->price = $product->price;
                                                                        $product->quantity = $product_variation->quantity;
                                                                    } else {
                                                                        $product->price = $product->price;
                                                                        $product->quantity = $product->quantity;
                                                                    }
                                                                } else {
                                                                    if($product_variation) {
                                                                        $product->price = $product_variation->price;
                                                                        $product->quantity = $product_variation->quantity;
                                                                    } else {
                                                                        $product->price = $product->price;
                                                                        $product->quantity = $product->quantity;
                                                                    }
                                                                }
                                                            }
                                                        }

                                                        $sale_on = false;

                                                        if($product->store_type == 'principal') {
                                                            if($product->promo_data) {
                                                                $promo_data = json_decode($product->promo_data, true);
                                                                if(isset($promo_data['status'])) {
                                                                    if($promo_data['status'] == '1' || $product->promo) {
                                                                        if($promo_data['type'] == 'P') {
                                                                            $disc_price = round(($product->price * $promo_data['discount']) / 100);
                                                                            $label_disc = $promo_data['discount'].'% off';
                                                                        } else {
                                                                            $disc_price = $promo_data['discount'];
                                                                            $label_disc = 'SALE';
                                                                        }
                                                                        
                                                                        $end_price = intval($product->price) - intval($disc_price);
                                                                        $today = date('Y-m-d');
                                                                        $today=date('Y-m-d', strtotime($today));
                                                                        $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                                                        $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                                                                        if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                                                            $sale_on = true;
                                                                        } else {
                                                                            $sale_on = false;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        } else {
                                                            if($product->promo) {
                                                                $promo_data = json_decode($product->promo_data,true);
                                                            
                                                                if($promo_data['type'] == 'P') {
                                                                    $disc_price = round(($product->price * $promo_data['discount']) / 100);
                                                                    $label_disc = $promo_data['discount'].'% off';
                                                                } else {
                                                                    $disc_price = $promo_data['discount'];
                                                                    $label_disc = 'SALE';
                                                                }
                                                                
                                                                $end_price = intval($product->price) - intval($disc_price);
                                                                $today = date('Y-m-d');
                                                                $today=date('Y-m-d', strtotime($today));
                                                                $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                                                $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                                                                if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                                                    $sale_on = true;
                                                                } else {
                                                                    $sale_on = false;
                                                                }
                                                            }
                                                        }

                                                        
                                                ?>
                                                        <div class="product-item col-sm-2" style="padding: 0px; border:none;">
                                                            <div class="product" onmouseover="change_store(<?= $key ?>, 'over')" onmouseout="change_store(<?= $key ?>, 'out')">

                                                                <!-- <?php if ($sale_on){ ?>
                                                                    <span class="product-discount-label"><?php echo $label_disc;?></span>
                                                                <?php
                                                                    }
                                                                ?>

                                                                <?php if ($this->ion_auth->logged_in()) : ?>
                                                                    <a href="#" class="btn-add-to-wishlist" style="color:<?= ($product_wishlist) ? '#d9534f;' : '#BBB'  ?>" onclick="add_wishlist(this)" data-product="<?= encode($product->product_id) ?>" data-store="<?= encode($product->store_id) ?>" data-customer="<?= encode($id_customer) ?>">
                                                                        <i class="fa fa-heart" aria-hidden="true"></i>
                                                                    </a>
                                                                <?php else : ?>
                                                                    <a href="#" class="btn-add-to-wishlist" style="color:#BBB" data-toggle="modal" data-target="#modal-guest" data-back="<?= site_url() ?>">
                                                                        <i class="fa fa-heart" aria-hidden="true"></i>
                                                                    </a>
                                                                <?php endif; ?> -->

                                                                <div class="row-left">
                                                                    <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="hoverBorder container_item" style="min-height: 180px;">
                                                                        <img src="<?php echo base_url('assets/frontend/images/loading.gif') ?>" class="img-responsive lazyload" data-src="<?= ($product->image) ? site_url('files/images/' . $product->image) : site_url('assets/frontend/images/noimage.jpg'); ?>" alt="<?php echo $product->name; ?>">
                                                                    </a>
                                                                </div>

                                                                <div class="garis-tengah"></div>
                                                                <div class="tombol-cart">
                                                                <?php if($this->ion_auth->logged_in()) : ?>
                                                                    <?php if ($product->variation || $product->min_buy > 1) : ?>
                                                                        <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="icon-cart">
                                                                            <!-- <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i> -->
                                                                            <img src="<?= site_url('assets/frontend/images/cart.png') ?>" style="width: 18px;margin-left:10px;line-height:38px">
                                                                        </a>
                                                                    <?php else : ?>
                                                                        <button data-id="<?php echo encode($product->product_id); ?>" data-storeid="<?= encode($product->store_id) ?>" onclick="addtoCart($(this).data('id'), $(this).data('storeid'))" class="icon-cart">
                                                                            <!-- <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i> -->
                                                                            <img src="<?= site_url('assets/frontend/images/cart.png') ?>" style="width: 18px;">
                                                                        </button>
                                                                    <?php endif; ?>
                                                                <?php else : ?>
                                                                    <button type="button" class="icon-cart btn-cart-not-login" data-toggle="modal" data-target="#modal-guest" data-back="<?= site_url() ?>">
                                                                        <!-- <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i> -->
                                                                        <img src="<?= site_url('assets/frontend/images/cart.png') ?>" style="width: 18px;">
                                                                    </button>
                                                                    <!-- <a href="<?php echo site_url('/?back=' . $this->input->get('back')) ?>" class="icon-cart" id="btn-cart-not-login" data-toggle="modal" data-target="#modal-guest">
                                                                        <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                                                    </a> -->
                                                                <?php endif; ?>
                                                                </div>

                                                                <div class="row-right animMix">
                                                                    <div class="grid-mode">
                                                                        <div class="product-title">
                                                                            <a class="title-5" style="font-weight: bold; width: 75%;transition:.3s;" href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>"><?php echo $product->name; ?></a>
                                                                        </div>

                                                                        <div class="viewed-div">
                                                                            <i class="fa fa-eye" aria-hidden="true"></i> <span><?= $product->viewed; ?></span>
                                                                        </div>

                                                                        <div class="product-price">
                                                                            <span class="price_sale">

                                                                                <!-- <?php //if ($sale_on) { ?>
                                                                                    <span class="money" style="text-decoration: line-through;font-size:11px;color:#8b8f8b"><?php echo rupiah($product->price); ?></span>
                                                                                    <span class="money"><?php echo rupiah($end_price); ?></span>
                                                                                <?php
                                                                                    //}
                                                                                   // else {
                                                                                ?>
                                                                                        <span class="money"><?php echo rupiah($product->price); ?></span>
                                                                                <?php // } ?> -->

                                                                                <?php if($sale_on) : ?>
                                                                                    <div class="money price-end"><?= rupiah($end_price) ?></div>
                                                                                    <div class="price_sale">
                                                                                        <span class="money promo-price"><?= rupiah($product->price) ?></span>
                                                                                        <span class="promo-text"><?= $label_disc ?></span>
                                                                                    </div>
                                                                                <?php else : ?>
                                                                                    <div class="money price-end"><?= rupiah($product->price) ?></div>
                                                                                <?php endif; ?>

                                                                            </span>
                                                                        </div>

                                                                        <?php if($product->preorder) : ?>
                                                                            <div class="pre-order-label">Preorder</div>
                                                                        <?php endif; ?>
                                                                        <?php if($product->free_ongkir) : ?>
                                                                            <div class="free-ongkir-label">Free Ongkir</div>
                                                                        <?php endif; ?>

                                                                        <?php
                                                                        // $merchant = $this->main->get('merchants', ['id' => $product->store_id]);
                                                                        // $city = $this->main->get('cities', ['id' => $merchant->city]);
                                                                        ?>
                                                                        <!-- <div style="height: 15px;overflow: hidden;">
                                                                            <span id="text-ellipsis-city-<?= $key ?>" style="display: block;"><?= ($city->type == 'Kabupaten') ? 'Kab. ' . $city->name : $city->name ?></span>
                                                                            <span id="text-ellipsis-store-<?= $key ?>" style="display: block;"><?= $merchant->name ?></span>
                                                                        </div> -->

                                                                        <div class="merchant-div">
                                                                            <?php
                                                                            $merchant = $this->main->get('merchants', ['id' => $product->store_id]);
                                                                            $city = $this->main->get('cities', ['id' => $merchant->city]);
                                                                            ?>
                                                                            <div class="merchant-div-body">
                                                                                <span id="text-ellipsis-city-<?= $key ?>" style="display:block;"><?= ($city->type == 'Kabupaten') ? 'Kab. ' . $city->name : '' . $city->name ?></span>
                                                                                <span id="text-ellipsis-store-<?= $key ?>" style="display: block;"><?= $merchant->name ?></span>
                                                                            </div>
                                                                        </div>

                                                                        <!-- <div class="rating-star">
                                                                            <span class="spr-badge" data-rating="0.0">
                                                                                <span class="spr-starrating spr-badge-starrating">
                                                                                    <?php
                                                                                        $rating = $this->recomend->get_rating ($product->product_id, $product->store_id);
                                                                                        
                                                                                        if (round($rating->rating) > 0) {
                                                                                            
                                                                                            for ($i = 1; $i <= round($rating->rating); $i++) {
                                                                                    ?>
                                                                                                <i class="spr-icon spr-icon-star"></i>
                                                                                    <?php
                                                                                            }
                                                                                            for ($n = (($rating->rating) + 1); $n <= 5; $n++) {
                                                                                    ?>
                                                                                                <i class="spr-icon spr-icon-star-empty"></i>
                                                                                    <?php
                                                                                            }
                                                                                        }
                                                                                    ?>
                                                                                </span>
                                                                            </span>
                                                                        </div> -->

                                                                        <!-- <div class="product-footer">
                                                                            &nbsp;
                                                                            <i class="fa fa-eye" aria-hidden="true"></i> <?= $product->viewed; ?>
                                                                        </div> -->
                                                                       <?php
                                                                        // $preorder = $product->preorder;
                                                                        // $free_ongkir = $product->free_ongkir;

                                                                        // if ($preorder == 1){
                                                                        //     echo'<div class="pre-order-label">Preorder</div>';
                                                                        // }
                                                                        // if($free_ongkir == 1) {
                                                                        //     echo'<div class="free-ongkir-label">Free Ongkir</div>';
                                                                        // } ?>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                <?php
                                                    }
                                                ?>
                                                <div class="load-more" lastView="<?php echo $product->date_modified; ?>" last_key="<?= $key + 1 ?>" style="display: none;"> </div>
                                            </div>
                                        </div>
                                        <?php if($products->num_rows() > 11) : ?>
                                            <div class="lds-ring"><div></div><div></div><div></div><div></div></div> 
                                            <div id="bottom_check"></div>
                                        <?php endif; ?>
                                        <?php
                                            //if ($pagination) {
                                        ?>
                                                <!-- <div class="collection-bottom-toolbar">
                                                    <div class="product-pagination">
                                                        <div class="pagination_group">
                                                            <?php //echo $pagination; ?>
                                                        </div>
                                                    </div>
                                                </div> -->
                                    <?php  //}
                                        } else {
                                    ?>
                                            <p>Tidak ada produk yang dapat ditampilkan</p>
                                    <?php
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
function addtoCart(t,a){$.ajax({url:site_url+"catalog/products/add_to_cart_ajax_new",type:"post",data:{product:t,store:a,qty:1},beforeSend:function(){$("#tags-load").css("display","block")},complete:function(){$("#tags-load").css("display","none")},success:function(t){if("error"==(t=JSON.parse(t)).status)Swal.fire({type:"error",title:"Terjadi Kesalahan",text:t.message});else{var a=Object.keys(t.items).length,e=0,i=0;if(a>0){var s='<div style="max-height: 200px; overflow: auto">';s+='<div class="carts-items">',$.each(t.items,function(t,a){if(a.option){let t=JSON.parse(a.option);$.each(t,function(t,o){e+=Number(o.price*o.quantity),i+=Number(o.quantity),s+='<div class="row" id="cart-header-option-'+a.id_encrypt+'" style="padding: 10px 10px;border-bottom:1px solid #EEE;">',s+='<div class="cart-left col-md-4" style="display: inline-block;">',s+='<a class="cart-image" href="'+site_url+"catalog/products/view/"+a.product_id+"/"+a.merchant_id+'">',s+='<img style="width: 50px;" src="'+site_url+"files/images/"+o.image+'" alt="" title="">',s+="</a>",s+="</div>",s+='<div class="cart-right col-md-8" style="display: inline-block;margin-left:-25px;">',s+='<div class="">',s+='<a href="'+site_url+"catalog/products/view/"+a.product_id+"/"+a.merchant_id+'" style="color:#000;">',s+=a.name,s+="</a>",s+="</div>",s+='<div class="cart-price">',s+='<span class="money" style="color: #97C23C;">'+formatRupiah(o.price,".")+"</span>",s+='<span class="x" style="color: #97C23C;">x '+o.quantity+"</span>",s+="</div>",s+="</div>",s+="</div>"})}else e+=Number(a.price*a.quantity),i+=Number(a.quantity),s+='<div class="row" id="cart-header-'+a.id_encrypt+'" style="padding: 10px 10px;border-bottom:1px solid #EEE;">',s+='<div class="cart-left col-md-4" style="display: inline-block;">',s+='<a class="cart-image" href="'+site_url+"catalog/products/view/"+a.product_id+"/"+a.merchant_id+'">',s+='<img style="width: 50px;" src="'+site_url+"files/images/"+a.image+'" alt="" title="">',s+="</a>",s+="</div>",s+='<div class="cart-right col-md-8" style="display: inline-block;margin-left:-25px;">',s+='<div class="">',s+='<a href="'+site_url+"catalog/products/view/"+a.product_id+"/"+a.merchant_id+'" style="color:#000;">',s+=a.name,s+="</a>",s+="</div>",s+='<div class="cart-price">',s+='<span class="money" style="color: #97C23C;">'+formatRupiah(a.price,".")+"</span>",s+='<span class="x" style="color: #97C23C;">x '+a.quantity+"</span>",s+="</div>",s+="</div>",s+="</div>"}),s+="</div>",s+="</div>",s+='<div class="subtotal" style="color: black;padding: 15px 20px;font-size:14px;border-top:1px solid #EEE;">',s+='<span>Subtotal:</span><span class="cart-total-right money" style="float: right;">'+formatRupiah(e,".")+"</span>",s+="</div>",s+='<div class="action" style="padding: 15px 20px;margin-top:-10px;">',s+='<a href="'+site_url+'cart" class="btn btn-show-all" style="font-size:10px; width: 100%;border-radius:3px;">Lihat Semua</a>',s+="</div>"}else s='<p style="font-size: 13px; font-weight: bold; padding: 15px;text-align:center;">Keranjang belanja Anda kosong. Yuk, belanja sekarang!</p>';$("#cart-target .badge").html(i),$("#cart-button .badge").html(i),$(".icon-cart .cart_text .number").html(i),$("#cart-target-mobile .number").html(i),$("#cart-target .cart-info .cart-content").html(s),$("#cart-target-mobile .cart-info .cart-content").html(s),$(".cart_icon #icon_cart .badge").html(i),$(".cart_icon .cart-dropdown").html(s),Swal.fire({type:"success",title:"Produk Berhasil Ditambah!",timer:1e3,showConfirmButton:!1,customClass:"swal-class"})}}})}function formatRupiah(t,a){var e=(t=t.toString()).replace(/[^,\d]/g,"").toString().split(","),i=e[0].length%3,s=e[0].substr(0,i),o=e[0].substr(i).match(/\d{3}/gi);return o&&(separator=i?".":"",s+=separator+o.join(".")),s=null!=e[1]?s+","+e[1]:s,null==a?s:s?"Rp "+s:""}function loaddata(t,a){$.ajax({type:"POST",url:site_url+"catalog/recomendation/load_more_recomendation_product",data:{viewed:t,last_key:a},beforeSend:function(){$(".load-more").show(),$(".lds-ring").show()},success:function(t){$(".load-more").remove(),$(".lds-ring").remove(),$("#product_recommended").append(t)}})}function change_store(t,a){"over"==a?$("#text-ellipsis-city-"+t).attr("style","margin-top:-18px;display:block;transition:.3s;"):$("#text-ellipsis-city-"+t).attr("style","margin-top:0px;display:block;transition:.3s;")}function add_wishlist(t){let a=$(t).data("product"),e=$(t).data("store"),i=$(t).data("customer");$.ajax({url:site_url+"catalog/products/add_wishlist_new",data:{product:a,store:e,customer:i},type:"POST",success:function(a){"add"==(a=JSON.parse(a)).status?($(t).attr("style","color:#d9534f;"),Swal.fire({type:"success",title:"Wishlist Berhasil Ditambah",timer:1e3,showConfirmButton:!1}),$(".profile-list .profile-dropdown .profile-body .wishlist-link .badge").html(a.total),$(".profile-list .profile-dropdown .profile-body .wishlist-link .badge").attr("style","position: absolute;right:25px;")):($(t).attr("style","color:#BBB;"),Swal.fire({type:"success",title:"Wishlist Berhasil Dihapus",timer:1e3,showConfirmButton:!1}),$(".profile-list .profile-dropdown .profile-body .wishlist-link .badge").html(a.total),$(".profile-list .profile-dropdown .profile-body .wishlist-link .badge").attr("style","position: absolute;right:25px;"))}})}$(document).ready(function(){var t;$(window).scroll(function(){t&&clearTimeout(t);var a=$(".load-more").attr("lastView"),e=$(".load-more").attr("last_key"),i=$("#bottom_check").offset().top-($(window).scrollTop()+$(window).height());t=setTimeout(function(){i<300&&0!=a&&loaddata(a,e)},600)}),$(".btn-add-to-wishlist").on("click",function(t){t.preventDefault()})});
</script>