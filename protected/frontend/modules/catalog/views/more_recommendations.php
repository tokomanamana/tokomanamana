        <?php
            foreach ($products as $product) {

                if ($product->merchant == 0) {
                    $merchant = $this->session->userdata('list_merchant')[0];
                    $price_group = $this->main->get('product_price', array('product' => $product->product_id, 'merchant_group' => $merchant['group']));
                    if ($price_group) {
                        $product->price = $price_group->price;
                    }
                }

                if ($this->ion_auth->logged_in()) {
                    $id_customer = $this->data['user']->id;
                    $get_wishlist = $this->main->get('wishlist', ['product' => $product->product_id, 'branch_id' => $product->store_id, 'customer' => $id_customer]);
                    if ($get_wishlist) {
                        $product_wishlist = true;
                    } else {
                        $product_wishlist = false;
                    }
                } else {
                    $product_wishlist = false;
                }

                if($product->variation) {
                    $product_variation = $this->main->get('product_option', ['product' => $product->product_id, 'default' => 1]);
                    if($product->price_grosir) {
                        if($product_variation) {
                            $product->price = $product->price;
                            $product->quantity = $product_variation->quantity;
                        } else {
                            $product->price = $product->price;
                            $product->quantity = $product->quantity;
                        }
                    } else {
                        if($product_variation) {
                            $product->price = $product_variation->price;
                            $product->quantity = $product_variation->quantity;
                        } else {
                            $product->price = $product->price;
                            $product->quantity = $product->quantity;
                        }
                    }
                }

                $sale_on = false;

                if($product->promo) {
                    $promo_data = json_decode($product->promo_data,true);
                
                    if($promo_data['type'] == 'P') {
                        $disc_price = round(($product->price * $promo_data['discount']) / 100);
                        $label_disc = $promo_data['discount'].'% off';
                    } else {
                        $disc_price = $promo_data['discount'];
                        $label_disc = 'SALE';
                    }
                    
                    $end_price = intval($product->price) - intval($disc_price);
                    $today = date('Y-m-d');
                    $today=date('Y-m-d', strtotime($today));
                    $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                    $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                    if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                        $sale_on = true;
                    } else {
                        $sale_on = false;
                    }
                }
        ?>
                <div class="product-item col-sm-2" style="padding: 0px; border:none;">
                    <div class="product" onmouseover="change_store(<?= $key ?>, 'over')" onmouseout="change_store(<?= $key ?>, 'out')">

                        <!-- <?php if ($sale_on){ ?>
                            <span class="product-discount-label"><?php echo $label_disc;?></span>
                        <?php
                            }
                        ?> -->

                        <!-- <?php if ($this->ion_auth->logged_in()) : ?>
                            <a href="#" class="btn-add-to-wishlist" style="color:<?= ($product_wishlist) ? '#d9534f;' : '#BBB'  ?>" onclick="add_wishlist(this)" data-product="<?= encode($product->product_id) ?>" data-store="<?= encode($product->store_id) ?>" data-customer="<?= encode($id_customer) ?>">
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </a>
                        <?php else : ?>
                            <a href="#" class="btn-add-to-wishlist" style="color:#BBB" data-toggle="modal" data-target="#modal-guest" data-back="<?= site_url() ?>">
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </a>
                        <?php endif; ?> -->

                        <div class="row-left">
                            <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="hoverBorder container_item" style="min-height: 180px;">
                                <img src="<?php echo base_url('assets/frontend/images/loading.gif') ?>" class="img-responsive lazyload" data-src="<?= ($product->image) ? site_url('files/images/' . $product->image) : site_url('assets/frontend/images/noimage.jpg'); ?>" alt="<?php echo $product->name; ?>">
                            </a>
                        </div>

                        <div class="garis-tengah"></div>
                        <div class="tombol-cart">
                        <?php if($this->ion_auth->logged_in()) : ?>
                        <?php if ($product->variation || $product->min_buy) : ?>
                            <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="icon-cart">
                                <!-- <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i> -->
                                <img src="<?= site_url('assets/frontend/images/cart.png') ?>" style="width: 18px;margin-left:10px;line-height:38px">
                            </a>
                        <?php else : ?>
                            <button data-id="<?php echo encode($product->product_id); ?>" data-storeid="<?= encode($product->store_id) ?>" onclick="addtoCart($(this).data('id'), $(this).data('storeid'))" class="icon-cart">
                                <!-- <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i> -->
                                <img src="<?= site_url('assets/frontend/images/cart.png') ?>" style="width: 18px;">
                            </button>
                        <?php endif; ?>
                        <?php else : ?>
                            <button type="button" class="icon-cart btn-cart-not-login" data-toggle="modal" data-target="#modal-guest" data-back="<?= site_url() ?>">
                                <!-- <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i> -->
                                <img src="<?= site_url('assets/frontend/images/cart.png') ?>" style="width: 18px;">
                            </button>
                            <!-- <a href="<?php echo site_url('/?back=' . $this->input->get('back')) ?>" class="icon-cart" id="btn-cart-not-login" data-toggle="modal" data-target="#modal-guest">
                                <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                            </a> -->
                        <?php endif; ?>
                        </div>

                        <div class="row-right animMix">
                            <div class="grid-mode">
                                <div class="product-title">
                                    <a class="title-5" style="font-weight: bold; width: 75%;transition:.3s;" href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>"><?php echo $product->name; ?></a>
                                </div>

                                <div class="viewed-div">
                                    <i class="fa fa-eye" aria-hidden="true"></i> <span><?= $product->viewed; ?></span>
                                </div>

                                <div class="product-price">
                                    <span class="price_sale">

                                        <?php if($sale_on) : ?>
                                            <div class="money price-end"><?= rupiah($end_price) ?></div>
                                            <div class="price_sale">
                                                <span class="money promo-price"><?= rupiah($product->price) ?></span>
                                                <span class="promo-text"><?= $label_disc ?></span>
                                            </div>
                                        <?php else : ?>
                                            <div class="money price-end"><?= rupiah($product->price) ?></div>
                                        <?php endif; ?>

                                    </span>
                                </div>

                                <?php if($product->preorder) : ?>
                                    <div class="pre-order-label">Preorder</div>
                                <?php endif; ?>
                                <?php if($product->free_ongkir) : ?>
                                    <div class="free-ongkir-label">Free Ongkir</div>
                                <?php endif; ?>

                                <div class="merchant-div">
                                    <?php
                                    $merchant = $this->main->get('merchants', ['id' => $product->store_id]);
                                    $city = $this->main->get('cities', ['id' => $merchant->city]);
                                    ?>
                                    <div class="merchant-div-body">
                                        <span id="text-ellipsis-city-<?= $key ?>" style="display:block;"><?= ($city->type == 'Kabupaten') ? 'Kab. ' . $city->name : '' . $city->name ?></span>
                                        <span id="text-ellipsis-store-<?= $key ?>" style="display: block;"><?= $merchant->name ?></span>
                                    </div>
                                </div>

                                <!-- <div class="rating-star">
                                    <span class="spr-badge" data-rating="0.0">
                                        <span class="spr-starrating spr-badge-starrating">
                                            <?php
                                                $rating = $this->recomend->get_rating ($product->product_id, $product->store_id);
                                                
                                                if (round($rating->rating) > 0) {
                                                    
                                                    for ($i = 1; $i <= round($rating->rating); $i++) {
                                            ?>
                                                        <i class="spr-icon spr-icon-star"></i>
                                            <?php
                                                    }
                                                    for ($n = (($rating->rating) + 1); $n <= 5; $n++) {
                                            ?>
                                                        <i class="spr-icon spr-icon-star-empty"></i>
                                            <?php
                                                    }
                                                }
                                            ?>
                                        </span>
                                    </span>
                                </div>

                                <div class="product-footer">
                                    &nbsp;
                                    <i class="fa fa-eye" aria-hidden="true"></i> <?= $product->viewed; ?>
                                </div> -->
                                <?php
                                // $preorder = $product->preorder;
                                // $free_ongkir = $product->free_ongkir;

                                // if ($preorder == 1){
                                //     echo'<div class="pre-order-label">Preorder</div>';
                                // }
                                // if($free_ongkir == 1) {
                                //     echo'<div class="free-ongkir-label">Free Ongkir</div>';
                                // } ?>

                            </div>
                        </div>
                    </div>
                </div>
        <?php
        $key++;
            }
        ?>
        <?php if($products_num > $products_limit) : ?>
            <div class="load-more" lastView="<?php echo $product->date_modified; ?>" last_key="<?= $key + 1 ?>" style="display: none;"> </div>
        <?php endif; ?>

<?php if($products_num > $products_limit) : ?>
<div class="lds-ring"><div></div><div></div><div></div><div></div></div> 
<?php endif; ?>

<script>
$(function() {
    $('.btn-add-to-wishlist').on('click', function(e) {
        e.preventDefault();
    })
    $('.lazyload', 'div').each(function() {
        // randomly show an image after between 1 and 5 seconds
        var showtimetime = Math.floor(Math.random() * (5000 - 1000 + 1) + 1000);
        var $this = this;
        setTimeout(function() {
            //console.log($this);
            $($this).attr('src', $($this).attr('data-src'));
        }, showtimetime);

    });
})
</script>