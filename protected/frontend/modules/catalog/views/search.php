<style>
    .collection-title {
    margin-top: 25px;
    font-size: 18px !important;
    color: #a7c22a !important;
    font-weight: 700 !important;
}
.breadcrumb a,
i.fa.fa-home {
    color: #97c23c !important;
}
.breadcrumb a:hover,
.breadcrumb a:hover i.fa.fa-home {
    color: #969eaa !important;
}
#collection .collection-mainarea .collection-items .product-item {
    border: none;
}
.pagination_group {
    padding: 10px 0 !important;
    text-align: right;
}
.pagination_group .pagination li a {
    border-top: 1px solid #e0e0e0;
    border-bottom: 1px solid #e0e0e0;
    border-left: 1px solid #e0e0e0;
    background-color: #fff;
    color: #97c23c;
    height: 30px;
    vertical-align: middle;
    padding: 0 13px;
    text-transform: capitalize;
    font-size: 13px;
    border-radius: 0;
    font-weight: 500;
    -webkit-transition: all 0.5s;
    -moz-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -o-transition: all 0.5s;
    transition: all 0.5s;
}
.pagination_group .pagination li.active a {
    background-color: #f1f1f1;
    color: #ccc;
    border: 1px solid #e0e0e0;
}
.pagination_group .pagination li:hover a {
    background-color: #f1f1f1;
    color: #ccc;
}
.collection-leftsidebar {
    border: 1px solid #e8e8e8;
    box-shadow: 0 0 7px -3px rgba(0, 0, 0, 0.3);
}
.sidebar.collection-leftsidebar {
    margin-top: -65px;
}
.collection-wrapper {
    margin-top: 20px;
}
</style>
<link rel="stylesheet" href="<?= site_url('assets/frontend/css/minified/home_min.css') ?>">
<?php $word = htmlspecialchars($this->input->get('word')); ?>
<section class="collection-heading heading-content" style="<?= ($this->agent->is_mobile()) ? 'margin-top:-35px;' : 'margin-top:-20px;' ?>">
    <div class="container">
        <div class="row">
            <div class="collection-wrapper" style="padding-right: 5px;padding-left: 5px;">
                <h1 class="collection-title">
                    <span><?php echo 'Pencarian produk "' . $word . '"'; ?></span>
                </h1>
                <?php echo $breadcrumb; ?>
            </div>
        </div>
    </div>
</section>

<section class="collection-content">
    <div class="collection-wrapper">
        <div class="container">
            <div class="row">
                <div id="shopify-section-collection-template" class="shopify-section">
                    <div class="collection-inner">
                        <div id="tags-load" style="display:none;">
                            <i class="fa fa-spinner fa-pulse fa-2x"></i>
                        </div>

                        <div id="collection">
                            <div class="collection_inner">
                                <div class="collection-mainarea col-sm-12 clearfix">
                                    <?php if ($products->num_rows() > 0) { ?>
                                        <div class="collection_toolbar" style="padding-left: 7px; padding-right: 7px;">
                                            <div class="toolbar_left">
                                                Item <?php echo number($start); ?> s/d <?php echo number($to); ?> dari <?php echo number($total_products); ?> Total Item
                                            </div>

                                            <div class="toolbar_right">
                                                <div class="group_toolbar">
                                                    <form id="form-filter">
                                                        <input type="hidden" name="word" value="<?php echo $this->input->get('word'); ?>">
                                                        <input type="hidden" name="cat" value="<?php echo $this->input->get('cat'); ?>">
                                                        <div class="sortBy">
                                                            <span class="toolbar_title">Urut Berdasarkan:</span>
                                                            <div class="control-container">
                                                                <select name="sort" tabindex="-1" onchange="$('#form-filter').submit();">
                                                                    <!-- <option value="distance" <?= ($this->input->get('sort') && $this->input->get('sort' == 'distance')) ? 'selected' : '' ?>>Terdekat</option> -->
                                                                    <option value="popular" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'popular') ? 'selected' : ''; ?>>Terpopuler</option>
                                                                    <option value="nameAZ" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'nameAZ') ? 'selected' : ''; ?>>Nama (A-Z)</option>
                                                                    <option value="nameZA" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'nameZA') ? 'selected' : ''; ?>>Nama (Z-A)</option>
                                                                    <option value="lowprice" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'lowprice') ? 'selected' : ''; ?>>Harga Termurah</option>
                                                                    <option value="highprice" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'highprice') ? 'selected' : ''; ?>>Harga Termahal</option>
                                                                    <option value="newest" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'newest') ? 'selected' : ''; ?>>Terbaru</option>
                                                                </select>
                                                            </div>

                                                            <span class="toolbar_title">Tampilkan:</span>
                                                            <div class="control-container">
                                                                <select name="show" tabindex="-1" onchange="$('#form-filter').submit();">
                                                                    <option value="20" <?php echo ($this->input->get('show') && $this->input->get('show') == '20') ? 'selected' : ''; ?>>20</option>
                                                                    <option value="40" <?php echo ($this->input->get('show') && $this->input->get('show') == '40') ? 'selected' : ''; ?>>40</option>
                                                                    <option value="80" <?php echo ($this->input->get('show') && $this->input->get('show') == '80') ? 'selected' : ''; ?>>80</option>
                                                                </select>
                                                            </div>
                                                            <span class="toolbar_title">Item Per Halaman</span>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="collection-items clearfix" style="border:none;">
                                            <div class="products" style="border:none;">
                                                <?php
                                                foreach ($products->result() as $key => $product) {

                                                    // if ($product->merchant == 0) {
                                                    //   $merchant = $this->session->userdata('list_merchant')[0];
                                                    //   $price_group = $this->main->get('product_price', array('product' => $product->product_id, 'merchant_group' => $merchant['group']));
                                                    //   if ($price_group) {
                                                    //     $product->price = $price_group->price;
                                                    //   }
                                                    // }

                                                    if ($this->ion_auth->logged_in()) {
                                                        $id_customer = $this->data['user']->id;
                                                        $get_wishlist = $this->main->get('wishlist', ['product' => $product->product_id, 'branch_id' => $product->store_id, 'customer' => $id_customer]);
                                                        if ($get_wishlist) {
                                                            $product_wishlist = true;
                                                        } else {
                                                            $product_wishlist = false;
                                                        }
                                                    } else {
                                                        $product_wishlist = false;
                                                    }

                                                    $sale_on = false;

                                                    if($product->variation) {
                                                        $product_variation = $this->main->get('product_option', ['product' => $product->product_id, 'default' => 1]);
                                                        if($product->merchant_type == 'merchant') {
                                                            if($product->price_grosir) {
                                                                if($product_variation) {
                                                                    $product->price = $product->price;
                                                                    $quantity_variations = $product_variation->quantity;
                                                                } else {
                                                                    $product->price = 0;
                                                                    $quantity_variations = 0;
                                                                }
                                                            } else {
                                                                if($product_variation) {
                                                                    $product->price = $product_variation->price;
                                                                    $quantity_variations = $product_variation->quantity;
                                                                } else {
                                                                    $product->price = 0;
                                                                    $quantity_variations = 0;
                                                                }
                                                            }
                                                        } else {
                                                            $product_variation_branch = $this->main->get('products_principal_stock', ['product_id' => $product->product_id, 'branch_id' => $product->store_id, 'id_option' => $product->id_option]);
                                                            if($product_variation_branch) {
                                                                if($product_variation_branch->price != 0) {
                                                                    $product->price = $product_variation_branch->price;
                                                                    $quantity_variations = $product_variation_branch->quantity;
                                                                } else {
                                                                    $product->price = $product_variation->price;
                                                                    $quantity_variations = $product_variation_branch->quantity;
                                                                }
                                                            } else {
                                                                $product->price = $product_variation->price;
                                                                $quantity_variations = $product_variation->quantity;
                                                            }
                                                        }
                                                    }

                                                    if($product->merchant_type == 'merchant') {
                                                        if($product->promo) {
                                                            $promo_data = json_decode($product->promo_data, true);
    
                                                            if ($promo_data['type'] == 'P') {
                                                                $disc_price = round(($product->price * $promo_data['discount']) / 100);
                                                                $label_disc = $promo_data['discount'] . '% off';
                                                            } else {
                                                                $disc_price = $promo_data['discount'];
                                                                $label_disc = 'SALE';
                                                            }
    
                                                            $end_price = intval($product->price) - intval($disc_price);
                                                            $today = date('Y-m-d');
                                                            $today = date('Y-m-d', strtotime($today));
                                                            $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                                            $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                                                            if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                                                $sale_on = true;
                                                            } else {
                                                                $sale_on = false;
                                                            }
                                                        }
                                                    } else {
                                                        if($product->promo_data) {
                                                            $promo_data = json_decode($product->promo_data, true);
                                                            if(isset($promo_data['status'])) {
                                                                if($promo_data['status'] == '1') {
                                                                    if ($promo_data['type'] == 'P') {
                                                                        $disc_price = round(($product->price * $promo_data['discount']) / 100);
                                                                        $label_disc = $promo_data['discount'] . '% off';
                                                                    } else {
                                                                        $disc_price = $promo_data['discount'];
                                                                        $label_disc = 'SALE';
                                                                    }
            
                                                                    $end_price = intval($product->price) - intval($disc_price);
                                                                    $today = date('Y-m-d');
                                                                    $today = date('Y-m-d', strtotime($today));
                                                                    $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                                                    $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                                                                    if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                                                        $sale_on = true;
                                                                    } else {
                                                                        $sale_on = false;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                ?>
                                                    <div class="product-item col-sm-2">
                                                        <div class="product" onmouseover="change_store(<?= $key ?>, 'over')" onmouseout="change_store(<?= $key ?>, 'out')">

                                                            <!-- <?php if ($sale_on) : ?>
                                                                <span class="product-discount-label"><?php echo $label_disc; ?></span>
                                                            <?php endif; ?> -->

                                                            <!-- <?php if ($this->ion_auth->logged_in()) : ?>
                                                                <a href="#" class="btn-add-to-wishlist" style="<?= ($product_wishlist) ? 'color:#d9534f;' : 'color:#BBB'  ?>" onclick="add_wishlist(this)" data-product="<?= encode($product->product_id) ?>" data-store="<?= encode($product->store_id) ?>" data-customer="<?= encode($id_customer) ?>">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>
                                                                </a>
                                                            <?php else : ?>
                                                                <a href="#" class="btn-add-to-wishlist" style="color:#BBB" data-toggle="modal" data-target="#modal-guest" data-back="<?= site_url() ?>">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>
                                                                </a>
                                                            <?php endif; ?> -->

                                                            <div class="row-left">
                                                                <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="hoverBorder container_item" style="min-height: 180px;">
                                                                    <img src="<?php echo base_url('assets/frontend/images/loading.gif') ?>" alt="<?= $product->name; ?>" class="img-responsive lazyload" data-src="
                                                                    <?php 
                                                                        if($product->image_thumb)echo site_url('files/image_thumbs/'. $product->image_thumb);
                                                                        else{ 
                                                                            if($product->image)echo site_url('files/images/' . $product->image);
                                                                            else echo site_url('assets/frontend/images/noimage.jpg');
                                                                        } 
                                                                    ?>">
                                                                </a>

                                                            </div>

                                                            <div class="garis-tengah"></div>
                                                            <div class="tombol-cart">
                                                                <?php if ($this->ion_auth->logged_in()) : ?>
                                                                    <?php if ($product->variation || $product->min_buy > 1) : ?>
                                                                        <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="icon-cart">
                                                                            <img src="<?= site_url('assets/frontend/images/cart.png') ?>" style="width: 18px;margin-left:10px;line-height:38px">
                                                                        </a>
                                                                    <?php else : ?>
                                                                        <button data-id="<?php echo encode($product->product_id); ?>" data-storeid="<?= encode($product->store_id) ?>" onclick="addtoCart($(this).data('id'), $(this).data('storeid'))" class="icon-cart">
                                                                            <img src="<?= site_url('assets/frontend/images/cart.png') ?>" style="width: 18px;">
                                                                        </button>
                                                                    <?php endif; ?>
                                                                <?php else : ?>
                                                                    <button type="button" class="icon-cart" data-toggle="modal" data-target="#modal-guest">
                                                                        <img src="<?= site_url('assets/frontend/images/cart.png') ?>" style="width: 18px;">
                                                                    </button>
                                                                <?php endif; ?>
                                                            </div>

                                                            <div class="row-right animMix">
                                                                <div class="grid-mode">
                                                                    <div class="product-title">
                                                                        <a class="title-5" style="font-weight: bold; width: 75%;transition:.3s;" href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>"><?php echo $product->name; ?></a>
                                                                    </div>

                                                                    <div class="viewed-div">
                                                                        <i class="fa fa-eye" aria-hidden="true"></i> <span><?= $product->viewed; ?></span>
                                                                    </div>

                                                                    <div class="product-price">
                                                                        <span class="price_sale">

                                                                            <?php if($sale_on) : ?>
                                                                                <div class="money price-end"><?= rupiah($end_price) ?></div>
                                                                                <div class="price_sale">
                                                                                    <span class="money promo-price"><?= rupiah($product->price) ?></span>
                                                                                    <span class="promo-text"><?= $label_disc ?></span>
                                                                                </div>
                                                                            <?php else : ?>
                                                                                <div class="money price-end"><?= rupiah($product->price) ?></div>
                                                                            <?php endif; ?>

                                                                        </span>
                                                                    </div>

                                                                    <?php if($product->preorder) : ?>
                                                                        <div class="pre-order-label">Preorder</div>
                                                                    <?php endif; ?>
                                                                    <?php if($product->free_ongkir) : ?>
                                                                        <div class="free-ongkir-label">Free Ongkir</div>
                                                                    <?php endif; ?>

                                                                    <div class="merchant-div">
                                                                        <?php
                                                                        $merchant = $this->main->get('merchants', ['id' => $product->store_id]);
                                                                        $city = $this->main->get('cities', ['id' => $merchant->city]);
                                                                        ?>
                                                                        <div class="merchant-div-body">
                                                                            <span id="text-ellipsis-city-<?= $key ?>" style="display:block;"><?= ($city->type == 'Kabupaten') ? 'Kab. ' . $city->name : '' . $city->name ?></span>
                                                                            <span id="text-ellipsis-store-<?= $key ?>" style="display: block;"><?= $merchant->name ?></span>
                                                                        </div>
                                                                    </div>

                                                                    <!-- <div class="rating-star">
                                                                        <span class="spr-badge" data-rating="0.0">
                                                                            <span class="spr-starrating spr-badge-starrating">
                                                                                <?php
                                                                                $rating = $this->search->get_rating($product->product_id, $product->store_id);

                                                                                if (round($rating->rating) > 0) {

                                                                                    for ($i = 1; $i <= round($rating->rating); $i++) {
                                                                                ?>
                                                                                        <i class="spr-icon spr-icon-star"></i>
                                                                                    <?php
                                                                                    }
                                                                                    for ($n = (($rating->rating) + 1); $n <= 5; $n++) {
                                                                                    ?>
                                                                                        <i class="spr-icon spr-icon-star-empty"></i>
                                                                                <?php
                                                                                    }
                                                                                }
                                                                                ?>
                                                                            </span>
                                                                        </span>
                                                                    </div>

                                                                    <div class="product-footer">
                                                                        &nbsp;
                                                                        <i class="fa fa-eye" aria-hidden="true"></i> <?= $product->viewed; ?>
                                                                    </div> -->
                                                                    <?php
                                                                    // $preorder = $product->preorder;
                                                                    // $free_ongkir = $product->free_ongkir;

                                                                    // if ($preorder == 1) {
                                                                    //     echo '<div class="pre-order-label">Preorder</div>';
                                                                    // }
                                                                    // if ($free_ongkir == 1) {
                                                                    //     echo '<div class="free-ongkir-label">Free Ongkir</div>';
                                                                    // } ?>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        </div>

                                        <?php if ($pagination) { ?>
                                            <div class="collection-bottom-toolbar">
                                                <div class="product-pagination">
                                                    <div class="pagination_group">
                                                        <?php echo $pagination; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php   }
                                    } else { ?>
                                        <p>Tidak ada produk dengan kata kunci yang Anda masukkan</p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>