<?php foreach ($option_groups as $option_group => $group) { ?>
    <?php //var_dump($group); ?>
    <div class="swatch swatch clearfix ">
        <div class="header"><?php echo $group['name']; ?></div>
        <?php if ($group['type'] == 's') { ?>
            <select class="form-control" id="group-<?php echo $option_group; ?>" data-product-option="<?php echo $option_group; ?>" name="option[<?php echo $option_group; ?>]">

                <?php foreach ($group['options'] as $id_option => $option) { ?>
                    <option value="<?php echo $id_option; ?>" <?php echo ($option['selected']) ? 'selected' : ''; ?>><?php echo $option['name']; ?></option>

                <?php } ?>
            </select>
        <?php } else { ?>
            <?php foreach ($group['options'] as $id_option => $option) { ?>
                <div class="swatch-element <?php echo ($group['type'] == 'Warna') ? 'color' : 'not-color'; ?> available <?php echo ($option['selected']) ? 'active' : ''; ?> <?php echo ($option['active'] == FALSE) ? 'disabled' : '' ?>" >
                    <input type="radio" name="option[<?php echo $option_group; ?>]" value="<?php echo $id_option; ?>" data-product-option="<?php echo $option_group; ?>" id="option-<?php echo $id_option; ?>" <?php echo ($option['selected']) ? 'checked' : ''; ?> <?php echo ($option['active'] == FALSE) ? 'disabled' : '' ?>>
                    <?php if ($group['type'] == 'Warna') { ?>
                        <label for="option-<?php echo $id_option; ?>" style="background-color: <?php echo $option['color']; ?>; border-color: <?php echo $option['color']; ?>"><?php //echo $option['name']; ?></label>
                    <?php } else { ?>
                        <label for="option-<?php echo $id_option; ?>" data-toggle="tooltip" data-placement="top" data-original-text="<?php echo $option['name']; ?>"><?php echo $option['name']; ?></label>
                    <?php } ?>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
<?php } ; ?>