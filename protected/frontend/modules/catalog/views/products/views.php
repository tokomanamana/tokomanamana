<?php 
function tgl_indo($tanggal){
    $bulan = array (
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $pecahkan = explode('-', $tanggal);
 
    return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}
?>
<style>
    .quantity-field input[class="number_qty"] {
        max-width: 5rem;
        text-align: center;
        padding: .5rem;
        border: solid #DDDDDD;
        border-width: 0 2px;
    }
</style>
<link rel="stylesheet" href="<?= site_url('assets/frontend/css/minified/home_min.css') ?>">
<div id="tags-load" style="display:none;"><i class="fa fa-spinner fa-pulse fa-2x"></i></div>
<section class="collection-heading heading-content">
    <div class="container">
        <div class="row">
            <div class="collection-wrapper">
                <div class="breadcrumb-group">
                    <div class="breadcrumb clearfix">
                        <?php echo $breadcrumb; ?>
                    </div>
                </div>
                <h1 class="collection-title"></h1>
            </div>
        </div>
    </div>
</section>

<section class="product-detail-content" style="margin-top:30px;">
    <div class="detail-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="shopify-section">
                    <div class="detail-content-inner">
                        <div id="product" class="neque-porro-quisquam-est-qui-dolor-ipsum-quia-9 detail-content">

                            <div class="col-md-12 info-detail-pro clearfix">

                                <div class="col-md-4" id="product-image">
                                    <div class="show-image-load" style="display: none;">
                                        <div class="show-image-load-inner">
                                            <i class="fa fa-spinner fa-pulse fa-2x"></i>
                                        </div>
                                    </div>

                                    <?php if ($this->agent->is_mobile()) : ?>
                                        <div id="mobile-slider" class="flexslider">
                                            <ul class="slides slides-big">
                                                <?php
                                                if ($images) {
                                                    $list_images = array();
                                                    $img_primary = array();
                                                    if ($is_option) {
                                                        foreach ($images as $image) {
                                                            echo $image;
                                                        }
                                                    } else {
                                                        foreach ($images->result() as $image) {
                                                            if ($image->primary == "1") {
                                                                $img_primary[] = $image;
                                                            } else {
                                                                $list_images[] = $image;
                                                            }
                                                        }

                                                        $newlist_img = array_merge($img_primary, $list_images);


                                                        foreach ($newlist_img as $image) {
                                                ?>
                                                            <li>
                                                                <a href="<?php echo get_image($image->image); ?>" data-fancybox="gallery" data-options='{"loop":true,"smallBtn":false,"toolbar":false}'>
                                                                    <img src="<?php echo get_image($image->image); ?>" alt="<?php echo $data->name; ?>" data-item="<?php echo $image->id; ?>" />
                                                                </a>
                                                            </li>
                                                <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                                <!-- items mirrored twice, total of 12 -->
                                            </ul>
                                        </div>
                                    <?php else : ?>
                                        <div id="slider" class="flexslider">
                                            <ul class="slides slides-big">
                                                <?php
                                                if ($images) {
                                                    $list_images = array();
                                                    $img_primary = array();
                                                    if ($is_option) {
                                                        foreach ($images as $image) {
                                                            echo $image;
                                                        }
                                                        // echo $images;
                                                    } else {
                                                        foreach ($images->result() as $image) {
                                                            if ($image->primary == "1") {
                                                                $img_primary[] = $image;
                                                            } else {
                                                                $list_images[] = $image;
                                                            }
                                                        }

                                                        $newlist_img = array_merge($img_primary, $list_images);


                                                        foreach ($newlist_img as $image) {
                                                ?>
                                                            <li>
                                                                <a href="<?php echo get_image($image->image); ?>" data-fancybox="gallery" data-options='{"loop":true,"smallBtn":false,"toolbar":false}'>
                                                                    <img src="<?php echo get_image($image->image); ?>" alt="<?php echo $data->name; ?>" data-item="<?php echo $image->id; ?>" />
                                                                </a>
                                                            </li>
                                                <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                                <!-- items mirrored twice, total of 12 -->
                                            </ul>
                                        </div>

                                        <div id="carousel" class="flexslider">
                                            <ul class="slides slides-small">
                                                <?php
                                                if ($images) {
                                                    $list_images = array();
                                                    $img_primary = array();
                                                    if ($is_option) {
                                                        foreach ($images as $image) {
                                                            echo $image;
                                                        }
                                                        // echo $images;
                                                    } else {
                                                        foreach ($images->result() as $image) {
                                                            if ($image->primary == "1") {
                                                                $img_primary[] = $image;
                                                            } else {
                                                                $list_images[] = $image;
                                                            }
                                                        }

                                                        $newlist_img = array_merge($img_primary, $list_images);

                                                        foreach ($newlist_img as $image) {
                                                ?>
                                                            <li>
                                                                <img src="<?php echo get_image($image->image); ?>" alt="<?php echo $data->name; ?>" data-item="<?php echo $image->id; ?>" />
                                                            </li>
                                                <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                                <!-- items mirrored twice, total of 12 -->
                                            </ul>
                                        </div>
                                    <?php endif; ?>
                                    <!-- </div> -->
                                </div>
                                <!-- END OF PRODUCT-IMAGE -->

                                <!-- PRODUCT INFORMATION -->
                                <?php if ($this->agent->is_mobile()) : ?>
                                    <div class="row" style="margin-top: -30px;padding:0px;position:relative;" id="product-information">
                                        <div class="col-sm-12">
                                            <div class="add-to-wishlist-mobile" style="position: absolute;right: 35px;">

                                                <?php if ($this->ion_auth->logged_in()) : ?>
                                                    <?php if ($mywish) : ?>
                                                        <a href="javascript:void(0);" class="wish-list" onclick="add_to_wishlist_new('<?php echo encode($data->product_id); ?>', '<?php echo encode($data->store_id) ?>', '<?= encode($this->data['user']->id) ?>')" style="font-size:2rem;">
                                                            <i class="fa fa-heart" aria-hidden="true" style="color: #d9534f;"></i>
                                                        </a>
                                                    <?php else : ?>
                                                        <a href="javascript:void(0);" class="wish-list" onclick="add_to_wishlist_new('<?php echo encode($data->product_id); ?>', '<?php echo encode($data->store_id) ?>', '<?= encode($this->data['user']->id) ?>')" style="font-size:2rem;">
                                                            <i class="fa fa-heart" aria-hidden="true" style="color: #CCC;"></i>
                                                        </a>
                                                    <?php endif; ?>
                                                <?php else : ?>
                                                    <a href="javascript:void(0);" class="wish-list" style="font-size:2rem;" data-toggle="modal" data-target="#modal-guest" data-back="<?= current_url() ?>">
                                                        <i class="fa fa-heart" aria-hidden="true" style="color: #CCC;"></i>
                                                    </a>
                                                <?php endif; ?>

                                            </div>
                                            <h1 itemprop="name" class="title" style="font-weight: bold;font-size:18px;"><?php echo $data->name; ?></h1>
                                            <?php 
                                            if($data->variation) {
                                                if(isset($options)) {
                                                    $product_option_id = $options_setting['product_option']->id;
                                                } else {
                                                    $product_option_id = '';
                                                }
                                            } else {
                                                $product_option_id = '';
                                            }
                                            ?>

                                            <input type="hidden" name="product_option" value="<?php echo $product_option_id ?>">

                                            <div class="col-md-12 col-xs-12" style="padding: 0;">
                                                <?php if ($data->price_grosir) : ?>
                                                    <div class="grochire-detail col-md-8 col-sm-8 col-xs-8 grochire-div">
                                                        <?php
                                                            $price_level = $this->product->product_price_level($data->id);
                                                        ?>
                                                        <?php if($price_level) : ?>
                                                            <?php
                                                                $min_qty_price_level = $price_level->row(0)->min_qty;
                                                                if($data->variation) {
                                                                    $data_option = $this->main->get('product_option', ['id' => decode($product_option_id)]);
                                                                    if($data_option) {
                                                                        $data->product_quantity = $data_option->quantity;
                                                                    } else {
                                                                        $data->product_quantity = $data->product_quantity;
                                                                    }
                                                                }
                                                            ?>
                                                            <?php if($min_qty_price_level <= $data->product_quantity) : ?>
                                                                <?php
                                                                    $first_price_level = $price_level->row(0);
                                                                    $end_price_level = $first_price_level->price;
                                                                    
                                                                    if ($inPromo) {
                                                                        $promo_data = json_decode($data->promo_data, TRUE);

                                                                        if ($promo_data['type'] == 'P') {
                                                                            $discount_amount = round($first_price_level->price * $promo_data['discount']) / 100;
                                                                            $end_price_level = $first_price_level->price - $discount_amount;
                                                                        } else {
                                                                            if ($promo_data['discount'] >= $first_price_level->price) {
                                                                                $end_price_level = $promo_data['discount'];
                                                                            } else {
                                                                                $end_price_level = $first_price_level->price - $promo_data['discount'];
                                                                            }
                                                                        }
                                                                    }
                                                                    $check_dropdown = 0;
                                                                    if($price_level->num_rows > 2) {
                                                                        $num_rows_price_level = 2;
                                                                    } else {
                                                                        $num_rows_price_level = $price_level->num_rows();
                                                                    }
                                                                ?>

                                                                <!-- <div class="grochire">
                                                                    <p>Beli <?php //echo $price_level->row(0)->min_qty; ?> lebih murah <b>@ <?php //echo rupiah($end_price_level); ?></b>.</p>
                                                                </div> -->
                                                                <span style="font-size: 14px;margin-top: 10px;display: block;margin-bottom: 2px;font-weight: 500;">Harga Grosir</span>
                                                                <?php $count_row = floor(12 / ($num_rows_price_level + 1)); ?>
                                                                <div class="col-md-<?php echo $count_row; ?> col-sm-<?php echo $count_row; ?> col-xs-<?php echo $count_row; ?> grochire-qty active" data-id="<?php echo encode($data->id) ?>" data-qty="<?php echo (int)1; ?>" data-type="grochire" onclick="change_grochire(this)">
                                                                    <span>1</span>
                                                                    <span><?php echo rupiah($price); ?></span>
                                                                </div>
                                                                <?php foreach ($price_level->result() as $row => $pl) : ?>
                                                                    <?php
                                                                        $end_price_level = $pl->price;
                                                                        if ($inPromo) {
                                                                            $promo_data = json_decode($data->promo_data, TRUE);

                                                                            if ($promo_data['type'] == 'P') {
                                                                                $end_price_level = $pl->price - (round($pl->price * $promo_data['discount']) / 100);
                                                                            } else {
                                                                                if ($promo_data['discount'] >= $pl->price) {
                                                                                    $end_price_level = $promo_data['discount'];
                                                                                } else {
                                                                                    $end_price_level = $pl->price - $promo_data['discount'];
                                                                                }
                                                                            }

                                                                            $today = date('Y-m-d');
                                                                            $today=date('Y-m-d', strtotime($today));
                                                                            $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                                                            $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

                                                                            if (($today >= $DateBegin) && ($today <= $DateEnd)){
                                                                                $end_price_level = $end_price_level;
                                                                            } else {
                                                                                $end_price_level = $pl->price;
                                                                            }
                                                                        } else {
                                                                            $end_price_level = $pl->price;
                                                                        }
                                                                    ?>
                                                                    <?php if($pl->min_qty <= $data->product_quantity) : ?>
                                                                        <?php $check_dropdown++; ?>
                                                                        <?php if($row <= 1) : ?>
                                                                            <div class="col-md-<?php echo $count_row; ?> col-sm-<?php echo $count_row; ?> col-xs-<?php echo $count_row; ?> grochire-qty" data-id="<?php echo encode($data->id) ?>" data-qty="<?php echo $pl->min_qty; ?>" data-type="grochire" data-product_qty="<?php echo $data->product_quantity ?>" onclick="change_grochire(this)">
                                                                                <span><?php echo ($row + 1 == $price_level->num_rows()) ? '>= '. $pl->min_qty : $pl->min_qty .' - '. ($price_level->row($row + 1)->min_qty - 1); ?></span>
                                                                                <span><?php echo rupiah($end_price_level); ?></span>
                                                                            </div>
                                                                        <?php else : ?>
                                                                            <div class="col-md-<?php echo $count_row; ?> col-sm-<?php echo $count_row; ?> col-xs-<?php echo $count_row; ?> grochire-qty see-other-grochire" data-id="<?php echo encode($data->id) ?>" data-qty="<?php echo $pl->min_qty; ?>" data-type="grochire" data-product_qty="<?php echo $data->product_quantity ?>" style="display: none;" onclick="change_grochire(this)">
                                                                                <span><?php echo ($row + 1 == $price_level->num_rows()) ? '>= '. $pl->min_qty : $pl->min_qty .' - '. ($price_level->row($row + 1)->min_qty - 1); ?></span>
                                                                                <span><?php echo rupiah($end_price_level); ?></span>
                                                                            </div>
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                <?php endforeach; ?>
                                                                <?php if($check_dropdown > 2) : ?>
                                                                    <div class="col-md-12 col-sm-12" style="text-align: center;margin-top: 2px;">
                                                                        <a href="javascript:void(0)" id="see_others_grochire_price_link" data-type="show">Tampilkan Semua</a>
                                                                    </div>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                <?php endif; ?>

                                                <?php if ($data->price_reseller) : ?>
                                                    <?php 
                                                        $reseller = $this->main->gets('product_price_reseller', ['product' => $data->id]);
                                                        $min_qty = $reseller->first_row()->min_qty;
                                                    ?>
                                                    <?php if($min_qty <= $data->quantity) : ?>
                                                        <div class="grochire-detail col-md-4 col-sm-4 col-xs-4" style="padding-right: 0px;">
                                                            <span style="font-size: 14px;margin-top: 10px;display: block;margin-bottom: 2px;font-weight: 500;">Harga Reseller</span>
                                                            <?php 
                                                            if($reseller->num_rows > 2) {
                                                                $num_rows_reseller = 2;
                                                            } else {
                                                                $num_rows_reseller = $reseller->num_rows();
                                                            }
                                                            $check_dropdown_reseller = 0;
                                                            ?>
                                                            <?php $count_reseller = floor(12 / $num_rows_reseller); ?>
                                                            <?php foreach ($reseller->result() as $row => $rs) : ?>
                                                                <?php 
                                                                    $end_price_level = $rs->price;
                                                                    if ($inPromo) {
                                                                        $promo_data = json_decode($data->promo_data, TRUE);

                                                                        if ($promo_data['type'] == 'P') {
                                                                            $end_price_level = $rs->price - (round($rs->price * $promo_data['discount']) / 100);
                                                                        } else {
                                                                            if ($promo_data['discount'] >= $rs->price) {
                                                                                $end_price_level = $promo_data['discount'];
                                                                            } else {
                                                                                $end_price_level = $rs->price - $promo_data['discount'];
                                                                            }
                                                                        }

                                                                        $today = date('Y-m-d');
                                                                        $today=date('Y-m-d', strtotime($today));
                                                                        $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                                                        $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

                                                                        if (($today >= $DateBegin) && ($today <= $DateEnd)){
                                                                            $end_price_level = $end_price_level;
                                                                        } else {
                                                                            $end_price_level = $rs->price;
                                                                        }
                                                                    } else {
                                                                        $end_price_level = $rs->price;
                                                                    }
                                                                ?>
                                                                <?php if($rs->min_qty <= $data->quantity) : ?>
                                                                    <?php $check_dropdown_reseller++; ?>
                                                                    <?php if($row <= 1) : ?>
                                                                        <div class="col-md-<?php echo $count_reseller; ?> col-sm-<?php echo $count_reseller; ?> col-xs-<?php echo $count_reseller; ?> grochire-qty reseller-item" data-id="<?php echo encode($data->id) ?>" data-qty="<?php echo $rs->min_qty; ?>" data-type="reseller" data-product_qty="<?php echo $data->product_quantity ?>" data-login="<?php echo ($this->ion_auth->logged_in()) ? 'true' : 'false' ?>" data-mobile="true" onclick="change_reseller(this)">
                                                                            <span><?php echo (($row + 1) == $reseller->num_rows()) ? '>= '. $rs->min_qty : $rs->min_qty .' - '. ($reseller->row($row + 1)->min_qty - 1); ?></span>
                                                                            <span><?php echo rupiah($end_price_level); ?></span>
                                                                        </div>
                                                                    <?php else : ?>
                                                                        <div class="col-md-<?php echo $count_reseller; ?> col-sm-<?php echo $count_reseller; ?> col-xs-<?php echo $count_reseller; ?> grochire-qty other-reseller-item reseller-item" data-id="<?php echo encode($data->id) ?>" data-qty="<?php echo $rs->min_qty; ?>" data-type="reseller" data-product_qty="<?php echo $data->product_quantity ?>" data-login="<?php echo ($this->ion_auth->logged_in()) ? 'true' : 'false' ?>" style="display: none;" data-mobile="true" onclick="change_reseller(this)">
                                                                            <span><?php echo (($row + 1) == $reseller->num_rows()) ? '>= '. $rs->min_qty : $rs->min_qty .' - '. ($reseller->row($row + 1)->min_qty - 1); ?></span>
                                                                            <span><?php echo rupiah($end_price_level); ?></span>
                                                                        </div>
                                                                    <?php endif; ?>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                            <?php if($check_dropdown_reseller > 2) : ?>
                                                                <div class="col-md-12 col-sm-12" style="text-align: center;margin-top: 2px;">
                                                                    <a href="javascript:void(0)" id="see_others_grochire_reseller" data-type="show">Tampilkan Semua</a>
                                                                </div>
                                                            <?php endif; ?>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </div>

                                            <div class="variants-mobile">
                                                <div class="product-options">

                                                    <?php if ($data->brand) { ?>
                                                        <div class="vendor-type">
                                                            <span>Merek : <?php echo $data->brand_name; ?></span>
                                                        </div>
                                                    <?php } ?>

                                                    <div class="vendor-type">
                                                        <span>Berat : <?php echo $data->weight; ?> gram</span>
                                                    </div>

                                                    <?php if ($data->guarantee) : ?>
                                                        <div class="vendor-type">
                                                            <span>Garansi : <?= $data->guarantee ?></span>
                                                        </div>
                                                    <?php endif; ?>

                                                    <div class="rating-star" style="margin-bottom: 2rem;">
                                                        <span class="spr-badge">
                                                            <span class="spr-badge-caption">
                                                            </span>

                                                            <span class="spr-starrating spr-badge-starrating">

                                                                <?php for ($i = 1; $i <= $rating->rating; $i++) { ?>
                                                                    <i class="spr-icon spr-icon-star"></i>
                                                                <?php
                                                                }

                                                                for ($i = $rating->rating + 1; $i <= 5; $i++) {
                                                                ?>
                                                                    <i class="spr-icon spr-icon-star-empty"></i>
                                                                <?php } ?>

                                                            </span>
                                                        </span>
                                                    </div>
                                                    <?php if ($data->free_ongkir == 1) { ?>
                                                        <span class="product_free_ongkir" style="left:0;">
                                                            <img height="38" width="80" src="<?php echo site_url('assets/frontend/images/FREE ONGKIR ICON-02-01.png'); ?>">
                                                        </span>
                                                    <?php } ?>

                                                    <?php if ($is_option) { ?>
                                                        <div class="product-type" id="product-option"><?php echo $options; ?></div>
                                                    <?php } ?>

                                                    <hr class="divider">

                                                    <div style="margin-bottom: 2rem;">
                                                        <b>Deskripsi Singkat :</b> <br><?= $data->short_description ?>
                                                    </div>

                                                    <hr class="divider">

                                                    <div>
                                                        <b>Estimasi ongkos kirim</b>
                                                        <table id="table_cek_ongkir">
                                                            <thead>
                                                                <tr>
                                                                    <th style="padding-left:10px;padding-right:10px;text-transform: capitalize;text-align:left;">
                                                                        <label class="control-label">Kecamatan</label>
                                                                        <select style="width:100%;" name="district" id="district" class="form-control district">
                                                                        </select>
                                                                    </th>
                                                                    <th style="padding-left:10px;padding-right:10px;text-transform: capitalize;text-align:left;">
                                                                        <label class="control-label">Berat</label>
                                                                        <b style="text-transform: lowercase;"><?php echo $data->weight; ?>gr</b>
                                                                        <input type="hidden" id="berat" value="<?php echo $data->weight; ?>">
                                                                    </th>
                                                                    <th style="padding-left:10px;padding-right:10px;">
                                                                        <a style="color:#78c93a; cursor:pointer;" onclick="districts()"><b>Hitung</b></a>
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody></tbody>
                                                        </table>
                                                    </div>
                                                    <!-- END OF CEK ONGKIR -->
                                                </div>
                                                <!-- END OF PRODUCT OPTIONS -->
                                            </div>

                                        </div>

                                        <!-- SHIPPING -->
                                        <div class="col-sm-12 col-xs-12">
                                            <div class="div_kanan" style="overflow: auto;position: relative; border-radius: 5px;">
                                                <?php if ($inPromo) : ?>
                                                    <div style="margin:10px auto" class="disc-product-price col-xs-12">
                                                        <span style="background-color:#d9534f;color:white;padding:5px 10px;font-weight: 550;border-radius: 3px;">
                                                            <?php echo $label_disc; ?>
                                                        </span>
                                                        <span class="money" style="text-decoration: line-through;font-size:12px;color:#8b8f8b;margin-left: 5px;">
                                                            <?php echo rupiah($old_price); ?>
                                                        </span>
                                                    </div>
                                                <?php endif; ?>
                                                <div class="product-price col-xs-12">
                                                    <h2 class="price" style="color: #B7CD69 !important;font-weight: bold;font-size: 20px;outline-color: red;margin-top: 15px;"><?php echo rupiah($price); ?></h2>
                                                </div>

                                                <!-- <?php if (isset($price_level) && $price_level->num_rows() > 0) : ?>
                                                    <?php $pl_1 = $price_level->row(0); ?>
                                                    <?php if ($inPromo && $price_level) : ?>
                                                        <?php $promo_data = json_decode($data->promo_data); ?>
                                                        <?php if ($promo_data->type == 'P') : ?>
                                                            <?php foreach ($price_level->result() as $pl) : ?>
                                                                <?php $pl->price = intval($pl->price); ?>
                                                                <?php $disc_price = round(($pl->price * $promo_data->discount) / 100) ?>
                                                                <?php $pl->price -= intval($disc_price) ?>
                                                            <?php endforeach; ?>
                                                            <div class="grochire">
                                                                Beli <?= $price_level->row(0)->min_qty ?> lebih murah <b>@ <?= rupiah($price_level->row(0)->price) ?></b>
                                                                <a data-toggle="popover" data-container="body" data-placement="bottom" data-html="true" href="javascript:void(0);" id="view-list-grochire" class="link">Lihat harga grosir</a>

                                                                <div class="popover">
                                                                    <table class="table">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Jumlah</th>
                                                                                <th>Harga / Unit</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <?php foreach ($price_level->result() as $row => $pl) : ?>
                                                                                <tr>
                                                                                    <td>
                                                                                        <?php if (($row + 1) == $price_level->num_rows()) : ?>
                                                                                            <?= '>=' . $pl->min_qty ?>
                                                                                        <?php else : ?>
                                                                                            <?= $pl->min_qty . ' - ' . (intval($price_level->row($row + 1)->min_qty) - 1) ?>
                                                                                        <?php endif; ?>
                                                                                    </td>
                                                                                    <td><?= rupiah(intval($pl->price)) ?></td>
                                                                                </tr>
                                                                            <?php endforeach; ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        <?php elseif ($promo_data->type == 'F') : ?>
                                                            <?php foreach ($price_level->result() as $pl) : ?>
                                                                <?php $pl->price = intval($pl->price); ?>
                                                                <?php $disc_price = intval($promo_data->discount) ?>
                                                                <?php $pl->price -= $disc_price ?>
                                                            <?php endforeach; ?>
                                                            <div class="grochire">
                                                                Beli <?= $price_level->row(0)->min_qty ?> lebih murah <b>@ <?= rupiah($price_level->row(0)->price) ?></b>
                                                                <a data-toggle="popover" data-container="body" data-placement="bottom" data-html="true" href="javascript:void(0);" id="view-list-grochire" class="link">Lihat harga grosir</a>

                                                                <div class="popover">
                                                                    <table class="table">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Jumlah</th>
                                                                                <th>Harga / Unit</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <?php foreach ($price_level->result() as $row => $pl) : ?>
                                                                                <tr>
                                                                                    <td>
                                                                                        <?php if (($row + 1) == $price_level->num_rows()) : ?>
                                                                                            <?= '>=' . intval($pl->min_qty) ?>
                                                                                        <?php else : ?>
                                                                                            <?= $pl->min_qty . ' - ' . (intval($price_level->row($row + 1)->min_qty) - 1) ?>
                                                                                        <?php endif; ?>
                                                                                    </td>
                                                                                    <td><?= rupiah(intval($pl->price)) ?></td>
                                                                                </tr>
                                                                            <?php endforeach; ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                    <?php else : ?>
                                                        <div class="grochire">
                                                            Beli <?php echo $pl_1->min_qty; ?> lebih murah <b>@ <?php echo rupiah(intval($pl_1->price)); ?></b>
                                                            <a data-toggle="popover" data-container="body" data-placement="bottom" data-html="true" href="javascript:void(0);" id="view-list-grochire" class="link">Lihat harga grosir</a>

                                                            <div class="popover">
                                                                <table class="table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Jumlah</th>
                                                                            <th>Harga / Unit</th>
                                                                        </tr>
                                                                    </thead>

                                                                    <tbody>
                                                                        <?php foreach ($price_level->result() as $row => $pl) { ?>
                                                                            <tr>
                                                                                <td>
                                                                                    <?php
                                                                                    if (($row + 1) == $price_level->num_rows()) {
                                                                                        echo '>= ' . $pl->min_qty;
                                                                                    } else {
                                                                                        echo $pl->min_qty . ' - ' . (intval($price_level->row($row + 1)->min_qty) - 1);
                                                                                    }
                                                                                    ?>
                                                                                </td>
                                                                                <td><?php echo rupiah(intval($pl->price)); ?></td>
                                                                            </tr>
                                                                        <?php } ?>
                                                                    </tbody>

                                                                </table>
                                                            </div>

                                                            <br>

                                                            <?php if ($data->guarantee) { ?>
                                                                <br>

                                                                <span class="verified-title">
                                                                    <b>Garansi : <?php echo ucwords($data->guarantee) ?></b>
                                                                </span>
                                                            <?php } ?>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php endif; ?> -->

                                                <div class="vendor-type col-xs-12" style="margin-top: 10px;">

                                                    <?php if ($data->merchant_name) { ?>
                                                        <div class="product_type">
                                                            <div style="width:100%;">
                                                                <div style="width:30%;font-weight:bold;">
                                                                    Penjual:
                                                                </div>
                                                                <div class="profile-merchant">
                                                                    <?php
                                                                    if ($data->merchant_image == '' || $data->merchant_image == null) {
                                                                        $profile_picture_merchant = 'profilepicture_merchant/default_image.jpeg';
                                                                    } else {
                                                                        $profile_picture_merchant = $data->merchant_image;
                                                                    }

                                                                    $get_merchant = $this->main->get('merchants', ['id' => $data->branch_id]);
                                                                    $get_city = $this->main->get('cities', ['id' => $get_merchant->city]);
                                                                    ?>
                                                                    <div class="img-profile-merchant" style="width:13%;">
                                                                        <a href="<?php echo seo_url('merchant_home/view/' . $data->branch_id); ?>">
                                                                            <img src="<?php echo site_url('files/images/' . $profile_picture_merchant) ?>" style="width:100%;">
                                                                        </a>
                                                                    </div>
                                                                    <div class="text-profile-merchant" style="padding-top:1%;">
                                                                        <span class="text-name-profile-merchant"><a href="<?php echo seo_url('merchant_home/view/' . $data->branch_id); ?>"><?= $data->merchant_name ?></a></span>
                                                                        <span class="text-location-profile-merchant"><?= ($get_city->type == 'Kabupaten') ? 'Kab. ' . $get_city->name : $get_city->name; ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>

                                                </div>
                                                <input type="hidden" id="product" name="product" value="<?php echo encode($data->product_id); ?>">
                                                <input type="hidden" id="store" name="store" value="<?php echo encode($data->store_id); ?>">
                                                <input type="hidden" id="type_merchant" name="type_merchant" value="">
                                                <?php if($data->variation) : ?>
                                                    <?php 
                                                    if($data->store_type == 'principal') {
                                                        $product_option_branch = $this->main->get('products_principal_stock', ['id_option' => decode($product_option_id), 'branch_id' => $data->store_id, 'product_id' => $data->product_id]);
                                                        $quantity_option = $product_option_branch->quantity;
                                                    } else {
                                                        $option = $this->main->get('product_option', ['id' => decode($product_option_id)]);
                                                        if($option) {
                                                            $quantity_option = $option->quantity;
                                                        } else {
                                                            $quantity_option = $data->product_quantity;
                                                        }
                                                    }
                                                    ?>
                                                    <div id="quantity_notification_mobile">
                                                        <?php if ($quantity_option > 0) : ?>
                                                            <?php if ($quantity_option == 1) : ?>
                                                                <span style="margin-top:20px;display:inline-block;margin-bottom:20px;" class="col-sm-12">
                                                                    <b>Stok hampir habis!</b> Tersisa <?= $quantity_option ?>
                                                                </span>
                                                            <?php elseif ($quantity_option < 5) : ?>
                                                                <span style="margin-top:20px;display:inline-block;margin-bottom:20px;" class="col-sm-12">
                                                                    <b>Stok hampir habis!</b> Tersisa < 5
                                                                </span> 
                                                            <?php elseif ($quantity_option < 10) : ?> 
                                                                    <span style="margin-top:20px;display:inline-block;margin-bottom:20px;" class="col-sm-12">
                                                                        <b>Stok hampir habis!</b> Tersisa < 10
                                                                    </span> 
                                                            <?php endif; ?> 
                                                        <?php endif; ?> 
                                                    </div>
                                                    <div class="purchase-section multiple col-xs-12 purchase-section-mobile" style="display: inline-block;margin-top:<?= ($quantity_option < 10) ? '-20px;' : '0px;' ?>">


                                                        <div class="purchase-section-mobile-wrapper">

                                                            <div class="error-mobile" style="color: #d9534f;display:none;width:100%"></div>

                                                            <?php if ($data->merchant_name && $quantity_option < 1) { ?>
                                                                <h3 style="color: #d9534f;font-size:20px;margin-top:30px;">Stok habis</h3>
                                                            <?php
                                                            } elseif ($data->preorder == 1 || $quantity_option >= 1) {
                                                            ?>
                                                                <div class="quantity-wrapper" style="padding: unset;float: left;">
                                                                    <div class="quantity-field" style="margin-left: 0px !important;margin-top: 10px;border-radius: 5px;">

                                                                        <button onclick="down_qty($('input[name=qty]').data('qty'), <?php echo ($data->price_grosir || $data->price_reseller) ? 'true' : 'false';  ?>, <?php echo ($customer) ? ($customer->type == 'm' ? 'true' : 'false' ) : 'false'  ?>)" disabled id="btn_down_qty">
                                                                            <i class="fa fa-minus"></i>
                                                                        </button>
                                                                        <input type="text" id="qty" class="number_qty" name="qty" value="1" min="1" maxlength="3" class="item-quantity" style="-webkit-appearance: textfield;" data-qty="<?= $quantity_option ?>" data-type="<?php echo ($data->price_grosir || $data->price_reseller) ? 'true' : 'false'  ?>"data-merchant="<?php echo ($customer) ? ($customer->type == 'm' ? 'true' : 'false' ) : 'false'  ?>">
                                                                        <button onclick="up_qty($('input[name=qty]').data('qty'), <?php echo ($data->price_grosir || $data->price_reseller) ? 'true' : 'false';  ?>, <?php echo ($customer) ? ($customer->type == 'm' ? 'true' : 'false' ) : 'false'  ?>)" class="plus" id="btn_up_qty" <?= ($data->product_quantity == 1) ? 'disabled' : '' ?>>
                                                                            <i class="fa fa-plus"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>

                                                                <?php
                                                                if ($data->preorder == 1) {
                                                                    $button_buy = 'Pre Order';
                                                                } else {
                                                                    $button_buy = 'Beli Sekarang';
                                                                }
                                                                ?>

                                                                <div class="purchase" style="margin: 0 0 2rem 0;padding: 0;display: inline;">
                                                                    <div id="notification">
                                                                    </div>
                                                                    <?php if ($data->preorder == 1) : ?>
                                                                        <div>
                                                                            <span style="display: block;color: #d9534f;">Barang akan diproses selama <?= $data->preorder_time ?> hari</span>
                                                                        </div>
                                                                    <?php endif; ?>

                                                                    <?php if ($this->ion_auth->logged_in()) : ?>
                                                                        <button class="btn btn-md add-to-cart" id="btn-cart" type="button"><?= $button_buy ?></button>

                                                                        <a href="javascript:void(0);" class="btn btn-default btn-md" onclick="addtoCarts()" style="text-transform: uppercase;">
                                                                            Tambah Ke Keranjang
                                                                        </a>
                                                                    <?php else : ?>
                                                                        <button class="btn btn-md add-to-cart btn-cart-not-login" id="btn-cart-not" type="button" data-toggle="modal" data-target="#modal-guest" data-back="<?= seo_url('catalog/products/view/' . $data->product_id . '/' . $data->branch_id) ?>"><?= $button_buy ?></button>

                                                                        <a href="javascript:void(0);" class="btn btn-default btn-md btn-cart-not-login" style="text-transform: uppercase;" data-toggle="modal" data-target="#modal-guest" data-back="<?= seo_url('catalog/products/view/' . $data->product_id . '/' . $data->branch_id) ?>">
                                                                            Tambah Ke Keranjang
                                                                        </a>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <?php } ?>
                                                        </div>
                                                    </div>
                                                <?php else : ?>
                                                    <div id="quantity_notification">
                                                        <?php if ($data->product_quantity > 0) : ?>
                                                            <?php if ($data->product_quantity == 1) : ?>
                                                                <span style="margin-top:20px;display:inline-block;margin-bottom:20px;" class="col-sm-12">
                                                                    <b>Stok hampir habis!</b> Tersisa <?= $data->product_quantity ?>
                                                                </span>
                                                            <?php elseif ($data->product_quantity < 5) : ?>
                                                                <span style="margin-top:20px;display:inline-block;margin-bottom:20px;" class="col-sm-12">
                                                                    <b>Stok hampir habis!</b> Tersisa < 5
                                                                </span> 
                                                            <?php elseif ($data->product_quantity < 10) : ?> 
                                                                    <span style="margin-top:20px;display:inline-block;margin-bottom:20px;" class="col-sm-12">
                                                                        <b>Stok hampir habis!</b> Tersisa < 10
                                                                    </span> 
                                                            <?php endif; ?> 
                                                        <?php endif; ?> 
                                                    </div>
                                                    <div class="purchase-section multiple col-xs-12 purchase-section-mobile" style="display: inline-block;margin-top:<?= ($data->product_quantity < 10) ? '-20px;' : '0px;' ?>">

                                                        <div class="purchase-section-mobile-wrapper">

                                                            <div class="error-mobile" style="color: #d9534f;display:none;width:100%"></div>

                                                            <?php if ($data->merchant_name && $data->product_quantity < 1) { ?>
                                                                <h3 style="color: #d9534f;font-size:20px;margin-top:30px;">Stok habis</h3>
                                                            <?php
                                                            } elseif ($data->preorder == 1 || $data->product_quantity >= 1) {
                                                            ?>
                                                                <div class="quantity-wrapper" style="padding: unset;float: left;">
                                                                    <div class="quantity-field" style="margin-left: 0px !important;margin-top: 10px;border-radius: 5px;">

                                                                        <button onclick="down_qty(<?= $data->product_quantity ?>, <?php echo ($data->price_grosir || $data->price_reseller) ? 'true' : 'false';  ?>, <?php echo ($customer) ? ($customer->type == 'm' ? 'true' : 'false' ) : 'false'  ?>)" disabled id="btn_down_qty">
                                                                            <i class="fa fa-minus"></i>
                                                                        </button>
                                                                        <input type="text" id="qty" class="number_qty" name="qty" value="1" min="1" maxlength="3" class="item-quantity" style="-webkit-appearance: textfield;" data-qty="<?= $data->product_quantity ?>" data-type="<?php echo ($data->price_grosir || $data->price_reseller) ? 'true' : 'false'  ?>"data-merchant="<?php echo ($customer) ? ($customer->type == 'm' ? 'true' : 'false' ) : 'false'  ?>">
                                                                        <button onclick="up_qty(<?= $data->product_quantity ?>, <?php echo ($data->price_grosir || $data->price_reseller) ? 'true' : 'false';  ?>, <?php echo ($customer) ? ($customer->type == 'm' ? 'true' : 'false' ) : 'false'  ?>)" class="plus" id="btn_up_qty" <?= ($data->product_quantity == 1) ? 'disabled' : '' ?>>
                                                                            <i class="fa fa-plus"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>

                                                                <?php
                                                                if ($data->preorder == 1) {
                                                                    $button_buy = 'Pre Order';
                                                                } else {
                                                                    $button_buy = 'Beli Sekarang';
                                                                }
                                                                ?>

                                                                <div class="purchase" style="margin: 0 0 2rem 0;padding: 0;display: inline;">
                                                                    <?php if ($data->preorder == 1) : ?>
                                                                        <div>
                                                                            <span style="display: block;color: #d9534f;">Barang akan diproses selama <?= $data->preorder_time ?> hari</span>
                                                                        </div>
                                                                    <?php endif; ?>

                                                                    <?php if ($this->ion_auth->logged_in()) : ?>
                                                                        <button class="btn btn-md add-to-cart" id="btn-cart" type="button"><?= $button_buy ?></button>

                                                                        <a href="javascript:void(0);" class="btn btn-default btn-md" onclick="addtoCarts()" style="text-transform: uppercase;">
                                                                            Tambah Ke Keranjang
                                                                        </a>
                                                                    <?php else : ?>
                                                                        <button class="btn btn-md add-to-cart btn-cart-not-login" id="btn-cart-not" type="button" data-toggle="modal" data-target="#modal-guest" data-back="<?= seo_url('catalog/products/view/' . $data->product_id . '/' . $data->branch_id) ?>"><?= $button_buy ?></button>

                                                                        <a href="javascript:void(0);" class="btn btn-default btn-md btn-cart-not-login" style="text-transform: uppercase;" data-toggle="modal" data-target="#modal-guest" data-back="<?= seo_url('catalog/products/view/' . $data->product_id . '/' . $data->branch_id) ?>">
                                                                            Tambah Ke Keranjang
                                                                        </a>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <?php } ?>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                        </div>
                                    </div>
                                <?php else : ?>
                                    <div class="col-md-8" id="product-information" style="padding-left:0px;padding-top:0px;">
                                        <div class="col-md-7">
                                            <h1 itemprop="name" class="title" style="font-weight: 550;"><?php echo $data->name; ?></h1>
                                            <?php 
                                            if($data->variation) {
                                                if(isset($options)) {
                                                    $product_option_id = $options_setting['product_option']->id;
                                                } else {
                                                    $product_option_id = '';
                                                }
                                            } else {
                                                $product_option_id = '';
                                            }
                                            ?>

                                            <input type="hidden" name="product_option" value="<?php echo $product_option_id ?>">

                                            <div class="col-md-12 grochire-cover" id="grochire-cover" style="padding: 0;">
                                                <?php if ($data->price_grosir) : ?>
                                                    <div class="grochire-detail col-md-8 col-sm-8 grochire-div">
                                                    <?php if($data->variation) {
                                                        if($data->store_type == 'principal') {
                                                            $product_option_branch = $this->main->get('products_principal_stock', ['product_id' => $data->product_id, 'branch_id' => $data->branch_id, 'id_option' => decode($product_option_id)]);
                                                            $data->quantity = $product_option_branch->quantity;
                                                        } else {
                                                            $data_option = $this->main->get('product_option', ['id' => decode($product_option_id)]);
                                                            $data->quantity = $data_option->quantity;
                                                        }
                                                    } else {
                                                        $data->quantity = $data->quantity;
                                                    } ?>
                                                    <?php
                                                        $product_branch = '';
                                                        if($data->store_type == 'principal') {
                                                            $product_branch = $this->main->get('products_principal_stock', ['product_id' => $data->product_id, 'branch_id' => $data->branch_id]);
                                                            $data_price_level = json_decode($product_branch->id_price_level);
                                                            $price_level_temp = [];
                                                            if($data_price_level) {
                                                                foreach($data_price_level->data as $id) {
                                                                    $price_level_ = $this->main->get('product_price_level', ['id' => $id]);
                                                                    if($price_level_) {
                                                                        array_push($price_level_temp, $price_level_);
                                                                    }
                                                                }
                                                                $price_level = $price_level_temp;
                                                            } else {
                                                                $price_level = $this->product->product_price_level($data->id);
                                                            }
                                                        } else {
                                                            $price_level = $this->product->product_price_level($data->id);
                                                        }
                                                    ?>
                                                    <?php if($price_level) : ?>
                                                        <?php 
                                                        $count_price_level = 0;
                                                        if($data->store_type == 'merchant') {
                                                            $price_level = $price_level->result();
                                                        }
                                                        foreach($price_level as $pl_temp) {
                                                            if($pl_temp->min_qty <= $data->quantity) {
                                                                $count_price_level++;
                                                            }
                                                        }
                                                        ?>
                                                        <?php
                                                            $min_qty_price_level = $price_level[0]->min_qty;
                                                        ?>
                                                        <?php if($min_qty_price_level <= $data->quantity) : ?>
                                                            <?php
                                                                $first_price_level = $price_level[0];
                                                                $end_price_level = $first_price_level->price;
                                                                
                                                                if ($inPromo) {
                                                                    $promo_data = json_decode($data->promo_data, TRUE);
    
                                                                    if ($promo_data['type'] == 'P') {
                                                                        $discount_amount = round($first_price_level->price * $promo_data['discount']) / 100;
                                                                        $end_price_level = $first_price_level->price - $discount_amount;
                                                                    } else {
                                                                        if ($promo_data['discount'] >= $first_price_level->price) {
                                                                            $end_price_level = $promo_data['discount'];
                                                                        } else {
                                                                            $end_price_level = intval($first_price_level->price) - intval($promo_data['discount']);
                                                                        }
                                                                    }
                                                                }
                                                                $check_dropdown = 0;
                                                                if($count_price_level > 2) {
                                                                    $num_rows_price_level = 2;
                                                                } else {
                                                                    $num_rows_price_level = $count_price_level;
                                                                }
                                                            ?>

                                                            <!-- <div class="grochire">
                                                                <p>Beli <?php //echo $price_level->row(0)->min_qty; ?> lebih murah <b>@ <?php //echo rupiah($end_price_level); ?></b>.</p>
                                                            </div> -->
                                                                <span style="font-size: 14px;margin-top: 10px;display: block;margin-bottom: 2px;font-weight: 500;">Harga Grosir</span>
                                                                <?php $count_row = floor(12 / ($num_rows_price_level + 1)); ?>
                                                                <div class="col-md-<?php echo $count_row; ?> col-sm-<?php echo $count_row; ?> grochire-qty active" data-id="<?php echo encode($data->id) ?>" data-qty="<?php echo (int)1; ?>" data-store_id="<?= encode($data->store_id) ?>" data-type="grochire" onclick="change_grochire(this)">
                                                                    <span>1</span>
                                                                    <span><?php echo rupiah($price); ?></span>
                                                                </div>
                                                                <?php foreach ($price_level as $row => $pl) : ?>
                                                                    <?php
                                                                        $end_price_level = $pl->price;
                                                                        if ($inPromo) {
                                                                            if($data->store_type == 'principal' && $product_branch != '') {
                                                                                $promo_data = json_decode($product_branch->promo_data, TRUE);
                                                                            } else {
                                                                                $promo_data = json_decode($data->promo_data, TRUE);
                                                                            }

                                                                            if ($promo_data['type'] == 'P') {
                                                                                $end_price_level = $pl->price - (round($pl->price * $promo_data['discount']) / 100);
                                                                            } else {
                                                                                if ($promo_data['discount'] >= $pl->price) {
                                                                                    $end_price_level = $promo_data['discount'];
                                                                                } else {
                                                                                    $end_price_level = $pl->price - $promo_data['discount'];
                                                                                }
                                                                            }

                                                                            $today = date('Y-m-d');
                                                                            $today=date('Y-m-d', strtotime($today));
                                                                            $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                                                            $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

                                                                            if (($today >= $DateBegin) && ($today <= $DateEnd)){
                                                                                $end_price_level = $end_price_level;
                                                                            } else {
                                                                                $end_price_level = $pl->price;
                                                                            }
                                                                        } else {
                                                                            $end_price_level = $pl->price;
                                                                        }
                                                                    ?>
                                                                    <?php if($pl->min_qty <= $data->quantity) : ?>
                                                                        <?php $check_dropdown++; ?>
                                                                        <?php if($row <= 1) : ?>
                                                                            <div class="col-md-<?php echo $count_row; ?> col-sm-<?php echo $count_row; ?> grochire-qty" data-id="<?php echo encode($data->id) ?>" data-qty="<?php echo $pl->min_qty; ?>" data-type="grochire" data-product_qty="<?php echo $data->product_quantity ?>" data-store_id="<?= encode($data->store_id) ?>" onclick="change_grochire(this)">
                                                                                <span><?php echo ($row + 1 == count($price_level)) ? '>= '. $pl->min_qty : $pl->min_qty .' - '. ($price_level[$row + 1]->min_qty - 1); ?></span>
                                                                                <span><?php echo rupiah($end_price_level); ?></span>
                                                                            </div>
                                                                        <?php else : ?>
                                                                            <div class="col-md-<?php echo $count_row; ?> col-sm-<?php echo $count_row; ?> grochire-qty see-other-grochire" data-id="<?php echo encode($data->id) ?>" data-qty="<?php echo $pl->min_qty; ?>" data-type="grochire" data-product_qty="<?php echo $data->product_quantity ?>" data-store_id="<?= encode($data->store_id) ?>" style="display: none;" onclick="change_grochire(this)">
                                                                                <span><?php echo ($row + 1 == count($price_level)) ? '>= '. $pl->min_qty : $pl->min_qty .' - '. ($price_level[$row + 1]->min_qty - 1); ?></span>
                                                                                <span><?php echo rupiah($end_price_level); ?></span>
                                                                            </div>
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                <?php endforeach; ?>
                                                                <?php if($check_dropdown > 2) : ?>
                                                                    <div class="col-md-12 col-sm-12" style="text-align: center;margin-top: 2px;">
                                                                        <a href="javascript:void(0)" id="see_others_grochire_price_link" data-type="show" onclick="see_others_grochire(this)">Tampilkan Semua</a>
                                                                    </div>
                                                                <?php endif; ?>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                    </div>
                                                <?php endif; ?>


                                                <?php if ($data->price_reseller) : ?>
                                                    <div class="grochire-detail col-md-4 col-sm-4 reseller-div" style="padding-right: 0px;">
                                                        <?php 
                                                            $product_branch_reseller = '';
                                                            if($data->store_type == 'principal') {
                                                                $product_branch_reseller = $this->main->get('products_principal_stock', ['product_id' => $data->product_id, 'branch_id' => $data->branch_id]);
                                                                $data_price_reseller = json_decode($product_branch_reseller->id_price_reseller);
                                                                $price_reseller_temp = [];
                                                                if($data_price_reseller) {
                                                                    foreach($data_price_reseller->data as $id) {
                                                                        $price_reseller_ = $this->main->get('product_price_reseller', ['id' => $id]);
                                                                        if($price_reseller_) {
                                                                            array_push($price_reseller_temp, $price_reseller_);
                                                                        }
                                                                    }
                                                                    if(count($price_reseller_temp) > 0) {
                                                                        $reseller = $price_reseller_temp;
                                                                    } else {
                                                                        $reseller = false;
                                                                    }
                                                                } else {
                                                                    $reseller_ = $this->main->gets('product_price_reseller', ['product' => $data->id]);
                                                                }
                                                            } else {
                                                                $reseller_ = $this->main->gets('product_price_reseller', ['product' => $data->id]);
                                                                if($reseller_) {
                                                                    $reseller = $reseller_->result();
                                                                } else {
                                                                    $reseller = false;
                                                                }
                                                            }
                                                        ?>
                                                        <?php if($reseller) : ?>
                                                            <?php if($data->variation) {
                                                                if($data->store_type == 'principal') {
                                                                    $product_option_branch = $this->main->get('products_principal_stock', ['product_id' => $data->product_id, 'branch_id' => $data->branch_id, 'id_option' => decode($product_option_id)]);
                                                                     $data->quantity = $product_option_branch->quantity;
                                                                } else {
                                                                    $data_option = $this->main->get('product_option', ['id' => decode($product_option_id)]);
                                                                    $data->quantity = $data_option->quantity;
                                                                }
                                                            } else {
                                                                $data->quantity = $data->quantity;
                                                            } ?>
                                                            <?php
                                                                $min_qty = $reseller[0]->min_qty;
                                                            ?>
                                                            <?php 
                                                            $count_num_row_reseller = 0;
                                                            foreach($reseller as $rs_temp) {
                                                                if($rs_temp->min_qty <= $data->quantity) {
                                                                    $count_num_row_reseller++;
                                                                }
                                                            }
                                                            ?>
                                                            <?php if($min_qty <= $data->quantity) : ?>
                                                                <span style="font-size: 14px;margin-top: 10px;display: block;margin-bottom: 2px;font-weight: 500;">Harga Reseller</span>
                                                                <?php 
                                                                if($count_num_row_reseller > 2) {
                                                                    $num_rows_reseller = 2;
                                                                } else {
                                                                    $num_rows_reseller = $count_num_row_reseller;
                                                                }
                                                                $check_dropdown_reseller = 0;
                                                                ?>
                                                                <?php $count_reseller = floor(12 / $num_rows_reseller); ?>
                                                                <?php foreach ($reseller as $row => $rs) : ?>
                                                                    <?php 
                                                                        $end_price_level = $rs->price;
                                                                        if ($inPromo) {
                                                                            if($data->store_type == 'principal') {
                                                                                $promo_data = json_decode($product_branch_reseller->promo_data, TRUE);
                                                                            } else {
                                                                                $promo_data = json_decode($data->promo_data, TRUE);
                                                                            }

                                                                            if ($promo_data['type'] == 'P') {
                                                                                $end_price_level = $rs->price - (round($rs->price * $promo_data['discount']) / 100);
                                                                            } else {
                                                                                if ($promo_data['discount'] >= $rs->price) {
                                                                                    $end_price_level = $promo_data['discount'];
                                                                                } else {
                                                                                    $end_price_level = $rs->price - $promo_data['discount'];
                                                                                }
                                                                            }

                                                                            $today = date('Y-m-d');
                                                                            $today=date('Y-m-d', strtotime($today));
                                                                            $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                                                            $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

                                                                            if (($today >= $DateBegin) && ($today <= $DateEnd)){
                                                                                $end_price_level = $end_price_level;
                                                                            } else {
                                                                                $end_price_level = $rs->price;
                                                                            }
                                                                        } else {
                                                                            $end_price_level = $rs->price;
                                                                        }
                                                                    ?>
                                                                    <?php if($rs->min_qty <= $data->quantity) : ?>
                                                                        <?php $check_dropdown_reseller++; ?>
                                                                        <?php if($row <= 1) : ?>
                                                                            <div class="col-md-<?php echo $count_reseller; ?> col-sm-<?php echo $count_reseller; ?> grochire-qty reseller-item" data-id="<?php echo encode($data->id) ?>" data-qty="<?php echo $rs->min_qty; ?>" data-type="reseller" data-product_qty="<?php echo $data->product_quantity ?>" data-store_id="<?= encode($data->store_id) ?>" data-login="<?php echo ($this->ion_auth->logged_in()) ? true : false ?>" data-mobile="false" onclick="change_reseller(this)">
                                                                                <span><?php echo (($row + 1) == count($reseller)) ? '>= '. $rs->min_qty : $rs->min_qty .' - '. ($reseller[$row + 1]->min_qty - 1); ?></span>
                                                                                <span><?php echo rupiah($end_price_level); ?></span>
                                                                            </div>
                                                                        <?php else : ?>
                                                                            <div class="col-md-<?php echo $count_reseller; ?> col-sm-<?php echo $count_reseller; ?> grochire-qty other-reseller-item reseller-item" data-id="<?php echo encode($data->id) ?>" data-qty="<?php echo $rs->min_qty; ?>" data-type="reseller" data-product_qty="<?php echo $data->product_quantity ?>" data-store_id="<?= encode($data->store_id) ?>" data-login="<?php echo ($this->ion_auth->logged_in()) ? true : false ?>" style="display: none;" data-mobile="false" onclick="change_reseller(this)">
                                                                                <span><?php echo (($row + 1) == count($reseller)) ? '>= '. $rs->min_qty : $rs->min_qty .' - '. ($reseller[$row + 1]->min_qty - 1); ?></span>
                                                                                <span><?php echo rupiah($end_price_level); ?></span>
                                                                            </div>
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                <?php endforeach; ?>
                                                                <?php if($check_dropdown_reseller > 2) : ?>
                                                                    <div class="col-md-12 col-sm-12" style="text-align: center;margin-top: 2px;">
                                                                        <a href="javascript:void(0)" id="see_others_grochire_reseller" data-type="show" onclick="see_others_reseller(this)">Tampilkan Semua</a>
                                                                    </div>
                                                                <?php endif; ?>
                                                                
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                <?php endif; ?>
                                            </div>

                                            <div class="variants col-md-12 col-sm-12" style="padding-left: 0px; padding-right: 0px;">
                                                <div class="product-options row">

                                                    <?php if ($data->brand) { ?>
                                                        <div class="vendor-type col-md-12 col-sm-12">
                                                            <span>Merek : <?php echo $data->brand_name; ?></span>
                                                        </div>
                                                    <?php } ?>

                                                    <div class="vendor-type col-md-12 col-sm-12">
                                                        <span>Berat : <?php echo $data->weight; ?> gram</span>
                                                    </div>

                                                    <?php //if ($data->guarantee) : ?>
                                                        <!-- <div class="vendor-type col-md-12 col-sm-12">
                                                            <span>Garansi : <?//= $data->guarantee ?></span>
                                                        </div> -->
                                                    <?php //endif; ?>

                                                    <div class="rating-star col-md-6 col-sm-6" style="margin-bottom: 2rem;">
                                                        <span class="spr-badge">
                                                            <span class="spr-badge-caption">
                                                            </span>

                                                            <span class="spr-starrating spr-badge-starrating">

                                                                <?php for ($i = 1; $i <= $rating->rating; $i++) { ?>
                                                                    <i class="spr-icon spr-icon-star"></i>
                                                                <?php
                                                                }

                                                                for ($i = $rating->rating + 1; $i <= 5; $i++) {
                                                                ?>
                                                                    <i class="spr-icon spr-icon-star-empty"></i>
                                                                <?php } ?>

                                                            </span>
                                                        </span>
                                                    </div>
                                                    <?php if ($data->free_ongkir == 1) { ?>
                                                        <span class="product_free_ongkir">
                                                            <img height="38" width="80" src="<?php echo site_url('assets/frontend/images/FREE ONGKIR ICON-02-01.png'); ?>">
                                                        </span>
                                                    <?php } ?>

                                                    <?php if($data->variation) : ?>
                                                        <div id="product-option-container">
                                                        <?php if ($is_option) { ?>
                                                            <div class="product-type col-sm-12 col-md-12" id="product-option"><?php echo $options; ?></div>
                                                        <?php } ?>
                                                        </div>
                                                    <?php endif; ?>

                                                    <div id="notification" class="col-sm-12 col-md-12">
                                                
                                                    </div>

                                                    <hr class="divider">

                                                    <div class="col-md-12" style="margin-bottom: 2rem;">
                                                        <b>Deskripsi Singkat :</b> <br><?= $data->short_description ?>
                                                    </div>

                                                    <hr class="divider">

                                                    <div class="col-md-12">
                                                        <b>Estimasi ongkos kirim</b>
                                                        <table id="table_cek_ongkir">
                                                            <thead>
                                                                <tr>
                                                                    <th style="padding-left:10px;padding-right:10px;text-transform: capitalize;text-align:left;">
                                                                        <label class="control-label">Kecamatan</label>
                                                                        <select style="width:100%;" name="district" id="district" class="form-control district">
                                                                        </select>
                                                                    </th>
                                                                    <th style="padding-left:10px;padding-right:10px;text-transform: capitalize;text-align:left;">
                                                                        <label class="control-label">Berat</label>
                                                                        <b style="text-transform: lowercase;"><?php echo $data->weight; ?>gr</b>
                                                                        <input type="hidden" id="berat" value="<?php echo $data->weight; ?>">
                                                                    </th>
                                                                    <th style="padding-left:10px;padding-right:10px;">
                                                                        <a style="color:#78c93a; cursor:pointer;" onclick="districts()"><b>Hitung</b></a>
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody></tbody>
                                                        </table>
                                                    </div>
                                                    <!-- END OF CEK ONGKIR -->
                                                </div>
                                                <!-- END OF PRODUCT OPTIONS -->
                                            </div>

                                        </div>

                                        <!-- SHIPPING -->
                                        <div class="col-md-5">
                                            <div class="div_kanan" style="overflow: auto;position: relative; border-radius: 5px;">
                                                <div class="add-to-wishlist-desktop" style="position: absolute;top: 20px; right: 20px;z-index: 1;">

                                                    <?php if ($this->ion_auth->logged_in()) : ?>
                                                        <?php if ($mywish) : ?>
                                                            <a href="javascript:void(0);" class="wish-list" onclick="add_to_wishlist_new('<?php echo encode($data->product_id); ?>', '<?php echo encode($data->store_id) ?>', '<?= encode($this->data['user']->id) ?>')" style="font-size:2rem;">
                                                                <i class="fa fa-heart" aria-hidden="true" style="color: #d9534f;"></i>
                                                            </a>
                                                        <?php else : ?>
                                                            <a href="javascript:void(0);" class="wish-list" onclick="add_to_wishlist_new('<?php echo encode($data->product_id); ?>', '<?php echo encode($data->store_id) ?>', '<?= encode($this->data['user']->id) ?>')" style="font-size:2rem;">
                                                                <i class="fa fa-heart" aria-hidden="true" style="color: #CCC;"></i>
                                                            </a>
                                                        <?php endif; ?>
                                                    <?php else : ?>
                                                        <a href="javascript:void(0);" class="wish-list" style="font-size:2rem;" data-toggle="modal" data-target="#modal-guest" data-back="<?= current_url() ?>">
                                                            <i class="fa fa-heart" aria-hidden="true" style="color: #CCC;"></i>
                                                        </a>
                                                    <?php endif; ?>

                                                </div>
                                                <?php if ($inPromo) : ?>
                                                    <div style="margin:10px auto" class="disc-product-price col-sm-12">
                                                        <span style="background-color:#d9534f;color:white;padding:5px 10px;font-weight: 550;border-radius: 3px;">
                                                            <?php echo $label_disc; ?>
                                                        </span>
                                                        <span class="money" style="text-decoration: line-through;font-size:12px;color:#8b8f8b;margin-left: 5px;">
                                                            <?php echo rupiah($old_price); ?>
                                                        </span>
                                                    </div>
                                                <?php endif; ?>
                                                <div class="product-price col-sm-12">
                                                    <h2 class="price" style="color: #B7CD69 !important;font-weight: bold;font-size: 20px;outline-color: red;margin-top: 15px;"><?php echo rupiah($price); ?></h2>
                                                </div>
                                                
                                                <!-- <?php //if ($reseller) : ?>
                                                    <div class="grochire">
                                                        <p class="text-center">atau beli sebagai reseller:</p>
                                                    </div>

                                                    <?php //$count_reseller = floor(12 / $reseller->num_rows()); ?>
                                                    <?php //foreach ($reseller->result() as $row => $rs) : ?>
                                                        <?php 
                                                            // if ($rs->type != 'grochire') {
                                                            //     $end_price_level = $rs->price;
                                                            //     if ($inPromo) {
                                                            //         $promo_data = json_decode($data->promo_data, TRUE);
                                                        
                                                            //         if ($promo_data['type'] == 'P') {
                                                            //             $end_price_level = $rs->price - (round($rs->price * $promo_data['discount']) / 100);
                                                            //         } else {
                                                            //             if ($promo_data['discount'] >= $rs->price) {
                                                            //                 $end_price_level = $promo_data['discount'];
                                                            //             } else {
                                                            //                 $end_price_level = $rs->price - $promo_data['discount'];
                                                            //             }
                                                            //         }
                                                        
                                                            //         $today = date('Y-m-d');
                                                            //         $today=date('Y-m-d', strtotime($today));
                                                            //         $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                                            //         $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                                                        
                                                            //         if (($today >= $DateBegin) && ($today <= $DateEnd)){
                                                            //             $price = $end_price_level;
                                                            //         } else {
                                                            //             $price = $rs->price;
                                                            //         }
                                                            //     } else {
                                                            //         $price = $rs->price;
                                                            //     }
                                                            // }
                                                            // $end_price_level = $rs->price;
                                                            // if ($inPromo) {
                                                            //     $promo_data = json_decode($data->promo_data, TRUE);
                                                    
                                                            //     if ($promo_data['type'] == 'P') {
                                                            //         $end_price_level = $rs->price - (round($rs->price * $promo_data['discount']) / 100);
                                                            //     } else {
                                                            //         if ($promo_data['discount'] >= $rs->price) {
                                                            //             $end_price_level = $promo_data['discount'];
                                                            //         } else {
                                                            //             $end_price_level = $rs->price - $promo_data['discount'];
                                                            //         }
                                                            //     }
                                                    
                                                            //     $today = date('Y-m-d');
                                                            //     $today=date('Y-m-d', strtotime($today));
                                                            //     $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                                            //     $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                                                    
                                                            //     if (($today >= $DateBegin) && ($today <= $DateEnd)){
                                                            //         $price = $end_price_level;
                                                            //     } else {
                                                            //         $price = $rs->price;
                                                            //     }
                                                            // } else {
                                                            //     $price = $rs->price;
                                                            // }
                                                        ?>
                                                        <?php //if($rs->min_qty <= $data->product_quantity) : ?>
                                                            <div class="col-md-<?php //echo $count_reseller; ?> grochire-qty reseller-item" data-id="<?php //echo encode($data->id) ?>" data-qty="<?php //echo $rs->min_qty; ?>" data-type="reseller" data-product_qty="<?php //echo $data->product_quantity ?>">
                                                                <span><?php //echo (($row + 1) == $reseller->num_rows()) ? '>= '. $rs->min_qty : $rs->min_qty .' - '. ($reseller->row($row + 1)->min_qty - 1); ?></span>
                                                                <span><?php //echo rupiah($price); ?></span>
                                                            </div>
                                                        <?php //endif; ?>
                                                    <?php //endforeach; ?>
                                                <?php //endif; ?> -->

                                                <?php if ($data->guarantee) { ?>
                                                    <br>

                                                    <span class="verified-title col-sm-12">
                                                        <b>Garansi : <?php echo ucwords($data->guarantee) ?></b>
                                                    </span>
                                                <?php } ?>

                                                <div class="vendor-type col-sm-12" style="margin-top: 10px;">

                                                    <?php if ($data->merchant_name) { ?>
                                                        <div class="product_type">
                                                            <div style="width:100%;">
                                                                <div style="width:30%;font-weight:bold;">
                                                                    Penjual:
                                                                </div>
                                                                <div class="profile-merchant">
                                                                    <?php
                                                                    if ($data->merchant_image == '' || $data->merchant_image == null) {
                                                                        $profile_picture_merchant = 'profilepicture_merchant/default_image.jpeg';
                                                                    } else {
                                                                        $profile_picture_merchant = $data->merchant_image;
                                                                    }

                                                                    $get_merchant = $this->main->get('merchants', ['id' => $data->branch_id]);
                                                                    $get_city = $this->main->get('cities', ['id' => $get_merchant->city]);
                                                                    ?>
                                                                    <div class="img-profile-merchant">
                                                                        <a href="<?php echo seo_url('merchant_home/view/' . $data->branch_id); ?>">
                                                                            <img src="<?php echo site_url('files/images/' . $profile_picture_merchant) ?>" style="width:100%;">
                                                                        </a>
                                                                    </div>
                                                                    <div class="text-profile-merchant">
                                                                        <span class="text-name-profile-merchant"><a href="<?php echo seo_url('merchant_home/view/' . $data->branch_id); ?>"><?= $data->merchant_name ?></a></span>
                                                                        <span class="text-location-profile-merchant"><?= ($get_city->type == 'Kabupaten') ? 'Kab. ' . $get_city->name : $get_city->name; ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>

                                                </div>
                                                <input type="hidden" id="min_buy" value="<?= $data->min_buy ?>">
                                                <?php if($data->variation) : ?>
                                                    <?php 
                                                    if($data->store_type == 'principal') {
                                                        $product_option_branch = $this->main->get('products_principal_stock', ['branch_id' => $data->store_id, 'product_id' => $data->product_id, 'id_option' => decode($product_option_id)]);
                                                        if($product_option_branch) {
                                                            $quantity_option = $product_option_branch->quantity;
                                                        } else {
                                                            $quantity_option = $data->product_quantity;
                                                        }
                                                    } else {
                                                        $option = $this->main->get('product_option', ['id' => decode($product_option_id)]);  
                                                        if($option) {
                                                            $quantity_option = $option->quantity;
                                                        } else {
                                                            $quantity_option = $data->product_quantity;
                                                        }
                                                    }
                                                    ?>
                                                    <div id="quantity_notification">
                                                        <?php if ($quantity_option > 0) : ?>
                                                            <?php if ($quantity_option == 1) : ?>
                                                                <span style="margin-top:20px;display:inline-block;" class="col-sm-12">
                                                                    <b>Stok hampir habis!</b> Tersisa <?= $quantity_option ?>
                                                                </span>
                                                            <?php elseif ($quantity_option < 5) : ?>
                                                                <span style="margin-top:20px;display:inline-block;" class="col-sm-12">
                                                                    <b>Stok hampir habis!</b> Tersisa < 5
                                                                </span> 
                                                            <?php elseif ($quantity_option < 10) : ?> 
                                                                <span style="margin-top:20px;display:inline-block;" class="col-sm-12">
                                                                    <b>Stok hampir habis!</b> Tersisa < 10
                                                                </span> 
                                                            <?php endif; ?> 
                                                        <?php endif; ?> 
                                                    </div>
                                                    <div class="purchase-section multiple col-sm-12 purchase-section-desktop" style="display: inline-block;margin-top:<?= ($quantity_option < 10) ? '-20px;' : '0px;' ?>">
                                                        <?php if($quantity_option == 0) : ?>
                                                            <h3 style="color: #d9534f;font-size:20px;margin-top:30px;">Stok habis</h3>
                                                        <?php endif; ?>
                                                        <div id="purchase-section-desktop-wrapper" style="display:<?php echo ($quantity_option > 1 || $data->preorder == 1) ? 'block;' : 'none;'; ?>">

                                                            <div class="quantity-wrapper" style="margin-top: 12px;margin-bottom: 25px;padding: unset;float: left;">
                                                                <div class="quantity-field" style="margin-left: 0px !important;margin-top: 10px;border-radius: 5px;">
                                                                    <button onclick="down_qty($('input[name=qty]').data('qty'), <?php echo ($data->price_grosir || $data->price_reseller) ? 'true' : 'false';  ?>, <?php echo ($customer) ? ($customer->type == 'm' ? 'true' : 'false' ) : 'false'  ?>)" disabled id="btn_down_qty">
                                                                        <i class="fa fa-minus"></i>
                                                                    </button>
                                                                    <input type="text" id="qty" class="number_qty" name="qty" value="<?= $data->min_buy ?>" min="1" maxlength="3" class="item-quantity" style="-webkit-appearance: textfield;" data-qty="<?= $quantity_option ?>" data-type="<?php echo ($data->price_grosir || $data->price_reseller) ? 'true' : 'false'  ?>"data-merchant="<?php echo ($customer) ? ($customer->type == 'm' ? 'true' : 'false' ) : 'false'  ?>">
                                                                    <button onclick="up_qty($('input[name=qty]').data('qty'), <?php echo ($data->price_grosir || $data->price_reseller) ? 'true' : 'false';  ?>, <?php echo ($customer) ? ($customer->type == 'm' ? 'true' : 'false' ) : 'false'  ?>)" class="plus" id="btn_up_qty" <?= ($quantity_option == 1) ? 'disabled' : '' ?>>
                                                                        <i class="fa fa-plus"></i>
                                                                    </button>
                                                                </div>
                                                            </div>

                                                            <?php
                                                            if ($data->preorder == 1) {
                                                                $button_buy = 'Pre Order';
                                                            } else if($data->preorder == 2) {
                                                                $button_buy = 'Coming Soon';
                                                            } else {
                                                                $button_buy = 'Beli Sekarang';
                                                            }
                                                            ?>

                                                            <div class="purchase" style="margin: 0 0 2rem 0;padding: 0;display: inline">
                                                                <div id="err_qty" style="display:none;color:#d9534f;margin-top: 10px;margin-bottom: -10px !important;margin-top:-22px;position:absolute;"></div>
                                                                <?php if($data->min_buy > 1) : ?>
                                                                <div style="margin-top: -20px;margin-bottom: 10px;" id="min_buy_notification">
                                                                    <span style="font-weight: 550">Minimal pembelian <?= $data->min_buy ?> produk</span>
                                                                </div>
                                                                <?php endif; ?>
                                                                <?php if ($data->preorder == 1) : ?>
                                                                    <div>
                                                                        <span style="display: block;color: #d9534f;">Barang akan diproses selama <?= $data->preorder_time ?> hari</span>
                                                                    </div>
                                                                <?php elseif($data->preorder == 2) : ?>
                                                                    <?php 
                                                                    $coming_soon_time = strtotime($data->coming_soon_time);
                                                                    $now = time();
                                                                    ?>
                                                                    <?php if($now <= $coming_soon_time) : ?>
                                                                        <div>
                                                                            <span style="display: block;color: #d9534f;">
                                                                                Barang akan ada pada <b><?php echo tgl_indo(date('Y-m-d', strtotime($data->coming_soon_time))); ?></b>
                                                                            </span>
                                                                        </div>
                                                                    <?php endif; ?>
                                                                <?php endif; ?>

                                                                <?php if ($this->ion_auth->logged_in()) : ?>
                                                                    <button class="btn btn-md add-to-cart" id="btn-cart" type="button" <?php echo ($data->preorder == 2) ? 'disabled' : '' ?>><?= $button_buy ?></button>

                                                                    <?php if($data->preorder != 2) : ?>
                                                                        <a href="javascript:void(0);" class="btn btn-default btn-md" onclick="addtoCarts()" style="text-transform: uppercase;">
                                                                            Tambah Ke Keranjang
                                                                        </a>
                                                                    <?php endif; ?>
                                                                <?php else : ?>
                                                                    <button class="btn btn-md add-to-cart btn-cart-not-login" id="btn-cart-not" type="button" data-toggle="modal" data-target="#modal-guest" data-back="<?= seo_url('catalog/products/view/' . $data->product_id . '/' . $data->branch_id) ?>"><?= $button_buy ?></button>

                                                                    <?php if($data->preorder != 2) : ?>
                                                                        <a href="javascript:void(0);" class="btn btn-default btn-md btn-cart-not-login" style="text-transform: uppercase;" data-toggle="modal" data-target="#modal-guest" data-back="<?= seo_url('catalog/products/view/' . $data->product_id . '/' . $data->branch_id) ?>">
                                                                            Tambah Ke Keranjang
                                                                        </a>
                                                                    <?php endif; ?>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>

                                                    </div>
                                                <?php else : ?>
                                                    <div id="quantity_notification">
                                                        <?php if ($data->product_quantity > 0 && $data->min_buy <= $data->product_quantity) : ?>
                                                            <?php if ($data->product_quantity == 1) : ?>
                                                                <span style="margin-top:20px;display:inline-block;" class="col-sm-12">
                                                                    <b>Stok hampir habis!</b> Tersisa <?= $data->product_quantity ?>
                                                                </span>
                                                            <?php elseif ($data->product_quantity < 5) : ?>
                                                                <span style="margin-top:20px;display:inline-block;" class="col-sm-12">
                                                                    <b>Stok hampir habis!</b> Tersisa < 5
                                                                </span> 
                                                            <?php elseif ($data->product_quantity < 10) : ?> 
                                                                <span style="margin-top:20px;display:inline-block;" class="col-sm-12">
                                                                    <b>Stok hampir habis!</b> Tersisa < 10
                                                                </span> 
                                                            <?php endif; ?> 
                                                        <?php endif; ?> 
                                                    </div>
                                                    <div class="purchase-section multiple col-sm-12 purchase-section-desktop" style="display: inline-block;margin-top:<?= ($data->product_quantity < 10) ? '-20px;' : '0px;' ?>">

                                                        <?php if (($data->merchant_name && $data->product_quantity < 1) || ($data->min_buy > $data->product_quantity)) { ?>
                                                            <h3 style="color: #d9534f;font-size:20px;margin-top:30px;">Stok habis</h3>
                                                        <?php
                                                        } elseif ($data->preorder == 1 || $data->product_quantity >= 1 || ($data->min_buy <= $data->product_quantity)) {
                                                        ?>
                                                            <div class="quantity-wrapper" style="margin-top: 12px;margin-bottom: 25px;padding: unset;float: left;">
                                                                <div class="quantity-field" style="margin-left: 0px !important;margin-top: 10px;border-radius: 5px;">
                                                                    <button onclick="down_qty(<?= $data->product_quantity ?>, <?php echo ($data->price_grosir || $data->price_reseller) ? 'true' : 'false';  ?>, <?php echo ($customer) ? ($customer->type == 'm' ? 'true' : 'false' ) : 'false'  ?>)" disabled id="btn_down_qty">
                                                                        <i class="fa fa-minus"></i>
                                                                    </button>
                                                                    <input type="text" id="qty" class="number_qty" name="qty" value="<?= $data->min_buy ?>" min="1" maxlength="3" class="item-quantity" style="-webkit-appearance: textfield;" data-qty="<?= $data->product_quantity ?>" data-type="<?php echo ($data->price_grosir || $data->price_reseller) ? 'true' : 'false'  ?>"data-merchant="<?php echo ($customer) ? ($customer->type == 'm' ? 'true' : 'false' ) : 'false'  ?>">
                                                                    <button onclick="up_qty(<?= $data->product_quantity ?>, <?php echo ($data->price_grosir || $data->price_reseller) ? 'true' : 'false';  ?>, <?php echo ($customer) ? ($customer->type == 'm' ? 'true' : 'false' ) : 'false'  ?>)" class="plus" id="btn_up_qty" <?= ($data->product_quantity == 1) ? 'disabled' : '' ?>>
                                                                        <i class="fa fa-plus"></i>
                                                                    </button>
                                                                </div>
                                                            </div>

                                                            <?php
                                                            if ($data->preorder == 1) {
                                                                $button_buy = 'Pre Order';
                                                            } else if($data->preorder == 2) {
                                                                $button_buy = 'Coming Soon';
                                                            } else {
                                                                $button_buy = 'Beli Sekarang';
                                                            }
                                                            ?>

                                                            <div class="purchase" style="margin: 0 0 2rem 0;padding: 0;display: inline">
                                                                <div id="err_qty" style="display:none;color:#d9534f;margin-top: 10px;margin-bottom: -10px !important;margin-top:-22px;margin-bottom:30px;"></div>
                                                                <?php if($data->min_buy > 1) : ?>
                                                                <div style="margin-top: -20px;margin-bottom: 10px;" id="min_buy_notification">
                                                                    <span style="font-weight: 550">Minimal pembelian <?= $data->min_buy ?> produk</span>
                                                                </div>
                                                                <?php endif; ?>
                                                                <?php if ($data->preorder == 1) : ?>
                                                                    <div>
                                                                        <span style="display: block;color: #d9534f;">Barang akan diproses selama <?= $data->preorder_time ?> hari</span>
                                                                    </div>
                                                                <?php elseif($data->preorder == 2) : ?>
                                                                    <?php 
                                                                    $coming_soon_time = strtotime($data->coming_soon_time);
                                                                    $now = time();
                                                                    ?>
                                                                    <?php if($now <= $coming_soon_time) : ?>
                                                                        <div>
                                                                            <span style="display: block;color: #d9534f;">
                                                                                Barang akan ada pada <b><?php echo tgl_indo(date('Y-m-d', strtotime($data->coming_soon_time))); ?></b>
                                                                            </span>
                                                                        </div>
                                                                    <?php endif; ?>
                                                                <?php endif; ?>
                                                                <!-- <input type="hidden" id="product" name="product" value="<?php //echo encode($data->product_id); ?>">
                                                                <input type="hidden" id="store" name="store" value="<?php //echo encode($data->branch_id); ?>">
                                                                <input type="hidden" id="type_merchant" name="type_merchant" value=""> -->

                                                                <?php if ($this->ion_auth->logged_in()) : ?>
                                                                    <button class="btn btn-md add-to-cart" id="btn-cart" type="button" <?php echo ($data->preorder == 2) ? 'disabled' : '' ?>><?= $button_buy ?></button>

                                                                    <?php if($data->preorder != 2) : ?>
                                                                        <a href="javascript:void(0);" class="btn btn-default btn-md" onclick="addtoCarts()" style="text-transform: uppercase;">
                                                                            Tambah Ke Keranjang
                                                                        </a>
                                                                    <?php endif; ?>
                                                                <?php else : ?>
                                                                    <button class="btn btn-md add-to-cart btn-cart-not-login" id="btn-cart-not" type="button" data-toggle="modal" data-target="#modal-guest" data-back="<?= seo_url('catalog/products/view/' . $data->product_id . '/' . $data->branch_id) ?>"><?= $button_buy ?></button>

                                                                    <?php if($data->preorder != 2) : ?>
                                                                        <a href="javascript:void(0);" class="btn btn-default btn-md btn-cart-not-login" style="text-transform: uppercase;" data-toggle="modal" data-target="#modal-guest" data-back="<?= seo_url('catalog/products/view/' . $data->product_id . '/' . $data->branch_id) ?>">
                                                                            Tambah Ke Keranjang
                                                                        </a>
                                                                    <?php endif; ?>
                                                                <?php endif; ?>
                                                            </div>
                                                        <?php } ?>

                                                    </div>
                                                <?php endif; ?>
                                                <input type="hidden" id="product" name="product" value="<?php echo encode($data->product_id); ?>">
                                                <input type="hidden" id="store" name="store" value="<?php echo encode($data->branch_id); ?>">
                                                <input type="hidden" id="type_merchant" name="type_merchant" value="">

                                        </div>
                                    </div>
                                <?php endif; ?>
                                <!-- END OF SHIPPING -->
                            </div>
                            <!-- END OF PRODUCT INFORMATION -->

                            <?php
                            $delapan = "margin-top:-8px";
                            $delapan_ = "margin-top:-30px";
                            ?>

                            <!-- <div class="col-md-12 col-xs-12" style="<?//php if ($data->guarantee) {
                                                                        //echo $delapan;
                                                                    //} else {
                                                                        //echo $delapan_;
                                                                    //} ?>">
                            </div> -->
                            <div class="col-md-12 col-xs-12" style="margin-top: <?= ($data->guarantee) ? '-8px;' : '-30px;' ?>"></div>

                        </div>
                        <!-- end of info-detail-pro class -->

                    </div>
                    <!-- end of product ID -->

                    <?php if(!$this->agent->is_mobile()) : ?>
                        <div class="heading-information clearfix col-md-12">
                            <ul>
                                <?php if($data->description) : ?>
                                    <li><a href="#description_information" class="active">Deskripsi</a></li>
                                <?php endif; ?>
                                <?php if($spesifikasi) : ?>
                                    <li><a href="#spesification_information">Spesifikasi</a></li>
                                <?php endif; ?>
                                <li><a href="#review_information">Ulasan</a></li>
                                <?php if($spesifikasi && $category->num_rows() > 1) : ?>
                                    <li><a href="#compare_information">Bandingkan Produk Lain</a></li>
                                <?php endif; ?>
                                <?php if(isset($products_merchant) || $product_relations) : ?>
                                    <?php if($products_merchant->num_rows() > 0 || $product_relations) : ?>
                                        <li><a href="#other_products">Produk Lainnya</a></li>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </ul>
                        </div>
                    <?php endif; ?>

                    <?php if ($this->agent->is_mobile()) : ?>
                        <div id="tabs-information" class="col-md-12" style="padding: 0px 15px 25px !important">
                            <div class="content-information">
                                <hr>
                                <?php if($data->description) : ?>
                                    <div class="information_content_mobile">
                                        <div class="information_content_heading">
                                            Deskripsi Produk
                                        </div>
                                        <div class="information_content_body">
                                            <?php echo $data->description ?>
                                        </div>
                                    </div>
                                    <hr>
                                <?php endif; ?>
                                <?php if($spesifikasi) : ?>
                                    <div class="information_content_mobile">
                                        <div class="information_content_heading">
                                            Spesifikasi
                                        </div>
                                        <div class="information_content_body">
                                            <table class="table table-specification">
                                                <tbody>
                                                    <?php foreach($spesifikasi as $feature) : ?>
                                                        <tr>
                                                            <td style="font-weight: bold;"><b><?php echo ucwords($feature->name); ?></b></td>
                                                            <td><?php echo $feature->value; ?></td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <hr>
                                <?php endif; ?>
                                <div class="information_content_mobile">
                                    <div class="information_content_heading">
                                        <?php if($rating->rating != 0 && $rating->review != 0) : ?>
                                            Ulasan (<?php echo number($rating->review) ?>)
                                        <?php else : ?>
                                            Tidak Ada Ulasan
                                        <?php endif; ?>
                                    </div>
                                    <div class="information_content_body">
                                        <?php if($rating->review) : ?>
                                            <div class="rating_review">
                                                <div class="star_rating_review">
                                                    <?php for ($i = 1; $i <= $rating->rating; $i++) : ?>
                                                        <i class="spr-icon spr-icon-star"></i>
                                                    <?php endfor; ?>

                                                    <?php for ($i = $rating->rating + 1; $i <= 5; $i++) : ?>
                                                        <i class="spr-icon spr-icon-star-empty"></i>
                                                    <?php endfor; ?>
                                                </div>
                                                <span>(<?php echo number($rating->review) ?>) Ulasan</span>
                                            </div>
                                        <?php endif; ?>
                                        <?php if($reviews) : ?>
                                            <div class="all_reviews">
                                                <div class="all_reviews_title">
                                                    <span>Semua Ulasan (<?php echo number($rating->review) ?>)</span>
                                                </div>
                                                <div class="all_reviews_content">
                                                    <?php foreach($reviews->result() as $review) : ?>
                                                        <div class="all_reviews_content_container">
                                                            <div class="reviews_star_profile">
                                                                <div class="reviews_star_left">
                                                                    
                                                                </div>
                                                                <div class="reviews_star_right">
                                                                    <span class="reviews_name_profile">
                                                                        <?php echo $review->name ?>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="reviews_star_content">
                                                                <div class="reviews_star">
                                                                    <?php for ($i = 1; $i <= $review->rating; $i++) : ?>
                                                                        <i class="spr-icon spr-icon-star"></i>
                                                                    <?php endfor; ?>

                                                                    <?php for ($i = $review->rating + 1; $i <= 5; $i++) : ?>
                                                                        <i class="spr-icon spr-icon-star-empty"></i>
                                                                    <?php endfor; ?>
                                                                </div>
                                                                <div class="reviews_description">
                                                                    <?php echo $review->description; ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                    <?php if($rating->rating != 0 && $rating->review != 0) : ?>
                                        
                                    <?php else : ?>
                                        <hr>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php else : ?>
                        <div id="tabs-information" class="col-md-12" <?= ($this->agent->is_mobile()) ? 'style="margin-bottom:45px;"' : ''; ?> style="padding: 0px 0px 25px !important;">
                            
                            <div class="content-information">
                                <?php
                                if ($data->description) :
                                ?>
                                    <div class="information_content" id="description_information">
                                        <div class="information_body">
                                            <?php echo $data->description ?>
                                        </div>
                                    </div>
                                <?php
                                endif;

                                if ($spesifikasi) {
                                ?>
                                <hr>
                                    <!-- <div class="information_content panel panel-default">
                                        <div class="panel-heading" role="tab" id="heading_spec">
                                            <h4 class="panel-title" data-toggle="collapse" href="#collapse_spec" aria-expanded="true" aria-controls="collapse_des">
                                                Spesifikasi
                                                <i class="fa-icon fa fa-angle-up"></i>
                                            </h4>
                                        </div>
                                        <div id="collapse_spec" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="collapse_spec">
                                            <div class="panel-body">
                                                <table class="table table-specification">
                                                    <tbody>
                                                        <?php
                                                        foreach ($spesifikasi as $feature) {
                                                        ?>
                                                            <tr>
                                                                <td style="color:#999"><b><?php echo ucwords($feature->name); ?></b></td>
                                                                <td><b><?php echo $feature->value; ?></b></td>
                                                            </tr>
                                                        <?php
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="information_content" id="spesification_information">
                                        <div class="information_body">
                                            <table class="table table-specification">
                                                <tbody>
                                                    <?php foreach($spesifikasi as $feature) : ?>
                                                        <tr>
                                                            <td style="font-weight: bold;"><b><?php echo ucwords($feature->name); ?></b></td>
                                                            <td><?php echo $feature->value; ?></td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                <?php
                                }
                                ?>
                                <hr>
                                <!-- <div class="information_content panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading_review">
                                        <h4 class="panel-title" data-toggle="collapse" href="#collapse_review" aria-expanded="true" aria-controls="collapse_review">Review<i class="fa-icon fa fa-angle-up"></i></h4>
                                    </div>
                                    <div id="collapse_review" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading_review">
                                        <div class="panel-body">
                                            <div id="customer_review">
                                                <div class="preview_content">
                                                    <div id="shopify-product-reviews">
                                                        <div class="spr-container">
                                                            <div class="spr-header">
                                                                <div class="spr-summary">
                                                                    <?php
                                                                    if ($rating->review) {
                                                                    ?>
                                                                        <span class="spr-starrating spr-summary-starrating">
                                                                            <?php
                                                                            for ($i = 1; $i <= $rating->rating; $i++) {
                                                                            ?>
                                                                                <i class="spr-icon spr-icon-star"></i>
                                                                            <?php
                                                                            }

                                                                            for ($i = $rating->rating + 1; $i <= 5; $i++) {
                                                                            ?>
                                                                                <i class="spr-icon spr-icon-star-empty"></i>
                                                                            <?php
                                                                            }
                                                                            ?>
                                                                        </span>

                                                                        <span class="spr-summary-caption">
                                                                            <span class="spr-summary-actions-togglereviews">Berdasarkan <?php echo number($rating->review); ?> ulasan</span>
                                                                        </span>
                                                                    <?php
                                                                    } else {
                                                                    ?>
                                                                        <span class="spr-summary-caption">
                                                                            <span class="spr-summary-actions-togglereviews">Tidak ada ulasan</span>
                                                                        </span>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div class="spr-content">
                                                                <?php
                                                                if ($reviews) {
                                                                ?>
                                                                    <div class="spr-reviews">
                                                                        <?php
                                                                        foreach ($reviews->result() as $review) {
                                                                        ?>
                                                                            <div class="spr-review">
                                                                                <div class="spr-review-header">
                                                                                    <span class="spr-starratings spr-review-header-starratings">
                                                                                        <?php
                                                                                        for ($i = 1; $i <= $review->rating; $i++) {
                                                                                        ?>
                                                                                            <i class="spr-icon spr-icon-star"></i>
                                                                                        <?php
                                                                                        }

                                                                                        for ($i = $review->rating + 1; $i <= 5; $i++) {
                                                                                        ?>
                                                                                            <i class="spr-icon spr-icon-star-empty"></i>
                                                                                        <?php
                                                                                        }
                                                                                        ?>
                                                                                    </span>

                                                                                    <span class="spr-review-header-byline">
                                                                                        <strong><?php echo $review->name; ?></strong> on <strong><?php echo get_date($review->date_added); ?></strong>
                                                                                    </span>
                                                                                </div>

                                                                                <div class="spr-review-content">
                                                                                    <p class="spr-review-content-body"><?php echo $review->description; ?></p>
                                                                                </div>
                                                                            </div>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                <?php
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="information_content" id="review_information">
                                    <div class="information_heading">
                                        <?php if($rating->rating != 0 && $rating->review != 0) : ?>
                                            <h4>Ulasan (<?php echo number($rating->review) ?>)</h4>
                                        <?php else : ?>
                                            <h4>Tidak Ada Ulasan</h4>
                                        <?php endif; ?>
                                    </div>
                                    <div class="information_body">
                                        <?php if($rating->review) : ?>
                                            <div class="rating_review">
                                                <div class="star_rating_review">
                                                    <?php for ($i = 1; $i <= $rating->rating; $i++) : ?>
                                                        <i class="spr-icon spr-icon-star"></i>
                                                    <?php endfor; ?>

                                                    <?php for ($i = $rating->rating + 1; $i <= 5; $i++) : ?>
                                                        <i class="spr-icon spr-icon-star-empty"></i>
                                                    <?php endfor; ?>
                                                </div>
                                                <span>(<?php echo number($rating->review) ?>) Ulasan</span>
                                            </div>
                                        <?php endif; ?>
                                        <?php if($reviews) : ?>
                                            <div class="all_reviews">
                                                <div class="all_reviews_title">
                                                    <span>Semua Ulasan (<?php echo number($rating->review) ?>)</span>
                                                </div>
                                                <div class="all_reviews_content">
                                                    <?php foreach($reviews->result() as $review) : ?>
                                                        <div class="all_reviews_content_container">
                                                            <div class="reviews_star_profile">
                                                                <div class="reviews_star_left">
                                                                    
                                                                </div>
                                                                <div class="reviews_star_right">
                                                                    <span class="reviews_name_profile">
                                                                        <?php echo $review->name ?>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="reviews_star_content">
                                                                <div class="reviews_star">
                                                                    <?php for ($i = 1; $i <= $review->rating; $i++) : ?>
                                                                        <i class="spr-icon spr-icon-star"></i>
                                                                    <?php endfor; ?>

                                                                    <?php for ($i = $review->rating + 1; $i <= 5; $i++) : ?>
                                                                        <i class="spr-icon spr-icon-star-empty"></i>
                                                                    <?php endfor; ?>
                                                                </div>
                                                                <div class="reviews_description">
                                                                    <?php echo $review->description; ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <!-- COMPARE PRODUCT -->
                    <?php if ($spesifikasi && $category->num_rows() > 1) : ?>
                        <?php if(!$this->agent->is_mobile()) : ?>
                            <div class="shopify-section index-section index-section-proban">
                                <div>
                                    <section class="home_proban_layout">
                                        <div class="home_proban_wrapper">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="home_proban_inner home-content-inner" id="compare_information">
                                                        <div class="home-content-header page-title">
                                                            <div class="home-content-title group_title">
                                                                <h4>Bandingkan Produk Lain</h4>
                                                            </div>
                                                        </div>
                                                        <div class="home_proban_content" id="compare-product">
                                                            <div class="col-md-3">
                                                                <table class="table" style="max-width: 103%; width: 110%;">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="height: 194px;"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 55px;">
                                                                                <b>Nama Produk</b>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 37px;"><b>Ulasan</b></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>Harga</b></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>Dijual Oleh</b></td>
                                                                        </tr>
                                                                        <?php foreach ($spesifikasi as $compare) : ?>
                                                                            <tr>
                                                                                <td><b><?= $compare->name ?></b></td>
                                                                            </tr>
                                                                        <?php endforeach; ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <table class="table" style="max-width: 103%; width: 110%;">
                                                                    <tbody class="hover-compare">
                                                                        <tr>
                                                                            <td class="text-center" style="height: 180px;">
                                                                                <img src="<?php echo get_image($data->image) ?>" alt="<?= $data->name ?>" style="width: 177px; height: 177px;">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 55px;overflow: auto;">
                                                                                <span style="color: #a7c22a;font-weight: bold"><?= $data->name ?></span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <?php
                                                                                $rating = $this->product->get_rating($data->product_id, $data->store_id);

                                                                                ?>
                                                                                <?php if (round($rating->rating) > 0) : ?>
                                                                                    <?php for ($i = 1; $i <= round($rating->rating); $i++) : ?>
                                                                                        <i class="spr-icon spr-icon-star"></i>
                                                                                    <?php endfor; ?>
                                                                                    <?php for ($n = (($rating->rating) + 1); $n <= 5; $n++) : ?>
                                                                                        <i class="spr-icon spr-icon-star-empty"></i>
                                                                                    <?php endfor; ?>
                                                                                <?php else : ?>
                                                                                    <?php for ($n = (($rating->rating) + 1); $n <= 5; $n++) : ?>
                                                                                        <i class="spr-icon spr-icon-star-empty"></i>
                                                                                    <?php endfor; ?>
                                                                                <?php endif; ?>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <?php if($data->variation) : ?>
                                                                                    <?php $data_variation = $this->main->get('product_option', ['product' => $data->product_id, 'default' => 1]) ?>
                                                                                    <?php if($data->price_grosir) : ?>
                                                                                        <span style="color: #a7c22a;"><?= rupiah($data->price) ?></span>
                                                                                    <?php else : ?>
                                                                                        <?php if($data_variation) : ?>
                                                                                            <span style="color: #a7c22a;"><?= rupiah($data_variation->price) ?></span>
                                                                                        <?php else : ?>
                                                                                            <span style="color: #a7c22a;"><?= rupiah($data->price) ?></span>
                                                                                        <?php endif; ?>
                                                                                    <?php endif; ?>
                                                                                <?php else : ?>
                                                                                    <span style="color: #a7c22a;"><?= rupiah($data->price) ?></span>
                                                                                <?php endif; ?>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <span style="color: #a7c22a;"><?= $data->merchant_name ?></span>
                                                                            </td>
                                                                        </tr>
                                                                        <?php foreach (json_decode($data->table_description) as $compp) : ?>
                                                                            <tr>
                                                                                <td><span style="color: #a7c22a;"><?= $compp->value ?></span></td>
                                                                            </tr>
                                                                        <?php endforeach; ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <?php foreach ($category->result() as $product) : ?>
                                                                <?php if ($product->table_description) : ?>
                                                                    <div class="col-md-3">
                                                                        <table class="table" style="max-width: 103%; width: 110%;">
                                                                            <tbody class="hover-compare">
                                                                                <tr>
                                                                                    <td class="text-center" style="height: 180px;">
                                                                                        <?php if ($product->product_id == $data->product_id && $product->store_id == $data->branch_id) : ?>
                                                                                            <img src="<?php echo get_image($product->image) ?>" alt="<?= $product->name ?>" style="width: 177px; height: 177px;">
                                                                                        <?php else : ?>
                                                                                            <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>">
                                                                                                <img src="<?php echo get_image($product->image) ?>" alt="<?= $product->name ?>" style="width: 177px; height: 177px;">
                                                                                            </a>
                                                                                        <?php endif; ?>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="height: 55px;overflow: auto;">
                                                                                        <?php if ($product->product_id == $data->product_id && $product->store_id == $data->branch_id) : ?>
                                                                                            <span style="color: #a7c22a;font-weight: bold"><?= $product->name ?></span>
                                                                                        <?php else : ?>
                                                                                            <a class="title-compare" href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>">
                                                                                                <?php echo $product->name; ?>
                                                                                            </a>
                                                                                        <?php endif; ?>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <?php
                                                                                        $rating = $this->product->get_rating($product->product_id, $product->store_id);

                                                                                        ?>
                                                                                        <?php if (round($rating->rating) > 0) : ?>
                                                                                            <?php for ($i = 1; $i <= round($rating->rating); $i++) : ?>
                                                                                                <i class="spr-icon spr-icon-star"></i>
                                                                                            <?php endfor; ?>
                                                                                            <?php for ($n = (($rating->rating) + 1); $n <= 5; $n++) : ?>
                                                                                                <i class="spr-icon spr-icon-star-empty"></i>
                                                                                            <?php endfor; ?>
                                                                                        <?php else : ?>
                                                                                            <?php for ($n = (($rating->rating) + 1); $n <= 5; $n++) : ?>
                                                                                                <i class="spr-icon spr-icon-star-empty"></i>
                                                                                            <?php endfor; ?>
                                                                                        <?php endif; ?>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <?= rupiah($product->price) ?>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <?= $product->merchant_name ?>
                                                                                    </td>
                                                                                </tr>
                                                                                <?php foreach (json_decode($product->table_description) as $compp) : ?>
                                                                                    <tr>
                                                                                        <td><?= $compp->value ?></td>
                                                                                    </tr>
                                                                                <?php endforeach; ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>

                    <div id="other_products" class="clearfix col-md-12"></div>

                    <?php if ($product_relations) { ?>
                        <?php if ($this->agent->is_mobile()) : ?>
                            <div class="shopify-section index-section index-section-proban">
                                <div>
                                    <section class="home_proban_layout">
                                        <div class="home_proban_wrapper">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="home_proban_inner home-content-inner">
                                                        <div class="home-content-header page-title" style="margin-bottom: 5px;">
                                                            <div class="home-content-title group_title" style="padding-left:7px; padding-right:7px;">
                                                                <h4>Produk Terkait</h4>
                                                            </div>
                                                        </div>

                                                        <div class="home_proban_content" id="promo-product">
                                                            <?php
                                                            foreach ($product_relations->result() as $key => $product) {
                                                                $sale_on = FALSE;

                                                                if ($this->ion_auth->logged_in()) {
                                                                    $id_customer = $this->data['user']->id;
                                                                    $get_wishlist = $this->main->get('wishlist', ['product' => $product->id, 'branch_id' => $product->store_id, 'customer' => $id_customer]);
                                                                    if ($get_wishlist) {
                                                                        $product_wishlist = true;
                                                                    } else {
                                                                        $product_wishlist = false;
                                                                    }
                                                                } else {
                                                                    $product_wishlist = false;
                                                                }

                                                                if($product->variation) {
                                                                    $product_variation = $this->main->get('product_option', ['product' => $product->id, 'default' => 1]);
                                                                    if($product->price_grosir) {
                                                                        if($product_variation) {
                                                                            $product->price = $product->price;
                                                                            $product->quantity = $product_variation->quantity;
                                                                        } else {
                                                                            $product->price = $product->price;
                                                                            $product->quantity = $product->quantity;
                                                                        }
                                                                    } else {
                                                                        if($product_variation) {
                                                                            $product->price = $product_variation->price;
                                                                            $product->quantity = $product_variation->quantity;
                                                                        } else {
                                                                            $product->price = $product->price;
                                                                            $product->quantity = $product->quantity;
                                                                        }
                                                                    }
                                                                }

                                                                if ($product->promo == 1) {
                                                                    if ($product->merchant == 0) {
                                                                        $merchant = $this->session->userdata('list_merchant')[0];
                                                                        $price_group = $this->main->get('product_price', array('product' => $product->id, 'merchant_group' => $merchant['group']));

                                                                        if ($price_group) {
                                                                            $product->price = $price_group->price;
                                                                        }
                                                                    }

                                                                    $promo_data = json_decode($product->promo_data, true);

                                                                    if ($promo_data['type'] == 'P') {
                                                                        $disc_price = round(($product->price * $promo_data['discount']) / 100);
                                                                        $label_disc = $promo_data['discount'] . '% off';
                                                                    } else {
                                                                        $disc_price = $promo_data['discount'];
                                                                        $label_disc = 'SALE';
                                                                    }

                                                                    $disc_price = intval($disc_price);
                                                                    $product->price = intval($product->price);

                                                                    $end_price = $product->price - $disc_price;
                                                                    $today = date('Y-m-d');
                                                                    $today = date('Y-m-d', strtotime($today));
                                                                    $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                                                    $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

                                                                    if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                                                        $sale_on = TRUE;
                                                                    }
                                                                }
                                                            ?>
                                                                <div class="col-sm-2 proban_product">
                                                                    <div class="row-container product list-unstyled clearfix">

                                                                        <?php if ($sale_on) { ?>
                                                                            <span class="product-discount-label" style="position:absolute;top:5px;right:10px;z-index:1;background-color:#d9534f;color:white;padding:5px 10px;border-radius:3px;"> <?php echo $label_disc; ?> </span>
                                                                        <?php
                                                                        }
                                                                        ?>

                                                                        <?php if ($this->ion_auth->logged_in()) : ?>
                                                                            <a href="#" class="btn-add-to-wishlist" style="color:<?= ($product_wishlist) ? '#d9534f;' : '#BBB'; ?>" onclick="add_wishlist(this)" data-product="<?= encode($product->id) ?>" data-store="<?= encode($product->store_id) ?>" data-customer="<?= encode($id_customer) ?>">
                                                                                <i class="fa fa-heart" aria-hidden="true"></i>
                                                                            </a>
                                                                        <?php else : ?>
                                                                            <a href="#" class="btn-add-to-wishlist" style="color:#BBB" data-toggle="modal" data-target="#modal-guest" data-back="<?= site_url() ?>">
                                                                                <i class="fa fa-heart" aria-hidden="true"></i>
                                                                            </a>
                                                                        <?php endif; ?>

                                                                        <div class="row-left">
                                                                            <a href="<?php echo seo_url('catalog/products/view/' . $product->id . '/' . $product->store_id); ?>" class="hoverBorder container_item">
                                                                                <div class="hoverBorderWrapper">
                                                                                    <img src="<?php echo ($product->image) ? site_url('files/images/' . $product->image) : site_url('assets/frontend/images/noimage.jpg'); ?>" class="img-responsive front" alt="<?php echo $product->name; ?>">
                                                                                </div>
                                                                            </a>
                                                                        </div>

                                                                        <div class="garis-tengah"></div>
                                                                        <div class="tombol-cart">
                                                                            <?php if ($this->ion_auth->logged_in()) : ?>
                                                                                <?php if ($product->variation || $product->min_buy > 1) : ?>
                                                                                    <a href="<?php echo seo_url('catalog/products/view/' . $product->id . '/' . $product->store_id); ?>" class="icon-cart">
                                                                                        <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                                                                    </a>
                                                                                <?php else : ?>
                                                                                    <?php //if ($product->merchant != 0 && $product->quantity == 0) : 
                                                                                    if ($product->merchant != 0) : ?>
                                                                                        <a href="<?php echo seo_url('catalog/products/view/' . $product->id . '/' . $product->store_id); ?>" class="icon-cart">
                                                                                            <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                                                                        </a>
                                                                                    <?php else : ?>
                                                                                        <button data-id="<?php echo encode($product->id); ?>" data-storeid="<?= encode($product->store_id) ?>" onclick="addtoCart($(this).data('id'), $(this).data('storeid'))" class="icon-cart">
                                                                                            <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                                                                        </button>
                                                                                    <?php endif; ?>
                                                                                <?php endif; ?>
                                                                            <?php else : ?>
                                                                                <button type="button" class="icon-cart" data-toggle="modal" data-target="#modal-guest">
                                                                                    <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                                                                </button>
                                                                            <?php endif; ?>
                                                                        </div>

                                                                        <div class="row-right animMix" style="text-align: left;">
                                                                            <div class="product-title">
                                                                                <a class="title-5" style="font-weight: bold; width: 75%;" href="<?php echo seo_url('catalog/products/view/' . $product->id . '/' . $product->store_id); ?>"><?php echo $product->name; ?></a>
                                                                            </div>

                                                                            <div class="product-price">
                                                                                <span class="price_sale">

                                                                                    <?php if ($sale_on) { ?>
                                                                                        <span class="money" style="text-decoration: line-through;font-size:11px;color:#8b8f8b"><?php echo rupiah($product->price); ?></span>
                                                                                        <span class="money"><?php echo rupiah($end_price); ?></span>
                                                                                    <?php
                                                                                    } else {
                                                                                    ?>
                                                                                        <span class="price_sale"><span class="money"><?php echo rupiah($product->price); ?></span></span>
                                                                                    <?php } ?>

                                                                                </span>
                                                                            </div>

                                                                            <div>
                                                                                <?php
                                                                                $merchant = $this->main->get('merchants', ['id' => $product->store_id]);
                                                                                $city = $this->main->get('cities', ['id' => $merchant->city]);
                                                                                ?>
                                                                                <div style="height:15px;overflow:hidden;">
                                                                                    <span id="text-ellipsis-city-last_seen-<?= $key ?>" style="display:block;"><?= ($city->type == 'Kabupaten') ? 'Kab. ' . $city->name : '' . $city->name ?></span>
                                                                                    <span id="text-ellipsis-store-last_seen-<?= $key ?>" style="display: block;"><?= $product->store_name ?></span>
                                                                                </div>
                                                                            </div>

                                                                            <div class="rating-star">
                                                                                <span class="spr-badge" data-rating="0.0">
                                                                                    <span class="spr-starrating spr-badge-starrating">
                                                                                        <?php
                                                                                        $rating = $this->product->get_rating($product->id, $product->store_id);

                                                                                        if (round($rating->rating) > 0) {

                                                                                            for ($i = 1; $i <= round($rating->rating); $i++) {
                                                                                        ?>
                                                                                                <i class="spr-icon spr-icon-star"></i>
                                                                                            <?php
                                                                                            }
                                                                                            for ($n = (($rating->rating) + 1); $n <= 5; $n++) {
                                                                                            ?>
                                                                                                <i class="spr-icon spr-icon-star-empty"></i>
                                                                                        <?php
                                                                                            }
                                                                                        }
                                                                                        ?>
                                                                                    </span>
                                                                                </span>
                                                                            </div>
                                                                            <div class="product-footer">
                                                                                <i class="fa fa-eye" aria-hidden="true"></i> <?= $product->viewed ?>
                                                                            </div>
                                                                            <?php
                                                                            $preorder = $product->preorder;
                                                                            $free_ongkir = $product->free_ongkir;

                                                                            if ($preorder == 1) {
                                                                                echo '<div class="pre-order-label">Preorder</div>';
                                                                            }
                                                                            if ($free_ongkir == 1) {
                                                                                echo '<div class="free-ongkir-label">Free Ongkir</div>';
                                                                            } ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php
                                                            }
                                                            ?>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        <?php else : ?>
                            <div class="shopify-section index-section index-section-proban">
                                <div>
                                    <section class="home_proban_layout">
                                        <div class="home_proban_wrapper">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="home_proban_inner home-content-inner">
                                                        <div class="home-content-header page-title">
                                                            <div class="home-content-title group_title" style="padding-left:7px; padding-right:7px;">
                                                                <h4>Produk Terkait</h4>
                                                            </div>
                                                        </div>

                                                        <div class="home_proban_content" id="promo-product">
                                                            <?php
                                                            foreach ($product_relations->result() as $key => $product) {
                                                                $sale_on = FALSE;

                                                                if ($this->ion_auth->logged_in()) {
                                                                    $id_customer = $this->data['user']->id;
                                                                    $get_wishlist = $this->main->get('wishlist', ['product' => $product->id, 'branch_id' => $product->store_id, 'customer' => $id_customer]);
                                                                    if ($get_wishlist) {
                                                                        $product_wishlist = true;
                                                                    } else {
                                                                        $product_wishlist = false;
                                                                    }
                                                                } else {
                                                                    $product_wishlist = false;
                                                                }

                                                                if($product->variation) {
                                                                    $product_variation = $this->main->get('product_option', ['product' => $product->id, 'default' => 1]);
                                                                    if($product->price_grosir) {
                                                                        if($product_variation) {
                                                                            $product->price = $product->price;
                                                                            $product->quantity = $product_variation->quantity;
                                                                        } else {
                                                                            $product->price = $product->price;
                                                                            $product->quantity = $product->quantity;
                                                                        }
                                                                    } else {
                                                                        if($product_variation) {
                                                                            $product->price = $product_variation->price;
                                                                            $product->quantity = $product_variation->quantity;
                                                                        } else {
                                                                            $product->price = $product->price;
                                                                            $product->quantity = $product->quantity;
                                                                        }
                                                                    }
                                                                }

                                                                if ($product->promo == 1) {
                                                                    if ($product->merchant == 0) {
                                                                        $merchant = $this->session->userdata('list_merchant')[0];
                                                                        $price_group = $this->main->get('product_price', array('product' => $product->id, 'merchant_group' => $merchant['group']));

                                                                        if ($price_group) {
                                                                            $product->price = $price_group->price;
                                                                        }
                                                                    }

                                                                    $promo_data = json_decode($product->promo_data, true);

                                                                    if ($promo_data['type'] == 'P') {
                                                                        $disc_price = round(($product->price * $promo_data['discount']) / 100);
                                                                        $label_disc = $promo_data['discount'] . '% off';
                                                                    } else {
                                                                        $disc_price = $promo_data['discount'];
                                                                        $label_disc = 'SALE';
                                                                    }

                                                                    $disc_price = intval($disc_price);
                                                                    $product->price = intval($product->price);

                                                                    $end_price = $product->price - $disc_price;
                                                                    $today = date('Y-m-d');
                                                                    $today = date('Y-m-d', strtotime($today));
                                                                    $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                                                    $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

                                                                    if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                                                        $sale_on = TRUE;
                                                                    }
                                                                }
                                                            ?>
                                                                <div class="col-sm-2 proban_product">
                                                                    <div class="row-container product list-unstyled clearfix" onmouseover="change_store(<?= $key ?>, 'over', 'related')" onmouseout="change_store(<?= $key ?>, 'out', 'related')">

                                                                        <?php if ($sale_on) { ?>
                                                                            <!-- <span class="product-discount-label" style="position:absolute;top:5px;right:10px;z-index:1;background-color:#d9534f;color:white;padding:5px 10px;border-radius:3px;"> <?php echo $label_disc; ?> </span> -->
                                                                        <?php
                                                                        }
                                                                        ?>

                                                                        <!-- <?php if ($this->ion_auth->logged_in()) : ?>
                                                                            <a href="#" class="btn-add-to-wishlist" style="color:<?= ($product_wishlist) ? '#d9534f;' : '#BBB'; ?>" onclick="add_wishlist(this)" data-product="<?= encode($product->id) ?>" data-store="<?= encode($product->store_id) ?>" data-customer="<?= encode($id_customer) ?>">
                                                                                <i class="fa fa-heart" aria-hidden="true"></i>
                                                                            </a>
                                                                        <?php else : ?>
                                                                            <a href="#" class="btn-add-to-wishlist" style="color:#BBB" data-toggle="modal" data-target="#modal-guest" data-back="<?= site_url() ?>">
                                                                                <i class="fa fa-heart" aria-hidden="true"></i>
                                                                            </a>
                                                                        <?php endif; ?> -->

                                                                        <div class="row-left">
                                                                            <a href="<?php echo seo_url('catalog/products/view/' . $product->id . '/' . $product->store_id); ?>" class="hoverBorder container_item">
                                                                                <div class="hoverBorderWrapper">
                                                                                    <img src="<?php echo base_url('assets/frontend/images/loading.gif') ?>" class="img-responsive lazyload" data-src="<?= ($product->image) ? site_url('files/images/' . $product->image) : site_url('assets/frontend/images/noimage.jpg'); ?>" alt="<?php echo $product->name; ?>">
                                                                                </div>
                                                                            </a>
                                                                        </div>

                                                                        <div class="garis-tengah"></div>
                                                                        <div class="tombol-cart">
                                                                            <?php if ($this->ion_auth->logged_in()) : ?>
                                                                                <?php if ($product->variation || $product->min_buy > 1) : ?>
                                                                                    <a href="<?php echo seo_url('catalog/products/view/' . $product->id . '/' . $product->store_id); ?>" class="icon-cart">
                                                                                        <img src="<?= site_url('assets/frontend/images/cart.png') ?>" style="width: 18px;margin-left:10px;line-height:38px">
                                                                                    </a>
                                                                                <?php else : ?>
                                                                                    <button data-id="<?php echo encode($product->id); ?>" data-storeid="<?= encode($product->store_id) ?>" onclick="addtoCart($(this).data('id'), $(this).data('storeid'))" class="icon-cart">
                                                                                        <img src="<?= site_url('assets/frontend/images/cart.png') ?>" style="width: 18px;">
                                                                                    </button>
                                                                                <?php endif; ?>
                                                                            <?php else : ?>
                                                                                <button type="button" class="icon-cart" data-toggle="modal" data-target="#modal-guest">
                                                                                    <img src="<?= site_url('assets/frontend/images/cart.png') ?>" style="width: 18px;">
                                                                                </button>
                                                                            <?php endif; ?>
                                                                        </div>

                                                                        <div class="row-right animMix">
                                                                            <div class="product-title">
                                                                                <a class="title-5" style="font-weight: bold; width: 75%;" href="<?php echo seo_url('catalog/products/view/' . $product->id . '/' . $product->store_id); ?>"><?php echo $product->name; ?></a>
                                                                            </div>

                                                                            <div class="viewed-div">
                                                                                <i class="fa fa-eye" aria-hidden="true"></i> <span><?= $product->viewed; ?></span>
                                                                            </div>

                                                                            <div class="product-price">
                                                                                <span class="price_sale">

                                                                                    <?php if($sale_on) : ?>
                                                                                        <div class="money price-end"><?= rupiah($end_price) ?></div>
                                                                                        <div class="price_sale">
                                                                                            <span class="money promo-price"><?= rupiah($product->price) ?></span>
                                                                                            <span class="promo-text"><?= $label_disc ?></span>
                                                                                        </div>
                                                                                    <?php else : ?>
                                                                                        <div class="money price-end"><?= rupiah($product->price) ?></div>
                                                                                    <?php endif; ?>

                                                                                </span>
                                                                            </div>

                                                                            <?php if($product->preorder) : ?>
                                                                                <div class="pre-order-label">Preorder</div>
                                                                            <?php endif; ?>
                                                                            <?php if($product->free_ongkir) : ?>
                                                                                <div class="free-ongkir-label">Free Ongkir</div>
                                                                            <?php endif; ?>

                                                                            <div class="merchant-div">
                                                                                <?php
                                                                                $merchant = $this->main->get('merchants', ['id' => $product->store_id]);
                                                                                $city = $this->main->get('cities', ['id' => $merchant->city]);
                                                                                ?>
                                                                                <div class="merchant-div-body">
                                                                                    <span id="text-ellipsis-city-<?= $key ?>" style="display:block;"><?= ($city->type == 'Kabupaten') ? 'Kab. ' . $city->name : '' . $city->name ?></span>
                                                                                    <span id="text-ellipsis-store-<?= $key ?>" style="display: block;"><?= $merchant->name ?></span>
                                                                                </div>
                                                                            </div>

                                                                            <!-- <div class="rating-star">
                                                                                <span class="spr-badge" data-rating="0.0">
                                                                                    <span class="spr-starrating spr-badge-starrating">
                                                                                        <?php
                                                                                        $rating = $this->product->get_rating($product->id, $product->store_id);

                                                                                        if (round($rating->rating) > 0) {

                                                                                            for ($i = 1; $i <= round($rating->rating); $i++) {
                                                                                        ?>
                                                                                                <i class="spr-icon spr-icon-star"></i>
                                                                                            <?php
                                                                                            }
                                                                                            for ($n = (($rating->rating) + 1); $n <= 5; $n++) {
                                                                                            ?>
                                                                                                <i class="spr-icon spr-icon-star-empty"></i>
                                                                                        <?php
                                                                                            }
                                                                                        }
                                                                                        ?>
                                                                                    </span>
                                                                                </span>
                                                                            </div>
                                                                            <div class="product-footer">
                                                                                <i class="fa fa-eye" aria-hidden="true"></i> <?= $product->viewed ?>
                                                                            </div> -->
                                                                            <?php
                                                                            // $preorder = $product->preorder;
                                                                            // $free_ongkir = $product->free_ongkir;

                                                                            // if ($preorder == 1) {
                                                                            //     echo '<div class="pre-order-label">Preorder</div>';
                                                                            // }
                                                                            // if ($free_ongkir == 1) {
                                                                            //     echo '<div class="free-ongkir-label">Free Ongkir</div>';
                                                                            // } ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php
                                                            }
                                                            ?>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div> 
                        <?php endif; ?>
                        <?php
                    }

                    if (isset($products_merchant)) {
                        if ($products_merchant->num_rows() > 0) {
                        ?>
                            <div class="shopify-section index-section index-section-proban">
                                <div>
                                    <section class="home_proban_layout">
                                        <div class="home_proban_wrapper">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="home_proban_inner home-content-inner">
                                                        <div class="home-content-header page-title" <?= ($this->agent->is_mobile()) ? 'style="margin-bottom:5px;margin-top:25px;"' : '' ?>>
                                                            <div class="home-content-title group_title" style="padding-left:7px; padding-right:7px;">
                                                                <h4>Lainnya di toko ini</h4>
                                                            </div>
                                                        </div>
                                                        <div class="home_proban_content" id="promo-product">
                                                            <?php
                                                            foreach ($products_merchant->result() as $key => $product) {
                                                                $sale_on = FALSE;

                                                                if ($this->ion_auth->logged_in()) {
                                                                    $id_customer = $this->data['user']->id;
                                                                    $get_wishlist = $this->main->get('wishlist', ['product' => $product->product_id, 'branch_id' => $product->store_id, 'customer' => $id_customer]);
                                                                    if ($get_wishlist) {
                                                                        $product_wishlist = true;
                                                                    } else {
                                                                        $product_wishlist = false;
                                                                    }
                                                                } else {
                                                                    $product_wishlist = false;
                                                                }

                                                                if($product->variation) {
                                                                    $product_variation = $this->main->get('product_option', ['product' => $product->product_id, 'default' => 1]);
                                                                    if($product->price_grosir) {
                                                                        if($product_variation) {
                                                                            $product->price = $product->price;
                                                                            $product->quantity = $product_variation->quantity;
                                                                        } else {
                                                                            $product->price = $product->price;
                                                                            $product->quantity = $product->quantity;
                                                                        }
                                                                    } else {
                                                                        if($product_variation) {
                                                                            $product->price = $product_variation->price;
                                                                            $product->quantity = $product_variation->quantity;
                                                                        } else {
                                                                            $product->price = $product->price;
                                                                            $product->quantity = $product->quantity;
                                                                        }
                                                                    }
                                                                }

                                                                if ($product->promo == 1) {

                                                                    if ($product->merchant == 0) {
                                                                        $merchant = $this->session->userdata('list_merchant')[0];
                                                                        $price_group = $this->main->get('product_price', array('product' => $product->product_id, 'merchant_group' => $merchant['group']));

                                                                        if ($price_group) {
                                                                            $product->price = $price_group->price;
                                                                        }
                                                                    }

                                                                    $promo_data = json_decode($product->promo_data, TRUE);

                                                                    if ($promo_data['type'] == 'P') {
                                                                        $disc_price = round(($product->price * $promo_data['discount']) / 100);
                                                                        $label_disc = $promo_data['discount'] . '% off';
                                                                    } else {
                                                                        $disc_price = $promo_data['discount'];
                                                                        $label_disc = 'SALE';
                                                                    }

                                                                    $disc_price = intval($disc_price);
                                                                    $product->price = intval($product->price);

                                                                    $end_price = $product->price - $disc_price;

                                                                    $today = date('Y-m-d');
                                                                    $today = date('Y-m-d', strtotime($today));
                                                                    $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                                                    $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

                                                                    if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                                                        $sale_on = TRUE;
                                                                    }
                                                                }
                                                            ?>
                                                                <div class="col-sm-2 proban_product">
                                                                    <div class="row-container product list-unstyled clearfix" onmouseover="change_store(<?= $key ?>, 'over', 'last_seen')" onmouseout="change_store(<?= $key ?>, 'out', 'last_seen')">

                                                                        <?php if ($sale_on) { ?>
                                                                            <!-- <span class="product-discount-label" style="position:absolute;top:5px;right:10px;z-index:1;background-color:#d9534f;color:white;padding:5px 10px;border-radius:3px;"><?php echo $label_disc; ?></span> -->
                                                                        <?php
                                                                        }
                                                                        ?>

                                                                        <!-- <?php if ($this->ion_auth->logged_in()) : ?>
                                                                            <a href="#" class="btn-add-to-wishlist" style="color:<?= ($product_wishlist) ? '#d9534f;' : '#BBB'; ?>" onclick="add_wishlist(this)" data-product="<?= encode($product->product_id) ?>" data-store="<?= encode($product->store_id) ?>" data-customer="<?= encode($id_customer) ?>">
                                                                                <i class="fa fa-heart" aria-hidden="true"></i>
                                                                            </a>
                                                                        <?php else : ?>
                                                                            <a href="#" class="btn-add-to-wishlist" style="color:#BBB" data-toggle="modal" data-target="#modal-guest" data-back="<?= site_url() ?>">
                                                                                <i class="fa fa-heart" aria-hidden="true"></i>
                                                                            </a>
                                                                        <?php endif; ?> -->

                                                                        <div class="row-left">
                                                                            <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="hoverBorder container_item">
                                                                                <div class="hoverBorderWrapper">
                                                                                    <img src="<?php echo base_url('assets/frontend/images/loading.gif') ?>" class="img-responsive lazyload" data-src="<?= ($product->image) ? site_url('files/images/' . $product->image) : site_url('assets/frontend/images/noimage.jpg'); ?>" alt="<?php echo $product->name; ?>">
                                                                                </div>
                                                                            </a>
                                                                        </div>

                                                                        <div class="garis-tengah"></div>
                                                                        <div class="tombol-cart">
                                                                            <?php if ($this->ion_auth->logged_in()) : ?>
                                                                                <?php if ($product->variation || $product->min_buy > 1) : ?>
                                                                                    <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="icon-cart">
                                                                                        <img src="<?= site_url('assets/frontend/images/cart.png') ?>" style="width: 18px;margin-left:10px;line-height:38px">
                                                                                    </a>
                                                                                <?php else : ?>
                                                                                    <button data-id="<?php echo encode($product->product_id); ?>" data-storeid="<?= encode($product->store_id) ?>" onclick="addtoCart($(this).data('id'), $(this).data('storeid'))" class="icon-cart">
                                                                                        <img src="<?= site_url('assets/frontend/images/cart.png') ?>" style="width: 18px;">
                                                                                    </button>
                                                                                <?php endif; ?>
                                                                            <?php else : ?>
                                                                                <button type="button" class="icon-cart" data-toggle="modal" data-target="#modal-guest">
                                                                                    <img src="<?= site_url('assets/frontend/images/cart.png') ?>" style="width: 18px;">
                                                                                </button>
                                                                            <?php endif; ?>
                                                                        </div>

                                                                        <div class="row-right animMix" style="text-align: left;">
                                                                            <div class="product-title">
                                                                                <a class="title-5" style="font-weight: bold; width: 75%;" href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>"><?php echo $product->name; ?></a>
                                                                            </div>

                                                                            <div class="viewed-div">
                                                                                <i class="fa fa-eye" aria-hidden="true"></i> <span><?= $product->viewed; ?></span>
                                                                            </div>

                                                                            <div class="product-price">
                                                                                <span class="price_sale">

                                                                                    <?php if($sale_on) : ?>
                                                                                        <div class="money price-end"><?= rupiah($end_price) ?></div>
                                                                                        <div class="price_sale">
                                                                                            <span class="money promo-price"><?= rupiah($product->price) ?></span>
                                                                                            <span class="promo-text"><?= $label_disc ?></span>
                                                                                        </div>
                                                                                    <?php else : ?>
                                                                                        <div class="money price-end"><?= rupiah($product->price) ?></div>
                                                                                    <?php endif; ?>

                                                                                </span>
                                                                            </div>

                                                                            <div class="merchant-div">
                                                                                <?php
                                                                                $merchant = $this->main->get('merchants', ['id' => $product->store_id]);
                                                                                $city = $this->main->get('cities', ['id' => $merchant->city]);
                                                                                ?>
                                                                                <div class="merchant-div-body">
                                                                                    <span id="text-ellipsis-city-<?= $key ?>" style="display:block;"><?= ($city->type == 'Kabupaten') ? 'Kab. ' . $city->name : '' . $city->name ?></span>
                                                                                    <span id="text-ellipsis-store-<?= $key ?>" style="display: block;"><?= $merchant->name ?></span>
                                                                                </div>
                                                                            </div>

                                                                            <!-- <div class="rating-star">
                                                                                <span class="spr-badge" data-rating="0.0">
                                                                                    <span class="spr-starrating spr-badge-starrating">
                                                                                        <?php
                                                                                        $rating = $this->product->get_rating($product->product_id, $product->store_id);

                                                                                        if (round($rating->rating) > 0) {

                                                                                            for ($i = 1; $i <= round($rating->rating); $i++) {
                                                                                        ?>
                                                                                                <i class="spr-icon spr-icon-star"></i>
                                                                                            <?php
                                                                                            }
                                                                                            for ($n = (($rating->rating) + 1); $n <= 5; $n++) {
                                                                                            ?>
                                                                                                <i class="spr-icon spr-icon-star-empty"></i>
                                                                                        <?php
                                                                                            }
                                                                                        }
                                                                                        ?>
                                                                                    </span>
                                                                                </span>
                                                                            </div>
                                                                            <div class="product-footer">
                                                                                &nbsp;
                                                                                <i class="fa fa-eye" aria-hidden="true"></i> <?= $product->viewed; ?>
                                                                            </div> -->
                                                                            <?php
                                                                            // $preorder = $product->preorder;
                                                                            // $free_ongkir = $product->free_ongkir;

                                                                            // if ($preorder == 1) {
                                                                            //     echo '<div class="pre-order-label">Preorder</div>';
                                                                            // }
                                                                            // if ($free_ongkir == 1) {
                                                                            //     echo '<div class="free-ongkir-label">Free Ongkir</div>';
                                                                            // } ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                    <?php
                        }
                    } ?>

                </div>
            </div>
            <!-- END OF detail-content-inner -->
        </div>
    </div>
    </div>
    </div>
</section>
<!-- End of product-detail-content SECTION -->

<script type="text/javascript">
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();

        // $('#jssocial').jsSocials ({
        //     shares: [
        //               'facebook',
        //               'twitter',
        //               'googleplus',
        //               {
        //                   share: 'email',
        //                   logo: 'fa fa-envelope',
        //                   shareIn: 'popup',
        //                   shareColor: '#A0A0A0'
        //               }
        //           ],
        //     showLabel: false,
        //     showCount: false,
        //     shareIn: 'popup'
        // });
    });
</script>