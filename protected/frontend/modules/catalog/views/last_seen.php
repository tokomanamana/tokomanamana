<section class="collection-heading heading-content">
    <div class="container">
        <div class="row">
            <div class="collection-wrapper">
                <h1 class="collection-title"></h1>
                <div class="breadcrumb-group">
                    <div class="breadcrumb clearfix">
                        <?php echo $breadcrumb; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="collection-content">
    <div class="collection-wrapper">
        <div class="container">
            <div class="row">
                <div id="shopify-section-collection-template" class="shopify-section">
                    <div class="collection-inner">
                        <div id="tags-load" style="display:none;"><i class="fa fa-spinner fa-pulse fa-2x"></i></div>
                        <div id="collection">
                            <div class="collection_inner">
                                <div class="collection-mainarea col-sm-12 clearfix">
                                    <?php if ($products->num_rows() > 0) { ?>
                                        <div class="collection_toolbar">
                                            <div class="toolbar_left">
                                                Item <?php echo number($start); ?> s/d <?php echo number($to); ?> dari <?php echo number($total_products); ?> Total Item
                                            </div>
                                        </div>
                                        <div class="collection-items clearfix">
                                            <div class="products">
                                                <?php
                                                    foreach ($products->result() as $product) {

                                                        if ($product->merchant == 0) {
                                                            $merchant = $this->session->userdata('list_merchant')[0];
                                                            $price_group = $this->main->get('product_price', array('product' => $product->product_id, 'merchant_group' => $merchant['group']));
                                                            if ($price_group) {
                                                                $product->price = $price_group->price;
                                                            }
                                                        }

                                                        $promo_data = json_decode($product->promo_data,true);
                                                        
                                                        if($promo_data['type'] == 'P') {
                                                            $disc_price = round(($product->price * $promo_data['discount']) / 100);
                                                            $label_disc = $promo_data['discount'].'% off';
                                                        } else {
                                                            $disc_price = $promo_data['discount'];
                                                            $label_disc = 'SALE';
                                                        }
                                                        
                                                        $end_price = $product->price - $disc_price;
                                                        $today = date('Y-m-d');
                                                        $today=date('Y-m-d', strtotime($today));
                                                        $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                                        $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                                                ?>
                                                        <div class="product-item col-sm-2">
                                                            <div class="product">
                                                                
                                                                <?php if (($today >= $DateBegin) && ($today <= $DateEnd)){ ?>
                                                                    <span class="product-discount-label" style="position:absolute;top:5px;right:10px;z-index:1;background-color:red;color:white;padding:5px 10px;"><?php echo $label_disc;?></span>
                                                                <?php
                                                                    } elseif ($product->preorder == 1) {
                                                                ?>
                                                                    <span class="product-discount-label" style="position:absolute;top:5px;right:10px;z-index:1;background-color:red;color:white;padding:5px 10px;">Pre Order</span>
                                                                <?php } ?>

                                                                <div class="row-left">
                                                                    <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="hoverBorder container_item">
                                                                        <img src="<?php echo ($product->image) ? site_url('files/images/' . $product->image) : site_url('assets/frontend/images/noimage.jpg'); ?>" class="img-responsive" alt="<?php echo $product->name; ?>">
                                                                    </a>

                                                                </div>
                                                                <div class="row-right animMix">
                                                                    <div class="grid-mode">
                                                                        <div class="product-title">
                                                                            <a class="title-5" href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>"><?php echo $product->name; ?></a>
                                                                        </div>

                                                                        <div class="product-price">
                                                                            <span class="price_sale">

                                                                                <?php if (($today >= $DateBegin) && ($today <= $DateEnd)) { ?>
                                                                                    <span class="money" style="text-decoration: line-through;font-size:9px;color:#8b8f8b"><?php echo rupiah($product->price); ?></span>
                                                                                    <span class="money"><?php echo rupiah($end_price); ?></span>
                                                                                <?php
                                                                                    }
                                                                                    else {
                                                                                ?>
                                                                                        <span class="money"><?php echo rupiah($product->price); ?></span>
                                                                                <?php } ?>

                                                                            </span>
                                                                        </div>

                                                                        <div>
                                                                            <span class="text-ellipsis"><?php echo $product->merchant_name; ?></span>
                                                                        </div>

                                                                        <div class="rating-star">
                                                                            <span class="spr-badge" data-rating="0.0">
                                                                                <span class="spr-starrating spr-badge-starrating">
                                                                                    <?php
                                                                                        $rating = $this->last_seen->get_rating ($product->product_id, $product->store_id);

                                                                                        if (round($rating->rating) > 0) {
                                                                                            for ($i = 1; $i <= round($rating->rating); $i++) {
                                                                                    ?>
                                                                                                <i class="spr-icon spr-icon-star"></i>
                                                                                    <?php
                                                                                            }
                                                                                            for ($i = round($rating->rating) + 1; $i <= 5; $i++) {
                                                                                    ?>
                                                                                                <i class="spr-icon spr-icon-star-empty"></i>
                                                                                    <?php
                                                                                            }
                                                                                        }
                                                                                    ?>
                                                                                </span>
                                                                            </span>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php if ($pagination) { ?>
                                            <div class="collection-bottom-toolbar">
                                                <div class="product-pagination">
                                                    <div class="pagination_group">
                                                        <?php echo $pagination; ?>
                                                    </div>
                                                </div>
                                            </div>
                                    <?php   }
                                        } else {
                                    ?>
                                            <p>Tidak ada produk yang dapat ditampilkan</p>
                                    <?php
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
