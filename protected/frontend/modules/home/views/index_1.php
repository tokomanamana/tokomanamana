<div class="shopify-section index-section index-section-slideshow">
    <div>
        <section class="home_slideshow main-slideshow">
            <div class="home-slideshow-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="group-home-slideshow">
                            <div class="home-sidemenu-inner col-sm-3">
                                <ul class="navigation_links_left">
                                    <?php if ($categories) foreach ($categories->result() as $category) { ?>
                                            <li class="nav-item _icon">
                                                <a href="<?php echo seo_url('catalog/categories/view/' . $category->id); ?>">
                                                    <img src="<?php echo get_image($category->icon); ?>">
                                                    <span><?php echo $category->name; ?></span>
                                                </a>
                                            </li>
                                        <?php } ?>
                                </ul>
                            </div>
                            <div class="home-slideshow-inner col-sm-9">
                                <div class="home-slideshow">
                                    <div id="home_main-slider" class="carousel slide  main-slider">
                                        <ol class="carousel-indicators">
                                            <?php
                                            if ($banners) {
                                                $i = 0;
                                                foreach ($banners->result() as $banner) {
                                                    ?>
                                                    <li data-target="#home_main-slider" data-slide-to="<?php echo $i; ?>" class="carousel-<?php echo $i + 1; ?> <?php echo ($i == 0) ? 'active' : ''; ?>"></li>
                                                    <?php
                                                    $i++;
                                                }
                                            }
                                            ?>
                                        </ol>
                                        <div class="carousel-inner">
                                            <?php
                                            if ($banners) {
                                                $i = 0;
                                                foreach ($banners->result() as $banner) {
                                                    ?>
                                                    <div class="item image <?php echo ($i == 0) ? 'active' : ''; ?>">
                                                        <img src="<?php echo get_image($banner->image); ?>" alt="<?php echo $banner->name; ?>" title="<?php echo $banner->name; ?>">
                                                        <?php if ($banner->content) echo $banner->content; ?>
                                                    </div>
                                                    <?php
                                                    $i++;
                                                }
                                            }
                                            ?>
                                        </div>
                                        <a class="left carousel-control" href="#home_main-slider" data-slide="prev">
                                            <span class="icon-prev"></span>
                                        </a>
                                        <a class="right carousel-control" href="#home_main-slider" data-slide="next">
                                            <span class="icon-next"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="home-banner-inner col-sm-3">
                                <div class="banner-content">
                                    <?php if ($block_slider) foreach ($block_slider->result() as $block) { ?>
                                            <div class="banner-1">
                                                <a href="<?php echo $block->link; ?>">
                                                    <img src="<?php echo get_image($block->image); ?>" alt="<?php echo $block->name; ?>">
                                                </a>
                                            </div>
                                        <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!--<div id="shopify-section-1493002138267" class="shopify-section index-section index-section-procol">
    <div data-section-id="1493002138267" data-section-type="procol-section">
        <section class="home_procol_layout">
            <div class="home_procol_wrapper">
                <div class="container">
                    <div class="row">
                        <div class="home_procol_inner">
                            <div class="page-title">
                                <h2>Discount Today</h2>
                                <a href="#">View all products</a>
                            </div>
                            <div class="home_procol_content">
                                <div class="procol_item procol_pro_one">
                                    <div class="row-container product list-unstyled clearfix">
                                        <div class="row-left">
                                            <a href="./product.html" class="hoverBorder container_item">
                                                <div class="hoverBorderWrapper">
                                                    <img src="<?php echo site_url('assets/frontend/images/demo_195×195.png'); ?>" class="not-rotation img-responsive front" alt="Hair Care">
                                                    <div class="mask"></div>
                                                    <img src="<?php echo site_url('assets/frontend/images/demo_195×195.png'); ?>" class="rotation img-responsive" alt="Hair Care">
                                                </div>
                                            </a>
                                            <span class="sale_banner">
                                                <span class="sale_text">-50.0%</span>
                                            </span>
                                            <div class="hover-mask">
                                                <div class="group-mask">
                                                    <div class="inner-mask">
                                                        <div class="group-actionbutton">
                                                            <form action="./cart.html" method="post">
                                                                <div class="effect-ajax-cart">
                                                                    <input type="hidden" name="quantity" value="1">
                                                                    <button class="btn select-option" type="button"><i class="fa fa-bars"></i></button>
                                                                </div>
                                                            </form>
                                                            <ul class="quickview-wishlist-wrapper">
                                                                <li class="quickview hidden-xs hidden-sm">
                                                                    <div class="product-ajax-cart">
                                                                        <span class="overlay_mask"></span>
                                                                        <div data-handle="hair-care" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                            <a class=""><i class="fa fa-search" title="Quick View"></i></a>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="wishlist hidden-xs">
                                                                    <a class="wish-list" href="./wish-list.html"><span class="hidden-xs"><i class="fa fa-heart" title="Wishlist"></i></span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    inner-mask
                                                </div>
                                                Group mask
                                            </div>
                                        </div>

                                        <div class="row-right animMix">
                                            <div class="product-title"><a class="title-5" href="./product.html">Hair Care</a></div>
                                            <div class="rating-star">
                                                <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                </span>
                                            </div>
                                            <div class="product-price">
                                                <span class="price_sale"><span class="money" data-currency-usd="$200.00">$200.00</span></span>
                                                <del class="price_compare"> <span class="money" data-currency-usd="$400.00">$400.00</span></del>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="procol_item">
                                    <div class="procol_pro_two">
                                        <div class="row-container ">
                                            <div class="product home_product">
                                                <div class="row-left">
                                                    <a href="./product.html" class="container_item">
                                                        <div class="hoverBorderWrapper">
                                                            <img src="<?php echo site_url('assets/frontend/images/demo_64×64.png'); ?>" class="not-rotation img-responsive front" alt="Laptops">
                                                            <div class="mask"></div>
                                                            <img src="<?php echo site_url('assets/frontend/images/demo_64×64.png'); ?>" class="rotation img-responsive" alt="Laptops">
                                                        </div>
                                                    </a>
                                                    <span class="sale_banner">
                                                        <span class="sale_text">-50.0%</span>
                                                    </span>
                                                </div>
                                                <div class="row-right">
                                                    <div class="product-title"><a class="title-5" href="./product.html">Laptops</a></div>
                                                    <div class="rating-star">
                                                        <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                        </span>
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="price_sale"><span class="money" data-currency-usd="$200.00">$200.00</span></span>
                                                        <del class="price_compare"> <span class="money" data-currency-usd="$400.00">$400.00</span></del>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="procol_pro_two">
                                        <div class="row-container ">
                                            <div class="product home_product">
                                                <div class="row-left">
                                                    <a href="./product.html" class="container_item">
                                                        <div class="hoverBorderWrapper">
                                                            <img src="<?php echo site_url('assets/frontend/images/demo_64×64.png'); ?>" class="not-rotation img-responsive front" alt="Laptops">
                                                            <div class="mask"></div>
                                                            <img src="<?php echo site_url('assets/frontend/images/demo_64×64.png'); ?>" class="rotation img-responsive" alt="Laptops">
                                                        </div>
                                                    </a>
                                                    <span class="sale_banner">
                                                        <span class="sale_text">-50.0%</span>
                                                    </span>
                                                </div>
                                                <div class="row-right">
                                                    <div class="product-title"><a class="title-5" href="./product.html">LG G5</a></div>
                                                    <div class="rating-star">
                                                        <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                        </span>
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="price_sale"><span class="money" data-currency-usd="$200.00">$200.00</span></span>
                                                        <del class="price_compare"> <span class="money" data-currency-usd="$400.00">$400.00</span></del>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="procol_item procol_pro_one">
                                    <div class="row-container product list-unstyled clearfix">
                                        <div class="row-left">
                                            <a href="./product.html" class="hoverBorder container_item">
                                                <div class="hoverBorderWrapper">
                                                    <img src="<?php echo site_url('assets/frontend/images/demo_195×195.png'); ?>" class="not-rotation img-responsive front" alt="Hair Care">
                                                    <div class="mask"></div>
                                                    <img src="<?php echo site_url('assets/frontend/images/demo_195×195.png'); ?>" class="rotation img-responsive" alt="Hair Care">
                                                </div>
                                            </a>
                                            <span class="sale_banner">
                                                <span class="sale_text">-50.0%</span>
                                            </span>
                                            <div class="hover-mask">
                                                <div class="group-mask">
                                                    <div class="inner-mask">
                                                        <div class="group-actionbutton">
                                                            <form action="./cart.html" method="post">
                                                                <div class="effect-ajax-cart">
                                                                    <input type="hidden" name="quantity" value="1">
                                                                    <button class="btn select-option" type="button"><i class="fa fa-bars"></i></button>
                                                                </div>
                                                            </form>
                                                            <ul class="quickview-wishlist-wrapper">
                                                                <li class="quickview hidden-xs hidden-sm">
                                                                    <div class="product-ajax-cart">
                                                                        <span class="overlay_mask"></span>
                                                                        <div data-handle="hair-care" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                            <a class=""><i class="fa fa-search" title="Quick View"></i></a>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="wishlist hidden-xs">
                                                                    <a class="wish-list" href="./wish-list.html"><span class="hidden-xs"><i class="fa fa-heart" title="Wishlist"></i></span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    inner-mask
                                                </div>
                                                Group mask
                                            </div>
                                        </div>

                                        <div class="row-right animMix">
                                            <div class="product-title"><a class="title-5" href="./product.html">Light Therapy</a></div>
                                            <div class="rating-star">
                                                <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                </span>
                                            </div>
                                            <div class="product-price">
                                                <span class="price_sale"><span class="money" data-currency-usd="$200.00">$200.00</span></span>
                                                <del class="price_compare"> <span class="money" data-currency-usd="$400.00">$400.00</span></del>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="procol_item">
                                    <div class="procol_pro_two">
                                        <div class="row-container ">
                                            <div class="product home_product">
                                                <div class="row-left">
                                                    <a href="./product.html" class="container_item">
                                                        <div class="hoverBorderWrapper">
                                                            <img src="<?php echo site_url('assets/frontend/images/demo_64×64.png'); ?>" class="not-rotation img-responsive front" alt="Laptops">
                                                            <div class="mask"></div>
                                                            <img src="<?php echo site_url('assets/frontend/images/demo_64×64.png'); ?>" class="rotation img-responsive" alt="Laptops">
                                                        </div>
                                                    </a>
                                                    <span class="sale_banner">
                                                        <span class="sale_text">-50.0%</span>
                                                    </span>
                                                </div>
                                                <div class="row-right">
                                                    <div class="product-title"><a class="title-5" href="./product.html">iPhone</a></div>
                                                    <div class="rating-star">
                                                        <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                        </span>
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="price_sale"><span class="money" data-currency-usd="$200.00">$200.00</span></span>
                                                        <del class="price_compare"> <span class="money" data-currency-usd="$400.00">$400.00</span></del>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="procol_pro_two">
                                        <div class="row-container ">
                                            <div class="product home_product">
                                                <div class="row-left">
                                                    <a href="./product.html" class="container_item">
                                                        <div class="hoverBorderWrapper">
                                                            <img src="<?php echo site_url('assets/frontend/images/demo_64×64.png'); ?>" class="not-rotation img-responsive front" alt="Laptops">
                                                            <div class="mask"></div>
                                                            <img src="<?php echo site_url('assets/frontend/images/demo_64×64.png'); ?>" class="rotation img-responsive" alt="Laptops">
                                                        </div>
                                                    </a>
                                                    <span class="sale_banner">
                                                        <span class="sale_text">-50.0%</span>
                                                    </span>
                                                </div>
                                                <div class="row-right">
                                                    <div class="product-title"><a class="title-5" href="./product.html">iPad</a></div>
                                                    <div class="rating-star">
                                                        <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                        </span>
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="price_sale"><span class="money" data-currency-usd="$200.00">$200.00</span></span>
                                                        <del class="price_compare"> <span class="money" data-currency-usd="$400.00">$400.00</span></del>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="procol_item procol_pro_one">
                                    <div class="row-container product list-unstyled clearfix">
                                        <div class="row-left">
                                            <a href="./product.html" class="hoverBorder container_item">
                                                <div class="hoverBorderWrapper">
                                                    <img src="<?php echo site_url('assets/frontend/images/demo_195×195.png'); ?>" class="not-rotation img-responsive front" alt="Hair Care">
                                                    <div class="mask"></div>
                                                    <img src="<?php echo site_url('assets/frontend/images/demo_195×195.png'); ?>" class="rotation img-responsive" alt="Hair Care">
                                                </div>
                                            </a>
                                            <span class="sale_banner">
                                                <span class="sale_text">-50.0%</span>
                                            </span>
                                            <div class="hover-mask">
                                                <div class="group-mask">
                                                    <div class="inner-mask">
                                                        <div class="group-actionbutton">
                                                            <form action="./cart.html" method="post">
                                                                <div class="effect-ajax-cart">
                                                                    <input type="hidden" name="quantity" value="1">
                                                                    <button class="btn select-option" type="button"><i class="fa fa-bars"></i></button>
                                                                </div>
                                                            </form>
                                                            <ul class="quickview-wishlist-wrapper">
                                                                <li class="quickview hidden-xs hidden-sm">
                                                                    <div class="product-ajax-cart">
                                                                        <span class="overlay_mask"></span>
                                                                        <div data-handle="hair-care" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                            <a class=""><i class="fa fa-search" title="Quick View"></i></a>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="wishlist hidden-xs">
                                                                    <a class="wish-list" href="./wish-list.html"><span class="hidden-xs"><i class="fa fa-heart" title="Wishlist"></i></span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    inner-mask
                                                </div>
                                                Group mask
                                            </div>
                                        </div>

                                        <div class="row-right animMix">
                                            <div class="product-title"><a class="title-5" href="./product.html">Mixers</a></div>
                                            <div class="rating-star">
                                                <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                </span>
                                            </div>
                                            <div class="product-price">
                                                <span class="price_sale"><span class="money" data-currency-usd="$200.00">$200.00</span></span>
                                                <del class="price_compare"> <span class="money" data-currency-usd="$400.00">$400.00</span></del>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="procol_item">
                                    <div class="procol_pro_two">
                                        <div class="row-container ">
                                            <div class="product home_product">
                                                <div class="row-left">
                                                    <a href="./product.html" class="container_item">
                                                        <div class="hoverBorderWrapper">
                                                            <img src="<?php echo site_url('assets/frontend/images/demo_64×64.png'); ?>" class="not-rotation img-responsive front" alt="Laptops">
                                                            <div class="mask"></div>
                                                            <img src="<?php echo site_url('assets/frontend/images/demo_64×64.png'); ?>" class="rotation img-responsive" alt="Laptops">
                                                        </div>
                                                    </a>
                                                    <span class="sale_banner">
                                                        <span class="sale_text">-50.0%</span>
                                                    </span>
                                                </div>
                                                <div class="row-right">
                                                    <div class="product-title"><a class="title-5" href="./product.html">Office</a></div>
                                                    <div class="rating-star">
                                                        <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                        </span>
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="price_sale"><span class="money" data-currency-usd="$200.00">$200.00</span></span>
                                                        <del class="price_compare"> <span class="money" data-currency-usd="$400.00">$400.00</span></del>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="procol_pro_two">
                                        <div class="row-container ">
                                            <div class="product home_product">
                                                <div class="row-left">
                                                    <a href="./product.html" class="container_item">
                                                        <div class="hoverBorderWrapper">
                                                            <img src="<?php echo site_url('assets/frontend/images/demo_64×64.png'); ?>" class="not-rotation img-responsive front" alt="Laptops">
                                                            <div class="mask"></div>
                                                            <img src="<?php echo site_url('assets/frontend/images/demo_64×64.png'); ?>" class="rotation img-responsive" alt="Laptops">
                                                        </div>
                                                    </a>
                                                    <span class="sale_banner">
                                                        <span class="sale_text">-50.0%</span>
                                                    </span>
                                                </div>
                                                <div class="row-right">
                                                    <div class="product-title"><a class="title-5" href="./product.html">OLED TVs</a></div>
                                                    <div class="rating-star">
                                                        <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                        </span>
                                                    </div>
                                                    <div class="product-price">
                                                        <span class="price_sale"><span class="money" data-currency-usd="$200.00">$200.00</span></span>
                                                        <del class="price_compare"> <span class="money" data-currency-usd="$400.00">$400.00</span></del>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div id="shopify-section-1492746108232" class="shopify-section index-section index-section-banner">
    <div data-section-id="1492746108232" data-section-type="banner-section">
        <section class="home_banner_layout">
            <div class="home_banner_wrapper">
                <div class="container">
                    <div class="row">
                        <div class="home_banner_inner">
                            <div class="home_banner_content">
                                <div class="col-sm-6 banner_item">
                                    <a href="#">
                                        <img src="<?php echo site_url('assets/frontend/images/demo_570×108.png'); ?>" alt="">
                                    </a>
                                </div>
                                <div class="col-sm-6 banner_item">
                                    <a href="#">
                                        <img src="<?php echo site_url('assets/frontend/images/demo_570×108.png'); ?>" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>-->
<?php
if ($categories) {
    $i = 0;
    foreach ($categories->result() as $category) {
        $i++;
//        $subcategories = $this->home->get_sub_categories($category->id, 8);
        $products = $this->home->products($category->id, 9);
        if ($products) {
            ?>
            <div class="shopify-section index-section index-section-proban">
                <div>
                    <section class="home_proban_layout">
                        <div class="home_proban_wrapper">
                            <div class="container">
                                <div class="row">
                                    <div class="home_proban_inner">
                                        <div class="page-title" style="border-color: <?php echo $category->border_color; ?>">
                                            <div class="group_title">
                                                <h2><?php echo $category->name; ?></h2>
                                                <ul class="link_menu">
                                                    <li class="nav-item">
                                                        <a href="#">Best Sellers</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a href="#">Specials</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a href="#">Most Reviews</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a href="#">New Arrivals</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <a href="<?php echo seo_url('catalog/categories/view/' . $category->id); ?>">View all products</a>
                                        </div>
                                        <div class="home_proban_content">
                                            <div class="col-sm-6 proban_banner">
                                                <a href="#">
                                                    <img src="<?php echo ($category->image_home) ? get_image($category->image_home) : site_url('assets/frontend/images/demo_584×257.png'); ?>" alt="">
                                                </a>
                                            </div>
                                            <?php foreach ($products->result() as $product) { ?>
                                                <div class="col-sm-2 proban_product">
                                                    <div class="row-container product list-unstyled clearfix">
                                                        <div class="row-left">
                                                            <a href="<?php echo seo_url('catalog/products/view/' . $product->id); ?>" class="hoverBorder container_item">
                                                                <div class="hoverBorderWrapper">
                                                                    <img src="<?php echo get_image($product->image); ?>" class="img-responsive front" alt="<?php echo $product->name; ?>">
                                                                </div>
                                                            </a>
                                                            <?php if ($product->discount) { ?>
                                                                <span class="sale_banner">
                                                                    <span class="sale_text">-<?php echo $product->discount; ?>%</span>
                                                                </span>
                                                            <?php } ?>
                                                        </div>

                                                        <div class="row-right animMix">
                                                            <div class="product-title"><a class="title-5" href="<?php echo seo_url('catalog/products/view/' . $product->id); ?>"><?php echo $product->name; ?></a></div>
                                                            <div class="rating-star">
                                                                <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                                </span>
                                                            </div>
                                                            <div class="product-price">
                                                                <?php if (!$product->merchant && $product->total_merchant == 0) { ?>
                                                                    N/A
                                                                <?php } else { ?>
                                                                    <span class="price_sale"><span class="money"><?php echo rupiah($product->price); ?></span></span>
                                                                    <?php if ($product->discount > 0) { ?>
                                                                        <del class="price_compare"> <span class="money"><?php echo rupiah($product->price_old); ?></span></del>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <?php
        }
    }
}
?>
<div id="shopify-section-1493279349128" class="shopify-section index-section index-section-bansli"><div data-section-id="1493279349128" data-section-type="bansli-section">
        <section class="home_bansli_layout">
            <div class="home_bansli_wrapper">
                <div class="container">
                    <div class="row">
                        <div class="home_bansli_inner">
                            <div class="home_bansli_content">

                                <div class="bansli_left">
                                    <div class="left_image_top">

                                        <a href="/collections/appliances">
                                            <img src="//cdn.shopify.com/s/files/1/1843/7025/files/12.jpg?v=1493279377" alt="">
                                        </a>


                                        <a href="/collections/cellphone">
                                            <img src="//cdn.shopify.com/s/files/1/1843/7025/files/11.jpg?v=1493279384" alt="">
                                        </a>

                                    </div>
                                    <div class="left_image_bottom">

                                        <a href="/collections/home-office">
                                            <img src="//cdn.shopify.com/s/files/1/1843/7025/files/13.jpg?v=1493279392" alt="">
                                        </a>

                                    </div>
                                </div>


                                <div class="bansli_middle">
                                    <span class="middle_title">
                                        Famous Brands
                                    </span>
                                    <div class="middle_content">
                                        <ul class="middle_linklist_left linklist_menu">

                                            <li>
                                                <a href="/collections">Calvin Klein</a>
                                            </li>

                                            <li>
                                                <a href="/collections">Jockey</a>
                                            </li>

                                            <li>
                                                <a href="/collections">Bare Necessities</a>
                                            </li>

                                            <li>
                                                <a href="/collections">Spanx</a>
                                            </li>

                                            <li>
                                                <a href="/collections">Crocs Outlet</a>
                                            </li>

                                            <li>
                                                <a href="/collections">Speedo</a>
                                            </li>

                                            <li>
                                                <a href="/collections">Tommy Hilfiger</a>
                                            </li>

                                            <li>
                                                <a href="/collections">Dooney &amp; Bourke</a>
                                            </li>

                                            <li>
                                                <a href="/collections">The Watchery</a>
                                            </li>

                                            <li>
                                                <a href="/collections">Jomashop</a>
                                            </li>

                                        </ul>
                                        <ul class="middle_linklist_right linklist_menu">

                                            <li>
                                                <a href="/collections">Guess</a>
                                            </li>

                                            <li>
                                                <a href="/collections">Hanes</a>
                                            </li>

                                            <li>
                                                <a href="/collections">Wilson Leather</a>
                                            </li>

                                            <li>
                                                <a href="/collections">Wacoal</a>
                                            </li>

                                            <li>
                                                <a href="/collections">Ebags</a>
                                            </li>

                                            <li>
                                                <a href="/collections">Timex</a>
                                            </li>

                                            <li>
                                                <a href="/collections">Charles Wilson</a>
                                            </li>

                                            <li>
                                                <a href="/collections">Puma</a>
                                            </li>

                                            <li>
                                                <a href="/collections">Asics</a>
                                            </li>

                                            <li>
                                                <a href="/collections">Sunglasses</a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>


                                <div class="bansli_right">
                                    <div class="right_content right_1493279349128 owl-carousel owl-theme" style="display: block; opacity: 1;">

                                        <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 1436px; left: 0px; display: block;"><div class="owl-item" style="width: 359px;"><div class="right_item">
                                                        <a href="">
                                                            <img src="//cdn.shopify.com/s/files/1/1843/7025/files/14.jpg?v=1493279436" alt="">
                                                        </a>
                                                    </div></div><div class="owl-item" style="width: 359px;"><div class="right_item">
                                                        <a href="">
                                                            <img src="//cdn.shopify.com/s/files/1/1843/7025/files/14.jpg?v=1493279445" alt="">
                                                        </a>
                                                    </div></div></div></div>	



                                        <div class="owl-controls"><div class="owl-buttons"><div class="owl-prev"><i class="fa fa-angle-left"></i></div><div class="owl-next"><i class="fa fa-angle-right"></i></div></div></div></div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>   
        </section>
    </div>

    <script>
        $(".right_1493279349128").owlCarousel({
            navigation: true,
            pagination: false,
            autoPlay: false,
            items: 1,
            slideSpeed: 200,
            paginationSpeed: 800,
            rewindSpeed: 1000,
            itemsDesktop: [1199, 1],
            itemsDesktopSmall: [979, 1],
            itemsTablet: [768, 1],
            itemsTabletSmall: [540, 1],
            itemsMobile: [360, 1],
            navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
        });
    </script>

</div>
<div id="shopify-section-1493002960357" class="shopify-section index-section index-section-prosli"><div data-section-id="1493002960357" data-section-type="blog-section">
        <section class="home_blog_layout">
            <div class="home_blog_wrapper">
                <div class="container">
                    <div class="row">
                        <div class="home_blog_inner">
                            <div class="page-title">
                                <h2>From the blog</h2>
                                <a href="#">View all blog</a>
                            </div>
                            <div class="home_blog_content">



                                <div class="article-wrapper col-sm-3">
                                    <div class="article-inner">
                                        <div class="article-top">
                                            <a class="article-img" href="/blogs/news/pellentesque-vitae-enim-quis-risus-6">
                                                <img src="//cdn.shopify.com/s/files/1/1843/7025/articles/10.jpg?v=1491277590" alt="Pellentesque vitae enim quis risus">
                                            </a>
                                        </div>
                                        <div class="article-bottom">
                                            <div class="article-name"><a href="/blogs/news/pellentesque-vitae-enim-quis-risus-6">Pellentesque vitae enim quis risus</a></div>
                                            <ul class="article-info list-inline">
                                                <li class="article-date">Apr 03, 2017</li>                     
                                                <li class="article-comment">
                                                    <a href="/blogs/news/pellentesque-vitae-enim-quis-risus-6#comments">

                                                        <span> - </span>
                                                        <i class="comment">0 comment</i>

                                                    </a>
                                                </li>                      
                                            </ul>
                                            <div class="article-content"><p>Pellentesque vitae gravida urna. Mauris sit amet feugiat quam, sed tempor justo....</p></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="article-wrapper col-sm-3">
                                    <div class="article-inner">
                                        <div class="article-top">
                                            <a class="article-img" href="/blogs/news/pellentesque-vitae-enim-quis-risus-5">
                                                <img src="//cdn.shopify.com/s/files/1/1843/7025/articles/09.jpg?v=1491277570" alt="Pellentesque vitae enim quis risus">
                                            </a>
                                        </div>
                                        <div class="article-bottom">
                                            <div class="article-name"><a href="/blogs/news/pellentesque-vitae-enim-quis-risus-5">Pellentesque vitae enim quis risus</a></div>
                                            <ul class="article-info list-inline">
                                                <li class="article-date">Apr 03, 2017</li>                     
                                                <li class="article-comment">
                                                    <a href="/blogs/news/pellentesque-vitae-enim-quis-risus-5#comments">

                                                        <span> - </span>
                                                        <i class="comment">0 comment</i>

                                                    </a>
                                                </li>                      
                                            </ul>
                                            <div class="article-content"><p>Pellentesque vitae gravida urna. Mauris sit amet feugiat quam, sed tempor justo....</p></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="article-wrapper col-sm-3">
                                    <div class="article-inner">
                                        <div class="article-top">
                                            <a class="article-img" href="/blogs/news/pellentesque-vitae-enim-quis-risus-4">
                                                <img src="//cdn.shopify.com/s/files/1/1843/7025/articles/12.jpg?v=1491277557" alt="Pellentesque vitae enim quis risus">
                                            </a>
                                        </div>
                                        <div class="article-bottom">
                                            <div class="article-name"><a href="/blogs/news/pellentesque-vitae-enim-quis-risus-4">Pellentesque vitae enim quis risus</a></div>
                                            <ul class="article-info list-inline">
                                                <li class="article-date">Apr 03, 2017</li>                     
                                                <li class="article-comment">
                                                    <a href="/blogs/news/pellentesque-vitae-enim-quis-risus-4#comments">

                                                        <span> - </span>
                                                        <i class="comment">0 comment</i>

                                                    </a>
                                                </li>                      
                                            </ul>
                                            <div class="article-content"><p>Pellentesque vitae gravida urna. Mauris sit amet feugiat quam, sed tempor justo....</p></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="article-wrapper col-sm-3">
                                    <div class="article-inner">
                                        <div class="article-top">
                                            <a class="article-img" href="/blogs/news/pellentesque-vitae-enim-quis-risus-3">
                                                <img src="//cdn.shopify.com/s/files/1/1843/7025/articles/02.jpg?v=1491277548" alt="Pellentesque vitae enim quis risus">
                                            </a>
                                        </div>
                                        <div class="article-bottom">
                                            <div class="article-name"><a href="/blogs/news/pellentesque-vitae-enim-quis-risus-3">Pellentesque vitae enim quis risus</a></div>
                                            <ul class="article-info list-inline">
                                                <li class="article-date">Apr 03, 2017</li>                     
                                                <li class="article-comment">
                                                    <a href="/blogs/news/pellentesque-vitae-enim-quis-risus-3#comments">

                                                        <span> - </span>
                                                        <i class="comment">0 comment</i>

                                                    </a>
                                                </li>                      
                                            </ul>
                                            <div class="article-content"><p>Pellentesque vitae gravida urna. Mauris sit amet feugiat quam, sed tempor justo....</p></div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>   
        </section>
    </div>
</div>