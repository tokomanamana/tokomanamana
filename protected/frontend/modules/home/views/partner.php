
<style>
.big-title-segment{font-size:18px;color:#a7c22a;font-weight:700;margin-bottom:20px}.box-partner{box-shadow:0 1px 6px 0 rgba(49,53,59,.12);border:1px solid #a7c22a;border-radius:3px}.enter{margin-top:25px;margin-bottom:25px;overflow:auto;width:100%}.image_container{display:grid;align-items:center;grid-template-columns:repeat(auto-fit,minmax(180px,1fr));grid-gap:1.5em}.individual_container{border:1px solid;border-color:#a7c22a;padding:15px;border-radius:3%}.individual_container img{max-height:50px;margin-left:auto;margin-right:auto;display:block;max-width:1000}body{background-color:#fff}
</style>
<div class="enter"></div>
<div class="container">
   <?php  if ($brands) { ?>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                    <div class="row big-title-segment">PARTNER RESMI</div>
                </div>
                
            </div>
        </div>
        <div class="col-sm-12">
            <div class="image_container" >
                
                <?php foreach ($brands->result() as $brand) { ?>

                    <div class="individual_container">
                    <a href="<?php echo ($brand->principle_id ? seo_url('principal_home/view/' . $brand->principle_id) : seo_url('catalog/brands/view/' . $brand->id)) ?>">
                        <img style=""height=100 src="<?php echo get_image($brand->image); ?>" alt="<?php echo $brand->name;?>">
                    </a>
                    </div>
                <?php } ?>
                
            </div>
        </div>

    <?php } ?>
</div>

