<div class="main">
    <div class="container" style="margin-bottom: 30px;">
        <article>  
            <div class="parallax">    
                <div class="bg__foo">foo</div>    
                <!--                <div class="bg__bar">bar</div>    
                                <div class="bg__baz">baz</div>    
                                <div class="bg__bazz">bazz</div>  -->
            </div>  
            <header>    
                <h1><?php echo settings('store_name'); ?></h1>    
                <h2>Solusi Belanja IT Murah, Harga Jujur Tanpa Diskon</h2>    
            </header>  
            <section>    
                <p><?php echo settings('store_name'); ?> merupakan salah satu perusahaan Online Store di Indonesia yang telah berdiri sejak 2013 dan telah melayani penjualan online sejak tahun 2014 sampai saat ini. Berkomitmen untuk menjual barang-barang kebutuhan IT dengan harga murah meski tidak pernah ada diskon, pengiriman yang cepat, dan kualitas yang prima.</p>
                <p><?php echo settings('store_name'); ?> akan memberikan Anda kemudahan dan kenyamanan dalam berbelanja barang-barang kebutuhan IT.</p>
                <p>Produk dari berbagai brand ternama kami sediakan untuk Anda, seperti HP, Lenovo, Logitech, Asus, Acer, Toshiba, Samsung, Sandisk, dan lainnya.</p>
                <p>Dengan berbagai macam kategori yang kami tawarkan akan menjadi pilihan yang tepat bagi Anda untuk berbelanja di <?php echo settings('store_name'); ?></p>
            </section>  
            <section class="parallax">    
                <div class="bg__break" id="map">          
                </div>  
                <div class="capture"><h2>Kami disini</h2>Jalan Raya Syekh Quro Telagasari<br>Desa Talagasari RT 02 RW 01<br>Telagasari, Karawang<br>Jawa Barat 41381</div>
            </section>  
        </article>
        <script>
            var marker;
            function initMap() {
                var mapDiv = document.getElementById('map');
                var map = new google.maps.Map(mapDiv, {
                    center: {lat: -6.288898, lng: 107.391548},
                    zoom: 16
                });
                marker = new google.maps.Marker({
                    map: map,
                    animation: google.maps.Animation.DROP,
                    position: {lat: -6.288898, lng: 107.391548},
                    title: 'osadasd'
                });
                marker.addListener('click', toggleBounce);
            }
            function toggleBounce() {
                if (marker.getAnimation() !== null) {
                    marker.setAnimation(null);
                } else {
                    marker.setAnimation(google.maps.Animation.BOUNCE);
                }
            }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?callback=initMap"
        async defer></script>
    </div>
</div>