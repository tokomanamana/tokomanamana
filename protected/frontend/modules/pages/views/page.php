<section class="page-content">
    <div class="page-wrapper">
        <div class="container">
            <div class="row">
                <div class="page-inner">
                    <div class="shopify-section">
                        <div class="about-content">
                            <?php if (!$options->hidden_title) { ?>
                                <h1 class="title text-center"><?php echo $data->title; ?></h1>
                            <?php } ?>
                            <?php echo htmlspecialchars_decode($data->content); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
