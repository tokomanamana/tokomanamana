<?php echo $breadcrumb; ?>
<div class="main">
    <div class="container" style="margin-bottom: 30px;">
        <div class="row">
            <?php $this->load->view('navigation'); ?>
            <div class="col-sm-9 col-right">
                <div class="page-title">
                    <h1><?php echo lang('help_text_warranty'); ?></h1>
                </div>
                <div class="panel-group" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" data-toggle="collapse" href="#warranty" aria-expanded="true" aria-controls="collapseOne" style="cursor: pointer; display: block;">
                            Klaim Garansi
                        </div>

                        <div id="warranty" class="panel-collapse collapse in" role="tabpanel">
                            <div class="panel-body" style="padding-left: 30px; padding-right: 30px;">
                                <p>Proses Klaim Garansi:</p>
                                <ul>
                                    <li>Untuk melakukan klaim garansi, Anda dapat menghubungi kami cs@tokokarawang.com kemudian sampaikan No Invoice dan Deskripsi Kerusakan beserta fotonya atau telpon kami di <?php echo settings('phone'); ?> untuk informasi lebih lanjut.</li>
                                    <li>Anda juga dapat klaim garansi dengan mendatangi toko kami secara langsung untuk proses pengecekkan lebih cepat.</li>
                                </ul>
                                <p>Semua barang yang dijual <?php echo settings('store_name') ?> memiliki garansi 7 hari ganti baru</p>
                                <ul>
                                    <li>Meski tertulis tidak ada garansi pada barang, namun apabila terdapat cacat barang saat baru diterima. Dapat claim langsung kepada kami untuk penggantian dengan barang baru.</li>
                                    <li>Ongkos kirim pengembalian produk dari <?php echo settings('store_name'); ?> kepada pelanggan sepenuhnya ditanggung oleh pelanggan</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>