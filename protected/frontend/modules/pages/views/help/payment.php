<?php echo $breadcrumb; ?>
<div class="main">
    <div class="container" style="margin-bottom: 30px;">
        <div class="row">
            <?php $this->load->view('navigation'); ?>
            <div class="col-sm-9 col-right">
                <div class="page-title">
                    <h1><?php echo lang('help_text_payment'); ?></h1>
                </div>
                <div class="panel-group" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" data-toggle="collapse" href="#warranty" aria-expanded="true" aria-controls="collapseOne" style="cursor: pointer; display: block;">
                            Transfer Bank
                        </div>

                        <div id="warranty" class="panel-collapse collapse in" role="tabpanel">
                            <div class="panel-body" style="padding-left: 30px; padding-right: 30px;">
                                <p>Pembayaran dengan metode transfer bank dapat dilakukan dengan cara mengirimkan dana sebesar pesanan ke salah satu rekening berikut ini:</p>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Bank</th>
                                            <th>No. Rekening</th>
                                            <th>Atas Nama</th>
                                            <th>Cabang</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="vertical-align: middle"><img src="<?php echo site_url('assets/frontend/images/bca.png'); ?>" width="100px"></td>
                                            <td style="vertical-align: middle"><span style="color: #fc411d;">1091 44 3151</span></td>
                                            <td style="vertical-align: middle">Rifky Syaripudin</td>
                                            <td style="vertical-align: middle">Cab. Karawang</td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle"><img src="<?php echo site_url('assets/frontend/images/mandiri.png'); ?>" width="100px"></td>
                                            <td style="vertical-align: middle"><span style="color: #fc411d;">173 00000 289 52</span></td>
                                            <td style="vertical-align: middle">Rifky Syaripudin</td>
                                            <td style="vertical-align: middle">Cab. Karawang</td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle"><img src="<?php echo site_url('assets/frontend/images/bri.png'); ?>" width="100px"></td>
                                            <td style="vertical-align: middle"><span style="color: #fc411d;">79070 1000 912 537</span></td>
                                            <td style="vertical-align: middle">Rifky Syaripudin</td>
                                            <td style="vertical-align: middle">Cab. Karang Pawitan, Karawang</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <p>Perlu diketahui bahwa saat ini TokoKarawang.com hanya memiliki 3 rekening diatas, dan apabila ada pihak yang mengatasnamakan TokoKarawang.com dan memberikan nomor rekening selain diatas dapat dipastikan itu adalah tindak penipuan dan bukan merupakan tanggung jawab kami.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>