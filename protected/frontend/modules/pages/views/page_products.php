<link rel="stylesheet" href="<?php echo site_url('assets/frontend/css/minified/home_min.css') ?>">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
<!-- Add the slick-theme.css if you want default styling -->
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css" />
<?php if($data->image) : ?>
   <section class="home_slideshow main-slideshow" style="margin-top: 0px;">
      <div class="home-slideshow-wrapper">
          <div class="container">
              <div class="row">
                  <div class="group-home-slideshow">
                    <div class="home-slideshow-inner col-xs-12 col-lg-12" style="text-align: center;">
                      <div class="home-slideshow">
                            <div>
                                <img class="lazyload" src="<?php echo base_url('assets/frontend/images/loading.gif')?>" data-src="<?php echo get_image($data->image); ?>" alt="<?php echo $data->title; ?>" title="<?php echo $data->title; ?>" style="border-radius: 5px;">
                            </div>
                        </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
<?php endif; ?>
<section class="collection-heading heading-content" style="margin-top: 25px;">
  <div class="container">
    <div class="row">
      <div class="collection-wrapper" style="padding-right: 5px;padding-left: 5px;">
        <h1 class="collection-title">
          <span class="title-segment">
            <?php echo $title_page; ?>
          </span>
        </h1>
      </div>
    </div>
  </div>
</section>
<section class="collection-content">
  <div class="collection-wrapper">
    <div class="container">
      <div class="row">
        <div id="shopify-section-collection-template" class="shopify-section">
          <div class="collection-inner">
            <div id="tags-load" style="display:none;"><i class="fa fa-spinner fa-pulse fa-2x"></i></div>
            <div id="collection">
              <div class="collection_inner">
                <div class="collection-mainarea col-sm-12 clearfix">
                  <?php if ($products) { ?>
                    <div class="collection-items clearfix" style="border:none;">
                      <div class="products" style="border:none;">
                        <?php foreach ($products as $key => $product) { 
                          if($product->variation) {
                              $product_variation = $this->main->get('product_option', ['product' => $product->product_id, 'default' => 1]);
                              if($product->store_type == 'principal') {
                                  $product_variation_branch = $this->main->get('products_principal_stock', ['product_id' => $product->product_id, 'branch_id' => $product->store_id, 'id_option' => $product->id_option]);
                                  if($product_variation_branch) {
                                      if($product_variation_branch->price != 0) {
                                          $product->price = $product_variation_branch->price;
                                          $product->quantity = $product_variation_branch->quantity;
                                      } else {
                                          $product->price = $product_variation->price;
                                          $product->quantity = $product_variation_branch->quantity;
                                      }
                                  } else {
                                      $product->price = $product_variation->price;
                                      $product->quantity = $product->quantity;
                                  }
                              } else {
                                  if($product->price_grosir) {
                                      if($product_variation) {
                                          $product->price = $product->price;
                                          $product->quantity = $product_variation->quantity;
                                      } else {
                                          $product->price = $product->price;
                                          $product->quantity = $product->quantity;
                                      }
                                  } else {
                                      if($product_variation) {
                                          $product->price = $product_variation->price;
                                          $product->quantity = $product_variation->quantity;
                                      } else {
                                          $product->price = $product->price;
                                          $product->quantity = $product->quantity;
                                      }
                                  }
                              }
                          }
                          
                          $sale_on = FALSE;

                          if($product->store_type == 'principal') {
                              if($product->promo_data) {
                                  $promo_data = json_decode($product->promo_data, true);
                                  if(isset($promo_data['status'])) {
                                      if($promo_data['status'] == '1' || $product->promo) {
                                          if($promo_data['type'] == 'P') {
                                              $disc_price = round(($product->price * $promo_data['discount']) / 100);
                                              $label_disc = $promo_data['discount'].'% off';
                                          } else {
                                              $disc_price = $promo_data['discount'];
                                              $label_disc = 'SALE';
                                          }
                                          
                                          $end_price = intval($product->price) - intval($disc_price);
                                          $today = date('Y-m-d');
                                          $today=date('Y-m-d', strtotime($today));
                                          $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                          $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                                          if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                              $sale_on = true;
                                          } else {
                                              $sale_on = false;
                                          }
                                      }
                                  }
                              }
                          } else {
                              if($product->promo) {
                                  $promo_data = json_decode($product->promo_data,true);
                              
                                  if($promo_data['type'] == 'P') {
                                      $disc_price = round(($product->price * $promo_data['discount']) / 100);
                                      $label_disc = $promo_data['discount'].'% off';
                                  } else {
                                      $disc_price = $promo_data['discount'];
                                      $label_disc = 'SALE';
                                  }
                                  
                                  $end_price = intval($product->price) - intval($disc_price);
                                  $today = date('Y-m-d');
                                  $today=date('Y-m-d', strtotime($today));
                                  $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                  $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                                  if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                      $sale_on = true;
                                  } else {
                                      $sale_on = false;
                                  }
                              }
                          }
                        ?>
                          <div class="product-item col-sm-2" style="padding: 0px; border:none;">
                            <div class="product" onmouseover="change_store(<?= $key ?>, 'over')" onmouseout="change_store(<?= $key ?>, 'out')">

                              <div class="row-left">
                                <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="hoverBorder container_item" style="min-height: 180px;">
                                    <img src="<?php echo ($product->image) ? site_url('files/images/' . $product->image) : site_url('assets/frontend/images/noimage.jpg'); ?>" class="img-responsive" alt="<?php echo $product->name; ?>">
                                </a>
                              </div>
                            <div class="garis-tengah"></div>
                            <div class="tombol-cart">
                              <?php if($this->ion_auth->logged_in()) : ?>
                                  <?php if ($product->variation || $product->min_buy > 1) : ?>
                                      <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="icon-cart">
                                          <img src="<?= site_url('assets/frontend/images/cart.png') ?>" style="width: 18px;margin-left:10px;line-height:38px">
                                      </a>
                                  <?php else : ?>
                                      <button data-id="<?php echo encode($product->product_id); ?>" data-storeid="<?= encode($product->store_id) ?>" onclick="addtoCart($(this).data('id'), $(this).data('storeid'))" class="icon-cart">
                                          <img src="<?= site_url('assets/frontend/images/cart.png') ?>" style="width: 18px;">
                                      </button>
                                  <?php endif; ?>
                              <?php else : ?>
                                  <button type="button" class="icon-cart btn-cart-not-login" data-toggle="modal" data-target="#modal-guest" data-back="<?= site_url() ?>">
                                      <img src="<?= site_url('assets/frontend/images/cart.png') ?>" style="width: 18px;">
                                  </button>
                              <?php endif; ?>
                            </div>

                              <div class="row-right animMix">
                                <div class="grid-mode">
                                    <div class="product-title">
                                        <a class="title-5" style="font-weight: bold; width: 75%;transition:.3s;" href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>"><?php echo $product->name; ?></a>
                                    </div>

                                    <div class="viewed-div">
                                        <i class="fa fa-eye" aria-hidden="true"></i> <span><?= $product->viewed; ?></span>
                                    </div>

                                    <div class="product-price">
                                        <span class="price_sale">

                                            <?php if($sale_on) : ?>
                                                <div class="money price-end"><?= rupiah($end_price) ?></div>
                                                <div class="price_sale">
                                                    <span class="money promo-price"><?= rupiah($product->price) ?></span>
                                                    <span class="promo-text"><?= $label_disc ?></span>
                                                </div>
                                            <?php else : ?>
                                                <div class="money price-end"><?= rupiah($product->price) ?></div>
                                            <?php endif; ?>

                                        </span>
                                    </div>

                                    <?php if($product->preorder) : ?>
                                        <div class="pre-order-label">Preorder</div>
                                    <?php endif; ?>
                                    <?php if($product->free_ongkir) : ?>
                                        <div class="free-ongkir-label">Free Ongkir</div>
                                    <?php endif; ?>

                                    <div class="merchant-div">
                                        <?php
                                        $merchant = $this->main->get('merchants', ['id' => $product->store_id]);
                                        $city = $this->main->get('cities', ['id' => $merchant->city]);
                                        ?>
                                        <div class="merchant-div-body">
                                            <span id="text-ellipsis-city-<?= $key ?>" style="display:block;"><?= ($city->type == 'Kabupaten') ? 'Kab. ' . $city->name : '' . $city->name ?></span>
                                            <span id="text-ellipsis-store-<?= $key ?>" style="display: block;"><?= $merchant->name ?></span>
                                        </div>
                                    </div>

                                </div>
                              </div>
                          
                        </div>
                      </div>
                    <?php } ?>
                  </div>
                </div>
              <?php } else { ?>
                <p>Tidak ada produk</p>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</section>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="<?= base_url('assets/frontend/js/modules/home.js') ?>"></script>