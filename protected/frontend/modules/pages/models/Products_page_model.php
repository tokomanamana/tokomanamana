<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products_page_model extends CI_Model {

	function get_product($id, $store_type) {

		if($store_type == 'merchant') {
			$query = "SELECT DISTINCT p.name,
					p.id as product_id,
					(CASE WHEN p.variation = 1 THEN po.price ELSE p.price END) as price,
					p.discount,
					p.promo,
					p.preorder,
					p.promo_data,
					p.viewed,
					p.free_ongkir,
					pi.image,
					m.name as merchant_name,
					p.store_id as store_id,
					p.store_type as store_type,
					(CASE WHEN p.variation = 1 THEN po.quantity ELSE p.quantity END) as quantity,
					p.date_modified as date_modified,
					p.variation,
					p.price_grosir,
					po.id as id_option,
					p.price_reseller,
					(CASE WHEN p.min_buy THEN p.min_buy ELSE 1 END) as min_buy,
					(CASE WHEN p.variation = 1 THEN po.default ELSE 0 END) as product_option_default
					FROM products p
					LEFT JOIN product_image pi ON pi.product = p.id AND pi.primary = 1
					LEFT JOIN merchants m ON m.id = p.store_id
					LEFT JOIN product_option po ON CASE WHEN p.variation = 1 THEN po.product = p.id AND po.default = 1 ELSE 0 END
					WHERE p.id = $id
					AND p.status != 0
					AND p.store_type = 'merchant'
					AND (p.quantity > 0 OR p.preorder = 1 OR p.variation = 1)
					AND m.status = 1";
		} else {
			$query = "SELECT DISTINCT p.name,
						p.id as product_id,
						(CASE WHEN pps.price != 0 THEN pps.price ELSE (CASE WHEN p.variation = 1 THEN (CASE WHEN p.price_grosir = 0 OR p.price_reseller = 0 THEN po.price ELSE p.price END) ELSE p.price END) END) as price,
						p.discount,
						p.promo,
						p.preorder,
						(CASE WHEN pps.promo_data != '' THEN pps.promo_data ELSE p.promo_data END) as promo_data,
						p.viewed,
						p.free_ongkir,
						pi.image,
						m.name as merchant_name,
						pps.branch_id as store_id,
						p.store_type as store_type,
						pps.quantity as quantity,
						p.date_modified as date_modified,
						p.variation,
						p.price_grosir,
						pps.id_option,
						p.price_reseller,
						(CASE WHEN p.min_buy THEN p.min_buy ELSE 1 END) as min_buy,
						(CASE WHEN p.variation = 1 THEN po.default ELSE 0 END) as product_option_default
						FROM products_principal_stock pps
						LEFT JOIN products p ON p.id = pps.product_id
						LEFT JOIN merchants m ON m.id = pps.branch_id
						LEFT JOIN product_image pi ON pi.product = p.id AND pi.primary = 1
						LEFT JOIN principles pr ON pr.id = p.store_id
						LEFT JOIN product_option po ON CASE WHEN p.variation = 1 THEN po.id = pps.id_option AND po.default = 1 ELSE 0 END
						WHERE p.id = $id
						AND p.status != 0
						AND p.store_type = 'principal'
						AND (pps.quantity > 0 OR p.preorder = 1 OR p.variation = 1)
						AND (pr.status = 1 AND m.status = 1)
						GROUP BY pps.product_id, pps.branch_id
						HAVING (CASE WHEN p.variation = 1 THEN product_option_default = 1 ELSE product_option_default = 0 END)";
		}

		return $this->db->query($query);
	} 	

}