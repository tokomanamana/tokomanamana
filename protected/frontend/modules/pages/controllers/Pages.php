<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('products_page_model', 'products_page');
    }

    public function view($id) {
        $data = $this->main->get('pages', array('id' => $id));
        if (!$data)
            // show_404();
            redirect('error_404');

        $this->data['data'] = $data;
        $this->data['options'] = json_decode($this->data['data']->options);
        $this->template->_init();
        $this->output->set_title((($data->meta_title) ? $data->meta_title : $data->title) . ' - ' . settings('meta_title'));
        $this->load->view('page', $this->data);
    }

    public function products($id) {
        $data = $this->main->get('pages', ['id' => $id, 'status' => 1, 'content' => 'products']);
        if(!$data) {
            redirect('error_404');
        }
        $products = $this->main->gets('products', ['status' => 1], 'viewed DESC');
        $view_product = [];
        foreach($products->result() as $product) {
            if($product->pages) {
                $pages = json_decode($product->pages);
                foreach($pages as $page) {
                    if($page == $id) {
                        $get_product = $this->products_page->get_product($product->id, $product->store_type)->result();
                        foreach ($get_product as $prdct) {
                            array_push($view_product, $prdct);
                        }
                    }
                }
            }
        }
        $this->output->set_title((($data->meta_title) ? $data->meta_title : $data->title) . ' - ' . settings('meta_title'));
        $this->data['meta_description'] = 'Halaman ' . $data->title;
        $this->template->_init();
        $this->data['products'] = $view_product;
        $this->data['data'] = $data;
        $this->data['title_page'] = $data->title;
        $this->load->view('page_products', $this->data);
    }

}
