<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model extends CI_Model
{

    function get_from_query($query){
        return $this->db->query($query);
    }


    public function get_id($val=''){
        $y = date('Y');
        $m = date('m');
        if($val == 'peh'){
            $params = 'ORDER/REWARD/';
            $q_peh = $this->get_data('kode', 'point_exchange_history', null, null, '', 'kode desc');
            if($q_peh->num_rows() > 0){
                if($q_peh->row()->kode != ''){
                    $e = explode('/', $q_peh->row()->kode);
                    if($e[2] == $y.$m){
                        $e0 = intval($e[3]) + 1;
                        if($e0 < 10){
                            $kode = $params.$y.$m.'/000'.$e0;
                        }else if($e0 < 100){
                            $kode = $params.$y.$m.'/00'.$e0;
                        }else if($e0 < 1000){
                            $kode = $params.$y.$m.'/0'.$e0;
                        }else{
                            $kode = $params.$y.$m.'/'.$e0;
                        }
                    }else{
                        $kode = $params.$y.$m.'/0001';
                    }
                }else{
                    $kode = $params.$y.$m.'/0001';
                }                
            }else{
                $kode = $params.$y.$m.'/0001';
            }
            return $kode;
        }
    }
    function get_data($select='*', $tabel, $join=null, $where=null, $groupby='', $orderby='', $limit=0, $offset=0, $where1=null, $like=null, $like1=null){
        $database_ = $this->db;
        $select = $select == '' ? '*' : $select;
        $database_->select($select);
        $database_->from($tabel);
        if($join != null){
            for($a=0; $a<count($join); $a++){
                if(count($join[$a]) == 2){
                    $database_->join($join[$a][0], $join[$a][1], 'left');
                }else{
                    
                    $exp = explode('-', $join[$a]);
                    $database_->join($exp[0], $exp[1], 'left');
                }
            }
        }
        if($where != null){
            $database_->where($where);
        }
        if($where1 != null){
            for($a=0; $a<count($where1); $a++){
              if($where1[$a] != ''){
                $database_->where("(".$where1[$a].")");
              }
            }
        }
        if($groupby != ''){
            $database_->group_by($groupby);
        }
        if($orderby != ''){
            $database_->order_by($orderby);
        }
        if($limit != 0){
            $database_->limit($limit, $offset);
        }
        if($like != null){
            $database_->like($like['key'], $like['value']);
        }
        if($like1 != null){
            for ($i=0; $i < count($like1); $i++) { 
                $database_->like($like1[$i]['key'], $like1[$i]['value']);
            }
        }
        return $database_->get();
    }

    function get_data_all($tabel, $order='', $by='', $limit='', $p_count=0, $start='')
    {
        if($p_count == 0){
            $this->db->select('*');
            $this->db->from($tabel);
            if($order!=''){
                $this->db->order_by($order, $by);
            }
            if($limit!=''){
                $this->db->limit($limit, $start);
            }

            $query = $this->db->get();
        }else{
            $this->db->from($tabel);
            $query = $this->db->count_all_results();
        }

        return $query;
    }

    function get_data_where1($select, $tabel, $join='', $where_value='', $group_by='', $order_by=''){
        if($group_by != ''){
            $group_by = 'group by '.$group_by;
        }
        if($order_by != ''){
            $order_by = 'order by '.$order_by;
        }
        return $this->db->query('
            select '.$select.'
            from '.$tabel.' '.$join.' '.$where_value.' '.$group_by.' '.$order_by);
    }

    function get_data_where($tabel, $where=array(), $value=array(), $params=null, $order='', $by='', $p_count=0, $limit='', $start='')
    {
        if($p_count == 0){
            $this->db->select('*');
            $this->db->from($tabel);
            if($params==null){
                for ($i=0; $i < count($where); $i++) {
                    $this->db->where($where[$i], $value[$i]);
                }
            }else{
                for ($i=0; $i < count($where); $i++) {
                    if($params[$i] == 'and'){
                        $this->db->where($where[$i], $value[$i]);
                    }else if($params[$i] == 'or'){
                        $this->db->or_where($where[$i], $value[$i]);
                    }else if($params[$i] == 'nand'){
                        $this->db->where($where[$i].' != ', $value[$i]);
                    }else if($params[$i] == 'nor'){
                        $this->db->or_where($where[$i]. ' != ', $value[$i]);
                    }
                }
            }
            if($order!=''){
                $this->db->order_by($order, $by);
            }
            if($limit!=''){
                $this->db->limit($limit, $start);
            }
            $query = $this->db->get();
        }else{
            $this->db->from($tabel);
            if($params==null){
                for ($i=0; $i < count($where); $i++) {
                    $this->db->where($where[$i], $value[$i]);
                }
            }else{
                for ($i=0; $i < count($where); $i++) {
                    if($params == 'and'){
                        $this->db->where($where[$i], $value[$i]);
                    }else if($params == 'or'){
                        $this->db->or_where($where[$i], $value[$i]);
                    }else if($params == 'nand'){
                        $this->db->where($where[$i].' != ', $value[$i]);
                    }else if($params == 'nor'){
                        $this->db->or_where($where[$i]. ' != ', $value[$i]);
                    }
                }
            }
            $query = $this->db->count_all_results();
        }

        return $query;
    }

    function insert_data($tabel, $data)
    {
        $query = $this->db->insert($tabel, $data);

        if($query)
            return true;
        else
            return false;
    }

    function update_data($tabel, $data, $id)
    {
         $query = $this->db->update($tabel, $data, $id);

         if($query)
            return true;
         else
            return false;
    }

    function delete_data($tabel, $where, $value)
    {
        $this->db->where($where, $value);

        $query = $this->db->delete($tabel);

        if($query)
            return true;
        else
            return false;
    }

    function delete_data1($tabel, $where)
    {
        $this->db->where($where);

        $query = $this->db->delete($tabel);

        if($query)
            return true;
        else
            return false;
    }

    function get_data_like($tabel, $like, $value, $order, $by)
    {
        $this->db->select('*');
        $this->db->from($tabel);
        $this->db->like($like, $value);
        $this->db->order_by($order, $by);
        $query = $this->db->get();
        return $query;
    }

    function get_data_where_like($tabel, $where1, $value1, $where2, $value2, $order, $by)
    {
        $this->db->select('*');
        $this->db->from($tabel);
        $this->db->where($where1, $value1);
        $this->db->like($where2, $value2);
        $this->db->order_by($order, $by);
        $query = $this->db->get();
        return $query;
    }

    function check_kode_unik($total_plus_kode){
        $ko = $this->db->query("SELECT id, unique_code, total FROM orders WHERE total_plus_kode='$total_plus_kode' AND payment_status = 'Pending' ");
        return $ko;
    }


}
