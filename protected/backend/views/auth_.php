<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo "{$title}"; ?></title>

        <?php
        foreach ($css as $file) {
            echo "\n    ";
            echo '<link href="' . $file . '" rel="stylesheet" type="text/css" />';
        } echo "\n";
        ?>
        <script type="text/javascript">
            var site_url = '<?php echo site_url(); ?>';
            var base_url = '<?php echo base_url(); ?>';
            var current_url = '<?php echo current_url(); ?>';
            var decimal_digit = '<?php echo settings('number_of_decimal'); ?>';
            var decimal_separator = '<?php echo settings('number_separator_decimal'); ?>';
            var thousand_separator = '<?php echo settings('number_separator_thousand'); ?>';
        </script>
    </head>

    <body class="login-container">
        <div class="page-container">
            <div class="page-content">
                <div class="content-wrapper">
                    <div class="content pb-20">
                        <form action="<?php echo current_url(); ?>" method="post" id="login">
                            <input type="hidden" value="<?php echo $this->input->get('back'); ?>" name="back">
                            <div class="panel panel-body login-form">
                                <div class="text-center">
                                    <h5 class="content-group-lg">Login Admin Application</h5>
                                </div>
                                <div class="message"></div>
                                <div class="form-group has-feedback has-feedback-left">
                                    <input type="email" name="identity" id="identity" class="form-control" placeholder="Alamat Email" autocomplete="off" >
                                    <div class="form-control-feedback">
                                        <i class="icon-user text-muted"></i>
                                    </div>
                                </div>

                                <div class="form-group has-feedback has-feedback-left">
                                    <input type="password" name="password" id="password" class="form-control" placeholder="Kata Sandi" autocomplete="off" >
                                    <div class="form-control-feedback">
                                        <i class="icon-lock2 text-muted"></i>
                                    </div>
                                </div>
                                <div class="form-group has-feedback has-feedback-left">
                                    <div claas="col-md-12">
                                        <input type="text" name="otp_code" id="otp_code" class="form-control" placeholder="Kode OTP" autocomplete="off" >
                                        <div class="form-control-feedback">
                                            <i class="icon-lock text-muted"></i>
                                        </div>
                                    </div>
                                    <div claas="col-md-12">
                                        <a id="btn-token" onclick="request_otp()" href="javascript:void(0)" class="btn btn-block" style="background-color:green;color:white;"> Kirim OTP </a>
                                    </div>
                                </div>
                                <div class="form-group login-options">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" class="styled" name="remember" value="1">
                                                Ingat saya
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn bg-blue btn-block">Login <i class="icon-arrow-right14 position-right"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php
        foreach ($js as $file) {
            echo "\n    ";
            echo '<script src="' . $file . '"></script>';
        } echo "\n";
        ?>
    </body>
</html>
