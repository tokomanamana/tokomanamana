<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('branch_heading'); ?></h2>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <table class="table" id="table_branch" data-url="<?php echo site_url('merchants/branches/get_list'); ?>">
                <thead>
                    <tr>
                        <th class="default-sort" data-sort="asc"><?php echo lang('branch_name_th'); ?></th>
                        <th><?php echo lang('branch_owner_name_th') ?></th>
                        <th><?php echo lang('branch_status_th') ?></th>
                        <th><?php echo lang('branch_cash_balance_th') ?></th>
                        <th><?php echo lang('branch_stock_th') ?></th>
                        <th><?php echo lang('branch_city_th'); ?></th>
                        <th><?php echo lang('branch_principal_th') ?></th>
                        <th class="no-sort text-center" style="width: 20px;"><?php echo lang('actions_th'); ?></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="stockModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="stockModalLabel">Cek Stok</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="responsive-table">
                <table class="table table-bordered" style="margin-bottom:2em;width:100%;" id="stock_table" >
                    <thead>
                        <tr>
                            <th>Nama Barang</th>
                            <th>Stock</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
      </div>
    </div>
  </div>
</div>