<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('heading'); ?></h2>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <table class="table" id="table" data-url="<?php echo site_url('merchants/withdrawals/get_list'); ?>">
                <thead>
                    <tr>
                        <th class="default-sort" data-sort="desc"><?php echo lang('date'); ?></th>
                        <th><?php echo lang('merchant'); ?></th>
                        <th><?php echo lang('amount'); ?></th>
                        <th class="no-sort"><?php echo lang('bank'); ?></th>
                        <th><?php echo lang('status'); ?></th>
                        <th class="no-sort text-center" style="width: 20px;"></th>
                        <th class="no-sort text-center" style="width: 20px;"><?php echo lang('actions'); ?></th>
                        <th class="no-sort text-center" style="width: 20px;"></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>