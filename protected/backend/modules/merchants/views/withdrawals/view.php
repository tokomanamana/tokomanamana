<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('heading_invoice'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12">
                        <h5>Total Penarikan Dana: <b><?= rupiah($total_withdrawal)?></b></h5>
                        <h6 class="text-semibold">Invoice:</h6>
                        <div class="tabbable nav-tabs-vertical nav-tabs-left">
                            <ul class="nav nav-tabs nav-tabs-highlight">
                                <?php if ($invoices) foreach ($invoices as $key => $invoice) { ?>
                                    <?php $invoice_detail = $this->withdrawals->get_withdrawal_invoice($invoice);?>
                                    <li class="<?php echo ($key == 0) ? 'active' : ''; ?>">
                                        <a href="<?php echo '#inv-' . $invoice_detail->id; ?>" data-toggle="tab"><?php echo $invoice_detail->code; ?> 
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                            <div class="tab-content">
                                <?php if ($invoices) foreach ($invoices as $key => $invoice) { ?>
                                    <?php $invoice_detail = $this->withdrawals->get_withdrawal_invoice($invoice); ?>
                                    
                                        <div class="tab-pane <?php echo ($key == 0) ? 'active' : ''; ?>" id="<?php echo 'inv-' . $invoice_detail->id; ?>">
                                            <h6 class="text-semibold">Merchant: <?php echo $invoice_detail->merchant_name; ?></h6>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th colspan="2" class="text-center">Detail</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><?php echo lang('order_invoice_code'); ?></td>
                                                                <td><?php echo $invoice_detail->code; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><?php echo lang('order_invoice_status'); ?></td>
                                                                <td><?php echo '<label class="label no-margin label-' . $invoice_detail->order_status_color . '">' . $invoice_detail->order_status_name . '</label>'; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><?php echo lang('order_invoice_subtotal'); ?></td>
                                                                <td style="font-size: 15px; font-weight: bold;"><?php echo rupiah($invoice_detail->subtotal); ?></td>
                                                            </tr>
                                                            <!-- <tr>
                                                                <td><?php // echo lang('order_invoice_total'); ?></td>
                                                                <td style="font-size: 15px; font-weight: bold;"><?php // echo ($discount > 0) ? rupiah($invoice->total - $discount) : rupiah($invoice->total); ?></td>
                                                            </tr> -->
                                                            
                                                            <tr>
                                                                <td><?= lang('order_invoice_notes') ?></td>
                                                                <td style="font-size: 15px;"><?= isset($invoice_detail->notes) ? $invoice_detail->notes : ''; ?></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-sm-6">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th colspan="2" class="text-center"><?php echo lang('order_shipping_tabs'); ?></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><?php echo lang('order_invoice_shipping_courier'); ?></td>
                                                                <td><?php
                                                                    if ($invoice_detail->shipping_courier == 'kurir-express') {
                                                                        $invoice_detail->shipping_courier = 'tokomanamana-express';
                                                                    }
                                                                    $shipping = explode('-', $invoice_detail->shipping_courier);
                                                                    echo strtoupper($shipping[0]) . (isset($shipping[1]) ? ' ' . $shipping[1] : '');
                                                                    ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><?php echo lang('order_invoice_shipping_weight'); ?></td>
                                                                <td><?php echo number($invoice_detail->shipping_weight) . ' gram'; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><?php echo lang('order_invoice_shipping_cost'); ?></td>
                                                                <td><?php echo rupiah($invoice_detail->shipping_cost); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><?php echo lang('order_invoice_shipping_tracking'); ?></td>
                                                                <td><?php echo $invoice_detail->tracking_number; ?></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <table class="table table-bordered mt-15">
                                                <thead>
                                                    <tr>
                                                        <th><?php echo lang('order_product_code'); ?></th>
                                                        <th><?php echo lang('order_product_name'); ?></th>
                                                        <th><?php echo lang('order_product_option') ?></th>
                                                        <th><?php echo lang('order_product_weight'); ?></th>
                                                        <th><?php echo lang('order_product_quantity'); ?></th>
                                                        <th><?php echo lang('order_product_price'); ?></th>
                                                        <th><?php echo lang('order_product_subtotal'); ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if ($products = $this->main->gets('order_product', array('invoice' => $invoice_detail->id))) foreach ($products->result() as $product) { ?>
                                                        <?php if($product->options) : ?>
                                                            <?php $product_options = json_decode($product->options); ?>
                                                            <?php if(count($product_options) > 1 || !isset($product_options[0]->option_name)) : ?>
                                                                <?php foreach($product_options as $product_option) : ?>
                                                                    <tr>
                                                                        <td><?php echo $product->code; ?></td>
                                                                        <td>
                                                                            <?php echo $product->name; ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php $options = json_decode($product_option->data_option); ?>
                                                                            <?php foreach($options as $option) : ?>
                                                                                <?php echo $option->type . ' ' . $option->value . ' ' ?>
                                                                            <?php endforeach; ?>
                                                                        </td>
                                                                        <td><?php echo number($product_option->total_weight) . ' gram'; ?></td>
                                                                        <td><?php echo number($product_option->quantity); ?></td>
                                                                        <td><?php echo rupiah($product_option->price); ?></td>
                                                                        <td><?php echo rupiah($product_option->quantity * $product_option->price); ?></td>
                                                                    </tr>
                                                                <?php endforeach; ?>
                                                            <?php else : ?>
                                                                <tr>
                                                                    <td><?php echo $product->code; ?></td>
                                                                    <td><?php echo $product->name; ?></td>
                                                                    <td>
                                                                        <?php foreach($product_options as $option) : ?>
                                                                            <?php echo $option->option_name ?>
                                                                        <?php endforeach; ?>
                                                                    </td>
                                                                    <td><?php echo number($product->weight) . ' gram'; ?></td>
                                                                    <td><?php echo number($product->quantity); ?></td>
                                                                    <td><?php echo rupiah($product->price); ?></td>
                                                                    <td><?php echo rupiah($product->total); ?></td>
                                                                </tr>
                                                            <?php endif; ?>
                                                        <?php else : ?>
                                                            <tr>
                                                                <td><?php echo $product->code; ?></td>
                                                                <td><?php echo $product->name; ?></td>
                                                                <td>-</td>
                                                                <td><?php echo number($product->weight) . ' gram'; ?></td>
                                                                <td><?php echo number($product->quantity); ?></td>
                                                                <td><?php echo rupiah($product->price); ?></td>
                                                                <td><?php echo rupiah($product->total); ?></td>
                                                            </tr>
                                                        <?php endif; ?>
                                                        <?php } ?>
                                                </tbody>
                                            </table>
                                            <table class="table table-bordered mt-15">
                                                <thead>
                                                    <tr>
                                                        <th><?php echo lang('order_history_time'); ?></th>
                                                        <th><?php echo lang('order_history_status'); ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if ($histories = $this->withdrawals->invoice_histories($invoice_detail->id)) foreach ($histories->result() as $history) { ?>
                                                            <tr>
                                                                <td><?php echo get_date_time($history->date_added); ?></td>
                                                                <td><?php echo $history->order_status_name; ?></td>
                                                            </tr>
                                                        <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
