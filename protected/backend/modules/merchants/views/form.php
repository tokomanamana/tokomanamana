<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo ($data) ? lang('edit_heading') : lang('add_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('merchants'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('merchants/save'); ?>" method="post" class="form-horizontal" id="form">
                    <input type="hidden" name="id" value="<?php echo set_value('id', ($data) ? encode($data->id) : ''); ?>">
                    <input type="hidden" name="lat" id="lat" value="<?php echo ($data) ? $data->lat : ''; ?>">
                    <input type="hidden" name="lng" id="lng" value="<?php echo ($data) ? $data->lng : ''; ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#data" data-toggle="tab"><?php echo lang('data_tabs'); ?></a></li>
                                    <li class=""><a href="#address" data-toggle="tab"><?php echo lang('address_tabs'); ?></a></li>
                                    <li class=""><a href="#owner" data-toggle="tab"><?php echo lang('owner_tabs'); ?></a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="data">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="name"><?php echo lang('name'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" required="" id="name" name="name" placeholder="<?php echo lang('name_placeholder'); ?>" value="<?php echo ($data) ? $data->name : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('description'); ?></label>
                                            <div class="col-md-9">
                                                <textarea cols="30" rows="2" class="form-control" id="description" name="description" placeholder="<?php echo lang('description_placeholder'); ?>"><?php echo ($data) ? $data->description : ''; ?></textarea>
                                            </div>
                                        </div>
                                       <!--  <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('group'); ?></label>
                                            <div class="col-md-4">
                                                <select class="bootstrap-select"  name="group" required="" data-live-search="true" data-width="100%">
                                                    <option value=""></option>
                                                    <?php if ($groups) { ?>
                                                        <?php foreach ($groups->result() as $group) { ?>
                                                            <option value="<?php echo $group->id; ?>" <?php echo ($data) ? ($data->group == $group->id) ? 'selected' : '' : ''; ?>><?php echo $group->name; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div> -->
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="telephone"><?php echo lang('telephone'); ?></label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="telephone" value="<?php echo ($data) ? $data->telephone : ''; ?>">
                                            </div>
                                        </div>
                                        <?php
                                        if ($data) {
                                        ?>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="telephone">Signature</label>
                                            <div class="col-md-4">
                                                <img width="200" src="data:image/jpeg;base64,<?php echo ($data) ? $data->signature_img : ''; ?>">
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('status'); ?></label>
                                            <div class="col-md-4">
                                                <input type="checkbox" name="status" value="1" data-on-text="<?php echo lang('enabled'); ?>" data-off-text="<?php echo lang('disabled'); ?>" class="switch" <?php echo ($data) ? (($data->status == 1) ? 'checked' : '') : 'checked'; ?>>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane " id="address">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('address'); ?></label>
                                            <div class="col-md-9">
                                                <textarea cols="30" rows="2" class="form-control" id="address" name="address"><?php echo ($data) ? $data->address : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="province"><?php echo lang('province'); ?></label>
                                            <div class="col-md-4">
                                                <select class="bootstrap-select"  name="province" id="province" data-live-search="true" data-width="100%">
                                                    <option value=""></option>
                                                    <?php if ($provinces) { ?>
                                                        <?php foreach ($provinces->result() as $province) { ?>
                                                            <option value="<?php echo $province->id; ?>" <?php echo ($data) ? ($data->province == $province->id) ? 'selected' : '' : ''; ?>><?php echo $province->name; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="city"><?php echo lang('city'); ?></label>
                                            <div class="col-md-4">
                                                <select class="bootstrap-select"  name="city" id="city" data-live-search="true" data-width="100%">
                                                    <option value=""></option>
                                                    <?php if ($cities) { ?>
                                                        <?php foreach ($cities->result() as $city) { ?>
                                                            <option value="<?php echo $city->id; ?>" <?php echo ($data) ? ($data->city == $city->id) ? 'selected' : '' : ''; ?>><?php echo $city->name; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="district"><?php echo lang('district'); ?></label>
                                            <div class="col-md-4">
                                                <select class="bootstrap-select"  name="district" id="district" data-live-search="true" data-width="100%">
                                                    <option value=""></option>
                                                    <?php if ($districts) { ?>
                                                        <?php foreach ($districts->result() as $district) { ?>
                                                            <option value="<?php echo $district->id; ?>" <?php echo ($data) ? ($data->district == $district->id) ? 'selected' : '' : ''; ?>><?php echo $district->name; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('maps'); ?></label>
                                            <div class="col-md-9">
                                                <script type="text/javascript">
                                                    var centreGot = false;
                                                </script>
                                                <?php echo $map['js']; ?>
                                                <input class="form-control" id="search-location">
                                                <?php echo $map['html']; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane " id="owner">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="fullname"><?php echo lang('owner_name'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" required="" name="fullname" value="<?php echo ($data) ? $data->fullname : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="email"><?php echo lang('email'); ?></label>
                                            <div class="col-md-9">
                                                <input type="email" class="form-control" <?php echo (!$data) ? 'required' : 'disabled' ?> name="email" value="<?php echo ($data) ? $data->email : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Phone</label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="phone" value="<?php echo ($data) ? $data->phone: ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('setting_profile_bank_account_name_label'); ?></label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="bank_account_name" value="<?php echo ($data) ? $data->bank_account_name: ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('setting_profile_bank_account_number_label'); ?></label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="bank_account_number" value="<?php echo ($data) ? $data->bank_account_number : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('setting_profile_bank_name_label'); ?></label>
                                            <div class="col-md-4">
                                               <select class="bootstrap-select" required="" name="bank_name" data-live-search="true" id="bank_name" data-width="100%">
                                                    <option></option>
                                                    <?php if ($bank_lists) {  ?>
                                                        <?php foreach ($bank_lists->result() as $bank_list) {
                                                            if($data->bank_name == 'BCA') $data->bank_name = 'BANK BCA'; ?>
                                                            <option value="<?php echo $bank_list->name;?>" <?php echo ($data) ? ($data->bank_name == $bank_list->name) ? 'selected' : '' : ''; ?>><?= $bank_list->name;?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('setting_profile_bank_branch_label'); ?></label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="bank_branch" value="<?php echo ($data) ? $data->bank_branch : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo 'Username' ?></label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" <?php echo (!$data) ? 'required' : 'disabled' ?> name="username" value="<?php echo ($data) ? $data->username : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="password"><?php echo lang('password'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" <?php echo (!$data) ? 'required' : '' ?> name="password">
                                                <?php if ($data) { ?>
                                                    <span class="help-block"><?php echo lang('password_update_help'); ?></span>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('merchants'); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <button type="button" id="buttonSubmit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>