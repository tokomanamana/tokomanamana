<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['group_heading'] = 'Kelompok Merchant';
$lang['group_sub_heading'] = 'Kelola Data Kelompok Merchant';
$lang['group_list_heading'] = 'Daftar Kelompok Merchant';
$lang['group_add_heading'] = 'Tambah Kelompok Merchant';
$lang['group_edit_heading'] = 'Edit Kelompok Merchant';
$lang['merchant_list_heading'] = 'Daftar Merchant';

$lang['group_image_th'] = 'Gambar';
$lang['group_name_th'] = 'Nama Kelompok Merchant';
$lang['group_branch_th'] = 'Cabang';

$lang['group_form_name_label'] = 'Nama';
$lang['group_form_branch_label'] = 'Cabang';

$lang['group_save_success_message'] = "Kelompok Merchant '%s' berhasil disimpan.";
$lang['group_save_error_message'] = "Kelompok Merchant '%s' gagal disimpan.";
$lang['group_delete_success_message'] = "Kelompok Merchant '%s' berhasil dihapus.";
$lang['group_delete_error_message'] = "Kelompok Merchant '%s' gagal dihapus.";