<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['heading'] = 'Penarikan Dana';

$lang['date'] = 'Tanggal';
$lang['merchant'] = 'Merchant';
$lang['amount'] = 'Nominal';
$lang['bank'] = 'Bank';
$lang['status'] = 'Status';

$lang['heading_invoice'] = 'Invoice Penarikan Dana';
$lang['order_detail_tabs'] = 'Detail';

$lang['order_invoice_code'] = 'Kode Invoice';
$lang['order_invoice_subtotal'] = 'Total Belanja';
$lang['order_invoice_total'] = 'Total Pembayaran';
$lang['order_invoice_notes'] = 'Catatan';
$lang['order_invoice_status'] = 'Status';
$lang['order_invoice_shipping_courier'] = 'Pengiriman';
$lang['order_invoice_shipping_cost'] = 'Ongkos Kirim';
$lang['order_invoice_shipping_weight'] = 'Berat Pengiriman';
$lang['order_invoice_shipping_tracking'] = 'Resi Pengiriman';

$lang['order_invoice_tabs'] = 'Invoice';
$lang['order_shipping_tabs'] = 'Pengiriman';
$lang['order_product_name'] = 'Nama Produk';
$lang['order_product_code'] = 'SKU';
$lang['order_product_quantity'] = 'Jumlah';
$lang['order_product_price'] = 'Harga';
$lang['order_product_subtotal'] = 'Total harga';
$lang['order_product_weight'] = 'Berat';
$lang['order_product_option'] = 'Variasi';

$lang['order_history_time'] = 'Waktu';
$lang['order_history_status'] = 'Status';

$lang['save_success'] = "Tarik Dana merchant '%s' berhasil.";