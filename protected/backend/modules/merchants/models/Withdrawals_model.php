<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Withdrawals_model extends CI_Model {

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->filter($search);
        if ($order) {
            $columns = array(0 => 'bw.date_added', 1 => 'm.name', 2 => 'bw.amount', 4 => 'bw.status');
            $order['column'] = $columns[$order['column']];
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('bw.*, m.name merchant_name')
                ->join('merchants m', 'bw.merchant = m.id', 'left')
                ->limit($length, $start);

        return $this->db->get('merchant_balance_withdrawal bw');
    }

    function get_withdrawal_invoice($id) {
        $this->db->select('oi.*, sos.name order_status_name, sos.color order_status_color, m.name merchant_name')
                ->join('setting_order_status sos', 'oi.order_status = sos.id', 'left')
                ->join('merchants m', 'oi.merchant = m.id', 'left')
                ->where('oi.id', $id);
        return $this->db->get('order_invoice oi')->row();
    }

    function invoice_histories($invoice) {
        $this->db->select('oh.*, sos.name order_status_name, sos.color order_status_color')
                ->join('setting_order_status sos', 'oh.order_status = sos.id', 'left')
                ->where('oh.invoice', $invoice)
                ->order_by('oh.id', 'desc');
        return $this->db->get('order_history oh');
    }

    function count_all($search = '') {
        $this->filter($search);
        $this->db->join('merchants m', 'bw.merchant = m.id', 'left');
        return $this->db->count_all_results('merchant_balance_withdrawal bw');
    }

    function filter($search = '') {
        $columns = array('bw.date_added', 'm.name', 'bw.amount', 'bw.status');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
            $this->db->group_end();
        }
    }

}
