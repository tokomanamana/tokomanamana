<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Merchants_model extends CI_Model {

    function get($id) {
        $this->db->select('m.*, au.fullname,au.username, au.email, au.signature_img,au.phone')
                ->join('merchant_users au', 'm.auth = au.id', 'left')
             
                ->where('m.id', $id);
        $query = $this->db->get('merchants m');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->where_like($search);
        if ($order) {
            $order['column'] = $this->get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('m.*, au.fullname owner_name,au.username, au.email, c.name city')
                ->join('merchant_users au', 'm.auth = au.id', 'left')
                // ->join('merchant_groups mg', 'mg.id = m.group', 'left')
                ->join('cities c', 'c.id = m.city', 'left')
                // ->where("(m.type = 'merchant') OR (m.type = 'principle_branch')")
                ->where('m.type', 'merchant')
                ->limit($length, $start); 
                $this->db->select('(SELECT SUM(quantity) FROM products p WHERE p.store_id = m.id) ttl_stock');

            return $this->db->get('merchants m');
        }

        function get_alias_key($key) {
            switch ($key) {
                case 0: $key = 'm.name';
                    break;
            case 1: $key = 'au.fullname';
                break;
            case 2: $key = 'm.type';
                break;
            case 3: $key = 'm.status';
                break;
            case 4: $key = 'cash_balance';
                break;
            case 5: $key = 'ttl_stock';
                break;
            case 6: $key = 'm.date_added';
                break;
        }
        return $key;
    }

    function stock_get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'p.name';
                break;
            case 1: $key = 'm.quantity';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->where_like($search);
        $this->db->join('merchant_users au', 'm.auth = au.id', 'left')
                // ->join('merchant_groups mg', 'mg.id = m.group', 'left')
                ->where("(m.type='merchant' OR m.type='principal_branch')");
        return $this->db->count_all_results('merchants m');
    }

    function count_all_stock($search = '',$merchant = '') {
        $this->stock_where_like($search);
        $this->db->select('p.name,p.quantity')

        ->where('p.store_id',$merchant)
        ->where('p.quantity >',0);
        return $this->db->count_all_results('products p');
    }

    function get_stock_all($start = 0, $length, $search = '', $order = array(), $merchant = '') {
        $this->stock_where_like($search);
        if ($order) {
            $order['column'] = $this->stock_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('p.name,p.quantity')
        // ->join('products p', 'p.id = m.product', 'left')
        ->where('p.store_id',$merchant)
        ->where('p.quantity >',0)
        ->limit($length, $start);
        return $this->db->get('products p');
    }

    function where_like($search = '') {
        $columns = array('m.name', 'au.fullname', 'm.type');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
            $this->db->group_end();
        }
    }

    function stock_where_like($search = '') {
        $columns = array('p.name');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
            $this->db->group_end();
        }
    }

    function get_admins() {
        $this->ion_auth_model->_ion_select = array('auth_users.id, fullname');
        $this->ion_auth_model->_ion_where = array('auth_users.id NOT IN (SELECT auth FROM merchants)');
        return $this->ion_auth->users(array(3));
    }

}
