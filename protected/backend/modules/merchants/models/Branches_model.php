<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Branches_model extends CI_Model {

    function get($id) {
        $this->db->select('m.*, au.fullname, au.email,au.phone')
                ->join('auth_users au', 'm.auth = au.id', 'left')
                ->where('m.is_branch', 1)
                ->where('m.id', $id);
        $query = $this->db->get('merchants m');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->where_like($search);
        if ($order) {
            $order['column'] = $this->get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('m.*, c.name city, mu.fullname, p.name as principal_name')
                ->where('m.type', 'principle_branch')
                ->join('cities c', 'c.id = m.city', 'left')
                ->join('merchant_users mu', 'mu.id = m.auth', 'left')
                ->join('principles p', 'p.id = m.principal_id', 'left')
                ->limit($length, $start);
        $this->db->select('(SELECT SUM(quantity) FROM products_principal_stock pps WHERE pps.branch_id = m.id) ttl_stock');

        return $this->db->get('merchants m');
    }

    function get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'm.name';
                break;
            case 1: $key = 'mu.fullname';
                break;
            case 2: $key = 'm.status';
                break;
            case 3: $key = 'm.cash_balance';
                break;
            case 4: $key = 'ttl_stock';
                break;
            case 5: $key = 'c.name';
                break;
            case 6: $key = 'principal_name';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->where_like($search);
        $this->db->join('cities c', 'c.id = m.city', 'left')
                ->join('merchant_users mu', 'mu.id = m.auth', 'left')
                ->join('principles p', 'p.id = m.principal_id', 'left')
                ->where('m.type', 'principle_branch');
        return $this->db->count_all_results('merchants m');
    }

    function where_like($search = '') {
        $columns = array('m.name', 'c.name', 'mu.fullname, p.name');
        if ($search) {
            foreach ($columns as $column) {
                $this->db->like('IFNULL(' . $column . ',"")', $search);
            }
        }
    }

    function get_admins() {
        $this->ion_auth_model->_ion_select = array('auth_users.id, fullname');
        $this->ion_auth_model->_ion_where = array('auth_users.id NOT IN (SELECT auth FROM merchants)');
        return $this->ion_auth->users(array(3));
    }

    function get_stock_all($start = 0, $length, $search = '', $order = array(), $branch = '') {
        $this->stock_where_like($search);
        if ($order) {
            $order['column'] = $this->stock_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('p.name, (CASE WHEN pps.id_option != "" THEN SUM(pps.quantity) ELSE pps.quantity END) as quantity')
        ->join('products_principal_stock pps', 'pps.product_id = p.id')
        ->where('pps.branch_id',$branch)
        ->where('pps.quantity >',0)
        ->limit($length, $start)
        ->group_by('pps.product_id');
        return $this->db->get('products p');
    }

    function stock_where_like($search = '') {
        $columns = array('p.name');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
            $this->db->group_end();
        }
    }

    function stock_get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'p.name';
                break;
            case 1: $key = 'pps.quantity';
                break;
        }
        return $key;
    }

    function count_all_stock($search = '',$merchant = '') {
        $this->stock_where_like($search);
        $this->db->select('p.name,pps.quantity')
        ->join('products_principal_stock pps', 'pps.product_id = p.id')

        ->where('pps.branch_id',$merchant)
        ->where('pps.quantity >',0);
        return $this->db->count_all_results('products p');
    }

}
