<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Merchants extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->aauth->control('merchant');
        $this->lang->load('merchants', settings('language'));
        $this->load->model('merchants_model', 'merchants');

        $this->data['menu'] = 'merchant_list';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('merchant'), '/');
        $this->breadcrumbs->push(lang('merchant'), '/merchants');
        $status = isset($this->input->get()['status']) ? $this->input->get()['status'] : NULL;
        if($status != NULL) {
            $this->data['url_ajax'] = site_url('merchants/get_list?status='.$status);
        } else {
            $this->data['url_ajax'] = site_url('merchants/get_list');
        }
        $this->load->js('../assets/backend/js/merchant/list.js');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->output->set_title(lang('heading'));
        $this->load->view('list', $this->data);
    }

    public function form($id = '') {
        $this->load->library('googlemaps');

        $this->data['provinces'] = $this->main->gets('provincies', array(), 'name asc');
        $this->data['bank_lists'] = $this->main->gets('rekening_bank',[],'id asc');
        // $this->data['groups'] = $this->main->gets('merchant_groups', array(), 'name asc');
        $this->data['cities'] = array();
        $this->data['districts'] = array();
        $this->data['data'] = array();
        $config['center'] = 'auto';
        if ($id) {
            $this->aauth->control('merchant/edit');
            $id = decode($id);
            $this->data['data'] = $this->merchants->get($id);
            if ($this->data['data']->province)
                $this->data['cities'] = $this->main->gets('cities', array('province' => $this->data['data']->province), 'name asc');
            if ($this->data['data']->city)
                $this->data['districts'] = $this->main->gets('districts', array('city' => $this->data['data']->city), 'name asc');
            if ($this->data['data']->lat && $this->data['data']->lng) {
                $config['center'] = $this->data['data']->lat . ',' . $this->data['data']->lng;
            }
        } else {
            $this->aauth->control('merchant/add');
        }

        $config['loadAsynchronously'] = TRUE;
        $config['map_height'] = '500px';
        $config['onboundschanged'] = 'if (!centreGot) {
                var mapCentre = map.getCenter();
                marker_0.setOptions({
                        position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng()) 
                });
                setLocation(mapCentre.lat(), mapCentre.lng());
            }centreGot = true;';
        $config['disableFullscreenControl'] = TRUE;
        $config['disableMapTypeControl'] = TRUE;
        $config['disableStreetViewControl'] = TRUE;
        $config['places'] = TRUE;
        $config['placesAutocompleteInputID'] = 'search-location';
        $config['placesAutocompleteBoundsMap'] = TRUE; // set results biased towards the maps viewport
        $config['placesAutocompleteOnChange'] = 'map.setCenter(this.getPlace().geometry.location); 
            marker_0.setOptions({
                        position: new google.maps.LatLng(this.getPlace().geometry.location.lat(), this.getPlace().geometry.location.lng()) 
                });
                setLocation(this.getPlace().geometry.location.lat(), this.getPlace().geometry.location.lng());';
        $this->googlemaps->initialize($config);
        $marker = array();
        $marker['draggable'] = true;
        $marker['ondragend'] = 'setLocation(event.latLng.lat(), event.latLng.lng());';
        $this->googlemaps->add_marker($marker);
        $this->data['map'] = $this->googlemaps->create_map();

        $this->breadcrumbs->push(lang('merchant'), '/merchants');
        $this->breadcrumbs->push(($this->data['data']) ? lang('edit_heading') : lang('add_heading'), '/');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->template->_init();
        $this->template->form();
        $this->load->js('../assets/backend/js/modules/merchants/form.js');
        $this->output->set_title(($this->data['data']) ? lang('edit_heading') : lang('add_heading'));
        $this->load->view('form', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $status = isset($this->input->get()['status']) ? $this->input->get()['status'] : NULL;
        if($status != NULL){
            $this->db->where('m.status',$status);
        }
        $datas = $this->merchants->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->name,
                    $data->owner_name,
                    $data->type,
                    lang('status_' . $data->status),
                    rupiah($data->cash_balance),
                    isset($data->ttl_stock) && $data->ttl_stock > 0 ? '<a href="javascript:void(0)" class="see_stock" onclick="seeStock('.$data->id.')">
                    '.$data->ttl_stock.'
                    </a>' : 0,
                    $data->date_added,
                    '<td class="text-center">
                    <ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">' .
                    // ($data->status == 0 && $this->aauth->is_allowed('merchant/activate') ? '<li><a href="' . site_url('merchants/activate/' . encode($data->id)) . '" class="activate">' . lang('button_activate') . '</a></li>' : '') .
                    ($data->status == 0 || $data->status == 1 && $this->aauth->is_allowed('merchant/edit') ? '<li><a href="' . site_url('merchants/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>' : '') .
                    ($this->aauth->is_allowed('merchant/delete') ? '<li><a href="' . site_url('merchants/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>' : '') .
                    '</ul>
                    </li>
                    </ul>
                    </td>',

                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->merchants->count_all();
        $output['recordsFiltered'] = $this->merchants->count_all($search);
        echo json_encode($output);
    }

    public function get_stock_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $merchant_id = $this->input->post('id');
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        // $status = isset($this->input->get()['status']) ? $this->input->get()['status'] : NULL;
        // if($status != NULL){
        //     $this->db->where('m.status',$status);
        // }
        $datas = $this->merchants->get_stock_all($start, $length, $search, $order,$merchant_id);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->name,
                    $data->quantity,
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->merchants->count_all_stock('',$merchant_id);
        $output['recordsFiltered'] = $this->merchants->count_all_stock($search,$merchant_id);
        echo json_encode($output);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'lang:name', 'trim|required');
        $this->form_validation->set_rules('province', 'lang:province', 'trim|required');
        $this->form_validation->set_rules('city', 'lang:city', 'trim|required');
        $this->form_validation->set_rules('district', 'lang:district', 'trim|required');
        $this->form_validation->set_rules('fullname', 'lang:owner_name', 'trim|required');
        $this->form_validation->set_rules('phone', 'lang:phone', 'trim|required|callback_check_phone_registered');

        // $this->form_validation->set_rules('group', 'lang:group', 'trim|required');
        if (!$this->input->post('id')) {
//       
            $this->form_validation->set_rules('email', 'lang:email', 'trim|required|valid_email');
            $this->form_validation->set_rules('username', 'lang:username', 'trim|required|is_unique[merchant_users.username]');
            $this->form_validation->set_rules('password', 'lang:password', 'trim|required');
            
        }

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);
            do {
                $this->load->library('ion_auth');
                $data['status'] = (isset($data['status']) && $data['status'] == 1) ? $data['status'] : 0;

                if (!$data['id']) {
                    $token = bin2hex(random_bytes(32));
                    $user = $this->ion_auth->register($data['email'],$data['password'],$data['email'],$data['phone'],array('username'=> $data['username'],'fullname' => $data['fullname'], 'telephone' => $data['telephone'],'token_confirm' => $token,'token_confirm_status' => 0), array(3));
                    if ($user) {
                        unset($data['email']);
                        unset($data['password']);
                        // unset($data['username']);
                        unset($data['fullname']);
                        unset($data['phone']);
                        $data['auth'] = $user;
                        $data['shipping'] = json_encode(array('jne', 'tiki', 'pos'));
                        if($data['bank_name'] == 'BANK BCA'){
                            $data['bank_name'] = 'BCA';
                        }
                        $save = $this->main->insert('merchants', $data);
                        $insert_id = $this->db->insert_id();
                        // log_message('debug',$insert_id);

                        if($data['status']==1){
                            $data2 = $this->main->get('merchants', array('id' => $insert_id));
                            $user = $this->main->get('merchant_users', array('id' => $data2->auth));
                            $this->data['name'] = $user->fullname;
                            $message = $this->load->view('email/merchant/activate', $this->data, TRUE);
                            $cronjob = array(
                                'from' => settings('send_email_from'),
                                'from_name' => settings('store_name'),
                                'to' => $user->email,
                                'subject' => 'Akun penjual ' . settings('store_name').' Anda telah aktif',
                                'message' => $message
                            );
                            $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));

                            $dir= FCPATH.'../files/images/merchant/'.$data['username'];
                            if (!is_dir($dir)) {
                                mkdir($dir,0777,TRUE);
                            }

                            $dir_thumb= FCPATH.'../files/image_thumbs/merchant/'.$data['username'];
                            if (!is_dir($dir_thumb)) {
                                mkdir($dir_thumb,0777,TRUE);
                            }

                            $this->load->library('sprint');
                            $sms = json_decode(settings('sprint_sms'), true);
                            $code = rand(1000, 9999);
                            $sms['m'] = 'Akun penjual Anda telah aktif. Silahkan download Tokomanamana Seller di Playstore / login di http://tokomanamana.com/ma/';
                            $sms['d'] = $user->phone;
                            $url = $sms['url'];
                            unset($sms['url']);
                            $sprint_response = $this->sprint->sms($url, $sms);                       
                        }
//                        $this->main->insert('seo_url', array('keyword' => $data['username'], 'query' => 'merchants/view/' . $this->db->insert_id()));
                    } else {
                        $return = array('message' => $this->ion_auth->errors(), 'status' => 'error');
                        break;
                    }
                } else {

                    $data['id'] = decode($data['id']);
                    if($data['bank_name'] == 'BANK BCA'){
                            $data['bank_name'] = 'BCA';
                    }
                    $merchant = $this->main->get('merchants', array('id' => $data['id']));
                    $auth = $merchant->auth;
                    $data_auth = array('fullname' => $data['fullname'],'phone' => $data['phone']);
                    if (isset($data['password'])) {
                        $data_auth['password'] = $data['password'];
                        unset($data['password']);
                    }
                    $this->ion_auth->update($auth, $data_auth);
                    // unset($data['username']);
                    unset($data['fullname']);
                    unset($data['phone']);
                    $save = $this->main->update('merchants', $data, array('id' => $data['id']));

                    if($data['status']==1){
                        $user = $this->main->get('merchant_users', array('id' => $auth));
                        $message = $this->load->view('email/merchant/activate', $this->data, TRUE);
                        $cronjob = array(
                            'from' => settings('send_email_from'),
                            'from_name' => settings('store_name'),
                            'to' => $user->email,
                            'subject' => 'Akun penjual ' . settings('store_name').' Anda telah aktif',
                            'message' => $message
                        );
                       

                        $dir= FCPATH.'../files/images/merchant/'.$merchant->username;
                        if (!is_dir($dir)) {
                            mkdir($dir,0777,TRUE);
                            $dir_thumb= FCPATH.'../files/image_thumbs/merchant/'.$merchant->username;
                            if (!is_dir($dir_thumb)) {
                                mkdir($dir_thumb,0777,TRUE);
                            }
                            $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
                            $this->load->library('sprint');
                            $sms = json_decode(settings('sprint_sms'), true);
                            $code = rand(1000, 9999);
                            $sms['m'] = 'Akun penjual Anda telah aktif. Silahkan download Tokomanamana Seller di Playstore / login di http://tokomanamana.com/ma/';
                            $sms['d'] = $user->phone;
                            $url = $sms['url'];
                            unset($sms['url']);
                            $sprint_response = $this->sprint->sms($url, $sms);    
                        }

                                             
                    }
                }
                $return = array('message' => sprintf(lang('save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('merchants'));
            } while (0);
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function check_phone_registered($str){
        $phone_check = $this->main->get('merchant_users',array('phone' => $str));

        if($this->input->post('id')){
            $id_merchant = decode($this->input->post('id'));
            $merchant = $this->main->get('merchants', array('id' => $id_merchant));
            $merchant_user = $this->main->get('merchant_users',array('id' => $merchant->auth));

            if($str == $merchant_user->phone){
                return TRUE;
            }
            else if(!$phone_check){
                return TRUE;
            }
            else{
                $this->form_validation->set_message('check_phone_registered', 'Nomor HP sudah terdaftar!');
                return FALSE;
                
            }

        }
        else{
            if(!$phone_check){
                return TRUE;
            }
            else{
                 $this->form_validation->set_message('check_phone_registered', 'Nomor HP sudah terdaftar!');
                return FALSE;
               
            }
        }
        

    }

    public function activate($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $data = $this->main->get('merchants', array('id' => $id));
        $user = $this->main->get('merchant_users', array('id' => $data->auth));
        $this->data['name'] = $user->fullname;
        $this->main->update('merchants', array('status' => 1), array('id' => $id));
        $message = $this->load->view('email/merchant/activate', $this->data, TRUE);
        $cronjob = array(
            'from' => settings('send_email_from'),
            'from_name' => settings('store_name'),
            'to' => $user->email,
            'subject' => 'Akun penjual ' . settings('store_name').' Anda telah aktif',
            'message' => $message
        );
        $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));

        $dir= FCPATH.'../files/images/merchant/'.$data->username;
        if (!is_dir($dir)) {
            mkdir($dir,0777,TRUE);
        }
        $dir_thumb= FCPATH.'../files/image_thumbs/merchant/'.$data->username;
        if (!is_dir($dir_thumb)) {
            mkdir($dir_thumb,0777,TRUE);
        }

        //insert seo url
        $url = 'merchant_home/view/' . $id;
        $seo_url = strtolower($data->username);
        $this->main->insert('seo_url', array('query' => $url, 'keyword' => $seo_url));


        $this->load->library('sprint');
        $sms = json_decode(settings('sprint_sms'), true);
        $code = rand(1000, 9999);
        $sms['m'] = 'Akun penjual Anda telah aktif. Silahkan download Tokomanamana Seller di Playstore / login di http://tokomanamana.com/ma/';
        $sms['d'] = $user->phone;
        $url = $sms['url'];
        unset($sms['url']);
        $sprint_response = $this->sprint->sms($url, $sms);
        $return = array('message' => sprintf(lang('activate_message'), $data->name), 'status' => 'success');
        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->aauth->control('merchant/delete');
        $id = decode($id);
        $data = $this->main->get('merchants', array('id' => $id));
        $this->main->delete('merchants', array('id' => $id));
        $this->main->delete('merchant_users', array('id' => $data->auth));
        $this->main->delete('merchant_user_to_group', array('user' => $data->auth));
        $this->main->delete('product_merchant', array('merchant' => $data->id));
        $products = $this->main->gets('products', array('merchant' => $data->id));
        if ($products) {
            foreach ($products->result() as $product) {
                $this->main->delete('product_price', array('product' => $product->id));
                $this->main->delete('product_price_level', array('product' => $product->id));
                $this->main->delete('product_feature', array('product' => $product->id));
                $this->main->delete('product_image', array('product' => $product->id));
                $reviews = $this->main->gets('product_review', array('product' => $product->id));
                if ($reviews) {
                    foreach ($reviews->result() as $review) {
                        $this->main->delete('product_review_likedislike', array('product_review' => $review->id));
                    }
                }
                $this->main->delete('product_review', array('product' => $product->id));
                $options = $this->main->gets('product_option', array('product' => $product->id));
                if ($options) {
                    foreach ($options->result() as $option) {
                        $this->main->delete('product_option_combination', array('product_option' => $option->id));
                        $this->main->delete('product_option_image', array('product_option' => $option->id));
                    }
                }
                $this->main->delete('product_option', array('product' => $product->id));
            }
        }
        $this->main->delete('products', array('merchant' => $data->id));

        $return = array('message' => sprintf(lang('delete_success_message'), $data->name), 'status' => 'success');

        echo json_encode($return);
    }

    public function check_username($str) {
        if ($this->main->get('merchants', array('username' => $str)) && $this->main->get('seo_url', array('keyword' => $str))) {
            $this->form_validation->set_message('check_username', sprintf(lang('username_exist_message'), $str));
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function get_cities() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $output = '<option value=""></option>';
        if ($province = $this->input->post('id')) {
            $cities = $this->main->gets('cities', array('province' => $province), 'name asc');
            if ($cities)
                foreach ($cities->result() as $city) {
                    $output .= '<option value="' . $city->id . '">' . $city->name . '</option>';
                }
        }
        echo $output;
    }

    public function get_districts() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $output = '<option value=""></option>';
        if ($city = $this->input->post('id')) {
            $districts = $this->main->gets('districts', array('city' => $city), 'name asc');
            if ($districts)
                foreach ($districts->result() as $district) {
                    $output .= '<option value="' . $district->id . '">' . $district->name . '</option>';
                }
        }
        echo $output;
    }

}
