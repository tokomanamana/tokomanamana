<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo ($data) ? lang('category_edit_heading') : lang('category_add_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('catalog/categories'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('category_list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('catalog/categories/save'); ?>" method="post" class="form-horizontal" id="form" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?php echo set_value('id', ($data) ? encode($data->id) : ''); ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#general" data-toggle="tab"><?php echo lang('category_form_general_tabs'); ?></a></li>
                                    <li><a href="#feature" data-toggle="tab"><?php echo lang('category_form_feature_tabs'); ?></a></li>
                                    <li><a href="#variation" data-toggle="tab"><?php echo lang('category_form_variation_tabs'); ?></a></li>
                                    <li><a href="#seo" data-toggle="tab"><?php echo lang('category_form_seo_tabs'); ?></a></li>
                                    <li><a href="#home" data-toggle="tab"><?php echo lang('category_form_home_tabs'); ?></a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="general">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="name"><?php echo lang('category_form_name_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" required="" id="name" name="name" placeholder="<?php echo lang('category_form_name_placeholder'); ?>" value="<?php echo ($data) ? $data->name : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('category_form_parent_label'); ?></label>
                                            <div class="col-md-9">
                                                <select class="bootstrap-select" name="parent" data-live-search="true" data-width="100%">
                                                    <option value=""></option>
                                                    <?php if ($categories) { ?>
                                                        <?php foreach ($categories->result() as $category) { ?>
                                                            <option value="<?php echo $category->id; ?>" <?php echo ($data) ? ($data->parent == $category->id) ? 'selected="selected"' : '' : ''; ?>><?php echo $category->name; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('category_form_icon_label'); ?></label>
                                            <div id="image-container">
                                                <div class="col-sm-1" id="image-preview" <?= ($data) ? ($data->icon) ? '' : 'style="display:none;"' : 'style="display:none;"' ?>>
                                                    <div class="thumbnail">
                                                        <div class="thumb">
                                                            <img id="img" src="<?php echo ($data) ? ($data->icon) ? site_url('../files/images/' . $data->icon) : '' : ''; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="image_name" name="image_name" value="<?php echo ($data) ? $data->icon : ''; ?>">
                                                <input type="file" accept="image/*" name="icon_image" id="icon_image" style="display: none;" onchange="change_image(this)">
                                            </div>
                                            <div class="col-sm-6" id="add-image">
                                                <button type="button" id="add-image-btn" class="btn btn-default" data-toggle="modal" data-target="#filemanager" <?php echo ($data && $data->icon) ? 'style="display:none;"' : ''; ?>>
                                                    <i class="icon-plus3 position-left"></i> 
                                                    <?php echo lang('category_form_add_image_button'); ?>
                                                </button>

                                                <button type="button" id="btn-edit-image" class="btn btn-default" data-toggle="modal" data-target="#filemanager" <?php echo (!$data || ($data && !$data->icon)) ? 'style="display:none;"' : ''; ?>>
                                                    <i class="icon-pencil position-left"></i> 
                                                    <?php echo lang('category_form_edit_image_button'); ?>
                                                </button>

                                                <button type="button" id="btn-delete-image" class="btn btn-default" <?php echo (!$data || ($data && !$data->icon)) ? 'style="display:none;"' : ''; ?>>
                                                    <i class="icon-cross2 position-left"></i> 
                                                    <?php echo lang('category_form_delete_image_button'); ?>
                                                </button>

                                                <!-- <input type="hidden" id="image" name="icon" value="<?php echo ($data) ? $data->icon : ''; ?>"> -->
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('category_form_description_label'); ?></label>
                                            <div class="col-md-9">
                                                <textarea cols="30" rows="3" class="form-control" id="description" name="description" placeholder="<?php echo lang('category_form_description_placeholder'); ?>"><?php echo ($data) ? $data->description : ''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane " id="feature">
                                        <p class="content-group">Pilih fitur yang sesuai dengan kategori ini. Biarkan kosong jika tidak diperlukan.</p>

                                        <select multiple="multiple" name="feature[]" class="form-control listbox">
                                            <?php
                                            $cf_list = array();
                                            $category_feature = $this->main->gets('category_feature', array('category' => $data->id));
                                            if ($category_feature) {
                                                foreach ($category_feature->result() as $cf) {
                                                    $cf_list[] = $cf->feature;
                                                }
                                            }
                                            if ($features) {
                                                foreach ($features->result() as $feature) {
                                                    echo '<option value="' . $feature->id . '" ' . ((in_array($feature->id, $cf_list)) ? 'selected' : '') . '>' . $feature->name . '</option>';
                                                }
                                            }
                                            ?>
                                        </select>

                                    </div>
                                    <div class="tab-pane " id="variation">
                                        <p class="content-group">Pilih variasi yang sesuai dengan kategori ini. Biarkan kosong jika tidak diperlukan.</p>

                                        <select multiple="multiple" name="variation[]" class="form-control listbox">
                                            <?php
                                            $variation_list = array();
                                            $category_variation = $this->main->gets('category_variation', array('category' => $data->id));
                                            if ($category_variation) {
                                                foreach ($category_variation->result() as $cv) {
                                                    $variation_list[] = $cv->variation;
                                                }
                                            }
                                            if ($variations) {
                                                foreach ($variations->result() as $variation) {
                                                    echo '<option value="' . $variation->id . '" ' . ((in_array($variation->id, $variation_list)) ? 'selected' : '') . '>' . $variation->name . '</option>';
                                                }
                                            }
                                            ?>
                                        </select>

                                    </div>
                                    <div class="tab-pane " id="seo">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('category_form_meta_title_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" name="meta_title" class="form-control" value="<?php echo ($data) ? $data->meta_title : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('category_form_meta_description_label'); ?></label>
                                            <div class="col-md-9">
                                                <textarea type="text" name="meta_description" class="form-control"><?php echo ($data) ? $data->meta_description : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('category_form_meta_keyword_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" name="meta_keyword" class="tags-input" value="<?php echo ($data) ? $data->meta_keyword : ''; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane " id="home">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Tampilkan di Home</label>
                                            <div class="col-md-9">
                                                <input type="checkbox" name="home_show" value="1" data-on-text="Ya" data-off-text="Tidak" class="switch" <?php echo ($data) ? (($data->home_show == 1) ? 'checked' : '') : 'checked'; ?>>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Banner</label>
                                            <div id="banner-container">
                                                <div class="col-md-3" id="banner-preview" <?= ($data) ? ($data->home_banner) ? '' : 'style="display:none;"' : 'style="display:none;"' ?>>
                                                    <div class="thumbnail">
                                                        <div class="thumb">
                                                            <img id="img_banner" src="<?php echo ($data) ? ($data->home_banner) ? site_url('../files/images/' . $data->home_banner) : '' : ''; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="banner" name="home_banner" value="<?php echo ($data) ? $data->home_banner : ''; ?>">
                                                <input type="file" accept="image/*" id="banner_image" name="banner_image" style="display: none;" onchange="change_banner(this)">
                                            </div>
                                            <div class="col-md-6" id="add-banner">
                                                <button type="button" id="add-banner-btn" class="btn btn-default" data-toggle="modal" data-target="#filemanager2" <?php echo ($data && $data->home_banner) ? 'style="display:none;"' : ''; ?>>
                                                    <i class="icon-plus3 position-left"></i> 
                                                    <?php echo lang('category_form_add_image_button'); ?>
                                                </button>

                                                <button type="button" id="btn-edit-banner" class="btn btn-default" data-toggle="modal" data-target="#filemanager2" <?php echo (!$data || ($data && !$data->home_banner)) ? 'style="display:none;"' : ''; ?>>
                                                    <i class="icon-pencil position-left"></i> 
                                                    <?php echo lang('category_form_edit_image_button'); ?>
                                                </button>

                                                <button type="button" id="btn-delete-banner" class="btn btn-default" <?php echo (!$data || ($data && !$data->home_banner)) ? 'style="display:none;"' : ''; ?>>
                                                    <i class="icon-cross2 position-left"></i> 
                                                    <?php echo lang('category_form_delete_image_button'); ?>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Warna Garis</label>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control" name="home_border" value="<?php echo ($data) ? $data->home_border : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Urutan</label>
                                            <div class="col-md-2">
                                                <input type="number" class="form-control" name="home_sort_order" value="<?php echo ($data) ? $data->home_sort_order : 0; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('catalog/categories'); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- <div id="filemanager" class="modal" data-backdrop="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">File Manager</h5>
            </div>

            <div class="modal-body">
                <iframe  width="100%" height="550" frameborder="0" src="<?php echo site_url('filemanager/dialog.php?type=1&editor=false&field_id=image&relative_url=1'); ?>"></iframe>
            </div>
        </div>
    </div>
</div>
<div id="filemanager2" class="modal" data-backdrop="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">File Manager</h5>
            </div>

            <div class="modal-body">
                <iframe  width="100%" height="550" frameborder="0" src="<?php echo site_url('filemanager/dialog.php?type=1&editor=false&field_id=banner&relative_url=1'); ?>"></iframe>
            </div>
        </div>
    </div>
</div> -->
<script type="text/javascript">
    var rules_form = {};
</script>