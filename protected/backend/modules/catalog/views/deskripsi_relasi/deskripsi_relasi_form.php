<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo ($data) ? lang('deskripsi_relasi_edit_heading') : lang('deskripsi_relasi_add_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('catalog/deskripsi_relasi'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('deskripsi_relasi_list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('catalog/deskripsi_relasi/save'); ?>" method="post" class="form-horizontal" id="form">
                    <input type="hidden" name="id" value="<?php echo set_value('id_kategori', ($data) ? encode($data->id) : ''); ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="tabbable">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="general">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('deskripsi_relasi_form_name_label'); ?></label>
                                            <div class="col-md-9">
                                                <?php if ($categories_lama){ ?>
                                                    <?php foreach ($categories_lama->result() as $tes){ $id_k = $tes->id; } ?>
                                                    <input type="hidden" value="<?php echo $tes->id ?>" name="idkat">
                                                    <input class="form-control" type="text" disabled="" value="<?php echo $tes->name ?>"> 
                                                <?php } else { ?>
                                                <input type="hidden" value="" name="idkat">
                                                <select class="bootstrap-select" name="parent" data-live-search="true" data-width="100%"><?php
                                                         if ($categories) { ?>
                                                        <?php foreach ($categories->result() as $deskripsi_relasi) { ?>
                                                            <option value="<?php echo $deskripsi_relasi->id; ?>" <?php echo ($data) ? ($id_k == $deskripsi_relasi->id) ? 'selected="selected"' : '' : ''; ?>><?php echo $deskripsi_relasi->name; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('deskripsi_relasi_form_param_label'); ?></label>
                                            <div class="col-md-5">
                                                <div class="well pt-5 pb-5" style="max-height: 200px; overflow: auto;">
                                                    <?php
                                                    if ($param_kosong){
                                                        foreach ($param_kosong->result() as $ksg) { ?>
                                                            <div class="checkbox"><label><input <?php if($ksg->hasil == 'YES'){ echo "checked"; } ?> type="checkbox" name="param[]" value="<?php echo $ksg->id_deskripsi; ?>" ><?php echo ucwords($ksg->nama); ?></label></div>
                                                    <?php  }
                                                    } else { 
                                                        foreach ($param->result() as $prm) { ?>
                                                            <div class="checkbox"><label><input type="checkbox" name="param[]" value="<?php echo $prm->id_deskripsi; ?>" ><?php echo ucwords($prm->nama); ?></label></div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('catalog/categories'); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var rules_form = {};
</script>