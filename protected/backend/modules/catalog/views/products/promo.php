<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h3>Sortir Promo</h3>
            </div>
        </div>
    </div>
    
    <div class="content">
        <div class="panel panel-flat">
            <div class="panel-body">
                <form id="promo-settings">
                    <div class="form-group">
                        <label for="" class="col-md-4 control-label">Sortir Promo Berdasarkan</label>
                        <div class="col-md-4">
                            <select name="promo" class="form-control" id="promo">
                                <?php $e = json_decode(settings('promo_sort')); ?>
                                <?php foreach($e as $key => $promo) : ?>
                                    <?php if (settings('promo_sort_by') == $key) : ?>
                                        <option value="<?php echo encode($key); ?>" selected><?php echo ucwords($promo->desc); ?></option>
                                    <?php else : ?>
                                        <option value="<?php echo encode($key); ?>"><?php echo ucwords($promo->desc); ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>