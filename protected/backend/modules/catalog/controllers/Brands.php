<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Brands extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->aauth->control('catalog/brand');

        $this->lang->load('brands', settings('language'));
        $this->load->model('brands_model', 'brands');

        $this->data['menu'] = 'catalog_brand';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('brand'), '/catalog/brands');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        // Script Delete Images
        // $dir = FCPATH . '../files/images/BRAND';
        // $images = glob("$dir/*.*");
        // $not_available_images = [];
        // $brands_images = [];
        // $brands = $this->main->gets('brands', [])->result();
        // foreach($brands as $brand) {
        //     array_push($brands_images, FCPATH . '../files/images/' . $brand->image);
        // }
        // foreach($images as $image) {
        //     if(!in_array($image, $brands_images)) {
        //         array_push($not_available_images, $image);
        //     }
        // }
        // foreach($not_available_images as $img) {
        //     if(file_exists($img)) {
        //         unlink($img);
        //     }
        // }

        $this->output->set_title(lang('brand_heading'));
        $this->load->view('brands/brands', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->brands->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    ($data->image) ? '<a href="#"><img src="' . site_url('../files/images/' . $data->image) . '" class="img-circle img-xs"></a>' : '',
                    $data->name,
                    $data->seo_url,
                    // '<td class="text-center">
                    // <ul class="icons-list">
                    // <li class="dropdown">
                    // <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    // <ul class="dropdown-menu dropdown-menu-right">
                    // <li><a href="' . site_url('catalog/brands/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>
                    // <li><a href="' . site_url('catalog/brands/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>
                    // </ul>
                    // </li>
                    // </ul>
                    // </td>',
                    '<a href="' . site_url('catalog/brands/form/' . encode($data->id)) . '"><button type="button"   class="edit btn btn-info" style="background: #5BC0DE;">' . lang('button_edit') . '</button></a> ' . '<a href="' . site_url('catalog/brands/delete/' . encode($data->id)) . '" class="delete btn btn-danger" style="background: #D9534F">' . lang('button_delete') . '</a>'
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->brands->count_all();
        $output['recordsFiltered'] = $this->brands->count_all($search);
        echo json_encode($output);
    }

    public function form($id = '') {
        $this->template->_init();
        $this->template->form();

        $this->load->js('../assets/backend/js/modules/catalog/brand_form.js');

        $this->data['data'] = array();
        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('brand'), '/catalog/brands');

        if ($id) {
            // $this->aauth->control('catalog/brand/edit');
            $id = decode($id);
            $this->data['data'] = $this->brands->get($id);
            $this->data['data']->description = tinymce_get_url_files($this->data['data']->description);
            $this->breadcrumbs->push(lang('brand_edit_heading'), '/catalog/brands/form/' . encode($id));
            $this->breadcrumbs->push($this->data['data']->name, '/');
        } else {
            // $this->aauth->control('catalog/brand/add');
            $this->breadcrumbs->push(lang('brand_add_heading'), '/catalog/brands/form');
        }
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(($this->data['data']) ? lang('brand_edit_heading') : lang('brand_add_heading'));
        $this->load->view('brands/brand_form', $this->data);
    }

    // public function save() {
    //     $this->input->is_ajax_request() or exit('No direct post submit allowed!');
    //     $this->load->library('form_validation');

    //     $this->form_validation->set_rules('name', 'lang:brand_form_name_label', 'trim|required');
    //     $this->form_validation->set_rules('seo_url', 'lang:brand_form_seo_url_label', 'trim|alpha_dash');

    //     if ($this->form_validation->run() === true) {
    //         $data = $this->input->post(null, true);

    //         do {
    //             if ($data['id'])
    //                 $data['id'] = decode($data['id']);

    //             $data['description'] = str_replace("\n", "<br />",  $data['description']); 
                
    //             $url = 'catalog/brands/view/' . $data['id'];
    //             if ($seo_url = $data['seo_url']) {
    //                 if (check_url($seo_url, $url)) {
    //                     $return = array('message' => lang('brand_friendly_url_exist_message'), 'status' => 'error');
    //                     break;
    //                 }
    //             } else {
    //                 $seo_url = $url;
    //             }
    //             unset($data['seo_url']);

    //             if (!$data['id']) {
    //                 $id = $this->main->insert('brands', $data);
    //                 $url .= $id;
    //             } else {
    //                 $save = $this->main->update('brands', $data, array('id' => $data['id']));
    //                 $this->main->delete('seo_url', array('query' => $url));
    //             }
                
    //             $this->main->insert('seo_url', array('query' => $url, 'keyword' => $seo_url));

    //             $return = array('message' => sprintf(lang('brand_save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('catalog/brands'));
    //         } while (0);
    //     } else {
    //         $return = array('message' => validation_errors(), 'status' => 'error');
    //     }
    //     echo json_encode($return);
    // }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'lang:brand_form_name_label', 'trim|required');
        $this->form_validation->set_rules('seo_url', 'lang:brand_form_seo_url_label', 'trim|alpha_dash');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);

            do {
                if($data['image'] == '') {
                    $return = array('message' => 'Gambar harus diisi!', 'status' => 'error');
                    break;
                } 
                if($data['id'])
                    $data['id'] = decode($data['id']);
                // $data['description'] = tinymce_parse_url_files($data['description']);

                $data['meta_description'] = $data['description'];
                $data['description'] = str_replace("\n", "<br />",  $data['description']); 
                // $data['status'] = (isset($data['status']) && $data['status'] == 1) ? $data['status'] : 0;
                $data['meta_title'] = $data['name'];

                $url = 'catalog/brands/view/' . $data['id'];
                if ($seo_url = $data['seo_url']) {
                    if (check_url($seo_url, $url)) {
                        $return = array('message' => 'Seo URL sudah digunakan', 'status' => 'error');
                        break;
                    }
                } else {
                    $seo_url = '';
                }
                unset($data['seo_url']);

                if($data['id']) {
                    $brand = $this->main->get('brands', ['id' => $data['id']]);
                    if($brand->image != $data['image']) {
                        $old_brand_image = FCPATH . '../files/images/' . $brand->image;
                        if(file_exists($old_brand_image)) {
                            unlink($old_brand_image);
                        }
                    } 
                    $save = $this->main->update('brands', $data, array('id' => $data['id']));
                    $this->main->delete('seo_url', array('query' => $url));
                } else {
                    $id = $this->main->insert('brands', $data);
                    $url .= $id;
                }

                $this->main->insert('seo_url', array('query' => $url, 'keyword' => $seo_url));

                $return = array('message' => sprintf(lang('brand_save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('catalog/brands'));
            } while (0);
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        // $this->aauth->control('catalog/brand/delete');
        $id = decode($id);
        $data = $this->main->get('brands', array('id' => $id));

        $delete = $this->main->delete('brands', array('id' => $id));
        if ($delete) {
            if($data->image) {
                $brand_image = FCPATH . '../files/images/' . $data->image;
                if(file_exists($brand_image)) {
                    unlink($brand_image);
                }
            }
            $this->main->delete('seo_url', array('query' => 'catalog/brands/view/' . $data->id));
            $return = array('message' => sprintf(lang('brand_delete_success_message'), $data->name), 'status' => 'success');
        } else {
            $return = array('message' => lang('brand_delete_error_message'), 'status' => 'error');
        }

        echo json_encode($return);
    }

    public function image() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $data = $this->input->post(null, true);
        $return = [];
        $config['upload_path']          = FCPATH.'../files/images/BRAND/';
        $config['allowed_types']        = 'jpg|jpeg|png';
        $config['file_name']            = $data['image_name'];
        $config['overwrite']            = true;
        $config['max_size']             = 1024; //1MB

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('brand_image'))
        {
            $error = $this->upload->display_errors();
            $return = [
                'status' => 'error',
                'message' => $error
            ];
        }
        else
        {   
            // if($data['image_name_temp']) {
            //     $brand_image = FCPATH . '../files/images/' . $data['image_name_temp'];
            //     if(file_exists($brand_image)) {
            //         unlink($brand_image);
            //     }
            // }
            $return = [
                'status' => 'success',
                'message' => 'BRAND/' . $config['file_name']
            ];
        }
        echo json_encode($return);
    }

    public function delete_image() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $data = $this->input->post(null, true);
        $brand_image = FCPATH . '../files/images/' . $data['image_name'];
        if(file_exists($brand_image)) {
            unlink($brand_image);
        }
        echo 'success';
    }

}
