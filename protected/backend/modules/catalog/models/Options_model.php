<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Options_model extends CI_Model {

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->where_like($search);
        if ($order) {
            $order['column'] = $this->get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('og.*, COUNT(o.id) count_option')
                ->join('options o', 'og.id = o.group', 'left')
                ->group_by('og.id')
                ->limit($length, $start);

        return $this->db->get('option_group og');
    }

    function get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'name';
                break;
            case 1: $key = 'sort_order';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->where_like($search);
        return $this->db->count_all_results('option_group');
    }

    function where_like($search = '') {
        $columns = array('name');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
            $this->db->group_end();
        }
    }

    function get_option_variants($option, $start = 0, $length, $search = '', $order = array()) {
        if ($order) {
//            $order['column'] = $this->get_alias_key($order['column']);
            $this->db->order_by('value', $order['dir']);
        }
        $this->db->where('group', $option)
                ->like('value', $search)
                ->limit($length, $start);

        return $this->db->get('options');
    }

    function count_option_variants($option, $search = '') {
        $this->db->where('group', $option)
                ->like('value', $search);
        return $this->db->count_all_results('options');
    }

}
