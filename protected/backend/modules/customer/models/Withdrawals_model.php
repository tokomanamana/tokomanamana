<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Withdrawals_model extends CI_Model {

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->filter($search);
        if ($order) {
            $columns = array(0 => 'bw.date_added', 1 => 'c.fullname', 2 => 'bw.amount', 4 => 'bw.status');
            $order['column'] = $columns[$order['column']];
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('bw.*, c.fullname customer_name')
                ->join('customers c', 'bw.customer_id = c.id', 'left')
                ->limit($length, $start);

        return $this->db->get('customer_balance_request bw');
    }

    function count_all($search = '') {
        $this->filter($search);
        $this->db->join('customers c', 'bw.customer_id = c.id', 'left');
        return $this->db->count_all_results('customer_balance_request bw');
    }

    function filter($search = '') {
        $columns = array('bw.date_added', 'c.fullname', 'bw.amount', 'bw.status');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
            $this->db->group_end();
        }
    }

}
