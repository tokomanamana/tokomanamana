<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['heading'] = 'Penarikan Dana';

$lang['date'] = 'Tanggal';
$lang['merchant'] = 'Merchant';
$lang['amount'] = 'Nominal';
$lang['bank'] = 'Bank';
$lang['status'] = 'Status';

$lang['save_success'] = "Tarik Dana merchant '%s' berhasil.";