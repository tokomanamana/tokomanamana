<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product_pages_model extends CI_Model {

    function get($id) {
        $this->db->select('p.*, keyword seo_url')
                ->join('seo_url su', "su.query = CONCAT('pages/products/',p.id)",'left')
                ->where('p.id', $id);
        $query = $this->db->get('pages p');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    function get_all($start = 0, $length, $search = '', $order = []) {
        $this->_where_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('p.id, title, status, keyword')
                ->join('seo_url su', "su.query = CONCAT('pages/products/',p.id)",'left')
                ->where('p.content', 'products')
                ->limit($length, $start);
        return $this->db->get('pages p');
    }

    private function _get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'title';
                break;
            case 1: $key = 'keyword';
                break;
            case 2: $key = 'status';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->_where_like($search);
        $this->db->join('seo_url su', "su.query = CONCAT('pages/view/',p.id)",'left')->where('p.content', 'products');
        return $this->db->count_all_results('pages p');
    }

    private function _where_like($search = '') {
        $columns = array('title', 'keyword');
        if ($search) {
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
        }
    }

    function get_all_category() {
        $this->db->select("cp.category id, GROUP_CONCAT(c2.name ORDER BY cp.level SEPARATOR ' > ') as name, c1.image, c1.active, c1.sort_order, c1.home_show, c1.home_sort_order", FALSE)
                ->join('categories c1', 'cp.category = c1.id', 'left')
                ->join('categories c2', 'cp.path = c2.id', 'left')
                ->where('c1.active', 1)
                ->group_by('cp.category')
                ->order_by('name', 'ASC');

        return $this->db->get('category_path cp');
    }

}
