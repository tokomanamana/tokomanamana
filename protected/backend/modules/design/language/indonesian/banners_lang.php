<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['banner_heading'] = 'Banner';
$lang['banner_list_heading'] = 'Daftar Banner';
$lang['banner_add_heading'] = 'Tambah Banner';
$lang['banner_edit_heading'] = 'Edit Banner';

$lang['banner_name_th'] = 'Nama';
$lang['banner_type_th'] = 'Jenis';
$lang['banner_status_th'] = 'Tampilkan';
$lang['actions_th'] = 'Opsi';

$lang['banner_form_general_tabs'] = 'Umum';
$lang['banner_form_seo_tabs'] = 'SEO';

$lang['banner_form_name_label'] = 'Nama';
$lang['banner_form_type_label'] = 'Jenis';
$lang['banner_form_content_label'] = 'Konten Banner';
$lang['banner_form_image_label'] = 'Gambar';
$lang['banner_form_link_label'] = 'Link URL';
$lang['banner_form_status_label'] = 'Tampilkan';

$lang['banner_save_success_message'] = "Banner '%s' berhasil disimpan.";
$lang['banner_save_error_message'] = "Banner '%s' gagal disimpan.";
$lang['banner_delete_success_message'] = "Banner '%s' berhasil dihapus.";
$lang['banner_delete_error_message'] = "Banner '%s' gagal dihapus.";

$lang['banner_form_name_placeholder'] = 'Masukkan nama banner';
$lang['banner_form_url_placeholder'] = 'Masukkan link URL';
$lang['banner_form_content_placeholder'] = 'Masukkan konten banner';