<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['brand_heading'] = 'Merek';
$lang['brand_list_heading'] = 'Daftar Merek';
$lang['brand_add_heading'] = 'Tambah Merek';
$lang['brand_edit_heading'] = 'Edit Merek';

$lang['brand_name_th'] = 'Nama';
$lang['brand_type_th'] = 'Jenis';
$lang['brand_status_th'] = 'Tampilkan';
$lang['action_th'] = 'Opsi';

$lang['brand_form_general_tabs'] = 'Umum';
$lang['brand_form_seo_tabs'] = 'SEO';

$lang['brand_form_name_label'] = 'Nama';
$lang['brand_form_type_label'] = 'Jenis';
$lang['brand_form_content_label'] = 'Konten Banner';
$lang['brand_form_image_label'] = 'Gambar';
$lang['brand_form_link_label'] = 'Link URL';
$lang['brand_form_status_label'] = 'Tampilkan';

$lang['brand_save_success_message'] = "Merek'%s' berhasil disimpan.";
$lang['brand_save_error_message'] = "Merek'%s' gagal disimpan.";
$lang['brand_delete_success_message'] = "Merek'%s' berhasil dihapus.";
$lang['brand_delete_error_message'] = "Merek'%s' gagal dihapus.";