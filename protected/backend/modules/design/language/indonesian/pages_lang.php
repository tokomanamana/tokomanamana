<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['heading'] = 'Halaman';
$lang['list_heading'] = 'Daftar Halaman';
$lang['add_heading'] = 'Tambah Halaman';
$lang['edit_heading'] = 'Edit Halaman';

$lang['general_tabs'] = 'Umum';
$lang['seo_tabs'] = 'SEO';
$lang['option_tabs'] = 'Opsi';

$lang['title'] = 'Judul';
$lang['content'] = 'Isi Halaman';
$lang['seo_url'] = 'Friendly URL';
$lang['status'] = 'Tampilkan';
$lang['meta_title'] = 'Meta Judul';
$lang['meta_description'] = 'Meta Deskripsi';
$lang['meta_keyword'] = 'Meta Kata Kunci';
$lang['hidden_title'] = 'Jangan Tampilkan Judul';

$lang['save_success'] = "Halaman '%s' berhasil disimpan.";
$lang['save_error'] = "Halaman '%s' gagal disimpan.";
$lang['delete_success'] = "Halaman '%s' berhasil dihapus.";
$lang['delete_error'] = "Halaman '%s' gagal dihapus.";
$lang['friendly_url_exist'] = "Friendly URL sudah digunakan.";