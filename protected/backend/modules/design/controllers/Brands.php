<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Brands extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->aauth->control('design/brand');

        $this->lang->load('design/brands', settings('language'));
        $this->load->model('brands_model', 'brands');
        $this->data['menu'] = 'design_brands';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('design'), '/');
        $this->breadcrumbs->push(lang('brand'), '/design/brands');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('brand_heading'));
        $this->load->view('brands/list', $this->data);
    }

    public function form($id = '') {
        $this->template->_init();
        $this->template->form();

        $this->data['data'] = array();

        $this->breadcrumbs->unshift(lang('design'), '/');
        $this->breadcrumbs->push(lang('brand'), '/design/brands');

        if ($id) {
            $this->aauth->control('design/brand/edit');
            $id = decode($id);
            $this->data['data'] = $this->main->get('brands', array('id' => $id));

            $this->breadcrumbs->push(lang('brand_edit_heading'), '/design/brands/form/' . encode($id));
            $this->breadcrumbs->push($this->data['data']->name, '/');
        } else {
            $this->aauth->control('design/brand/add');
            $this->breadcrumbs->push(lang('brand_add_heading'), '/design/brands/form');
        }

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->load->js(site_url('../assets/backend/js/modules/design/brand_form.js'));

        $this->output->set_title(($this->data['data']) ? lang('brand_edit_heading') : lang('brand_add_heading'));
        $this->load->view('brands/form', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->brands->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->name,
                    ($data->status == 1) ? '<i class="icon-checkmark3 text-success"></i>' : '<i class="icon-cross2 text-danger-400"></i>',
                    // '<ul class="icons-list">
                    // <li class="dropdown">
                    // <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    // <ul class="dropdown-menu dropdown-menu-right">'.
                    // ($this->aauth->is_allowed('design/brands/edit')?'<li><a href="' . site_url('design/brands/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>':'').
                    // ($this->aauth->is_allowed('design/brands/delete')?'<li><a href="' . site_url('design/brands/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>':'').
                    // '</ul>
                    // </li>
                    // </ul>',
                    ($this->aauth->is_allowed('design/brands/edit') ? '<a href="' . site_url('design/brands/form/' . encode($data->id)) . '"><button type="button"   class="edit btn btn-info" style="background: #5BC0DE;">' . lang('button_edit') . '</button></a> ' : '') .
                    ($this->aauth->is_allowed('design/brands/delete') ? ' <a href="' . site_url('design/brands/delete/' . encode($data->id)) . '" class="delete btn btn-danger" style="background: #D9534F">' . lang('button_delete') . '</a>' : '')
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->brands->count_all();
        $output['recordsFiltered'] = $this->brands->count_all($search);
        echo json_encode($output);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'lang:brand_form_name_label', 'trim|required');
        $this->form_validation->set_rules('image', 'lang:brand_form_image_label', 'trim|required');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);

            do {
                if ($data['id'])
                    $data['id'] = decode($data['id']);

                $data['status'] = (isset($data['status']) && $data['status'] == 1) ? $data['status'] : 0;

                if ($data['id']) {
                    $brand = $this->main->get('brands', ['id' => $data['id']]);
                    if($brand->image != $data['image']) {
                        $old_brand_image = FCPATH . '../files/images/' . $brand->image;
                        if(file_exists($old_brand_image)) {
                            unlink($old_brand_image);
                        }
                    } 
                    $this->main->update('brands', $data, array('id' => $data['id']));
                } else {
                    $id = $this->main->insert('brands', $data);
                }

                $return = array('message' => sprintf(lang('brand_save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('design/brands'));
            } while (0);
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }

        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->aauth->control('design/brand/delete');
        $id = decode($id);
        $data = $this->main->get('brands', array('id' => $id));
        $delete = $this->main->delete('brands', array('id' => $id));
        if ($delete) {
            if($data->image) {
                $brand_image = FCPATH . '../files/images/' . $data->image;
                if(file_exists($brand_image)) {
                    unlink($brand_image);
                }
            }
            $this->main->delete('seo_url', array('query' => 'catalog/brands/view/' . $id));
            $return = array('message' => sprintf(lang('brand_delete_success_message'), $data->name), 'status' => 'success');
        } else {
            $return = array('message' => sprintf(lang('brand_delete_error_message'), $data->name), 'status' => 'danger');
        }
        echo json_encode($return);
    }

}
