<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Product_pages extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->aauth->control('design/product_pages');
        $this->lang->load('design/product_pages', settings('language'));
        $this->load->model('product_pages_model', 'product_pages');
        $this->data['menu'] = 'design_product_pages';
        $this->load->helper(array('form', 'url'));
    }

    public function index() {
        $this->breadcrumbs->unshift(lang('design'), '/');
        $this->breadcrumbs->push(lang('product_pages'), '/design/product_pages');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->template->_init();
        $this->template->table();
        $this->output->set_title(lang('heading'));
        $this->load->view('product_pages/list', $this->data);
    }

    public function form($id = '') {
        $this->data['data'] = array();

        $this->breadcrumbs->unshift(lang('design'), '/');
        $this->breadcrumbs->push(lang('page'), '/design/product_pages');

        if ($id) {
            $this->aauth->control('design/product_pages/edit');
            $id = decode($id);
            $this->data['data'] = $this->product_pages->get($id);
            $this->breadcrumbs->push(lang('edit_heading'), '/design/product_pages/form/' . encode($id));
            $this->breadcrumbs->push($this->data['data']->title, '/');
        } else {
            $this->aauth->control('design/product_pages/add');
            $this->breadcrumbs->push(lang('add_heading'), '/design/product_pages/form');
        }

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->template->_init();
        $this->template->form();
        $this->output->set_title(($this->data['data']) ? lang('edit_heading') : lang('add_heading'));
        $this->load->js(site_url('../assets/backend/js/modules/design/product_pages_form.js'));
        $this->load->view('product_pages/form', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->product_pages->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->title,
                    ($data->keyword) ? $data->keyword : '',
                    ($data->status == 1) ? '<i class="icon-checkmark3 text-success"></i>' : '<i class="icon-cross2 text-danger-400"></i>',
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">' .
                    ($this->aauth->is_allowed('design/product_pages/edit') ? '<li><a href="' . site_url('design/product_pages/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>' : '') .
                    ($this->aauth->is_allowed('design/product_pages/delete') ? '<li><a href="' . site_url('design/product_pages/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>' : '') .
                    '</ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->product_pages->count_all();
        $output['recordsFiltered'] = $this->product_pages->count_all($search);
        echo json_encode($output);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->load->library('form_validation');

        $this->form_validation->set_rules('title', 'lang:title', 'trim|required');
        $this->form_validation->set_rules('seo_url', 'lang:seo_url', 'trim|seo_url');
        $this->form_validation->set_rules('product[]', 'lang:list_product', 'required');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);
            do {
                if ($data['id'])
                    $data['id'] = decode($data['id']);

                if(isset($data['image'])) {
                    $data['image'] = $data['image'];
                } else {
                    if(isset($_FILES['image'])) {
                        $image_name = $_FILES['image']['name'];
                        $image_name = str_replace(' ', '-', $image_name);
                        $image_name = explode('.', $image_name);
                        $date = date('Y_m_d_H_i_s');
                        $full_name = $image_name[0] . '-' . $date . '.' . $image_name[1];
                        $config['upload_path']          = '../files/images/pages/';
                        $config['allowed_types']        = 'gif|jpg|png|jpeg';
                        $config['file_name']            = $full_name;
                        $this->load->library('upload', $config);
                        if(!$this->upload->do_upload('image')) {
                            $return = [
                                'message' => $this->upload->display_errors(),
                                'status' => 'error'
                            ];
                            break;
                        } else {
                            $data['image'] = 'pages/' . $full_name;
                        }
                    } else {
                        $data['image'] = '';
                    }
                }

                $data['status'] = (isset($data['status']) && $data['status'] == 1) ? $data['status'] : 0;
                $values = $data['product'];
                $set_by = $data['set_by'];

                $url = 'pages/products/' . $data['id'];
                if ($seo_url = $data['seo_url']) {
                    if (check_url($seo_url, $url)) {
                        $return = array('message' => lang('friendly_url_exist'), 'status' => 'error');
                        break;
                    }
                } else {
                    $seo_url = $url;
                }
                unset($data['seo_url']);
                $data['content'] = 'products';

                if ($data['id']) {
                    $data['user_modified'] = $this->data['user']->id;
                    $table_pages = [
                        'status' => $data['status'],
                        'title' => $data['title'],
                        'content' => $data['content'],
                        'user_modified' => $data['user_modified'],
                        'options' => $set_by,
                        'image' => $data['image']
                    ];
                    $this->main->update('pages', $table_pages, array('id' => $data['id']));
                    $this->main->delete('seo_url', array('query' => $url));
                    $this->main->update('products', ['pages' => null]);
                    if($set_by == 'category') {
                        foreach($values as $value) {
                            $product_by_category = $this->main->gets('products', ['category' => $value]);
                            if($product_by_category) {
                                foreach($product_by_category->result() as $val) {
                                    $this->main->update('products', ['pages' => null], ['id' => $val->id]);
                                    $pages = [];
                                    if($val->pages) {
                                        $pages_ = json_decode($val->pages);
                                        array_push($pages_, $data['id']);
                                        $new_pages = json_encode($pages_);
                                        $this->main->update('products', ['pages' => $new_pages], ['id' => $val->id]);
                                    } else {
                                        array_push($pages, $data['id']);
                                        $new_pages = json_encode($pages);
                                        $this->main->update('products', ['pages' => $new_pages], ['id' => $val->id]);
                                    }
                                }
                            }
                        }
                    } else {
                        foreach($values as $value) {
                            $check = $this->main->get('products', ['id' => $value]);
                            $pages = [];
                            if($check->pages) {
                                $pages_ = json_decode($check->pages);
                                array_push($pages_, $data['id']);
                                $new_pages = json_encode($pages_);
                                $this->main->update('products', ['pages' => $new_pages], ['id' => $check->id]);
                            } else {
                                array_push($pages, $data['id']);
                                $new_pages = json_encode($pages);
                                $this->main->update('products', ['pages' => $new_pages], ['id' => $check->id]);
                            }
                        }
                    }
                } else {
                    $data['user_added'] = $this->data['user']->id;
                    $table_pages = [
                        'status' => $data['status'],
                        'title' => $data['title'],
                        'content' => $data['content'],
                        'user_added' => $data['user_added'],
                        'options' => $set_by,
                        'image' => $data['image']
                    ];
                    $id = $this->main->insert('pages', $table_pages);
                    $url .= $id;
                    if($set_by == 'category') {
                        foreach($values as $value) {
                            $product_by_category = $this->main->gets('products', ['category' => $value]);
                            if($product_by_category) {
                                foreach($product_by_category->result() as $val) {
                                    $this->main->update('products', ['pages' => null], ['id' => $val->id]);
                                    $pages = [];
                                    if($val->pages) {
                                        $pages_ = json_decode($val->pages);
                                        array_push($pages_, $id);
                                        $new_pages = json_encode($pages_);
                                        $this->main->update('products', ['pages' => $new_pages], ['id' => $val->id]);
                                    } else {
                                        array_push($pages, $id);
                                        $new_pages = json_encode($pages);
                                        $this->main->update('products', ['pages' => $new_pages], ['id' => $val->id]);
                                    }
                                }
                            }
                        }
                    } else {
                        foreach($values as $value) {
                            $check = $this->main->get('products', ['id' => $value]);
                            $pages = [];
                            if($check->pages) {
                                $pages_ = json_decode($check->pages);
                                array_push($pages_, $id);
                                $new_pages = json_encode($pages_);
                                $this->main->update('products', ['pages' => $new_pages], ['id' => $check->id]);
                            } else {
                                array_push($pages, $id);
                                $new_pages = json_encode($pages, true);
                                $this->main->update('products', ['pages' => $new_pages], ['id' => $check->id]);
                            }
                        }
                    }
                }

                $this->main->insert('seo_url', array('query' => $url, 'keyword' => $seo_url));
                $return = array('message' => sprintf(lang('save_success'), $data['title']), 'status' => 'success', 'redirect' => site_url('design/product_pages'));
            } while (0);
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }

        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->aauth->control('design/product_pages/delete');
        $id = decode($id);
        $data = $this->main->get('pages', array('id' => $id));
        $delete = $this->main->delete('pages', array('id' => $id));
        if ($delete) {
            if($data->image) {
                if(file_exists('../files/images/' . $data->image)) {
                    unlink('../files/images/' . $data->image);
                }
            }
            $products = $this->main->gets('products');
            foreach($products->result() as $product) {
                if($product->pages) {
                    $this->main->update('products', ['pages' => NULL], ['id' => $product->id]);
                    $pages = json_decode($product->pages);
                    foreach ($pages as $key => $value) {
                        $new_pages = [];
                        if($value != $id) {
                            array_push($new_pages, $value);
                            $new_pages = json_encode($new_pages);
                            $this->main->update('products', ['pages' => $new_pages], ['id' => $product->id]);
                        }
                    }
                }
            }
            $this->main->delete('seo_url', array('query' => 'pages/products/' . $id));
            $return = array('message' => sprintf(lang('delete_success'), $data->title), 'status' => 'success');
        } else {
            $return = array('message' => sprintf(lang('delete_error'), $data->title), 'status' => 'danger');
        }
        echo json_encode($return);
    }

    public function get_product_category() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $category = $this->product_pages->get_all_category();
        $return = [];
        foreach ($category->result() as $key => $value) {
            array_push($return, [
                'id' => $value->id,
                'category_name' => $value->name
            ]);
        }
        echo json_encode($return);
    }

    public function get_all_product() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $products = $this->main->gets('products', ['status' => 1], 'name ASC');
        $return = [];
        foreach($products->result() as $value) {
            array_push($return, [
                'id' => $value->id,
                'name' => $value->name
            ]);
        }
        echo json_encode($return);
    }

}
