<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo ($data) ? lang('page_edit_heading') : lang('page_add_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('design/pages'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('page_list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('design/pages/save'); ?>" class="form-horizontal" method="post" id="form">
                    <input type="hidden" name="id" value="<?php echo ($data) ? encode($data->id) : ''; ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#general" data-toggle="tab"><?php echo lang('page_form_general_tabs'); ?></a></li>
                                    <li class=""><a href="#seo" data-toggle="tab"><?php echo lang('page_form_seo_tabs'); ?></a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="general">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('page_form_title_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" required="" name="title" value="<?php echo ($data) ? $data->title : ''; ?>" onkeyup="convertToSlug(this.value);">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('page_form_content_label'); ?></label>
                                            <div class="col-md-9">
                                                <textarea cols="30" rows="2" class="form-control tinymce" name="content"><?php echo ($data) ? $data->content : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('page_form_status_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="checkbox" name="status" value="1" data-on-text="<?php echo lang('yes'); ?>" data-off-text="<?php echo lang('no'); ?>" class="switch" <?php echo ($data) ? (($data->status == 1) ? 'checked' : '') : 'checked'; ?>>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="seo">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('page_form_seo_url_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="seo_url" name="seo_url" value="<?php echo ($data) ? $data->seo_url : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('page_form_meta_title_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="meta_title" placeholder="<?php echo lang('page_form_meta_title_placeholder'); ?>" value="<?php echo ($data) ? $data->meta_title : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('page_form_meta_description_label'); ?></label>
                                            <div class="col-md-9">
                                                <textarea cols="30" rows="2" class="form-control" name="meta_description" placeholder="<?php echo lang('page_form_meta_description_placeholder'); ?>"><?php echo ($data) ? $data->meta_description : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('page_form_meta_keyword_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" name="meta_keyword" class="tags-input" value="<?php echo ($data) ? $data->meta_keyword : ''; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('design/pages'); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>