<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo ($data) ? lang('edit_heading') : lang('add_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('design/product_pages'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('design/product_pages/save'); ?>" class="form-horizontal" method="post" id="form" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?php echo ($data) ? encode($data->id) : ''; ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('image'); ?></label>
                                <div id="image">
                                <?php if($data) : ?>
                                    <?php if($data->image) : ?>
                                        <div class="col-md-3">
                                            <img src="<?php echo site_url('../files/images/' . $data->image) ?>" style="width: 100%;">
                                            <input type="hidden" name="image" style="display: none;" value="<?php echo $data->image ?>">
                                        </div>
                                        <div class="col-md-3">
                                            <button type="button" class="btn btn-danger" onclick="delete_image()">Hapus</button>
                                        </div>
                                    <?php else : ?>
                                        <div class="col-md-3">
                                            <input type="file" class="form-control" name="image">
                                        </div>
                                    <?php endif; ?>
                                <?php else : ?>
                                    <div class="col-md-3">
                                        <input type="file" class="form-control" name="image">
                                    </div>
                                <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('title'); ?></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" required="" name="title" value="<?php echo ($data) ? $data->title : ''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('set_by') ?></label>
                                <div class="col-md-9">
                                    <select name="set_by" id="set_by" class="bootstrap-select" data-width="50%">
                                        <option value="" selected="" disabled="">Pilih berdasarkan</option>
                                        <option value="category" <?php echo ($data) ? (($data->options == 'category') ? 'selected' : '') : '' ?>>Kategori</option>
                                        <option value="product" <?php echo ($data) ? (($data->options == 'product') ? 'selected' : '') : '' ?>>Produk</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('list_product'); ?></label>
                                <div class="col-md-4">
                                    <div class="addto-search" style="margin-bottom: 15px;">
                                        <form action="#">
                                            <div class="form-group">
                                                <input class="form-control" id="search_product" type="text" placeholder="Cari..">
                                            </div>
                                        </form>
                                    </div>
                                    <div class="well pt-5 pb-5" style="max-height: 500px; overflow: auto;" id="checkbox_value">
                                        <?php if($data) : ?>
                                            <?php if($data->options == 'category') : ?>
                                                <?php $categories = $this->product_pages->get_all_category(); ?>
                                                <?php foreach ($categories->result() as $category) : ?>
                                                    <?php $get_products = $this->main->gets('products', ['category' => $category->id]); ?>
                                                    <?php $checked = false; ?>
                                                    <?php if($get_products) : ?>
                                                        <?php foreach($get_products->result() as $product) : ?>
                                                            <?php 
                                                            if($product->pages) {
                                                                $pages = json_decode($product->pages);
                                                                foreach($pages as $page) {
                                                                    if($page == $data->id) {
                                                                        $checked = true;
                                                                    } else {
                                                                        $checked = false;
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                    <div class="checkbox"><label><input type="checkbox" name="product[]" <?php echo ($checked) ? 'checked' : ''  ?> value="<?php echo $category->id ?>"><?php echo $category->name ?></label></div>
                                                <?php endforeach; ?>
                                            <?php else : ?>
                                                <?php $products = $this->main->gets('products', ['status' => 1], 'name ASC'); ?>
                                                <?php foreach($products->result() as $product) : ?>
                                                    <?php 
                                                    $checked = false;
                                                    if($product->pages) {
                                                        $pages = json_decode($product->pages);
                                                        foreach($pages as $page) {
                                                            if($page == $data->id) {
                                                                $checked = true;
                                                            } else {
                                                                $checked = false;
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                    <div class="checkbox"><label><input type="checkbox" name="product[]" <?php echo ($checked) ? 'checked=""' : '' ?> value="<?php echo $product->id ?>"><?php echo $product->name ?></label></div>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        <?php else : ?>
                                            Silahkan klik tombol diatas terlebih dahulu
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"><?php echo lang('seo_url') ?></label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" required="" name="seo_url" placeholder="Seo URL" value="<?php echo ($data) ? $data->seo_url : '' ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('status'); ?></label>
                                <div class="col-md-9">
                                    <input type="checkbox" name="status" value="1" data-on-text="<?php echo lang('yes'); ?>" data-off-text="<?php echo lang('no'); ?>" class="switch" <?php echo ($data) ? (($data->status == 1) ? 'checked' : '') : 'checked'; ?>>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('design/product_pages'); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>