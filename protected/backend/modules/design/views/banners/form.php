<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo ($data) ? lang('banner_edit_heading') : lang('banner_add_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('design/banners'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('banner_list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('design/banners/save'); ?>" class="form-horizontal" method="post" id="form">
                    <input type="hidden" name="id" id="id_brand" value="<?php echo ($data) ? encode($data->id) : ''; ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('banner_form_name_label'); ?></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" required="" name="name" value="<?php echo ($data) ? $data->name : ''; ?>" placeholder="<?php echo lang('banner_form_name_placeholder') ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('banner_form_type_label'); ?></label>
                                <div class="col-md-3">
                                    <select class="form-control bootstrap-select" name="type" required="">
                                        <option value="slider" <?php echo ($data && $data->type == 'slider') ? 'selected' : ''; ?>>slider</option>
                                        <option value="block_slider" <?php echo ($data && $data->type == 'block_slider') ? 'selected' : ''; ?>>block_slider</option>
                                        <option value="slider_payment" <?php echo ($data && $data->type == 'slider_payment') ? 'selected' : ''; ?>>slider_payment</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('banner_form_link_label'); ?></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" required="" name="link" value="<?php echo ($data) ? $data->link : '#'; ?>" placeholder="<?php echo lang('banner_form_url_placeholder') ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('banner_form_image_label'); ?></label>
                                <div id="image-container">
                                    <?php if ($data && $data->image) { ?>
                                        <div class="col-md-3" id="image-preview">
                                            <div class="thumbnail">
                                                <div class="thumb">
                                                    <img src="<?php echo site_url('../files/images/' . $data->image); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <input type="hidden" id="image_name" name="image" value="<?php echo ($data) ? $data->image : ''; ?>">
                                </div>
                                <div class="col-md-6" id="add-image">
                                    <button type="button" id="add-image-btn" class="btn btn-default" data-toggle="modal" data-target="#filemanager" <?php echo ($data && $data->image) ? 'style="display:none;"' : ''; ?>><?php echo lang('button_add_image'); ?></button>
                                    <button type="button" id="btn-edit-image" class="btn btn-default" data-toggle="modal" data-target="#filemanager" <?php echo (!$data || ($data && !$data->image)) ? 'style="display:none;"' : ''; ?>><?php echo lang('button_edit_image'); ?></button>
                                    <button type="button" id="btn-delete-image" class="btn btn-default" <?php echo (!$data || ($data && !$data->image)) ? 'style="display:none;"' : ''; ?>><?php echo lang('button_delete_image'); ?></button>
                                    <!-- <input type="hidden" id="image" name="image" value="<?php echo ($data) ? $data->image : ''; ?>"> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('banner_form_content_label'); ?></label>
                                <div class="col-md-9">
                                    <!-- <textarea cols="30" rows="2" class="form-control tinymce" name="content" placeholder="<?php echo lang('banner_form_content_placeholder') ?>"><?php echo ($data) ? $data->content : ''; ?></textarea> -->
                                    <textarea cols="30" rows="5" class="form-control" name="content" placeholder="<?php echo lang('banner_form_content_placeholder') ?>"><?php echo ($data) ? $data->content : ''; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('banner_form_status_label'); ?></label>
                                <div class="col-md-9">
                                    <input type="checkbox" name="status" value="1" data-on-text="<?php echo lang('yes'); ?>" data-off-text="<?php echo lang('no'); ?>" class="switch" <?php echo ($data) ? (($data->status == 1) ? 'checked' : '') : 'checked'; ?>>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('design/banners'); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<input type="file" name="banner_image" id="banner_image" style="display: none;">
<!-- <div id="filemanager" class="modal">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">File Manager</h5>
            </div>

            <div class="modal-body">
                <iframe  width="100%" height="550" frameborder="0" src="<?php echo site_url('filemanager/dialog.php?type=1&editor=false&field_id=image&relative_url=1'); ?>"></iframe>
            </div>
        </div>
    </div>
</div> -->