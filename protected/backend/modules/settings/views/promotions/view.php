<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('heading'); ?></h2>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('settings/promotions/form'); ?>" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span>Tambah Promosi</span></a>
                    <a href="#" class="btn btn-link btn-float has-text" data-toggle="modal" data-target="#set_url_promotions"><i class="icon-pencil text-primary"></i><span>Set URL Promosi</span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <table class="table" id="table" data-url="<?php echo site_url('settings/promotions/get_list'); ?>">
                <thead>
                    <tr>
                        <th class="default-sort" data-sort="asc"><?php echo lang('promotions_name_th') ?></th>
                        <th><?php echo lang('promotions_date_start_th') ?></th>
                        <th><?php echo lang('promotions_date_end_th'); ?></th>
                        <th><?php echo lang('promotions_status_th') ?></th>
                        <th><?php echo lang('promotions_color_th') ?></th>
                        <th><?php echo lang('promotions_message_countdown_label') ?></th>
                        <th><?php echo lang('promotions_url_th') ?></th>
                        <th class="no-sort text-center" style="width: 20px;"><?php echo lang('actions_th'); ?></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="set_url_promotions" tabindex="-1" role="dialog" aria-labelledby="set_url_promotions_title" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="set_url_promotions_title">Set URL Promosi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo site_url('settings/promotions/save_url') ?>" method="post" id="form">
        <input type="hidden" name="query" value="<?php echo ($url) ? $url->query : '' ?>">
      <div class="modal-body">
        <div class="row">
        <div class="form-group">
            <label class="col-md-3 control-label"><?php echo lang('promotions_url_label') ?></label>
            <div class="col-md-9">
                <input type="text" name="keyword" class="form-control" name="url" placeholder="Contoh: promosi-hari-ini" autocomplete="off" value="<?php echo ($url) ? $url->keyword : '' ?>">
            </div>
        </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>