<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('promotions_add_heading'); ?></h2>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('settings/promotions'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('promotions_list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('settings/promotions/save'); ?>" class="form-horizontal" method="post" id="form">
                    <input type="hidden" name="id" id="id_countdown" value="<?php echo ($data) ? $data->id : '' ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('promotions_form_name_label') ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" placeholder="<?php echo lang('promotions_form_name_placeholder') ?>" value="<?php echo ($data) ? $data->name : '' ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('promotions_image_label') ?></label>
                                <div id="image_container">
                                    <?php if($data && $data->image != '') : ?>
                                        <div class="col-sm-3" id="image-preview">
                                            <div class="thumbnail">
                                                <div class="thumb">
                                                    <img src="<?php echo base_url('../files/images/' . $data->image); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <input type="hidden" name="gambar" id="image" value="<?= ($data) ? (($data->image != null) ? $data->image : '') : '' ?>">
                                </div>
                                <div class="col-sm-6" id="add-image">
                                    <button type="button" id="upload-image-btn" class="btn btn-default" onclick="upload_image_btn()" <?php echo ($data && $data->image != '') ? 'style="display:none;"' : '' ?>><?php echo lang('button_add_image'); ?></button>
                                    <button type="button" id="btn-delete-image" class="btn btn-default" <?= (!$data && $data->image == '') ? 'style="display: none;"' : '' ?>>Hapus</button>
                                    <!-- <input type="hidden" id="image" name="gambar" value="<?php echo ($data) ? (($data->image != null) ? $data->image : '') : '' ?>"> -->
                                    <span>Ukuran yang disarankan: 1188×80</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('promotions_color_label') ?></label>
                                <div class="col-md-1">
                                    <input type="color" style="height: 50px;" class="form-control" name="color" required="" value="<?php echo ($data) ? $data->color : '' ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('promotions_url_label') ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="url" placeholder="<?= lang('promotions_url_placeholder') ?>" autocomplete="off" value="<?php echo ($data) ? $data->url : '' ?>">
                                    <span><i><?= lang('promotions_url_messages') ?></i></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('promotions_message_countdown_label') ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="countdown_message" autocomplete="off" placeholder="<?php echo lang('promotions_countmessage_placeholder') ?>" value="<?php echo ($data) ? $data->countdown_message : '' ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('promotions_count_down_label') ?></label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control datetimepicker" name="date_start" autocomplete="off" placeholder="<?php echo lang('promotions_date_start_placeholder') ?>" value="<?php echo ($data) ? $data->date_start : '' ?>">
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control datetimepicker" name="date_end" autocomplete="off" placeholder="<?php echo lang('promotions_date_end_placeholder') ?>" value="<?php echo ($data) ? $data->date_end : '' ?>">
                                </div>
                            </div>
                            <!-- <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('promotions_url_label') ?></label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="seo_url" value="<?= ($data) ? $data->url : '' ?>" placeholder="Masukkan URL Promosi">
                                </div>
                            </div> -->
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('promotions_status_label') ?></label>
                                <div class="col-md-9">
                                    <input type="checkbox" name="status" value="1" data-on-text="<?php echo lang('enabled'); ?>" data-off-text="<?php echo lang('disabled'); ?>" class="switch" <?php echo ($data) ? (($data->status == 1) ? 'checked' : '') : 'checked'; ?>>
                                </div>
                            </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('settings/promotions'); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<input type="file" name="countdown_image" id="countdown_image" style="display: none;">
<!-- <div id="filemanager" class="modal" data-backdrop="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo lang('filemanager'); ?></h5>
            </div>

            <div class="modal-body">
                <iframe  width="100%" height="550" frameborder="0" src="<?php echo base_url('filemanager/dialog.php?type=1&editor=false&field_id=image&relative_url=1'); ?>"></iframe>
            </div>
        </div>
    </div>
</div> -->