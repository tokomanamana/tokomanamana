<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo ($data) ? lang('group_edit_heading') : lang('group_add_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('settings/principle_users/groups'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('group_list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('settings/principle_users/group_save'); ?>" method="post" class="form-horizontal" id="form">
                    <input type="hidden" name="id" value="<?php echo ($data) ? $data->id : ''; ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('group_name'); ?></label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" required="" name="name" value="<?php echo ($data) ? $data->name : ''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('group_definition'); ?></label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="definition" value="<?php echo ($data) ? $data->definition : ''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('group_permissions'); ?></label>
                                <div class="col-md-5">
                                    <div class="well pt-5 pb-5" style="max-height: 200px; overflow: auto;">
                                        <?php
                                        foreach ($permissions->result() as $permission) {
                                            echo '<div class="checkbox"><label><input type="checkbox" name="permissions[]" value="' . $permission->id . '" ' . ($data ? in_array($permission->id, $perm_exist) ? 'checked' : '' : '') . '> ' . $permission->name . '</label></div>';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('settings/principle_users/groups'); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>