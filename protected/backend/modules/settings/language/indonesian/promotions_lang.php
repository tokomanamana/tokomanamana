<?php 

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['enabled'] = 'Aktif';
$lang['disabled'] = 'Nonaktif';

$lang['heading'] = 'Pengaturan Countdown Promosi';
$lang['promotions_name_th'] = 'Nama Promosi';
$lang['promotions_date_start_th'] = 'Mulai Count Down';
$lang['promotions_date_end_th'] = 'Selesai Count Down';
$lang['promotions_color_th'] = 'Warna';
$lang['promotions_status_th'] = 'Status';
$lang['promotions_url_th'] = 'Link Promo';
$lang['actions_th'] = 'Opsi';
$lang['promotions_add_heading'] = 'Tambah Promosi';
$lang['promotions_edit_heading'] = 'Edit Promosi';
$lang['promotions_list_heading'] = 'Daftar Promosi';

$lang['promotions_form_name_label'] = 'Nama Promosi';
$lang['promotions_form_name_placeholder'] = 'Masukkan Nama Promosi';
$lang['promotions_count_down_label'] = 'Pengaturan Count Down';
$lang['promotions_date_start_placeholder'] = 'Masukkan Tanggal Mulai Count Down';
$lang['promotions_date_end_placeholder'] = 'Masukkan Tanggal Berakhir Count Down';
$lang['promotions_color_label'] = 'Warna Background Promosi';
$lang['promotions_image_label'] = 'Gambar';
$lang['promotions_status_label'] = 'Status';
$lang['promotions_url_label'] = 'Link Promosi';
$lang['promotions_message_countdown_label'] = 'Pesan dalam Countdown';
$lang['promotions_countmessage_placeholder'] = 'Masukkan pesan dalam countdown';
$lang['promotions_url_placeholder'] = 'https://tokomanamana.com/promo-hari-ini';

$lang['promotions_url_messages'] = 'Biarkan kosong jika link sama dengan link promosi';

$lang['promotions_save_success_message'] = "Promotions '%s' berhasil disimpan.";
$lang['promotions_save_error_message'] = "Promotions '%s' gagal disimpan.";
$lang['promotions_delete_success_message'] = "Promotions '%s' berhasil dihapus.";
$lang['promotions_delete_error_message'] = "Promotions '%s' gagal dihapus.";

$lang['promotions_save_failed_date_message'] = 'Tanggal Countdown Promosi Sudah Ada!';

$lang['button_add'] = 'Tambah';
$lang['button_add_image'] = 'Unggah';