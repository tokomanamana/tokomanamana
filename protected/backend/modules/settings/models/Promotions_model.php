<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promotions_model extends CI_Model {

	function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->_where_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->limit($length, $start);

        return $this->db->get('setting_promotions');
    }

    private function _where_like($search = '') {
        $columns = array('name', 'show','color','date_start','date_end', 'image');
        if ($search) {
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
        }
    }

    private function _get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'name';
                break;
            case 1: $key = 'date_start';
                break;
            case 2: $key = 'date_end';
                break;
            case 3: $key = 'status';
                break;
            case 4: $key = 'color';
                break;
            case 5: $key = 'countdown_message';
                break;
            case 6: $key = 'url';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->_where_like($search);
        return $this->db->count_all_results('setting_promotions');
    }

}