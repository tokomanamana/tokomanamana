<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Groups_model extends CI_Model {

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->where_like($search);
        if ($order) {
            $order['column'] = $this->get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->limit($length, $start);

        return $this->db->get('aauthm_groups');
    }

    function get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'name';
                break;
            case 1: $key = 'definition';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->where_like($search);
        return $this->db->count_all_results('aauthm_groups');
    }

    function where_like($search = '') {
        $columns = array('name', 'definition');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
            $this->db->group_end();
        }
    }

    function create_group($group, $definition) {
        $check = $this->main->get('aauthm_groups', ['name' => $group]);
        if($check) {
            return false;
        } else {
            $data = [
                'name' => $group,
                'definition' => $definition
            ];
            $this->db->insert('aauthm_groups', $data);
            return $this->db->insert_id();
        }
    }

    function delete_group($id) {
        $check = $this->main->get('aauthm_groups', ['id' => $id]);
        if($check) {

            $this->db->where('group_id', $id);
            $this->db->delete('aauthm_user_to_group');

            $this->db->where('group_id', $id);
            $this->db->delete('aauthm_perm_to_group');

            $this->db->where('group_id', $id);
            $this->db->delete('aauthm_group_to_group');

            $this->db->where('subgroup_id', $id);
            $this->db->delete('aauthm_group_to_group');

            $this->db->where('id', $id);
            $this->db->delete('aauthm_groups');

            return true;

        } else {
            return false;
        }
    }

}
