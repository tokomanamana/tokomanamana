<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merchant_users extends CI_Controller {

	public function __construct() {
        parent::__construct();

        $this->aauth->control('setting/merchant_users');

        $this->lang->load('users', settings('language'));
        $this->load->model('users/merchant/users_model', 'users');
        $this->load->model('users/merchant/groups_model', 'groups');
        $this->load->model('users/merchant/permissions_model', 'permissions');

        $this->data['menu'] = 'setting_merchant_user';
    }

    public function index() {
        $this->breadcrumbs->unshift(lang('setting'), '/');
        $this->breadcrumbs->push(lang('merchant_user'), '/settings/users_merchant');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->data['menu'] = 'setting_merchant_user_user';

        $this->template->_init();
        $this->template->table();
        $this->output->set_title(lang('user_merchant_heading'));
        $this->load->view('users/merchant/list', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->users->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->username,
                    $data->email,
                    $data->group_name,
                    $data->merchant_name,
                    '<td class="text-center">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="' . site_url('settings/merchant_users/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>
                                </ul>
                            </li>
                        </ul>
                    </td>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->users->count_all();
        $output['recordsFiltered'] = $this->users->count_all($search);
        echo json_encode($output);
    }

    public function form($id) {
        if($id == null) {
            redirect('setting/merchant_users','refresh');
        }

        $id = decode($id);

        $this->data['groups'] = $this->main->gets('aauthm_groups');
        $this->data['merchants'] = $this->main->gets('merchants');
        $this->data['data'] = array();
        $this->data['data'] = $this->main->get('aauthm_users', array('id' => $id));
        $this->data['data']->group = $this->main->gets('aauthm_user_to_group', array('user_id' => $id))->result();
        $this->data['data']->merchant = $this->main->gets('merchants', array('id' => $this->data['data']->merchant_id));

        $this->breadcrumbs->unshift(lang('setting'), '/');
        $this->breadcrumbs->push(lang('user'), '/settings/merchant_users');
        $this->breadcrumbs->push(($this->data['data']) ? lang('user_edit_heading') : lang('user_add_heading'), '/settings/merchant_users/form/'.$id);
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->template->_init();
        $this->template->form();
        $this->output->set_title(($this->data['data']) ? lang('user_edit_heading') : lang('user_add_heading'));
        $this->load->view('users/merchant/form', $this->data);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('email', 'lang:email', 'trim|required|valid_email');
        $this->form_validation->set_rules('username', 'lang:username', 'trim|required');
        $this->form_validation->set_rules('group', 'lang:group', 'trim|required');
        $this->form_validation->set_rules('merchant', 'lang:merchant', 'trim');
        $this->form_validation->set_rules('password', 'lang:password', 'trim');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);
            $id = decode($data['id']);

            $datas = array(
                'id' => $id,
                'email' => $data['email'],
                'username' => $data['username'],
                'pass' => password_hash($data['password'], PASSWORD_BCRYPT),
                'merchant_id' => $data['merchant']
            );

            $save = $this->main->update('aauthm_users', $datas, array('id' => $id));
            $this->db->set('group_id', $data['group']);
            $this->db->where('user_id', $id);
            $this->db->update('aauthm_user_to_group');

            if (!$save) {
                $return = array('message' => $this->aauth->print_errors(), 'status' => 'error');
            } else {
                $return = array('message' => sprintf(lang('save_success'), $data['username']), 'status' => 'success', 'redirect' => site_url('settings/merchant_users'));
            }
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function groups($act = '', $id = '') {
        $this->data['menu'] = 'setting_merchant_user_group';
        
        $this->breadcrumbs->unshift(lang('setting'), '/');
        $this->breadcrumbs->push(lang('user'), '/settings/merchant_users');

        if ($act == 'form') {
            $this->group_form($id);
        } elseif ($act == 'get_list') {
            $this->group_list();
        } else {
            $this->breadcrumbs->push(lang('user_group'), '/settings/merchant_users/groups');
            $this->data['breadcrumbs'] = $this->breadcrumbs->show();
            
            $this->template->_init();
            $this->template->table();
            $this->output->set_title(lang('group_principle_heading'));
            $this->load->view('users/merchant/groups/list', $this->data);
        }
    }

    public function group_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->groups->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->name,
                    $data->definition,
                    '<td class="text-center">
                    <ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="' . site_url('settings/merchant_users/groups/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>
                    <li><a href="' . site_url('settings/merchant_users/group_delete/' . $data->id) . '" class="delete">' . lang('button_delete') . '</a></li>
                    </ul>
                    </li>
                    </ul>
                    </td>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->groups->count_all();
        $output['recordsFiltered'] = $this->groups->count_all($search);
        echo json_encode($output);
    }

    public function group_form($id = '') {
        $this->data['data'] = array();
        if ($id) {
            $id = decode($id);
            $this->data['data'] = $this->main->get('aauthm_groups', array('id' => $id));
            $this->data['perm_exist'] = array();
            $permissions = $this->main->gets('aauthm_perm_to_group', array('group_id' => $id));
            if ($permissions) {
                foreach ($permissions->result() as $perm) {
                    array_push($this->data['perm_exist'], $perm->perm_id);
                }
            }
        }
        $this->data['permissions'] = $this->main->gets('aauthm_perms', array(), 'name asc');
        $this->breadcrumbs->push(lang('user_permission'), '/settings/merchant_users/permissions');
        $this->breadcrumbs->push(($this->data['data']) ? lang('permission_edit_heading') : lang('permission_add_heading'), '/settings/merchant_users/permissions/'.$id);
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->template->_init();
        $this->template->form();
        $this->output->set_title(($this->data['data']) ? lang('group_edit_heading') : lang('group_add_heading'));
        $this->load->view('users/merchant/groups/form', $this->data);
    }

    public function group_save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'lang:name', 'trim|required|seo_url');
        $this->form_validation->set_rules('definition', 'lang:definition', 'trim');
        $this->form_validation->set_rules('permissions', 'lang:permissions', 'trim');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);

            if(!isset($data['permissions']) || !$data['permissions'] || $data['permissions'] == '') {
                $return = ['message' => 'Bidang Permission Harus Diisi!', 'status' => 'error'];
            } else {
                if (!$data['id']) {
                    // $save = $this->aauth->create_group($data['name'], $data['definition']);
                    $save = $this->groups->create_group($data['name'], $data['definition']);
                    if (!$save) {
                        // $return = array('message' => $this->aauth->print_infos(), 'status' => 'error');
                        $return = ['message' => 'Nama Group Sudah Ada', 'status' => 'error'];
                    } else {
                        $permissions = array_filter($data['permissions']);
                        if ($permissions) {
                            $data_perm = array();
                            foreach ($permissions as $perm) {
                                array_push($data_perm, array('perm_id' => $perm, 'group_id' => $save));
                            }
                            $this->db->insert_batch('aauthm_perm_to_group', $data_perm);
                        }
                        $return = array('message' => sprintf(lang('save_success'), lang('group') . ' ' . $data['name']), 'status' => 'success', 'redirect' => site_url('settings/merchant_users/groups'));
                    }
                } else {
                    $this->main->update('aauthm_groups', array('name' => $data['name'], 'definition' => $data['definition']), array('id' => $data['id']));
                    $this->main->delete('aauthm_perm_to_group', array('group_id' => $data['id']));
                    $permissions = array_filter($data['permissions']);
                    if ($permissions) {
                        $data_perm = array();
                        foreach ($permissions as $perm) {
                            array_push($data_perm, array('perm_id' => $perm, 'group_id' => $data['id']));
                        }
                        $this->db->insert_batch('aauthm_perm_to_group', $data_perm);
                    }
                    $return = array('message' => sprintf(lang('save_success'), lang('group') . ' ' . $data['name']), 'status' => 'success', 'redirect' => site_url('settings/merchant_users/groups'));
                }
            }
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function group_delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        // $delete = $this->aauth->delete_group($id);
        $delete = $this->groups->delete_group($id);
        if ($delete) {
            $return = array('message' => sprintf(lang('delete_success'), lang('group')), 'status' => 'success');
        } else {
            $return = array('message' => $this->aauth->print_infos(), 'status' => 'error');
        }

        echo json_encode($return);
    }

    public function permissions($act = '', $id = '') {
        $this->data['menu'] = 'setting_merchant_user_permission';

        $this->breadcrumbs->unshift(lang('setting'), '/');
        $this->breadcrumbs->push(lang('user'), '/settings/merchant_users');

        if ($act == 'form') {
            $this->permission_form($id);
        } elseif ($act == 'get_list') {
            $this->permission_list();
        } else {
            $this->breadcrumbs->push(lang('user_permission'), '/settings/merchant_users/permissions');
            $this->data['breadcrumbs'] = $this->breadcrumbs->show();
            $this->template->_init();
            $this->template->table();
            $this->output->set_title(lang('permission_heading'));
            $this->load->view('users/merchant/permissions/list', $this->data);
        }
    }

    public function permission_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->permissions->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->name,
                    $data->definition,
                    '<td class="text-center">
                    <ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="' . site_url('settings/merchant_users/permissions/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>
                    <li><a href="' . site_url('settings/merchant_users/permission_delete/' . $data->id) . '" class="delete">' . lang('button_delete') . '</a></li>
                    </ul>
                    </li>
                    </ul>
                    </td>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->permissions->count_all();
        $output['recordsFiltered'] = $this->permissions->count_all($search);
        echo json_encode($output);
    }

    public function permission_form($id = '') {
        $this->data['data'] = array();
        if ($id) {
            $id = decode($id);
            $this->data['data'] = $this->main->get('aauthm_perms', array('id' => $id));
        }
        $this->breadcrumbs->push(lang('user_permission'), '/settings/merchant_users/permissions');
        $this->breadcrumbs->push(($this->data['data']) ? lang('permission_edit_heading') : lang('permission_add_heading'), '/settings/merchant_users/permissions/'.$id);
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->template->_init();
        $this->template->form();
        $this->output->set_title(($this->data['data']) ? lang('permission_edit_heading') : lang('permission_add_heading'));
        $this->load->view('users/merchant/permissions/form', $this->data);
    }

    public function permission_save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'lang:name', 'trim|required|seo_url');
        $this->form_validation->set_rules('definition', 'lang:definition', 'trim');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);

            if (!$data['perm']) {
                // $save = $this->aauth->create_perm($data['name'], $data['definition']);
                $save = $this->permissions->create_perm($data['name'], $data['definition']);
                if (!$save) {
                    $return = array('message' => 'Hak Akses Sudah Ada', 'status' => 'error');
                } else {
                    $return = array('message' => sprintf(lang('save_success'), lang('permission') . ' ' . $data['name']), 'status' => 'success', 'redirect' => site_url('settings/merchant_users/permissions'));
                }
            } else {
                // $this->aauth->update_perm($data['perm'], $data['name'], $data['definition']);
                $this->permissions->update_perm($data['perm'], $data['name'], $data['definition']);
                $return = array('message' => sprintf(lang('save_success'), lang('permission') . ' ' . $data['name']), 'status' => 'success', 'redirect' => site_url('settings/merchant_users/permissions'));
            }
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function permission_delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        // $delete = $this->aauth->delete_perm($id);
        $delete = $this->permissions->delete_perms($id);
        if (!$delete) {
            $return = array('message' => 'Something went wrong! Call your administrator.', 'status' => 'error');
        } else {
            $return = array('message' => sprintf(lang('save_success'), lang('permission')), 'status' => 'success');
        }

        echo json_encode($return);
    }

}
