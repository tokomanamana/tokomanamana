<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promotions extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->aauth->control('setting/promotions');
        $this->load->model('Promotions_model', 'promo');
        $this->load->model('Model');

        $this->data['menu'] = 'setting_promo';
        $this->lang->load('promotions', settings('language'));
    }

    public function index() {
    	$this->template->_init();
        $this->template->table();
        $this->template->form();

        $this->breadcrumbs->unshift(lang('setting'), '/');
        $this->breadcrumbs->push(lang('setting_promotions'), '/settings/promotions');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->data['url'] = $this->main->get('seo_url', ['query' => 'catalog/promotion']);

        $this->output->set_title('Pengaturan Promo');
        $this->load->js('../assets/backend/js/modules/settings/promotions.js');
        $this->load->css('../assets/backend/css/jquery.treeSelector.css');
        $this->load->js('../assets/backend/js/jquery.treeSelector.js');
        $this->load->js('../assets/backend/js/datetimepicker.min.js');
        $this->load->css('../assets/backend/css/plugins/datetimepicker.min.css');
        $this->load->view('promotions/view', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->promo->get_all($start, $length, $search, $order);
        $url_promo = $this->main->get('seo_url', ['query' => 'catalog/promotion']);
        $url = 'tokomanamana.com/' . $url_promo->keyword;
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->name,
                    $data->date_start,
                    $data->date_end,
                    $this->status($data->status),
                    $this->color($data->color),
                    $data->countdown_message,
                    ($data->url == '' ? $url : $data->url),
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="' . site_url('settings/promotions/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>
                    <li><a href="' . site_url('settings/promotions/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>
                    </ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->promo->count_all();
        $output['recordsFiltered'] = $this->promo->count_all($search);
        echo json_encode($output);
    }

    public function save() {
    	$this->input->is_ajax_request() or exit('No direct post submit allowed!');

    	$this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'lang:promotions_form_name_label', 'trim|required');
    	$this->form_validation->set_rules('color', 'lang:promotions_color_label', 'trim|required');
    	$this->form_validation->set_rules('date_start', 'Tanggal mulai count down', 'trim|required');
    	$this->form_validation->set_rules('date_end', 'Tanggal berakhir count down', 'trim|required');
    	$this->form_validation->set_rules('gambar', 'Gambar Dibutuhkan', 'trim|required');

    	if ($this->form_validation->run() === TRUE) {
    		$data = $this->input->post(null, true);
            $date_start = $data['date_start'];
            $date_end = $data['date_end'];

            if($data['id']) {
                $this->db->where('status', 1);
                $this->db->where('id !=', $data['id']);
                $all_promotions = $this->db->get('setting_promotions');
            } else {
                $all_promotions = $this->main->gets('setting_promotions', ['status' => 1]);
            }
            
            $check_date = 0;

            if($all_promotions) {
                foreach($all_promotions->result() as $promotion) {
                    if(($promotion->date_start <= $date_start && $promotion->date_end > $date_start) || ($promotion->date_start < $date_end && $promotion->date_end >= $date_end)) {
                        $check_date++;
                    }
                }
            } else {
                $check_date = 0;
            }

            if($check_date > 0) {
                $return = ['message' => lang('promotions_save_failed_date_message'), 'status' => 'error'];
            } else {
                if(!isset($data['status'])) {
                    $data['status'] = 0;
                }
                
                $data_insert = [
                    'name' => $data['name'],
                    'date_start' => $date_start,
                    'date_end' => $date_end,
                    'color' => $data['color'],
                    'image' => $data['gambar'],
                    'url' => $data['url'],
                    'status' => $data['status'],
                    'countdown_message' => $data['countdown_message']
                ];

                if($data['id']) {
                 $this->db->update('setting_promotions', $data_insert, ['id' => $data['id']]);
                } else {
                 $this->db->insert('setting_promotions', $data_insert);
                }

                // $this->db->update('seo_url', ['keyword' => $data['url']], ['query' => 'home/promo']);

                $return = array('message' => sprintf(lang('promotions_save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('settings/promotions'));
            }
    	} else {
    		$return = array('message' => validation_errors(), 'status' => 'error');
    	}

    	echo json_encode($return);
    }

    public function save_url() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('keyword', 'lang:promotions_url_label', 'trim|required');

        if ($this->form_validation->run() == TRUE) {
            $data = $this->input->post(null, true);
            if($data['query']) {
                if(check_url($data['keyword'], $data['query'])) {
                    $return = array('message' => 'URL Sudah digunakan', 'status' => 'error');
                    echo json_encode($return);
                    exit();
                }
                $this->main->update('seo_url', ['keyword' => $data['keyword']], ['query' => $data['query']]);
                $return = [
                    'message' => 'URL Berhasil disimpan!',
                    'status' => 'success',
                    'redirect' => site_url('settings/promotions')
                ];
            } else {
                $query = 'catalog/promotion';
                if(check_url($data['keyword'], $query)) {
                    $return = array('message' => 'URL Sudah digunakan', 'status' => 'error');
                    echo json_encode($return);
                    exit();
                }
                $data_seo_url = [
                    'query' => $query,
                    'keyword' => $data['keyword']
                ];
                $this->main->insert('seo_url', $data_seo_url);
                $count_down = $this->main->gets('setting_promotions');
                if($count_down) {
                    foreach($count_down->result() as $count) {
                        $this->main->update('setting_promotions', ['url' => $data['keyword']], ['id' => $count->id]);
                    }
                } 
                $return = [
                    'message' => 'URL Berhasil disimpan!',
                    'status' => 'success',
                    'redirect' => site_url('settings/promotions')
                ];
            }
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function form($id = '') {
        $this->template->_init();
        $this->template->form();

        $this->data['data'] = array();
        if ($id) {
            $id = decode($id);
            $this->data['data'] = $this->main->get('setting_promotions', array('id' => $id));
        }

        $this->breadcrumbs->unshift(lang('setting'), '/');
        $this->breadcrumbs->push(lang('setting_promotions'), 'settings/promotions');
        $this->breadcrumbs->push(($this->data['data']) ? lang('promotions_edit_heading') : lang('promotions_add_heading'), '/');
        $this->data['url'] = $this->main->get('seo_url', ['query' => 'home/promo']);

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(($this->data['data']) ? lang('promotions_edit_heading') : lang('promotions_add_heading'));
        $this->load->js('../assets/backend/js/modules/settings/promotions.js');
        $this->load->js('../assets/backend/js/datetimepicker.min.js');
        $this->load->css('../assets/backend/css/plugins/datetimepicker.min.css');
        $this->load->view('promotions/form', $this->data);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $data = $this->main->get('setting_promotions', array('id' => $id));

        $delete = $this->main->delete('setting_promotions', array('id' => $id));
        if ($delete) {
            if($data->image) {
                $image_countdown = FCPATH . '../files/images/' . $data->image;
                if(file_exists($image_countdown)) {
                    unlink($image_countdown);
                }
            }
            $return = array('message' => sprintf(lang('promotions_delete_success_message'), $data->name), 'status' => 'success');
        } else {
            $return = array('message' => lang('promotions_delete_error_message'), 'status' => 'error');
        }

        echo json_encode($return);
    }

    private function color($string) {
    	$color = '<div style="width:20px;height:20px;background:' . $string . ';display:block;"></div>';
    	return $color;
    }

    private function status($show) {
    	if($show == 1) {
    		$output = '<span style="display:inline-block;color:white;background:#5cb85c; padding: 3px 5px;border-radius:3px;">Aktif</span>';
    	} else {
    		$output = '<span style="display:inline-block;color:white;background:#d9534f; padding: 3px 5px;border-radius:3px;">Tidak Aktif</span>';
    	}
    	return $output;
    }

    public function image() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $data = $this->input->post(null, true);
        $return = [];
        $config['upload_path']          = FCPATH.'../files/images/banners/';
        $config['allowed_types']        = 'jpg|jpeg|png';
        $config['file_name']            = $data['image_name'];
        $config['overwrite']            = true;
        $config['max_size']             = 1024; //1MB

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('countdown_image'))
        {
            $error = $this->upload->display_errors();
            $return = [
                'status' => 'error',
                'message' => $error
            ];
        }
        else
        {   
            $return = [
                'status' => 'success',
                'message' => 'banners/' . $config['file_name']
            ];
        }
        echo json_encode($return);
    }

    public function delete_image() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $data = $this->input->post(null, true);
        $image = FCPATH . '../files/images/' . $data['image_name'] ;
        
        if(file_exists($image)) {
            unlink($image);
        }
        echo 'success';
    }

}