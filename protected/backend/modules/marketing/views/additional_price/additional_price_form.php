<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo ($data) ? lang('additional_price_edit_heading') : lang('additional_price_add_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('marketing/additional_price'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('additional_price_list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('marketing/additional_price/save'); ?>" class="form-horizontal" method="post" id="form">
                    <input type="hidden" name="id" value="<?php echo ($data) ? encode($data->id) : ''; ?>">
                    <input type="hidden" id="decodeid" value="<?php echo ($data) ? $data->id : ''; ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('additional_price_form_convenience_label'); ?> (%)</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="convenience" value="<?php echo ($data) ? $data->convenience : ''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('additional_price_form_sanitizer_label'); ?></label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control number" name="sanitizer" value="<?php echo ($data) ? $data->sanitizer : ''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('additional_price_form_express_label'); ?></label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control number" name="express" value="<?php echo ($data) ? $data->express : ''; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('marketing/additional_price'); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
