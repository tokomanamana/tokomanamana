<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('coupon_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('marketing/coupons/form'); ?>" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span><?php echo lang('coupon_add_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <table class="table table-hover" id="table" data-url="<?php echo site_url('marketing/coupons/get_list'); ?>">
                <thead>
                    <tr>
                        <th class="default-sort" data-sort="asc"><?php echo lang('coupon_name_th'); ?></th>
                        <th><?php echo lang('coupon_code_th'); ?></th>
                        <th><?php echo lang('coupon_customer_name_th') ?></th>
                        <th><?php echo lang('coupon_discount_th'); ?></th>
                        <th><?php echo lang('coupon_date_start_th'); ?></th>
                        <th><?php echo lang('coupon_date_end_th'); ?></th>
                        <th style="width: 45px;" class="text-center"><?php echo lang('coupon_status_th'); ?></th>
                        <th style="width: 45px;" class="text-center"><?php echo lang('coupon_show_th') ?></th>
                        <th style="width: 45px;" class="text-center"><?php echo lang('coupon_event_th') ?></th>
                        <th class="no-sort text-center" style="width: 20px;"><?php echo lang('actions_th'); ?></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="couponModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="stockModalLabel">Kupon</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="responsive-table">
                <table class="table table-bordered" style="margin-bottom:2em;width:100%;" id="coupon_table" >
                    <thead>
                        <tr>
                            <th>No Order</th>
                            <th>Nama Customer</th>
                            <th>Tanggal</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
      </div>
    </div>
  </div>
</div>