<style>
    .checkbox {
        padding-top: 0px !important;
    }
    .checkbox label {
        padding-top: 7px;
        padding-bottom: 7px;
        display: block;
    }
    .checkbox label:hover {
        background: #F1F1F1;
        border-radius: 3px;
        width: 100%;
    }
</style>
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo ($data) ? lang('coupon_edit_heading') : lang('coupon_add_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('marketing/coupons'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('coupon_list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('marketing/coupons/save'); ?>" class="form-horizontal" method="post" id="form">
                    <input type="hidden" name="id" value="<?php echo ($data) ? encode($data->id) : ''; ?>">
                    <input type="hidden" id="decodeid" value="<?php echo ($data) ? $data->id : ''; ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('coupon_form_name_label'); ?></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" required="" name="name" value="<?php echo ($data) ? $data->name : ''; ?>" placeholder="<?= lang('coupon_form_name_placeholder') ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?= lang('coupon_form_description_label') ?></label>
                                <div class="col-md-9">
                                    
                                    <textarea class="form-control" id="deskripsi" required name="deskripsi" placeholder="<?= lang('coupon_description_form_placeholder') ?>"><?=($data ? $data->deskripsi : '')?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('coupon_form_code_label'); ?></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" required="" name="code" value="<?php echo ($data) ? $data->code : ''; ?>" placeholder="<?= lang('coupon_form_code_placeholder') ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('coupon_form_type_label'); ?></label>
                                <div class="col-md-3">
                                    <select class="form-control bootstrap-select" name="type" required="">
                                        <option value="F" <?php if(($data ? $data->type : '') == 'F'){ echo "selected";}?>>Fixed Amount</option>
                                        <option value="P"  <?php if(($data ? $data->type : '') == 'P'){ echo "selected";}?>>Percentage</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?= lang('coupon_form_coupon_type_label') ?></label>
                                <div class="col-md-3">
                                    <select class="form-control bootstrap-select" name="coupon_type" required="">
                                        <option value="all" <?php if(($data ? $data->coupon_type : '') == 'all'){ echo "selected";}?>>All</option>
                                        <option value="branch" <?php if(($data ? $data->coupon_type : '') == 'branch'){ echo "selected";}?>>Branch</option>
                                        <option value="merchant" <?php if(($data ? $data->coupon_type : '') == 'merchant'){ echo "selected";}?>>Merchant</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?= lang('coupon_form_autofill_label') ?></label>
                                <div class="col-md-9">
                                    <input type="checkbox" name="autofill" value="1" data-on-text="<?php echo lang('yes'); ?>" data-off-text="<?php echo lang('no'); ?>" class="switch" <?php echo ($data) ? (($data->autofill == 1) ? 'checked' : '') : 'checked'; ?>>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?= lang('coupon_expedition_label') ?></label>
                                <div class="col-md-9">
                                    <input type="checkbox" name="ekspedisi" value="1" data-on-text="<?php echo lang('yes'); ?>" data-off-text="<?php echo lang('no'); ?>" class="switch" <?php echo ($data) ? (($data->ekspedisi == 1) ? 'checked' : '') : 'checked'; ?>>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?= lang('coupon_apply_label') ?></label>
                                <div class="col-md-3">
                                    <select class="form-control bootstrap-select" name="apply" required="">
                                        <option value="0" <?php if(($data ? $data->apply : '') == '0'){ echo "selected";}?>>All</option>
                                        <option value="1" <?php if(($data ? $data->apply : '') == '1'){ echo "selected";}?>>Mobile</option>
                                        <option value="2" <?php if(($data ? $data->apply : '') == '2'){ echo "selected";}?>>Website</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Per Order/ Produk</label>
                                <div class="col-md-3">
                                    <select class="form-control bootstrap-select" name="for_product" required="">
                                        <option value="0" <?php if(($data ? $data->for_product : '') == '0'){ echo "selected";}?>> / Order</option>
                                        <option value="1"  <?php if(($data ? $data->for_product : '') == '1'){ echo "selected";}?>> / Product</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Pilih Berdasarkan</label>
                                <div class="col-md-3">
                                    <select id="select_type" name="select_type" class="bootstrap-select" required>
                                        <option value="">Pilih Berdasarkan</option>
                                        <option value="category" <?= ($data) ? ($data->catIds) ? 'selected' : '' : '' ?>>Kategori</option>
                                        <option value="product" <?= ($data) ? ($data->product_id) ? 'selected' : '' : '' ?>>Produk</option>
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" id="product_type" value="<?= ($data) ? ($data->product_id) ? "1" : "0" : "0" ?>">
                            <input type="hidden" id="category_type" value="<?= ($data) ? ($data->catIds) ? "1" : "0" : "0" ?>">
                            <div class="form-group category-wrapper">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-3">
                                    <div id="treeSelectorCat"></div>
                                    <input id="catIds" name="catIds" type="hidden" value="<?php echo ($data) ? $data->catIds : NULL; ?>">
                                </div>
                            </div>
                            <div class="form-group product-wrapper">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-3">
                                    <div id="treeSelectorProducts"></div>
                                    <input id="product_id" name="product_id" type="hidden" value="<?php echo ($data) ? $data->product_id : NULL; ?>">
                                </div>
                            </div>
                            <!-- <div class="form-group">
                                <label class="col-md-3 control-label">Zona</label>
                                <div class="col-md-3">
                                <select class="form-control" name="zonaIds[]" required="" multiple="">
                                    <?php 
                                        //$exp_zona = $data ? explode(',',$data->zonaIds) : array();
                                        //foreach ($zonas as $zona) {?>
                                        <option value="<?php echo $zona->id?>" <?php if (in_array($zona->id, $exp_zona)) { echo "selected";}?>><?php echo $zona->name?></option>
                                    <?php //} ?>
                                </select>
                                    <div id="treeSelectorZona"></div>
                                    <input id="zonaIds" name="zonaIds" type="hidden" value="<?php echo ($data) ? $data->zonaIds : []; ?>">
                                </div>
                            </div> -->
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?= lang('coupon_form_city_label') ?></label>
                                <div class="col-md-3">
                                    <div id="treeSelectorCity"></div>
                                    <input type="hidden" name="cityIds" id="cityIds" value="<?= ($data) ? $data->cityIds : '' ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('coupon_form_discount_label'); ?></label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control number" required="" name="discount" value="<?php echo ($data) ? $data->discount : ''; ?>" placeholder="<?= lang('coupon_form_discount_placeholder') ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('coupon_form_min_order_label'); ?></label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control number" name="min_order" value="<?php echo ($data) ? $data->min_order : ''; ?>" placeholder="<?= lang('coupon_form_min_order_label') ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('coupon_form_available_date_label'); ?></label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control date" name="date_start" value="<?php echo ($data) ? $data->date_start : ''; ?>" placeholder="<?php echo lang('coupon_form_date_start_placeholder'); ?>" autocomplete="off">
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control date" name="date_end" value="<?php echo ($data) ? $data->date_end : ''; ?>" placeholder="<?php echo lang('coupon_form_date_end_placeholder'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('coupon_form_use_per_coupon_label'); ?></label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control number" name="uses_total" value="<?php echo ($data) ? $data->uses_total : 1; ?>">
                                </div>
                                <span class="help-block"><?php echo lang('coupon_form_use_per_coupon_help'); ?></span>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('coupon_form_use_per_customer_label'); ?></label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control number" name="uses_customer" value="<?php echo ($data) ? $data->uses_customer : 1; ?>">
                                </div>
                                <span class="help-block"><?php echo lang('coupon_form_use_per_customer_help'); ?></span>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Gambar</label>
                                <div class="col-md-9">
                                    <?php
                                        $img_style = 'display: none';
                                        $img_src = '';
                                        if($data){
                                            $img_src = $data->gambar;
                                            if($data->gambar != ''){
                                                $img_style = '';
                                            }
                                        }
                                    ?>
                                    <img src="<?=$img_src?>" class="thumbnail" style="width: 240px; <?=$img_style?>" id="img">
                                    <button type="button" class="btn btn-info" onclick="chose_image()">Pilih Gambar</button>
                                    <input type="file" accept="image/*" id="gambar_input" placeholder="Point" class="form-control" style="display: none" onchange="change_image(this)">
                                    <input type="hidden" name="gambar" id="base64_image" value="<?=$img_src?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('coupon_form_status_label'); ?></label>
                                <div class="col-md-9">
                                    <input type="checkbox" name="status" value="1" data-on-text="<?php echo lang('yes'); ?>" data-off-text="<?php echo lang('no'); ?>" class="switch" <?php echo ($data) ? (($data->status == 1) ? 'checked' : '') : 'checked'; ?>>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('coupon_form_show_label'); ?></label>
                                <div class="col-md-9">
                                    <input type="checkbox" name="show" value="1" data-on-text="<?php echo lang('yes'); ?>" data-off-text="<?php echo lang('no'); ?>" class="switch" <?php echo ($data) ? (($data->show == 1) ? 'checked' : '') : 'checked'; ?>>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('coupon_form_event_label'); ?></label>
                                <div class="col-md-9">
                                    <input type="checkbox" name="event" value="1" data-on-text="<?php echo lang('yes'); ?>" data-off-text="<?php echo lang('no'); ?>" class="switch" <?php echo ($data) ? (($data->event == 1) ? 'checked' : '') : ''; ?>>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Tipe Event</label>
                                <div class="col-md-4">
                                    <select name="type_for" id="type_for" class="bootstrap-select">
                                        <option value="">Pilih tipe</option>
                                        <option value="customer">Customer</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('marketing/coupons'); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
