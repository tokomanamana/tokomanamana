<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reward_points_model extends CI_Model {

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->_where_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->limit($length, $start);

        return $this->db->get('coupons');
    }

    private function _get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'name';
                break;
            case 1: $key = 'code';
                break;
            case 2: $key = 'discount';
                break;
            case 3: $key = 'date_start';
                break;
            case 4: $key = 'date_end';
                break;
            case 5: $key = 'status';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->_where_like($search);
        return $this->db->count_all_results('coupons');
    }

    private function _where_like($search = '') {
        $columns = array('name', 'code','discount','date_start','date_end');
        if ($search) {
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
        }
    }

}
