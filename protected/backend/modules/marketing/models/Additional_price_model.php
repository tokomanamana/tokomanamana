<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Additional_price_model extends CI_Model {

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->_where_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->limit($length, $start);

        return $this->db->get('additional_price');
    }

    function get_user_all($start = 0, $length, $search = '', $order = array(), $id="") {
        $this->_where_additional_price_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_additional_price_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('c.fullname as customer,ch.date_added as date_added,o.code as order_code');
        $this->db->limit($length, $start);
        $this->db->join('customers c','c.id = ch.customer','left');
        $this->db->join('orders o','o.id = ch.order','left');
        $this->db->where('ch.additional_price',$id);
        return $this->db->get('additional_price_history ch');
        // $q = $this->db->query("SELECT * FROM additional_price_history where additional_price = $id 
        //                     UNION 
        //                 SELECT * FROM additional_price_history_merchant where additional_price = $id
        //                 ");
        // return $q;
    }

    private function _get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'convenience';
                break;
            case 1: $key = 'sanitizer';
                break;
            case 2: $key = 'express';
                break;
        }
        return $key;
    }

    private function _get_alias_additional_price_key($key) {
        switch ($key) {
            case 0: $key = 'customer';
                break;
            case 1: $key = 'date_added';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->_where_like($search);
        return $this->db->count_all_results('additional_price');
    }

    function count_all_user($search = '',$id="") {
        $this->_where_like($search);
        $this->db->select('c.fullname as customer,ch.date_added as date_added,o.code as order_code');
        $this->db->join('customers c','c.id = ch.customer','left');
        $this->db->join('orders o','o.id = ch.order','left');
        $this->db->where('ch.additional_price',$id);
        //$this->db->query("SELECT * FROM additional_price_history UNION SELECT * FROM additional_price_history_merchant");
        return $this->db->count_all_results('additional_price_history ch');
    }

    private function _where_like($search = '') {
        $columns = array('convenience', 'sanitizer','express');
        if ($search) {
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
        }
    }

    private function _where_additional_price_like($search = '') {
        $columns = array('customer','date_added');
        if ($search) {
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
        }
    }

}
