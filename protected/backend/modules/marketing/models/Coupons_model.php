<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Coupons_model extends CI_Model {

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->_where_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('coupons.*, customers.fullname as customer_name')->join('customers', 'customers.id = coupons.customer_id', 'left')->limit($length, $start);

        return $this->db->get('coupons');
    }

    function get_user_all($start = 0, $length, $search = '', $order = array(), $id="") {
        $this->_where_coupon_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_coupon_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('c.fullname as customer,ch.date_added as date_added,o.code as order_code');
        $this->db->limit($length, $start);
        $this->db->join('customers c','c.id = ch.customer','left');
        $this->db->join('orders o','o.id = ch.order','left');
        $this->db->where('ch.coupon',$id);
        return $this->db->get('coupon_history ch');
        // $q = $this->db->query("SELECT * FROM coupon_history where coupon = $id 
        //                     UNION 
        //                 SELECT * FROM coupon_history_merchant where coupon = $id
        //                 ");
        // return $q;
    }

    private function _get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'name';
                break;
            case 1: $key = 'code';
                break;
            case 2: $key = 'customers.fullname';
                break;
            case 3: $key = 'discount';
                break;
            case 4: $key = 'date_start';
                break;
            case 5: $key = 'date_end';
                break;
            case 6: $key = 'status';
                break;
            case 7: $key = 'show';
                break;
            case 8: $key = 'event';
                break;
        }
        return $key;
    }

    private function _get_alias_coupon_key($key) {
        switch ($key) {
            case 0: $key = 'customer';
                break;
            case 1: $key = 'date_added';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->_where_like($search);
        $this->db->select('coupons.*, customers.fullname as customer_name')->join('customers', 'customers.id = coupons.customer_id', 'left');
        return $this->db->count_all_results('coupons');
    }

    function count_all_user($search = '',$id="") {
        $this->_where_like($search);
        $this->db->select('c.fullname as customer,ch.date_added as date_added,o.code as order_code');
        $this->db->join('customers c','c.id = ch.customer','left');
        $this->db->join('orders o','o.id = ch.order','left');
        $this->db->where('ch.coupon',$id);
        //$this->db->query("SELECT * FROM coupon_history UNION SELECT * FROM coupon_history_merchant");
        return $this->db->count_all_results('coupon_history ch');
    }

    private function _where_like($search = '') {
        $columns = array('name', 'code','discount','date_start','date_end', 'customers.fullname');
        if ($search) {
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
        }
    }

    private function _where_coupon_like($search = '') {
        $columns = array('customer','date_added');
        if ($search) {
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
        }
    }

}
