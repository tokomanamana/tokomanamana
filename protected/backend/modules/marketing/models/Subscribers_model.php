<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Subscribers_model extends CI_Model {

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->_where_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->limit($length, $start);

        return $this->db->get('subscriptions');
    }

    private function _get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'email';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->_where_like($search);
        return $this->db->count_all_results('subscriptions');
    }

    private function _where_like($search = '') {
        $columns = array('email');
        if ($search) {
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
        }
    }

}
