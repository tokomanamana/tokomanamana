<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['additional_price'] = 'Harga Tambahan';
$lang['additional_price_heading'] = 'Harga Tambahan';
$lang['additional_price_list_heading'] = 'Daftar Harga Tambahan';
$lang['additional_price_add_heading'] = 'Tambah Harga Tambahan';
$lang['additional_price_edit_heading'] = 'Edit Harga Tambahan';

$lang['additional_price_convenience_th'] = 'Convenience';
$lang['additional_price_sanitizer_th'] = 'Sanitizer';
$lang['additional_price_express_th'] = 'Express';

$lang['additional_price_form_convenience_label'] = 'Harga Convenience';
$lang['additional_price_form_sanitizer_label'] = 'Harga Sanitize';
$lang['additional_price_form_express_label'] = 'Harga Kurir Express';
$lang['actions_th'] = 'Action';
$lang['additional_price_save_success_message'] = "Harga Tambahan '%s' berhasil disimpan.";
$lang['additional_price_save_error_message'] = "Harga Tambahan '%s' gagal disimpan.";
$lang['additional_price_delete_success_message'] = "Harga Tambahan '%s' berhasil dihapus.";
$lang['additional_price_delete_error_message'] = "Harga Tambahan '%s' gagal dihapus."; 