<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['coupon_heading'] = 'Kupon';
$lang['coupon_list_heading'] = 'Daftar Kupon';
$lang['coupon_add_heading'] = 'Tambah Kupon';
$lang['coupon_edit_heading'] = 'Edit Kupon';

$lang['coupon_name_th'] = 'Nama';
$lang['coupon_code_th'] = 'Kode';
$lang['coupon_discount_th'] = 'Diskon';
$lang['coupon_available_th'] = 'Ketersediaan';
$lang['coupon_date_start_th'] = 'Mulai';
$lang['coupon_date_end_th'] = 'Selesai';
$lang['coupon_status_th'] = 'Aktif';
$lang['coupon_show_th'] = 'Tampilkan';
$lang['coupon_event_th'] = 'Event';
$lang['coupon_customer_name_th'] = 'Pemilik Kupon';
$lang['actions_th'] = 'Opsi';

$lang['coupon_form_name_label'] = 'Nama Kupon';
$lang['coupon_form_code_label'] = 'Kode';
$lang['coupon_form_type_label'] = 'Jenis';
$lang['coupon_form_coupon_type_label'] = 'Tipe Kupon';
$lang['coupon_form_status_label'] = 'Status';
$lang['coupon_form_discount_label'] = 'Diskon';
$lang['coupon_form_min_order_label'] = 'Minimum Order';
$lang['coupon_form_available_date_label'] = 'Tanggal Ketersediaan';
$lang['coupon_form_use_per_coupon_label'] = 'Stok Kupon';
$lang['coupon_form_use_per_customer_label'] = 'Stok Per Pelanggan';
$lang['coupon_form_show_label'] = 'Tampilkan';
$lang['coupon_form_event_label'] = 'Event';
$lang['coupon_form_description_label'] = 'Deskripsi Kupon';
$lang['coupon_form_category_label'] = 'Kategori';
$lang['coupon_form_min_order_label'] = 'Minimal Order';
$lang['coupon_form_autofill_label'] = 'Autofill';
$lang['coupon_expedition_label'] = 'Ekspedisi';
$lang['coupon_apply_label'] = 'Apply';
$lang['coupon_form_city_label'] = 'Kota';

$lang['coupon_form_date_start_placeholder'] = 'Tanggal Mulai';
$lang['coupon_form_date_end_placeholder'] = 'Tanggal Selesai';
$lang['coupon_form_name_placeholder'] = 'Masukkan Nama Kupon';
$lang['coupon_description_form_placeholder'] = 'Masukkan Deskripsi Kupon';
$lang['coupon_form_code_placeholder'] = 'Masukkan Kode Kupon';
$lang['coupon_form_discount_placeholder'] = 'Masukkan Diskon';

$lang['coupon_form_use_per_coupon_help'] = 'Stok kupon yang bisa digunakan semua pelanggan, kosongkan jika tidak ada batasan';
$lang['coupon_form_use_per_customer_help'] = 'Stok kupon yang bisa digunakan satu pelanggan, kosongkan jika tidak ada batasan';

$lang['coupon_save_success_message'] = "Kupon '%s' berhasil disimpan.";
$lang['coupon_save_error_message'] = "Kupon '%s' gagal disimpan.";
$lang['coupon_delete_success_message'] = "Kupon '%s' berhasil dihapus.";
$lang['coupon_delete_error_message'] = "Kupon '%s' gagal dihapus.";