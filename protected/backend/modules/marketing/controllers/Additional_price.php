<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Additional_price extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->aauth->control('marketing/additional_price');
        $this->lang->load('additional_price', settings('language'));
        $this->load->model('additional_price_model', 'additional_price');
        $this->data['menu'] = 'marketing_additional_price';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('marketing'), '/');
        $this->breadcrumbs->push(lang('additional_price'), '/marketing/additional_price');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->load->js('../assets/backend/js/modules/marketing/additional_price.js');
        $this->output->set_title(lang('additional_price'));
        $this->load->view('additional_price/additional_price', $this->data);
    }

    public function form($id = '') {
        $this->template->_init();
        $this->template->form();

        $this->data['data'] = array();

        $this->breadcrumbs->unshift(lang('marketing'), '/');
        $this->breadcrumbs->push(lang('additional_price'), '/marketing/additional_price');
        if ($id) {
            $this->aauth->control('marketing/additional_price/edit');
            $id = decode($id);
            $this->data['data'] = $this->main->get('additional_price', array('id' => $id));
            // var_dump($this->data['data']); exit();
            $this->breadcrumbs->push(lang('additional_price_edit_heading'), '/marketing/additional_price/form/' . encode($id));
            $this->breadcrumbs->push('Harga Tambahan', '/');
        } else {
            $this->aauth->control('marketing/additional_price/add');
            $this->breadcrumbs->push(lang('additional_price_add_heading'), '/marketing/additional_price/form');
        }

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->load->css('../assets/backend/css/jquery.treeSelector.css');
        $this->load->js('../assets/backend/js/jquery.treeSelector.js');
        $this->load->js('../assets/backend/js/modules/design/additional_price.js');
        $this->output->set_title(($this->data['data']) ? lang('additional_price_edit_heading') : lang('additional_price_add_heading'));
        $this->load->view('additional_price/additional_price_form', $this->data);
    }
    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->additional_price->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->convenience.' %',
                    'Rp. '.number($data->sanitizer),
                    'Rp. '.number($data->express),
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">'.
                    ($this->aauth->is_allowed('marketing/additional_price/edit')?'<li><a href="' . site_url('marketing/additional_price/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>':'').
                    ($this->aauth->is_allowed('marketing/additional_price/delete')?'<li><a href="' . site_url('marketing/additional_price/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>':'').
                    '</ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->additional_price->count_all();
        $output['recordsFiltered'] = $this->additional_price->count_all($search);
        echo json_encode($output);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        // $this->load->library('form_validation');

        // if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);

            do {
                if ($data['id'])
                    $data['id'] = decode($data['id']);

                // $data['status'] = (isset($data['status']) && $data['status'] == 1) ? $data['status'] : 0;
                // $data['autofill'] = (isset($data['autofill']) && $data['autofill'] == 1) ? $data['autofill'] : 0;
                // $data['ekspedisi'] = (isset($data['ekspedisi']) && $data['ekspedisi'] == 1) ? $data['ekspedisi'] : 0;
                // $data['zonaIds'] = isset($data['zonaIds']) ? implode(",",$data['zonaIds']) : '';
                // $data['gambar'] = str_replace('[removed]', 'data:image/jpeg;base64,', $data['gambar']);

                if ($data['id']) {
                    $this->main->update('additional_price', $data, array('id' => $data['id']));
                } else {
                    $id = $this->main->insert('additional_price', $data);
                }

                $return = array('message' => sprintf(lang('additional_price_save_success_message'), 'Convenience'), 'status' => 'success', 'redirect' => site_url('marketing/additional_price'));
            } while (0);
        // } else {
        //     $return = array('message' => validation_errors(), 'status' => 'error');
        // }

        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->aauth->control('marketing/additional_price/delete');
        $id = decode($id);
        $data = $this->main->get('additional_price', array('id' => $id));
        $delete = $this->main->delete('additional_price', array('id' => $id));
        if ($delete) {
            $return = array('message' => sprintf(lang('additional_price_delete_success_message'), 'Convenience'), 'status' => 'success');
        } else {
            $return = array('message' => sprintf(lang('additional_price_delete_error_message'), 'Convenience'), 'status' => 'danger');
        }
        echo json_encode($return);
    }

}
