<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Reward_points extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->aauth->control('marketing/reward_points');
        //$this->lang->load('coupons', settings('language'));
        $this->load->model('reward_points_model', 'reward_points');
        $this->load->model('coupons_model', 'coupons');
        $this->load->model('Model');
        $this->data['menu'] = 'marketing_reward_points';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('marketing'), '/');
        $this->breadcrumbs->push(lang('reward_points'), '/marketing/reward_points');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->output->set_title('Reward Points');
        $this->load->js('../assets/backend/js/modules/design/reward_points.js');
        $this->load->view('reward_points/reward_points', $this->data);
    }

    public function form($id = '') {
        $this->template->_init();
        $this->template->form();

        $this->data['data'] = array();

        $this->breadcrumbs->unshift(lang('marketing'), '/');
        $this->breadcrumbs->push('Reward Point', '/marketing/reward_points');
        if ($id) {
            $this->aauth->control('marketing/coupon/edit');
            $id = decode($id);
            $join = array(
                0 => 'products p0-p0.id=r.id_product',
                1 => 'products p1-p1.id=r.id_product_paket',
                2 => 'coupons c-c.id=r.id_coupon'
            );
            $q = $this->Model->get_data(
                'r.*, p0.name as p0_name, p1.name as p1_name, c.name as c_name',
                'reward r', $join, array('r.id' => $id))->row();
            $this->data['data'] = $q;

            $this->breadcrumbs->push('Edit Reward Points', '/marketing/reward_points/form/' . encode($id));
            $this->breadcrumbs->push($q->nama, '/');
        } else {
            $this->aauth->control('marketing/reward_points/add');
            $this->breadcrumbs->push('Tambah Reward Points', '/marketing/reward_points/form');
        }

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->load->css('../assets/backend/css/jquery.treeSelector.css');
        $this->load->js('../assets/backend/js/jquery.treeSelector.js');
        $this->load->js('../assets/backend/js/modules/design/reward_points.js');
        $this->output->set_title(($this->data['data']) ? 'Edit Reward Points' : 'Tambah Reward Points');
        $this->load->view('reward_points/reward_points_form', $this->data);
    }

    public function filter_0(){
        $m = $this->Model;
        $search = $this->input->post('search');

        $where1 = array(
            0 => "p.name LIKE '%$search%' or c.name LIKE '%$search%'"
        );
        $where = array(
            'p.type' => 's'
        );

        $join = array(
            0 => 'categories c-c.id=p.category'
        );
        $q = $m->get_data('p.id, p.name', 'products p', $join, $where, 'name', 'name asc', 10, 0, $where1);
        //var_dump($this->db->last_query());exit();
        if($q->num_rows() > 0){
            $data = array();

            foreach ($q->result() as $key) {
                $data[count($data)] = array(
                    'id' => $key->id,
                    'name' => $key->name
                );
            }
            
            die(json_encode(array('success' => '1', 'data' => $data)));
        }
        die(json_encode(array('success' => '0', 'msg' => 'Data Tidak Ada')));
    }

    public function filter_1(){
        $m = $this->Model;
        $search = $this->input->post('search');

        $where1 = array(
            0 => "p.name LIKE '%$search%' or c.name LIKE '%$search%'"
        );
        $where = array(
            'p.type' => 'p'
        );

        $join = array(
            0 => 'categories c-c.id=p.category'
        );
        $q = $m->get_data('p.id, p.name', 'products p', $join, $where, 'name', 'name asc', 10, 0, $where1);

        if($q->num_rows() > 0){
            $data = array();

            foreach ($q->result() as $key) {
                $data[count($data)] = array(
                    'id' => $key->id,
                    'name' => $key->name
                );
            }
            
            die(json_encode(array('success' => '1', 'data' => $data)));
        }
        die(json_encode(array('success' => '0', 'msg' => 'Data Tidak Ada')));
    }

    public function filter_2(){
        $m = $this->Model;
        $search = $this->input->post('search');

        $where1 = array(
            0 => "name LIKE '%$search%' or code LIKE '%$search%'"
        );

        $q = $m->get_data('', 'coupons', null, null, 'name', 'name asc', 10, 0, $where1);

        if($q->num_rows() > 0){
            $data = array();

            foreach ($q->result() as $key) {
                $data[count($data)] = array(
                    'id' => $key->id,
                    'name' => $key->name
                );
            }
            
            die(json_encode(array('success' => '1', 'data' => $data)));
        }
        die(json_encode(array('success' => '0', 'msg' => 'Data Tidak Ada')));
    }

    public function getCategories() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $cats = array();
        $this->db->select('id,name,parent');
        $this->db->where('parent',0);
        $this->db->where('active',1);
        $q = $this->db->get('categories');
        $res = $q->result();
        foreach($res as $resx){
            $this->db->select('id,name as title,parent,id as value');
            $this->db->where('parent',$resx->id);
            $this->db->where('active',1);
            $qc = $this->db->get('categories');
            $resc = $qc->result();
            $resx->children = $resc;
            $cats[] = $resx;
        }
        $response['data'] = $cats;
        echo json_encode($response);
    }

    public function getZona() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->db->select('id,name,id as value');
        $q = $this->db->get('merchant_groups');
        $res = $q->result();
        $response['data'] = $res;
        echo json_encode($response);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $m = $this->Model;
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $column[0] = 'r.nama '.$order['dir'];
        $column[1] = '';
        $column[2] = 'r.point '.$order['dir'];
        $column[3] = 'r.qty '.$order['dir'];
        $column[4] = 'r.is_active '.$order['dir'];

        $output['data'] = array();
        $join = array(
            0 => 'products p-p.id=r.id_product',
            1 => 'products p0-p0.id=r.id_product_paket',
            2 => 'coupons c-c.id=r.id_coupon'
        );
        $where = array();

        if($search != ''){
            $where[0] = "(r.nama LIKE '%$search%' or p.name LIKE '%$search%' or p0.name LIKE '%$search%' or c.name LIKE '%$search%')";
        }
        
        $datas = $m->get_data('p.name, p0.name as p_name, c.name as c_name, r.*', 'reward r', $join, null, '', $column[$order['column']], $length, $start, $where);
        //$datas = $this->coupons->get_all($start, $length, $search, $order);
        if ($datas->num_rows() > 0) {
            foreach ($datas->result() as $data) {
                $name_ = '';

                $exp = explode('-', $data->tipe);

                if($exp[0] != ''){
                    $name_ .= $data->name;
                }
                if($exp[1] != ''){
                    $name_ .= $name_ == '' ? $data->p_name : ' | '.$data->p_name;
                }
                if($exp[2] != ''){
                    $name_ .= $name_ == '' ? $data->c_name : ' | '.$data->c_name;
                }
                $output['data'][] = array(
                    $data->nama,
                    $name_,
                    number_format($data->point),
                    number_format($data->qty),
                    ($data->is_active == '0' ? '<i class="icon-cross2 text-danger-400"></i>' : '<i class="icon-checkmark3 text-success"></i>'),
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">'.
                    ($this->aauth->is_allowed('marketing/reward_points/edit')?'<li><a href="' . site_url('marketing/reward_points/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>':'').
                    ($this->aauth->is_allowed('marketing/reward_points/delete')?'<li><a href="' . site_url('marketing/reward_points/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>':'').
                    '</ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->coupons->count_all();
        $output['recordsFiltered'] = $this->coupons->count_all($search);
        echo json_encode($output);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $m = $this->Model;

        $tipe = $this->input->post('tipe');
        $id_product = $this->input->post('id_product');
        $id_product_paket = $this->input->post('id_product_paket');
        $id_coupon = $this->input->post('id_coupon');
        $is_active = $this->input->post('is_active');
        $point = $this->input->post('point');
        $customer_merchant = $this->input->post('customer_merchant');
        $nama = $this->input->post('nama');
        $desc = $this->input->post('desc');
        $qty_reward = $this->input->post('qty_reward');
        $base64_image = $this->input->post('base64_image');
        $qty0 = $this->input->post('qty0');
        $qty1 = $this->input->post('qty1');
        $qty2 = $this->input->post('qty2');

        $data = array(
            'tipe' => $tipe,
            'id_product' => $id_product,
            'id_product_paket' => $id_product_paket,
            'id_coupon' => $id_coupon,
            'is_active' => $is_active,
            'date_add' => date('Y/m/d H:i:s'),
            'date_modified' => date('Y/m/d H:i:s'),
            'point' => $point,
            'customer_merchant' => $customer_merchant,
            'nama' => $nama,
            'deskripsi' => $desc,
            'qty' => $qty_reward,
            'gambar' => $base64_image,
            'qty_product' => $qty0,
            'qty_paket' => $qty1,
            'qty_coupon' => $qty2
        );

        $m->insert_data('reward', $data);

        die(json_encode(array('success' => '1')));
    }

    public function save_edit() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $m = $this->Model;

        $tipe = $this->input->post('tipe');
        $id_product = $this->input->post('id_product');
        $id_product_paket = $this->input->post('id_product_paket');
        $id_coupon = $this->input->post('id_coupon');
        $is_active = $this->input->post('is_active');
        $point = $this->input->post('point');
        $customer_merchant = $this->input->post('customer_merchant');
        $nama = $this->input->post('nama');
        $desc = $this->input->post('desc');
        $qty_reward = $this->input->post('qty_reward');
        $id = $this->input->post('id');
        $base64_image = $this->input->post('base64_image');
        $qty0 = $this->input->post('qty0');
        $qty1 = $this->input->post('qty1');
        $qty2 = $this->input->post('qty2');

        $data = array(
            'tipe' => $tipe,
            'id_product' => $id_product,
            'id_product_paket' => $id_product_paket,
            'id_coupon' => $id_coupon,
            'is_active' => $is_active,
            'date_add' => date('Y/m/d H:i:s'),
            'date_modified' => date('Y/m/d H:i:s'),
            'point' => $point,
            'customer_merchant' => $customer_merchant,
            'nama' => $nama,
            'deskripsi' => $desc,
            'gambar' => $base64_image,
            'qty_product' => $qty0,
            'qty_paket' => $qty1,
            'qty_coupon' => $qty2,
            'qty' => $qty_reward
        );

        $where['id'] = $id;

        $m->update_data('reward', $data, $where);

        die(json_encode(array('success' => '1')));
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $id = decode($id);
        $data = $this->Model->get_data('nama', 'reward', null, array('id' => $id))->row();
        $delete = $this->Model->delete_data1('reward', array('id' => $id));
        if ($delete) {
            $this->Model->delete_data1('point_exchange_history', array('id_reward' => $id));
            $return = array('message' => sprintf('Reward Point Berhasil Dihapus', $data->nama), 'status' => 'success');
        } else {
            $return = array('message' => sprintf('Reward Point Gagal Dihapus', $data->nama), 'status' => 'danger');
        }
        echo json_encode($return);
    }

    public function update_periode_tukar_point(){
        $i = $this->input;

        $tahun_mulai = $i->post('tahun_mulai');
        $bulan_mulai = $i->post('bulan_mulai');
        $tanggal_mulai = $i->post('tanggal_mulai');
        $tahun_selesai = $i->post('tahun_selesai');
        $bulan_selesai = $i->post('bulan_selesai');
        $tanggal_selesai = $i->post('tanggal_selesai');

        $value = $tahun_mulai.';'.$bulan_mulai.';'.$tanggal_selesai.';'.$tahun_selesai.';'.$bulan_selesai.';'.$tanggal_selesai.';1';
        $cek = $this->Model->get_data('', 'settings', null, array('key' => 'periode_tukar_point'));
        if($cek->num_rows() > 0){
            $this->Model->update_data('settings', array('value' => $value), array('key' => 'periode_tukar_point'));
        }else{
            $this->Model->insert_data('settings', array('key' => 'periode_tukar_point', 'value' => $value));
        }

        die(json_encode(array('success' => 1)));
    }

}
