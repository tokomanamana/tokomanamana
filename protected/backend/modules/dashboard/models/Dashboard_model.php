<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    function sales_summary() {
        $status = settings('order_finish_status');
        $this->db->select('IFNULL(COUNT(id),0) transaction, IFNULL(SUM(total),0) sales')
                ->where(array('order_status' => $status, 'YEAR(date_added)' => date('Y'), 'MONTH(date_added)' => date('m')));
        return $this->db->get('order_invoice')->row();
    }

    function last_orders() {
        $this->db->select('oi.code, oi.order, oi.total, c.fullname customer_name, oi.customer, m.name merchant_name, oi.merchant, op.quantity')
                ->join('customers c', 'oi.customer = c.id', 'left')
                ->join('merchants m', 'oi.merchant = m.id', 'left')
                ->join('(SELECT SUM(quantity) quantity, invoice FROM order_product op GROUP BY invoice) op', 'op.invoice = oi.id', 'left')
                ->where(array('YEAR(oi.date_added)' => date('Y'), 'MONTH(oi.date_added)' => date('m')))
                ->limit(10)
                ->order_by('oi.id DESC');
        return $this->db->get('order_invoice oi');
    }

    function last_search() {
        $this->db->select('*, COUNT(keyword) uses')
                ->limit(5)
                ->group_by('keyword')
                ->order_by('time DESC');
        return $this->db->get('search');
    }

    function top_search() {
        $this->db->select('*, COUNT(keyword) uses')
                ->limit(5)
                ->group_by('keyword')
                ->order_by('uses DESC');
        return $this->db->get('search');
    }

    // function bestsellers() {
    //     $status = settings('order_finish_status');
    //     $this->db->select('op.name, SUM(quantity) quantity, op.price')
    //             ->join('order_invoice oi', 'op.invoice = oi.id', 'left')
    //             ->limit(15)
    //             ->group_by('op.product')
    //             ->where('oi.order_status', $status)
    //             ->order_by('quantity DESC');
    //     return $this->db->get('order_product op');
    // }

    // function mostviewed() {
    //     $this->db->select('name, price, viewed')
    //             ->where('status', 1)
    //             ->order_by('viewed desc')
    //             ->limit(15);
    //     return $this->db->get('products');
    // }

    function get_new_order ($status, $from, $to) {
        $query = $this->db->query("SELECT COUNT(*) AS `count` 
                                    FROM orders 
                                    WHERE DATE(date_added) BETWEEN '". $from ."' AND '". $to ."' AND payment_status = '". $status ."'");
        return $query ? $query->result() : FALSE;
    }

    function get_new_user($table, $from, $to) {
        if ($table == 'customers') {
            $from = strtotime($from);
            $to = strtotime($to);
            $param = 'created_on';
        } elseif ($table == 'merchants') {
            $param = 'date_added';
        } elseif ($table == 'principles') {
            $param = 'DATE(date_added)';
        }

        $query = $this->db->query("SELECT COUNT(*) AS `count` FROM ". $table ." WHERE ". $param ." BETWEEN ". $from ." AND ". $to ."");
        return $query ? $query->result() : FALSE;
    }

    // New Method
    function bestsellers($from, $to) {
        $status = settings('order_finish_status');
        $this->db->select('op.name, SUM(op.quantity) quantity, op.price, (CASE WHEN op.options != "" THEN 1 ELSE 0 END) as option, op.product as product_id')
                ->join('order_invoice oi', 'op.invoice = oi.id', 'left')
                ->limit(10)
                ->group_by('op.product')
                ->where('oi.order_status', $status)
                ->where('DATE(oi.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'')
                ->order_by('quantity DESC');
        return $this->db->get('order_product op');
    }

    // New Method
    function mostviewed($from = '', $to = '') {
        // $this->db->select('p.name, p.price, SUM(pv.view) total')
        //         ->join('products p', 'pv.product = p.id', 'LEFT')
        //         ->limit(10)
        //         ->group_by('p.id')
        //         ->order_by('total', 'DESC')
        //         ->where('DATE(pv.date_modified) BETWEEN \'' . $from . '\' AND \'' . $to . '\'');
        // return $this->db->get('product_viewed pv');
        $this->db->select('name, price, viewed as total')
                ->where('status', 1)
                ->order_by('total desc')
                ->limit(10);
        return $this->db->get('products');
    }

}
