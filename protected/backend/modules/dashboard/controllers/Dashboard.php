<?php

defined('BASEPATH') or exit('No direct script access allowed!');

require '../vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('dashboard_model','dashboard');
        $this->data['menu'] = 'dashboard';
    }

//     public function index() {
//         $this->data['summary'] = $this->dashboard->sales_summary();
//         $this->data['last_orders'] = $this->dashboard->last_orders();
// //        $this->data['last_search'] = $this->dashboard->last_search();
// //        $this->data['top_search'] = $this->dashboard->top_search();
//         $this->data['bestsellers'] = $this->dashboard->bestsellers();
//         $this->data['mostviewed'] = $this->dashboard->mostviewed();
        
//         $this->template->_init();
// //        $this->load->js('assets/js/plugins/visualization/d3/d3.min.js');
// //        $this->load->js('assets/js/plugins/visualization/c3/c3.min.js');
// //        $this->load->js('assets/js/modules/dashboard.js');
//         $this->output->set_title('Dasbor');
//         $this->load->view('dashboard', $this->data);
//     }

    public function index() {
        $this->data['summary'] = $this->dashboard->sales_summary();
        $this->data['last_orders'] = $this->dashboard->last_orders();
        $this->data['pending_orders'] = $this->main->count('orders', array('MONTH(date_added)' => date('m'), 'YEAR(date_added)' => date('Y'), 'payment_status' => 'Pending'));
        $this->data['paid_orders'] = $this->main->count('orders', array('MONTH(date_added)' => date('m'), 'YEAR(date_added)' => date('Y'), 'payment_status' => 'Paid'));
        $this->data['confirmed_order'] = $this->main->count('orders', array('MONTH(date_added)' => date('m'), 'YEAR(date_added)' => date('Y'), 'payment_status' => 'Confirmed'));
        $this->data['mostviewed'] = $this->dashboard->mostviewed();
        $this->template->_init();
        $this->template->form();
        $this->template->table();
        // $this->load->js('assets/js/plugins/visualization/d3/d3.min.js');
        // $this->load->js('assets/js/plugins/visualization/c3/c3.min.js');
        // $this->load->js('../assets/backend/js/modules/dashboard.js');
        $this->load->js('../assets/backend/js/modules/dashboard.js');
        $this->output->set_title('Dasbor');
        $this->load->view('dashboard', $this->data);
    }

    public function orders () {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $input = $this->input->post(null, true);
        $filter = $input['filter'];
        $output['data'] = array();
        
        $pendings = $this->dashboard->get_new_order('Pending', $filter['from'], $filter['to']);
        if ($pendings) {
            foreach ($pendings as $p) {
                $pending = array('Pesanan Baru', $p->count, '<button class="btn btn-primary btn-xs" onclick="see_filtered_order(\'Pending\');">Lihat Semua</button>');
                array_push($output['data'], $pending);
            }
        }
        
        $confimeds = $this->dashboard->get_new_order('Confirmed', $filter['from'], $filter['to']);
        if ($confimeds) {
            foreach ($confimeds as $c) {
                $confimed = array('Pesanan Dikonfirmasi', $c->count, '<button class="btn btn-primary btn-xs" onclick="see_filtered_order(\'Confirmed\');">Lihat Semua</button>');
                array_push($output['data'], $confimed);
            }
        }

        $paids = $this->dashboard->get_new_order('Paid', $filter['from'], $filter['to']);
        if ($paids) {
            foreach ($paids as $p) {
                $paid = array('Pesanan Dibayar', $p->count, '<button class="btn btn-primary btn-xs" onclick="see_filtered_order(\'Paid\');">Lihat Semua</button>');
                array_push($output['data'], $paid);
            }
        }
        
        echo json_encode($output);
    }

    public function list_user () {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $input = $this->input->post(null, true);
        $filter = $input['filter'];
        $output['data'] = array();

        $customers = $this->dashboard->get_new_user('customers', $filter['from'], $filter['to']);
        if ($customers) {
            foreach ($customers as $c) {
                $customer = array('Customer', $c->count);
                array_push($output['data'], $customer);
            }
        }
        
        $merchants = $this->dashboard->get_new_user('merchants', $filter['from'], $filter['to']);
        if ($merchants) {
            foreach ($merchants as $m) {
                $merchant = array('Merchant', $m->count);
                array_push($output['data'], $merchant);
            }
        }
        
        $principles = $this->dashboard->get_new_user('principles', $filter['from'], $filter['to']);
        if ($principles) {
            foreach ($principles as $p) {
                $principle = array('Principal', $p->count);
                array_push($output['data'], $principle);
            }
        }
        

        echo json_encode($output);
    }

    public function bestseller () {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $input = $this->input->post(null, true);
        $filter = $input['filter'];
        $output['data'] = array();

        $datas = $this->dashboard->bestsellers($filter['from'], $filter['to']);

        if ($datas->num_rows() > 0) {
            foreach ($datas->result() as $data) {
                if($data->option) {
                    $product_option = $this->main->get('product_option', ['product' => $data->product_id, 'default' => 1]);
                    if($product_option) {
                        $output['data'][] = array(
                            $data->name,
                            rupiah($product_option->price),
                            $data->quantity
                        );
                    }
                } else {
                    $output['data'][] = array(
                        $data->name,
                        rupiah($data->price),
                        $data->quantity
                    );
                }
            }
        }
        echo json_encode($output);
    }

    public function export_bestseller () {
        $filename = "Best Seller";
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        foreach(range('A','F') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }
        $sheet->fromArray(array('No.', 'Produk', 'Merchant', 'Harga', 'Terjual'));
        $input = $this->input->post(null, true);
        $full_name = "${filename}_". $input['from'] .'_'. $input['to'];
        $datas = $this->dashboard->bestsellers($input['from'], $input['to']);
        
        if ($datas->num_rows() > 0) {
            $i = 2;
            $n = 1;
            foreach ($datas->result() as $data) {
                if($data->price == 0 && $data->options) {
                    $options = json_decode($data->options, true);
                    foreach($options as $opt) {
                        $row = array(
                            $n,
                            $data->name,
                            $data->merchant_name,
                            $opt['price'],
                            $data->quantity
                        );
                    }
                } else {
                    $row = array(
                        $n,
                        $data->name,
                        $data->merchant_name,
                        $data->price,
                        $data->quantity
                    );
                }

                $sheet->fromArray($row, NULL, 'A'. $i);
                $i++; $n++;
            }
        }

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename='. $full_name .'.xls');
        header('Cache-Control: max-age = 0');
        header('Cache-Control: max-age = 1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: '. gmdate('D, d M Y H:i:s') .' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
        ob_start();
        $writer->save('php://output');
        $xlsData = ob_get_contents();
        ob_end_clean();
        $response = array(
            'op' => 200,
            'file' => 'data: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; base64, '. base64_encode($xlsData),
            'name' => $full_name .'.xls'
        );

        die(json_encode($response));
    }

    public function mostviewed () {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $input = $this->input->post(null, true);
        // $filter = $input['filter'];
        $output['data'] = array();

        // $datas = $this->dashboard->mostviewed($filter['from'], $filter['to']);
        $datas = $this->dashboard->mostviewed();
        
        if ($datas->num_rows() > 0) {
            foreach($datas->result() as $data) {
                $output['data'][] = array(
                    $data->name,
                    rupiah($data->price),
                    $data->total
                );
            }
        }

        echo json_encode($output);
    }

    public function export_mostviewed () {
        $filename = "Most Viewed";
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        foreach(range('A','F') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }
        $sheet->fromArray(array('No.', 'Produk', 'Harga', 'Dilihat'));
        // $input = $this->input->post(null, true);
        // $full_name = "${filename}_". $input['from'] .'_'. $input['to'];
        $full_name = $filename;
        // $datas = $this->dashboard->mostviewed($input['from'], $input['to']);
        $datas = $this->dashboard->mostviewed();
        
        if ($datas->num_rows() > 0) {
            $i = 2;
            $n = 1;
            foreach ($datas->result() as $data) {
                $row = array(
                    $n,
                    $data->name,
                    $data->price,
                    $data->total
                );

                $sheet->fromArray($row, NULL, 'A'. $i);
                $i++; $n++;
            }
        }

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename='. $full_name .'.xls');
        header('Cache-Control: max-age = 0');
        header('Cache-Control: max-age = 1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: '. gmdate('D, d M Y H:i:s') .' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
        ob_start();
        $writer->save('php://output');
        $xlsData = ob_get_contents();
        ob_end_clean();
        $response = array(
            'op' => 200,
            'file' => 'data: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; base64, '. base64_encode($xlsData),
            'name' => $full_name .'.xls'
        );

        die(json_encode($response));
    }

}
