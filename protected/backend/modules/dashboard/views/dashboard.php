<div class="content-wrapper">
    <div class="content">
        <div class="row">
            <div class="col-sm-6">
                <div class="panel">
                    <div class="panel-heading">
                        <h6 class="panel-title">Penjualan Bulan Ini</h6>
                    </div>
                    <div class="panel-body">
                        <div class="row text-center">
                            <div class="col-xs-5">
                                <a href="javascript:void(0)" onclick="see_month_sales()">
                                    <div style="background:#a7c22a;border-radius:5px;padding:10px">
                                        <span style="color:#fff;font-size:18px" class="text-semibold no-margin"><?php echo number($summary->transaction); ?></span><br>
                                        <span style="color:#fff" class="text-muted text-size-small">Transaksi</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xs-7">
                                <a href="javascript:void(0)" onclick="see_month_sales()">
                                    <div style="background:#a7c22a;border-radius:5px;padding:10px">
                                        <span style="color:#fff;font-size:18px" class="text-semibold no-margin"><?php echo rupiah($summary->sales); ?></span><br>
                                        <span style="color:#fff" class="text-muted text-size-small">Penjualan</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-heading">
                        <h6 class="panel-title">Pengguna Baru</h6>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <form id="filter-user-list">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Filter By:</label>
                                            <select class="form-control filter-by custom-select">
                                                <option value="today">Hari Ini</option>
                                                <option value="yesterday">Kemarin</option>
                                                <option value="this month">Bulan Ini</option>
                                                <option value="last month">Bulan Lalu</option>
                                                <option value="this year">Tahun Ini</option>
                                                <option value="all">Semua</option>
                                                <option value="99">Kostum</option>
                                            </select>
                                        </div>
                                    </div>
    
                                    <div class="col-xs-6 col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Dari: </label>
                                            <input type="text" name="from" class="form-control" readonly="" value="<?php echo date('Y-m-d'); ?>">
                                        </div>
                                    </div>
                                    
                                    <div class="col-xs-6 col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Sampai: </label>
                                            <input type="text" name="to" class="form-control" readonly="" value="<?php echo date('Y-m-d'); ?>">
                                        </div>
                                    </div>
    
                                    <div class="col-xs-6 col-sm-6">
                                        <button class="btn btn-default btn-block filter" type="button">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-hover" width="100%" id="table-user-list" data-url="<?php echo site_url('dashboard/list_user'); ?>">
                                    <thead>
                                        <tr>
                                            <th class="no-sort">Type Pengguna</th>
                                            <th class="no-sort">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-heading">
                        <h6 class="panel-title">10 Pesanan Terakhir</h6>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Pelanggan</th>
                                    <th>Merchant</th>
                                    <th>Item</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($last_orders->num_rows() > 0) { ?>
                                    <?php foreach ($last_orders->result() as $order) { ?>
                                        <tr>
                                            <td><?php echo $order->code; ?></td>
                                            <td><?php echo $order->customer_name; ?></td>
                                            <td><?php echo $order->merchant_name; ?></td>
                                            <td><?php echo number($order->quantity); ?></td>
                                            <td><?php echo rupiah($order->total); ?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="4">Tidak ada transaksi</td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="panel">
                    <div class="panel-heading">
                        <h6 class="panel-title">Penjualan Bulan Ini</h6>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <form id="filter-order">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Filter By:</label>
                                            <select class="form-control filter-by custom-select">
                                                <option value="today">Hari Ini</option>
                                                <option value="yesterday">Kemarin</option>
                                                <option value="this month">Bulan Ini</option>
                                                <option value="last month">Bulan Lalu</option>
                                                <option value="this year">Tahun Ini</option>
                                                <option value="99">Kostum</option>
                                            </select>
                                        </div>
                                    </div>
    
                                    <div class="col-xs-6 col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Dari: </label>
                                            <input type="text" name="from" class="form-control" readonly="" value="<?php echo date('Y-m-d'); ?>">
                                        </div>
                                    </div>
                                    
                                    <div class="col-xs-6 col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Sampai: </label>
                                            <input type="text" name="to" class="form-control" readonly="" value="<?php echo date('Y-m-d'); ?>">
                                        </div>
                                    </div>
    
                                    <div class="col-xs-6 col-sm-6">
                                        <button class="btn btn-default btn-block filter" type="button">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-hover" width="100%" id="table-order" data-url="<?php echo site_url('dashboard/orders'); ?>">
                                    <thead>
                                        <tr>
                                            <th class="no-sort">Status</th>
                                            <th class="no-sort">Jumlah</th>
                                            <th class="no-sort"></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-content-bordered content-group">
                    <ul class="nav nav-tabs nav-tabs-highlight nav-lg nav-justified">
                        <li class="active"><a href="#bestsellers" data-toggle="tab">Bestsellers</a></li>
                        <li><a href="#mostviewed" data-toggle="tab">Banyak Dilihat</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane has-padding active" id="bestsellers">
							<div class="row">
								<form id="filter-bestseller">
									<div class="row">
										<div class="col-xs-12 col-sm-4">
											<div class="form-group">
												<label class="control-label">Filter By:</label>
												<select class="form-control filter-by custom-select">
													<option value="today">Hari Ini</option>
													<option value="yesterday">Kemarin</option>
													<option value="this month">Bulan Ini</option>
													<option value="last month">Bulan Lalu</option>
													<option value="this year">Tahun Ini</option>
													<option value="99">Kostum</option>
												</select>
											</div>
										</div>

										<div class="col-xs-6 col-sm-4">
											<div class="form-group">
												<label class="control-label">Dari: </label>
												<input type="text" name="from" class="form-control" readonly="" value="<?php echo date('Y-m-d'); ?>">
											</div>
										</div>
										
										<div class="col-xs-6 col-sm-4">
											<div class="form-group">
												<label class="control-label">Sampai: </label>
												<input type="text" name="to" class="form-control" readonly="" value="<?php echo date('Y-m-d'); ?>">
											</div>
										</div>

										<div class="col-xs-6 col-sm-6">
											<button class="btn btn-default btn-block filter" type="button">Submit</button>
										</div>

										<?php if ($this->aauth->is_allowed('order')) { ?>
											<div class="col-xs-6 col-sm-6">
												<button class="btn btn-default btn-block has-text export" type="button"><i class="icon-download7 text-primary"></i><span> Export</span></button>
											</div>
										<?php } ?>
									</div>
								</form>
							</div>

							<div class="row" style="margin-top: 2em;">
								<div class="table-responsive">
									<table class="table table-hover" width="100%" id="table-bestseller" data-url="<?php echo site_url('dashboard/bestseller'); ?>">
										<thead>
											<tr>
                                                <th class="no-sort">Produk</th>
												<th class="no-sort">Harga</th>
												<th class="no-sort">Terjual</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
                        </div>
                        <div class="tab-pane has-padding" id="mostviewed">
							<div class="row">
								<form id="filter-mostviewed">
									<div class="row">
										<!-- <div class="col-xs-12 col-sm-4">
											<div class="form-group">
												<label class="control-label">Filter By:</label>
												<select class="form-control filter-by custom-select" style="width: 100%;">
													<option value="today">Hari Ini</option>
													<option value="yesterday">Kemarin</option>
													<option value="this month">Bulan Ini</option>
													<option value="last month">Bulan Lalu</option>
													<option value="this year">Tahun Ini</option>
													<option value="99">Kostum</option>
												</select>
											</div>
										</div>

										<div class="col-xs-6 col-sm-4">
											<div class="form-group">
												<label class="control-label">Dari: </label>
												<input type="text" name="from" class="form-control" readonly="" value="<?php echo date('Y-m-d'); ?>">
											</div>
										</div>
										
										<div class="col-xs-6 col-sm-4">
											<div class="form-group">
												<label class="control-label">Sampai: </label>
												<input type="text" name="to" class="form-control" readonly="" value="<?php echo date('Y-m-d'); ?>">
											</div>
										</div>

										<div class="col-xs-6 col-sm-6">
											<button class="btn btn-default btn-block filter" type="button">Submit</button>
										</div> -->

										<?php if ($this->aauth->is_allowed('order')) { ?>
											<div class="col-xs-12 col-sm-12">
												<button class="btn btn-default btn-block has-text export" type="button"><i class="icon-download7 text-primary"></i><span> Export</span></button>
											</div>
										<?php } ?>
									</div>
								</form>
							</div>

							<div class="row" style="margin-top: 2em;">
								<div class="table-responsive">
									<!-- <table class="table table-hover" width="100%" id="table-mostviewed" data-url="<?php //echo site_url('dashboard/mostviewed'); ?>">
										<thead>
											<tr>
												<th class="no-sort">Produk</th>
												<th class="no-sort">Harga</th>
												<th class="no-sort">Dilihat</th>
											</tr>
										</thead>
										<tbody></tbody>
                                    </table> -->
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Produk</th>
                                                <th>Harga</th>
                                                <th>Dilihat</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if($mostviewed->num_rows() > 0) : ?>
                                            <?php foreach($mostviewed->result() as $view) : ?>
                                                <tr>
                                                    <td><?= $view->name ?></td>
                                                    <td><?= rupiah($view->price) ?></td>
                                                    <td><?= $view->total ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                            <?php else : ?>
                                                <tr>
                                                    <td colspan="3">Tidak ada data</td>
                                                </tr>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>