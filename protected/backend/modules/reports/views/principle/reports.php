<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('principle_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <?php if ($this->aauth->is_allowed('report/principal')) { ?>
                        <a href="javascript:void(0);" class="btn btn-link btn-float has-text" id="btn-export-principle"><i class="icon-download7 text-primary"></i><span>Export</span></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="row">
                    <form id="filter-form">
                        <input type="hidden" name="principal" value="<?php echo encode($principal->id); ?>">
                        <div class="col-sm-5">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">Filter By:</label>
                                        <select id="filter-by" class="form-control bootstrap-select">
                                            <option value="today">Hari Ini</option>
                                            <option value="yesterday">Kemarin</option>
                                            <option value="this month">Bulan Ini</option>
                                            <option value="last month">Bulan Lalu</option>
                                            <option value="this year">Tahun Ini</option>
                                            <option value="99">Kostum</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xs-6 col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">Dari:</label>
                                        <input type="text" class="form-control" readonly="" name="from" value="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                </div>
                                
                                <div class="col-xs-6 col-sm-4 no-padding-right">
                                    <div class="form-group">
                                        <label class="control-label">Sampai:</label>
                                        <input type="text" class="form-control" readonly="" name="to" value="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-7">
                            <div class="col-xs-6 col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Status Transaksi</label>
                                    <select name="order_status" class="form-control bootstrap-select">
                                        <option value="">Semua</option>
                                        <option value="1">Pesanan Baru</option>
                                        <option value="3">Sudah Dibayar</option>
                                        <option value="4">Sedang Diproses</option>
                                        <option value="5">Sudah Dikirim</option>
                                        <option value="6">Sampai Ditujuan</option>
                                        <option value="7">Selesai</option>
                                        <option value="8">Dibatalkan</option>
                                        <option value="9">Ditolak Penjual</option>
                                        <option value="10">Siap Pickup</option>
                                        <option value="11">Pickup Selesai</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-6 col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Status Pembayaran:</label>
                                    <select name="payment_status" class="form-control bootstrap-select">
                                        <option value="">Semua</option>
                                        <option value="Pending">Pending</option>
                                        <option value="Paid">Paid</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-6 col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Pengiriman:</label>
                                    <select name="shipping" class="form-control bootstrap-select">
                                        <option value="">Semua</option>
                                        <option value="jne">JNE</option>
                                        <option value="tiki">TIKI</option>
                                        <option value="pos">POS</option>
                                        <option value="pickup">Pickup</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 no-padding">
                            <div class="col-xs-6 col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Produk:</label>
                                    <select name="product" class="select2 form-control" style="width: 100%;">
                                        <option value="">Semua</option>
                                        <?php if ($products->num_rows() > 0) foreach($products->result() as $product) { ?>
                                            <option value="<?php echo $product->id; ?>"><?php echo $product->name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-6 col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Cabang:</label>
                                    <select name="branch" class="form-control bootstrap-select">
                                        <option value="">Semua</option>
                                        <?php $branches = $this->main->gets('merchants', ['principal_id' => $principal->id]); ?>
                                        <?php if ($branches) : ?>
                                            <?php foreach ($branches->result() as $branch) : ?>
                                                <option value="<?php echo $branch->id ?>"><?php echo $branch->name ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-6 col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" style="width: 100%;">Kota</label>
                                    <select name="city" class="select2 form-control">
                                            <option value="">Semua</option>
                                        <?php if ($cities = $this->main->gets('cities')) foreach ($cities->result() as $city) { ?>
                                                <option value="<?php echo $city->id; ?>"><?php echo $city->name; ?></option>
                                            <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="col-xs-6 col-sm-4">
                                <button class="btn btn-default" type="button" id="filter" style="margin-top: 2em;">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="panel panel-flat table-responsive">
            <table class="table table-hover" width="100%" id="table-report" data-url="<?php echo site_url('reports/principle/get_report/'. encode($principal->id)); ?>">
                <thead>
                    <tr>
                        <th class="default-sort" data-sort="desc">ID</th>
                        <th>Produk</th>
                        <th>Tanggal</th>
                        <th>Pelanggan</th>
                        <th>Harga</th>
                        <th>Jumlah</th>
                        <th class="no-sort">Potongan</th>
                        <th class="no-sort">Kupon</th>
                        <th>Total</th>
                        <th>Pembayaran</th>
                        <th>Pembayaran Via</th>
                        <th>Status</th>
                        <th>Pengiriman</th>
                        <th>Cabang</th>
                        <th>Kota / Kabupaten</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>