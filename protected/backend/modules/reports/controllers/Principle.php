<?php

    defined('BASEPATH') or exit('No direct script access allowed!');

    require '../vendor/autoload.php';
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Writer\Xls;
    
    class Principle extends CI_Controller {
        public function __construct () {
            parent::__construct();
            $this->aauth->control('report/principal');
            $this->aauth->is_allowed('report/principal');
            $this->lang->load('sales', settings('language'));
            $this->load->model('principle_model', 'principle');
            $this->data['menu'] = 'report_principle';
            $this->data['body_class'] = 'has-detached-left';
        }

        public function index () {
            $this->breadcrumbs->unshift(lang('report'), '/');
            $this->breadcrumbs->push(lang('report_principle'), '/reports/principle');
            $this->data['breadcrumbs'] = $this->breadcrumbs->show();
            $this->template->_init();
            $this->template->table();
            $this->load->js('../assets/backend/js/modules/reports.js');
            $this->output->set_title(lang('principle_heading'));
            $this->load->view('principle/index', $this->data);
        }

        public function data () {
            $this->input->is_ajax_request() or exit('No direct post submit allowed!');

            $input = $this->input->post(null, true);
            $draw = intval($input['draw']);

            $output['data'] = array();
            $datas = $this->principle->get_brand($input['start'], $input['length'], $input['search']['value'], $input['order'][0]);
            if ($datas) {
                foreach ($datas->result() as $data) {
                    $output['data'][] = array(
                        '<a href="'. site_url('reports/principle/product/' . encode($data->id)) .'">'. $data->name .'</a>',
                        '<td class="text-center">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="'. site_url('reports/principle/product/'. encode($data->id)) .'">' . lang('button_view') . '</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </td>'
                    );
                }
            }

            $output['draw'] = $draw++;
            $ouput['recordsTotal'] = $this->main->count('brands');
            $output['recordsFiltered'] = $this->principle->count_all($input['search']['value']);
            echo json_encode($output);
        }

        public function product ($id) {
            $id = decode($id);
            $this->breadcrumbs->unshift(lang('report'), '/');
            $this->breadcrumbs->push(lang('report_principle'), '/reports/principle');
            $this->data['breadcrumbs'] = $this->breadcrumbs->show();

            $this->data['products'] = $this->main->gets('products', array('status' => '1', 'store_type' => 'principal', 'store_id' => $id));
            $this->data['principal'] = $this->main->get('principles', array('id' => $id));

            $this->template->_init();
            $this->template->form();
            $this->template->table();
            $this->load->css('../assets/backend/css/jquery.treeSelector.css');
            $this->load->js('../assets/backend/js/jquery.treeSelector.js');
            $this->load->js('../assets/backend/js/modules/reports.js');
            $this->output->set_title(lang('principle_heading'));
            $this->load->view('principle/reports', $this->data);
        }

        public function get_report ($id) {
            $this->input->is_ajax_request() or exit('No direct post submit allowed!');

            $principal = decode($id);
            $input = $this->input->post(null, true);
            $filter = $input['filter'];
            $output['data'] = array();
            $invoices = $this->db->query("SELECT DISTINCT invoice FROM order_product LEFT JOIN products ON order_product.product = products.id WHERE products.store_type = 'principal' AND products.store_id = $principal AND products.status = 1");
            $invoice = array();
            if ($invoices) {
                foreach ($invoices->result() as $i) {
                    array_push($invoice, $i->invoice);
                }
            }
            $datas = $this->principle->reports($invoice, $principal, $filter['product'], $input['start'], $input['length'], $input['order'][0], $filter['from'], $filter['to'], $filter['order_status'], $filter['payment_status'], $filter['shipping'], $filter['branch'], isset($filter['city']) ? $filter['city'] : '');
            if ($datas) {
                foreach ($datas->result() as $data) {
                    $coupon = $this->principle->get_coupon($data->order);
                    if ($coupon) {
                        $get_invoice = $this->main->gets('order_invoice', array('order' => $data->order));
                        if ($get_invoice) {
                            $total_invoice_without_shipping = 0;
                            foreach ($get_invoice->result() as $inv) {
                                $total_invoice_without_shipping += $inv->subtotal;
                            }

                            $n = ($coupon->amount / $total_invoice_without_shipping) * 100;
                            $percentage = 100 - $n;

                            $discount_amount = $data->price * $percentage / 100;
                            $discount_amount = $data->price - $discount_amount;
                        } else {
                            $discount_amount = 0;
                        }
                    } else {
                        $discount_amount = 0;
                    }

                    $output['data'][] = array(
                        '<a href="'. site_url('orders/view/'. encode($data->order)) .'" target="_blank">'. $data->invoice .'</a>',
                        $data->product,
                        get_date($data->date),
                        $data->customer,
                        number($data->price),
                        $data->quantity,
                        number($discount_amount),
                        $coupon ? $coupon->code : '',
                        rupiah($data->total - $discount_amount),
                        $data->payment_status,
                        $data->payment_method,
                        $data->order_status,
                        strtoupper($data->shipping_courier),
                        $data->merchant_name,
                        $data->city_type . ' ' . $data->city_name
                    );
                }
            }
            $output['recordsTotal'] = $this->principle->reports_total($invoice, $principal, $filter['product'], $filter['from'], $filter['to']);
            $output['recordsFiltered'] = $this->principle->reports_total($invoice, $principal, $filter['product'], $filter['from'], $filter['to'], $filter['order_status'], $filter['payment_status'], $filter['shipping'], $filter['branch'], isset($filter['city']) ? $filter['city'] : '');
            echo json_encode($output);
        }

        public function export () {
            // $this->aauth->control()
            $filename = "Laporan Principal";
            $spreadsheet = new Spreadsheet();
            $spreadsheet->setActiveSheetIndex(0);
            $sheet = $spreadsheet->getActiveSheet();
            foreach(range('A','O') as $columnID) {
                $sheet->getColumnDimension($columnID)->setAutoSize(true);
            }
            \PhpOffice\PhpSpreadsheet\Shared\File::setUseUploadTempDirectory(true);
            $sheet->fromArray(array('ID', 'Produk', 'Tanggal', 'Pelanggan', 'Harga', 'Jumlah', 'Potongan', 'Kupon', 'Total', 'Pembayaran', 'Pembayaran Via', 'Status', 'Pengiriman', 'Cabang', 'Kota / Kabupaten'), NULL, 'A1');
            $data = $this->input->get(null, true);
            $fullFileName = "${filename}_".$data['from'].'_'.$data['to'];
            $principal_id = decode($data['principal']);
            $invoices = $this->db->query("SELECT DISTINCT invoice FROM order_product LEFT JOIN products ON order_product.product = products.id WHERE products.store_type = 'principal' AND products.store_id = $principal_id");
            $invoice = array();
            if ($invoices) {
                foreach ($invoices->result() as $i) {
                    array_push($invoice, $i->invoice);
                }
            }
            if ($data['city'] == 'undefined') {
                $data['city'] = '';
            }
            $reports = $this->principle->reports_exports($invoice, $principal_id, $data['product'], $data['from'], $data['to'], $data['order_status'], $data['payment_status'], $data['shipping'], $data['branch'], isset($data['city']) ? $data['city'] : '');
            $i = 2;
            if ($reports->num_rows() > 0) {
                foreach ($reports->result() as $report) {
                    $coupon = $this->principle->get_coupon($report->order);
                    if ($coupon) {
                        $get_invoice = $this->main->gets('order_invoice', array('order' => $report->order));
                        if ($get_invoice) {
                            $total_invoice_without_shipping = 0;
                            foreach ($get_invoice->result() as $inv) {
                                $total_invoice_without_shipping += $inv->subtotal;
                            }

                            $n = ($coupon->amount / $total_invoice_without_shipping) * 100;
                            $percentage = 100 - $n;

                            $discount_amount = $report->price * $percentage / 100;
                            $discount_amount = $report->price - $discount_amount;
                        } else {
                            $discount_amount = 0;
                        }
                    } else {
                        $discount_amount = 0;
                    }

                    $row_data = array(
                        $report->invoice,
                        $report->product,
                        get_date($report->date),
                        $report->customer,
                        $report->price,
                        $report->quantity,
                        $discount_amount,
                        $coupon ? $coupon->code : '',
                        $report->total - $discount_amount,
                        $report->payment_status,
                        $report->payment_method,
                        $report->order_status,
                        strtoupper($report->shipping_courier),
                        $report->merchant_name,
                        $report->city_type . ' ' . $report->city_name
                    );

                    $sheet->fromArray($row_data, NULL, 'A' . $i);
                    $i++;
                }
            }
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $fullFileName . '.xls"');
            header('Cache-Control: max-age=0');
            $writer = IOFactory::createWriter($spreadsheet, 'Xls');
            $writer->save('php://output');
        }
    }