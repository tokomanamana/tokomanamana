<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Merchant extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->aauth->control('report/merchant');
        $this->lang->load('merchant', settings('language'));
        $this->load->model('reports_model', 'reports');
        $this->data['menu'] = 'report_merchant';
        $this->data['body_class'] = 'has-detached-left';
    }

    public function index() {
        $this->breadcrumbs->unshift(lang('report'), '/');
        $this->breadcrumbs->push(lang('report_merchant'), '/reports/merchant');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
//        $this->data['datas'] = $this->reports->sales_merchant(date('Y-m-d'), date('Y-m-d'));
        $this->template->_init();
        $this->template->form();
        $this->template->table();
        $this->load->js('../assets/backend/js/modules/reports.js');
        $this->output->set_title(lang('heading'));
        $this->data['merchants'] = $this->reports->get_all_merchant();
        $this->load->view('merchant/index', $this->data);
    }

    public function data() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $input = $this->input->post(null, true);
        $filter = $input['filter'];
        $output['data'] = array();
        $datas = $this->reports->sales_merchant($filter['from'], $filter['to'], $input['start'], $input['length'], $input['order'][0], $filter['merchants']);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    '<a href="'. site_url('merchants/view/'. encode($data->id)).'" target="_blank">' . $data->name . '</a>',
                    $data->stock_product != '' ? $data->stock_product : ' - ',
                    ($data->type == 'merchant') ? 'Merchant' : 'Cabang',
                    number($data->pending_payment),
                    number($data->pending_process),
                    number($data->on_process),
                    number($data->on_shipment),
                    number($data->finish),
                    number($data->cancel),
                );
            }
        }
        $output['recordsTotal'] = $this->main->count('merchants');
        $output['recordsFiltered'] = $this->main->count('merchants', array('id' => $filter['merchants']));
        echo json_encode($output);
    }

}
