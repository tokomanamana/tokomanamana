<?php

defined('BASEPATH') or exit('No direct script access allowed!');

require '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

class Sales extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->aauth->control('report/sales');
        $this->lang->load('sales', settings('language'));
        $this->load->model('reports_model', 'reports');
        $this->data['menu'] = 'report_sales';
        $this->data['body_class'] = 'has-detached-left';
    }

    public function index() {
        $this->breadcrumbs->unshift(lang('report'), '/');
        $this->breadcrumbs->push(lang('report_sales'), '/reports/sales');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->template->_init();
        $this->template->form();
        $this->template->table();
        $this->load->js('../assets/backend/js/modules/reports.js');
        $this->output->set_title(lang('heading'));
        $this->load->view('sales/index', $this->data);
    }
    
    public function data() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $input = $this->input->post(null, true);
        $filter = $input['filter'];
        $output['data'] = array();
        // $datas = $this->reports->sales($input['start'], $input['length'], $input['order'][0], $filter['from'], $filter['to'],$filter['order_status'],$filter['payment_status'],$filter['shipping'],$filter['zone'],isset($filter['city'])?$filter['city']:'');
        $datas = $this->reports->sales($input['start'], $input['length'], $input['order'][0], $filter['from'], $filter['to'],$filter['order_status'],$filter['payment_status'],$filter['shipping'],isset($filter['city'])?$filter['city']:'',$filter['merchants']);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $coupon = $this->reports->get_coupon($data->order);
                if ($coupon) {
                    $invoice = $this->main->gets('order_invoice', array('order' => $data->order));
                    if ($invoice) {
                        $total_invoice_without_shipping = 0;
                        foreach ($invoice->result() as $inv) {
                            $total_invoice_without_shipping += $inv->subtotal;
                        }

                        $n = ($coupon->amount / $total_invoice_without_shipping) * 100;
                        $percentage = 100 - $n;
                        $discount_ammount = $data->subtotal * $percentage / 100;
                        $discount_ammount = $data->subtotal - $discount_ammount;
                    } else {
                        $percentage = 0;
                        $discount_ammount = 0;
                    }
                } else {
                    $discount_ammount = 0;
                }
                // $shipping = explode('-', $data->shipping_courier);
                if($data->payment_method == 'transfer'){
                    $tot = $data->total + $data->unique_code;
                } else {
                    $tot = $data->total;
                }
//                $shipping = explode('-', $data->shipping_courier);
                $output['data'][] = array(
                    '<a href="'. site_url('orders/view/'. encode($data->order)).'" target="_blank">' . $data->invoice . '</a>',
                    get_date($data->date),
                    $data->customer,
                    number($data->subtotal),
                    number($discount_ammount),
                    $coupon ? $coupon->code : '', 
                    number($tot),
                    $data->payment_status,
                    $data->payment_method,
                    $data->order_status,
                    strtoupper($data->shipping_courier),
                    $data->merchant_name,
                    $data->city_type . ' ' . $data->city
                );
            }
        }
        $output['recordsTotal'] = $this->main->count('order_invoice');
        // $output['recordsFiltered'] = $this->reports->sales_count($filter['from'], $filter['to'],$filter['order_status'],$filter['payment_status'],$filter['shipping'],$filter['zone'],isset($filter['city'])?$filter['city']:'');
        $output['recordsFiltered'] = $this->reports->sales_count($filter['from'], $filter['to'],$filter['order_status'],$filter['payment_status'],$filter['shipping'],isset($filter['city'])?$filter['city']:'',$filter['merchants']);
        echo json_encode($output);
    }

    public function export() {
        $filename = "Laporan Penjualan";
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        foreach(range('A','O') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }
        \PhpOffice\PhpSpreadsheet\Shared\File::setUseUploadTempDirectory(true);
        $sheet->fromArray(array('No. Invoice', 'Tanggal', 'Pelanggan', 'Subtotal', 'Potongan', 'Kupon', 'Total', 'Pembayaran', 'Pembayaran Via', 'Status', 'Pengiriman', 'Toko', 'Kota / Kabupaten'), NULL, 'A1');
        $filter = $this->input->get(null, true);
        if ($filter['city'] == 'undefined') {
            $filter['city'] = '';
        }
        $fullFileName = "${filename}_".$filter['from'].'_'.$filter['to'];
        $datas = $this->reports->sales_export($filter['from'], $filter['to'],$filter['order_status'],$filter['payment_status'],$filter['shipping'], $filter['merchant'], $filter['city']);
        $i = 2;
        if ($datas->num_rows() > 0) {
            foreach ($datas->result() as $data) {
                $coupon = $this->reports->get_coupon($data->order);
                if ($coupon) {
                    $invoice = $this->main->gets('order_invoice', array('order' => $data->order));
                    if ($invoice) {
                        $total_invoice_without_shipping = 0;
                        foreach ($invoice->result() as $inv) {
                            $total_invoice_without_shipping += $inv->subtotal;
                        }

                        $n = ($coupon->amount / $total_invoice_without_shipping) * 100;
                        $percentage = 100 - $n;
                        $discount_ammount = $data->subtotal * $percentage / 100;
                        $discount_ammount = $data->subtotal - $discount_ammount;
                    } else {
                        $percentage = 0;
                        $discount_ammount = 0;
                    }
                } else {
                    $discount_ammount = 0;
                }

                if($data->payment_method == 'transfer'){
                    $tot = $data->total + $data->unique_code;
                } else {
                    $tot = $data->total;
                }
                $row_data = array(
                    $data->invoice,
                    get_date($data->date),
                    $data->customer,
                    $data->subtotal,
                    $discount_ammount,
                    $coupon ? $coupon->code : '',
                    $tot,
                    $data->payment_status,
                    $data->payment_method,
                    $data->order_status,
                    strtoupper($data->shipping_courier),
                    $data->merchant_name,
                    $data->city_type . ' ' . $data->city
                );
                $sheet->fromArray($row_data, null, 'A' . $i);
                $i++;
            }
        };
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $fullFileName . '.xls"');
        header('Cache-Control: max-age=0');
        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save('php://output');
    }

}
