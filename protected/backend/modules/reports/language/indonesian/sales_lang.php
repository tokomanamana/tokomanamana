<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['heading'] = 'Laporan Penjualan';
$lang['daily_heading'] = 'Penjualan Harian';
$lang['product_heading'] = 'Penjualan Produk';

$lang['daily'] = 'Harian';
$lang['monthly'] = 'Bulanan';
$lang['product'] = 'Produk';
$lang['city'] = 'Kota';
$lang['zone'] = 'Zona';
$lang['merchant'] = 'Merchant';
$lang['branch'] = 'Cabang';

$lang['date'] = 'Tanggal';
$lang['total_transaction'] = 'Jml. Transaksi';
$lang['total_product'] = 'Jml. Produk';
$lang['total_sales'] = 'Total Penjualan';
$lang['total_invoice'] = 'Invoice';
$lang['product_name'] = 'Nama Produk';

$lang['principle_heading'] = 'Laporan Principal';

$lang['brand_image_th'] = 'Gambar';
$lang['brand_name_th'] = 'Nama';

$lang['actions_th'] = 'Opsi';