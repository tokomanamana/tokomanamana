<?php

    defined('BASEPATH') or exit('No direct script access allowed!');

    class Principle_model extends CI_Model {
        public function get_brand ($start = 0, $length, $search = '', $order = array()) {
            $this->db->select('*')
                    ->limit($length, $start);
            $this->_where_brand($search);
            if ($order) {
                $col = $order['column'];
                switch ($order['column']) {
                    case 0: $key = 'name';
                        break;
                }
            }
            
            return $this->db->get('principles');
        }

        private function _get_alias_brand ($key) {
            switch ($key) {
                case 0: $key = 'name';
                    break;
            }
            return $key;
        }

        public function count_all ($search = '') {
            $this->_where_brand($search);
            return $this->db->count_all_results('principles');
        }

        private function _where_brand($search = '') {
            $columns = array('name');
            if ($search) {
                foreach ($columns as $column) {
                    $this->db->or_like('IFNULL('. $column .',"")', $search);
                }
            }
        }

        public function reports ($invoices, $principal, $product, $start = 0, $length, $order = array(), $from, $to, $order_status = '', $payment_status = '', $shipping = '', $branch = '', $city = '') {
            $this->db->select('o.id order, oi.id, oi.code invoice, o.code, p.name product, p.store_id as principal, p.category, o.payment_method, cst.fullname customer, op.total, oi.shipping_courier, o.date_added date, c.name city, mg.name merchant_group, sos.name order_status, o.payment_status, op.price, op.quantity, m.name as merchant_name, c.name as city_name, c.type as city_type, oi.subtotal')
                    ->join('orders o', 'o.id = oi.order', 'left')
                    ->join('order_product op', 'op.invoice = oi.id', 'left')
                    ->join('products p', 'op.product = p.id', 'left')
                    ->join('customers cst', 'cst.id = o.customer', 'left')
                    ->join('setting_order_status sos', 'sos.id = oi.order_status', 'left')
                    ->join('cities c', 'c.id = o.shipping_city', 'left')
                    ->join('merchants m', 'm.id = oi.merchant', 'left')
                    ->join('merchant_groups mg', 'mg.id = m.group', 'left')
                    ->where('DATE(o.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'')
                    // ->where('p.brand', $brand)
                    // ->where('p.store_type', 'principal')
                    // ->where('p.store_id', $principal)
                    ->where('m.principal_id', $principal)
                    ->where_in('oi.id', $invoices)
                    ->limit($length, $start);
            if ($product) 
                $this->db->where('p.id', $product);
            if ($order_status)
                $this->db->where('oi.order_status', $order_status);
            if ($payment_status)
                $this->db->where('o.payment_status', $payment_status);
            if ($shipping)
                $this->db->like('oi.shipping_courier', $shipping);
            if ($branch)
                $this->db->where('m.id', $branch);
            if ($city) {
                // $city = array_filter(explode(',', $city));
                $this->db->where('o.shipping_city', $city);
            }
            if ($order) {
                $col = $order['column'];
                switch ($order['column']) {
                    case 0: $col = 'invoice';
                        break;
                    case 1: $col = 'product';
                        break;
                    case 2: $col = 'date';
                        break;
                    case 3: $col = 'customer';
                        break;
                    case 4: $col = 'subtotal';
                        break;
                    case 5: $col = 'quantity';
                        break;
                    case 8: $col = 'total';
                        break;
                    case 9: $col = 'payment_status';
                        break;
                    case 10: $col = 'payment_method';
                        break;
                    case 11: $col = 'order_status';
                        break;
                    case 12: $col = 'shipping_courier';
                        break;
                    case 13: $col = 'merchant_name';
                        break;
                    case 14: $col = 'city_name';
                        break;
                }
                $this->db->order_by($col, $order['dir']);
            }
            return $this->db->get('order_invoice oi');
        }

        public function reports_total ($invoices, $principal, $product = '', $from, $to, $order_status = '', $payment_status = '', $shipping = '', $zone = '', $city = '') {
            $this->db->join('orders o', 'o.id = oi.order', 'left')
                    ->join('order_product op', 'op.invoice = oi.id', 'left')
                    ->join('products p', 'op.product = p.id', 'left')
                    ->join('customers cst', 'cst.id = o.customer', 'left')
                    ->join('setting_order_status sos', 'sos.id = oi.order_status', 'left')
                    ->join('cities c', 'c.id = o.shipping_city', 'left')
                    ->join('merchants m', 'm.id = oi.merchant', 'left')
                    ->join('merchant_groups mg', 'mg.id = m.group', 'left')
                    ->where('DATE(o.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'')
                    // ->where('p.store_type', 'principal')
                    // ->where('p.store_id', $principal)
                    ->where('m.principal_id', $principal)
                    ->where_in('oi.id', $invoices);
            if ($product) 
                $this->db->where('p.id', $product);
            if ($order_status)
                $this->db->where('oi.order_status', $order_status);
            if ($payment_status)
                $this->db->where('o.payment_status', $payment_status);
            if ($shipping)
                $this->db->like('oi.shipping_courier', $shipping);
            if ($zone)
                $this->db->where('m.group', $zone);
            if ($city) {
                $city = array_filter(explode(',', $city));
                $this->db->where_in('o.shipping_city', $city);
            }
            return $this->db->count_all_results('order_invoice oi');
        }
        
        public function reports_exports ($invoices, $principal, $product, $from, $to, $order_status = '', $payment_status = '', $shipping = '', $branch = '', $city = '') {
            $this->db->select('o.id order, oi.id, oi.code invoice, o.code, p.name product, p.store_id as principal, p.category, o.payment_method, cst.fullname customer, op.total, oi.shipping_courier, o.date_added date, c.name city, mg.name merchant_group, sos.name order_status, o.payment_status, op.price, op.quantity, m.name as merchant_name, c.name as city_name, c.type as city_type, oi.subtotal')
                    ->join('orders o', 'o.id = oi.order', 'left')
                    ->join('order_product op', 'op.invoice = oi.id', 'left')
                    ->join('products p', 'op.product = p.id', 'left')
                    ->join('customers cst', 'cst.id = o.customer', 'left')
                    ->join('setting_order_status sos', 'sos.id = oi.order_status', 'left')
                    ->join('cities c', 'c.id = o.shipping_city', 'left')
                    ->join('merchants m', 'm.id = oi.merchant', 'left')
                    ->join('merchant_groups mg', 'mg.id = m.group', 'left')
                    ->where('DATE(o.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'')
                    // ->where('p.brand', $brand)
                    // ->where('p.store_type', 'principal')
                    // ->where('p.store_id', $principal)
                    ->where('m.principal_id', $principal)
                    ->where_in('oi.id', $invoices);
            if ($product) 
                $this->db->where('p.id', $product);
            if ($order_status)
                $this->db->where('oi.order_status', $order_status);
            if ($payment_status)
                $this->db->where('o.payment_status', $payment_status);
            if ($shipping)
                $this->db->like('oi.shipping_courier', $shipping);
            if ($branch)
                $this->db->where('m.id', $branch);
            if ($city) {
                // $city = array_filter(explode(',', $city));
                $this->db->where('o.shipping_city', $city);
            }
            return $this->db->get('order_invoice oi');
        }

        public function get_coupon ($order) {
            $query = $this->db->select('coupon_history.amount, coupons.coupon_type, coupons.code, coupons.type, coupons.for_product, coupons.zonaIds, coupons.catIds')
                        ->join('coupons', 'coupon_history.coupon = coupons.id', 'left')
                        ->where('coupon_history.order', $order)
                        ->get('coupon_history');
            return ($query->num_rows() > 0) ? $query->row() : false;
        }
    }