<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Principle_model extends CI_Model {

    // function get($id) {
    //     $this->db->select('b.*, keyword seo_url,au.username as fullname, au.email,au.phone')
    //             ->join('seo_url su', "su.query = CONCAT('principle/view/',b.id)",'left')
    //             ->join('aauthp_users au', 'b.auth = au.id', 'left')
    //             ->where('b.id', $id);
    //     $query = $this->db->get('principles b');
    //     return ($query->num_rows() > 0) ? $query->row() : false;
    // }

    function get($id) {
        $this->db->select('b.*,au.fullname as fullname, au.email,au.phone')
                ->join('aauthp_users au', 'b.auth = au.id', 'left')
                ->where('b.id', $id);
        $query = $this->db->get('principles b');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }
    
    // function get_all($start = 0, $length, $search = '', $order = array()) {
    //     $this->where_like($search);
    //     if ($order) {
    //         $order['column'] = $this->get_alias_key($order['column']);
    //         $this->db->order_by($order['column'], $order['dir']);
    //     }
    //     $this->db->select('b.*, su.keyword seo_url')
    //             ->join('seo_url su', "su.query = CONCAT('principle/view/',b.id)",'left')
    //             ->limit($length, $start);

    //     return $this->db->get('principles b');
    // }

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->where_like($search);
        if ($order) {
            $order['column'] = $this->get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('b.*, au.fullname as fullname')
                ->join('aauthp_users au', 'au.id = b.auth', 'left')
                ->limit($length, $start);
        $this->db->select('(SELECT COUNT(m.id) FROM merchants m WHERE m.principal_id = b.id) as count_merchant');
        $this->db->select('(SELECT SUM(m.cash_balance) FROM merchants m WHERE m.principal_id = b.id) as cash_balance');

        return $this->db->get('principles b');
    }

    function get_alias_key($key) {
        switch ($key) {
            case 1: $key = 'name';
                break;
            case 2: $key = 'fullname';
                break;
            case 3: $key = 'status';
                break;
            case 4: $key = 'count_merchant';
                break;
            // case 5: $key = 'cash_balance';
            //     break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->where_like($search);
        // return $this->db->join('seo_url su', "su.query = CONCAT('principle/view/',b.id)",'left')->count_all_results('principles b');
        return $this->db->join('aauthp_users au', 'au.id = b.auth', 'left')->count_all_results('principles b');
    }

    function where_like($search = '') {
        $columns = array('name', 'fullname');
        if ($search) {
            foreach ($columns as $column) {
                $this->db->like('IFNULL(' . $column . ',"")', $search);
            }
        }
    }

    function get_branches_all($start = 0, $length, $search = '', $order = array(), $principal = '') {
        $this->branches_where_like($search);
        if ($order) {
            $order['column'] = $this->branches_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('m.id, m.name as branch_name, c.name as city_name')
                    ->join('cities c', 'c.id = m.city', 'left')
                    ->where('m.principal_id', $principal)
                    ->limit($length, $start);
        return $this->db->get('merchants m');
    }

    function branches_where_like($search = '') {
        $columns = array('m.name', 'c.name');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
            $this->db->group_end();
        }
    }

    function branches_get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'branch_name';
                break;
            case 1: $key = 'city_name';
                break;
        }
        return $key;
    }

    function count_all_branches($search = '',$principal = '') {
        $this->branches_where_like($search);
        // $this->db->select('p.name,pps.quantity')
        // ->join('products_principal_stock pps', 'pps.product_id = p.id')

        // ->where('pps.branch_id',$principal)
        // ->where('pps.quantity >',0);
        // return $this->db->count_all_results('products p');

        $this->db->select('m.id, m.name as branch_name, c.name as city_name')
                ->join('cities c', 'c.id = m.city', 'left')
                ->where('m.principal_id', $principal);
        return $this->db->count_all_results('merchants m');
    }

}
