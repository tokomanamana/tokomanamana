<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Principle extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->aauth->control('principle');

        $this->lang->load('principle', settings('language'));
        $this->load->model('principle_model', 'principle');
        
        $this->load->library('aauth');
        $this->config->load('aauth', TRUE);
        $v = $this->config->item("aauth");
        $v['users'] = 'aauthp_users';
        $v['groups'] = 'aauthp_groups';
        $v['group_to_group'] = 'aauthp_group_to_group';
        $v['user_to_group'] = 'aauthp_user_to_group';
        $v['perms'] = 'aauthp_perms';
        $v['perm_to_group'] = 'aauthp_perm_to_group';
        $v['perm_to_user'] = 'aauthp_perm_to_user';
        $v['pms'] = 'aauthp_pms';
        $v['user_variables'] = 'aauthp_user_variables';
        $v['login_attempts'] = 'aauthp_login_attempts';
        //$this->config->set_item("aauth", $v);
        $this->data['v'] = $v;
        $this->aauth->change_config($this->data['v']);
        $this->data['menu'] = 'principle_list';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('principle'), '/');
        $this->breadcrumbs->push(lang('principle'), '/');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->load->js(site_url('../assets/backend/js/modules/principle/list.js'));
        $this->output->set_title(lang('principle_heading'));
        $this->load->view('principle/list', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->principle->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    // ($data->image) ? '<a href="#"><img src="' . site_url('../files/images/' . $data->image) . '" class="img-circle img-xs"></a>' : '',
                    $data->name,
                    $data->fullname,
                    lang('status_' . $data->status),
                    '<a href="javascript:void(0)" onclick="seeBranches(\'' . encode($data->id) . '\')">' . $data->count_merchant . '</a>',
                    // rupiah($data->cash_balance),
                    '<td class="text-center">
                    <ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">' . 
                    ($this->aauth->is_allowed('principle/edit') ? '<li><a href="' . site_url('principle/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>' : '') . 
                    ($this->aauth->is_allowed('principle/delete') ? '<li><a href="' . site_url('principle/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>' : '') .
                    '
                    </ul>
                    </li>
                    </ul>
                    </td>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->principle->count_all();
        $output['recordsFiltered'] = $this->principle->count_all($search);
        echo json_encode($output);
    }

    public function form($id = '') {
        $this->template->_init();
        $this->template->form();

        $this->load->js('../assets/backend/js/modules/principle/form.js');

        $this->data['data'] = array();
        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('principle'), '/principle');

        if ($id) {
            $id = decode($id);
            $this->aauth->control('principle/edit');
            $this->data['data'] = $this->principle->get($id);
            $this->data['data']->description = str_replace("<br />", "\n", $this->data['data']->description); 
            $this->breadcrumbs->push(lang('principle_edit_heading'), '/principle/form/' . encode($id));
            $this->breadcrumbs->push($this->data['data']->name, '/');
        } else {
            $this->breadcrumbs->push(lang('principle_add_heading'), '/principle/form');
        }
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(($this->data['data']) ? lang('principle_edit_heading') : lang('principle_add_heading'));
        $this->load->view('principle/form', $this->data);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'lang:principle_form_name_label', 'trim|required');
        // $this->form_validation->set_rules('seo_url', 'lang:principle_form_seo_url_label', 'trim|alpha_dash');
        
        if (!$this->input->post('id')) {
            $this->form_validation->set_rules('email', 'lang:email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'lang:password', 'trim|required');
        }

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);
            if(!isset($data['status'])) {
                $data['status'] = 0;
            }

            do {
                if ($data['id']) {
                    $data['id'] = decode($data['id']);
                    $data['meta_description'] = $data['description'];
                    $data['meta_title'] = $data['name'];
                    $data['meta_keyword'] = $data['name'];
                    $data['description'] = str_replace("\n", "<br />",  $data['description']); 
                    $principle = $this->main->get('principles', array('id' => $data['id']));
                    $auth = $principle->auth;
                    $data_auth = array('fullname' => $data['fullname'],'phone' => $data['phone']);
                    if (isset($data['password']) && $data['password'] != '') {
                        if(strlen($data['password']) < 5) {
                            $return = [
                                'message' => 'Minimal password 5 karakter',
                                'status' => 'error'
                            ];
                            break;
                        } else {
                            $data_auth['pass'] = $this->aauth->hash_password($data['password'], $auth);
                        }
                    }
                    // $this->aauth->update_user($auth, FALSE, ($data_auth['password']) ? $data_auth['password'] : FALSE, $data_auth['fullname'], 1 ,$data['phone']);
                    $this->main->update('aauthp_users', $data_auth, ['id' => $auth]);
                    //$this->ion_auth->update($auth, $data_auth);
                    unset($data['fullname']);
                    unset($data['phone']);
                    unset($data['password']);
                    $save = $this->main->update('principles', $data, array('id' => $data['id']));
                    //$save = $this->main->update('principle', $data, array('id' => $data['id']));
                    
                }
                
                // $url = 'principle/view/' . $data['id'];
                // if ($seo_url = $data['seo_url']) {
                //     if (check_url($seo_url, $url)) {
                //         $return = array('message' => lang('principle_friendly_url_exist_message'), 'status' => 'error');
                //         break;
                //     }
                // } else {
                //     $seo_url = $url;
                // }
                // unset($data['seo_url']);

//                 if (!$data['id']) {

//                     $user = $this->aauth->create_user($data['email'], $data['password'], $data['fullname'], 1 ,$data['phone']);
//                     //var_dump($user);exit();
//                     if ($user) {
//                         unset($data['email']);
//                         unset($data['password']);
//                         unset($data['fullname']);
//                         unset($data['phone']);
//                         $data['auth'] = $user;
//                         //$save = $this->main->insert('merchants', $data);
// //                        $this->main->insert('seo_url', array('keyword' => $data['username'], 'query' => 'merchants/view/' . $this->db->insert_id()));
//                     } else {
//                         $return = array('message' => $this->aauth->print_errors(), 'status' => 'error');
//                         break;
//                     }
//                     $id = $this->main->insert('principles', $data);
//                     $url .= $id;
//                 } else {
//                     // $principle = $this->main->get('principles', array('id' => $data['id']));
//                     // $auth = $principle->auth;
//                     // $data_auth = array('fullname' => $data['fullname'],'phone' => $data['phone']);
//                     // if (isset($data['password'])) {
//                     //     $data_auth['password'] = $data['password'];
//                     //     unset($data['password']);
//                     // }
//                     // $this->aauth->update_user($data['id'], FALSE, FALSE, $data['fullname'], 1 ,$data['phone']);
//                     // //$this->ion_auth->update($auth, $data_auth);
//                     // unset($data['fullname']);
//                     // unset($data['phone']);
//                     // $save = $this->main->update('principles', $data, array('id' => $data['id']));
//                     // //$save = $this->main->update('principle', $data, array('id' => $data['id']));
//                     // $this->main->delete('seo_url', array('query' => $url));
//                 }

                $return = array('message' => sprintf(lang('principle_save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('principle'));
            } while (0);
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->aauth->control('principle/delete');

        $id = decode($id);
        $data = $this->main->get('principles', array('id' => $id));
        $delete = $this->main->delete('principles', array('id' => $id));
        $auth = $this->main->get('aauthp_users', ['id' => $data->auth]);
        $this->main->delete($this->data['v']['users'], array('id' => $data->auth));
        $this->main->delete($this->data['v']['user_to_group'], array('user_id' => $data->auth));
        $this->main->delete('products_principal_history', ['principal_id' => $id]);
        $this->main->delete('seo_url', ['query' => 'principal_home/view/' . $id]);
        $brands = $this->main->gets('brands', ['principle_id' => $id]);
        $products = $this->main->gets('products', ['store_id' => $id, 'store_type' => 'principal']);
        $branches = $this->main->gets('merchants', ['principal_id' => $id, 'type' => 'principle_branch']);
        if($products) {
            if($products->num_rows() > 0) {
                foreach($products->result() as $product) {
                    // $this->main->delete('products_principal_stock', ['product_id' => $product->id]);
                    $product_branch = $this->main->gets('products_principal_stock', ['product_id' => $product->id]);
                    if($product_branch) {
                        if($product_branch->num_rows() > 0) {
                            foreach ($product_branch->result() as $product_) {
                                $this->main->delete('seo_url', ['query' => 'catalog/products/view/' . $product_->product_id . '/' . $product_->branch_id]);
                                $this->main->delete('products_principal_stock', ['id' => $product_->id]);
                            }
                        }
                    }
                    $this->main->delete('product_option', ['product' => $product->id]);
                    $this->main->delete('product_price_level', ['product' => $product->id]);
                    $this->main->delete('product_price_reseller', ['product' => $product->id]);
                    $this->main->delete('product_image', ['product' => $product->id]);
                    $this->main->delete('product_last_review', ['product_id' => $product->id]);
                }
            }
        }
        if($branches) {
            if($branches->num_rows() > 0) {
                foreach($branches->result() as $branch) {
                    $this->main->delete('merchants', ['id' => $branch->id]);
                    $this->main->delete('merchant_users', ['id' => $branch->auth]);
                    $this->main->delete('seo_url', ['query' => 'merchant_home/view/' . $branch->id]);
                }
            }
        }
        if($brands) {
            if($brands->num_rows() > 0) {
                foreach($brands->result() as $brand) {
                    $brand_img = FCPATH . '../files/images/' . $brand->image;
                    if($brand_img) {
                        unlink($brand_img);
                    }
                    $this->main->delete('brands', ['id' => $brand->id]);
                    $this->main->delete('seo_url', ['query' => 'catalog/brands/view/' . $brand->id]);
                }
            }
        }
        $dir = FCPATH .'../files/images/principal/'.$auth->username;
        if(is_dir($dir)) {
            array_map('unlink', glob("$dir/*.*"));
            rmdir($dir);
        }
        if ($delete) {
            $this->main->delete('seo_url', array('query' => 'principle/view/' . $data->id));
            $return = array('message' => sprintf(lang('principle_delete_success_message'), $data->name), 'status' => 'success');
        } else {
            $return = array('message' => lang('principle_delete_error_message'), 'status' => 'error');
        }

        echo json_encode($return);
    }

    public function get_branches() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $principal_id = decode($this->input->post('id'));
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        // $status = isset($this->input->get()['status']) ? $this->input->get()['status'] : NULL;
        // if($status != NULL){
        //     $this->db->where('m.status',$status);
        // }
        $datas = $this->principle->get_branches_all($start, $length, $search, $order,$principal_id);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->branch_name,
                    $data->city_name,
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->principle->count_all_branches('',$principal_id);
        $output['recordsFiltered'] = $this->principle->count_all_branches($search,$principal_id);
        echo json_encode($output);
    }

}
