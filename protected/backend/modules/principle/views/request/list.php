<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('principle_heading'); ?></h2>
            </div>

            <div class="heading-elements">

            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <table class="table" id="table_principal" data-url="<?php echo $url_ajax; ?>">
                <thead>
                    <tr>
                        <th class="default-sort" data-sort="asc"><?php echo lang('principle_company_th'); ?></th>
                        <th><?php echo lang('principle_owner_th'); ?></th>
                        <th><?php echo lang('principle_phone_th'); ?></th>
                        <th>Status</th>
                        <th class="no-sort text-center" style="width: 20px;"><?php echo lang('actions_th'); ?></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>