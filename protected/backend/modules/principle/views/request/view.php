<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('view_principle') . ' ' . $data->company_name; ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <!--<a href="<?php echo site_url('orders/form'); ?>" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span><?php echo lang('order_add_heading'); ?></span></a>-->
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-5">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th colspan="2">Principal Data</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?php echo lang('req_brand_name'); ?></td>
                                    <td><?php echo $data->brand_name; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo lang('req_company_name'); ?></td>
                                    <td><?php echo $data->company_name; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo lang('req_owner_name'); ?></td>
                                    <td><?php echo $data->owner_name; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo lang('req_owner_email'); ?></td>
                                    <td><?php echo $data->owner_email; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo lang('req_owner_phone'); ?></td>
                                    <td><?php echo $data->owner_phone; ?></td>
                                </tr>

                                <tr>
                                    <td><?php echo lang('req_date_added'); ?></td>
                                    <td><?php echo get_date_time($data->date_added); ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-7">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th colspan="2">Detail</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?php echo lang('req_company_type'); ?></td>
                                    <td><?php echo $data->company_type; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo lang('req_main_category'); ?></td>
                                    <td><?php echo $data->main_category; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo lang('req_sku'); ?></td>
                                    <td><?php echo $data->sku_label; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo lang('req_revenue'); ?></td>
                                    <td><?php echo $data->revenue_label; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo lang('req_document'); ?></td>
                                    <td>
                                        <ul>
                                            <?php $documents = explode(', ', $data->document); ?>
                                            <?php foreach ($documents as $document) : ?>
                                                <?php if ($document == " " || $document == "") : ?>
                                                <?php else : ?>
                                                    <li>
                                                        <?php if($document == 'haki') : ?>
                                                            Hak Merek Dagang (HAKI)
                                                        <?php elseif($document == 'npwp') : ?>
                                                            NPWP Perusahaan
                                                        <?php elseif($document == 'akta') : ?>
                                                            Akta Perusahaan
                                                        <?php else : ?>
                                                            <?php echo strtoupper($document) ?>
                                                        <?php endif; ?>
                                                    </li>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </ul>

                                    </td>
                                </tr>
                                <tr>
                                    <td><?php echo lang('req_instagram'); ?></td>
                                    <td><?php echo $data->instagram_acc; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo lang('req_ecommerce'); ?></td>
                                    <td><?php echo $data->other_ecommerce; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo lang('req_price'); ?></td>
                                    <td><?php echo $data->price_label; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo lang('req_know_from'); ?></td>
                                    <td><?php echo $data->from_label; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo lang('req_custom_from'); ?></td>
                                    <td>
                                        <?php if ($data->custom_know_form == null || $data->custom_know_form == "") : ?>
                                            -
                                        <?php else : ?>
                                            <?= $data->custom_know_form ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="row">
                    <h4 class="col-md-12">
                        <b>File Dokumen</b>
                    </h4>
                </div>
                <div class="row">
                    <?php $files = json_decode($data->document_file); ?>
                    <?php foreach ($files as $key => $file) : ?>
                        <?php 
                        $type_file = explode('.', $file);
                        ?>
                        <?php if ($file == "") : ?>
                        <?php else : ?>
                            <div class="col-md-3">
                                <?php if($key == 'haki') : ?>
                                    Hak Merek Dagang (HAKI)
                                <?php elseif($key == 'npwp') : ?>
                                    NPWP Perusahaan
                                <?php elseif($key == 'akta') : ?>
                                    Akta Perusahaan
                                <?php else : ?>
                                    <?php echo strtoupper($key) ?>
                                <?php endif; ?>
                            </div>
                            <?php if($type_file[1] == 'jpg') : ?>
                                <div class="col-md-9" id="image-preview">
                                    <div class="thumbnail" style="width: 50%;">
                                        <div class="thumb">
                                            <a href="<?php echo site_url('../files/principal_register/' . $file); ?>" data-lightbox="roadtrip">
                                                <img src="<?php echo site_url('../files/principal_register/' . $file); ?>">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            <?php elseif($type_file[1] == 'pdf') : ?>
                                <div class="col-md-9">
                                    <a href="<?php echo site_url('../files/principal_register/' . $file); ?>" target="_blank" style="width: 50%;background: #EEE;display: block;text-align: center;padding-top:10px;padding-bottom:10px;border-radius: 3px;margin-bottom: 20px;"><?php echo $file ?></a>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
                <?php if ($data->status == 0) : ?>
                    <div class="row">
                        <div class="col-md-2">
                            <button id="approve" class="btn btn-primary" data-id="<?= encode($data->id) ?>">Setuju</button>
                            <button id="reject" class="btn btn-danger">Tidak Setuju</button>
                        </div>
                    </div>
                <?php elseif ($data->status == 1) : ?>
                    <div class="row">
                        <div class="col-md-2">
                            <button class="btn btn-success" type="button">
                                Di terima <i class="icon-check position-right"></i>
                            </button>
                        </div>
                    </div>
                <?php elseif ($data->status == 3) : ?>
                    <?php
                    $now = time();
                    $email_sent_time = strtotime($data->email_sent_time);
                    $range = $now - $email_sent_time;
                    ?>
                    <?php if($range <= 500) : ?>
                        <div class="row">
                            <div class="col-md-2">
                                <button class="btn btn-info" type="button">
                                    Menunggu Konfirmasi
                                </button>
                            </div>
                        </div>
                    <?php else : ?>
                        <div class="row">
                            <div class="col-md-2">
                                <button id="resending" class="btn btn-primary" data-id="<?= encode($data->id) ?>">Kirim Ulang Email</button>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php else : ?>
                    <div class="row">
                        <div class="col-md-2">
                            <button class="btn btn-danger" type="button">
                                Di tolak <i class="icon-cross position-right"></i>
                            </button>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal Reject-->
<div class="modal fade" id="modal_rejected" tabindex="-1" role="dialog" aria-labelledby="modal_rejected_label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_rejected_label">Alasan Penolakan Principal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?php echo site_url('principle/request/rejected_principal') ?>" method="post" id="form">
                <input type="hidden" value="<?php echo encode($data->id) ?>" name="id">
                <div class="modal-body">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="styled options" name="documents_not_complete"> Dokumen Tidak Lengkap
                            <div id="documents_not_complete" style="display: none;margin-top: 10px;">
                                <span>Pilih dokumen apa saja yang tidak lengkap:</span>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="styled options" name="document_not_complete[]" value="Hak Merek Dagang (HAKI)"> Hak Merek Dagang (HAKI)
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="styled options" name="document_not_complete[]" value="NPWP Perusahaan"> NPWP Perusahaan
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="styled options" name="document_not_complete[]" value="SIUP (Surat Izin Usaha Perdagangan)"> SIUP (Surat Izin Usaha Perdagangan)
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="styled options" name="document_not_complete[]" value="Akta Perusahaan"> Akta Perusahaan
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="styled options" name="document_not_complete[]" value="SPPKP (Surat Pengukuhan Perusahaan Kena Pajak)"> SPPKP (Surat Pengukuhan Perusahaan Kena Pajak)
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="styled options" name="document_not_complete[]" value="SKT (Surat Keterangan Terdaftar)"> SKT (Surat Keterangan Terdaftar)
                                    </label>
                                </div>
                            </div>
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="styled options" name="documents_not_valid"> Dokumen Tidak Valid
                            <div id="documents_not_valid" style="display: none;margin-top: 10px;">
                                <span>Pilih dokumen apa saja yang tidak valid:</span>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="styled options" name="document_not_valid[]" value="Hak Merek Dagang (HAKI)"> Hak Merek Dagang (HAKI)
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="styled options" name="document_not_valid[]" value="NPWP Perusahaan"> NPWP Perusahaan
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="styled options" name="document_not_valid[]" value="SIUP (Surat Izin Usaha Perdagangan)"> SIUP (Surat Izin Usaha Perdagangan)
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="styled options" name="document_not_valid[]" value="Akta Perusahaan"> Akta Perusahaan
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="styled options" name="document_not_valid[]" value="SPPKP (Surat Pengukuhan Perusahaan Kena Pajak)"> SPPKP (Surat Pengukuhan Perusahaan Kena Pajak)
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="styled options" name="document_not_valid[]" value="SKT (Surat Keterangan Terdaftar)"> SKT (Surat Keterangan Terdaftar)
                                    </label>
                                </div>
                            </div>
                        </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary" id="btn_submit">Kirim</button>
                </div>
            </form>
        </div>
    </div>
</div>