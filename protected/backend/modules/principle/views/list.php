<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('principle_heading'); ?></h2>
            </div>

            <!-- <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('principle/form'); ?>" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span><?php echo lang('principle_add_heading'); ?></span></a>
                </div>
            </div> -->
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <table class="table" id="table_principal" data-url="<?php echo site_url('principle/get_list'); ?>">
                <thead>
                    <tr>
                        <!-- <th class="no-sort" style="width: 45px;"><?php echo lang('principle_image_th'); ?></th> -->
                        <th class="default-sort" data-sort="asc"><?php echo lang('principle_name_th'); ?></th>
                        <!-- <th><?php echo lang('principle_seo_url_th'); ?></th> -->
                        <th><?php echo lang('principle_owner_name_th') ?></th>
                        <th><?php echo lang('status') ?></th>
                        <th><?php echo lang('principle_count_merchant_th') ?></th>
                        <!-- <th><?php echo lang('principle_cash_balance_th') ?></th> -->
                        <th class="no-sort text-center" style="width: 20px;"><?php echo lang('actions_th'); ?></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="branchModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="branchModalLabel">Jumlah Cabang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="responsive-table">
                        <table class="table table-bordered" style="margin-bottom:2em;width:100%;" id="branches_table">
                            <thead>
                                <tr>
                                    <th>Nama Toko</th>
                                    <th>Kota</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>