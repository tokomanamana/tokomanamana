<div bgcolor="#FFFFFF" style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;width:100%!important;height:100%;font-size:14px;color:#404040;margin:0;padding:0">
    <table style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:0" bgcolor="transparent">
        <tbody>
            <tr style="margin:0;padding:0">
                <td style="margin:0;padding:0"></td>
                <td style="display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0" bgcolor="#FFFFFF">
                    <div style="max-width:600px;display:block;border-collapse:collapse;margin:0 auto;border:1px solid #e7e7e7">
                        <table style="max-width:100%;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:20px;" bgcolor="transparent">
                            <tbody>
                                <tr style="margin:0;padding:0">
                                    <td>
                                        <img src="<?php echo site_url('../assets/frontend/images/logo.png'); ?>" alt="" style="margin:10px 0; height: 50px;">
                                    </td>
                                </tr>

                                <tr style="margin:0;padding:0">
                                    <td style="margin:0;padding:0">
                                        <h5 style="line-height:32px;color:#666;font-weight:700;font-size:20px;margin:0 0 20px;padding:0">Pendaftaran Principal <?php echo $site_name ?> Ditolak!</h5>
                                        <p style="font-weight:normal;color:#999;font-size:14px;line-height:1.6;margin:0 0 20px;padding:0">
                                            Hai <?php echo $name; ?>,<br>
                                            Mohon maaf untuk pendaftaran Principal Tokomanamana tidak dapat dilakukan karena alasan berikut: <br><br>
                                            <?php if($not_valid == true && $not_complete == true) : ?>
                                                Terdapat dokumen yang tidak lengkap, yaitu:
                                                <ul>
                                                    <?php foreach($documents_not_complete as $document) : ?>
                                                        <li><?php echo $document ?></li>
                                                    <?php endforeach; ?>
                                                </ul>
                                                <br>
                                                Terdapat dokumen yang tidak valid, yaitu:
                                                <ul>
                                                    <?php foreach($documents_not_valid as $document) : ?>
                                                        <li><?php echo $document ?></li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            <?php elseif($not_valid == true) : ?>
                                                Terdapat dokumen yang tidak valid, yaitu:
                                                <ul>
                                                    <?php foreach($documents_not_valid as $document) : ?>
                                                        <li><?php echo $document ?></li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            <?php elseif($not_complete == true) : ?>
                                                Terdapat dokumen yang tidak lengkap, yaitu:
                                                <ul>
                                                    <?php foreach($documents_not_complete as $document) : ?>
                                                        <li><?php echo $document ?></li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            <?php endif; ?>
                                            <br>
                                            Silahkan mendaftarkan ulang akun anda di halaman login principal <?php echo $site_name ?>.
                                            <br><br>
                                            Terimakasih.
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="padding:0 20px">
                            <p style="font-size:12px;color:#999;padding:24px 0 10px;margin:0;border-top: 1px solid #E0E0E0;">
                                Email dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.
                            </p>
                        </div>
                    </div>
                </td>
                <td style="margin:0;padding:0"></td>
            </tr>
        </tbody>
    </table>
</div>