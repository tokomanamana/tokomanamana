<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo ($data) ? lang('post_edit_heading') : lang('post_add_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('blogs/posts'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('post_list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('blogs/posts/save'); ?>" class="form-horizontal" method="post" id="form">
                    <input type="hidden" name="id" value="<?php echo ($data) ? encode($data->id) : ''; ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#general" data-toggle="tab"><?php echo lang('post_form_general_tabs'); ?></a></li>
                                    <li class=""><a href="#seo" data-toggle="tab"><?php echo lang('post_form_seo_tabs'); ?></a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="general">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('post_form_title_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" required="" name="title" value="<?php echo ($data) ? $data->title : ''; ?>" <?php echo (!$data) ? 'onkeyup="convertToSlug(this.value);"' : ''; ?>>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('post_form_category_label'); ?></label>
                                            <div class="col-md-5">
                                                <select name="category" class="form-control bootstrap-select" required="">
                                                    <option value="">Pilih Kategori</option>
                                                    <?php if ($categories) foreach ($categories->result() as $category) { ?>
                                                            <option value="<?php echo $category->id; ?>" <?php echo ($data) ? ($category->id == $data->category) ? 'selected' : '' : ''; ?>><?php echo $category->name; ?></option>
                                                        <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('post_form_image_label'); ?></label>
                                            <?php if ($data && $data->image) { ?>
                                                <div class="col-md-3" id="image-preview">
                                                    <div class="thumbnail">
                                                        <div class="thumb">
                                                            <img src="<?php echo site_url('../files/images/' . $data->image); ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div class="col-md-6" id="add-image">
                                                <button type="button" id="add-image-btn" class="btn btn-default" data-toggle="modal" data-target="#filemanager" <?php echo ($data && $data->image) ? 'style="display:none;"' : ''; ?>><?php echo lang('button_add_image'); ?></button>
                                                <button type="button" id="edit-image-btn" class="btn btn-default" data-toggle="modal" data-target="#filemanager" <?php echo (!$data || ($data && !$data->image)) ? 'style="display:none;"' : ''; ?>><?php echo lang('button_edit_image'); ?></button>
                                                <button type="button" id="delete-image-btn" class="btn btn-default" <?php echo (!$data || ($data && !$data->image)) ? 'style="display:none;"' : ''; ?>><?php echo lang('button_delete_image'); ?></button>
                                                <input type="hidden" id="image" name="image" value="<?php echo ($data) ? $data->image : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('post_form_content_label'); ?></label>
                                            <div class="col-md-9">
                                                <textarea cols="30" rows="2" class="form-control tinymce" name="content"><?php echo ($data) ? $data->content : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('post_form_status_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="checkbox" name="status" value="1" data-on-text="<?php echo lang('yes'); ?>" data-off-text="<?php echo lang('no'); ?>" class="switch" <?php echo ($data) ? (($data->status == 1) ? 'checked' : '') : 'checked'; ?>>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="seo">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('post_form_seo_url_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="seo_url" name="seo_url" value="<?php echo ($data) ? $data->seo_url : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('post_form_meta_title_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="meta_title" placeholder="<?php echo lang('post_form_meta_title_placeholder'); ?>" value="<?php echo ($data) ? $data->meta_title : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('post_form_meta_description_label'); ?></label>
                                            <div class="col-md-9">
                                                <textarea cols="30" rows="2" class="form-control" name="meta_description" placeholder="<?php echo lang('post_form_meta_description_placeholder'); ?>"><?php echo ($data) ? $data->meta_description : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('post_form_meta_keyword_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" name="meta_keyword" class="tags-input" value="<?php echo ($data) ? $data->meta_keyword : ''; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('blogs/posts'); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- <input type="file" name="blog_image" id="blog_image" style="display: none;"> -->
<div id="filemanager" class="modal">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">File Manager</h5>
            </div>

            <div class="modal-body">
                <iframe  width="100%" height="550" frameborder="0" src="<?php echo base_url('filemanager/dialog.php?type=1&editor=false&field_id=image&relative_url=1'); ?>"></iframe>
            </div>
        </div>
    </div>
</div>