<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['order_heading'] = 'Pesanan';
$lang['order_list_heading'] = 'Daftar Pesanan';
$lang['order_add_heading'] = 'Tambah Pesanan';
$lang['order_edit_heading'] = 'Edit Pesanan';

$lang['order_code_th'] = 'ID';
$lang['order_date_th'] = 'Tanggal';
$lang['order_customer_th'] = 'Pelanggan';
$lang['order_status_th'] = 'Status';
$lang['order_total_th'] = 'Total';
$lang['order_payment_th'] = 'Pembayaran';
$lang['order_merchant_th'] = 'Toko';
$lang['order_subtotal_th'] = 'Subtotal';
$lang['order_coupon_th'] = 'Kupon';

$lang['order_detail_tabs'] = 'Detail';
$lang['order_invoice_tabs'] = 'Invoice';
$lang['order_shipping_tabs'] = 'Pengiriman';
$lang['order_id'] = 'ID Pesanan';
$lang['order_customer'] = 'Pelanggan';
$lang['order_phone'] = 'No. HP Pelanggan';
$lang['order_uniqcode'] = 'Kode Unik';
$lang['order_total'] = 'Total Pesanan';
$lang['order_total_uniqcode'] = 'Total Pesanan + Kode Unik';
$lang['order_date'] = 'Tanggal Pemesanan';

$lang['order_payment_detail'] = 'Detail Pembayaran';
$lang['order_payment_method'] = 'Cara Pembayaran';
$lang['order_transfer_code'] = 'Kode Transfer';
$lang['order_payment_status'] = 'Status';
$lang['order_payment_date'] = 'Tanggal';

$lang['order_shipping_address'] = 'Alamat Pengiriman';

$lang['order_invoice_code'] = 'Kode Invoice';
$lang['order_invoice_subtotal'] = 'Total Belanja';
$lang['order_invoice_total'] = 'Total Pembayaran';
$lang['order_invoice_notes'] = 'Catatan';
$lang['order_status_pay_th'] = 'Status Pembayaran';
$lang['order_invoice_status'] = 'Status';
$lang['order_invoice_shipping_courier'] = 'Pengiriman';
$lang['order_invoice_shipping_cost'] = 'Ongkos Kirim';
$lang['order_invoice_shipping_weight'] = 'Berat Pengiriman';
$lang['order_invoice_shipping_tracking'] = 'Resi Pengiriman';

$lang['order_product_name'] = 'Nama Produk';
$lang['order_product_code'] = 'SKU';
$lang['order_product_quantity'] = 'Jumlah';
$lang['order_product_price'] = 'Harga';
$lang['order_product_subtotal'] = 'Total harga';
$lang['order_product_weight'] = 'Berat';
$lang['order_product_option'] = 'Variasi';

$lang['order_history_time'] = 'Waktu';
$lang['order_history_status'] = 'Status';

$lang['order_button_paid'] = '<i class="icon-cash"></i> Pembayaran Diterima';

$lang['order_save_success_message'] = "Pesanan '%s' berhasil disimpan.";
$lang['order_save_error_message'] = "Pesanan '%s' gagal disimpan.";
$lang['order_delete_success_message'] = "Pesanan '%s' berhasil dihapus.";
$lang['order_delete_error_message'] = "Pesanan '%s' gagal dihapus.";