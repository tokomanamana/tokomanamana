<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Orders_model extends CI_Model
{

    function get($id)
    {
        $this->db->select('o.*, cst.fullname customer_name, p.name shipping_province_name, c.name shipping_city_name, d.name shipping_district_name')
            ->join('customers cst', 'cst.id = o.customer', 'left')
            ->join('provincies p', 'o.shipping_province = p.id', 'left')
            ->join('cities c', 'o.shipping_city = c.id', 'left')
            ->join('districts d', 'o.shipping_district = d.id', 'left')
            ->where('o.id', $id);
        $query = $this->db->get('orders o');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    // function get_all($start = 0, $length, $search = '', $order = array()) {
    //     $this->_where_like($search);
    //     if ($order) {
    //         $order['column'] = $this->_get_alias_key($order['column']);
    //         $this->db->order_by($order['column'], $order['dir']);
    //     }
    //     $this->db->limit($length, $start);

    //     return $this->db->get('orders o');
    // }

    // function get_all($start = 0, $length, $search = '', $order = array(), $merchant = null)
    // {
    //     $this->db->select('o.*');
    //     $this->_where_like($search);
    //     // $this->db->join('order_invoice', 'order_invoice.order = o.id', 'left');
    //     // $this->db->join('merchants', 'merchants.id = order_invoice.merchant', 'left');
    //     if ($merchant) {
    //         // $this->db->where('order_invoice.merchant', $merchant);
    //     }
    //     if ($order) {
    //         $order['column'] = $this->_get_alias_key($order['column']);
    //         $this->db->order_by($order['column'], $order['dir']);
    //     }
    //     $this->db->limit($length, $start);

    //     return $this->db->get('orders o');
    // }

    function get_all($start = 0, $length, $search = '', $order = array(), $to, $from, $payment_status = '', $payment_method = '') {
        $this->_where_like($search);
        $this->db->select('o.id, o.code,o.payment_method, o.date_added, o.shipping_name, o.subtotal, o.shipping_cost, o.disc_value, o.total, o.unique_code, o.payment_method, o.payment_status')
                ->where('DATE(o.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'')
                ->limit($length, $start);
        if ($payment_status)
            $this->db->where('o.payment_status', $payment_status);
        if ($payment_method)
            $this->db->where('o.payment_method', $payment_method);
        if ($order) {
            $col = $order['column'];
            switch ($order['column']) {
                case 0: $col = 'code';
                    break;
                case 1: $col = 'date_added';
                    break;
                case 2: $col = 'shipping_name';
                    break;
                case 3: $col = 'subtotal';
                    break;
                case 4: $col = 'disc_value';
                    break;
                case 5: $col = 'total';
                    break;
                case 6: $col = 'payment_method';
                    break;
                case 7: $col = 'payment_status';
                    break;
            }
            $this->db->order_by($col, $order['dir']);
        }
        return $this->db->get('orders o');
    }

    public function get_all_merchant()
    {
        return $this->db->query("SELECT id, name FROM merchants ORDER BY name ASC");
    }

    private function _get_alias_key($key)
    {
        switch ($key) {
            case 0:
                $key = 'o.code';
                break;
            case 1:
                $key = 'o.date_added';
                break;
            case 2:
                $key = 'o.shipping_name';
                break;
            case 3:
                $key = 'o.total';
                break;
            case 4:
                $key = 'o.payment_method';
                break;
            case 5:
                $key = 'o.payment_status';
                break;
        }
        return $key;
    }

    // function count_all($search = '', $merchant = '') {
    //     $this->_where_like($search);
    //     return $this->db->count_all_results('orders o');
    // }

    // function count_all($search = '', $merchant = '')
    // {
    //     $this->_where_like($search);
    //     if ($merchant != '') {
    //         // $this->db->join('order_invoice', 'order_invoice.order = o.id', 'left');
    //         // $this->db->join('merchants', 'merchants.id = order_invoice.merchant', 'left');
    //         // $this->db->where('order_invoice.merchant', $merchant);
    //     }
    //     return $this->db->count_all_results('orders o');
    // }

    function count_all($from, $to, $payment_status = '', $payment_method = '', $search = '') {
        $this->db->where('DATE(o.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'');
        $this->_where_like($search);
        if ($payment_status)
            $this->db->where('o.payment_status', $payment_status);
        if ($payment_method)
            $this->db->where('o.payment_method', $payment_method);

        return $this->db->count_all_results('orders o');
    }


    private function _where_like($search = '')
    {
        $columns = array('o.code', 'o.shipping_name', 'o.date_added', 'o.total', 'o.payment_method', 'o.payment_status');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
            $this->db->group_end();
        }
    }

    function invoices($order)
    {
        $this->db->select('oi.*, sos.name order_status_name, sos.color order_status_color, m.name merchant_name')
            ->join('setting_order_status sos', 'oi.order_status = sos.id', 'left')
            ->join('merchants m', 'oi.merchant = m.id', 'left')
            ->where('oi.order', $order);
        return $this->db->get('order_invoice oi');
    }

    function invoice_histories($invoice)
    {
        $this->db->select('oh.*, sos.name order_status_name, sos.color order_status_color')
            ->join('setting_order_status sos', 'oh.order_status = sos.id', 'left')
            ->where('oh.invoice', $invoice)
            ->order_by('oh.id', 'desc');
        return $this->db->get('order_history oh');
    }
}
