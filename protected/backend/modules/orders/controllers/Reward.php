<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Reward extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->aauth->control('orders/reward');

        $this->lang->load('orders', settings('language'));
        $this->load->model('orders_model', 'orders');
        $this->load->model('Model', 'm');
        $this->data['menu'] = 'reward';
        $this->data['m'] = $this->m;
    }

    public function index() {
        $this->template->_init();
        $this->template->table();
        $this->load->js('../assets/backend/js/modules/order_reward.js');

        $this->breadcrumbs->push(lang('order'), '/orders');
        $this->breadcrumbs->push('Reward', '/orders/reward');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->data['m'] = $this->m;
        $this->output->set_title('Reward');
        $this->load->view('list_reward', $this->data);
    }

    public function view($id) {
        $this->template->_init();

        $id = decode($id);

        $select_ = 'peh.*, c.fullname as customer_name, r.point as total, r.nama, peh.last_update as date_added, ca.name as shipping_name, ca.address as shipping_address, d.name as shipping_district_name, city.name as shipping_city_name, p.name as shipping_province_name, ca.postcode as shipping_postcode, r.id_product, r.qty_product, r.id_product_paket, r.qty_paket, r.id_coupon, r.qty_coupon, peh.id_merchant, m.name as m_name';
        $join = array(
            0 => 'customers c-c.id=peh.id_customer',
            1 => 'reward r-r.id=peh.id_reward',
            2 => 'customer_address ca-ca.id=peh.id_address',
            3 => 'districts d-d.id=ca.district',
            4 => 'cities city-city.id=ca.city',
            5 => 'provincies p-p.id=ca.province',
            6 => 'merchants m-m.id=peh.id_merchant'
        );
        $where = array(
            'peh.id' => $id
        );
        $this->data['order'] = $this->m->get_data($select_, 'point_exchange_history peh', $join, $where)->row();
        //$this->data['invoices'] = $this->orders->invoices($id);

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('order'), '/orders');
        $this->breadcrumbs->push($this->data['order']->kode, '/orders/reward/view/' . encode($id));
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('order_heading') . ' ' . $this->data['order']->kode);
        $this->load->view('view_reward', $this->data);
    }

    public function filter_periode(){
        $search = $this->input->post('search');
        $data = array(
            'periode_tukar_poin' => $search
        );
        $this->session->set_userdata($data);

        die(json_encode(array('success' => 1)));
    }

    public function filter_cm(){
        $search = $this->input->post('search');
        $data = array(
            'cm' => $search
        );
        $this->session->set_userdata($data);

        die(json_encode(array('success' => 1)));
    }

    public function confirm_delivery(){
        $id_peh = $this->input->post('id_peh');

        $this->m->update_data('point_exchange_history', array('status_pengiriman' => 1), array('id' => $id_peh));

        die(json_encode(array('message' => 'Data Berhasil Diubah', 'status' => 'success')));
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $periode_sess = $this->session->userdata('periode_tukar_poin');
        $cm_sess = $this->session->userdata('cm');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();

        $where = array();
        $ww = '';

        $column[0] = 'r.nama '.$order['dir'];
        $column[1] = 'peh.last_update '.$order['dir'];
        if($cm_sess == 'c'){
            $column[2] = 'c.fullname '.$order['dir'];
            $where['peh.id_merchant'] = 0;
            $ww = "c.fullname LIKE '%".$search."%'";
        }else if($cm_sess == 'm'){
            $column[2] = 'm.name '.$order['dir'];
            $where['peh.id_customer'] = 0;
            $ww = "m.name LIKE '%".$search."%'";
        }else{
            $column[2] = '';
            $ww = "c.fullname LIKE '%".$search."%' or m.name LIKE '%".$search."%'";
        }
        
        $column[3] = 'r.point '.$order['dir'];
        $column[4] = 'rct.periode '.$order['dir'];

        $join = array(
            0 => 'reward_claim_temp rct-rct.id=peh.id_reward_claim',
            1 => 'customers c-c.id=peh.id_customer',
            2 => 'merchants m-m.id=peh.id_merchant',
            3 => 'reward r-r.id=peh.id_reward'
        );

        if($periode_sess){
            $where['rct.periode'] = $periode_sess;
        }

        $where1 = array(
            0 => "r.nama LIKE '%".$search."%' or ".$ww." or r.point LIKE '%".$search."%'"
        );

        $datas = $this->m->get_data('peh.id_reward, r.nama, peh.last_update, c.fullname as c_name, m.name as m_name, peh.id_merchant, peh.id_customer, peh.id, r.point, rct.periode, peh.status_pengiriman, peh.id as peh_id', 'point_exchange_history peh', $join, $where, '', $column[$order['column']], $length, $start, $where1);

        if ($datas) {
            foreach ($datas->result() as $data) {
                $e = explode(';', $data->periode);
                $output['data'][] = array(
                    '<a href="' . site_url('marketing/reward_points/form/' . encode($data->id_reward)) . '">' . $data->nama . '</a>',
                    get_date_time($data->last_update),
                    ($data->id_merchant == 0 ? $data->c_name : $data->m_name),
                    number($data->point),
                    $e[2].'/'.$e[1].'/'.$e[0].' - '.$e[5].'/'.$e[4].'/'.$e[3],
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">' .
                    ($this->aauth->is_allowed('order/confirm_payment') && ($data->status_pengiriman == 0) ? '<li><a onclick="confirm_delivery('.$data->id.')" href="#" >Barang Diterima</a></li>' : '') .
                    '<li><a href="' . site_url('orders/reward/view/' . encode($data->peh_id)) . '">' . lang('button_view') . '</a></li>
                    </ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->m->get_data('id', 'point_exchange_history')->num_rows();
        $output['recordsFiltered'] = $datas->num_rows();
        echo json_encode($output);
    }

    public function paid($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->aauth->control('order/confirm_payment');

        $id = decode($id);
        $data = $this->orders->get($id);
        $update = $this->main->update('orders', array('payment_status' => 'Paid'), array('id' => $id));
        if ($update) {
            $customer = $this->main->get('customers', array('id' => $data->customer));
            $this->data['order'] = $data;
            $this->data['customer'] = $customer;
            $invoices = $this->main->gets('order_invoice', array('order' => $id));
            if ($invoices) {
                foreach ($invoices->result() as $invoice) {
                    if($invoice->shipping_courier == 'pickup') {
                        $due_date_inv = date('Y-m-d H:i:s', strtotime('+1 days'));
                    } else {
                        $due_date_inv = date('Y-m-d H:i:s', strtotime('+2 days'));
                    }
                    $this->main->update('order_invoice', array('order_status' => settings('order_payment_received_status'), 'due_date' => $due_date_inv), array('id' => $invoice->id));
                    $this->main->insert('order_history', array('order' => $id, 'invoice' => $invoice->id, 'order_status' => settings('order_payment_received_status')));

                    $this->data['invoice'] = $invoice;
                    $this->data['merchant'] = $this->main->get('merchants', array('id' => $invoice->merchant));

                    //update stock merchant
                    if ($this->data['merchant']->type == 'merchant' || $this->data['merchant']->type == 'branch') {
                        $products = $this->main->gets('order_product', array('invoice' => $invoice->id));
                        if ($products) {
                            foreach ($products->result() as $product) {
                                $product_detail = $this->db->query("select type, package_items from products where id = $product->product ");
                                $row = $product_detail->row();
                                if($row->type == 'p') {
                                    $package_items = json_decode($row->package_items, true);
                                    foreach ($package_items as $package) {
                                        $this->db->query("UPDATE product_merchant SET quantity = (quantity - $product->quantity) WHERE product = ".$package['product_id']." AND merchant = " . $this->data['merchant']->id); 
                                    }
                                } else {
                                    $this->db->query("UPDATE product_merchant SET quantity = (quantity - $product->quantity) WHERE product = $product->product AND merchant = " . $this->data['merchant']->id);
                                }
                            }
                        }
                    }

                    $merchant_user = $this->main->get('merchant_users', array('id' => $this->data['merchant']->auth));
                    $message = $this->load->view('email/merchant/new_order', $this->data, true);
                    $cronjob = array(
                        'from' => settings('send_email_from'),
                        'from_name' => settings('store_name'),
                        'to' => $merchant_user->email,
                        'subject' => 'Pesanan baru dari ' . $customer->fullname,
                        'message' => $message
                    );
                    $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
                    if ($merchant_user->verification_phone) {
                        $sms = json_decode(settings('sprint_sms'), true);
                        $sms['m'] = 'Pesanan baru dari ' . $customer->fullname . '. Segera proses sebelum ' . get_date_indo_full($invoice->due_date) . ' WIB.';
                        $sms['d'] = $merchant_user->phone;
                        $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                    }
                }
            }
            $this->data['invoices'] = $invoices;
            $message = $this->load->view('email/transaction/payment_success', $this->data, true);
            $cronjob = array(
                'from' => settings('send_email_from'),
                'from_name' => settings('store_name'),
                'to' => $customer->email,
                'subject' => 'Pembayaran Sukses #' . $data->code,
                'message' => $message
            );
            $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
            if ($customer->verification_phone) {
                $sms = json_decode(settings('sprint_sms'), true);
                $sms['m'] = 'Pembayaran sebesar ' . rupiah($data->total) . ' telah berhasil. Pesanan Anda akan diteruskan ke penjual.';
                $sms['d'] = $this->data['customer']->phone;
                $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
            }
            $return = array('message' => 'Pembayaran pesanan ' . $data->code . ' berhasil dikonfirmasi.', 'status' => 'success');
        } else {
            $return = array('message' => 'Pembayaran pesanan ' . $data->code . ' gagal dikonfirmasi.', 'status' => 'danger');
        }
        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->aauth->control('order/delete');
        $id = decode($id);
        $data = $this->main->get('orders', array('id' => $id));
        $this->main->delete('orders', array('id' => $data->id));
        $this->main->delete('order_history', array('order' => $data->id));
        $this->main->delete('order_invoice', array('order' => $data->id));
        $this->main->delete('order_meta', array('order_id' => $data->id));
        $this->main->delete('order_product', array('order' => $data->id));
        $return = array('message' => 'Pesanan ' . $data->code . ' berhasil dihapus.', 'status' => 'success');
        echo json_encode($return);
    }

}
