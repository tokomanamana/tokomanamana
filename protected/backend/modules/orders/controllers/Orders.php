<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Orders extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->aauth->control('order');

        $this->lang->load('orders', settings('language'));
        $this->load->model('orders_model', 'orders');
        $this->data['menu'] = 'order';
    }

    public function index()
    {
        $this->template->_init();
        $this->template->table();
        $this->template->form();
        $this->load->js('../assets/backend/js/modules/orders.js');

        $this->breadcrumbs->push(lang('order'), '/orders');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->data['payment_method'] = $this->main->gets('payment_methods');

        $this->output->set_title(lang('order_heading'));
        $this->load->view('list', $this->data);
    }

    public function view($id)
    {
        $this->template->_init();

        $id = decode($id);
        $this->data['order'] = $this->orders->get($id);
        $this->data['invoices'] = $this->orders->invoices($id);

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('order'), '/orders');
        $this->breadcrumbs->push($this->data['order']->code, '/orders/view/' . encode($id));
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('order_heading') . ' ' . $this->data['order']->code);
        $this->load->view('view', $this->data);
    }

    public function get_list()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));
        // $merchant = $this->input->post('merchant');
        $filter = $this->input->post('filter', true);

        $output['data'] = array();
        // if($merchant != '') {
        //     $datas = $this->orders->get_all($start, $length, $search, $order, $merchant);
        // } else {
        //     $datas = $this->orders->get_all($start, $length, $search, $order);
        // }
        $datas = $this->orders->get_all($start, $length, $search, $order, $filter['to'], $filter['from'], $filter['payment_status'], $filter['payment_method']);
        if ($datas) {
            foreach ($datas->result() as $data) {
                if($data->payment_method == 'transfer'){
                    $total_payment = $data->total+$data->unique_code;
                } else {
                    $total_payment = $data->total;
                }
                $inv = $this->main->gets('order_invoice', array('order' => $data->id));
                if($inv){
                    $oi_status = [];
                    $num = 1;
                    foreach ($inv->result() as $or) {
                        $oi_status[$num] = $or->order_status;
                        $num++;
                    }
                    $stt = $this->main->gets('setting_order_status')->result();
                    $ts = [];
                    $numb = 1;
                    foreach ($stt as $s) {
                        $ts[$numb] = $s->name;
                        $numb++;
                    }
                    for($c = 1; $c < 12; $c++){
                        if (count(array_unique($oi_status)) == 1 && end($oi_status) == $c) {
                            $status_order = $ts[$c];
                        } else {
                            $status_order = $ts[$oi_status[1]];
                        }
                    }
                } else {
                    $status_order = 'Undefined';
                }
                $output['data'][] = array(
                    '<a href="' . site_url('orders/view/' . encode($data->id)) . '">' . $data->code . '</a>',
                    get_date_time($data->date_added),
                    $data->shipping_name,
                    number($data->subtotal + $data->shipping_cost),
                    '<div style="color:red;">'.number($data->disc_value).'</div>',
                    number($total_payment),
                    $data->payment_method,
                    $data->payment_status,
                    $status_order,
                    // $data->merchant_name,
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">' .
                        ($this->aauth->is_allowed('order/confirm_payment') && ($data->payment_status == 'Confirmed' || $data->payment_status == 'Pending') && ($data->payment_method == 'transfer') ? '<li><a href="' . site_url('orders/paid/' . encode($data->id)) . '" class="paid">' . lang('order_button_paid') . '</a></li>' : '') .
                        '<li><a href="' . site_url('orders/view/' . encode($data->id)) . '">' . lang('button_view') . '</a></li>' .
                        ($this->aauth->is_allowed('order/delete') ? '<li><a href="' . site_url('orders/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>' : '') .
                        '</ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        // $output['recordsTotal'] = $this->orders->count_all();
        $output['recordsTotal'] = $this->main->count('orders');
        $output['recordsFiltered'] = $this->orders->count_all($filter['from'], $filter['to'],$filter['payment_status'], $filter['payment_method'], $search);
        echo json_encode($output);
    }

    public function get_all_merchant()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $merchants = $this->orders->get_all_merchant()->result();
        $output = '';
        $output .= '<div class="datatable_merchant" style="display: inline-block;margin: 0 0 20px 20px;">';
        $output .= '<span>Filter By Toko: &nbsp;&nbsp;</span>';
        $output .= '<select name="table_merchant" id="table_merchant" style="height: 36px;padding: 7px 12px;font-size: 13px;line-height: 1.5384616;color: #333;background-color: #fff;border: 1px solid #ddd;outline: 0;" onchange="set_datatable()">';
        $output .= '<option value="" selected>Toko</option>';
        foreach ($merchants as $merchant) {
            $output .= '<option value="' . $merchant->id . '">' . $merchant->name . '</option>';
        }
        $output .= '</select>';
        $output .= '</div>';

        echo $output;
    }

    public function paid($id)
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->aauth->control('order/confirm_payment');

        $id = decode($id);
        $data = $this->orders->get($id);
        $update = $this->main->update('orders', array('payment_status' => 'Paid'), array('id' => $id));
        if ($update) {
            $customer = $this->main->get('customers', array('id' => $data->customer));
            $this->data['order'] = $data;
            $this->data['customer'] = $customer;
            $invoices = $this->main->gets('order_invoice', array('order' => $id));
            if ($invoices) {
                foreach ($invoices->result() as $invoice) {
                    if ($invoice->shipping_courier == 'pickup') {
                        $due_date_inv = date('Y-m-d H:i:s', strtotime('+1 days'));
                    } else {
                        $due_date_inv = date('Y-m-d H:i:s', strtotime('+2 days'));
                    }
                    $this->main->update('order_invoice', array('order_status' => settings('order_payment_received_status'), 'due_date' => $due_date_inv), array('id' => $invoice->id));
                    $this->main->insert('order_history', array('order' => $id, 'invoice' => $invoice->id, 'order_status' => settings('order_payment_received_status')));

                    $updated_invoice = $this->main->get('order_invoice',array('id' => $invoice->id));
                    $this->data['invoice'] = $updated_invoice;
                    $this->data['merchant'] = $this->main->get('merchants', array('id' => $invoice->merchant));

                    //update stock merchant
                    // if ($this->data['merchant']->type == 'merchant' || $this->data['merchant']->type == 'branch') {
                    //     $products = $this->main->gets('order_product', array('invoice' => $invoice->id));
                    //     if ($products) {
                    //         foreach ($products->result() as $product) {
                    //             $product_detail = $this->db->query("select type, package_items from products where id = $product->product ");
                    //             $row = $product_detail->row();
                    //             if ($row->type == 'p') {
                    //                 $package_items = json_decode($row->package_items, true);
                    //                 foreach ($package_items as $package) {
                    //                     //////
                    //                     $this->db->query("UPDATE product_merchant SET quantity = (quantity - $product->quantity) WHERE product = " . $package['product_id'] . " AND merchant = " . $this->data['merchant']->id);

                    //                 }
                    //             } else {
                    //               $this->db->query("UPDATE product_merchant SET quantity = (quantity - $product->quantity) WHERE product = $product->product AND merchant = " . $this->data['merchant']->id);

                    //             }
                    //         }
                    //     }
                    // }

                    $merchant_user = $this->main->get('merchant_users', array('id' => $this->data['merchant']->auth));
                    $message = $this->load->view('email/merchant/new_order', $this->data, true);
                    $cronjob = array(
                        'from' => settings('send_email_from'),
                        'from_name' => settings('store_name'),
                        'to' => $merchant_user->email,
                        'subject' => 'Pesanan baru dari ' . $customer->fullname,
                        'message' => $message
                    );
                    $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
                    if ($merchant_user->verification_phone) {
                        $sms = json_decode(settings('sprint_sms'), true);
                        $sms['m'] = 'Pesanan baru dari ' . $customer->fullname . '. Segera proses sebelum ' . get_date_indo_full($this->data['invoice']->due_date) . ' WIB.';
                        $sms['d'] = $merchant_user->phone;
                        $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                    }
                }
            }
            $this->data['invoices'] = $invoices;
            if($customer->email){
                $message = $this->load->view('email/transaction/payment_success', $this->data, true);
                $cronjob = array(
                    'from' => settings('send_email_from'),
                    'from_name' => settings('store_name'),
                    'to' => $customer->email,
                    'subject' => 'Pembayaran Sukses #' . $data->code,
                    'message' => $message
                );
            $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
            }
            if ($customer->verification_phone) {
                $sms = json_decode(settings('sprint_sms'), true);
                if($data->payment_method == 'transfer'){
                    $sms['m'] = 'Pembayaran sebesar ' . rupiah($data->total_plus_kode) . ' telah berhasil. Pesanan Anda akan diteruskan ke penjual.';
                }else{
                    $sms['m'] = 'Pembayaran sebesar ' . rupiah($data->total) . ' telah berhasil. Pesanan Anda akan diteruskan ke penjual.';
                }
                
                $sms['d'] = $this->data['customer']->phone;
                $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                $this->main->insert('log_sms', array('content' => json_encode($sms), 'to' => $this->data['customer']->phone));
            }
            $return = array('message' => 'Pembayaran pesanan ' . $data->code . ' berhasil dikonfirmasi.', 'status' => 'success');
        } else {
            $return = array('message' => 'Pembayaran pesanan ' . $data->code . ' gagal dikonfirmasi.', 'status' => 'danger');
        }
        echo json_encode($return);
    }

    public function delete($id)
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->aauth->control('order/delete');
        $id = decode($id);
        $data = $this->main->get('orders', array('id' => $id));
        $this->main->delete('orders', array('id' => $data->id));
        $this->main->delete('order_history', array('order' => $data->id));
        $this->main->delete('order_invoice', array('order' => $data->id));
        $this->main->delete('order_meta', array('order_id' => $data->id));
        $this->main->delete('order_product', array('order' => $data->id));
        $return = array('message' => 'Pesanan ' . $data->code . ' berhasil dihapus.', 'status' => 'success');
        echo json_encode($return);
    }

    public function edit_invoice($id = null) {
        $this->load->library('form_validation');
        $this->aauth->control('order/edit_invoice');

        $id = decode($id);
        
        if($id == null) {
            redirect('orders','refresh');
        } else {
            $check_invoice = $this->main->get('order_invoice', ['id' => $id]);
            if($check_invoice) {

                // $this->form_validation->set_rules('order_status', 'Status Order', 'trim|required');
                $this->form_validation->set_rules('merchant', 'Toko', 'trim|required');
                $this->form_validation->set_rules('subtotal', 'Total Belanja', 'trim|number');
                $this->form_validation->set_rules('total', 'Total Pembayaran', 'trim|number');
                $this->form_validation->set_rules('ongkos_kirim', 'Ongkos Kirim', 'trim|number');

                if ($this->form_validation->run() === TRUE) {

                    $data = $this->input->post(null, true);
                    $data['subtotal'] = str_replace('.', '', $data['subtotal']);
                    $data['total'] = str_replace('.', '', $data['total']);
                    $data['ongkos_kirim'] = str_replace('.', '', $data['ongkos_kirim']);

                    $old_shipping_cost = $check_invoice->shipping_cost;
                    $difference_shipping_cost = $old_shipping_cost - $data['ongkos_kirim'];

                    $data_update = [
                        'merchant' => $data['merchant'],
                        // 'order_status' => $data['order_status'],
                        'subtotal' => $data['subtotal'],
                        'shipping_cost' => $data['ongkos_kirim'],
                        'total' => $data['total'],
                        'notes' => $data['notes']
                    ];

                    $this->main->update('order_invoice', $data_update, ['id' => $id]);

                    $data_log = [
                        'order_id' => $check_invoice->order,
                        'modified_by' => $this->data['user']->id,
                        'type' => 'Modified_order'
                    ];

                    $this->main->insert('aauth_user_log', $data_log);

                    if($difference_shipping_cost > 0) {
                        $customer_balance = $this->main->get('customer_balance', ['customer_id' => $check_invoice->customer]);
                        if($customer_balance) {
                            $update_customer_balance = [
                                'balance' => $difference_shipping_cost + $customer_balance->balance
                            ];
                            $this->main->update('customer_balance', $update_customer_balance, ['id' => $customer_balance->id]);

                            $get_customer_balance_history = $this->main->gets('customer_balance_history', ['customer_id' => $check_invoice->customer], 'id DESC');

                            $data_insert_customer_balance_history = [
                                'customer_id' => $check_invoice->customer,
                                'order_id' => $check_invoice->order,
                                'type' => 'IN',
                                'amount' => $difference_shipping_cost,
                                'balance' => $difference_shipping_cost + $get_customer_balance_history->result()[0]->balance
                            ];

                            $this->main->insert('customer_balance_history', $data_insert_customer_balance_history);
                        } else {
                            $data_insert_customer_balance = [
                                'customer_id' => $check_invoice->customer,
                                'balance' => $difference_shipping_cost
                            ];

                            $this->main->insert('customer_balance', $data_insert_customer_balance);

                            $data_insert_customer_balance_history = [
                                'customer_id' => $check_invoice->customer,
                                'order_id' => $check_invoice->order,
                                'type' => 'IN',
                                'amount' => $difference_shipping_cost,
                                'balance' => $difference_shipping_cost
                            ];

                            $this->main->insert('customer_balance_history', $data_insert_customer_balance_history);
                        }
                    } 

                    $return = [
                        'status' => 'success',
                        'message' => 'Data berhasil diubah!',
                        'redirect' => site_url('orders/view/' . encode($check_invoice->order))
                    ];

                    echo json_encode($return);
                    
                } else {

                    $this->data['data'] = [
                        'id' => $id,
                        'code' => $check_invoice->code,
                        'merchant' => $check_invoice->merchant,
                        'order_status' => $check_invoice->order_status,
                        'order_product' => $this->main->gets('order_product', ['invoice' => $id])->result(),
                        'order_id' => $check_invoice->order,
                        'subtotal' => $check_invoice->subtotal,
                        'total' => $check_invoice->total,
                        'notes' => $check_invoice->notes,
                        'shipping_cost' => $check_invoice->shipping_cost
                    ];
                    $this->data['merchants'] = $this->db->get('merchants')->result();
                    $this->data['orders_status'] = $this->db->get('setting_order_status')->result();
                    $this->data['products'] = $this->db->get('products')->result();
                    $this->template->_init();
                    $this->template->form();
                    $this->load->js('https://code.jquery.com/ui/1.10.4/jquery-ui.js');

                    $orders_data = $this->main->get('orders', ['id' => $check_invoice->order]);

                    $this->breadcrumbs->unshift(lang('catalog'), '/');
                    $this->breadcrumbs->push(lang('order'), '/orders');
                    $this->breadcrumbs->push($orders_data->code, '/orders/view/' . encode($check_invoice->order));
                    $this->breadcrumbs->push($check_invoice->code, '/orders/edit_invoice/' . $id);
                    $this->data['breadcrumbs'] = $this->breadcrumbs->show();
                    $this->load->js(site_url().'../assets/backend/js/modules/edit_invoice.js');
                    $this->output->set_title('Edit Invoice');
                    $this->load->view('edit_invoice', $this->data);

                }
            } else {
                redirect('orders', 'refresh');
            }
        }

    }

    public function send_otp() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $id = decode($data['id']);
        $code = rand(1000, 9999);
        $now = date("Y-m-d h:i:s");
        $superadmin = $this->main->get('aauth_users', array('role' => 1));
        $order = $this->main->get('orders', array('id' => $id));
        $email = $superadmin->email;
        // $email = 'ariefnhidayah@gmail.com';
        $message = 'Hai, berikut KODE OTP konfirmasi pembayaran pesanan '. $order->code .' adalah ' . $code;
        send_mail('noreply@tokomanamana.com','Tokomanamana',$email, 'Kode OTP Konfirmasi Pembayaran Pesanan', $message);
        $update = $this->main->update('orders', array('otp_code_change' => $code, 'otp_change_sent_time' => $now), array('id' => $id));
        if ($update) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function check_account() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $user = $this->main->get('aauth_users', ['id' => $this->data['user']->id]);
        $check = $this->aauth->verify_password($data['password'], $user->pass);
        if ($check) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function check_otp_code() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $id = decode($data['id']);
        $order = $this->main->get('orders', ['id' => $id]);
        if($order->otp_code_change == $data['otp_code']){
            echo "true";
        } else {
            echo "false";
        }
    }
}
