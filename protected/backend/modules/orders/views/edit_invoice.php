<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2>Edit Invoice</h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('orders'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span>Daftar Pesanan</span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="" method="post" class="form-horizontal" id="form">
                    <input type="hidden" name="id" value="<?php echo $data['id'] ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="tabbable">
                                <div class="tab-content">
                                    <div class="form-group">
                                        <label class="col-md-12"><b>Informasi Invoice</b></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="code">Kode Invoice</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="code" value="<?php echo $data['code'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="merchant">Toko</label>
                                        <div class="col-md-9">
                                            
                                            <select class="bootstrap-select"  name="merchant" id="merchant" data-live-search="true" data-width="100%">
                                                <?php foreach($merchants as $merchant) : ?>
                                                    <option value="<?php echo $merchant->id ?>" <?php echo ($data['merchant'] == $merchant->id) ? 'selected' : '' ?>><?php echo $merchant->name ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>       
                                    <!-- <div class="form-group">
                                        <label class="col-md-3 control-label" for="order_status">Status</label>
                                        <div class="col-md-9">
                                            <select class="bootstrap-select"  name="order_status" id="order_status" data-live-search="true" data-width="100%">
                                                <?php foreach($orders_status as $order_status) : ?>
                                                    <option value="<?php echo $order_status->id ?>" <?php echo ($data['order_status'] == $order_status->id) ? 'selected' : '' ?>><?php echo $order_status->name ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>   -->
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="notes">Catatan</label>
                                        <div class="col-md-9">
                                            <textarea name="notes" id="notes" cols="30" rows="2" class="form-control" placeholder="Ketik catatan"><?php echo $data['notes'] ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="subtotal" class="col-md-3 control-label">Total Belanja</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control number" name="" value="<?= $data['subtotal'] ?>" disabled>
                                            <input type="hidden" name="subtotal" value="<?= $data['subtotal'] ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="total" class="col-md-3 control-label">Total Pembayaran</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control number" name="total_" value="<?= $data['total'] ?>" disabled>
                                            <input type="hidden" name="total" value="<?= $data['total'] ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="ongkos_kirim" class="col-md-3 control-label">Ongkos Kirim</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control number" name="ongkos_kirim" value="<?= $data['shipping_cost'] ?>" required>
                                        </div>
                                    </div>
                                    <!-- <div class="form-group">
                                        <label class="col-md-3 control-label" for="order_product">Produk Pesanan</label>
                                        <div class="col-md-9">
                                            <select class="bootstrap-select"  name="order_product" id="order_product" data-live-search="true" data-width="100%" onchange="change_product($(this).val())">
                                                <?php foreach($data['order_product'] as $product) : ?>
                                                    <option value="<?php echo $product->id ?>"><?php echo $product->name ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>              -->
                                </div>
                            </div>
                            <!-- <hr> -->
                            <!-- <div class="tabbable">
                                <div class="tab-content">
                                    <div class="form-group">
                                        <label class="col-md-12"><b>Detail Produk Pesanan</b></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="name_product">Nama Produk</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="name_product" value="<?php echo $data['order_product'][0]->name ?>" disabled id="name_product">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="code_product">Kode Produk</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="code_product" value="<?php echo $data['order_product'][0]->code ?>" disabled id="code_product">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="price">Harga Produk</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control input-sm number" name="price" value="<?php echo $data['order_product'][0]->price ?>" disabled id="price">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="quantity">Jumlah</label>
                                        <div class="col-md-9">
                                            <input type="number" class="form-control" name="quantity" value="<?php echo $data['order_product'][0]->quantity ?>" id="quantity" min="1">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="total_price">Total Harga</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control input-sm number" name="total_price" value="<?php echo $data['order_product'][0]->total ?>" disabled id="total_price">
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('orders/view/' . encode($data['order_id'])); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <button type="button" id="submitInvoice" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>