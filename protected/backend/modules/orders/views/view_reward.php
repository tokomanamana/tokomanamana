<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('order_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <!--<a href="<?php echo site_url('orders/form'); ?>" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span><?php echo lang('order_add_heading'); ?></span></a>-->
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th colspan="2">Order Detail</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>ID Tukar Reward</td>
                                    <td><?php echo $order->kode; ?></td>
                                </tr>
                                <tr>
                                    <td><?=($order->id_merchant == 0 ? 'Customer' : 'Merchant')?></td>
                                    <td><?=($order->id_merchant == 0 ? $order->customer_name : $order->m_name)?></td>
                                </tr>
                                <tr>
                                    <td>Reward</td>
                                    <td><?php echo $order->nama; ?></td>
                                </tr>
                                <tr>
                                    <td>Total Poin</td>
                                    <td style="font-size: 15px; font-weight: bold;"><?php echo number_format($order->total) ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo lang('order_date'); ?></td>
                                    <td><?php echo get_date_time($order->date_added); ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                    <?php
                        if($order->id_address != '' && $order->id_address != 0){
                    ?>
                    <div class="col-sm-8">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th><?php echo lang('order_shipping_address'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <?php echo $order->shipping_name . '<br>' . $order->shipping_address . '<br>' . $order->shipping_district_name . ' - ' . $order->shipping_city_name . '<br>' . $order->shipping_province_name . '<br>' . $order->shipping_postcode; ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php
                        }
                    ?>
                </div>
                <h6 class="text-semibold">Detail Reward</h6>
                <div class="row mt-15">
                    <div class="col-sm-12">
                        <div class="tabbable">
                            <div class="tab-content">
                                <div class="" id="">
                                    <table class="table table-bordered mt-15">
                                        <thead>
                                            <tr>
                                                <th>Nama</th>
                                                <th>Product/Coupon</th>
                                                <th>QTY</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if($order->id_product != 0){
                                                $p = $m->get_data('', 'products', null, array('id' => $order->id_product))->row();
                                                ?>
                                            <tr>
                                                <td><?=$p->name?></td>
                                                <td>Product</td>
                                                <td><?=$order->qty_product?></td>
                                            </tr>
                                                <?php
                                            }

                                            if($order->id_product_paket != 0){
                                                $p = $m->get_data('', 'products', null, array('id' => $order->id_product_paket))->row();
                                                ?>
                                            <tr>
                                                <td><?=$p->name?></td>
                                                <td>Product Paket</td>
                                                <td><?=$order->qty_paket?></td>
                                            </tr>
                                                <?php
                                            }

                                            if($order->id_merchant == 0){
                                                if($order->id_coupon != 0){
                                                    $p = $m->get_data('', 'coupons', null, array('id' => $order->id_coupon))->row();
                                                    ?>
                                                <tr>
                                                    <td><?=$p->name?></td>
                                                    <td>Coupon</td>
                                                    <td><?=$order->qty_coupon?></td>
                                                </tr>
                                                    <?php
                                                }
                                            }
                                            
                                            ?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>