<?php
    $periode_sess = $this->session->userdata('periode_tukar_poin');
    $cm_sess = $this->session->userdata('cm');
    if(!$periode_sess){
        $periode_sess = '';
    }
?>
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2>Reward</h2>
            </div>

<!--            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('orders/form'); ?>" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span><?php echo lang('order_add_heading'); ?></span></a>
                </div>
            </div>-->
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-xs-12 control-label">Periode Tukar Poin</label>
                    <input type="hidden" id="periode_inp" value=""/>
                    <div class="col-xs-12 col-md-4" style="">
                        <select class="form-control" onchange="change_0(this.value)">
                            <option value="">Pilih Periode</option>
                            <?php
                                $q_periode = $m->get_data('periode', 'reward_claim_temp', null, null, 'periode', 'periode desc');

                                foreach ($q_periode->result() as $key) {
                                    $e = explode(';', $key->periode);
                                    $periode0 = $e[2].'/'.$e[1].'/'.$e[0];
                                    $periode1 = $e[5].'/'.$e[4].'/'.$e[3];

                                    $selected = $periode_sess == $key->periode ? 'selected' : '';
                                    ?>
                            <option value="<?=$key->periode?>" <?=$selected?>><?=$periode0?> - <?=$periode1?></option>
                                    <?php
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-12 control-label" style="margin-top: 12px">Customer/Merchant</label>
                    <div class="col-xs-12 col-md-4" style="">
                        <select class="form-control" onchange="change_1(this.value)">
                            <option value="">----</option>
                            <option value="c" <?=$cm_sess == 'c' ? 'selected' : '';?>>Customer</option>
                            <option value="m" <?=$cm_sess == 'm' ? 'selected' : '';?>>Merchant</option>
                        </select>
                    </div>
                </div>
            </div>
            <table class="table table-hover" id="table" data-url="<?php echo site_url('orders/reward/get_list'); ?>" data-state-save="false">
                <thead>
                    <tr>
                        <th>REWARD</th>
                        <th class="default-sort" data-sort="desc">Tanggal</th>
                        <th id="th_cm"><?=($cm_sess == '' ? 'Customer/Merchant' : ($cm_sess == 'c' ? 'Customer' : 'Merchant'))?></th>
                        <th>Poin</th>
                        <th>Periode</th>
                        <th class="no-sort text-center" style="width: 20px;"><?php echo lang('actions'); ?></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
    var base_url = '<?=base_url()?>';
    function change_0(val) {
        $.ajax({
            type: 'POST',
            url: base_url + "orders/reward/filter_periode",
            data: {
                search: val
            },
            dataType: "json",
            success: function (data) {
                if (data.success == 1) {
                    var table = $('#table').DataTable();
                    table.ajax.reload();
                }else if(data.success == '0'){
                    var table = $('#table').DataTable();
                    table.ajax.reload();
                    swal({
                        title: "Error !",
                        text: '',
                        type: 'error'
                    }).then((result) => {
                        
                    });
                }
            }
        });
    }

    function change_1(val) {
        if(val == ''){
            $('#th_cm').html('Customer/Merchant');
        }else if(val == 'm'){
            $('#th_cm').html('Merchant');
        }else{
            $('#th_cm').html('Customer');
        }
        $.ajax({
            type: 'POST',
            url: base_url + "orders/reward/filter_cm",
            data: {
                search: val
            },
            dataType: "json",
            success: function (data) {
                if (data.success == 1) {
                    var table = $('#table').DataTable();
                    table.ajax.reload();
                }else if(data.success == '0'){
                    var table = $('#table').DataTable();
                    table.ajax.reload();
                    swal({
                        title: "Error !",
                        text: '',
                        type: 'error'
                    }).then((result) => {
                        
                    });
                }
            }
        });
    }

    function confirm_delivery(id_peh) {
        swal({
            title: 'Yakin pengiriman sudah dilakukan ?',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal',
            closeOnConfirm: false
        }, function () {
            $.ajax({
                url: base_url + "orders/reward/confirm_delivery",
                dataType: "json",
                data: {
                    id_peh: id_peh
                },
                type: 'POST',
                success: function (data) {
                    var table = $('#table').DataTable();
                    table.ajax.reload();
                    swal(data.message, '', data.status);
                }
            });
        });
    }
</script>