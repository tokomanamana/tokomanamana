<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2>Pesanan</h2>
            </div>

            <!--            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('orders/form'); ?>" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span><?php echo lang('order_add_heading'); ?></span></a>
                </div>
            </div>-->
        </div>
    </div>
    <div class="content">
    <div class="panel panel-flat">
            <div class="panel-body">
                <div class="row">
                    <form id="filter-form">
                        <div class="col-sm-5">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">Filter By:</label>
                                        <select id="filter-by" class="form-control">
                                            <option value="today">Hari Ini</option>
                                            <option value="yesterday">Kemarin</option>
                                            <option value="this month">Bulan Ini</option>
                                            <option value="last month">Bulan Lalu</option>
                                            <option value="this year">Tahun Ini</option>
                                            <option value="99">Kostum</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">Dari:</label>
                                        <input type="text" class="form-control" readonly="" name="from" value="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 no-padding-right">
                                    <div class="form-group">
                                        <label class="control-label">Sampai:</label>
                                        <input type="text" class="form-control" readonly="" name="to" value="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="col-xs-6 col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Status Pembayaran:</label>
                                    <select name="payment_status" class="form-control">
                                        <option value="">Semua</option>
                                        <option value="Pending">Pending</option>
                                        <option value="Paid">Paid</option>
                                        <option value="Confirmed">Confirmed</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-6 col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Jenis Pembayaran:</label>
                                    <select name="payment_method" class="form-control">
                                        <option value="">Semua</option>
                                        <?php
                                            foreach ($payment_method->result() as $pay) {
                                        ?>
                                        <option value="<?= $pay->name ?>"><?= $pay->title ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3">
                                <button  style="margin-top: 25px" class="btn btn-primary" type="button" id="filter">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="panel panel-flat">
            <table class="table table-hover" id="table_order" data-url="<?php echo site_url('orders/get_list'); ?>" data-state-save="false">
                <thead>
                    <tr>
                        <th><?php echo lang('order_code_th'); ?></th>
                        <th class="default-sort" data-sort="desc"><?php echo lang('order_date_th'); ?></th>
                        <th><?php echo lang('order_customer_th'); ?></th>
                        <th><?php echo lang('order_subtotal_th') ?></th>
                        <th><?php echo lang('order_coupon_th') ?></th>
                        <th><?php echo lang('order_total_th'); ?></th>
                        <th><?php echo lang('order_payment_th'); ?></th>
                        <th><?php echo lang('order_status_pay_th') ?></th>
                        <th><?php echo lang('order_status_th'); ?></th>
                        <!-- <th><?php echo lang('order_merchant_th'); ?></th> -->
                        <th class="no-sort text-center" style="width: 20px;"><?php echo lang('actions'); ?></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
    var base_url = '<?= base_url() ?>';

    function change_0(val) {
        $.ajax({
            type: 'POST',
            url: base_url + "orders/reward/filter_periode",
            data: {
                search: val
            },
            dataType: "json",
            success: function(data) {
                if (data.success == 1) {
                    var table = $('#table').DataTable();
                    table.ajax.reload();
                } else if (data.success == '0') {
                    var table = $('#table').DataTable();
                    table.ajax.reload();
                    swal({
                        title: "Error !",
                        text: '',
                        type: 'error'
                    }).then((result) => {

                    });
                }
            }
        });
    }
</script>