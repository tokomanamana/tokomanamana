<?php

(defined('BASEPATH')) or exit('No direct script access allowed');

class Template {

    public function __construct() {
        $this->ci = & get_instance();
    }

    public function _init($template = 'default') {
        $this->ci->output->set_template($template);

        $this->ci->load->css('https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900');
        $this->ci->load->css('../assets/backend/css/icons/icomoon/styles.css');
        $this->ci->load->css('../assets/backend/css/bootstrap.css');
        $this->ci->load->css('../assets/backend/css/core.css');
        $this->ci->load->css('../assets/backend/css/components.css');
        $this->ci->load->css('../assets/backend/css/colors.css');
        $this->ci->load->css('../assets/backend/css/custom.css');

        $this->ci->load->js('../assets/backend/js/plugins/loaders/pace.min.js');
        $this->ci->load->js('../assets/backend/js/core/libraries/jquery.min.js');
        $this->ci->load->js('../assets/backend/js/core/libraries/bootstrap.min.js');
        $this->ci->load->js('../assets/backend/js/plugins/loaders/blockui.min.js');
        $this->ci->load->js('../assets/backend/js/plugins/sweet_alert.min.js');
        $this->ci->load->js('../assets/backend/js/language/' . settings('language') . '.js');
        $this->ci->load->js('../assets/backend/js/core/app.js');
    }

    // public function shop() {
    //     $this->ci->output->set_template('shop');
    //     $this->ci->load->css('https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700');
    //     $this->ci->load->css('https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i');
    //     $this->ci->load->css('../assets/frontend/css/bootstrap.min.css');
    //     $this->ci->load->css('../assets/frontend/css/fonts.googleapis.css');
    //     $this->ci->load->css('../assets/frontend/css/font-awesome.min.css');
    //     $this->ci->load->css('../assets/frontend/css/icon-font.min.css');
    //     // $this->ci->load->css('../assets/frontend/css/social-buttons.css');
    //     $this->ci->load->css('../assets/frontend/css/cs-3.styles.css');
    //     $this->ci->load->css('../assets/frontend/css/owl.carousel.min.css');
    //     $this->ci->load->css('../assets/frontend/css/spr.css');
    //     $this->ci->load->css('../assets/frontend/css/slideshow-fade.css');
    //     $this->ci->load->css('../assets/frontend/css/cs.animate.css');
    //     $this->ci->load->css('../assets/frontend/css/custom.css');

    //     $this->ci->load->js('../assets/frontend/js/jquery.min.js');
    //     // $this->ci->load->js('../assets/frontend/js/classie.js');
    //     // $this->ci->load->js('../assets/frontend/js/cs.optionSelect.js');
    //     $this->ci->load->js('../assets/frontend/js/cs.script.js');
    //    // $this->ci->load->js('../assets/frontend/js/jquery.currencies.min.js');
    //     // $this->ci->load->js('../assets/frontend/js/jquery.zoom.min.js');
    //     // $this->ci->load->js('../assets/frontend/js/linkOptionSelectors.js');
    //     $this->ci->load->js('../assets/frontend/js/owl.carousel.min.js');
    //     // $this->ci->load->js('../assets/frontend/js/scripts.js');
    //     // $this->ci->load->js('../assets/frontend/js/social-buttons.js');
    //     $this->ci->load->js('../assets/frontend/js/bootstrap.min.js');
    //     $this->ci->load->js('../assets/frontend/js/jquery.touchSwipe.min.js');
    //     $this->ci->load->js('../assets/frontend/js/blockui.min.js');
    //     $this->ci->load->js('../assets/frontend/js/jquery.form.min.js');
    // }

    public function table() {
        $this->ci->load->js('../assets/backend/js/plugins/tables/datatables/datatables.min.js');
        $this->ci->load->js('../assets/backend/js/plugins/tables/table.js');
    }

    public function form() {
        $this->ci->load->css('../assets/backend/css/plugins/datepicker.min.css');
        
        $this->ci->load->js('../assets/backend/js/plugins/forms/styling/uniform.min.js');
        $this->ci->load->js('../assets/backend/js/plugins/forms/jquery.number.min.js');
        $this->ci->load->js('../assets/backend/js/plugins/forms/tinymce/tinymce.min.js');
        $this->ci->load->js('../assets/backend/js/plugins/forms/bootstrap_select.min.js');
        $this->ci->load->js('../assets/backend/js/plugins/forms/stepy.min.js');
        $this->ci->load->js('../assets/backend/js/plugins/forms/select2.min.js');
        $this->ci->load->js('../assets/backend/js/plugins/forms/tagsinput.min.js');
//        $this->ci->load->js('../assets/backend/js/core/libraries/jasny_bootstrap.min.js');
        $this->ci->load->js('../assets/backend/js/plugins/forms/styling/switch.min.js');
        $this->ci->load->js('../assets/backend/js/plugins/forms/validation/validate.min.js');
        // $this->ci->load->js('../assets/backend/js/plugins/forms/validation/validatefix.min.js');
        $this->ci->load->js('../assets/backend/js/plugins/forms/validation/localization/messages_' . settings('language') . '.js');
        $this->ci->load->js('../assets/backend/js/plugins/forms/jquery.form.min.js');
        $this->ci->load->js('../assets/backend/js/plugins/forms/datepicker.min.js');
        $this->ci->load->js('../assets/backend/js/merchant/form.js');
    }

}
