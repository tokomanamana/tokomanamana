<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['merchant_heading'] = 'UKM';
$lang['merchant_sub_heading'] = 'Kelola Data UKM';
$lang['merchant_list_heading'] = 'Daftar UKM';
$lang['merchant_add_heading'] = 'Tambah UKM';
$lang['merchant_edit_heading'] = 'Edit UKM';

$lang['merchant_image_th'] = 'Gambar';
$lang['merchant_name_th'] = 'Nama UKM';

$lang['merchant_form_general_tabs'] = 'Umum';
$lang['merchant_form_data_tabs'] = 'Data';
$lang['merchant_form_image_tabs'] = 'Gambar';

$lang['merchant_form_name_label'] = 'Nama';
$lang['merchant_form_short_description_label'] = 'Deskripsi Singkat';
$lang['merchant_form_description_label'] = 'Deskripsi';
$lang['merchant_form_username_label'] = 'Domain Toko';
$lang['merchant_form_auth_label'] = 'Admin';
$lang['merchant_form_address_label'] = 'Alamat';
$lang['merchant_form_telephone_label'] = 'Telepon';
$lang['merchant_form_status_label'] = 'Status';
$lang['merchant_form_image_profile_label'] = 'Gambar Profil';
$lang['merchant_form_image_cover_label'] = 'Gambar Cover';

$lang['merchant_form_name_placeholder'] = 'Masukkan nama UKM';
$lang['merchant_form_description_placeholder'] = 'Masukkan deskripsi dari UKM ini. Deskripsi akan ditampilkan di halaman profil UKM ini';
$lang['merchant_form_short_description_placeholder'] = 'Masukkan deskripsi singkat UKM';

$lang['merchant_save_success_message'] = "UKM '%s' berhasil disimpan.";
$lang['merchant_save_error_message'] = "UKM '%s' gagal disimpan.";
$lang['merchant_delete_success_message'] = "UKM '%s' berhasil dihapus.";
$lang['merchant_delete_error_message'] = "UKM '%s' gagal dihapus.";
$lang['merchant_username_exist_message'] = "Domain UKM '%s' sudah terdaftar.";