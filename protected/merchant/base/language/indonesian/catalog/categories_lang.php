<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['category_heading'] = 'Kategori';
$lang['category_sub_heading'] = 'Kelola Data Kategori';
$lang['category_list_heading'] = 'Daftar Kategori';
$lang['category_add_heading'] = 'Tambah Kategori';
$lang['category_edit_heading'] = 'Edit Kategori';

$lang['category_image_th'] = 'Gambar';
$lang['category_name_th'] = 'Nama Kategori';

$lang['category_form_general_tabs'] = 'Umum';
$lang['category_form_seo_tabs'] = 'SEO';

$lang['category_form_name_label'] = 'Nama';
$lang['category_form_image_label'] = 'Gambar';
$lang['category_form_description_label'] = 'Deskripsi';
$lang['category_form_parent_label'] = 'Kategori Utama';
$lang['category_form_meta_title_label'] = 'Meta Judul';
$lang['category_form_meta_description_label'] = 'Meta Deskripsi';
$lang['category_form_meta_keyword_label'] = 'Meta Keyword';

$lang['category_form_name_placeholder'] = 'Masukkan nama kategori';
$lang['category_form_description_placeholder'] = 'Masukkan deskripsi dari kategori ini. Deskripsi akan ditampilkan di halaman kategori';

$lang['category_form_add_image_button'] = 'Tambah Gambar';
$lang['category_form_edit_image_button'] = 'Ubah Gambar';
$lang['category_form_delete_image_button'] = 'Hapus Gambar';

$lang['category_save_success_message'] = "Kategori '%s' berhasil disimpan.";
$lang['category_save_error_message'] = "Kategori '%s' gagal disimpan.";
$lang['category_delete_success_message'] = "Kategori '%s' berhasil dihapus.";
$lang['category_delete_error_message'] = "Kategori '%s' gagal dihapus.";
$lang['category_delete_exist_child_message'] = "Kategori '%s' tidak dapat dihapus. Silahkan hapus dahulu kategori yang berada di kategori ini.";
$lang['category_delete_exist_product_message'] = "Kategori '%s' tidak dapat dihapus. Silahkan hapus dahulu produk yang berada di kategori ini.";
$lang['category_list_nothing_message'] = "Kategori belum ada.";