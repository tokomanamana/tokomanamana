<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['error_csrf'] = 'Form yang dikirim tidak lulus pemeriksaan keamanan kami.';

$lang['login_heading'] = 'Login Akun Merchant';
$lang['register_heading'] = 'Langkah Mudah untuk Berjualan di TokoManaMana.com';

$lang['account'] = 'Informasi Akun';
$lang['store'] = 'Informasi Toko';
$lang['bank'] = 'Informasi Bank';
$lang['email'] = 'Email';
$lang['username'] = 'Username';
$lang['password'] = 'Password';
$lang['confirm_password'] = 'Konfirmasi Password';
$lang['owner_name'] = 'Nama Pemilik';
$lang['owner_phone'] = 'No. Handphone';
$lang['verified_phone'] = 'Verifikasi No Hp';
$lang['owner_birthday'] = 'Tanggal Lahir';
$lang['owner_id'] = 'No. KTP';
$lang['store_name'] = 'Nama Toko';
$lang['store_telephone'] = 'No. Telepon';
$lang['store_description'] = 'Deskripsi Toko';
$lang['store_address'] = 'Alamat Toko';
$lang['store_province'] = 'Provinsi';
$lang['store_city'] = 'Kota';
$lang['store_district'] = 'Kecamatan';
$lang['store_postcode'] = 'Kode Pos';
$lang['bank_name'] = 'Nama Bank';
$lang['bank_branch'] = 'Cabang';
$lang['bank_account_number'] = 'No. Rekening';
$lang['bank_account_name'] = 'Nama Rekening';
$lang['terms'] = 'Syarat dan Ketentuan';
$lang['location'] = 'Lokasi';
$lang['terms_prefix'] = 'Dengan ini saya menyetujui';
$lang['remember'] = 'Ingat saya';
$lang['login'] = 'Login';

$lang['username_placeholder'] = 'Masukkan username unik';
$lang['email_placeholder'] = 'Masukkan alamat email';
$lang['password_placeholder'] = 'Masukkan password';
$lang['confirm_password_placeholder'] = 'Masukkan ulang password';
$lang['owner_name_placeholder'] = 'Masukkan nama berdasarkan KTP';
$lang['owner_phone_placeholder'] = 'Masukkan nomor handphone';
$lang['owner_birthday_day_placeholder'] = 'Tanggal';
$lang['owner_birthday_month_placeholder'] = 'Bulan';
$lang['owner_birthday_year_placeholder'] = 'Tahun';
$lang['owner_id_placeholder'] = 'Masukkan nomor KTP / paspor';
$lang['store_name_placeholder'] = 'Masukkan nama toko';
$lang['store_telephone_placeholder'] = 'Masukkan nomor telepon';
$lang['store_description_placeholder'] = 'Masukkan deskripsi toko anda';
$lang['store_address_placeholder'] = 'Masukkan alamat toko';
$lang['store_province_placeholder'] = 'Pilih provinsi';
$lang['store_city_placeholder'] = 'Pilih kota';
$lang['store_district_placeholder'] = 'Pilih kecamatan';
$lang['store_postcode_placeholder'] = 'Masukkan kode pos';
$lang['bank_name_placeholder'] = 'Pilih nama bank';
$lang['bank_branch_placeholder'] = 'Masukkan cabang bank';
$lang['bank_account_number_placeholder'] = 'Masukkan nomor rekening';
$lang['bank_account_name_placeholder'] = 'Masukkan nama pemilik rekening';

$lang['email_registered'] = 'Email %s sudah terdaftar';
$lang['username_registered'] = 'Username %s sudah terdaftar';
$lang['register_successful'] = 'Pendaftaran berhasil, akun Anda akan diverifikasi';
$lang['captcha_failed'] = 'Captcha gagal diverifikasi';
$lang['terms_required'] = 'Syarat dan ketentuan harus disetujui';
$lang['login_unsuccessful'] = 'Login gagal, email atau password tidak sesuai';