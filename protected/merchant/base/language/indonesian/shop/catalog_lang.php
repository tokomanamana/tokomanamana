<?php

//categories

//products
$lang['product_field_warranty'] = 'Garansi';
$lang['product_field_sku'] = 'SKU';
$lang['product_field_weight'] = 'Berat';
$lang['product_field_brand'] = 'Merek';
$lang['product_field_stock'] = 'Ketersediaan';

$lang['product_field_review_title'] = 'Judul';
$lang['product_field_review_description'] = 'Isi Ulasan';
$lang['product_review_submit'] = 'Kirim Ulasan';

$lang['product_tabs_overview'] = 'Overview';
$lang['product_tabs_spesification'] = 'Spesifikasi';
$lang['product_tabs_review'] = 'Ulasan';

$lang['product_text_no_warranty'] = 'Tidak ada garansi';
$lang['product_text_online'] = 'Online';
$lang['product_text_store'] = 'Toko';
$lang['product_text_review_star'] = 'Bintang';
$lang['product_text_review'] = 'Ulasan';
$lang['product_text_review_like_this'] = 'Anda menyukai ulasan ini';
$lang['product_text_review_dislike_this'] = 'Anda tidak menyukai ulasan ini';
