<?php

function get_user() {
    $CI = & get_instance();
    $CI->data['categories'] = $CI->main->gets('categories', array('parent' => 0, 'active' => 1));
    if ($CI->ion_auth->logged_in() || $CI->aauth->is_loggedin()) {
        if (uri_string() == 'auth/login') {
            redirect('');
        }
        if (uri_string() != 'auth/logout' || uri_string() != 'logout') {
            if (!$CI->session->userdata('merchant_user')) {
                if($CI->ion_auth->logged_in()) {
                    $user = $CI->ion_auth->user()->row();
                    $merchant = $CI->main->get('merchants', array('auth' => $user->id));
                    $user->merchant_owner = $user->id;
                }
                if($CI->aauth->is_loggedin()) {
                    $user = new \stdClass();
                    $merchant = $CI->main->get('merchants', array('id' => $CI->session->userdata('merchant_id')));
                    $merchant_owner = $CI->main->get('merchant_users', array('id' => $merchant->auth));
                    $user->verification_phone = 1;
                    $user->token_confirm_status = 1;
                    $user->id = NULL;
                    $user->fullname = $CI->session->userdata('username');
                    $user->email = $CI->session->userdata('email');
                    $user->phone = '';
                    $user->merchant_owner = $merchant->auth;
                    $user->signature_img = $merchant_owner->signature_img;
                }
                $user->merchant = $merchant->id;
                $user->merchant_username = $merchant->username;
                $user->profile_picture = $merchant->merchant_profile_image;
                $user->merchant_name = $merchant->name;
                $user->merchant_group = $merchant->group;
                $user->merchant_status = $merchant->status;
                $user->merchant_type = $merchant->type;
                $user->merchant_parent = $merchant->parent;
                $user->principal_id = $merchant->principal_id;
                $CI->session->set_userdata('merchant_user', $user);
            }
            $CI->data['user'] = $CI->session->userdata('merchant_user');
            $CI->data['orders_pending'] = $CI->main->gets('orders_merchant',array('customer'=>$CI->data['user']->merchant,'payment_status'=>'Pending'));
            $CI->data['total_active_order'] = $CI->db->where_not_in('order_status',array(settings('order_finish_status'), settings('order_cancel_status')))->where('customer',$CI->data['user']->merchant)->count_all_results('order_invoice_merchant');
            //$CI->data['complain_chat'] = $CI->db->where('type','merchant')->where('receiver_id',$CI->data['user']->id)->count_all_results('chat_complain');
            //$CI->data['total_notif'] =  $CI->data['total_active_order'] + $CI->data['complain_chat'];
            $CI->data['total_notif'] =  $CI->data['total_active_order'];
            $CI->data['total_stock'] =  $CI->db->where('merchant',$CI->data['user']->merchant)->select('count(quantity) as qty')->get('product_merchant')->row()->qty;
            $CI->data['notifications'] = array();
            $CI->data['merchant_notifications'] = array();
            $orders = $CI->db->select('oi.id, oi.code, oi.order_status, c.fullname customer_name')
                    ->where('oi.order_status', 3)
                    ->where('oi.merchant',$CI->data['user']->merchant)
                    ->join('customers c', 'c.id = oi.customer')
                    ->order_by('oi.date_modified desc')
                    ->get('order_invoice oi');
            if ($orders->num_rows() > 0) {
                foreach ($orders->result() as $order) {
                    $notif['url'] = site_url('orders/view/' . encode($order->id));
                    if ($order->order_status == 3) {
                        $notif['msg'] = 'Pesanan baru "' . $order->code . '" dari ' . $order->customer_name . '.';
                    } else {
                        $notif['msg'] = 'Konfirmasi pembayaran pesanan "' . $order->code . '" telah dilakukan.';
                    }
                    array_push($CI->data['notifications'], $notif);
                }
            }

            $merchant_orders = $CI->db->select('oi.id, oi.code, oi.order_status, m.name customer_name')
            ->where('oi.order_status', 3)
            ->where('oi.merchant',$CI->data['user']->merchant)
            ->join('merchants m', 'm.id = oi.customer')
            ->order_by('oi.date_modified desc')
            ->get('order_invoice_merchant oi');
            if ($merchant_orders->num_rows() > 0) {
                foreach ($merchant_orders->result() as $order) {
                    $notif['url'] = site_url('orders/orders_merchant/view/' . encode($order->id));
                    if ($order->order_status == 3) {
                        $notif['msg'] = 'Pesanan baru "' . $order->code . '" dari ' . $order->customer_name . '.';
                    } else {
                        $notif['msg'] = 'Konfirmasi pembayaran pesanan "' . $order->code . '" telah dilakukan.';
                    }
                    array_push($CI->data['merchant_notifications'], $notif);
                }
            }
        }
    } else {
        if ($CI->router->fetch_class() != 'auth') {
            if ($CI->uri->segment(1) == 'api') {
                
            } else {
                redirect('auth/login?back=' . uri_string());
            }
        }
        /*echo $CI->uri->segment(1);
        $not_redirect = ['auth/login', 'auth/logout', 'auth/register', 'login', 'register'];
        if (!in_array(uri_string(), $not_redirect)) {
            redirect('auth/login?back=' . uri_string());
        }
        echo $CI->router->fetch_class();*/
    }
}
