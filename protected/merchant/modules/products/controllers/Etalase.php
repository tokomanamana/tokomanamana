<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Etalase extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        // $this->aauth->control('etalase');  

        $this->lang->load('etalase', settings('language'));
        $this->load->model('etalase_model', 'etalase_model');

        $this->data['menu'] = 'etalase';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();

        // $this->breadcrumbs->unshift(lang('catalog'), '/');
        // $this->breadcrumbs->push(lang('brand'), '/catalog/brands');

        // $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('etalase_heading'));
        $this->load->view('etalase/etalase', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->etalase_model->get_all($this->data['user']->merchant,$start, $length, $search, $order);
        
        if ($datas) {
            foreach ($datas->result() as $data) {
                $product_name = array();
                $products = $this->etalase_model->get_related_products($data->id);
                $count=0;
                foreach ($products->result() as $product) {
                    if($count==0){
                        $count++;
                        $product_name[] = '<div style="max-width:250px;overflow:hidden;text-align:center;color:white;background-color:#97c23c;padding:5px;border-radius:5px;margin:3px;">'.$product->product_name.'</div>';
                    }
                    else{
                        $product_name[] = '<div style="max-width:250px;overflow:hidden;text-align:center;color:white;background-color:#97c23c;padding:5px;border-radius:5px;margin:3px;">'.$product->product_name.'</div>';    
                    }
                    
                }
                // var_dump(print_r($product_name));
                $row_data = array(
                    $data->etalase_name
                );

                $row_data[] =  '<div style="max-height:300px;overflow-y:auto;">'.implode(" ",$product_name) .'</div>';
                $row_data[] = '<a href="' . site_url('products/etalase/form/' . encode($data->id)) . '"><button type="button" class="btn btn-info">Edit</button></a>
                    <a href="' . site_url('products/etalase/delete/' . encode($data->id)) . '"class="delete"><button type="button" class="btn btn-danger">Remove</button></a>';

                
                $output['data'][] = $row_data;
   
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->etalase_model->count_all($this->data['user']->merchant);
        $output['recordsFiltered'] = $this->etalase_model->count_all($this->data['user']->merchant,$search);
        echo json_encode($output);
    }

    public function form($id = '') {
        $this->template->_init();
        $this->template->form();

        $this->load->js('../assets/backend/js/modules/merchants/etalase_form.js');

        $this->data['data'] = array();
        // $this->breadcrumbs->unshift(lang('catalog'), '/');
        // $this->breadcrumbs->push(lang('brand'), '/catalog/brands');

        if ($id) {
            $id = decode($id);
            $this->data['data'] = $this->etalase_model->get($id);
            // $this->data['data']->description = tinymce_get_url_files($this->data['data']->description);
            // $this->breadcrumbs->push(lang('etalase_edit_heading'), '/products/etalase/form/' . encode($id));
            // $this->breadcrumbs->push($this->data['data']->name, '/');
        } 
        // else {
        //     $this->breadcrumbs->push(lang('etalase_add_heading'), '/products/etalase/form/');
        // }
        // $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(($this->data['data']) ? lang('etalase_edit_heading') : lang('etalase_add_heading'));
        $this->load->view('etalase/etalase_form', $this->data);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'lang:etalase_form_name_label', 'trim|required');
        

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);

            do {
                if ($data['id'])
                    $data['id'] = decode($data['id']);


                if (!$data['id']) {
                    $unique = $this->etalase_model->etalase_unique_check($this->data['user']->merchant,$data['name']);
                    if($unique){
                        $return = array('message' => sprintf(lang('etalase_not_unique')), 'status' => 'error');
                        break;
                    }
                    else{
                        $id = $this->main->insert('etalase',array('name' => $data['name'],'store_id' =>$this->data['user']->merchant,'store_type' =>'merchant'));
                    }
                    
                } else {
                    $unique = $this->etalase_model->etalase_unique_check($this->data['user']->merchant,$data['name']);
                    if($unique){
                        $return = array('message' => sprintf(lang('etalase_not_unique')), 'status' => 'error');
                        break;
                    }
                    else{
                        $save = $this->main->update('etalase',array('name' => $data['name'],'store_id' =>$this->data['user']->merchant,'store_type' => 'merchant'), array('id' => $data['id']));
                    }
                }
                

                $return = array('message' => sprintf(lang('etalase_save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('products/etalase'));
            } while (0);
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $data = $this->main->get('etalase', array('id' => $id));
        $delete = $this->main->delete('etalase', array('id' => $id));
        if ($delete) {
            $this->main->update('products',array('etalase' => 0),array('etalase' => $data->id));
            
            $return = array('message' => sprintf(lang('etalase_delete_success_message'), $data->name), 'status' => 'success');
        } else {
            $return = array('message' => lang('etalase_delete_error_message'), 'status' => 'error');
        }

        echo json_encode($return);

        
    }

}
