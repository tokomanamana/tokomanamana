<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Products_model extends CI_Model {

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->_where_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
//        return $this->db->query("SELECT p.*, c.name category 
//                    FROM (SELECT p.id, p.code, p.name, p.category, p.discount, p.price, price_old, p.quantity, status, merchant, 'pm' FROM products p
//                        WHERE p.merchant = " . $this->data['user']->merchant . "
//                        UNION
//                        SELECT p.id, p.code, p.name, p.category, pm.discount, pm.price, pm.price_old, pm.quantity, pm.status, p.merchant, pm.id FROM products p
//                        LEFT JOIN product_merchant pm ON p.id = pm.product
//                        WHERE pm.merchant = " . $this->data['user']->merchant . ") p
//                        LEFT JOIN categories c ON p.category = c.id
//                        " . $this->_where_like($search) . "
//                        " . (($order) ? 'ORDER BY ' . $this->_get_alias_key($order['column']) . ' ' . $order['dir'] : '') . "
//                        LIMIT $start, $length
//                ");
        $this->db->select('ap.id, p.name, p.code, c.name category, IFNULL(pp.price, p.price) price, pi.image, pp.id ppid, pm.quantity')
                ->join('product_price pp', 'p.category = c.id AND pp.merchant_group = ' . $this->data['user']->merchant_group, 'left')
                ->join('product_image pi', 'pi.product = p.id AND pi.primary = 1', 'left')
                ->join('product_merchant pm', 'p.id = pm.product', 'left')
                ->join('categories c', 'p.category = c.id', 'left')
                ->where('p.merchant', 0)
                ->where('p.status', 1)
                ->limit($length, $start);
        return $this->db->get('products p');
    }

    private function _get_alias_key($key) {
        switch ($key) {
            case 1: $key = 'code';
                break;
            case 2: $key = 'p.name';
                break;
            case 4: $key = 'c.name';
                break;
            case 3: $key = 'price';
                break;
            case 5: $key = 'quantity';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
//        $this->_where_like($search);
//        $this->db->select('p.*, c.name category')
//                ->from("(SELECT p.id, p.code, p.name, p.category, p.discount, p.price, price_old, p.quantity, status, merchant, 'pm' FROM products p
//                        WHERE p.merchant = " . $this->data['user']->merchant . "
//                        UNION
//                        SELECT p.id, p.code, p.name, p.category, pm.discount, pm.price, pm.price_old, pm.quantity, pm.status, p.merchant, pm.id FROM products p
//                        LEFT JOIN product_merchant pm ON p.id = pm.product
//                        WHERE pm.merchant = " . $this->data['user']->merchant . ") p")
//                ->join('categories c', 'p.category = c.id', 'left');
        $query = $this->db->query("SELECT COUNT(*) count
                    FROM (SELECT p.id, p.code, p.name, p.category, p.discount, p.price, price_old, p.quantity, status, merchant, 'pm' FROM products p
                        WHERE p.merchant = " . $this->data['user']->merchant . "
                        UNION
                        SELECT p.id, p.code, p.name, p.category, pm.discount, pm.price, pm.price_old, pm.quantity, pm.status, p.merchant, pm.id FROM products p
                        LEFT JOIN product_merchant pm ON p.id = pm.product
                        WHERE pm.merchant = " . $this->data['user']->merchant . ") p
                        LEFT JOIN categories c ON p.category = c.id
                        " . $this->_where_like($search));
        return $query->row()->count;
    }

    private function _where_like($search = '') {
        $columns = array('code', 'p.name', 'c.name', 'price');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $col) {
                $this->db->like_or("IFNULL($col,'')", $search);
            }
            $this->db->group_end();
        }

//        $where = '';
//        if ($search) {
//            $where .= 'WHERE ';
//            $where .= "IFNULL('code','') LIKE '%$search%'";
//            $where .= "OR IFNULL('p.name','') LIKE '%$search%'";
//            $where .= "OR IFNULL('c.name','') LIKE '%$search%'";
//        }
//        return $where;
    }

    function get_categories() {
        $this->db->select("pcp.category id, GROUP_CONCAT(pc2.name ORDER BY pcp.level SEPARATOR ' > ') name", FALSE)
                ->join('categories pc1', 'pcp.category = pc1.id', 'left')
                ->join('categories pc2', 'pcp.path = pc2.id', 'left')
                ->order_by('pc2.name')
                ->group_by('pcp.category');

        return $this->db->get('category_path pcp');
    }

    function get_product_features($product) {
        $this->db->select('pf.product, pf.value, f.*')
                ->join('features f', 'pf.feature = f.id', 'left')
                ->where('pf.product', $product)
                ->group_by('pf.feature');
        return $this->db->get('product_feature pf');
    }

   

    function check_exist_code($code, $product = '', $is_combination = false) {
        $query = $this->db->query("SELECT id, code FROM products p WHERE code = '$code'"
                . (($product && $is_combination) ? " AND id != " . $product : "")
                . " UNION"
                . " SELECT id, code FROM product_combination pc WHERE code = '$code'"
                . (($product && !$is_combination) ? " AND id != " . $product : ""));
        return ($query->num_rows() > 0) ? true : false;
    }

    function get_features_by_category($category, $product = 0) {
        $this->db->select('f.*, pf.value')
                ->join('features f', 'cf.feature = f.id', 'left')
                ->join('product_feature pf', 'pf.feature = cf.feature AND pf.product = ' . $product, 'left')
                ->where('cf.category', $category)
                ->group_by('cf.feature')
                ->order_by('f.sort_order ASC');
        return $this->db->get('category_feature cf');
    }

    function get_product_feature_by_category($category, $product = 0) {
        $this->db->select('cf.category, f.*, fv.value variant')
                ->join('features f', 'f.id = cf.feature', 'left')
                ->join('feature_variant fv', 'fv.id = cf.feature', 'left')
                ->where('cf.category', $category)
                ->group_by('cf.feature')
                ->order_by('f.sort_order ASC');

        if ($product) {
            $this->db->select('pf.product, pf.value')
                    ->join('product_feature pf', 'pf.feature = cf.feature AND pf.product = ' . $product, 'left');
        }
        return $this->db->get('category_feature cf');
    }

    function get_category($id) {
        $this->db->select("cp.category id, GROUP_CONCAT(c2.name ORDER BY cp.level SEPARATOR ' > ') name, c1.image, c1.active, c1.sort_order", FALSE)
                ->join('categories c1', 'cp.category = c1.id', 'left')
                ->join('categories c2', 'cp.path = c2.id', 'left')
                ->group_by('cp.category');
        return $this->db->get_where('category_path cp', array('cp.category' => $id))->row();
    }

//    function get_pack_product_search($search, $list_pack) {
//        $this->db->select('name')
//                ->join('features f', 'pf.feature = f.id', 'left')
//                ->where('pf.product', $product)
//                ->group_by('pf.feature');
//        return $this->db->get('product_feature pf');
//    }

    function search($page = 1, $search = '', $count = false) {
        $this->db->select('p.id, name, code, price, pi.image')
                ->join('product_image pi', 'p.id = pi.product AND pi.primary = 1', 'left')
                ->where('merchant', 0)
                ->where('status', 1)
                ->like('name', $search)
                ->or_like('code', $search);
        if (!$count) {
            $start = ($page - 1) * 9;
            $this->db->limit(9, $start);
            return $this->db->get('products p');
        } else {
            return $this->db->count_all_results('products p');
        }
    }

}
