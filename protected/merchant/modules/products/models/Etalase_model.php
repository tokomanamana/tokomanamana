<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Etalase_model extends CI_Model {

    function get($id) {
        $this->db->select('e.*')
                ->where('e.id', $id);
        $query = $this->db->get('etalase e');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }
    
    function get_all($store_id,$start = 0, $length, $search = '', $order = array()) {
        $this->_where_like($search);
        if ($order) {
            $order['column'] = $this->get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('e.*,e.name AS etalase_name,p.name AS product_name')
                 ->join('products p','p.etalase = e.id','left')
        
                 ->where('e.store_id',$store_id)
                 ->limit($length, $start)
                 ->group_by('p.etalase');

        return $this->db->get('etalase e');
    }

    function get_related_products($id){
          $this->db->select('p.name AS product_name')
                ->where('p.etalase',$id)
                ->order_by('p.name','ASC');

                
        return $this->db->get('products p');
    }

    function get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'e.name';
                break;
            case 1: $key = 'p.name';
            // case 2: $key = 'seo_url';
            //     break;
        }
        return $key;
    }

    function etalase_unique_check($store_id,$name){
        $this->db->select('id')
             ->where('store_id',$store_id)
             ->where('name',$name);
        $query = $this->db->get('etalase');
        return ($query->num_rows() > 0) ? true : false;

    }

    // function count_all($merchant,$search = '') {
    //     $this->_where_like($search);
    //     $this->db->where('e.store_id',$merchant);
    //     return $this->db->select('e.id')->count_all_results('etalase e');
    // }

     function count_all($merchant, $search = '') {
        $this->_where_like($search);
        $this->db->join('products p','p.etalase = e.id','left')
                ->where('e.store_id',$merchant);
        return $this->db->count_all_results('etalase e');
    }

    // function where_like($search = '') {
    //     $columns = array('e.name','p.product_name');
    //     if ($search) {
    //         foreach ($columns as $column) {
    //             $this->db->like('IFNULL(' . $column . ',"")', $search);
    //         }
    //     }
    // }
    private function _where_like($search = '') {
        $columns = array('e.name', 'p.name');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $col) {
                $this->db->or_like("IFNULL($col,'')", $search);
            }
            $this->db->group_end();
        }
    }

}
