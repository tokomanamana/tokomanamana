<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Principal_model extends CI_Model {

    function get($id) {
        $this->db->select('p.*, keyword seo_url')
                ->join('seo_url su', "su.query = CONCAT('catalog/products/view/',p.id)", 'left')
                ->where('p.id', $id);
        $query = $this->db->get('products p');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    // function get_all($merchant, $merchant_group, $start = 0, $length, $search = '', $order = array()) {
    //     $this->_where_like($search);
    //     if ($order) {
    //         $order['column'] = $this->_get_alias_key($order['column']);
    //         $this->db->order_by($order['column'], $order['dir']);
    //     }
    //     $this->db->select('p.id, pi.image, p.code, p.name, p.category, c.name category, IFNULL(pp.price, p.price) price, IFNULL(pm.quantity,0) quantity, pm.id product_merchant, pm.pricing_level')
    //             ->join('product_image pi','p.id = pi.product AND pi.primary = 1','left')
    //             ->join('categories c', 'c.id = p.category', 'left')
    //             ->join('brands br', 'br.id = p.brand', 'left')
    //             ->join('product_price pp', 'pp.product = p.id AND pp.merchant_group ='.$merchant_group, 'left')
    //             ->join('product_merchant pm', 'pm.product = p.id AND pm.merchant ='.$merchant, 'left')
    //             ->where('p.status = 1')
    //             ->where('br.principle_id = '.$this->data['user']->principal_id)
    //             ->limit($length, $start);

    //     return $this->db->get('products p');
    // }

    function get_all($merchant, $merchant_group, $start = 0, $length, $search = '', $order = array()) {
        $this->_where_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('p.id, pi.image, p.code, p.name, p.category, c.name category, (CASE WHEN psp.price != 0 THEN psp.price ELSE (CASE WHEN p.variation = 1 AND psp.id_option THEN po.price ELSE p.price END) END) as price, p.date_added, psp.quantity, psp.id_option, p.variation, (CASE WHEN p.variation = 1 THEN po.id ELSE 0 END) as id_option')
                ->join('products p', 'p.id = psp.product_id', 'left')
                ->join('product_image pi','p.id = pi.product AND pi.primary = 1','left')
                ->join('product_option po', 'po.id = psp.id_option', 'left')
                ->join('categories c', 'c.id = p.category', 'left')
                ->where('p.status = 1')
                ->where('psp.branch_id', $this->data['user']->merchant)
                ->group_by('(CASE WHEN p.variation = 1 THEN po.id ELSE p.id END)')
                ->limit($length, $start);

        return $this->db->get('products_principal_stock psp');
    }

    private function _get_alias_key($key) {
        switch ($key) {
            case 1: $key = 'p.code';
                break;
            case 2: $key = 'p.name';
                break;
            case 3: $key = 'psp.id_option';
                break;
            case 4: $key = 'price';
                break;
            case 5: $key = 'c.name';
                break;
            case 6: $key = 'p.date_added';
                break;
            case 7: $key = 'psp.quantity';
                break;
        }
        return $key;
    }

    // function count_all($merchant, $merchant_group, $search = '') {
    //     $this->_where_like($search);
    //     $this->db->join('product_image pi','p.id = pi.product AND pi.primary = 1','left')
    //             ->join('categories c', 'c.id = p.category', 'left')
    //             ->join('product_price pp', 'pp.product = p.id AND pp.merchant_group ='.$merchant_group, 'left')
    //             ->join('product_merchant pm', 'pm.product = p.id AND pm.merchant ='.$merchant, 'left')
    //             ->join('brands br', 'br.id = p.brand', 'left')
    //             ->where('p.status = 1')
    //             ->where('br.principle_id = '.$this->data['user']->principal_id);
    //     return $this->db->count_all_results('products p');
    // }

    function count_all($merchant, $merchant_group, $search = '') {
        $this->_where_like($search);
        $this->db->join('products p', 'p.id = psp.product_id', 'left')
                ->join('product_image pi','p.id = pi.product AND pi.primary = 1','left')
                ->join('categories c', 'c.id = p.category', 'left')
                ->where('p.status = 1')
                ->where('psp.branch_id', $this->data['user']->merchant);

        return $this->db->count_all_results('products_principal_stock psp');
    }

    private function _where_like($search = '') {
        $columns = array('p.code', 'p.name', 'c.name');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $col) {
                $this->db->or_like("IFNULL($col,'')", $search);
            }
            $this->db->group_end();
        }
    }

    function get_username($merchant){
        $this->db->select('mu.username')
             ->join('merchant_users mu','mu.id = m.auth')
             ->where('m.id',$merchant);
        return $this->db->get('merchants m')->row();
    }
    
    function get_categories() {
        $this->db->select("pcp.category id, GROUP_CONCAT(pc2.name ORDER BY pcp.level SEPARATOR ' > ') name", FALSE)
                ->join('categories pc1', 'pcp.category = pc1.id', 'left')
                ->join('categories pc2', 'pcp.path = pc2.id', 'left')
                ->order_by('pc2.name')
                ->group_by('pcp.category');

        return $this->db->get('category_path pcp');
    }

    function get_product_features($product) {
        $this->db->select('pf.product, pf.value, f.*')
                ->join('features f', 'pf.feature = f.id', 'left')
                ->where('pf.product', $product)
                ->group_by('pf.feature');
        return $this->db->get('product_feature pf');
    }

    function get_product_feature_variants($product, $feature) {
        $this->db->select('fv.*')
                ->join('feature_variant fv', 'pf.feature = fv.feature AND pf.value = fv.id', 'left')
                ->where('pf.product', $product)
                ->where('pf.feature', $feature);
        $query = $this->db->get('product_feature pf');
        return($query->num_rows() > 0) ? $query : false;
    }

    function get_product_feature_by_category($category, $product = 0) {
        $this->db->select('cf.category, f.*, fv.value variant')
                ->join('features f', 'f.id = cf.feature', 'left')
                ->join('feature_variant fv', 'fv.id = cf.feature', 'left')
                ->where('cf.category', $category)
                ->group_by('cf.feature')
                ->order_by('f.sort_order ASC');

        if ($product) {
            $this->db->select('pf.product, pf.value')
                    ->join('product_feature pf', 'pf.feature = cf.feature AND pf.product = ' . $product, 'left');
        }
        return $this->db->get('category_feature cf');
    }

    function get_features_by_category($category, $product = 0) {
        $this->db->select('f.*, pf.value')
                ->join('features f', 'cf.feature = f.id', 'left')
                ->join('product_feature pf', 'pf.feature = cf.feature AND pf.product = ' . $product, 'left')
                ->where('cf.category', $category)
                ->group_by('cf.feature')
                ->order_by('f.sort_order ASC');
        return $this->db->get('category_feature cf');
    }

    function get_prices($product = '') {
        $this->db->select('pp.*, mg.id merchant_group, mg.name')
                ->join('product_price pp', 'mg.id = pp.merchant_group '.(($product)?'AND pp.product = '.$product:''), 'left');
        return $this->db->get('merchant_groups mg');
    }
    
    function price_level_group($product,$group=0){
        $this->db->select('pclg.*, pcl.min_qty, pcl.product')
                ->join('product_price_level pcl','pcl.id = pclg.product_price_level','left')
                ->where_in('product_price_level','(SELECT id FROM product_price_level WHERE product = '.$product.')',false)
                ->where('merchant_group',$group)
                ->order_by('pcl.min_qty asc');
        return $this->db->get('product_price_level_groups pclg');
    }

    function export_data($merchant,$merchant_group) {
        $this->db->select('p.id, pi.image, p.code, p.name, p.category, c.name category, IFNULL(pp.price, p.price) price, IFNULL(pm.quantity,0) quantity, pm.id product_merchant, pm.pricing_level')
                ->join('product_image pi','p.id = pi.product AND pi.primary = 1','left')
                ->join('categories c', 'c.id = p.category', 'left')
                ->join('product_price pp', 'pp.product = p.id AND pp.merchant_group ='.$merchant_group, 'left')
                ->join('product_merchant pm', 'pm.product = p.id AND pm.merchant ='.$merchant, 'left')
                ->where('p.status = 1')
                ->where('p.merchant = 0')
                ->order_by('quantity','DESC')
                ->order_by('p.name','ASC');

        return $this->db->get('products p');
    }

    function get_variation($id_product, $option) {
        $this->db->select('o.type, o.value')
                ->join('product_option po', 'po.id = poc.product_option', 'INNER')
                ->join('options o', 'o.id = poc.option', 'INNER')
                ->where('po.product', $id_product)
                ->where('po.id', $option);
        return $this->db->get('product_option_combination poc');
    }
}
