<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Other_model extends CI_Model {

    function get($id) {
        $this->db->select('p.*, keyword seo_url')
                ->join('seo_url su', "su.query = CONCAT('catalog/products/view/',p.id)", 'left')
                ->where('p.id', $id);
        $query = $this->db->get('products p');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->_where_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('p.id, code, p.name, price_old, discount, quantity, price, p.category, p.status, c.name category')
                ->join('categories c', 'c.id = p.category', 'left')
                ->where('p.merchant', $this->data['user']->merchant)
                ->limit($length, $start);
        
        return $this->db->get('products p');
    }

    private function _get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'code';
                break;
            case 1: $key = 'p.name';
                break;
            case 3: $key = 'price';
                break;
            case 2: $key = 'c.name';
                break;
            case 5: $key = 'p.status';
                break;
            case 4: $key = 'quatity';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->_where_like($search);
        $this->db->where('p.merchant', $this->data['user']->merchant)
                ->join('categories c', 'c.id = p.category', 'left');
        return $this->db->count_all_results('products p');
    }

    private function _where_like($search = '') {
        $columns = array('code', 'p.name', 'price', 'c.name');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
            $this->db->group_end();
        }
    }

    function get_categories() {
        $this->db->select("pcp.category id, GROUP_CONCAT(pc2.name ORDER BY pcp.level SEPARATOR ' > ') name", FALSE)
                ->join('categories pc1', 'pcp.category = pc1.id', 'left')
                ->join('categories pc2', 'pcp.path = pc2.id', 'left')
                ->order_by('pc2.name')
                ->group_by('pcp.category');

        return $this->db->get('category_path pcp');
    }

    function get_product_features($product) {
        $this->db->select('pf.product, pf.value, f.*')
                ->join('features f', 'pf.feature = f.id', 'left')
                ->where('pf.product', $product)
                ->group_by('pf.feature');
        return $this->db->get('product_feature pf');
    }

    function get_product_feature_variants($product, $feature) {
        $this->db->select('fv.*')
                ->join('feature_variant fv', 'pf.feature = fv.feature AND pf.value = fv.id', 'left')
                ->where('pf.product', $product)
                ->where('pf.feature', $feature);
        $query = $this->db->get('product_feature pf');
        return($query->num_rows() > 0) ? $query : false;
    }

    function get_product_feature_by_category($category, $product = 0) {
        $this->db->select('cf.category, f.*, fv.value variant')
                ->join('features f', 'f.id = cf.feature', 'left')
                ->join('feature_variant fv', 'fv.id = cf.feature', 'left')
                ->where('cf.category', $category)
                ->group_by('cf.feature')
                ->order_by('f.sort_order ASC');

        if ($product) {
            $this->db->select('pf.product, pf.value')
                    ->join('product_feature pf', 'pf.feature = cf.feature AND pf.product = ' . $product, 'left');
        }
        return $this->db->get('category_feature cf');
    }

    function get_features_by_category($category, $product = 0) {
        $this->db->select('f.*, pf.value')
                ->join('features f', 'cf.feature = f.id', 'left')
                ->join('product_feature pf', 'pf.feature = cf.feature AND pf.product = ' . $product, 'left')
                ->where('cf.category', $category)
                ->group_by('cf.feature')
                ->order_by('f.sort_order ASC');
        return $this->db->get('category_feature cf');
    }

    function get_combinations($product, $combination = '') {
        $this->db->select("pc.*, GROUP_CONCAT(CONCAT(ag.name, ' - ', a.value) SEPARATOR ', ') combination")
                ->join('product_combination_attribute pca', 'pc.id = pca.product_combination', 'left')
                ->join('attributes a', 'pca.attribute = a.id', 'left')
                ->join('attribute_group ag', 'a.attribute_group = ag.id', 'left')
                ->where('pc.product', $product)
                ->group_by('pc.id');
        if ($combination) {
            $this->db->where('pc.id', $combination);
        }
        return $this->db->get('product_combination pc');
    }

    function check_exist_code($code, $product = '', $is_combination = false) {
        $query = $this->db->query("SELECT id, code FROM products p WHERE code = '$code'"
                . (($product && $is_combination) ? " AND id != " . $product : "")
                . " UNION"
                . " SELECT id, code FROM product_combination pc WHERE code = '$code'"
                . (($product && !$is_combination) ? " AND id != " . $product : ""));
        return ($query->num_rows() > 0) ? true : false;
    }

    function check_exist_combination($product, $combination) {
        $this->db->select("pc.id, GROUP_CONCAT(pca.attribute ORDER BY pca.attribute ASC SEPARATOR ',') attribute_group")
                ->join('product_combination_attribute pca', 'pca.product_combination = pc.id', 'left')
                ->where('pc.product', $product)
                ->group_by('pca.product_combination')
                ->having('attribute_group', $combination);
        return ($this->db->get('product_combination pc')->num_rows() > 0) ? true : false;
    }

    function get_prices($product = '') {
        $this->db->select('pp.*, mg.id merchant_group, mg.name')
                ->join('product_price pp', 'mg.id = pp.merchant_group '.(($product)?'AND pp.product = '.$product:''), 'left');
        return $this->db->get('merchant_groups mg');
    }

//    function get_pack_product_search($search, $list_pack) {
//        $this->db->select('name')
//                ->join('features f', 'pf.feature = f.id', 'left')
//                ->where('pf.product', $product)
//                ->group_by('pf.feature');
//        return $this->db->get('product_feature pf');
//    }
}
