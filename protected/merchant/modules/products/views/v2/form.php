<style type="text/css">#description_ifr{width:99%!important}.product_title{margin-bottom:18px;margin-left:10px;margin-right:10px}.wajib{color:#fff;bottom:0;display:inline-block;margin-top:3px;margin-left:1px;margin-right:1px;position:relative;z-index:1;padding:2px 5px;background-color:#97c23c;font-size:10px;border-radius:2px}.circle{color:#fff;bottom:0;display:inline-block;margin-left:3px;position:relative;z-index:1;padding:4px;background-color:#97c23c;border-radius:50px}.nav-tabs{border-bottom:1px solid #97c23c}.nav-tabs>li.active>a{border:1px solid #97c23c;border-bottom-color:transparent}.page-title{width:auto !important;}#mceu_18{overflow-x: scroll;}.nav-tabs:before {content: 'Tabs .Produk';}#product-form #images{min-height: 163px;}h4{display: inline-block;}.info_price{font-size:small;display: inline-block;color:gray;}.inactive{background-color: #ccc;opacity: 0.3;}.panel-variasi-colour{margin-bottom: 10px !important;}.form-group-grosir{margin-left:0px !important;}#table-container{overflow:auto}.checkbox-status-variant{height:0;width:0;visibility:hidden;display:none}.label-status-variant{cursor:pointer;text-indent:-9999px;width:70px;height:35px;background:grey;display:block;border-radius:100px;position:relative}.label-status-variant:after{content:'';position:absolute;top:3px;left:4px;width:30px;height:30px;background:#fff;border-radius:90px;transition:.3s}.checkbox-status-variant:checked+.label-status-variant{background:#bada55}.checkbox-status-variant:checked+.label-status-variant:after{left:calc(100% - 4px);transform:translateX(-100%)}.checkbox-status-variant:active:after{width:70px}.custom-error-label{color:#f44336;}label{font-weight: 600;color: #878b8f}

</style>
<div class="content-wrapper" id="product-form">
    <form action="<?php echo site_url('products/productv2/save'); ?>" class="" method="post" id="form">
        <div class="page-header page-header-default">
            <div class="page-header-content">
                <div class="page-title" >
                <?php if($data->name != NULL || $data->name != ''){ ?>
                <h1>Edit Produk</h1>
                <?php }else{ ?>
                <h1>Produk Baru</h1>
                <?php } ?>
                </div>

                <div class="heading-elements">
                    <div class="heading-btn-group">
                        <a href="<?php echo site_url('products/productv2'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('list_heading'); ?></span></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <input type="hidden" name="id" id="id" value="<?php echo $data->id; ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#general" data-toggle="tab"><?php echo lang('general_tabs'); ?></a></li>
                                    <li id="option-tab" class=""><a id="a_option" href="#option" data-toggle="tab"><?php echo lang('option_tabs'); ?></a></li>
                                    <li><a href="#price" data-toggle="tab"><?php echo lang('price_tabs'); ?></a></li>
                                  <!--   <li class=""><a href="#feature" data-toggle="tab"><?php echo lang('feature_tabs'); ?></a></li> -->
                                    <li class=""><a href="#seo" data-toggle="tab"><?php echo lang('seo_tabs'); ?></a></li>
                                    <!-- <li id="package-tab" class=""><a id="a_package" href="#package" data-toggle="tab">Package</a></li> -->
                                    <li id="promo-tab" class=""><a id="a_promo" href="#promo" data-toggle="tab">Promo</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="general">
                                        <div class="row">
                                            <div class="product_title">
                                                <label><?php echo lang('product_form_product_name'); ?></label>
                                                <div class="wajib">Wajib</div>
                                                <input class="form-control big-input" required="" type="text" name="name" minlength="4" maxlength="70" value="<?php echo $data->name; ?>" placeholder="<?php echo lang('name_placeholder'); ?>">
                                            </div>
                                            
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label><?php echo lang('product_image_th'); ?></label>
                                                    <div class="wajib">Wajib</div>
                                                    <div id="images">
                                                        <?php
                                                        if ($images) {
                                                            foreach ($images->result() as $image) {
                                                                echo '<div class="image" id="img' . $image->id . '"><div class="image-bg" style="background-image: url(\'' . base_url('../files/images/' . $image->image) . '\')"></div>'
                                                                . '<div class="option">'
                                                                . '<div class="radio"><label><input type="radio" value="' . $image->id . '" ' . (($image->primary) ? 'checked' : '') . ' name="image_primary" id="image_primary' . $image->id . '">Default</label></div>'
                                                                . '<div><a href="javascript:delete_image(\'' . $image->id . '\')" class="text-muted pull-right"><i class="icon-trash"></i></a></div>'
                                                                . '</div>'
                                                                . '</div>';
                                                            }
                                                        }
                                                        ?>

                                                        <div class="image" id="add-image">
                                                            <div>
                                                                <span>+</span>
                                                            </div>
                                                            <input type="hidden" id="add-image-value">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label><?php echo lang('short_description'); ?></label>
                                                     <div class="wajib">Wajib</div>
                                                    <textarea cols="30" rows="2" class="form-control" id="short_description" name="short_description" placeholder="<?php echo lang('short_description_placeholder'); ?>"><?php echo ($data) ? $data->short_description : ''; ?></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label><?php echo lang('description'); ?></label>
                                                    <textarea cols="30" rows="8" class="form-control" id="description" name="description" placeholder="<?php echo lang('description_placeholder'); ?>"><?php echo ($data) ? $data->description : ''; ?></textarea>
                                                </div>
                                                <div class="form-group" style="border-bottom : 1px solid #999">
                                                    <label><?php echo lang('table_description'); ?></label>
                                                    <table class="table table-hover" id="table_desk">
                                                        <thead>
                                                            <tr>
                                                                <th>Nama Spesifikasi</th>
                                                                <th>Nilai Spesifikasi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <!-- ISI DARI AJAX -->
                                                            <?php
                                                                if (isset($data) && isset($data->table_description)) {
                                                                    foreach ($data->table_description as $tab) {
                                                                        $key_tab = $tab->row_id;
                                                                        $desk = $tab->id_deskripsi;
                                                            ?>
                                                            <tr><!-- name="package_items[<?php echo $key;?>][product_name]" value="<?php echo $item->product_name?>" -->
                                                            <td style="width:200px"><?php echo ucwords($tab->name); ?></td>
                                                            <input type="hidden" name="table_description[<?php echo $key_tab;?>][row_id]" id="tabledeskrowid<?php echo $key_tab;?>"  class="form-control" value="<?php echo $key_tab;?>">
                                                            <input type="hidden" name="table_description[<?php echo $key_tab;?>][id_deskripsi]" id="tabledescriptionid<?php echo $key_tab;?>"  class="form-control" value="<?php if($tab->id_deskripsi){ echo $key_tab; } ?>">
                                                            <input type="hidden"  name="table_description[<?php echo $key_tab;?>][name]" id="tabledescriptionname<?php echo $key_tab;?>"  class="form-control" value="<?php echo $tab->name;?>">
                                                            <td><input type="text" name="table_description[<?php echo $key_tab;?>][value]" value="<?php echo $tab->value?>" id="tabledescription<?php echo $key_tab;?>"  class="form-control" placeholder="Silahkan input <?php echo $tab->name?> produk Anda" required>
                                                            </td>
                                                            </tr>
                                                        <!-- <br><a onclick="add_param()" class="btn btn-success">Add Param</a><br><br> -->    <?php } ?> 
                                                            <?php } else {?>
                                                            <tr>
                                                                <td colspan="2" style="font-style:italic"><center>*Silahkan Pilih Kategori 
                                                                 Terlebih dahulu</center></td>
                                                            </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                    <table class="table table-hover" id="param_other_table_desk">
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                    <input type="hidden" id="count_btn" value="0"/>
                                                    <div id="add_param_table_desk">
                                                        
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label><?php echo lang('status'); ?></label>
                                                    <input type="checkbox" name="status" value="1" data-on-text="<?php echo lang('enabled'); ?>" data-off-text="<?php echo lang('disabled'); ?>" class="switch" <?php echo ($data) ? (($data->status == 1) ? 'checked' : '') : 'checked'; ?>>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                               
                                               <!--  <div class="form-group">
                                                    <label>Tipe Produk</label>
                                                    <select class="form-control"  name="type" id="type">
                                                        <option value="s" <?php// echo ($data) ? ($data->type == 's') ? 'selected' : '' : ''; ?>>Standard</option>
                                                        <option value="p" <?php //echo ($data) ? ($data->type == 'p') ? 'selected' : '' : ''; ?>>Package</option>
                                                    </select>
                                                </div> -->
                                                <input type="hidden" name="type" id="type" value="s">
                                                <div class="form-group">
                                                    <label>Jenis</label>
                                                    <select class="bootstrap-select form-control" name="preorder" id="preorder">
                                                        <option value="0" <?php echo ($data) ? ($data->preorder == '0') ? 'selected' : '' : ''; ?>>Ready Stock</option>
                                                        <option value="1" <?php echo ($data) ? ($data->preorder == '1') ? 'selected' : '' : ''; ?>>Pre-Order</option>
                                                        <option value="2" <?php echo ($data) ? ($data->preorder == '2') ? 'selected' : '' : ''; ?>>Coming Soon</option>
                                                    </select>
                                                </div>     
                                                <div class="form-group" id="preorder-time">
                                                    <label class="label-time"></label>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <input style="width: 60px;" id="preorder-time-id" type="number" name="preorder_time" class="form-control" value="<?php echo ($data) ? $data->preorder_time : 0; ?>">
                                                                <span class="input-group-addon">Hari</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group" id="coming-soon-time">
                                                    <label class="label-time"></label>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">Tanggal</span><input class="form-control" type="text" name="coming_soon_time" id="coming-soon-time-id" value="<?php echo ($data) ? $data->coming_soon_time : $today ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Promo</label>
                                                    <select class="bootstrap-select form-control" name="promo" id="promo-slc">
                                                        <option value="0" <?php echo ($data) ? ($data->promo == '0') ? 'selected' : '' : ''; ?>>Tidak</option>
                                                        <option value="1" <?php echo ($data) ? ($data->promo == '1') ? 'selected' : '' : ''; ?>>Iya</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Variasi</label>
                                                    <select class="bootstrap-select form-control" name="variation" id="option-slc">
                                                        <option value="0" <?php echo ($data) ? ($data->variation == '0') ? 'selected' : '' : ''; ?>>Tidak</option>
                                                        <option value="1" <?php echo ($data) ? ($data->variation == '1') ? 'selected' : '' : ''; ?>>Iya</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label><?php echo lang('code'); ?></label>
                                                    <input type="text" name="code" id="code"  class="form-control" maxlength="15" placeholder="ex: A001" value="<?php echo ($data) ? $data->code : ''; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label><?php echo lang('category'); ?></label>
                                                     <div class="wajib">Wajib</div>
                                                    <select class="bootstrap-select" onchange="get_table_deskripsi()"  name="category" id="category" data-live-search="true" data-width="100%">
                                                        <option value=""></option>
                                                        <?php if ($categories) { ?>
                                                            <?php foreach ($categories->result() as $category) { ?>
                                                                <option value="<?php echo $category->id; ?>" <?php echo ($data) ? ($data->category == $category->id) ? 'selected' : '' : ''; ?>><?php echo $category->name; ?></option>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label><?php echo lang('brand'); ?></label>
                                                    <select class="bootstrap-select" onchange="other_brand()" name="brand" id="brand" data-live-search="true" data-width="100%">
                                                        <!-- <option value=""></option> -->
                                                         <option value="0" <?php if($data->brand == 0 || $data->brand ==NULL && $data->brand_other != ''){ echo 'selected'; } ?>><?php echo $data->brand_other; ?></option>
                                                        <?php if ($brands) { ?>
                                                            <?php foreach ($brands->result() as $brand) { ?>
                                                                <option value="<?php echo $brand->id; ?>" <?php echo ($data) ? ($data->brand == $brand->id) ? 'selected' : '' : ''; ?>><?php echo $brand->name; ?></option>
                                                            <?php } ?>
                                                            <option value="0" >Other</option>
                                                        <?php } ?>
                                                    </select>
                                                    <div id="brand_other_div"></div>
                                                        <!-- <?php if($data->brand == 0  && $data->brand_other != ''){?>
                                                        <input type="text" name="brand_other" id="brand_other"  class="form-control" value="<?php echo ($data) ? $data->code : ''; ?>">
                                                    <?php } ?>
 -->                                                </div>
                                                 <div class="form-group">
                                                    <label><?php echo 'Etalase' ?></label>
                                                    <select class="bootstrap-select" name="etalase" id="etalase" data-live-search="true" data-width="100%">
                                                        <option value=""></option>
                                                        <?php if ($etalases) { ?>
                                                            <?php foreach ($etalases->result() as $etalase) { ?>
                                                                <option value="<?php echo $etalase->id; ?>" <?php echo ($data) ? ($data->etalase == $etalase->id) ? 'selected' : '' : ''; ?>><?php echo $etalase->name; ?></option>
                                                            <?php } ?>
                                                        <?php } else { ?>
                                                            <option value="" disabled>Buat Etalase Terlebih Dahulu</option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label><?php echo lang('guarantee'); ?></label>
                                                    <input type="text" name="guarantee" id="guarantee" placeholder="ex: 1 Tahun"  class="form-control" value="<?php echo ($data) ? $data->guarantee : ''; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label><?php echo lang('dimension'); ?></label>
                                                     <div class="wajib">Wajib</div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <input type="number" min="1" name="length" class="form-control" required="" value="<?php if($data->length) echo $data->length ?>">
                                                                <span class="input-group-addon">cm</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <input type="number" min="1" name="width" class="form-control" required="" value="<?php if($data->width) echo $data->width ?>">
                                                                <span class="input-group-addon">cm</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <input type="number" min="1" name="height" class="form-control" required="" value="<?php if($data->height) echo $data->height ; ?>">
                                                                <span class="input-group-addon">cm</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label><?php echo lang('weight'); ?></label>
                                                     <div class="wajib">Wajib</div>
                                                    
                                                            <div class="input-group">
                                                                <input type="number" min="1" name="weight" class="form-control" required="" value="<?php  if($data->weight!=NULL) echo $data->weight;?>">
                                                                <span class="input-group-addon">gram</span>
                                                            </div>
                                                        
                                                </div>
                                                <div class="form-group form-group-stock">
                                                    <label><?php echo 'Stock Produk';?></label>
                                                     <div class="wajib">Wajib</div>
                                               
                                                       
                                                                <input type="number" min="0" id="stock_normal" name="quantity" class="form-control" required="" value="<?php  if($data->quantity!=NULL) echo $data->quantity;?>">
                                                           
                                                    
                                                </div>
                                                 <div class="form-group form-group-min">
                                                    <label><?php echo 'Kuantitas Minimum Pembelian';?></label>
                                                     <div class="wajib">Wajib</div>
                                                  
                                                            
                                                                <input type="number" min="1" id="min_buy" name="min_buy" class="form-control" required="" value="<?php  if($data->min_buy!=NULL) echo $data->min_buy;?>">
                                                            
                                                     
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane form-horizontal" id="price">
                                      <div id="form-group-price">
                                        <div class="form-group" >
                                            <label class="col-md-3 control-label"><?php echo lang('price'); ?> <div class="wajib">Wajib</div></label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon">Rp</span>
                                                    <input type="text" name="price" id="price-input" required min="100" class="form-control number" value="<?php echo ($data) ? $data->price : 0; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                       </div>
                                        <!-- <div class="row p-10" style="max-height: 176px; overflow: auto;">
                                            <?php
                                            if ($prices) {
                                                foreach ($prices->result() as $price) {
                                                    ?>
                                                    <div class="col-sm-4 well p-10">
                                                        <label><?php echo $price->name; ?></label>
                                                        <div class="row">
                                                            <div class="col-xs-8">
                                                                <div class="has-feedback has-feedback-left">
                                                                    <input type="text" class="form-control input-sm number" name="prices[<?php echo $price->merchant_group; ?>][price]" value="<?php echo ($price->price) ? $price->price : 0; ?>">
                                                                    <div class="form-control-feedback">
                                                                        Rp
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div> -->
                                        
                                        <h4>Harga Grosir</h4>
                                        <input type="hidden" id="price-count" value="1">
                                        <div class="form-group form-group-grosir">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="input-group">
                                                        <select class="form-control" name="price_grosir" id="grosir-slc">
                                                            <option value="0" <?php echo ($data) ? ($data->price_grosir == '0') ? 'selected' : '' : ''; ?>>Tidak</option>
                                                            <option value="1" <?php echo ($data) ? ($data->price_grosir == '1') ? 'selected' : '' : ''; ?>>Iya</option>
                                                    </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table id="price-list" class="table">
                                            <thead>
                                                <tr>
                                                    <th style="width: 80px">Min QTY</th>
                                                    <th style="width: 200px">Harga</th>
                                                    <th style="width: 50px"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if ($price_levels) {
                                                    foreach ($price_levels->result() as $price) {
                                                        // $groups = $this->products->price_level_groups($price->id);
                                                        ?>
                                                        <tr>
                                                            <td class="price-qty" width="25%"><input type="number" required min="2" name="price_level[<?php echo $price->id; ?>][qty]" class="form-control td-grosir-qty" value="<?php echo $price->min_qty; ?>"></td>
                                                            <td class="grosir_price_col" width="55%"><div class="input-group"><span class="input-group-addon">Rp</span><input type="text" required min="100" name="price_level[<?php echo $price->id; ?>][price]" class="form-control number number-grosir" value="<?php echo $price->price; ?>"></div></td>
                                                            <!-- <td>
                                                                <div class="row p-10" style="max-height: 176px; overflow: auto;">
                                                                    <?php
                                                                    if ($groups) {
                                                                        foreach ($groups->result() as $group) {
                                                                            ?>
                                                                            <div class="col-sm-6 well p-10">
                                                                                <label><?php echo $group->name; ?></label>
                                                                                <div class="row">
                                                                                    <div class="col-xs-12">
                                                                                        <div class="has-feedback has-feedback-left">
                                                                                            <input type="text" class="form-control input-sm number" name="price_level[<?php echo $price->id; ?>][group][<?php echo $group->merchant_group; ?>][price]" value="<?php echo ($group->price) ? $group->price : 0; ?>">
                                                                                            <div class="form-control-feedback">
                                                                                                Rp
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </td> -->
                                                            <td><button type="button" class="btn btn-danger btn-xs remove-price">x</button></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th colspan="4" class="text-center"><button type="button" style="float: left" id="add-price" class="btn btn-success">Add Price</button></th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    <hr>
                                        <h4>Harga Reseller</h4><i class="info_price">&nbsp;&nbsp;&nbsp;Harga untuk reseller yang membeli dengan kuantitas tinggi</i>
                                        <input type="hidden" id="price-reseller-count" value="1">
                                         <div class="form-group form-group-grosir">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="input-group">
                                                        <select class="form-control" name="price_reseller" id="reseller-slc">
                                                            <option value="0" <?php echo ($data) ? ($data->price_reseller == '0') ? 'selected' : '' : ''; ?>>Tidak</option>
                                                            <option value="1" <?php echo ($data) ? ($data->price_reseller == '1') ? 'selected' : '' : ''; ?>>Iya</option>
                                                    </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table id="price-reseller-list" class="table">
                                            <thead>
                                                <tr>
                                                    <th style="width: 80px">Min QTY</th>
                                                    <th style="width: 200px">Harga</th>
                                                    <th style="width: 50px"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if ($price_resellers) {
                                                    foreach ($price_resellers->result() as $resell) {
                                                        ?>
                                                        <tr>
                                                            <td class="price-qty-reseller" width="25%"><input type="number" required min="2" name="price_reseller_list[<?php echo $resell->id; ?>][qty]" class="form-control td-reseller-qty" value="<?php echo $resell->min_qty; ?>"></td>
                                                            <td class="reseller_price_col"width="55%"><div class="input-group"><span class="input-group-addon">Rp</span><input type="text" required min="100" name="price_reseller_list[<?php echo $resell->id; ?>][price]" class="form-control number number-reseller" value="<?php echo $resell->price; ?>"></div></td>
                                                            <td><button type="button" class="btn btn-danger btn-xs remove-price-reseller">x</button></td>
                                                        </tr>
                                                        <?php
                                                    }

                                                 } ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th colspan="4" class="text-center"><button type="button" style="float: left" id="add-price-reseller" class="btn btn-success">Add Reseller Price</button></th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <div class="tab-pane form-horizontal" id="option">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <div class="alert alert-info" style="background-color: #97c23c;border-color: #607f1d; z-index: 0">
                                                    <p>Untuk membuat variasi baru, pilih nilai variasi kemudian klik "Hasilkan"</p>
                                                    <i style="font-size: smaller;">*Jika anda membuat variasi dengan tipe baru (contoh: Warna berubah menjadi Ukuran), data variasi lama akan dihapus.</i>
                                                </div>
                                                <div id='table-container'>
                                                <table id="combination-list" class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Default</th>
                                                            <th>Variasi</th>
                                                            <th>Harga</th>
                                                            <th>Stock</th>
                                                            <th>Status</th>
                                                            <th style="width: 130px"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php if ($product_options) { ?>
                                                            <?php foreach ($product_options->result() as $po) { ?>
                                                                <tr id="option-<?php echo $po->id; ?>">
                                                                    <td><input type="radio" class="form-control" name="default_option" <?php echo ($po->default==1)?'checked':''; ?> value="<?php echo $po->id; ?>"></td>
                                                                    <td><?php echo $po->option_combination; ?></td>
                                                                    <td><div class="input-group"><span class="input-group-addon">Rp</span><input type="text" class="number form-control variant_price" size="10" required="" min="100" name="options[<?php echo $po->id; ?>][price]" value="<?php echo ($po->price) ? $po->price : 100; ?>"></div></td>
                                                                    <td><input type="number" class="form-control " size="10" required="" name="options[<?php echo $po->id; ?>][stock]" value="<?php echo $po->quantity; ?>"></td>
                                                                    <td style="padding-bottom: 0px;">
                                                                        <input type="hidden" name="options[<?php echo $po->id; ?>][status]" value="0" />
                                                                        <input type="checkbox" class="checkbox-status-variant" name="options[<?php echo $po->id; ?>][status]" id="variant_status<?php echo $po->id; ?>" value="1" <?php echo ($po->status == 1) ? 'checked' : ''?>><label class="label-status-variant" for="variant_status<?php echo $po->id; ?>">Toggle</label>
                                                                    </td>
                                                                    <td>
                                                                        <button type="button" class="btn btn-default btn-xs" onclick="set_image_option(<?php echo $data->id; ?>,<?php echo $po->id; ?>)"><i class="icon-image2"></i></button>
                                                                        <button type="button" class="btn btn-danger btn-xs" onclick="delete_option(<?php echo $po->id; ?>)"><i class="icon-x"></i></button>
                                                                    </td>
                                                                </tr>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                                </div>
                                            </div>
                                            <div id="option-list" class="col-md-3 panel-group content-group-lg">
                                                <div id="variation-form">
                                                    <div class="panel panel-white panel-variasi-colour">
                                                        <div class="panel-heading header-variasi">
                                                            <h6 class="panel-title" style="display: inline-block;">
                                                                Warna
                                                            </h6>
                                                            <label>
                                                              <input type="checkbox" value="1" class="switch variation-swich-colour">
                                                          </label>
                                                      </div>
                                                      <div class="panel-collapse collapse in" aria-expanded="true" style="">

                                                          <div class="panel-body variation-body-colour">
                                                            <?php if ($colours = $this->main->gets('options', array('type' => 'warna'))) { ?>
                                                                <?php foreach ($colours->result() as $colour) { ?>
                                                                    <div class="checkbox checkbox-colour-count">
                                                                        <label>
                                                                            <div style="width: 11px;height: 11px;border-radius: 100%;border: 0.4px solid #959595;display: inherit;background-color:<?= $colour->color;?>"></div>
                                                                            <input type="checkbox" disabled="" name="option[<?php echo $colour->type;?>][<?php echo $colour->id; ?>]" class="styled options checkbox-colour" value="<?php echo $colour->id; ?>"> <?php echo $colour->value; ?>
                                                                        </label>
                                                                    </div>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-white panel-variasi">
                                                    <div class="panel-heading header-variasi">
                                                        <h6 class="panel-title" style="display: inline-block;">
                                                            Ukuran
                                                        </h6>
                                                        <label>
                                                          <input type="checkbox" value="1" class="switch variation-swich-size">
                                                      </label>
                                                  </div>
                                                  <div class="panel-collapse collapse in" aria-expanded="true" style="">
                                                      <div class="panel-body variation-body-size">
                                                        <?php if ($sizes = $this->main->gets('options', array('type' => 'ukuran'))) { ?>
                                                            <?php foreach ($sizes->result() as $size) { ?>
                                                                <div class="checkbox checkbox-size-count">
                                                                    <label>
                                                                        <input type="checkbox" disabled="" name="option[<?php echo $size->type;?>][<?php echo $size->id; ?>]"  class="styled options checkbox-size" value="<?php echo $size->id; ?>"> <?php echo $size->value; ?>
                                                                    </label>
                                                                </div>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </div>
                                                </div>

                                            </div>
                                            <button type="button" id="option-generate" disabled="" class="btn border-slate text-slate-800 btn-flat mt-10" style="width: 100%">Hasilkan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                   
                                    <div class="tab-pane form-horizontal" id="seo">
                                        
                                                <input type="hidden" class="form-control" id="seo_url" name="seo_url" value="<?php echo ($data) ? $data->seo_url : ''; ?>">
                                           
                                       
                                                <input type="hidden" class="form-control" name="meta_title"  value="<?php echo ($data) ? $data->meta_title : ''; ?>">

                                                <input type="hidden" class="form-control" name="meta_description"  value="<?php echo ($data) ? $data->meta_description : ''; ?>">
                                            
                                        
                                            
                                           
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('meta_keyword'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" name="meta_keyword" class="tags-input" placeholder="Kata kunci untuk search" value="<?php echo ($data) ? $data->meta_keyword : ''; ?>">
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="tab-pane form-horizontal" id="promo">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Jenis Promo</label>
                                            <div class="col-md-3">
                                                <select id="promo_type" class="form-control"  name="promo_data[type]" required="">
                                                    <option id="fixed_amount" value="F" <?php if(($data ? ($data->promo_data ? $data->promo_data->type : NULL) : '') == 'F'){ echo "selected";}?>>Jumlah Tetap</option>
                                                    <option id="percentage" value="P" <?php if(($data ? ($data->promo_data ? $data->promo_data->type : NULL) : '') == 'P'){ echo "selected";}?>>Persentasi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- <div class="form-group">
                                            <label class="col-md-3 control-label">Tipe Promo</label>
                                            <div class="col-md-3">
                                                <select class="form-control" name="promo_data[coupon_type]" required="">
                                                    <option <?php// if(($data ? ($data->promo_data ? $data->promo_data->coupon_type : '') : '') //== 'all'){ echo "selected";}?> value="all">All</option>
                                                    <option <?php //if(($data ? ($data->promo_data ? $data->promo_data->coupon_type : //'') : '') //== 'branch'){ echo "selected";}?> value="branch">Branch</option>
                                                    <option <?php// if(($data ? ($data->promo_data ? $data->promo_data->coupon_type : //'') : '') //== 'merchant'){ echo "selected";}?> value="merchant">Merchant</option>
                                                </select>
                                            </div>

                                        </div> -->
                                        <input type="hidden" name="promo_data[coupon_type]" value='all'>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Jangka Waktu Promo <div class="wajib">Wajib</div></label>
                                            <div class="col-md-3">
                                                <input type="text" id="start_date" class="form-control date" name="promo_data[date_start]" value="<?php echo ($data) ? ($data->promo_data ? $data->promo_data->date_start : '')  : '' ?>" placeholder=""> 
                                            </div>
                                            <div class="col-xs-1" style="width: 0%"><p style="padding-top: 8px;">-</p></div>
                                            <div class="col-md-3">

                                                <input type="text" id="end_date" class="form-control date" name="promo_data[date_end]" value="<?php echo ($data) ? ($data->promo_data ? $data->promo_data->date_end : '')  : '' ?>" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Discount <div class="wajib">Wajib</div></label>
                                            <div id="discount_div" class="col-md-3">
                                                <div class="input-group">
                                                    <span id="discount_addon"class="input-group-addon">Rp</span>
                                                    <input type="text" id="discount" class="form-control number" name="promo_data[discount]" value="<?php echo ($data) ? ($data->promo_data ? $data->promo_data->discount : '')  : '' ?>">
                                                    <span id="discount_addon_percent"class="input-group-addon">%</span>
                                                </div>
                                            </div>
                                   

                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Free Ongkir</label>
                                            <div class="col-md-3">
                                            <select class="form-control" name="free_ongkir" id="ongkir-slc">
                                                <option value="0" <?php echo ($data) ? ($data->free_ongkir == '0') ? 'selected' : '' : ''; ?>>No</option>
                                                <option value="1" <?php echo ($data) ? ($data->free_ongkir == '1') ? 'selected' : '' : ''; ?>>Yes</option>
                                            </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer" style="padding-left: 10px;padding-right: 10px;">
                           
                                
                                <a class="btn btn-default" style="float:left;" href="<?php echo site_url('products/productv2'); ?>"><?php echo lang('button_cancel'); ?></a>
                                
                                <button type="submit" id="submit" style="float:right;" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                 
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<input type="hidden" name="username" id="username" value="<?php echo $username; ?>">
<input type="hidden" name="merchant_id" id="merchant_id" value="<?php echo $merchant_id; ?>">
 <input type="file" id="file_main_image" name="product_image" style="display:none">
<div id="filemanager" class="modal">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">File Manager</h5>
            </div>

            <div class="modal-body">

                <!--  $data['file_explorer'] = base_url('assets/resources/filemanager/dialog.php?type=0&MY_UPLOAD_PATH=user1') -->
                <iframe  width="100%" height="550" frameborder="0" 
                src="<?php echo site_url('filemanager/dialog.php?type=1&folder='.$username.'&editor=false&field_id=add-image-value&relative_url=1'); ?>"></iframe>
            </div>
        </div>
    </div>
</div>
<div id="option-image" class="modal">
    <input type="hidden" name="product_option">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Pilih Gambar Untuk Variasi Ini</h5>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" onclick="save_image_option()">Simpan</button>
            </div>
        </div>
    </div>
</div>
<script>
    //var groups = JSON.parse('<?php //echo json_encode($this->products->price_level_groups()->result()); ?>');
</script>
