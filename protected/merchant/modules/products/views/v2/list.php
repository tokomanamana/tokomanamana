<style type="text/css">#submit-import{margin-top: 2px;}@media (max-width: 768px){.heading-elements:not(.not-collapsible){height:100px;}#submit-import{margin-top: 8px;}}hr{margin-top:0px;}.list_image{max-width: 100px;}.have_variant{color:#708090;}</style>
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('product_heading'); ?></h2>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="javascript:void(0);" style="float:right;" class="btn btn-link btn-float has-text" type="button" id="btn-export">
                        <i class="icon-download7 text-primary"></i><span> Export</span>
                    </a>
                    <a href="javascript:void(0);" style="float:right;" class="btn btn-link btn-float has-text" type="button" id="btn-import">
                        <i class="icon-upload7 text-primary"></i><span> Import</span>
                    </a>
                    
                    <a href="<?php echo site_url('products/productv2/add_merchant'); ?>" style="float:right;" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span><?php echo lang('product_add_heading'); ?></span>
                    </a>
                    <!-- <?php if ($this->aauth->is_allowed('products/productv2/add')) { ?> -->
                    <!-- <?php } ?> -->
                </div>
            </div>
        </div>
    </div>
    <div class="content">
       
        <div class="panel panel-flat">
            <table class="table table-hover" id="table" data-url="<?php echo site_url('products/productv2/get_list'); ?>">
                <thead>
                    <tr>
                        <th class="no-sort"><?php echo lang('product_image_th'); ?></th>
                        <th class="default-sort" data-sort="asc"><?php echo lang('product_code_th'); ?></th>
                        <th><?php echo lang('product_name_th'); ?></th>
                        <th><?php echo lang('product_price_th'); ?></th>
                        <th><?php echo lang('product_category_th'); ?></th>
                        <th><?php echo "Tanggal"; ?></th>
                        <th><?php echo "Status" ?></th>
                        <th width="8%"><?php echo lang('product_quantity_th'); ?></th>
                        <th><?php echo "Action" ?></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>


<div id="modal-import" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Import Product</h5>
            </div>
            <div class="modal-body">
                <hr>
                <div class="panel panel-danger">
                  <div class="panel-heading" style="padding: 5px 20px;">Langkah-langkah mengimport produk</div>
                  <div class="panel-body" style=" padding-bottom: 0;padding-top: 9px;">
                    <ol>
                    <li>Download template import terbaru</li>
                    <li>Download list-list import terbaru yang digunakan untuk template</li>
                    <li>Isi template dengan teliti mengikuti contoh dan list-list</li>
                    <li>Upload file template yang sudah terisi dengan benar</li>
                    </ol>
                  </div>
                </div>
                <form class="form-horizontal" action="<?php echo site_url('products/productv2/import'); ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-sm-3">File Import</label>
                        <div class="col-sm-9">
                            <input type="file" id="file" name="file" class="form-control" required="">
                        </div>
                          <i style="color:red;float: right;padding-right: 14px;">Gunakan template untuk mengimport produk!</i>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href='<?php echo site_url('products/productv2/download_template_import');?>' ><button type="button" style="float:left; margin: 3px;" class="btn btn-success">Download Template Import</button></a>
                 <a href="<?php echo site_url('products/productv2/download_lists');?>"><button type="button" style="float:left; margin: 3px;" class="btn btn-success">Download List-list Import</button></a>
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                <button type="button" id="submit-import" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </div>
</div>
