<div class="content-wrapper" id="product-form">
    <form action="<?php echo site_url('products/productv2/save'); ?>" class="" method="post" id="form">
        <div class="page-header page-header-default">
            <div class="page-header-content">
                <div class="page-title">
                    <input class="form-control big-input" required="" type="text" name="name" value="<?php echo $data->name; ?>" placeholder="<?php echo lang('name_placeholder'); ?>">
                </div>

                <div class="heading-elements">
                    <div class="heading-btn-group">
                        <a href="<?php echo site_url('products/productv2'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('list_heading'); ?></span></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <input type="hidden" name="id" id="id" value="<?php echo $data->id; ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#general" data-toggle="tab"><?php echo lang('general_tabs'); ?></a></li>
                                    <li><a href="#price" data-toggle="tab"><?php echo lang('price_tabs'); ?></a></li>
                                    <li class=""><a href="#option" data-toggle="tab"><?php echo lang('option_tabs'); ?></a></li>
                                    <li class=""><a href="#feature" data-toggle="tab"><?php echo lang('feature_tabs'); ?></a></li>
                                    <li class=""><a href="#seo" data-toggle="tab"><?php echo lang('seo_tabs'); ?></a></li>
                                    <li id="package-tab" class=""><a href="#package" data-toggle="tab">Package</a></li>
                                    <li id="promo-tab" class=""><a href="#promo" data-toggle="tab">Promo</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="general">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <div id="images">
                                                        <?php
                                                        if ($images) {
                                                            foreach ($images->result() as $image) {
                                                                echo '<div class="image" id="img' . $image->id . '"><div class="image-bg" style="background-image: url(\'' . base_url('../files/images/' . $image->image) . '\')"></div>'
                                                                    . '<div class="option">'
                                                                    . '<div class="radio"><label><input type="radio" value="' . $image->id . '" ' . (($image->primary) ? 'checked' : '') . ' name="image_primary" id="image_primary' . $image->id . '">Default</label></div>'
                                                                    . '<div><a href="javascript:delete_image(\'' . $image->id . '\')" class="text-muted pull-right"><i class="icon-trash"></i></a></div>'
                                                                    . '</div>'
                                                                    . '</div>';
                                                            }
                                                        }
                                                        ?>
                                                        <div class="image" id="add-image">
                                                            <div>
                                                                <span>+</span>
                                                            </div>
                                                            <input type="hidden" id="add-image-value">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label><?php echo lang('short_description'); ?></label>
                                                    <textarea cols="30" rows="2" class="form-control" id="short_description" name="short_description" placeholder="<?php echo lang('short_description_placeholder'); ?>"><?php echo ($data) ? $data->short_description : ''; ?></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label><?php echo lang('description'); ?></label>
                                                    <textarea cols="30" rows="2" class="form-control tinymce" id="description" name="description" placeholder="<?php echo lang('description_placeholder'); ?>"><?php echo ($data) ? $data->description : ''; ?></textarea>
                                                </div>
                                                <div class="form-group" style="border-bottom : 1px solid #999">
                                                    <label><?php echo lang('table_description'); ?></label>
                                                    <table class="table table-hover" id="table_desk">
                                                        <thead>
                                                            <tr>
                                                                <th>Param</th>
                                                                <th>Value</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <!-- ISI DARI AJAX -->
                                                            <?php
                                                            if (isset($data) && isset($data->table_description)) {
                                                                foreach ($data->table_description as $tab) {
                                                                    $key_tab = $tab->row_id;
                                                                    $desk = $tab->id_deskripsi;
                                                            ?>
                                                                    <tr>
                                                                        <!-- name="package_items[<?php echo $key; ?>][product_name]" value="<?php echo $item->product_name ?>" -->
                                                                        <td style="width:200px"><?php echo ucwords($tab->name); ?></td>
                                                                        <input type="hidden" name="table_description[<?php echo $key_tab; ?>][row_id]" id="tabledeskrowid<?php echo $key_tab; ?>" class="form-control" value="<?php echo $key_tab; ?>">
                                                                        <input type="hidden" name="table_description[<?php echo $key_tab; ?>][id_deskripsi]" id="tabledescriptionid<?php echo $key_tab; ?>" class="form-control" value="<?php if ($tab->id_deskripsi) {
                                                                                                                                                                                                                                            echo $key_tab;
                                                                                                                                                                                                                                        } ?>">
                                                                        <input type="hidden" name="table_description[<?php echo $key_tab; ?>][name]" id="tabledescriptionname<?php echo $key_tab; ?>" class="form-control" value="<?php echo $tab->name; ?>">
                                                                        <td><input type="text" name="table_description[<?php echo $key_tab; ?>][value]" value="<?php echo $tab->value ?>" id="tabledescription<?php echo $key_tab; ?>" class="form-control" placeholder="Silahkan input <?php echo $tab->name ?> produk Anda" required>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- <br><a onclick="add_param()" class="btn btn-success">Add Param</a><br><br> --> <?php } ?>
                                                            <?php } else { ?>
                                                                <tr>
                                                                    <td colspan="2" style="font-style:italic">
                                                                        <center>*Silahkan Pilih
                                                                            Terlebih dahulu</center>
                                                                    </td>
                                                                </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                    <table class="table table-hover" id="param_other_table_desk">
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                    <input type="hidden" id="count_btn" value="0" />
                                                    <div id="add_param_table_desk">

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label><?php echo lang('status'); ?></label>
                                                    <input type="checkbox" name="status" value="1" data-on-text="<?php echo lang('enabled'); ?>" data-off-text="<?php echo lang('disabled'); ?>" class="switch" <?php echo ($data) ? (($data->status == 1) ? 'checked' : '') : 'checked'; ?>>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label><?php echo lang('code'); ?></label>
                                                    <input type="text" name="code" id="code" class="form-control" value="<?php echo ($data) ? $data->code : ''; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Tipe Produk</label>
                                                    <select class="form-control" name="type" id="type">
                                                        <option value="s" <?php echo ($data) ? ($data->type == 's') ? 'selected' : '' : ''; ?>>Standard</option>
                                                        <option value="p" <?php echo ($data) ? ($data->type == 'p') ? 'selected' : '' : ''; ?>>Package</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Jenis</label>
                                                    <select class="form-control" name="preorder" id="preorder">
                                                        <option value="0" <?php echo ($data) ? ($data->preorder == '0') ? 'selected' : '' : ''; ?>>Ready Stock</option>
                                                        <option value="1" <?php echo ($data) ? ($data->preorder == '1') ? 'selected' : '' : ''; ?>>Pre-Order</option>
                                                        <option value="2" <?php echo ($data) ? ($data->preorder == '2') ? 'selected' : '' : ''; ?>>Coming Soon</option>
                                                    </select>
                                                </div>
                                                <div class="form-group" id="preorder-time">
                                                    <label class="label-time"></label>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <input style="width: 60px;" id="preorder-time-id" type="number" name="preorder_time" class="form-control" value="<?php echo ($data) ? $data->preorder_time : 0; ?>">
                                                                <span class="input-group-addon">Hari</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group" id="coming-soon-time">
                                                    <label class="label-time"></label>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">Tanggal</span><input class="form-control" type="text" name="coming_soon_time" id="coming-soon-time-id" value="<?php echo ($data) ? $data->coming_soon_time : $today ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Promo</label>
                                                    <select class="form-control" name="promo" id="promo-slc">
                                                        <option value="0" <?php echo ($data) ? ($data->promo == '0') ? 'selected' : '' : ''; ?>>No</option>
                                                        <option value="1" <?php echo ($data) ? ($data->promo == '1') ? 'selected' : '' : ''; ?>>Yes</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label><?php echo lang('category'); ?></label>
                                                    <select class="bootstrap-select" onchange="get_table_deskripsi()" name="category" id="category" data-live-search="true" data-width="100%">
                                                        <option value=""></option>
                                                        <?php if ($categories) { ?>
                                                            <?php foreach ($categories->result() as $category) { ?>
                                                                <option value="<?php echo $category->id; ?>" <?php echo ($data) ? ($data->category == $category->id) ? 'selected' : '' : ''; ?>><?php echo $category->name; ?></option>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label><?php echo lang('brand'); ?></label>
                                                    <select class="bootstrap-select" name="brand" id="brand" data-live-search="true" data-width="100%">
                                                        <option value=""></option>
                                                        <?php if ($brands) { ?>
                                                            <?php foreach ($brands->result() as $brand) { ?>
                                                                <option value="<?php echo $brand->id; ?>" <?php echo ($data) ? ($data->brand == $brand->id) ? 'selected' : '' : ''; ?>><?php echo $brand->name; ?></option>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label><?php echo lang('guarantee'); ?></label>
                                                    <input type="text" name="guarantee" id="guarantee" placeholder="ex: 1 Tahun" class="form-control" value="<?php echo ($data) ? $data->guarantee : ''; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label><?php echo lang('dimension'); ?></label>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <input type="number" min="0" name="length" class="form-control" required="" value="<?php echo ($data) ? $data->length : 0; ?>">
                                                                <span class="input-group-addon">cm</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <input type="number" min="0" name="width" class="form-control" required="" value="<?php echo ($data) ? $data->width : 0; ?>">
                                                                <span class="input-group-addon">cm</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <input type="number" min="0" name="height" class="form-control" required="" value="<?php echo ($data) ? $data->height : 0; ?>">
                                                                <span class="input-group-addon">cm</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label><?php echo lang('weight'); ?></label>
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <div class="input-group">
                                                                <input type="number" min="0" name="weight" class="form-control" value="<?php echo ($data) ? $data->weight : 100; ?>">
                                                                <span class="input-group-addon">gram</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Free Ongkir</label>
                                                    <select class="form-control" name="free_ongkir" id="ongkir-slc">
                                                        <option value="0" <?php echo ($data) ? ($data->free_ongkir == '0') ? 'selected' : '' : ''; ?>>No</option>
                                                        <option value="1" <?php echo ($data) ? ($data->free_ongkir == '1') ? 'selected' : '' : ''; ?>>Yes</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane form-horizontal" id="price">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('price'); ?></label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon">Rp</span>
                                                    <input type="text" name="price" class="form-control number" value="<?php echo ($data) ? $data->price : 0; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="row p-10" style="max-height: 176px; overflow: auto;">
                                            <?php
                                            if ($prices) {
                                                foreach ($prices->result() as $price) {
                                            ?>
                                                    <div class="col-sm-4 well p-10">
                                                        <label><?php echo $price->name; ?></label>
                                                        <div class="row">
                                                            <div class="col-xs-8">
                                                                <div class="has-feedback has-feedback-left">
                                                                    <input type="text" class="form-control input-sm number" name="prices[<?php echo $price->merchant_group; ?>][price]" value="<?php echo ($price->price) ? $price->price : 0; ?>">
                                                                    <div class="form-control-feedback">
                                                                        Rp
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                                    ?>
                                        </div> -->
                                        <hr>
                                        <h4>Price Level</h4>
                                        <input type="hidden" id="price-count" value="1">
                                        <table id="price-list" class="table">
                                            <thead>
                                                <tr>
                                                    <th style="width: 80px">Min QTY</th>
                                                    <th style="width: 200px">Harga</th>
                                                    <th style="width: 50px"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if ($price_levels) {
                                                    foreach ($price_levels->result() as $price) {
                                                        $groups = $this->products->price_level_groups($price->id);
                                                ?>
                                                        <tr>
                                                            <td class="price-qty" width="10%"><input type="number" name="price_level[<?php echo $price->id; ?>][qty]" class="form-control" value="<?php echo $price->min_qty; ?>"></td>
                                                            <td width="30%">
                                                                <div class="input-group"><span class="input-group-addon">Rp</span><input type="text" name="price_level[<?php echo $price->id; ?>][price]" class="form-control number" value="<?php echo $price->price; ?>"></div>
                                                            </td>
                                                            <!-- <td>
                                                                <div class="row p-10" style="max-height: 176px; overflow: auto;">
                                                                    <?php
                                                                    if ($groups) {
                                                                        foreach ($groups->result() as $group) {
                                                                    ?>
                                                                            <div class="col-sm-6 well p-10">
                                                                                <label><?php echo $group->name; ?></label>
                                                                                <div class="row">
                                                                                    <div class="col-xs-12">
                                                                                        <div class="has-feedback has-feedback-left">
                                                                                            <input type="text" class="form-control input-sm number" name="price_level[<?php echo $price->id; ?>][group][<?php echo $group->merchant_group; ?>][price]" value="<?php echo ($group->price) ? $group->price : 0; ?>">
                                                                                            <div class="form-control-feedback">
                                                                                                Rp
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                        }
                                                                    }
                                                                            ?>
                                                                </div>
                                                            </td> -->
                                                            <td><button type="button" class="btn btn-danger btn-xs remove-price">x</button></td>
                                                        </tr>
                                                    <?php
                                                    }
                                                } else {
                                                    ?>
                                                <?php } ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th colspan="4" class="text-center"><button type="button" id="add-price" class="btn btn-success">Add Price</button></th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <div class="tab-pane" id="option">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <div class="alert alert-info">
                                                    <p>Untuk membuat variasi baru, pilih nilai variasi kemudian klik "Hasilkan"</p>
                                                </div>
                                                <table id="combination-list" class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Default</th>
                                                            <th>Variasi</th>
                                                            <th>Harga</th>
                                                            <th>Berat</th>
                                                            <th style="width: 130px"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php if ($product_options) { ?>
                                                            <?php foreach ($product_options->result() as $po) { ?>
                                                                <tr id="option-<?php echo $po->id; ?>">
                                                                    <td><input type="radio" class="form-control" name="default_option" <?php echo ($po->default == 1) ? 'checked' : ''; ?> value="<?php echo $po->id; ?>"></td>
                                                                    <td><?php echo $po->option_combination; ?></td>
                                                                    <td><input type="text" class="number form-control" name="options[<?php echo $po->id; ?>][price]" value="<?php echo $po->price; ?>"></td>
                                                                    <td><input type="text" class="number form-control" name="options[<?php echo $po->id; ?>][weight]" value="<?php echo $po->weight; ?>"></td>
                                                                    <td>
                                                                        <button type="button" class="btn btn-default btn-xs" onclick="set_image_option(<?php echo $data->id; ?>,<?php echo $po->id; ?>)"><i class="icon-image2"></i></button>
                                                                        <button type="button" class="btn btn-danger btn-xs" onclick="delete_option(<?php echo $po->id; ?>)"><i class="icon-x"></i></button>
                                                                    </td>
                                                                </tr>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div id="option-list" class="col-md-3 panel-group content-group-lg">
                                                <?php if ($option_groups) { ?>
                                                    <?php foreach ($option_groups->result() as $og) { ?>
                                                        <div class="panel panel-white">
                                                            <div class="panel-heading">
                                                                <h6 class="panel-title">
                                                                    <a data-toggle="collapse" href="#option-group-<?php echo $og->id; ?>" aria-expanded="true" class=""><?php echo $og->name; ?></a>
                                                                </h6>
                                                            </div>
                                                            <div id="option-group-<?php echo $og->id; ?>" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                <div class="panel-body">
                                                                    <?php if ($options = $this->main->gets('options', array('group' => $og->id), 'sort_order')) { ?>
                                                                        <?php foreach ($options->result() as $option) { ?>
                                                                            <div class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" name="option[<?php echo $og->id; ?>][<?php echo $option->id; ?>]" class="styled options" value="<?php echo $option->id; ?>"> <?php echo $option->value; ?>
                                                                                </label>
                                                                            </div>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                                <button type="button" id="option-generate" class="btn border-slate text-slate-800 btn-flat mt-10" style="width: 100%">Hasilkan</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane form-horizontal" id="feature">
                                        <div id="feature-form">
                                            <?php
                                            if ($features->num_rows() > 0) {
                                                foreach ($features->result() as $feature) {
                                            ?>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label"><?php echo $feature->name; ?></label>
                                                        <div class="col-md-9">
                                                            <?php
                                                            if ($feature->type == 'i') {
                                                                echo '<input type="text" name="feature[' . $feature->id . '][value]" class="form-control" value="' . $feature->value . '">';
                                                            } elseif ($feature->type == 't') {
                                                                echo '<textarea name="feature[' . $feature->id . '][value]" class="form-control">' . $feature->value . '</textarea>';
                                                            } else {
                                                                $variants = $this->main->gets('feature_variant', array('feature' => $feature->id));
                                                                if ($feature->type == 's') {
                                                                    echo '<select name="feature[' . $feature->id . '][value]" class="form-control">';
                                                                    echo '<option value=""></option>';
                                                                    foreach ($variants->result() as $variant) {
                                                                        echo '<option value="' . $variant->id . '"' . (($variant->id == $feature->value) ? 'selected' : '') . '>' . $variant->value . '</option>';
                                                                    }
                                                                    echo '</select>';
                                                                } elseif ($feature->type == 'c') {
                                                                    $values = array();
                                                                    if ($product) {
                                                                        $feature_values = $this->main->gets('product_feature', array('product' => $product, 'feature' => $feature->id));
                                                                        if ($feature_values) {
                                                                            foreach ($feature_values->result() as $value) {
                                                                                $values[] = $value->value;
                                                                            }
                                                                        }
                                                                    }
                                                                    foreach ($variants->result() as $variant) {
                                                                        echo '<div class="checkbox"><label><input name="feature[' . $feature->id . '][value][]" type="checkbox" value="' . $variant->id . '" ' . ((in_array($variant->id, $values)) ? 'checked' : '') . '> ' . $variant->value . ' </label></div>';
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                            <?php
                                                }
                                            } else {
                                                echo lang('feature_hidden_info');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="tab-pane form-horizontal" id="seo">

                                        <input type="hidden" class="form-control" id="seo_url" name="seo_url" value="<?php echo ($data) ? $data->seo_url : ''; ?>">


                                        <input type="hidden" class="form-control" name="meta_title" value="<?php echo ($data) ? $data->meta_title : ''; ?>">

                                        <input type="hidden" class="form-control" name="meta_description" value="<?php echo ($data) ? $data->meta_description : ''; ?>">




                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('meta_keyword'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" name="meta_keyword" class="tags-input" placeholder="Kata kunci yang digunakan untuk search" value="<?php echo ($data) ? $data->meta_keyword : ''; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane form-horizontal" id="package">
                                        <div id="package-lists">
                                            <?php
                                            if (isset($data) && isset($data->package_items)) {
                                                foreach ($data->package_items as $item) {
                                                    $key = $item->row_id;
                                            ?>
                                                    <div class="row" id="rowID-<?php echo $key; ?>">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">Item Name</label>
                                                                <div class="col-md-9">
                                                                    <input id="packageItem<?php echo $key; ?>" class="autocompleteTxt form-control" type="text" name="package_items[<?php echo $key; ?>][product_name]" value="<?php echo $item->product_name ?>">
                                                                    <input id="packageItemRowId<?php echo $key; ?>" class="form-control" type="hidden" name="package_items[<?php echo $key; ?>][row_id]" value="<?php echo $item->row_id ?>">
                                                                    <input id="packageItemId<?php echo $key; ?>" class="form-control" type="hidden" name="package_items[<?php echo $key; ?>][product_id]" value="<?php echo $item->product_id ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">Qty</label>
                                                                <div class="col-md-9">
                                                                    <input class="form-control" type="number" name="package_items[<?php echo $key; ?>][qty]" value="<?php echo $item->qty ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <button type="button" class="btn btn-danger btn-xs remove-item" data-id="<?php echo $key; ?>">x</button>
                                                        </div>
                                                    </div>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                        <div class="text-center"><button type="button" id="add-package-item" class="btn btn-success">Add Package Item</button></div>
                                    </div>
                                    <div class="tab-pane form-horizontal" id="promo">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Jenis</label>
                                            <div class="col-md-3">
                                                <select class="form-control" name="promo_data[type]" required="">
                                                    <option value="F" <?php if (($data ? ($data->promo_data ? $data->promo_data->type : NULL) : '') == 'F') {
                                                                            echo "selected";
                                                                        } ?>>Fixed Amount</option>
                                                    <option value="P" <?php if (($data ? ($data->promo_data ? $data->promo_data->type : NULL) : '') == 'P') {
                                                                            echo "selected";
                                                                        } ?>>Percentage</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Tipe Promo</label>
                                            <div class="col-md-3">
                                                <select class="form-control" name="promo_data[coupon_type]" required="">
                                                    <option <?php if (($data ? ($data->promo_data ? $data->promo_data->coupon_type : '') : '') == 'all') {
                                                                echo "selected";
                                                            } ?> value="all">All</option>
                                                    <option <?php if (($data ? ($data->promo_data ? $data->promo_data->coupon_type : '') : '') == 'branch') {
                                                                echo "selected";
                                                            } ?> value="branch">Branch</option>
                                                    <option <?php if (($data ? ($data->promo_data ? $data->promo_data->coupon_type : '') : '') == 'merchant') {
                                                                echo "selected";
                                                            } ?> value="merchant">Merchant</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Available</label>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control date" name="promo_data[date_start]" value="<?php echo ($data) ? ($data->promo_data ? $data->promo_data->date_start : '')  : '' ?>" placeholder="">
                                            </div>
                                            <div class="col-xs-1" style="width: 0%">
                                                <p>-</p>
                                            </div>
                                            <div class="col-md-3">

                                                <input type="text" class="form-control date" name="promo_data[date_end]" value="<?php echo ($data) ? ($data->promo_data ? $data->promo_data->date_end : '')  : '' ?>" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Discount</label>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control number" name="promo_data[discount]" value="<?php echo ($data) ? ($data->promo_data ? $data->promo_data->discount : '')  : '' ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">

                                <a class="btn btn-default" href="<?php echo site_url('products/productv2'); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <div class="btn-group dropup">
                                        <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div id="filemanager" class="modal">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">File Manager</h5>
            </div>

            <div class="modal-body">
                <iframe width="100%" height="550" frameborder="0" src="<?php echo site_url('filemanager/dialog.php?type=1&editor=false&field_id=add-image-value&relative_url=1'); ?>"></iframe>
            </div>
        </div>
    </div>
</div>
<div id="option-image" class="modal">
    <input type="hidden" name="product_option">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Pilih Gambar Untuk Variasi Ini</h5>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" onclick="save_image_option()">Simpan</button>
            </div>
        </div>
    </div>
</div>
<script>
    var groups = JSON.parse('<?php echo json_encode($this->products->price_level_groups()->result()); ?>');
</script>