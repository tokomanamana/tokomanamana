<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('product_add_heading'); ?></h2>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('products'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('product_list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <div class="panel-body">
                <!--                <div class="text-center">
                                    <img style="width: 200px" src="<?php echo site_url('../assets/backend/images/AddProduct.svg'); ?>">
                                </div>-->
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 text-center">
                        <h3>Produk Tanaka apa yang ingin Anda jual?</h3>
                        <input id="search" type="text" class="form-control input-large" placeholder="Masukkan nama produk dan tekan ENTER">
                        <p class="mt-10">Ingin menambahkan bukan produk Tanaka? <a href="<?php echo site_url('products/form'); ?>" class="btn btn-primary">JUAL PRODUK SENDIRI</a></p>
                    </div>
                </div>
                <div class="row" id="list-product"></div>
                <a id="load-more" href="javascript:load()" class="btn btn-default center-block" style="display: none">Load More</a>
            </div>
        </div>
    </div>
</div>