<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
               <!--  <?php echo $breadcrumbs; ?> -->
                <h2><?php echo ($data) ? lang('etalase_edit_heading') : lang('etalase_add_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('products/etalase'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('etalase_list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('products/etalase/save'); ?>" method="post" class="form-horizontal" id="form">
                    <input type="hidden" name="id" value="<?php echo set_value('id', ($data) ? encode($data->id) : ''); ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="tabbable">
                                
                                <div class="tab-content">
                                    <div class="tab-pane active" id="general">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="name"><?php echo lang('etalase_form_name_label'); ?></label>
                                            <div class="col-md-5">
                                                <input type="text" class="form-control" required="" name="name" value="<?php echo ($data) ? $data->name : ''; ?>" onkeyup="convertToSlug(this.value);">
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('products/etalase'); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- <div id="filemanager" class="modal" data-backdrop="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">File Manager</h5>
            </div>

            <div class="modal-body">
                <iframe  width="100%" height="550" frameborder="0" src="<?php echo base_url('filemanager/dialog.php?type=1&editor=false&field_id=image&relative_url=1'); ?>"></iframe>
            </div>
        </div>
    </div>
</div> -->