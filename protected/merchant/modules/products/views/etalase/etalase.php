<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                
                <h2><?php echo lang('etalase_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('products/etalase/form'); ?>" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span><?php echo lang('etalase_add_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <table class="table" id="table" data-url="<?php echo site_url('products/etalase/get_list'); ?>">
                <thead>
                    <tr>
                        
                        <th class="default-sort"  data-sort="asc"><?php echo lang('etalase_name_th'); ?></th>
                        <th class="default-sort"  data-sort="asc"><?php echo lang('etalase_related_product'); ?></th>
                        
                        <th class="" style="width: 15%;"><?php echo lang('actions_th'); ?></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>