<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('product_heading'); ?></h2>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <table class="table table-hover" id="table" data-url="<?php echo site_url('products/principal/get_list'); ?>">
                <thead>
                    <tr>
                        <th class="no-sort"><?php echo lang('product_image_th'); ?></th>
                        <!-- <th class="default-sort" data-sort="asc"><?php echo lang('product_code_th'); ?></th> -->
                        <th class="default-sort" data-sort="asc">Kode Produk</th>
                        <th><?php echo lang('product_name_th'); ?></th>
                        <th><?php echo lang('product_variation_th') ?></th>
                        <th><?php echo lang('product_price_th'); ?></th>
                        <th><?php echo lang('product_category_th'); ?></th>
                        <th><?php echo "Tanggal"; ?></th>
                        <th width="10%"><?php echo lang('product_quantity_th'); ?></th>
                        <!-- <th>Harga Grosir</th> -->
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>