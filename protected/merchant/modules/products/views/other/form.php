<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo ($data) ? lang('product_edit_heading') : lang('product_add_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('products'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('product_list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <form class="stepy form-horizontal " action="<?php echo site_url('products/other/save'); ?>" method="post" id="form-product">
                <input type="hidden" name="id" value="<?php echo ($data) ? encode($data->id) : ''; ?>">
                <input type="hidden" name="id_temp" id="id_temp" value="<?php echo strtotime('now') . $user->merchant; ?>">
                <fieldset>
                    <legend class="text-semibold"><?php echo lang('product_form_general_step'); ?></legend>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('product_form_category_label'); ?></label>
                        <div class="col-md-9">
                            <select class="select" id="category" name="category" required data-live-search="true" data-width="100%">
                                <option value=""><?php echo lang('product_form_category_placeholder'); ?></option>
                                <?php if ($categories) { ?>
                                    <?php foreach ($categories->result() as $category) { ?>
                                        <option value="<?php echo $category->id; ?>" <?php echo ($data) ? ($data->category == $category->id) ? 'selected' : '' : ''; ?>><?php echo $category->name; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('product_form_name_label'); ?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" required id="name" name="name" placeholder="<?php echo lang('product_form_name_placeholder'); ?>" value="<?php echo ($data) ? $data->name : ''; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('product_form_brand_label'); ?></label>
                        <div class="col-md-9">
                            <select class="select" name="brand" id="brand" data-live-search="true" data-width="100%">
                                <option value=""><?php echo lang('product_form_brand_empty'); ?></option>
                                <?php if ($brands) { ?>
                                    <?php foreach ($brands->result() as $brand) { ?>
                                        <option value="<?php echo $brand->id; ?>" <?php echo ($data) ? ($data->brand == $brand->id) ? 'selected' : '' : ''; ?>><?php echo $brand->name; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('product_form_short_description_label'); ?></label>
                        <div class="col-md-9">
                            <textarea cols="30" rows="2" required class="form-control" id="short_description" name="short_description" placeholder="<?php echo lang('product_form_short_description_placeholder'); ?>"><?php echo ($data) ? $data->short_description : ''; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('product_form_description_label'); ?></label>
                        <div class="col-md-9">
                            <textarea cols="30" rows="2" class="form-control tinymce" id="description" name="description" placeholder="<?php echo lang('product_form_description_placeholder'); ?>"><?php echo ($data) ? $data->description : ''; ?></textarea>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend class="text-semibold"><?php echo lang('product_form_feature_step'); ?></legend>
                    <div id="feature-form"></div>
                </fieldset>
                <fieldset>
                    <legend class="text-semibold"><?php echo lang('product_form_data_step'); ?></legend>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('product_form_code_label'); ?></label>
                        <div class="col-md-4">
                            <input type="text" name="code" id="code" required="" class="form-control" value="<?php echo ($data) ? $data->code : product_code($user->merchant_name, $user->merchant); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('product_form_barcode_label'); ?></label>
                        <div class="col-md-4">
                            <input type="text" name="barcode" id="barcode" class="form-control" value="<?php echo ($data) ? $data->barcode : ''; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('product_form_price_label'); ?></label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <span class="input-group-addon">Rp</span>
                                <input type="text" id="price" name="price" required="" class="form-control number" value="<?php echo ($data) ? $data->price : 0; ?>">
                            </div>
                        </div>
                    </div>
                    <!--                    <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('product_form_discount_label'); ?></label>
                                            <div class="col-md-2">
                                                <div class="input-group">
                                                    <input type="number" id="discount" name="discount" min="0" max="100" class="form-control " value="<?php echo ($data) ? $data->discount : 0; ?>">
                                                    <span class="input-group-addon">%</span>
                                                </div>
                                            </div>
                                            <label class="col-md-2 control-label">Harga Setelah Diskon</label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon">Rp</span>
                                                    <input type="text" id="price-discount" class="form-control number" disabled="" value="<?php echo ($data) ? ($data->price - ($data->price * $data->discount / 100)) : 0; ?>">
                                                </div>
                                            </div>
                                        </div>-->
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('product_form_weight_label'); ?></label>
                        <div class="col-md-2">
                            <div class="input-group">
                                <input type="number" name="weight" required="" class="form-control" min="100" value="<?php echo ($data) ? $data->weight : 0; ?>">
                                <span class="input-group-addon">gram</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('product_form_quantity_label'); ?></label>
                        <div class="col-md-4">
                            <input type="text" id="quantity" name="quantity" required="" class="form-control number" value="<?php echo ($data) ? $data->quantity : 0; ?>">
                        </div>
                    </div>
                </fieldset>
                <?php // if (!$data) { ?>
                    <fieldset>
                        <legend class="text-semibold"><?php echo lang('product_form_image_step'); ?></legend>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="myId" class="dropzone" data-dropzone='<?php echo $data_images; ?>'></div>
                            </div>
                        </div>
                    </fieldset>
                <?php // } ?>
                <button type="submit" id="submit" class="btn btn-primary stepy-finish">Simpan <i class="icon-floppy-disk position-right"></i></button>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    var rules_form = {
        name: {required: true},
//        category1: {required: true},
//        category2: {required: true},
//        category3: {required: true},
    };
</script>