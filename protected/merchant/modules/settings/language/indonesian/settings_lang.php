<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['setting_store_heading'] = 'Pengaturan Toko';
$lang['setting_profile_heading'] = 'Pengaturan Profil';
$lang['setting_shipping_heading'] = 'Pengaturan Pengiriman';

$lang['setting_store_tabs'] = 'Toko';
$lang['setting_shipping_tabs'] = 'Pengiriman';
$lang['setting_profile_tabs'] = 'Profil';

$lang['setting_store_status_label'] = 'Status';
$lang['setting_store_name_label'] = 'Nama';
$lang['setting_store_description_label'] = 'Deskripsi';
$lang['setting_store_username_label'] = 'Domain Toko';
$lang['setting_store_province_label'] = 'Provinsi';
$lang['setting_store_city_label'] = 'Kota';
$lang['setting_store_district_label'] = 'Kecamatan';
$lang['setting_store_address_label'] = 'Alamat';
$lang['setting_store_postcode_label'] = 'Kode Pos';
$lang['setting_store_telephone_label'] = 'Telepon';
$lang['setting_profile_label'] = 'Biodata';
$lang['setting_profile_name_label'] = 'Nama';
$lang['setting_profile_phone_label'] = 'Handphone';
$lang['setting_profile_email_label'] = 'Alamat Email';
$lang['setting_profile_bank_label'] = 'Akun Bank';
$lang['setting_profile_bank_account_name_label'] = 'Pemilik Rekening';
$lang['setting_profile_bank_account_number_label'] = 'No Rekening';
$lang['setting_profile_bank_name_label'] = 'Nama Bank';
$lang['setting_profile_bank_branch_label'] = 'Cabang';
$lang['setting_profile_security_label'] = 'Ubah Password';
$lang['setting_profile_password_label'] = 'Password Baru';
$lang['setting_profile_password_old_label'] = 'Password Lama';
$lang['setting_shipping_label'] = 'Jasa Pengiriman Untuk Toko Anda';
$lang['setting_form_image_profile_label'] = 'Gambar Profil';
$lang['setting_form_image_cover_label'] = 'Gambar Cover';
$lang['setting_form_fullname_label'] = 'Nama Lengkap';
$lang['setting_form_email_label'] = 'Alamat Email';
$lang['setting_form_password_label'] = 'Password';
$lang['maps'] = 'Lokasi Maps';

$lang['setting_form_password_update_help'] = 'Biarkan kosong jika tidak ingin mengganti password setting';

$lang['setting_form_name_placeholder'] = 'Masukkan nama setting';
$lang['setting_form_description_placeholder'] = 'Masukkan deskripsi dari setting ini. Deskripsi akan ditampilkan di halaman profil setting ini';

$lang['setting_store_save_success_message'] = "Pengaturan Toko berhasil disimpan.";
$lang['setting_shipping_save_success_message'] = "Pengaturan Pengiriman berhasil disimpan.";
$lang['setting_profile_save_success_message'] = "Profil berhasil disimpan.";