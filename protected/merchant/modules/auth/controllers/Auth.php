<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->lang->load('ion_auth', settings('language'));
        $this->lang->load('auth', settings('language'));
        $this->load->library('form_validation');
        $this->load->helper('url');
        //$this->load->library('recaptcha');
    }
    public function login()
    {
        if ($this->ion_auth->logged_in() || $this->aauth->is_loggedin())
            redirect();
        $back = $this->input->get('back');
        if ($this->input->is_ajax_request()) {
            $this->load->library('form_validation');
            if ($this->input->post('login_type') == 'phone') {
                $this->form_validation->set_rules('phoneNum', 'No. Handphone', 'required');
                $this->form_validation->set_rules('otp_code', 'Kode Verifikasi', 'required');
            } else {
                $this->form_validation->set_rules('identity', 'lang:email', 'required');
                $this->form_validation->set_rules('password', 'lang:password', 'required');
            }
            if ($this->form_validation->run() == true) {
                $data = $this->input->post(null, true);
                $remember = (bool) $this->input->post('remember');
                do {
                    $return = ['status' => 'error', 'message' => 'Login gagal, silahkan coba lagi'];
                    if ($data['login_type'] == 'phone') {
                        $merchant = $this->main->get('merchant_users', array('phone' => $data['phoneNum']));
                        if (!$merchant) {
                            break;
                        }
                        if ($merchant->verification_code != $data['otp_code']) {
                            break;
                        }
                        $this->ion_auth_model->set_session($merchant);
                        $this->ion_auth_model->update_last_login($merchant->id);
                        $return = ['status' => 'success', 'redirect' => site_url($back)];
                    } else {
                        if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {
                            if ($this->ion_auth->errors()) {
                                $return = array('message' => $this->ion_auth->errors(), 'status' => 'error');
                            } else {
                                $return = array('message' => '', 'status' => 'success', 'redirect' => site_url($this->input->post('back')));
                            }
                        } else if ($this->aauth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {
                            $return = array('message' => '', 'status' => 'success', 'redirect' => site_url($this->input->post('back')));
                        } else {
                            $return = array('message' => $this->aauth->print_errors(), 'status' => 'error');
                        }
                    }
                } while (0);
            } else {
                $return = array('message' => validation_errors(), 'status' => 'error');
            }
            echo json_encode($return);
        } else {
            $this->data['page'] = 'login';

            $this->template->_init('auth');
            $this->template->form();
            $this->output->set_title('Merchant Login');
            $this->data['meta_description'] = 'Login Merchant Tokomanamana';
            $this->load->js('../assets/backend/js/merchant/modules/auth/login.js');
            $this->load->view('login', $this->data);
        }
    }
    public function logout()
    {

        $this->session->sess_destroy();
        // if ($this->aauth->is_loggedin()){
        //     $this->aauth->logout();
        // } 
        // if ($this->ion_auth->logged_in()){
        //     $this->ion_auth->logout();
        // } 
        // $this->session->unset_userdata('merchant_user');
        redirect('auth/login', 'refresh');
    }
    // public function login() {
    //     if ($this->ion_auth->logged_in())
    //         redirect();
    //     $back = $this->input->get('back');
    //     if ($this->input->is_ajax_request()) {
    //         $this->load->library('form_validation');
    //         if ($this->input->post('login_type') == 'phone') {
    //             $this->form_validation->set_rules('phoneNum', 'No. Handphone', 'required');
    //             $this->form_validation->set_rules('otp_code', 'Kode Verifikasi', 'required');
    //         } else {
    //             $this->form_validation->set_rules('identity', 'lang:email', 'required');
    //             $this->form_validation->set_rules('password', 'lang:password', 'required');
    //         }
    //         if ($this->form_validation->run() == true) {
    //             $data = $this->input->post(null, true);
    //             $remember = (bool) $this->input->post('remember');
    //             do {
    //                     $return = ['status' => 'error', 'message' => 'Login gagal, silahkan coba lagi'];
    //                     if ($data['login_type'] == 'phone') {
    //                         $merchant = $this->main->get('merchant_users', array('phone' => $data['phoneNum']));
    //                         if (!$merchant) {
    //                             break;
    //                         }
    //                         if ($merchant->verification_code != $data['otp_code']) {
    //                             break;
    //                         }
    //                         $this->ion_auth_model->set_session($merchant);
    //                         $this->ion_auth_model->update_last_login($merchant->id);
    //                         $return = ['status' => 'success', 'redirect' => site_url($back)];
    //                     } else {
    //                         if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {
    //                             $user = $this->ion_auth->user()->row();
    //                             if ($this->ion_auth->in_group(array(3, 2))) {
    //                                 $return = array('message' => '', 'status' => 'success', 'redirect' => site_url($this->input->post('back')));
    //                             } else {
    //                                 $this->ion_auth->logout();
    //                                 $return = array('message' => lang('login_unsuccessful'), 'status' => 'error');
    //                             }
    //                         } else {
    //                             $return = array('message' => $this->ion_auth->errors(), 'status' => 'error');
    //                         }
    //                 }
    //             } while (0);
    //         } else {
    //             $return = array('message' => validation_errors(), 'status' => 'error');
    //         }
    //         echo json_encode($return);
    //     } else {
    //         $this->data['page'] = 'login';

    //         $this->template->_init('auth');
    //         $this->template->form();
    //         $this->output->set_title('Merchant Application');
    //         $this->load->js('../assets/backend/js/merchant/modules/auth/login.js');
    //         $this->load->view('login', $this->data);
    //     }
    // }

    // public function logout() {
    //     $this->ion_auth->logout();
    //     $this->session->unset_userdata('merchant_user');
    //     redirect('login', 'refresh');
    // }

    public function forgot_password()
    {
        if ($this->ion_auth->logged_in())
            redirect();

        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules('email', 'Alamat Email', 'required|valid_email');
            if ($this->form_validation->run() == true) {
                $email=$this->input->post('email');
                $forgotten = $this->ion_auth->forgotten_password($this->input->post('email'));
                if ($forgotten) {
                    $return = array('message' => 'Permintaan reset password telah diterima. Silahkan cek email Anda untuk langkah selanjutnya.', 'status' => 'success','redirect'=>site_url('auth/login'));
                $data=$this->main->get('merchant_users',array('email'=>$email));
                $code=$data->forgotten_password_code;
                $code;
                $this->data = [
                'site_name' => 'Tokomanamana',
                'email' => $this->input->post('email'),
                'code'=> $code
                ];
                $message = $this->load->view('email/forgot_password', $this->data, TRUE);
                $cronjob = array(
                    'from' => settings('send_email_from'),
                    'from_name' => settings('store_name'),
                    'to' => $email,
                    'subject' => 'Reset Password Akun ' . settings('store_name'),
                    'message' => $message
                );
                $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));



                } else {
                    $return = array('message' => 'Email yang Anda masukkan tidak terdaftar.', 'status' => 'error');
                }
            } else {
                $return = array('message' => 'Masukan format email yang benar', 'status' => 'error');
            }
            echo json_encode($return);
        } else {
            $this->template->_init('auth');
                        // $this->template->_init('auth');
            $this->template->form();
            $this->load->js('../assets/backend/js/merchant/modules/auth/forgot_password.js');

            $this->output->set_title('Lupa Password TokoManaMana');
            $this->load->view('forgot_password');
        }
    }

    public function reset_password($code = null)
    {
        if ($this->ion_auth->logged_in())
            redirect();
        if ($code) {
            $code_check = $this->main->get('merchant_users', ['forgotten_password_code' => $code]);
            if ($code_check) {
                $time_limit = $code_check->forgotten_password_time + 24*60*60;
                if($time_limit > time()){
                    if($this->input->is_ajax_request()){
                        $this->form_validation->set_rules('new_password', 'Password Baru', 'required|min_length[6]');
                        $this->form_validation->set_rules('new_password_confirm', 'Confirm Password Baru', 'required|min_length[6]|matches[new_password]');
                        if ($this->form_validation->run() == true) {
                            $new_password = $this->input->post('new_password');
                            $new_password_confirm = $this->input->post('new_password_confirm');
                            $hashed_password_new = password_hash($new_password, PASSWORD_BCRYPT);

                            $return = array('message' => 'Reset Password Berhasil!', 'status' => 'success','redirect'=>site_url('auth/login'));
                            $this->main->update('merchant_users',['password' => $hashed_password_new,'forgotten_password_code' => NULL],['id'=>$code_check->id]);
                        }
                        else{
                             $return = array('message' => 'Check Kembali Password anda!', 'status' => 'error');
                        }
                    echo json_encode($return);

                    }
                    else{
                        $this->template->_init('auth');
                        $this->template->form();
                        $this->load->js('../assets/backend/js/merchant/modules/auth/forgot_password.js');
                        $this->output->set_title('Reset Password TokoManaMana');
                        $this->load->view('forgot_password_complete');
                    }
                }
                else{
                    $this->template->_init('auth');
                    $this->template->form();
                    $this->output->set_title('Reset Password TokoManaMana');
                    $this->load->view('forgot_password_failed');

                }
            }
            else{

                $this->template->_init('auth');
                $this->template->form();
                $this->output->set_title('Reset Password TokoManaMana');
                $this->load->view('forgot_password_failed');

            }
        }
        else{
            $this->template->_init('auth');
            $this->template->form();
            $this->output->set_title('Reset Password TokoManaMana');
            $this->load->view('forgot_password_failed');

        }
        
    }

    public function check_reset_password_ajax(){
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $password = $this->input->post('old_password');

        if ($this->main->get('merchant_users', ['username' => $username]) || $this->main->get_bad($username)) {
            echo "false";
        } else {
            echo "true";
        }

    }

    public function register()
    {
        $this->load->library('session');
        $this->load->helper(array('captcha', 'url'));
        if ($this->ion_auth->logged_in())
            redirect();

        $this->load->helper('captcha');
 
            $vals = array(
                'word' => '',
                'pool' => '0123456789',
                'word_length' => 4,
                'img_path'   => '../assets/frontend/captcha/',
                'img_url'    => base_url('../').'assets/frontend/captcha/',
                'img_width'  => '200',
                'img_height' => 35,
                'border' => 0,
                'font_size' => 26, 
                'expiration' => 7200,
                'font_path' => FCPATH. 'assets/frontend/captcha/fonts/coolvetica_rg.ttf',
                'colors' => array(
                    'background' => array(255, 255, 255),
                    'border' => array(230, 230, 230),
                    'text' => array(0, 0, 0),
                    'grid' => array(255, 255, 255)
                )
            );
 
            // create captcha image
            $cap = create_captcha($vals);
 
            // store image html code in a variable
            $this->data['image'] = $cap['image'];
                
       
            $this->session->set_userdata('mycaptcha', $cap['word']);
            $this->data['page'] = 'register';
            $this->data['provincies'] = $this->main->gets('provincies', [], 'name asc');

            //$this->data['widget'] = $this->recaptcha->getWidget();
            //$this->data['script'] = $this->recaptcha->getScriptTag();


             $this->load->library('googlemaps');
            $config['center'] = '-6.137325741920823,106.83344066563723';
            $config['loadAsynchronously'] = TRUE;
            $config['map_height'] = '500px';
            $config['onboundschanged'] = 'if (!centreGot) {
                var mapCentre = map.getCenter();
                marker_0.setOptions({
                        position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng()) 
                });
                setLocation(mapCentre.lat(), mapCentre.lng());
            }centreGot = true;';
            $config['disableFullscreenControl'] = TRUE;
            $config['disableMapTypeControl'] = TRUE;
            $config['disableStreetViewControl'] = TRUE;
            $config['places'] = TRUE;
            $config['placesAutocompleteInputID'] = 'search-location';
            $config['placesAutocompleteBoundsMap'] = TRUE; // set results biased towards the maps viewport
            $config['placesAutocompleteOnChange'] = 'map.setCenter(this.getPlace().geometry.location); 
            marker_0.setOptions({
                        position: new google.maps.LatLng(this.getPlace().geometry.location.lat(), this.getPlace().geometry.location.lng()) 
                });
                setLocation(this.getPlace().geometry.location.lat(), this.getPlace().geometry.location.lng());';
            $this->googlemaps->initialize($config);
            $marker = array();
            $marker['draggable'] = true;
            $marker['ondragend'] = 'setLocation(event.latLng.lat(), event.latLng.lng());';
            $this->googlemaps->add_marker($marker);
            $this->data['map'] = $this->googlemaps->create_map();

            
            $this->data['bank_lists'] = $this->main->gets('rekening_bank',[],'id asc');
            $this->template->_init('auth');
            $this->template->form();
            $this->output->set_title('Daftar Penjual');
            $this->data['meta_description'] = 'Daftar menjadi penjual ditokomanamana, Tanpa ribet, cukup update stok, dan terima order disekitar lokasi anda.';
            $this->load->css('../assets/frontend_1/css/libs/bootstrap-datepicker3.min.css');
            $this->load->js('../assets/frontend_1/js/bootstrap-datepicker.min.js');
            // $this->load->js('../assets/frontend/js/sweetalert2.all.min.js');
            $this->load->js('https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js');
            // $this->load->js('http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js');
            $this->load->js(base_url('../') . 'assets/backend/js/merchant/modules/auth/register.js');
            $this->load->view('register', $this->data);
        
        
    }

    public function save_register(){
        // $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        // $this->load->library('form_validation');
        // // $this->form_validation->set_rules('username', 'lang:username', 'required|alpha_numeric|callback_check_username');
        // // $this->form_validation->set_rules('email', 'lang:email', 'required|valid_email|callback_check_email');
        // // $this->form_validation->set_rules('password', 'lang:password', 'required|min_length[6]');
        // // $this->form_validation->set_rules('confirm_password', 'lang:confirm_password', 'required|matches[password]');
        // // $this->form_validation->set_rules('owner_name', 'lang:owner_name', 'required');
        // // $this->form_validation->set_rules('owner_phone', 'lang:owner_phone', 'required|numeric');
        // // $this->form_validation->set_rules('verified_phone', 'lang:verified_phone', 'required');
        // // $this->form_validation->set_rules('owner_birthday', 'lang:owner_birthday_day_placeholder', 'required');
        // // $this->form_validation->set_rules('owner_id', 'lang:owner_id', 'required');
        // // $this->form_validation->set_rules('store_name', 'lang:store_name', 'required');
        // // $this->form_validation->set_rules('store_telephone', 'lang:store_telephone', 'required|numeric');
        // // $this->form_validation->set_rules('store_address', 'lang:store_address', 'required');
        // // $this->form_validation->set_rules('store_province', 'lang:store_province', 'required');
        // // $this->form_validation->set_rules('store_city', 'lang:store_city', 'required');
        // // $this->form_validation->set_rules('store_district', 'lang:store_district', 'required');
        // // $this->form_validation->set_rules('store_postcode', 'lang:store_postcode', 'required');
        // // $this->form_validation->set_rules('bank_name', 'lang:bank_name', 'required');
        // // $this->form_validation->set_rules('bank_branch', 'lang:bank_branch', 'required');
        // // $this->form_validation->set_rules('bank_account_number', 'lang:bank_account_number', 'required');
        // // $this->form_validation->set_rules('bank_account_name', 'lang:bank_account_name', 'required');
        // $this->form_validation->set_rules('agree', 'lang:terms', 'required', lang('terms_required'));
        // $this->form_validation->set_rules( 'captcha', 'captcha', 'trim|callback_validate_captcha|required' );

        // if ($this->form_validation->run() == true) {

            $data = $this->input->post(null, true);
            $data['shipping'] = json_encode($data['shipping']);
            if($data['bank_name'] == 'BANK BCA'){
                $data['bank_name'] = 'BCA';
            }
            do {
                $birthday = date('Y-m-d',strtotime($data['owner_birthday']));
                $token = bin2hex(random_bytes(32));
                $user = $this->ion_auth->register($data['email'], $data['password'], $data['email'] , array('username'=>$data['username'],'fullname' => $data['owner_name'], 'phone' => $data['owner_phone'],'verification_phone' => 1, 'birthday' => $birthday, 'id_number' => $data['owner_id'],'signature_img'=>$data['base64_img'],'token_confirm' => $token,'token_confirm_status' => 0), array(3));
                if (!$user) {
                    $return = array('message' => $this->ion_auth->errors(), 'status' => 'error');
                    break;
                }
                $this->main->insert('merchants', array(
                    'auth' => $user,
                    'username' => $data['username'],
                    'type' => 'merchant',
                    'name' => $data['store_name'],
                    'description' => $data['store_description'],
                    'address' => $data['store_address'],
                    'province' => $data['store_province'],
                    'city' => $data['store_city'],
                    'district' => $data['store_district'],
                    'postcode' => $data['store_postcode'],
                    'telephone' => $data['store_telephone'],
                    'lat' => $data['lat'],
                    'lng' => $data['lng'],
                    'bank_name' => $data['bank_name'],
                    'bank_branch' => $data['bank_branch'],
                    'bank_account_number' => $data['bank_account_number'],
                    'bank_account_name' => $data['bank_account_name'],
                    'shipping' => $data['shipping'],
                    'status' => 2,
                    'know_from' => $data['know_from'],
                    'sales_name' => $data['sales_name']
                ));
                
                $this->data = [
                    'site_name' => 'Tokomanamana',
                    'token' => $token,
                    'name' => $data['owner_name']
                ];
                // $this->data['name'] = $data['owner_name'];
                $message = $this->load->view('email/register_confirmation', $this->data, TRUE);
                $cronjob = array(
                    'from' => settings('send_email_from'),
                    'from_name' => settings('store_name'),
                    'to' => $data['email'],
                    'subject' => 'Konfirmasi Pendaftaran Merchant ' . settings('store_name'),
                    'message' => $message
                );
                $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));

        //                    send_mail('noreply@tokomanamana.com', $data['email'], 'Konfirmasi Pendaftaran Merchant ' . settings('store_name'), $message);
                
                // $return = array('message' => lang('register_successful'), 'status' => 'success', 'redirect' => site_url('auth/login'));
                
            } while (0);
             $message = '<div class="alert alert-success show" role="alert">
                        <strong>Selamat</strong> registrasi berhasil, segera verifikasi email anda.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>';
            $this->session->set_flashdata('email_confirm_message', $message);
             redirect('auth/login', 'refresh');

        // }
        // else{
        //         $return = array('message' => validation_errors(), 'status' => 'error');


        // }
         // echo json_encode($return);

    }

    //for testing email
    public function send_email() {
        log_message('debug','test luar');
        $emails = $this->db->where('type', 'email')->get('cron_job');
        if ($emails->num_rows() > 0) {
            foreach ($emails->result() as $email) {
                $id = $email->id;
                $email = json_decode($email->content);
                if (send_mail($email->from, $email->from_name, $email->to, $email->subject, $email->message)) {
                    // $this->main->delete('cron_job', array('id' => $id));
                    log_message('debug','test masuk');
                }
            }
        }
    }

    public function send_email_verification(){
        $this->load->library('session');
        if (!$this->ion_auth->logged_in())
            redirect();

        $merchant_user = $this->main->get('merchant_users', array('id' => $this->data['user']->id));
        if ($merchant_user->token_confirm_status==1) {
            redirect();
        }
        log_message('error',$merchant_user->token_confirm);

        $this->data = [
            'site_name' => 'Tokomanamana',
            'token' => $merchant_user->token_confirm,
            'name' => $merchant_user->fullname
        ];
        // $this->data['name'] = $data['owner_name'];
        $message = $this->load->view('email/email_confirmation', $this->data, TRUE);
        $cronjob = array(
            'from' => settings('send_email_from'),
            'from_name' => settings('store_name'),
            'to' => $merchant_user->email,
            'subject' => 'Konfirmasi Email Merchant ' . settings('store_name'),
            'message' => $message
        );
        $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
        // $message2 = '<div class="alert alert-success show" role="alert">
        //                 Segera check email anda untuk verifikasi.
        //                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        //                 <span aria-hidden="true">&times;</span>
        //                 </button>
        //             </div>';
        
        // $this->session->set_flashdata('verification_success', 'Segera check email anda untuk verifikasi.');
        sleep(3);
        $this->logout();
        
    }

    public function merchant_email_confirm($token = null){
        $this->template->_init('auth');
     
        $this->template->form();
    
         if ($token) {
            $merchant = $this->main->get('merchant_users', ['token_confirm' => $token, 'token_confirm_status' => 0]);
            if ($merchant) {

                $merchant_check = $this->main->get('merchants', ['auth' => $merchant->id]);
                if($merchant_check->status==1){
                    $this->main->update('merchant_users',['token_confirm_status' => 1],['id'=>$merchant->id]);
                    $this->data['header'] = "Verifikasi Email Berhasil!";
                    $this->data['message'] = "";
                    $this->load->view('confirm_email', $this->data);
                    
                }
                else if($merchant_check->status==2){
                    $this->main->update('merchant_users',['token_confirm_status' => 1],['id'=>$merchant->id]);
                    $this->main->update('merchants', ['status' => 1], ['auth' => $merchant->id]);


                    $this->data['name'] = $merchant->fullname;
                    $message = $this->load->view('email/activate', $this->data, TRUE);
                    $cronjob = array(
                        'from' => settings('send_email_from'),
                        'from_name' => settings('store_name'),
                        'to' => $merchant->email,
                        'subject' => 'Akun penjual ' . settings('store_name').' Anda telah aktif',
                        'message' => $message
                    );
                    // $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
                    send_mail($cronjob['from'], $cronjob['from_name'], $cronjob['to'], $cronjob['subject'], $cronjob['message']);

                    $dir= FCPATH.'../files/images/merchant/'.$merchant_check->username;
                    if (!is_dir($dir)) {
                        mkdir($dir,0777,TRUE);
                    }

                    $dir_thumb= FCPATH.'../files/image_thumbs/merchant/'.$merchant_check->username;
                    if (!is_dir($dir_thumb)) {
                        mkdir($dir_thumb,0777,TRUE);
                    }

                    //insert seo url
                    $url = 'merchant_home/view/' . $merchant_check->id;
                    $seo_url = strtolower($merchant_check->username);
                    $this->main->insert('seo_url', array('query' => $url, 'keyword' => $seo_url));


                    $this->load->library('sprint');
                    $sms = json_decode(settings('sprint_sms'), true);
                    $code = rand(1000, 9999);
                    $sms['m'] = 'Akun penjual Anda telah aktif. Silahkan download Tokomanamana Seller di Playstore / login di http://tokomanamana.com/ma/';
                    $sms['d'] = $merchant->phone;
                    $url = $sms['url'];
                    unset($sms['url']);
                    $sprint_response = $this->sprint->sms($url, $sms);



                    $this->data['header'] = "Verifikasi Email Berhasil!";
                    $this->data['message'] = "Selamat, akun anda sudah aktif! Selamat berjualan!";
                    $this->load->view('confirm_email', $this->data);
                    
                }
            }
            else{
                $this->data['header'] = "Gagal Verifikasi Email!";
                $this->data['message'] = "Verifikasi email gagal, silahkan coba lagi.";
                $this->load->view('confirm_email', $this->data);
            }

        }else{
            $this->data['header'] = "Gagal Verifikasi Email!";
            $this->data['message'] = "Verifikasi email gagal, silahkan coba lagi.";
            $this->load->view('confirm_email', $this->data);
        }

    }

    public function validate_captcha() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        // $this->load->helper(array('captcha','url'));

        // $tt = $this->input->post('captcha');
        // $rr = $this->session->userdata['mycaptcha'];
       if ($this->input->post('captcha') == $this->session->userdata['mycaptcha']) {
           //var_dump('sss');exit();
           //$this->form_validation->set_message('validate_captcha', 'Sukses');
           echo "true";
       } else {
             /*var_dump($tt);
             var_dump($rr);
            exit();*/
           // $this->form_validation->set_message('validate_captcha', 'Wrong ! Captcha code');
           echo "false";

           // $vals = array(
           //      'word' => '',
           //      'pool' => '0123456789',
           //      'word_length' => 4,
           //      'img_path'   => '../assets/frontend/captcha/',
           //      'img_url'    => base_url('../').'assets/frontend/captcha/',
           //      'img_width'  => '200',
           //      'img_height' => 35,
           //      'border' => 0,
           //      'font_size' => 26, 
           //      'expiration' => 7200,
           //      'font_path' => FCPATH. 'assets/frontend/captcha/fonts/coolvetica_rg.ttf',
           //      'colors' => array(
           //          'background' => array(255, 255, 255),
           //          'border' => array(230, 230, 230),
           //          'text' => array(0, 0, 0),
           //          'grid' => array(255, 255, 255)
           //      )
           //  );
 
           //  // create captcha image
           //  $cap = create_captcha($vals);
 
           //  // store image html code in a variable
           //  $this->data['image'] = $cap['image'];
                
       
           //  $this->session->set_userdata('mycaptcha', $cap['word']);

            // return $cap['image'];
       }
    }

    public function create_new_captcha(){
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->helper(array('captcha','url'));
         $vals = array(
                'word' => '',
                'pool' => '0123456789',
                'word_length' => 4,
                'img_path'   => '../assets/frontend/captcha/',
                'img_url'    => base_url('../').'assets/frontend/captcha/',
                'img_width'  => '200',
                'img_height' => 35,
                'border' => 0,
                'font_size' => 26, 
                'expiration' => 7200,
                'font_path' => FCPATH. 'assets/frontend/captcha/fonts/coolvetica_rg.ttf',
                'colors' => array(
                    'background' => array(255, 255, 255),
                    'border' => array(230, 230, 230),
                    'text' => array(0, 0, 0),
                    'grid' => array(255, 255, 255)
                )
            );
 
            // create captcha image
            $cap = create_captcha($vals);
 
            // store image html code in a variable
            $this->data['image'] = $cap['image'];
                
       
            $this->session->set_userdata('mycaptcha', $cap['word']);

            $return['image'] =  $this->data['image'];
            $return['new_captcha'] = $this->session->set_userdata('mycaptcha', $cap['word']);
            echo json_encode($return);   

    }

    public function check_email_ajax() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $email = $this->input->post('email');

        if ($this->main->get('merchant_users', ['email' => $email])) {
            echo "false";
        } else {
            echo "true";
        }
    }

    public function check_username_ajax()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $username = $this->input->post('username');

        if ($this->main->get('merchant_users', ['username' => $username]) || $this->main->get_bad($username)) {
            echo "false";
        } else {
            echo "true";
        }
    }

    public function check_tokoname_ajax()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $tokoname = $this->input->post('store_name');

        if ($this->main->get_bad($tokoname)) {
            echo "false";
        } else {
            echo "true";
        }
    }

    public function check_phone_ajax()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $phone = $this->input->post('owner_phone');

        if ($this->main->get('merchant_users', ['phone' => $phone])) {
            echo "false";
        } else {
            echo "true";
        }
    }


    public function check_ktp_ajax()
    {
        $ktp = $this->input->post('owner_id');
        if ($this->main->get('merchant_users', ['id_number' => $ktp])) {
            echo "false";
        } else {
            echo "true";
        }
    }

    public function check_email($email)
    {
        if ($this->main->get('merchant_users', ['email' => $email])) {
            $this->form_validation->set_message('check_email', sprintf(lang('email_registered'), $email));
            return false;
        } else {
            return true;
        }
    }

    public function check_username($username)
    {
        if ($this->main->get('merchant_users', ['username' => $username])) {
            $this->form_validation->set_message('check_username', sprintf(lang('username_registered'), $username));
            return false;
        } else {
            return true;
        }
    }

    public function check_phone()
    {
        $phone = $this->input->post('phone');
        if ($this->main->get('merchant_users', ['phone' => $phone, 'verification_phone' => 1])) {
            echo "false";
        } else {
            echo "true";
        }
    }

    public function check_otpcode_ajax() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $httpd = $this->input->post('hpotpcode');
        $httpdexp = explode("_",$httpd);
        $phone = $httpdexp[0];
        $code = $httpdexp[1];
        $checking = $this->main->get('phone_verification', array('phone' => $phone,'verification_code' => $code));
        if($checking){
            echo 'true';
        } else {
            echo 'false';
        }
    }

    /*public function check_recaptcha() {
        $recaptcha = $this->input->post('g-recaptcha-response');
        $response = $this->recaptcha->verifyResponse($recaptcha);
        if (isset($response['success']) && $response['success'] == true) {
            return true;
        } else {
            $this->form_validation->set_message('check_recaptcha', lang('capctcha_failed'));
        }
    }*/

    public function request_otp_login()
    {
        $phone = $this->input->post('phone');
        $return = ['status' => 'error'];
        if ($merchant = $this->main->get('merchant_users', array('phone' => $phone))) {
            $check_user = $this->main->get('merchant_users', array('phone' => $phone));
            $this->load->library('sprint');
            $sms = json_decode(settings('sprint_sms'), true);
            $code = rand(1000, 9999);
            $sms['m'] = 'Hai, berikut kode login TokoManaMana Anda ' . $code;
            $sms['d'] = $phone;
            $url = $sms['url'];
            unset($sms['url']);
            //$sprint_response = $this->sprint->sms($url, $sms);
            send_mail('noreply@tokomanamana.com', 'Tokomanamana', $check_user->email, 'Kode verifikasi login merchant', $sms['m']);
            $this->main->update('merchant_users', array('verification_code' => $code, 'verification_sent_time' => date('Y-m-d H:i:s')), array('id' => $merchant->id));
            $return['msg'] = 'Kode OTP berhasil dikirim ke email ' . $check_user->email;
            $return['status'] = 'success';
        }
        echo json_encode($return);
    }
    public function request_token_merchant()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $phone = $this->input->post('phone');
        $check = $this->main->get('phone_verification', array('phone' => $phone));
        $check_user = $this->main->get('merchant_users', array('phone' => $phone));
        $return = ['status' => 'error', 'message' => ''];
        $date = date('Y-m-d H:i:s');
        $this->load->library('sprint');
        $sms = json_decode(settings('sprint_sms'), true);
        $code = rand(1000, 9999);
        $sms['m'] = 'Hai, berikut kode verifikasi no hp merchant TokoManaMana Anda ' . $code;
        $sms['d'] = $phone;
        $url = $sms['url'];
        unset($sms['url']);
        //send_mail('noreply@tokomanamana.com','Tokomanamana',$check_user->email, 'Kode verifikasi login merchant', $sms['m']);
        $sprint_response = $this->sprint->sms($url, $sms);
        if ($check) {
            $this->main->update('phone_verification', array('verification_code' => $code, 'verification_sent_time' => date('Y-m-d H:i:s')), array('phone' => $phone));
        } else {
            $this->main->insert('phone_verification', array(
                'phone' => $phone,
                'verification_code' => $code,
                'verification_sent_time' => $date,
            ));
        }
        $return['status'] = 'success';
        $return['message'] = 'Kode OTP berhasil dikirim!';
        echo json_encode($return);
    }
    public function check_token_merchant()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $phone = $this->input->post('phone');
        $code = $this->input->post('code');
        $checking = $this->main->get('phone_verification', array('phone' => $phone, 'verification_code' => $code));
        if ($checking) {
            $return['status'] = 'success';
            $return['message'] = 'Verifikasi berhasil!';
        } else {
            $return['status'] = 'failed';
            $return['message'] = 'Kode OTP tidak sesuai!';
        }
        echo json_encode($return);
    }
    public function cities($province)
    {
        $cities = $this->main->gets('cities', ['province' => $province], 'name asc');
        $output = '<option value="">' . lang('store_city') . '</option>';
        if ($cities) {
            foreach ($cities->result() as $city) {
                $output .= '<option value="' . $city->id . '">' . $city->name . '</option>';
            }
        }
        echo $output;
    }

    public function districts($city)
    {
        $districts = $this->main->gets('districts', ['city' => $city], 'name asc');
        $output = '<option value="">' . lang('store_district') . '</option>';
        if ($districts) {
            foreach ($districts->result() as $district) {
                $output .= '<option value="' . $district->id . '">' . $district->name . '</option>';
            }
        }
        echo $output;
    }

    public function verification_phone()
    {
        if (!$this->ion_auth->logged_in())
            redirect();

        $merchant_user = $this->main->get('merchant_users', array('id' => $this->data['user']->id));
        if ($merchant_user->verification_phone) {
            redirect();
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('token', 'Kode Verifikasi', 'trim|required');
        if ($this->form_validation->run() == true) {
            $token = $this->input->post('token');
            if ($token == $merchant_user->verification_code) {
                $this->main->update('merchant_users', array('verification_phone' => 1, 'verification_code' => ''), array('id' => $merchant_user->id));
                $this->session->set_flashdata('verification_success', 'No. Handphone Anda berhasil diverifikasi.');
                $user = $this->ion_auth->user()->row();
                $merchant = $this->main->get('merchants', array('auth' => $user->id));
                $user->merchant_owner = $merchant->auth;
                $user->merchant_type = $merchant->type;
                $user->merchant = $merchant->id;
                $user->merchant_username = $merchant->username;
                $user->merchant_name = $merchant->name;
                $user->merchant_group = $merchant->group;
                $user->merchant_status = $merchant->status;
                $user->token_confirm = $user->token_confirm;
                $user->token_confirm_status = $user->token_confirm_status;
                $this->session->set_userdata('merchant_user', $user);
                redirect();
            }
        } else {
            $this->data['message'] = validation_errors();

            $this->data['token_time_left'] = 0;
            if (!$merchant_user->verification_code) {
                $this->request_token();
                $this->data['token_time_left'] = 120;
            } else {
                $verification_time = strtotime($merchant_user->verification_sent_time);
                if ((strtotime(date('Y-m-d H:i:s')) - $verification_time) < 120) {
                    $this->data['token_time_left'] = strtotime(date('Y-m-d H:i:s')) - $verification_time;
                }
            }
            $this->data['page'] = 'verification';

            $this->template->_init('auth');
            $this->template->form();
            $this->output->set_title('Verifikasi No. Handphone');
            $this->load->js('../assets/backend/js/merchant/modules/auth/verification.js');
            $this->load->view('verification_phone', $this->data);
        }
    }

    public function verification_signature()
    {
        if (!$this->ion_auth->logged_in())
            redirect();

        $merchant_user = $this->main->get('merchant_users', array('id' => $this->data['user']->id));
        if ($merchant_user->signature_img) {
            redirect();
        }

        //echo "siganture verification";

        $this->load->library('form_validation');
        $this->form_validation->set_rules('agree', 'lang:terms', 'required', lang('terms_required'));
        if ($this->form_validation->run() == true) {
            $this->data['message_error'] = "";
            $data = $this->input->post(null, true);
            //var_dump($data);exit();
            if ($data['base64_img'] == '') {
                $this->data['message_error'] = 'Signature Image harus diisi!';
                $this->data['page'] = 'verification';

                $this->template->_init('auth');
                $this->template->form();
                $this->output->set_title('Verifikasi Signature');
                $this->load->js('https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js');
                $this->load->js('../assets/backend/js/merchant/modules/auth/verification_signature.js');
                $this->load->view('verification_signature', $this->data);
            } else {
                $this->main->update('merchant_users', array('signature_img' => $data['base64_img']), array('id' => $merchant_user->id));
                $this->session->set_flashdata('verification_success', 'Tanda tangan tersimpan.');
                $getdata = $this->session->userdata('merchant_user');
                $getdata->signature_img = $data['base64_img'];
                redirect();
            }
        } else {
            $this->data['message'] = validation_errors();

            $this->data['token_time_left'] = 0;
            $this->data['page'] = 'verification';

            $this->template->_init('auth');
            $this->template->form();
            $this->output->set_title('Verifikasi Signature');
            $this->load->js('https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js');
            $this->load->js('../assets/backend/js/merchant/modules/auth/verification_signature.js');
            $this->load->view('verification_signature', $this->data);
        }
    }

    public function request_token()
    {
        $this->load->library('sprint');
        $sms = json_decode(settings('sprint_sms'), true);
        $code = rand(1000, 9999);
        $sms['m'] = 'Hai, berikut kode verifikasi TokoManaMana Anda ' . $code;
        $sms['d'] = $this->data['user']->phone;
        $url = $sms['url'];
        unset($sms['url']);
        $sprint_response = $this->sprint->sms($url, $sms);
        $sprint_response = explode('_', $sprint_response);
        if ($sprint_response[0] == 0) {
            $this->main->update('merchant_users', array('verification_code' => $code, 'verification_sent_time' => date('Y-m-d H:i:s')), array('id' => $this->data['user']->id));
        }
    }

    public function login_with_users() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $id = decode($data['id_merchant']);
        $back = $data['back_merchant'];
        $merchant = $this->main->get('merchant_users', array('id' => $id));
        $this->ion_auth_model->set_session($merchant);
        $this->ion_auth_model->update_last_login($merchant->id);
        $return = ['status' => 'success', 'back' => $back];
        echo json_encode($return);
    }

    public function logout_with_user() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        if($this->session->userdata('merchant_user')) {
            $this->session->sess_destroy();
            $return['back'] = $data['back'];
        } else {
            $return['back'] = $data['back'];
        }
        echo json_encode($return);
    }

    public function check_user_merchant() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        if($this->ion_auth->logged_in()) {
            $return['status'] = 'login';
        } else {
            $return['status'] = 'not_login';
        }
        echo json_encode($return);
    }
}
