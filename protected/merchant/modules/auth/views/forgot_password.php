
<style>
    .wrapper {
        position: relative;
        width: 350px;
        height: 200px;
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    body {
        background-image: url('<?php echo site_url("../files/images/bg_register.jpeg") ?>');
        background-size: cover;
        background-attachment: fixed;
    }

    .btn-success{
        background-color: #97C23C;
        border-color: #97C23C;
    }
    .btn-success:hover{
        background-color: #84a936;
        border-color: #84a936;
    }


    @media screen and (max-width: 480px) {
        .wrapper {
            width: 230px;
            height: 150px;
        }

</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<div class="content">
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <br><br>
            <div class="panel panel-body">
                <div class="login_page_wrapper">
                    <div class="md-card" id="login_card">
                        <div class="md-card-content large-padding">
                            <img src="<?php echo site_url('../assets/frontend/images/logo.png'); ?>" alt="" style="margin:10px 0; height: 50px;">
                            <hr>
                            <center><h1 class="heading_a uk-margin-medium-bottom uk-text-center">Lupa Password TokoManaMana</h1></center>
                            <div id="message"></div>
                            <form id="forgot-password" method="post">
                                <p>Silahkan masukkan email login Anda, Anda akan menerima email untuk mendapatkan link reset password.</p>                    
                                <div class="form-group">
                                    <label class="col-md-3 control-label" style="margin: 0px;padding: 10px">Email</label>
                                    <div class="col-md-9">
                                        <input type="text" id="email" class="form-control" required="" name="email" placeholder="Masukkan alamat email" aria-required="true">
                                    </div>
                                </div>
                                <br><br>
                                <div class="uk-margin-medium-top" style="text-align: right;margin-right: 10px">
                                    <button class="button btn btn-success" type="submit">Ganti Kata Sandi</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
                <div>Sudah punya akun?? <a href="<?php echo site_url('login'); ?>">Login disini!</a></div>
            </div>
        </div>
    </div>

</div>


