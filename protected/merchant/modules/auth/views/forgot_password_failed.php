
<style>
    .wrapper {
        position: relative;
        width: 350px;
        height: 200px;
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    body {
        background-image: url('<?php echo site_url("../files/images/bg_register.jpeg") ?>');
        background-size: cover;
        background-attachment: fixed;
    }


    @media screen and (max-width: 480px) {
        .wrapper {
            width: 230px;
            height: 150px;
        }

</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<div class="content">
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <br><br>
            <div class="panel panel-body">
                <div class="login_page_wrapper">
                    <div class="md-card" id="login_card">
                        <div class="md-card-content large-padding">
                            <img src="<?php echo site_url('../assets/frontend/images/logo.png'); ?>" alt="" style="margin:10px 0; height: 50px;">
                            <hr>
                            <center><h1 class="heading_a uk-margin-medium-bottom uk-text-center">Gagal Reset Password Tokomanamana</h1></center>
                            <div id="message"></div>

                                <p>Password Anda gagal direset. Silahkan hubungi kami untuk informasi lebih lanjut.</p>                    
                                
                            
                        </div>
                    </div>
                    
                </div>
                <div>Sudah punya akun?? <a href="<?php echo site_url('login'); ?>">Login disini!</a></div>
            </div>
        </div>
    </div>

</div>


