<style>
body{background-image:url('<?php echo site_url("../files/images/bg_register.jpeg") ?>');background-size:cover;background-attachment:fixed}.field-icon{float:right;margin-left:0;margin-right:8px;margin-top:-25px;position:relative;z-index:2}.box-login{border-radius:5px;padding:0;background:#fff}.responsive{width:100%;height:auto;min-height:460px}.polaroid{box-shadow:0 4px 8px 0 rgba(0,0,0,.2),0 6px 20px 0 rgba(0,0,0,.19);text-align:center;min-width:650px}@media only screen and (max-width:651px){.responsive{width:100%;height:auto;float:left;margin-bottom:30px}.right-login{width:100%;height:auto}.polaroid{min-width:0}}.register_div{color:#a5a5a5;padding-bottom:10px}.nav-tabs{padding:0;}.nav-tabs:before{content:'Login Method';}@media (max-width: 768px) {.nav-tabs > li {display: table-cell;width: 1%;}.nav-tabs>li>a{border-bottom: 1px solid #ddd !important;border-radius: 4px 4px 0 0 !important;margin-bottom: 0 !important;}}

</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<div class="content pb-20">
    <div class="col-sm-12">
        <br><br>
        <div class="row">
            <div class="col-sm-3" ></div>
            <div class="col-sm-6 box-login polaroid"> 
                <div class="col-sm-6" style="padding:0px;">
                    <img class="responsive" src="<?php echo site_url('../assets/backend/images/bg-left-login.jpeg') ?>">
                </div>
                <div class="col-sm-6 right-login" style="padding-right: 20px; padding-left: 20px;">
                    <div class="">
                        <br><br>
                        <div class="text-center">
                            <img width="120" src="<?php echo site_url("../files/images/logo.png") ?>">
                        </div><br>
                        <ul class="nav nav-tabs ">
                          
                            <li class="active"><a data-toggle="tab" href="#via_email">Masuk via email</a></li>
                   
                     
                            <li><a data-toggle="tab" href="#via_phone">Masuk via No.HP</a></li>
                        
                        </ul>
                        <div class="tab-content">
                            <div id="via_email" class="tab-pane fade in active">
                                <div class="message"></div>
                                <?php if ($this->session->flashdata('email_confirm_message')) : ?>
                                <?= $this->session->flashdata('email_confirm_message') ?>
                                <?php endif; ?>
                                <form action="<?php echo current_full_url(); ?>" method="post" id="login">
                                <input type="hidden" name="login_type" value="email">
                                <div class="form-group has-feedback has-feedback-left">
                                    <input style="padding-right: 30px" type="email" name="identity" id="identity" class="form-control" placeholder="<?php echo lang('email'); ?>" autocomplete="off" >
                                    <div class="form-control-feedback">
                                        <i class="icon-user text-muted"></i>
                                    </div>
                                </div>
                                <div class="form-group has-feedback has-feedback-left">
                                    <input style="padding-right: 30px" type="password" name="password" id="password" class="form-control" placeholder="<?php echo lang('password'); ?>" autocomplete="off" >
                                    <span toggle="#password-field" class="fa fa-eye field-icon toggle-password"></span>
                                    <div class="form-control-feedback">
                                        <i class="icon-lock2 text-muted"></i>
                                    </div>
                                </div>
                                <div class="form-group login-options">
                                    <div class="row">
                                        <div class="col-sm-5" style="text-align: left;">
                                            <label class="checkbox-inline" style="font-size:12px; color: #a5a5a5;">
                                                <input type="checkbox" class="styled" name="remember" value="1">
                                                <?php echo lang('remember'); ?>
                                            </label>
                                        </div>
                                        <div class="col-sm-7" style="color: #a5a5a5; text-align: right;">Lupa Password?? <a style="color: #a7c22a" href="<?php echo site_url('auth/forgot_password');?>">Klik disini</a></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" style="background-color:#a7c22a; color: #fff" class="btn btn-block">Masuk <!-- <i class="icon-arrow-right14 position-right"></i> --></button>
                                </div>
                                </form>
                            </div>
                            <div id="via_phone" class="tab-pane fade">
                                <div class="message"></div>
                                <form action="<?php echo current_full_url(); ?>" method="post" id="phone_login">
                                    <input type="hidden" name="login_type" value="phone">
                                    <div class="form-group has-feedback has-feedback-left">
                                        <input type="text" name="phoneNum" id="phoneNum" class="form-control" placeholder="No Handphone" autocomplete="off" >
                                        <div class="form-control-feedback">
                                            <i class="icon-phone text-muted"></i>
                                        </div>
                                    </div>
                                    <div class="form-group has-feedback has-feedback-left">
                                        <div class="row">
                                            <div claas="col-sm-12">
                                                <div class="col-sm-8">
                                                    <input type="text" name="otp_code" id="otp_code" class="form-control" placeholder="Kode OTP" autocomplete="off" >
                                                    <div class="form-control-feedback" style="left:10px;">
                                                        <i class="icon-lock text-muted"></i>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <a id="btn-token" onclick="request_otp()" href="javascript:void(0)" class="btn btn-block" style="background-color:green;color:white;"> Kirim OTP </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" style="background-color:#a7c22a; color: #fff" class="btn btn-block">Masuk</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="register_div"><center>Belum punya akun?? <a style="color: #a7c22a" href="<?php echo site_url('register');?>">Daftar disini</a></center></div>
                        
                    </div>
                </div>
            </div>
            <div class="col-sm-3" ></div>
        </div>
    </div>
</div>
