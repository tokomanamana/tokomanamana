<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo ($data) ? lang('edit_heading') : lang('add_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('warehouses'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('warehouses/save'); ?>" class="form-horizontal" method="post" id="form">
                    <input type="hidden" name="id" value="<?php echo ($data) ? encode($data->id) : ''; ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-md-6"> 
                                    <label class="col-md-3 control-label"><?php echo lang('warehouse_name'); ?></label>
                                    <div class="col-md-9">
                                    <select class="bootstrap-select" name="warehouse_id" id="warehouse_id" data-live-search="true" data-width="100%">
                                        <option value=""></option>
                                        <?php if ($warehouses) { ?>
                                            <?php foreach ($warehouses->result() as $warehouse) { ?>
                                                <option value="<?php echo $warehouse->id; ?>" <?php echo ($data) ? ($data->warehouse_id == $warehouse->id) ? 'selected' : '' : ''; ?>><?php echo $warehouse->name; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-md-6"> 
                                    <label class="col-md-3 control-label"><?php echo lang('warehouse_form_total_space_label'); ?></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" required="" name="total_space" value="<?php echo ($data) ? $data->total_space : ''; ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                            <div id="item-lists">
                            <?php
                                if (isset($data) && isset($data->warehouse_items)) {
                                    foreach ($data->warehouse_items as $item) {
                                        $key = $item->row_id;
                            ?>
                                <div class="row" id="rowID-<?php echo $key;?>">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Item Name</label>
                                            <div class="col-md-9">
                                                <input id="warehouseItem<?php echo $key;?>" class="autocompleteTxt form-control" type="text" name="warehouse_items[<?php echo $key;?>][product_name]" value="<?php echo $item->product_name?>">
                                                <input id="warehouseItemRowId<?php echo $key;?>" class="form-control" type="hidden" name="warehouse_items[<?php echo $key;?>][row_id]" value="<?php echo $item->row_id?>">
                                                <input id="warehouseItemId<?php echo $key;?>" class="form-control" type="hidden" name="warehouse_items[<?php echo $key;?>][product_id]" value="<?php echo $item->product_id?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Qty</label>
                                            <div class="col-md-9">
                                                <input class="form-control" type="number" name="warehouse_items[<?php echo $key;?>][qty]" value="<?php echo $item->qty?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="button" class="btn btn-danger btn-xs remove-item" data-id="<?php echo $key;?>">x</button>
                                    </div>
                                </div>
                            <?php
                                    }
                                }
                            ?>
                            </div>
                            <div class="text-right"><button type="button" id="add-warehouse-item" class="btn btn-success">Tambah Barang</button></div>
                            </div>
                            
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('warehouses'); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>