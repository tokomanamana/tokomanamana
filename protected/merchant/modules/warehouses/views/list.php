<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('heading'); ?></h2>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <table class="table" id="table" data-url="<?php echo $url_ajax; ?>">
                <thead>
                    <tr>
                        <th class="default-sort" data-sort="asc">Rent ID</th>
                        <th><?php echo lang('warehouse_name'); ?></th>
                        <th>Harga /m3</th>
                        <th>Tanggal Dibuat</th>
                        <th>Tanggal Sewa</th>
                        <th><?php echo lang('status'); ?></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>