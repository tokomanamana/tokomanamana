<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Stock_model extends CI_Model {

    function get_all($branch_id, $start = 0, $length, $search = '', $order = array()) {
        $this->_where_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('p.code, p.name, psp.quantity AS old_quantity, pph.quantity AS new_quantity, pph.status, pph.date_added, pph.type, pph.id, p.variation AS variation_status, pph.id_option AS variation')
                ->join('products p', 'p.id = pph.product_id', 'left')
                ->join('products_principal_stock psp', 'psp.product_id = pph.product_id AND psp.id_option = pph.id_option', 'left')
                ->where('pph.status', 1)
                ->where('pph.quantity_revisi', null)
                ->where('pph.branch_id', $branch_id);

        return $this->db->get('products_principal_history pph');
    }

    private function _get_alias_key($key) {
        switch ($key) {
            case 1: $key = 'p.code';
                break;
            case 2: $key = 'p.name';
                break;
            case 3: $key = 'p.quantity';
                break;
            case 4: $key = 'pph.status';
                break;
            case 5: $key = 'pph.date_added';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->_where_like($search);
        $this->db->join('products p', 'p.id = pph.product_id', 'left')
                ->join('products_principal_stock psp', 'psp.product_id = pph.product_id', 'left')
                ->where('pph.status', 1)
                ->where('pph.quantity_revisi', null)
                ->where('pph.branch_id', $this->data['user']->merchant);

        return $this->db->count_all_results('products_principal_history pph');
    }

    private function _where_like($search = '') {
        $columns = array('p.code', 'p.name');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $col) {
                $this->db->or_like("IFNULL($col,'')", $search);
            }
            $this->db->group_end();
        }
    }

    public function get_history($id, $branch) {
        return $this->db->query("SELECT * FROM products_principal_history WHERE id = $id AND branch_id = $branch")->row();
    }

    public function get_product_branch($branch_id, $product) {
        return $this->db->query("SELECT * FROM products_principal_stock WHERE branch_id = $branch_id AND product_id = $product")->row();
    }

    public function get_variations($id) {
        $this->db->select('o.type, o.value')
                ->join('options o', 'o.id = poc.option', 'left')
                ->where('poc.product_option', $id);
        return $this->db->get('product_option_combination poc');
    }

}