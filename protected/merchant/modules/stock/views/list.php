<style>
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
    }

    /* Firefox */
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2>Manajemen Stok Cabang</h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <!-- <a href="<?php echo site_url('settings/users/form'); ?>" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span><?php echo lang('user_add_heading'); ?></span></a> -->
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <table class="table" id="table" data-url="<?php echo site_url('stock/get_list'); ?>" data="stave-save">
                <thead>
                    <tr>
                        <th>Code</th>
                        <th >Nama Produk</th>
                        <th>Variasi</th>
                        <th >Stok Lama</th>
                        <th class="no-sort">Stok Baru</th>
                        <th>Status</th>
                        <th>Jenis</th>
                        <th class="default-sort" data-sort="desc">Tanggal</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="not_corresponding" tabindex="-1" role="dialog" aria-labelledby="not_corresponding_label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="not_corresponding_label">Form Stok Tidak Sesuai</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <form action="<?= site_url('stock/not_corresponding') ?>" method="POST" id="form_not_corresponding">
            <input type="hidden" name="id" id="id">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-md-3">Stok masuk</label>
                            <div class="col-md-9">
                                <input type="text" disabled class="form-control" required id="stock_pricipal" name="stock_pricipal">
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-md-3">Stok yang sesuai</label>
                            <div class="col-md-9">
                                <input type="number" min="1" class="form-control" required id="new_stock" name="new_stock">
                            </div>
                        </div>
                    </div>
                </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary btn-send">Kirim</button>
            </div>
            </form>
        </div>
    </div>
</div>