<?php 

    defined('BASEPATH') or exit('No direct script access allowed');

    require APPPATH . 'libraries/REST_Controller.php';

    /**
	* This is an example of a few basic user interaction methods you could use
	* all done with a hardcoded array
	*
	* @package         CodeIgniter
	* @subpackage      Rest Server
	* @category        Controller
	* @author          Phil Sturgeon, Chris Kacerguis
	* @license         MIT
	* @link            https://github.com/chriskacerguis/codeigniter-restserver
    */
    
    class Auth extends REST_Controller {
        function __construct () {
            parent::__construct();
            header('Access-Control-Allow-Origin: *');
			header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
			header('Access-Control-Allow-Headers: *');
		}
		
		public function login_post () {
			$login_type = $this->post('login_type');
			$phone = $this->post('phone');
			$otp_code = $this->post('otp_code');
			$email = $this->post('email');
			$password = $this->post('password');

			$return = array();

			if ($login_type) {
				do {
					if (($login_type == 'phone') && $phone) {
						$merchant = $this->main->get('merchant_users', array('phone' => $phone));

						if ($merchant) {
							if ($merchant->verification_code != $otp_code) {
								$return = array(
									'status' => 'failed',
									'message' => 'Kode OTP yang anda masukan salah, silahkan coba lagi' 
								);
								break;
							} else {
								$this->ion_auth_model->update_last_login($merchant->id);
								$store = $this->main->get('merchants', array('auth' => $merchant->id));
								$data = array(
									'id' => $merchant->id,
									'username' => $merchant->username,
									'email' => $merchant->email,
									'active' => $merchant->active,
									'fullname' => $merchant->fullname,
									'phone' => $merchant->phone,
									'verification_phone' => $merchant->verification_phone,
									'birthday' => $merchant->birthday,
									'merchant' => $store->id,
									'merchant_username' => $store->username,
									'profile_picture' => $store->merchant_profile_image,
									'merchant_name' => $store->name,
									'merchant_group' => $store->group,
									'merchant_status' => $store->status,
									'merchant_type' => $store->type,
									'merchant_parent' => $store->parent,
									'principal_id' => $store->principal_id,
								);

								$return = array(
									'status' => 'success',
									'message' => $data
								);
							}
						} else {
							$return = array(
								'status' => 'failed',
								'message' => 'Tidak dapat menemukan pengguna, silahkan coba lagi' 
							);
							break;
						}
					} elseif (($login_type == 'email') && $email) {
						if ($this->ion_auth->login($email, $password)) {
							$merchant = $this->data['user'];
							$data = array(
								'id' => $merchant->id,
								'username' => $merchant->username,
								'email' => $merchant->email,
								'active' => $merchant->active,
								'fullname' => $merchant->fullname,
								'phone' => $merchant->phone,
								'verification_phone' => $merchant->verification_phone,
								'birthday' => $merchant->birthday,
								'merchant' => $merchant->merchant,
								'merchant_username' => $merchant->merchant_username,
								'profile_picture' => $merchant->profile_picture,
								'merchant_name' => $merchant->merchant_name,
								'merchant_group' => $merchant->merchant_group,
								'merchant_status' => $merchant->merchant_status,
								'merchant_type' => $merchant->merchant_type,
								'merchant_parent' => $merchant->merchant_parent,
								'principal_id' => $merchant->principal_id,
							);

							$return = array(
								'status' => 'success',
								'message' => $data
							);
						} else {
							$return = array(
								'status' => 'error',
								'message' => $this->ion_auth->errors()
							);
						}
					}
				} while (0);
			} else {
				$return = array(
					'status' => 'failed', 
					'message' => 'Login gagal, silahkan coba lagi'
				);
			}

			$this->response($return);
		}

		public function forgot_password_post () {
			$email = $this->post('email');

			$return = array(
				'status' => 'failed',
				'message' => ''
			);

			if ($email) {
				$forgotten = $this->ion_auth->forgotten_password($email);

				if ($forgotten) {
					$return = array(
						'status' => 'success',
						'message' => 'Permintaan reset password telah diterima. Silahkan cek email Anda untuk langkah selanjutnya.'
					);

					$data = $this->main->get('merchant_users', array('email' => $email));
					$code = $data->forgotten_password_code;
					$this->data = array(
						'site_name' => settings('store_name'),
						'email' => $this->post('email'),
						'code' => $code
					);
					$message = $this->load->view('email/forgot_password', $this->data, TRUE);
					$cronjob = array(
						'from' => settings('send_email_from'),
						'from_name' => settings('store_name'),
						'to' => $email,
						'subject' => 'Reset Password Akun '. settings('store_name'),
						'message' => $message
					);
					$this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
				} else {
					$return = array(
						'status' => 'failed',
						'message' => 'Email yang Anda masukkan tidak terdaftar.'
					);
				}
			} else {
				$return = array(
					'status' => 'failed',
					'message' => 'Email yang Anda masukkan tidak terdaftar.'
				);
			}

			$this->response($return);
		}

		/*public function reset_password_post () {
			$code = $this->post('code');
			$return = array();

			if ($code) {
				$code_check = $this->main->get('merchant_users', array('forgotten_password_code' => $code));

				if ($code_check) {
					$new_password = $this->post('new_password');
					$new_password_confirmation = $this->post('new_password_confirmation');
					$hashed_password = password_hash($new_password, PASSWORD_BCRYPT);

					if ($new_password == $new_password_confirmation) {
						$return = array(
							'status' => 'success',
							'message' => 'Reset password berhasil!'
						);

						$this->main->update('merchant_users', array('password' => $hashed_password, 'forgotten_password_code' => NULL), array('id' => $code_check->id));
					} else {
						$return = array(
							'status' => 'failed',
							'message' => 'Check kembali password anda!'
						);
					}
				} else {
					$return = array(
						'status' => 'failed',
						'message' => 'Anda belum pernah mengirimkan permintaan untuk merubah password sebelumnya, harap pastikan kembali!'
					);
				}
			}

			$this->response($return);
		}*/

		public function register_init_get () {
			$this->load->helper(array('captcha', 'url'));

			$capctha_data = array(
				'word' => '',
				'pool' => '01233456789',
				'word_length' => 4,
				'img_path' => '../assets/frontend/captcha/',
				'img_url' => base_url('../') . 'assets/frontend/captcha/',
				'img_width' => '200',
				'img_height' => 35,
				'border' => 0,
				'font_size' => 26,
				'expiration' => 7200,
				'font_path' => FCPATH . 'assets/frontend/captcha/fonts/coolvetica_rg.ttf',
				'colors' => array(
					'background' => array(255, 255, 255),
					'border' => array(230, 230, 230),
					'text' => array(0, 0, 0),
					'grid' => array(255, 255, 255)
				)
			);

			$capctha = create_captcha($capctha_data);

			$this->response(array(
					'status' => 'success',
					'message' => array(
						'provincies' => $this->get_provices(),
						'bank' => $this->get_bank(),
						'captcha' => array(
							'image' => $capctha['image'],
							'word' => $capctha['word']
						),
						'term_condition' => settings('term_merchant_register')
					)
				)
			);
		}

		private function get_bank () {
			$get = $this->main->gets('rekening_bank', array(), 'id ASC');
			
			if ($get->num_rows() > 0) {
				return $get->result();
			} else {
				return FALSE;
			}
		}

		private function get_provices () {
			$get = $this->main->gets('provincies', array(), 'name ASC');
			
			if ($get->num_rows() > 0) {
				return $get->result();
			} else {
				return FALSE;
			}
		}

		public function check_username_post () {
			$username = $this->post('username');
			$return = array();
			$get = $this->main->get('merchant_users', array('username' => $username));
			if ($get) {
				$return = array(
					'status' => 'failed',
					'message' => 'Maaf, username ini sudah terdaftar atau tidak sesuai!'
				);
			} else {
				if (! $this->main->get_bad($username)) {
					$return = array('status' => 'success', 'message' => '');
				} else {
					$return = array(
						'status' => 'failed',
						'message' => 'Maaf, username ini tidak sesuai!'
					);
				}
			}

			$this->response($return);
		}

		public function check_email_post () {
			$email = $this->post('email');
			$return = array();
			$chek = $this->main->get('merchant_users', array('email' => $email));

			if ($chek) {
				$return = array(
					'status' => 'failed',
					'message' => 'Maaf, email ini sudah terdaftar!'
				);
			} else {
				$return = array(
					'status' => 'success',
					'message' => ''
				);
			}

			$this->response($return);
		}

		public function check_phone_post () {
			$phone = $this->post('owner_phone');
			$return = array();
			$get = $this->main->get('merchant_users', array('phone' => $phone));

			if ($get) {
				$return = array(
					'status' => 'failed',
					'message' => 'Maaf, nomor ini sudah terdaftar!'
				);
			} else {
				$return = $this->check_verified_phone($phone);
			}

			$this->response($return);
		}

		private function check_verified_phone ($phone) {
			$return = array();
			$get = $this->main->get('merchant_users', array('phone' => $phone, 'verification_phone' => 1));

			if ($get) {
				$return = array(
					'status' => 'failed',
					'message' => 'No Hp ini sudah pernah diverifikasi'
				);
			} else {
				$return = array(
					'status' => 'success',
					'message' => ''
				);
			}

			return $return;
		}

		public function request_token_post () {
			$this->load->library('sprint');

			$phone = $this->post('owner_phone');
			$get = $this->main->get('phone_verification', array('phone' => $phone));
			$get_merchant = $this->main->get('merchant_users', array('phone' => $phone));
			$return = array('status' => 'failed', 'message' => '');
			$sms = json_decode(settings('sprint_sms', TRUE));
			$code = rand(1000, 9999);
			$sms->m = 'Hai, berikut kode verifikasi no hp merchant TokoManaMana Anda ' . $code;
			$sms->d = $phone;
			$url = $sms->url;

			unset($sms->url);

			$sms_response = $this->sprint->sms($url, $sms);

			if ($get) {
				$this->main->update('phone_verification', array('verification_code' => $code, 'verification_sent_time' => date('Y-m-d H:i:s')), array('phone' => $phone));
				$return = array(
					'status' => 'success',
					'message' => 'Kode OTP berhasil dikirim!'
				);
			} else {
				$data = array(
					'phone' => $phone,
					'verification_code' => $code,
					'verification_sent_time' => date('Y-m-d H:i:s')
				);

				$this->main->insert('phone_verification', $data);
				$return = array(
					'status' => 'success',
					'message' => 'Kode OTP berhasil dikirim!'
				);
			}

			$this->response($return);
		}

		public function check_token_post () {
			$phone = $this->post('owner_phone');
			$otp_code = $this->post('otp_code');
			$return = array();
			$get = $this->main->get('phone_verification', array('phone' => $phone, 'verification_code' => $otp_code));

			if ($get) {
				$return = array(
					'status' => 'success',
					'message' => 'Verifikasi berhasil'
				);
			} else {
				$return = array(
					'status' => 'failed',
					'message' => 'Kode OTP tidak sesuai'
				);
			}

			$this->response($return);
		}

		public function check_store_name_post () {
			$store_name = $this->post('store_name');

			if ($this->main->get_bad($store_name)) {
				$this->response(array('status' => 'failed', 'message' => 'Maaf, nama toko ini tidak sesuai!'));
			} else {
				$this->response(array('status' => 'success', 'message' => ''));
			}
		}

		public function cities_get () {
			$cities = $this->main->gets('cities', array('province' => $this->get('store_province')), 'name ASC');

			if ($cities->num_rows() > 0) {
				$this->response(array('status' => 'success', 'message' => $cities->result()));
			}
		}

		public function districts_get () {
			$districts = $this->main->gets('districts', array('city' => $this->get('store_city')), 'name ASC');

			if ($districts->num_rows() > 0) {
				$this->response(array('status' => 'success', 'message' => $districts->result()));
			}
		}

		public function create_new_captcha_get () {
			$this->load->helper(array('captcha', 'url'));

			$capctha_data = array(
				'word' => '',
				'pool' => '01233456789',
				'word_length' => 4,
				'img_path' => '../assets/frontend/captcha/',
				'img_url' => base_url('../') . 'assets/frontend/captcha/',
				'img_width' => '200',
				'img_height' => 35,
				'border' => 0,
				'font_size' => 26,
				'expiration' => 7200,
				'font_path' => FCPATH . 'assets/frontend/captcha/fonts/coolvetica_rg.ttf',
				'colors' => array(
					'background' => array(255, 255, 255),
					'border' => array(230, 230, 230),
					'text' => array(0, 0, 0),
					'grid' => array(255, 255, 255)
				)
			);

			$capctha = create_captcha($capctha_data);

			$this->response(array('status' => 'success', 'message' => array('image' => $capctha['image'], 'word' => $capctha['word'])));
		}

		public function register_post () {
			$shipping = json_encode($this->post('shipping'));

			if ($this->post('bank_name') == 'BANK BCA') {
				$bank_name = 'BCA';
			} else {
				$bank_name = $this->post('bank_name');
			}

			$return = array(
				'status' => 'failed',
				'message' => ''
			);

			do {
				$birthday = date('Y-m-d', strtotime($this->post('owner_birthday')));
				$token = bin2hex(random_bytes(32));
				$user = $this->ion_auth->register(
					$this->post('email'), 
					$this->post('password'), 
					$this->post('email'), 
					array(
						'username' => $this->post('username'), 
						'fullname' => $this->post('owner_name'), 
						'phone' => $this->post('owner_phone'), 
						'verification_phone' => 1, 
						'birthday' => $this->post('birthday'),
						'id_number' => $this->post('owner_id'),
						'signature_img' => $this->post('base64_img'),
						'token_confirm' => $token,
						'token_confirm_status' => 0
					),
					array(3)
				);

				if (!$user) {
					$return = array(
						'status' => 'failed',
						'message' => $this->ion_auth->errors()
					);
					break;
				} else {
					$data = array(
						'auth' => $user,
						'username' => $this->post('username'),
						'type' => 'merchant',
						'name' => $this->post('store_name'),
						'description' => $this->post('store_description'),
						'address' => $this->post('store_address'),
						'province' => $this->post('store_province'),
						'city' => $this->post('store_city'),
						'district' => $this->post('store_district'),
						'postcode' => $this->post('store_postcode'),
						'telephone' => $this->post('store_telephone'),
						'lat' => $this->post('lat'),
						'lng' => $this->post('lng'),
						'bank_name' => $bank_name,
						'bank_branch' => $this->post('bank_branch'),
						'bank_account_number' => $this->post('bank_account_number'),
						'bank_account_name' => $this->post('bank_account_name'),
						'shipping' => $this->post('shipping'),
						'status' => 2,
						'know_from' => $this->post('know_from'),
						'sales_name' => $this->post('sales_name')
					);
					$this->main->insert('merchants', $data);
					$this->data = array(
						'site_name' => settings('store_name'),
						'token' => $token,
						'name' => $this->post('owner_name')
					);
					$message = $this->load->view('email/register_confirmation', $this->data, TRUE);
					$cronjob = array(
						'from' => settings('send_email_from'),
						'from_name' => settings('store_name'),
						'to' => $this->post('email'),
						'subject' => 'Konfirmasi Pendaftaran Merchant '. settings('store_name'),
						'message' => $message
					);
					$this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
					$return = array(
						'status' => 'success',
						'message' => 'Selamat, registrasi berhasil, segera verifikasi email anda.'
					);
				}
			} while (0);

			$this->response($return);
		}

		public function request_otp_login_post () {
			$phone = $this->post('phone');
			$return = array(
				'status' => 'failed',
				'message' => ''
			);

			$merchant = $this->main->get('merchant_users', array('phone' => $phone));

			if ($merchant) {
				$this->load->library('sprint');
				$sms = json_decode(settings('sprint_sms'), TRUE);
				$code = rand(1000, 9999);
				$sms['m'] = 'Hai, berikut kode login TokoManaMana Anda '. $code;
				$sms['d'] = $phone;
				$url = $sms['url'];
				unset($sms['url']);

				send_mail(settings('send_email_from'), settings('store_name'), $merchant->email, 'Kode verifikasi login merchant', $sms['m']);
				
				$this->main->update('merchant_users', array('verification_code' => $code, 'verification_sent_time' => date('Y-m-d H:i:s')), array('id' => $merchant->id));
				$return = array(
					'status' => 'success',
					'message' => 'Kode OTP berhasil dikirim ke email '. $merchant->email
				);
			} else {
				$return = array(
					'status' => 'failed',
					'message' => 'Tidak dapat menemukan pengguna, silahkan coba lagi!'
				);
			}

			$this->response($return);
		}
    }

?>