<?php 

    defined('BASEPATH') or exit('No direct script access allowed');

    require APPPATH . 'libraries/REST_Controller.php';

    /**
	* This is an example of a few basic user interaction methods you could use
	* all done with a hardcoded array
	*
	* @package         CodeIgniter
	* @subpackage      Rest Server
	* @category        Controller
	* @author          Phil Sturgeon, Chris Kacerguis
	* @license         MIT
	* @link            https://github.com/chriskacerguis/codeigniter-restserver
    */
    
    class Config extends REST_Controller {
        function __construct () {
            parent::__construct();
            header('Access-Control-Allow-Origin: *');
			header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
			header('Access-Control-Allow-Headers: *');

			$this->load->helper(array('form', 'url'));
		}
		
		public function banner_post () {
			$id = $this->input->post('id');
			$return = array();

			if ($id && $this->main->get('merchants', array('id' => $id))) {
				$config['upload_path'] = FCPATH. '../files/images/banner_merchant';
				$config['allowed_types'] = 'jpg|jpeg|png';
				$config['file_name'] = 'banner_merchant_'. $id;
				$config['overwrite'] = true;
				$config['max_size'] = 2048; //2Mb

				$this->load->library('upload', $config);
				
				$old_filename = $_FILES['banner_file']['name'];
				$extension = substr($old_filename, strpos($old_filename, ".") + 1);
				$file_name = 'banner_merchant_'. $id .'.'. $extension;

				if (! $this->upload->do_upload('banner_file')) {
					$return = array('status' => 'failed', 'message' => $this->upload->display_errors());
				} else {
					$this->main->update('merchants', array('banner_merchant' => 'banner_merchant/'. $file_name), array('id' => $id));
					$return = array('status' => 'success', 'message' => 'Banner berhasil diubah');
				}
			} else {
				$return = array('status' => 'failed', 'message' => 'Tidak dapat menemukan merchant');
			}

			$this->response($return);
		}

		public function profile_post () {
			$id = $this->input->post('id');
			$return = array();

			if ($id && $this->main->get('merchants', array('id' => $this->input->post('id')))) {
				$config['upload_path'] = FCPATH. '../files/images/profilepicture_merchant';
				$config['allowed_types'] = 'jpg|jpeg|png';
				$config['file_name'] = 'profilepicture_'. $id;
				$config['overwrite'] = true;
				$config['max_size'] = 1024; // 1Mb

				$this->load->library('upload', $config);

				$old_filename = $_FILES['profile_file']['name'];
				$extension = substr($old_filename, strpos($old_filename, '.') + 1);
				$file_name = 'profilepicture_'. $id .'.'. $extension;

				if (! $this->upload->do_upload('profile_file')) {
					$return = array('status' => 'failed', 'message' => $this->upload->display_errors());
				} else {
					$this->main->update('merchants', array('merchant_profile_image' => 'profilepicture_merchant/'. $file_name), array('id' => $id));
					$return = array('status' => 'success', 'message' => 'Foto profil berhasil diubah');
				}
			} else {
				$return = array('status' => 'failed', 'message' => 'Tidak dapat menemukan merchant');
			}

			$this->response($return);
		}
    }

?>