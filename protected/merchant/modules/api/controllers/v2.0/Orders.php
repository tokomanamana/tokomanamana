<?php 

    defined('BASEPATH') or exit('No direct script access allowed');

    require APPPATH . 'libraries/REST_Controller.php';

    /**
	* This is an example of a few basic user interaction methods you could use
	* all done with a hardcoded array
	*
	* @package         CodeIgniter
	* @subpackage      Rest Server
	* @category        Controller
	* @author          Phil Sturgeon, Chris Kacerguis
	* @license         MIT
	* @link            https://github.com/chriskacerguis/codeigniter-restserver
    */
    
    class Orders extends REST_Controller {
        function __construct () {
            parent::__construct();
            header('Access-Control-Allow-Origin: *');
			header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
            header('Access-Control-Allow-Headers: *');

            $this->load->model('orders_model', 'orders');
        }

        public function index_post () {
            $id = $this->input->post('id');
            $status = $this->input->post('status') ? $this->input->post('status') : 1;
            $start = $this->input->post('start') ? $this->input->post('start') : 0;
            $length = $this->input->post('length') ? $this->input->post('length') : 10;

            $return = array();
            $not_in = array(settings('order_new_status'), settings('order_payment_confirmed_status'));

            if ($id && $merchant = $this->main->get('merchants', array('id' => $id))) {
                if (! in_array($status, $not_in)) {
                    $datas = $this->orders->get_all($id, $status, $start, $length);
                    $invoices = array();

                    if ($datas) {
                        foreach ($datas as $key => $value) {
                            $invoices[] = array(
                                'shipping_name' => $value->shipping_name,
                                'invoice' => $value->code,
                                'ammount' => $value->total,
                                'total_product' => $this->main->count('order_product', array('invoice' => $value->id)),
                                'product' => array()
                            );

                            $product = $this->main->get('order_product', array('invoice' => $value->id));
                            
                            if ($product) {
                                $product_option = array();
                                if ($product->options) {
                                    $options = json_decode($product->options, TRUE);
                                    foreach ($options as $option) {
                                        $data_option = json_decode($option['data_option'], TRUE);
                                        $product_image = get_image($option['image']);
    
                                        foreach ($data_option as $value) {
                                            $set = array_values($value);
                                            array_push($product_option, implode(' ', $set));
                                        }
                                    }
                                } else {
                                    $product_image = $this->main->get('product_image', array('product' => $product->product, 'primary' => '1'));
                                    $product_image = get_image($product_image->image);
                                }
    
                                $invoices[$key]['product'] = array(
                                    'name' => $product->name,
                                    'option' => $product_option,
                                    'total' => $product->total,
                                    'quantity' => $product->quantity,
                                    'image' => $product_image
                                );
                            } else {
                                $invoices[$key]['product'] = array();
                            }
                        }
                    }

                    $return = array('status' => 'success', 'message' => $invoices);
                } else {
                    $return = array('status' => 'failed', 'message' => 'Invalid order status');
                }
            } else {
                $return = array('status' => 'failed', 'message' => 'Tidak dapat menemukan pengguna');
            }


            $this->response($return);
        }

        public function view_post () {
            $id = $this->post('id');
            $merchant = $this->post('merchant');

            $order = $this->orders->get($id, $merchant);
            $products = $this->orders->get_products($id);

            $return = array('status' => 'failed', 'message' => '');

            if ($order) {
                if ($order->shipping_courier == 'grab') {
                    $shipping =  'Grab Express';
                } else if (($order->shipping_courier != 'pickup') && ($order->shipping_courier != 'Pickup')) {
                    $courier = explode('-', $order->shipping_courier);
                    $shipping = strtoupper($courier[0]) . ' ' . $courier[1];
                } else if (($order->shipping_courier == 'pickup') || ($order->shipping_courier == 'Pickup')){
                    $shipping = 'Ambil Sendiri';
                } else {
                    $shipping = $order->shipping_courier;
                }

                $product_detail = array();
                $product_option = array();
                if ($products) {
                    foreach ($products as $key => $product) {
                        $product_detail[] = array(
                            'name' => $product->name,
                            'quantity' => $product->quantity,
                            'weight' => $product->weight,
                            'price' => $product->price,
                            'total' => $product->total
                        );

                        if ($product->options) {
                            $options = json_decode($product->options, TRUE);
                            foreach ($options as $option) {
                                $data_option = json_decode($option['data_option'], TRUE);
                                $product_image = get_image($option['image']);

                                foreach ($data_option as $value) {
                                    $set = array_values($value);
                                    array_push($product_option, implode(' ', $set));
                                }
                            }
                        }

                        $product_detail[$key]['option'] = $product_option;
                        $product_detail[$key]['image'] = $product_image;
                    }
                }

                $return = array(
                    'status' => 'success',
                    'message' => array(
                        'customer' => $order->customer_name,
                        'invoice' => $order->code,
                        'payment_method' => $order->payment_method,
                        'courier' => $shipping,
                        'shipping' => array(
                            'shipping_name' => $order->shipping_name,
                            'shipping_address' => $order->shipping_address,
                            'shipping_province' => $order->shipping_province,
                            'shipping_city' => $order->shipping_city,
                            'shipping_district' => $order->shipping_district,
                            'shipping_postcode' => $order->shipping_postcode,
                            'shipping_phone' => $order->shipping_phone
                        ),
                        'products' => $product_detail
                    )
                );
            } else {
                $return = array(
                    'status' => 'failed',
                    'message' => 'Tidak dapat menemukan pesanan'
                );
            }

            $this->response($return);
        }
    }
?>