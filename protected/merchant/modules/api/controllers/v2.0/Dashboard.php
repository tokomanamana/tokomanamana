<?php 

    defined('BASEPATH') or exit('No direct script access allowed');

    require APPPATH . 'libraries/REST_Controller.php';

    /**
	* This is an example of a few basic user interaction methods you could use
	* all done with a hardcoded array
	*
	* @package         CodeIgniter
	* @subpackage      Rest Server
	* @category        Controller
	* @author          Phil Sturgeon, Chris Kacerguis
	* @license         MIT
	* @link            https://github.com/chriskacerguis/codeigniter-restserver
    */
    
    class Dashboard extends REST_Controller {
        function __construct () {
            parent::__construct();
            header('Access-Control-Allow-Origin: *');
			header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
            header('Access-Control-Allow-Headers: *');
            
            $this->load->model('dashboard_model', 'dashboard');
        }
        
        public function index_post () {
            $return = array();

            if ($merchant = $this->main->get('merchants', array('id' => $this->input->post('id')))) {
                if ($merchant->type == 'merchant') {
                    $summaries = $this->dashboard->sales_summary($merchant->id);
                    $bestsellers = $this->dashboard->bestsellers($merchant->id);
                    $mostviewed = $this->dashboard->mostviewed($merchant->id);
                } else {
                    $summaries = $this->dashboard->sales_summary_principal($merchant->id);
                    $bestsellers = $this->dashboard->bestsellers_principal($merchant->id);
                    $mostviewed = $this->dashboard->mostviewed_principal($merchant->id);
                }

                $transaction = 0;
                $sales = 0;
                $today = date('Y-m-d');
                $past_date = date('Y-m-d', strtotime('-6 days'));
                $future_date = $today;
                
                if ($summaries) {
                    foreach ($summaries as $sumary) {
                        $date_sumary = date('Y-m-d', strtotime($sumary->date_summary));
    
                        if ($past_date <= $today && $future_date >= $today) {
                            $transaction += $sumary->transaction;
                            $sales += $sumary->sales;
                        }
                    }
                }

                // $last_orders = $this->dashboard->last_orders($merchant->id);
                $new_orders = $this->dashboard->total_order_by_status(settings('order_payment_received_status'), $merchant->id);
                $processed_orders = $this->dashboard->total_order_by_status(settings('order_process_status'), $merchant->id);
                $canceled_orders = $this->dashboard->total_order_by_status(settings('order_cancel_status'), $merchant->id);
                $rejected_orders = $this->dashboard->total_order_by_status(settings('order_rejected_status'), $merchant->id);
                $shipped_orders = $this->dashboard->total_order_by_status(settings('order_shipped_status'), $merchant->id);
                $finished_orders = $this->dashboard->total_order_by_status(settings('order_finish_status'), $merchant->id);
                $balance = $merchant->cash_balance;
                $last_balances = $this->dashboard->last_balances($merchant->id);

                $return = array(
                    'status' => 'success',
                    'message' => array(
                        'transaction' => $transaction,
                        'sales' => $sales,
                        // 'last_order' => $last_orders,
                        'new_orders' => $new_orders,
                        'processed_orders' => $processed_orders,
                        'canceled_orders' => $canceled_orders,
                        'rejected_orders' => $rejected_orders,
                        'shipped_orders' => $shipped_orders,
                        'finish_orders' => $finished_orders,
                        'balance' => $balance,
                        // 'last_balance' => $last_balances,
                        'best_seller' => $bestsellers,
                        'most_viewed' => $mostviewed
                    )
                );
            } else {
                $return = array('status' => 'failed', 'message' => 'Tidak dapat menemukan pengguna');
            }

            $this->response($return);
        }

        public function sales_summary_post () {
            $return = array();
            $data = array('finish' => '0', 'progress' => '0', 'reject' => '0');
            $month = date('m');
            $year = date('Y');

            if ($merchant = $this->main->get('merchants', array('id' => $this->input->post('id')))) {
                if ($merchant->type == 'merchant') {
                    $datas = $this->dashboard->sales_summary($merchant->id);
                } elseif ($merchant->type == 'principal_branch') {
                    $datas = $this->dashboard->sales_summary_principal($merchant->id);
                }

                $data = array();
                if ($datas) {
                    foreach ($datas as $key => $value) {
                        $data[] = $value;
                    }
                }

                $return = array('status' => 'success', 'message' => $data);
            } else {
                $return = array('status' => 'failed', 'message' => 'Tidak dapat menemukan pengguna');
            }

            $this->response($return);
        }
    }

?>