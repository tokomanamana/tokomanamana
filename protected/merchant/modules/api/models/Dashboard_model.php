<?php

    defined('BASEPATH') or exit('No direct script access allowed');

    class Dashboard_model extends CI_Model {
        public function  sales_summary ($id) {
            $status = settings('order_finish_status');
            $query = $this->db->select('IFNULL(COUNT(order_product.id), 0) AS transaction,
                                        IFNULL(SUM(order_invoice.total), 0) AS sales,
                                        DAY(order_invoice.date_modified) AS date,
                                        DATE(order_invoice.date_modified) AS date_summary')
                            ->join('order_invoice', 'order_product.invoice = order_invoice.id', 'LEFT')
                            ->join('products', 'order_product.product = products.id', 'LEFT')
                            ->where(array(
                                            'order_invoice.order_status' => $status, 
                                            'YEAR(order_invoice.date_added)' => date('Y'), 
                                            'MONTH(order_invoice.date_added)' => date('m')
                                        ))
                            ->where('products.store_id', $id)
                            ->where('products.store_type', 'merchant')
                            ->group_by('DAY(order_invoice.date_added)')
                            ->order_by('DAY(order_invoice.date_added)', 'ASC')
                            ->get('order_product');
            return ($query->num_rows() > 0) ? $query->result() : FALSE;
        }

        public function sales_summary_principal ($id) {
            $status = settings('order_finish_status');
            $query = $this->db->select('IFNULL(COUNT(order_product.id), 0) AS transaction,
                                    IFNULL(SUM(order_invoice.total), 0) AS sales,
                                    DAY(order_invoice.date_modified) AS date,
                                    DATE(order_invoice.date_modified) AS date_summary')
                            ->join('order_invoice', 'order_product.invoice = order_invoice.id', 'LEFT')
                            ->join('products', 'order_product.product = products.id', 'LEFT')
                            ->join('products_principal_stock', 'products.id = products_principal_stock.product_id')
                            ->where(array(
                                            'order_invoice.order_status' => $status, 
                                            'YEAR(order_invoice.date_added)' => date('Y'), 
                                            'MONTH(order_invoice.date_added)' => date('m')
                                        ))
                            ->where('products_principal_stock.branch_id', $id)
                            ->where('products.store_type', 'principal')
                            ->group_by('DAY(order_invoice.date_added)')
                            ->order_by('DAY(order_invoice.date_added)', 'ASC')
                            ->get('order_product');
            return ($query->num_rows() > 0) ? $query->result() : FALSE;
        }

        public function bestsellers ($id) {
            $status = settings('order_finish_status');
            $query = $this->db->select('order_product.name, SUM(order_product.quantity) AS quantity, order_product.price, order_product.discount, products.price, 
                                        categories.name AS category_name, products.id, etalase.name AS etalase_name')
                            ->join('order_invoice', 'order_product.invoice = order_invoice.id', 'LEFT')
                            ->join('products', 'order_product.product = products.id', 'LEFT')
                            ->join('categories', 'categories.id = products.category', 'LEFT')
                            ->join('etalase', 'etalase.id = products.etalase', 'LEFT')
                            ->limit(5)
                            ->group_by('order_product.product')
                            ->where('order_invoice.order_status', $status)
                            ->where('products.store_id', $id)
                            ->where('products.store_type', 'merchant')
                            ->order_by('quantity', 'DESC')
                            ->get('order_product');
            return $query->num_rows();
        }

        public function bestsellers_principal ($id) {
            $status = settings('order_finish_status');
            $query = $this->db->select('order_product.name, SUM(order_product.quantity) AS quantity, order_product.price, order_product.discount, products.price, 
                                        categories.name AS category_name, products.id, etalase.name AS etalase_name')
                            ->join('order_invoice', 'order_product.invoice = order_invoice.id', 'LEFT')
                            ->join('products', 'order_product.product = products.id', 'LEFT')
                            ->join('categories', 'categories.id = products.category', 'LEFT')
                            ->join('etalase', 'etalase.id = products.etalase', 'LEFT')
                            ->join('products_principal_stock', 'products.id = products_principal_stock.product_id', 'LEFT')
                            ->limit(5)
                            ->group_by('order_product.product')
                            ->where('order_invoice.order_status', $status)
                            ->where('products_principal_stock.branch_id', $id)
                            ->where('products.store_type', 'principal')
                            ->order_by('quantity', 'DESC')
                            ->get('order_product');
            return $query->num_rows();
        }

        public function mostviewed ($id) {
            $query = $this->db->select('products.name, products.price, products.viewed, categories.name AS category_name, products.id, etalase.name AS etalase_name')
                            ->join('categories', 'categories.id = products.category', 'LEFT')
                            ->join('etalase', 'etalase.id = products.etalase', 'LEFT')
                            ->where('products.status', 1)
                            ->where('products.store_id', $id)
                            ->where('products.store_type', 'merchant')
                            ->order_by('products.viewed', 'DESC')
                            ->limit(5)
                            ->get('products');
            return $query->num_rows();
        }

        public function mostviewed_principal ($id) {
            $query = $this->db->select('products.name, products.price, products.viewed, categories.name AS category_name, products.id, etalase.name AS etalse_name')
                            ->join('categories', 'categories.id = products.category', 'LEFT')
                            ->join('etalase', 'etalase.id = products.etalase', 'LEFT')
                            ->join('products_principal_stock', 'products.id = products_principal_stock.product_id')
                            ->where('products.status', 1)
                            ->where('products_principal_stock.branch_id', $id)
                            ->where('products.store_type', 'principal')
                            ->order_by('products.viewed', 'DESC')
                            ->limit(5)
                            ->get('products');
            return $query->num_rows();
        }

        public function last_orders ($id) {
            $query = $this->db->select('order_invoice.code, order_invoice.order, order_invoice.total, customers.fullname AS customer_name, order_invoice.customer, 
                                        merchants.name AS merchant_name, order_invoice.merchant, op.quantity, order_invoice.order_status AS status')
                            ->join('order_invoice', 'order_product.invoice = order_invoice.id', 'LEFT')
                            ->join('products', 'order_product.product = products.id', 'LEFT')
                            ->join('brands', 'products.brand = brands.id', 'LEFT')
                            ->join('customers', 'order_invoice.customer = customers.id', 'LEFT')
                            ->join('merchants', 'order_invoice.merchant = merchants.id', 'LEFT')
                            ->join('(SELECT SUM(quantity) AS quantity, invoice FROM order_product GROUP BY invoice) op', 'op.invoice = order_invoice.id', 'LEFT')
                            ->where('merchants.id', $id)
                            ->limit(10)
                            ->order_by('order_product.id', 'DESC')
                            ->get('order_product');
            return ($query->num_rows() > 0) ? $query->result() : FALSE;
        }

        public function total_order_by_status ($status, $id) {
            $this->db->where('merchant', $id)
                    ->where('order_status', $status);
            return $this->db->count_all_results('order_invoice');
        }

        public function last_balances ($id) {
            $query = $this->db->select('merchant_balances.*, order_invoice.code AS invoice_code')
                            ->join('order_invoice', 'order_invoice.id = merchant_balances.invoice', 'LEFT')
                            ->where('merchant_balances.merchant', $id)
                            ->order_by('merchant_balances.id', 'DESC')
                            ->limit(4)
                            ->get('merchant_balances');
            return ($query->num_rows() > 0) ? $query->result() : FALSE;
        }
    }

?>