<?php

    defined('BASEPATH') or exit('No direct script access allowed');

    class Orders_model extends CI_Model {
        public function get_all ($id, $status, $start, $length) {
            $query = $this->db->select('order_invoice.*, orders.shipping_name')
                            ->join('orders', 'order_invoice.order = orders.id', 'LEFT')
                            ->join('setting_order_status', 'order_invoice.order_status = setting_order_status.id', 'LEFT')
                            ->where('order_invoice.merchant', $id)
                            ->where('order_invoice.order_status', $status)
                            ->order_by('orders.date_added', 'DESC')
                            ->limit($length, $start)
                            ->get('order_invoice');
            return ($query->num_rows() > 0) ? $query->result() : FALSE;
        }

        public function get ($id, $merchant) {
            $query = $this->db->select('order_invoice.*, orders.shipping_name, orders.shipping_address, orders.payment_method, orders.unique_code, orders.total_plus_kode,
                                        provincies.name AS shipping_province, cities.name AS shipping_city, districts.name AS shipping_district, orders.shipping_postcode,
                                        orders.shipping_phone, customers.fullname AS customer_name, setting_order_status.name_for_merchant AS order_status_name, 
                                        payment_methods.name AS payment_method, payment_methods.title AS payment_method_title, payment_methods.vendor')
                            ->join('orders', 'order_invoice.order = orders.id', 'LEFT')
                            ->join('setting_order_status', 'order_invoice.order_status = setting_order_status.id', 'LEFT')
                            ->join('customers', 'customers.id = orders.customer', 'LEFT')
                            ->join('provincies', 'provincies.id = orders.shipping_province', 'LEFT')
                            ->join('cities', 'cities.id = orders.shipping_city', 'LEFT')
                            ->join('districts', 'districts.id = orders.shipping_district', 'LEFT')
                            ->join('payment_methods', 'orders.payment_method = payment_methods.name', 'LEFT')
                            ->where('order_invoice.id', $id)
                            ->where('order_invoice.merchant', $merchant)
                            ->get('order_invoice');
            return ($query->num_rows() > 0) ? $query->row() : FALSE;
        }

        public function get_products ($id) {
            $query = $this->db->select('order_product.price_old, order_product.discount, order_product.price, order_product.weight, order_product.quantity, 
                                        order_product.total, order_product.options, products.name')
                            ->join('products', 'order_product.product = products.id', 'LEFT')
                            ->where('order_product.invoice', $id)
                            ->get('order_product');
            return ($query->num_rows() > 0) ? $query->result() : FALSE;
        }
    }