<?php

defined('BASEPATH') or exit('No direct script access allowed!');

require '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Sales extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->lang->load('sales', settings('language'));
        $this->load->model('reports_model', 'reports');
        $this->data['menu'] = 'report_sales';
        $this->data['body_class'] = 'has-detached-left';
    }
    
    public function index() {
        if($this->aauth->is_loggedin()){
            $this->aauth->control('report/balances');
        }
        $this->breadcrumbs->unshift(lang('report'), '/');
        $this->breadcrumbs->push(lang('report_sales'), '/report/sales');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        // $merchant_data = $this->session->userdata('merchant_user');
        // if($merchant_data->merchant_parent == 0){
        //     $this->data['downline'] = $this->reports->get_downline($this->data['user']->merchant);
        //     $downline_ids = array();
        //     $with_parent_downline_ids = array($this->data['user']->merchant);
        //     if($this->data['downline']->num_rows() > 0){
        //         foreach($this->data['downline']->result() as $dl){
        //             array_push($downline_ids,$dl->id);
        //         }
        //         $merge = array_merge($with_parent_downline_ids,$downline_ids);
        //         $this->data['merge_mid'] = implode(",", $merge);
        //         $this->data['downline_ids'] = implode(",", $downline_ids);
        //     }
        // } else {
        //     $this->data['downline'] = array();
        // }
        $this->template->_init();
        $this->template->form();
        $this->template->table();
        $this->load->js(base_url('../').'assets/backend/js/modules/reports.js');
        $this->output->set_title(lang('heading'));
        $this->load->view('sales/index', $this->data);
    }
    
    public function data() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $input = $this->input->post(null, true);
        $filter = $input['filter'];
        $output['data'] = array();
        $datas = $this->reports->sales($this->data['user']->merchant,$input['start'], $input['length'], $input['order'][0], $filter['from'], $filter['to'],$filter['order_status'],$filter['shipping']);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    '<a href="'. site_url('orders/view/'. encode($data->id)).'" target="_blank">' . $data->invoice . '</a>',
                    get_date($data->date),
                    $data->merchant_name,
                    $data->customer,
                    number($data->total),
                    $data->order_status,
                    strtoupper($data->shipping_courier)
                );
            }
        }
        $output['recordsTotal'] = $this->main->count('order_invoice',array('merchant'=> $this->data['user']->merchant));
        $output['recordsFiltered'] = $this->reports->sales_count($this->data['user']->merchant,$filter['from'], $filter['to'],$filter['order_status'],$filter['shipping']);
        echo json_encode($output);
    }
    public function export() {
        $filename = "sales";
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        \PhpOffice\PhpSpreadsheet\Shared\File::setUseUploadTempDirectory(true);
        $sheet->fromArray(array('ID', 'Tanggal', 'Merchant', 'Pelanggan', 'Total', 'Status', 'Pengiriman'), NULL, 'A1');
        $filter = $this->input->get(null, true);
        $fullFileName = "${filename}_".$filter['from'].'_'.$filter['to'];
        $datas = $this->reports->sales_export($this->data['user']->merchant, $filter['from'], $filter['to'],$filter['order_status'],$filter['shipping']);
        $i = 2;
        if ($datas->num_rows() > 0) {
            foreach ($datas->result() as $data) {
                $row_data = array(
                    $data->invoice,
                    get_date($data->date),
                    $data->merchant_name,
                    $data->customer,
                    number_format($data->total,2,',','.'),
                    $data->order_status,
                    strtoupper($data->shipping_courier)
                );
                $sheet->fromArray($row_data, null, 'A' . $i);
                $i++;
            }
        };
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $fullFileName . '.xlsx"');
        header('Cache-Control: max-age=0');
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        ob_end_clean();
        $writer->save('php://output');
    }

   
}
