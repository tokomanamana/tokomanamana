<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('heading'); ?></h2>
            </div>
            <div class="heading-elements">
                <span class="heading-text">
                    <h4 class="no-margin">Saldo: <b><?php echo rupiah($balance); ?></b></h4>
                </span>
                <!-- <button type="button" id="add-request" class="btn btn-primary heading-btn">Request Tarik Dana</button> -->
            </div>
        </div>
    </div>
    <div class="content">
        <?php if ($is_requested) { ?>
            <div class="alert alert-success">Saat ini Anda memiliki permintaan Tarik Dana sebesar <b><?php echo rupiah($is_requested->amount); ?></b> yang masih Diproses.</div>
        <?php } ?>
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="row">
                    <form id="filter-form">
                        <div class="col-sm-5">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">Filter By:</label>
                                        <select id="filter-by" class="form-control">
                                            <option value="today">Hari Ini</option>
                                            <option value="yesterday">Kemarin</option>
                                            <option value="this month">Bulan Ini</option>
                                            <option value="last month">Bulan Lalu</option>
                                            <option value="this year">Tahun Ini</option>
                                            <option value="99">Kostum</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">Dari:</label>
                                        <input type="text" class="form-control" readonly="" name="from" value="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 no-padding-right">
                                    <div class="form-group">
                                        <label class="control-label">Sampai:</label>
                                        <input type="text" class="form-control" readonly="" name="to" value="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <button class="btn btn-default" style="margin-top: 27px;" type="button" id="filter">Filter</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="panel panel-flat">
            <table class="table table-hover" id="table-report" data-url="<?php echo site_url('reports/balances/data'); ?>">
                <thead>
                    <tr>
                        <th class="no-sort">Tanggal</th>
                        <th class="no-sort">Keterangan</th>
                        <th class="no-sort">Nominal</th>
                        <th class="no-sort">Saldo</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
<div id="modal-request" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" style="top: 10%;">&times;</button>
                <h5 class="modal-title">Request Tarik Dana</h5>
                <hr style="margin:0">
               
            </div>
            <div class="modal-body">
                 <h5 class="" style="margin-left: auto;margin-right: auto;width: fit-content;">Saldo saat ini:</h5>
                  <div style="padding:5px 10px 5px 10px; margin-bottom: 30px; font-size:large; border-radius: 5%;background-color: #97c23c;width: fit-content;margin-left: auto;margin-right: auto;"><b><?php echo rupiah($balance); ?></b></div>
                <form id="form-request" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Jumlah Penarikan</label>
                        <div class="col-sm-6">
                            <input type="text" name="amount" class="number form-control" min="0" value="0">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Kode Verifikasi</label>
                        <div class="col-sm-6">
                            <input type="text" name="token" class="form-control" autocomplete="off">
                            <p>Belum punya kode? <span id="wait" style="display:none;">Tunggu <span></span> detik</span><a id="request-token" href="javascript:void(0)">Kirim Kode</a></p>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Batal</button>
                <button type="button" onclick="submit_request()" class="btn btn-primary">Konfirmasi</button>
            </div>
        </div>
    </div>
</div>
<script>
    var time_left_verification = 0;
</script>