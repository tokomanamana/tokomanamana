<div class="content pb-20">
        <div class="panel panel-body login-form">
            <div class="text-center">
                <h5 class="content-group-lg">Masuk Akun Merchant</h5>
            </div>
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#via_email">Masuk via email</a></li>
                <li><a data-toggle="tab" href="#via_phone">Masuk via No.HP</a></li>
            </ul>
            <div class="tab-content">
            <div id="via_email" class="tab-pane fade in active">
            <div class="message"></div>
            <form action="<?php echo current_url(); ?>" method="post" id="login">
                <input type="hidden" name="login_type" value="email">
                <div class="form-group has-feedback has-feedback-left">
                    <input type="email" name="identity" id="identity" class="form-control" placeholder="<?php echo lang('email'); ?>" autocomplete="off" >
                    <div class="form-control-feedback">
                        <i class="icon-user text-muted"></i>
                    </div>
                </div>

                <div class="form-group has-feedback has-feedback-left">
                    <input type="password" name="password" id="password" class="form-control" placeholder="<?php echo lang('password'); ?>" autocomplete="off" >
                    <div class="form-control-feedback">
                        <i class="icon-lock2 text-muted"></i>
                    </div>
                </div>

                <div class="form-group login-options">
                    <div class="row">
                        <div class="col-sm-6">
                            <label class="checkbox-inline">
                                <input type="checkbox" class="styled" name="remember" value="1">
                                <?php echo lang('remember'); ?>
                            </label>
                        </div>
                        <!--                                        <div class="col-sm-6 text-right">
                                                                    <a href="login_password_recover.html">Forgot password?</a>
                                                                </div>-->
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn bg-blue btn-block">Masuk <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </form>
            </div>
            <div id="via_phone" class="tab-pane fade">
            <div class="message"></div>
            <form action="<?php echo current_url(); ?>" method="post" id="phone_login">
                <input type="hidden" name="login_type" value="phone">
                <div class="form-group has-feedback has-feedback-left">
                    <input type="text" name="phoneNum" id="phoneNum" class="form-control" placeholder="No Handphone" autocomplete="off" >
                    <div class="form-control-feedback">
                        <i class="icon-phone text-muted"></i>
                    </div>
                </div>
                <div class="form-group has-feedback has-feedback-left">
                <div claas="col-md-12">
                    <input type="text" name="otp_code" id="otp_code" class="form-control" placeholder="Kode OTP" autocomplete="off" >
                    <div class="form-control-feedback">
                        <i class="icon-lock text-muted"></i>
                    </div>
                </div>
                <div claas="col-md-12">
                    <a id="btn-token" onclick="request_otp()" href="javascript:void(0)" class="btn btn-block" style="background-color:green;color:white;"> Kirim OTP </a>
                </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn bg-blue btn-block">Masuk <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </form>
            </div>
            </div>
            <div>Belum punya akun?? <a href="<?php echo site_url('register');?>">Daftar disini!</a></div>
        </div>
</div>