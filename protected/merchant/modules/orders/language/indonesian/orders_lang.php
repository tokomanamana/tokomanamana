<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['order_heading'] = 'Pesanan';
$lang['order_list_heading'] = 'Daftar Pesanan';
$lang['order_add_heading'] = 'Tambah Pesanan';
$lang['order_edit_heading'] = 'Edit Pesanan';

$lang['order_code_th'] = 'ID';
$lang['order_date_th'] = 'Tanggal';
$lang['order_customer_th'] = 'Pelanggan';
$lang['order_status_th'] = 'Status';
$lang['order_total_th'] = 'Total';
$lang['order_shipping_th'] = 'Pengiriman';

$lang['order_button_paid'] = '<i class="icon-cash"></i> Pembayaran Diterima';

$lang['order_save_success_message'] = "Pesanan '%s' berhasil disimpan.";
$lang['order_save_error_message'] = "Pesanan '%s' gagal disimpan.";
$lang['order_delete_success_message'] = "Pesanan '%s' berhasil dihapus.";
$lang['order_delete_error_message'] = "Pesanan '%s' gagal dihapus.";