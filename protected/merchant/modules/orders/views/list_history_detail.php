<div class="content-wrapper" style="background-color: #e0e0e0">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2>History Tukar Poin Detail</h2>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <div class="container" style="padding-left: 24px; padding-right: 24px; padding-top: 18px">
                <div class="row">
                    <div class="col-xs-12 thumbnail" style="padding: 8px">
                        <p style="color: grey; margin-top: 8px; margin-bottom: -16px">Reward</p>
<?php
                            $join0 = array(
                                0 => 'reward r-r.id=peh.id_reward',
                                1 => 'reward_claim_temp rct-rct.id=peh.id_reward_claim'
                            );
                            $where0 = array(
                                'peh.id' => $id_history
                            );
                            $q_history = $m->get_data('r.*, rct.periode, peh.status_pengiriman', 'point_exchange_history peh', $join0, $where0);
                            $r = $q_history->row();
?>
                        <h3 style="margin-left: 16px"><?=$r->nama?></h3>
                        <p style="color: grey; margin-bottom: -16px">Periode Tukar Poin</p>
<?php
                            $e = explode(';', $r->periode);

                            $periode = $e[2].'/'.$e[1].'/'.$e[1].' sampai '.$e[5].'/'.$e[4].'/'.$e[3];
?>
                        <h3 style="margin-left: 14px;"><?=$periode?></h3>
                        <p style="color: grey; margin-bottom: -16px">Status</p>
<?php
                            if($r->status_pengiriman == 0){
                                $status_pengiriman = '<label class="label label-danger">Belum Dikirim</label>';
                            }else{
                                $status_pengiriman = '<label class="label label-success">Sudah Dikirim</label>';
                            }
?>
                        <h3 style="margin-left: 14px;"><?=$status_pengiriman?></h3>
                    </div>
                </div>
            </div>
            
        </div>
    </div>

    <div class="content" style="margin-top:-62px">
        <div class="panel panel-flat">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12" style="text-align: center">
                        <h3><?=$r->nama?></h3>
                    </div>
<?php
                    if($r->id_product != 0){
                        $q_p = $m->get_data('', 'products p', array(0 => 'product_image i-i.product=p.id'), array('p.id' => $r->id_product, 'i.primary' => 1))->row();
?>
                    <div class="col-xs-6 col-sm-3 thumbnail" style="height: 300px; ">
                        <div class="content" style="height: 300px; ">
                            <div class="panel panel-flat">
                                <div class="row">
                                    <div class="col-xs-12" style="">
                                        <img src="https://tokomanamana.com/files/images/<?=$q_p->image?>" class="" style="margin-top: 8px; height: 266px; width: 100%; max-height: 266px; object-fit: cover;" >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-9 thumbnail" style="height: 300px; ">
                        <div class="content" style="height: 300px; ">
                            <div class="panel panel-flat">
                                <div class="row">
                                    <div class="col-xs-12" style="height: 274px">
                                        <p style="padding-left: 12px; font-size: 18px;"><?=$q_p->name?></p>
                                        <p style="padding-left: 12px; color: grey; font-size: 10px">Code</p>
                                        <p style="margin-top: -8px; padding-left: 22px;"><?=$q_p->code?></p>
                                        <p style="padding-left: 12px; color: grey; font-size: 10px">QTY</p>
                                        <p style="margin-top: -8px; padding-left: 22px;"><?=$r->qty_product?></p>
                                        <p style="padding-left: 12px; color: grey; font-size: 10px">Berat</p>
                                        <p style="margin-top: -8px; padding-left: 22px;"><?=number_format($q_p->weight)?> gram</p>
                                        <p style="padding-left: 12px; color: grey; font-size: 10px">Short Desc</p>
                                        <p style="margin-top: -8px; padding-left: 22px;"><?=$q_p->short_description?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
<?php
                    }

                    if($r->id_product_paket != 0){
                        $q_p = $m->get_data('', 'products p', array(0 => 'product_image i-i.product=p.id'), array('p.id' => $r->id_product_paket, 'i.primary' => 1))->row();
?>
                    <div class="col-xs-6 col-sm-3 thumbnail" style="height: 300px; ">
                        <div class="content" style="height: 300px; ">
                            <div class="panel panel-flat">
                                <div class="row">
                                    <div class="col-xs-12" style="">
                                        <img src="https://tokomanamana.com/files/images/<?=$q_p->image?>" class="" style="margin-top: 8px; height: 266px; width: 100%; max-height: 266px; object-fit: cover;" >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-9 thumbnail" style="height: 300px; ">
                        <div class="content" style="height: 300px; ">
                            <div class="panel panel-flat">
                                <div class="row">
                                    <div class="col-xs-12" style="height: 274px">
                                        <p style="padding-left: 12px; font-size: 18px;"><?=$q_p->name?></p>
                                        <p style="padding-left: 12px; color: grey; font-size: 10px">Code</p>
                                        <p style="margin-top: -8px; padding-left: 22px;"><?=$q_p->code?></p>
                                        <p style="padding-left: 12px; color: grey; font-size: 10px">QTY</p>
                                        <p style="margin-top: -8px; padding-left: 22px;"><?=$r->qty_paket?></p>
                                        <p style="padding-left: 12px; color: grey; font-size: 10px">Berat</p>
                                        <p style="margin-top: -8px; padding-left: 22px;"><?=number_format($q_p->weight)?> gram</p>
                                        <p style="padding-left: 12px; color: grey; font-size: 10px">Short Desc</p>
                                        <p style="margin-top: -8px; padding-left: 22px;"><?=$q_p->short_description?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
<?php
                    }
?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function detail_reward(val) {
        $.ajax({
            type: 'POST',
            url: '<?=base_url('orders/reward/get_detail_reward')?>',
            data: {id_reward: val, is_history: 0},
            dataType: "json",
            success: function (data) {
                $('#data_detail').html(data.data);
                $('#content_detail_reward').show();
                $('html, body').animate({
                    scrollTop: $("#content_detail_reward").offset().top
                }, 500);
            }
        });
        
    }

    function tukar_point(id_reward) {
        $.ajax({
            type: 'POST',
            url: '<?=base_url('orders/reward/tukar_point')?>',
            data: {id_reward: id_reward},
            dataType: "json",
            success: function (data) {
                if(data.success == 1){
                    window.location.reload(true);
                }else{
                    swal({
                        title: "Ooops",
                        text: data.msg,
                        type: "error",
                        html: true
                    }, function () {
                    });
                }
                
            }
        });
        
    }
</script>