<?php
    $join = array(
        0 => 'point_reward_cart_detail prcd-prcd.id_cart=prc.id',
        1 => 'reward r-r.id=prcd.id_reward'
    );
    $q_merchant = $m->get_data('', 'point_reward_cart prc', $join, array('prc.id_merchant' => $this->session->userdata('user_id')))->row();
?>
<div class="content-wrapper" style="background-color: #e0e0e0">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2>Konfirmasi Tukar Poin</h2>
            </div>
        </div>
    </div>
    <div class="content" >
        <div class="panel panel-flat" style="background-color: #d33">
            <div class="container" style="padding-left: 24px; padding-right: 24px; padding-top: 18px; background-color: #d33">
                <div class="row" style="background-color: #d33">
                    <div class="col-xs-12 thumbnail" style="padding: 8px; background-color: #d33; text-align: center">
                        <h3 style="margin-left: 16px; color: white; text-align: center">Yakin ingin tukar point?</h3>
                        <hr style="margin-top: -8px; width: 240px">
                        <div class="row">
                            <div class="col-xs-4"></div>
                            <div class="col-xs-2" style="height: 38px; padding-top: 6px;">
                                <a href="#" onclick="confirm_(<?=$q_merchant->id_reward?>)" style="font-size: 16px; color: white; width: 100%; height: 100%;"><i class="icon-check"></i> Ya</a>
                            </div>
                            <div class="col-xs-2" style="height: 38px; padding-top: 6px;">
                                <a href="#" onclick="cancel_(<?=$q_merchant->id_reward?>)" style="font-size: 16px; color: white; "><i class="icon-cross"></i> Tidak</a>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>

    <div class="content" style="margin-top:-62px;" id="content_detail_reward">
        <div class="panel panel-flat">
            <div class="container">
                <div class="row" id="data_detail">
                    
                    <div class="col-xs-12" style="text-align: center">
                        <h3><?=$q_merchant->nama?></h3>
                    </div>
                    <?php
                        if($q_merchant->id_product != 0){
                            $q_p = $m->get_data('', 'products p', array(0 => 'product_image i-i.product=p.id'), array('p.id' => $q_merchant->id_product, 'i.primary' => 1))->row();
                    ?>
                    <div class="col-xs-6 col-sm-3 thumbnail" style="height: 300px; ">
                        <div class="content" style="height: 300px; ">
                            <div class="panel panel-flat">
                                <div class="row">
                                    <div class="col-xs-12" style="">
                                        <img src="https://tokomanamana.com/files/images/<?=$q_p->image?>" class="" style="margin-top: 8px; height: 266px; width: 100%; max-height: 266px; object-fit: cover;" >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-9 thumbnail" style="height: 300px; ">
                        <div class="content" style="height: 300px; ">
                            <div class="panel panel-flat">
                                <div class="row">
                                    <div class="col-xs-12" style="height: 274px">
                                        <p style="padding-left: 12px; font-size: 18px;"><?=$q_p->name?></p>
                                        <p style="padding-left: 12px; color: grey; font-size: 10px">Code</p>
                                        <p style="margin-top: -8px; padding-left: 22px;"><?=$q_p->code?></p>
                                        <p style="padding-left: 12px; color: grey; font-size: 10px">QTY</p>
                                        <p style="margin-top: -8px; padding-left: 22px;"><?=$q_merchant->qty_product?></p>
                                        <p style="padding-left: 12px; color: grey; font-size: 10px">Berat</p>
                                        <p style="margin-top: -8px; padding-left: 22px;"><?=number_format($q_p->weight)?> gram</p>
                                        <p style="padding-left: 12px; color: grey; font-size: 10px">Short Desc</p>
                                        <p style="margin-top: -8px; padding-left: 22px;"><?=$q_p->short_description?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                        }

                        if($q_merchant->id_product_paket != 0){
                            $q_p = $m->get_data('', 'products p', array(0 => 'product_image i-i.product=p.id'), array('p.id' => $q_merchant->id_product_paket, 'i.primary' => 1))->row();
                    ?>
                    <div class="col-xs-6 col-sm-3 thumbnail" style="height: 300px; ">
                        <div class="content" style="height: 300px; ">
                            <div class="panel panel-flat">
                                <div class="row">
                                    <div class="col-xs-12" style="">
                                        <img src="https://tokomanamana.com/files/images/<?=$q_p->image?>" class="" style="margin-top: 8px; height: 266px; width: 100%; max-height: 266px; object-fit: cover;" >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-9 thumbnail" style="height: 300px; ">
                        <div class="content" style="height: 300px; ">
                            <div class="panel panel-flat">
                                <div class="row">
                                    <div class="col-xs-12" style="height: 274px">
                                        <p style="padding-left: 12px; font-size: 18px;"><?=$q_p->name?></p>
                                        <p style="padding-left: 12px; color: grey; font-size: 10px">Code</p>
                                        <p style="margin-top: -8px; padding-left: 22px;"><?=$q_p->code?></p>
                                        <p style="padding-left: 12px; color: grey; font-size: 10px">QTY</p>
                                        <p style="margin-top: -8px; padding-left: 22px;"><?=$q_merchant->qty_paket?></p>
                                        <p style="padding-left: 12px; color: grey; font-size: 10px">Berat</p>
                                        <p style="margin-top: -8px; padding-left: 22px;"><?=number_format($q_p->weight)?> gram</p>
                                        <p style="padding-left: 12px; color: grey; font-size: 10px">Short Desc</p>
                                        <p style="margin-top: -8px; padding-left: 22px;"><?=$q_p->short_description?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div class="content" style="margin-top:-62px">
        <div class="panel panel-flat">
            <div class="container">
                <div class="row">

                    
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function confirm_(id_reward) {
        $.ajax({
            type: 'POST',
            url: '<?=base_url('orders/reward/confirm_tukar_poin')?>',
            data: {id_reward: id_reward},
            dataType: "json",
            success: function (data) {
                if(data.success == 1){
                    swal({
                        title: "Permintaan selesai diproses!",
                        text: 'Reward akan segera dikirim',
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        html: true
                    }, function () {
                        window.location.href = "<?=base_url('orders/reward/history/')?>"+data.id_history;
                    });
                }else{
                    swal({
                        title: "Ooops",
                        text: data.msg,
                        type: "error",
                        html: true
                    }, function () {
                        window.location.reload(true);
                    });
                }
            }
        });
        
    }
</script>