<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('order_heading'); ?></h2>
            </div>

<!--            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('orders/form'); ?>" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span><?php echo lang('order_add_heading'); ?></span></a>
                </div>
            </div>-->
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <table class="table table-hover" id="table" data-url="<?php echo site_url('orders/get_list'); ?>" data-state-save="false">
                <thead>
                    <tr>
                        <th><?php echo lang('order_code_th'); ?></th>
                        <th class="default-sort" data-sort="desc"><?php echo lang('order_date_th'); ?></th>
                        <th><?php echo lang('order_customer_th'); ?></th>
                        <th><?php echo lang('order_total_th'); ?></th>
                        <th><?php echo lang('order_shipping_th'); ?></th>
                        <th><?php echo lang('order_status_th'); ?></th>
                        <th class="no-sort text-center" style="width: 20px;"><?php echo lang('actions_th'); ?></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>