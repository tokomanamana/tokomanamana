<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('order_heading') . ' ' . $order->code; ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('orders/orders_merchant/invoice/'. encode($order->id)); ?>" class="btn btn-link btn-float has-text" target="_blank"><i class="icon-printer2 text-primary"></i><span>Print</span></a>
                    <a href="<?php echo site_url('orders/orders_merchant/resi/'. encode($order->id)); ?>" class="btn btn-link btn-float has-text" target="_blank"><i class="icon-printer2 text-primary"></i><span>Cetak Resi</span></a>
                    <a href="<?php echo site_url('orders/orders_merchant'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('order_list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-sm-9">
                <div class="panel panel-flat panel-body">
                    <div class="row">
                        <div class="col-sm-7">
                            <h6 class="no-margin text-semibold text-uppercase text-muted">Info Order</h6>
                            <form class="form-horizontal">
                                <div class="row">
                                    <label class="col-sm-5 control-label">No Pesanan</label>
                                    <div class="col-sm-7 form-control-static"><?php echo $order->code; ?></div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-5 control-label">Tanggal</label>
                                    <div class="col-sm-7 form-control-static"><?php echo get_date_time($order->date_added); ?></div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-5 control-label">Status</label>
                                    <div class="col-sm-7 form-control-static"><?php echo $order->order_status_name; ?></div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-5 control-label">Pemesan</label>
                                    <div class="col-sm-7 form-control-static"><?php echo $order->customer_name; ?></div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-5 control-label">Alamat Pengiriman</label>
                                    <div class="col-sm-7 form-control-static"><?php echo $order->shipping_name . '<br>' . nl2br($order->shipping_address) . ', ' . $order->shipping_district . '<br>' . $order->shipping_city . ', ' . $order->shipping_province . '<br>' . $order->shipping_postcode; ?></div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-5 control-label">No Telepon</label>
                                    <div class="col-sm-7 form-control-static"><?php echo $order->shipping_phone; ?></div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-5 control-label">Total Pesanan</label>
                                    <div class="col-sm-7 form-control-static"><?php echo rupiah($order->total); ?></div>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-5">
                            <h6 class="no-margin text-semibold text-uppercase text-muted">Pengiriman</h6>
                            <form class="form-horizontal">
                                <div class="row">
                                    <label class="col-sm-5 control-label">Logistik</label>
                                    <div class="col-sm-7 form-control-static">
                                        <?php
                                        $courier = explode('-', $order->shipping_courier);
                                        if ($courier[0] != 'pickup') {
                                            echo strtoupper($courier[0]) . ' ' . $courier[1];
                                        } else {
                                            echo 'Ambil Sendiri';
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-5 control-label">No resi</label>
                                    <div class="col-sm-7 form-control-static"><?php echo $order->tracking_number.' '.(($order->order_status==settings('order_shipped_status'))?'<a href="javascript:void(0);" onclick="change_airway(this,\''.encode($order->id).'\')"><i class="icon-pencil3"></i></a>':''); ?>
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-5 control-label">Biaya Kirim</label>
                                    <div class="col-sm-7 form-control-static"><?php echo rupiah($order->shipping_cost); ?></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="panel panel-flat">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Kode Produk</th>
                                <th>Nama Produk</th>
                                <th>Qty</th>
                                <th>Berat</th>
                                <th>Harga</th>
                                <th>Total Harga</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($products) foreach ($products->result() as $product) { ?>
                                    <tr>
                                        <td><?php echo $product->code; ?></td>
                                        <td><?php echo $product->name; ?></td>
                                        <td><?php echo number($product->quantity); ?></td>
                                        <td><?php echo number($product->weight) . ' gram'; ?></td>
                                        <td><?php echo rupiah($product->price); ?></td>
                                        <td><?php echo rupiah($product->total); ?></td>
                                    </tr>
                                    <?php
                                        if($product->type == 'p') {
                                        $package_items = json_decode($product->package_items, true);
                                        foreach ($package_items as $package) {
                                    ?>
                                    <tr style="background-color:#e2e2e2">
                                        <td align="center"></td>
                                        <td><?php echo $package['product_name'] ?></td>
                                        <td><?php echo $package['qty'] ?></td>
                                        <td align="center">-</td>
                                        <td align="center">-</td>
                                        <td align="center">-</td>
                                    </tr>
                                    <?php 
                                            }
                                        } ?>
                                <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-sm-3">
                <?php
                $order_status_for_action = array(settings('order_payment_received_status'), settings('order_process_status'), settings('order_ready_pickup_status'));
                if (in_array($order->order_status, $order_status_for_action)) {
                    ?>
                    <div class="panel panel-flat panel-body">
                        <div class="row ">
                            <div class="col-sm-12 text-center">
                                <?php   
                                    $datenow = new DateTime();
                                    $due_date = new DateTime($order->due_date);
                                    $diff = $datenow->diff($due_date);
                                    $hari = $diff->d ? $diff->d.' Hari ' : '';
                                    $jam = $diff->h ? $diff->h.' Jam ' : '';
                                    $menit = $diff->i ? $diff->i.' Menit ' : '';
                                    $due_time = $hari.$jam.$menit;
                                ?>
                                <?php if($diff->invert == 0 && $order->order_status == 3) {?>
                                <div class="btn btn-default mb-10" style="width: 100%;">
                                    <div>Batas waktu : </div>
                                    <?php echo $due_time;?>
                                </div>
                                <?php } ?>
                                <?php if($diff->invert == 0 && $order->order_status != 3) {?>
                                <div class="btn btn-default mb-10" style="width: 100%;">
                                    <div>Batas waktu : </div>
                                    <?php echo $due_time;?>
                                </div>
                                <?php } ?>
                                <?php if ($order->order_status == settings('order_payment_received_status')) { ?>
                                    <a href="<?php echo site_url('orders/orders_merchant/respon/1/' . encode($order->id)); ?>" class="btn btn-primary btn-lg mb-10" style="width: 100%;">Terima Pesanan</a>
                                    <a href="<?php echo site_url('orders/orders_merchant/respon/2/' . encode($order->id)); ?>" class="btn btn-default btn-sm" style="width: 80%;">Tolak Pesanan</a>
                                <?php } elseif ($order->order_status == settings('order_process_status')) { ?>
                                    <?php if ($courier[0] == 'pickup') { ?>
                                        <a href="<?php echo site_url('orders/orders_merchant/update/' . encode($order->id) . '/' . encode(settings('order_ready_pickup_status'))); ?>" class="btn btn-primary btn-sm mb-10" style="width: 80%;">Pesanan Siap Diambil</a>
                                        <a href="<?php echo site_url('orders/orders_merchant/update/' . encode($order->id) . '/' . encode(settings('order_cancel_status'))); ?>" class="btn btn-default btn-sm" style="width: 80%;">Batalkan Pesanan</a>
                                    <?php } else { ?>
                                        <form action="<?php echo site_url('orders/orders_merchant/sent/' . encode($order->id)); ?>" method="post">
                                            <input type="text" class="form-control" name="airway" required placeholder="Input resi pengiriman disini">
                                            <button type="submit" class="btn btn-primary mt-10 mb-10">Konfirmasi Pengiriman</button>
                                            <a href="<?php echo site_url('orders/orders_merchant/update/' . encode($order->id) . '/' . encode(settings('order_cancel_status'))); ?>" class="btn btn-default btn-sm" style="width: 80%;">Batalkan Pesanan</a>
                                        </form>
                                    <?php } ?>
                                <?php } elseif ($order->order_status == settings('order_ready_pickup_status')) { ?>
                                <?php if ($courier[0] != 'pickup') { ?>
                                    <a href="<?php echo site_url('orders/orders_merchant/update/' . encode($order->id) . '/' . encode(settings('order_complete_pickup_status'))); ?>" class="btn btn-primary btn-sm" style="width: 80%;">Pesanan Sudah Diambil</a>
                                <?php } else { ?>
                                    <a data-toggle="modal" data-urlsuccess="<?php echo site_url('orders/orders_merchant/update/' . encode($order->id) . '/' . encode(settings('order_complete_pickup_status'))); ?>" data-mobilenumber="<?php echo $order->shipping_phone; ?>" data-target="#requestOTPModal" class="btn btn-primary btn-sm openOTPModal" style="width: 80%;">Pesanan Sudah Diambil</a>
                                <?php } } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($histories) { ?>
                    <div class="panel panel-flat panel-body">
                        <h6 class="no-margin text-semibold text-uppercase text-muted">Riwayat Pesanan</h6>
                        <ul class="list-feed">
                            <?php foreach ($histories->result() as $history) { ?>
                                <li class="border-warning-400">
                                    <div class="text-muted text-size-small mb-5"><?php echo get_date_time($history->date); ?></div>
                                    <?php echo $history->status; ?>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } ?>

                <div class="modal fade" id="requestOTPModal" tabindex="-1" role="dialog" 
                    aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <button type="button" class="close" 
                                data-dismiss="modal">
                                    <span aria-hidden="true">&times;</span>
                                    <span class="sr-only">Close</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">
                                    Request OTP
                                </h4>
                            </div>
                            
                            <!-- Modal Body -->
                            <div class="modal-body">
                                
                                <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label  class="col-sm-3 control-label"
                                            for="no-hp">No HP</label>
                                    <div class="col-sm-9">
                                        <input type="hidden" id="otpOrderID" value="<?php echo $order->id; ?>"/>
                                        <input type="hidden" id="otpOrderNum" value="<?php echo $order->code; ?>"/>
                                        <input type="text" class="form-control" 
                                        id="inputNoHP" value="" disabled=""/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"
                                        for="inputPassword3" >Kode Konfirmasi</label>
                                    <div class="col-sm-6">
                                        <input type="text" id="otpCode" class="form-control" placeholder="your otp code"/>
                                    </div>
                                    <div class="col-sm-3">
                                    <a id="btn-token" href="javascript:void(0)" class="btn btn-default" onclick="requestOTPButton()">Request OTP</a>
                                    </div>
                                </div>
                                </form>
                            </div>
                            
                            <!-- Modal Footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default"
                                        data-dismiss="modal">
                                            Close
                                </button>
                                <a href="javascript:void(0)" class="btn btn-primary" onclick="submitOTP()">
                                    Konfirmasi
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>