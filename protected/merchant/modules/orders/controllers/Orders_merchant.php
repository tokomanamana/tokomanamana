<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Orders_merchant extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->lang->load('orders', settings('language'));
        $this->load->model('orders_merchant_model', 'orders');
        $this->data['menu'] = 'order_merchant';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();
        $this->load->js('../assets/backend/js/modules/orders.js');

        $this->breadcrumbs->push(lang('menu_order'), '/orders');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('order_heading'));
        $this->load->view('list_order_merchant', $this->data);
    }

    public function view($id) {
        $this->template->_init();
        $this->load->js('../assets/backend/js/merchant/modules/orders/view.js');

        $id = decode($id);
        $this->data['order'] = $this->orders->get($id);
        $this->data['products'] = $this->orders->get_products($id);
        $this->data['histories'] = $this->orders->get_histories($id);

        $this->breadcrumbs->unshift(lang('menu_order'), '/orders');
        $this->breadcrumbs->push($this->data['order']->code, '/orders/view/' . encode($id));

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('order_heading') . ' ' . $this->data['order']->code);
        $this->load->view('view_order_merchant', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->orders->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                if ($data->shipping_courier != 'pickup') {
                    $data->shipping_courier = explode('-', $data->shipping_courier);
                    $data->shipping_courier = strtoupper($data->shipping_courier[0]) . ' ' . $data->shipping_courier[1];
                }
                $output['data'][] = array(
                    '<a href="' . site_url('orders/orders_merchant/view/' . encode($data->id)) . '">' . $data->code . '</a>',
                    get_date_time($data->date_added),
                    $data->shipping_name,
                    number($data->total),
                    $data->shipping_courier,
                    $data->order_status_name,
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">' .
                    '<li><a href="' . site_url('orders/orders_merchant/view/' . encode($data->id)) . '">' . lang('button_view') . '</a></li>
                    </ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->orders->count_all();
        $output['recordsFiltered'] = $this->orders->count_all($search);
        echo json_encode($output);
    }

    public function respon($respon = 1, $order) {
        $id = decode($order);
        $invoice = $this->main->get('order_invoice_merchant', array('id' => $id));
        if ($invoice) {
            if ($respon == 1) {
                $order_status = settings('order_process_status');
                $this->main->update('order_invoice_merchant', array('order_status' => $order_status, 'due_date' => date('Y-m-d H:i:s', strtotime(' +2 day'))), array('id' => $id));
                $this->main->insert('order_history_merchant', array('order' => $invoice->order, 'invoice' => $invoice->id, 'order_status' => $order_status));
                redirect('orders/orders_merchant/view/' . encode($id));
            } elseif ($respon == 2) {
                $merchant_group = $this->main->get('merchant_groups', array('id' => $this->data['user']->merchant_group));
//                $merchant = $this->main->get('merchants', array('id' => $merchant_group->branch));
                $product= $this->orders->get_products($id)->result()[0];
                $this->main->update('order_invoice_merchant', array('merchant' => $merchant_group->branch), array('id' => $id));
                //$this->db->query("UPDATE product_merchant SET quantity = (quantity + $product->quantity) WHERE product = $product->product AND merchant = " . $invoice->merchant);
                redirect('orders/orders_merchant');
            }
        }
    }

    public function sent($order) {
        $id = decode($order);
        $invoice = $this->main->get('order_invoice_merchant', array('id' => $id));
        if ($invoice && $this->input->post('airway')) {
            $order_status = settings('order_shipped_status');
            $this->main->update('order_invoice_merchant', array('order_status' => $order_status, 'tracking_number' => $this->input->post('airway')), array('id' => $id));
            $this->main->insert('order_history_merchant', array('order' => $invoice->order, 'invoice' => $invoice->id, 'order_status' => $order_status));

            $this->data['order'] = $this->orders->get($invoice->id);
            $this->data['products'] = $this->main->gets('order_product_merchant', ['invoice' => $invoice->id]);
            $this->data['merchant'] = $this->orders->merchant($this->data['order']->merchant);

            // $this->load->helper('text');
            // $message = $this->load->view('email/shipping_confirmation', $this->data, true);
            // $cronjob = array(
            //     'from' => settings('send_email_from'),
            //     'from_name' => settings('store_name'),
            //     'to' => $this->data['order']->customer_email,
            //     'subject' => 'Pesanan ' . $this->data['order']->code . ' Sudah Dikirim',
            //     'message' => $message
            // );
            // $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
            // $customer = $this->main->get('customers', array('id' => $this->data['order']->customer));
            // if ($customer->verification_phone) {
            //     $sms = json_decode(settings('sprint_sms'), true);
            //     $sms['m'] = 'Pesanan ' . $this->data['order']->code . ' sudah dikirim dengan nomor resi ' . $this->data['order']->tracking_number;
            //     $sms['d'] = $customer->phone;
            //     $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
            // }

            redirect('orders/orders_merchant/view/' . encode($id));
        }
    }

    public function update($order, $status) {
        $id = decode($order);
        $status = decode($status);
        $invoice = $this->main->get('order_invoice_merchant', array('id' => $id));
        $this->main->update('order_invoice_merchant', array('order_status' => $status), array('id' => $id));
        $this->main->insert('order_history_merchant', array('order' => $invoice->order, 'invoice' => $invoice->id, 'order_status' => $status));
        if ($status == settings('order_ready_pickup_status')) {
            $this->data['order'] = $this->orders->get($invoice->id);
            $this->data['products'] = $this->main->gets('order_product_merchant', ['invoice' => $invoice->id]);
            $this->data['merchant'] = $this->orders->merchant($this->data['order']->merchant);
            // $customer = $this->main->get('customers', array('id' => $this->data['order']->customer));
            // $this->load->helper('text');
            // $message = $this->load->view('email/shipping_confirmation', $this->data, true);
            // $cronjob = array(
            //     'from' => settings('send_email_from'),
            //     'from_name' => settings('store_name'),
            //     'to' => $customer->email,
            //     'subject' => 'Siap Diambil: ' . $this->data['order']->code,
            //     'message' => $message
            // );
            // $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));

            // if ($customer->verification_phone) {
            //     $this->load->library('sprint');
            //     $sms = json_decode(settings('sprint_sms'), true);
            //     $sms['m'] = 'Pesanan ' . $this->data['order']->code . ' siap diambil. Lihat alamat pengambilan di detail pesanan Anda';
            //     $sms['d'] = $customer->phone;
            //     $url = $sms['url'];
            //     unset($sms['url']);
            //     $sprint_response = $this->sprint->sms($url, $sms);
            //     $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
            // }
        }
        if ($status == settings('order_complete_pickup_status')) {
            $this->main->update('order_invoice_merchant', array('date_received' => date('Y-m-d H:i:s'), 'due_date' => date('Y-m-d H:i:s', strtotime(' +2 day'))), array('id' => $invoice->id));
            $this->data['order'] = $this->orders->get($invoice->id);
            $this->data['products'] = $this->main->gets('order_product_merchant', ['invoice' => $invoice->id]);
            $this->data['merchant'] = $this->orders->merchant($this->data['order']->merchant);
            // $customer = $this->main->get('customers', array('id' => $this->data['order']->customer));
            // $this->load->helper('text');
            // $message = $this->load->view('email/pickup_completed', $this->data, true);
            // $cronjob = array(
            //     'from' => settings('send_email_from'),
            //     'from_name' => settings('store_name'),
            //     'to' => $customer->email,
            //     'subject' => 'Konfirmasi Pengambilan: ' . $this->data['order']->code,
            //     'message' => $message
            // );
            // $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
        }
        if ($status == settings('order_cancel_status')) {
            $this->main->update('order_invoice', array('order_status' => $status), array('id' => $invoice->id));
            $this->data['order'] = $this->orders->get($invoice->id);
            $this->data['products'] = $this->main->gets('order_product_merchant', ['invoice' => $invoice->id]);
            $this->data['merchant'] = $this->orders->merchant($this->data['order']->merchant);
            $products = $this->main->gets('order_product_merchant', array('invoice' => $invoice->id));
            if ($products) {
                foreach ($products->result() as $product) {
                    $product_detail = $this->db->query("select type, package_items from products where id = $product->product ");
                    $row = $product_detail->row();
                    // if($row->type == 'p') {
                    //     $package_items = json_decode($row->package_items, true);
                    //     foreach ($package_items as $package) {
                    //         $this->db->query("UPDATE product_merchant SET quantity = (quantity + $product->quantity) WHERE product = ".$package['product_id']." AND merchant = " . $this->data['merchant']->id); 
                    //     }
                    // } else {
                    //     $this->db->query("UPDATE product_merchant SET quantity = (quantity + $product->quantity) WHERE product = $product->product AND merchant = " . $this->data['merchant']->id);
                    // }
                }
            }
        }
        redirect('orders/orders_merchant/view/' . encode($id));
    }

    public function request_token() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $order_id = $this->input->post('order_id');
        $phone = $this->input->post('phone');
        //$phone = '087777546456';
        $invoice = $this->main->get('order_invoice_merchant', array('id' => $order_id));
        $return = ['status' => 'error','message' => ''];
        $date = date('Y-m-d H:i:s');
        // $check_expired= $this->main->get('orders', array('id' => $order_id,'otp_expired <=' => $date));
        // if($check_expired){
                 $this->load->library('sprint');
            $sms = json_decode(settings('sprint_sms'), true);
            $code = rand(1000, 9999);
            // $currentDate = strtotime($date);
            // $futureDate = $currentDate+(60*2);
            // $formatDate = date('Y-m-d H:i:s', $futureDate);
            $sms['m'] = 'Hai, berikut kode verifikasi TokoManaMana Anda ' . $code;
            $sms['d'] = $phone;
            $url = $sms['url'];
            unset($sms['url']);
            $sprint_response = $this->sprint->sms($url, $sms);
            $this->main->update('orders_merchant', array('otp_code' => $code, 'otp_sent_time' => $date), array('id' => $invoice->order));
            $return['status'] = 'success';
            $return['message'] = 'Kode OTP berhasil dikirim!';
        //   } else {
        //     $return['status'] = 'failed';
        //     $return['message'] = 'Kode OTP sudah pernah dikirim. Harap tunggu 2 menit untuk request OTP baru!';
        // }
        echo json_encode($return);
    }

    public function check_token(){
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $order_id = $this->input->post('order_id');
        $code = $this->input->post('code');
        $invoice = $this->main->get('order_invoice_merchant', array('id' => $order_id));
        $checking = $this->main->get('orders_merchant', array('id' => $invoice->order,'otp_code' => $code));
        if($checking){
            //$datenow = date('Y-m-d H:i:s');
            //$check_expired= $this->main->get('orders', array('id' => $order_id,'otp_expired <=' => $datenow));
            // if($check_expired){
            //     $return['status'] = 'failed';
            //     $return['message'] = 'Kode OTP sudah expired!';
            // } else {
                $return['status'] = 'success';
                $return['message'] = 'Verifikasi berhasil!';
            //}
        } else {
            $return['status'] = 'failed';
            $return['message'] = 'Kode OTP tidak sesuai!';
        }
        echo json_encode($return);   
    }

    public function invoice($id) {
        $id = decode($id);
        $this->data['order'] = $this->orders->get($id);
        $this->data['products'] = $this->orders->get_products($id);
        $this->load->view('invoice', $this->data);
    }

    public function resi($id) {
        $id = decode($id);
        $this->data['order'] = $this->orders->get($id);
        $this->data['merchant'] = $this->orders->merchant($this->data['order']->merchant);
        $this->data['products'] = $this->orders->get_products($id);
        $this->load->view('resi', $this->data);
    }

}
