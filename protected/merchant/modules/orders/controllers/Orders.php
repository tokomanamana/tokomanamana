<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Orders extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->lang->load('orders', settings('language'));
        $this->load->model('orders_model', 'orders');
        $this->data['menu'] = 'order';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();
        // $this->load->js('../assets/backend/js/modules/orders.js');

        $this->load->js('../assets/backend/js/merchant/modules/orders/view.js');

        $this->breadcrumbs->push(lang('menu_order'), '/orders');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('order_heading'));
        $this->load->view('list', $this->data);
    }

    public function view($id) {
        $this->template->_init();
        $this->load->js('../assets/frontend/js/sweetalert2.all.min.js');
        $this->load->js('../assets/backend/js/merchant/modules/orders/view.js');

        $id = decode($id);
        $this->data['order'] = $this->orders->get($id);
        $this->data['products'] = $this->orders->get_products($id);
        $this->data['histories'] = $this->orders->get_histories($id);

        $this->breadcrumbs->unshift(lang('menu_order'), '/orders');
        $this->breadcrumbs->push($this->data['order']->code, '/orders/view/' . encode($id));

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('order_heading') . ' ' . $this->data['order']->code);
        $this->load->view('view', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->orders->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                if ($data->shipping_courier != 'pickup') {
                    $data->shipping_courier = explode('-', $data->shipping_courier);
                    $data->shipping_courier = strtoupper($data->shipping_courier[0]) . ' ' . $data->shipping_courier[1];
                }
                $output['data'][] = array(
                    '<a href="' . site_url('orders/view/' . encode($data->id)) . '">' . $data->code . '</a>',
                    get_date_time($data->date_added),
                    $data->shipping_name,
                    number($data->total),
                    $data->shipping_courier,
                    $data->order_status_name,
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">' .
                    '<li><a href="' . site_url('orders/view/' . encode($data->id)) . '">' . lang('button_view') . '</a></li>
                    </ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->orders->count_all();
        $output['recordsFiltered'] = $this->orders->count_all($search);
        echo json_encode($output);
    }

    public function respon_old($respon = 1, $order) {
        $id = decode($order);
        $invoice = $this->main->get('order_invoice', array('id' => $id));
        if ($invoice) {
            if ($respon == 1) {
                $order_status = settings('order_process_status');
                $this->main->update('order_invoice', array('order_status' => $order_status, 'due_date' => date('Y-m-d H:i:s', strtotime(' +2 day'))), array('id' => $id));
                $this->main->insert('order_history', array('order' => $invoice->order, 'invoice' => $invoice->id, 'order_status' => $order_status));
                redirect('orders/view/' . encode($id));
            } elseif ($respon == 2) {
                $merchant_group = $this->main->get('merchant_groups', array('id' => $this->data['user']->merchant_group));
//                $merchant = $this->main->get('merchants', array('id' => $merchant_group->branch));
                $product= $this->orders->get_products($id)->result()[0];
                $this->main->update('order_invoice', array('merchant' => $merchant_group->branch), array('id' => $id));
                $this->db->query("UPDATE product_merchant SET quantity = (quantity + $product->quantity) WHERE product = $product->product AND merchant = " . $invoice->merchant);
                redirect('orders');
            }
        }
    }

     public function respon($respon = 1, $order) {
        $id = decode($order);
        $invoice = $this->main->get('order_invoice', array('id' => $id));
        if ($invoice) {
            if ($respon == 1) {
                $order_status = settings('order_process_status');
                $products= $this->orders->get_products($id);
                $this->main->update('order_invoice', array('order_status' => $order_status, 'due_date' => date('Y-m-d H:i:s', strtotime(' +2 day'))), array('id' => $id));
                $this->main->insert('order_history', array('order' => $invoice->order, 'invoice' => $invoice->id, 'order_status' => $order_status));

                if($this->data['user']->merchant_type == 'merchant'){

                    foreach($products->result() AS $product){
                        if($product->options){
                            $products_option = json_decode($product->options);
                            if(count($products_option) > 1 || !isset($products_option[0]->type)){

                                foreach($products_option AS $product_option){
                                    $this->db->query("UPDATE product_option SET quantity = CASE WHEN quantity < $product_option->quantity THEN 0 ELSE quantity - $product_option->quantity END WHERE id = $product_option->id AND product = $product->product");

                                }

                            }
                    
                        }
                        else{
                            $this->db->query("UPDATE products SET quantity = CASE WHEN quantity < $product->quantity THEN 0 ELSE quantity - $product->quantity END WHERE id = $product->product AND store_type = 'merchant' AND store_id = " . $invoice->merchant);
                        }
                        
                     }
                }
                else if ($this->data['user']->merchant_type == 'principle_branch'){
                   
                    foreach($products->result() AS $product){
                        if($product->options){
                            $products_option = json_decode($product->options);
                            if(count($products_option) > 1 || !isset($products_option[0]->type)){

                                foreach($products_option AS $product_option){
                                    // $this->db->query("UPDATE product_option SET quantity = CASE WHEN quantity < $product_option->quantity THEN 0 ELSE quantity - $product_option->quantity END WHERE id = $product_option->id AND product = $product->product");

                                    $this->db->query("UPDATE products_principal_stock SET quantity = CASE WHEN quantity < $product_option->quantity THEN 0 ELSE quantity - $product_option->quantity END WHERE product_id = $product->product AND id_option = $product_option->id AND branch_id = " . $invoice->merchant);

                                }

                            }

                    
                        }else{
                             // $this->db->query("UPDATE products SET quantity = CASE WHEN quantity < $product->quantity THEN 0 ELSE quantity - $product->quantity END WHERE id = $product->product AND store_type = 'principal'");

                             $this->db->query("UPDATE products_principal_stock SET quantity = CASE WHEN quantity < $product->quantity THEN 0 ELSE quantity - $product->quantity END WHERE product_id = $product->product AND branch_id = " . $invoice->merchant);

                        }

                    }


                }
                
                $this->data['order'] = $this->orders->get($invoice->id);
                $this->data['products'] = $this->main->gets('order_product', ['invoice' => $invoice->id]);
                $this->data['merchant'] = $this->orders->merchant($this->data['order']->merchant);
                if($this->data['order']->customer_email){
                    $this->load->helper('text');
                    $message = $this->load->view('email/process', $this->data, true);
                    $cronjob = array(
                        'from' => settings('send_email_from'),
                        'from_name' => settings('store_name'),
                        'to' => $this->data['order']->customer_email,
                        'subject' => 'Pesanan ' . $this->data['order']->code . ' Sedang Diproses',
                        'message' => $message
                    );
                    $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
                }
                else{
                    if($this->data['order']->customer_verif){
                        // $this->load->library('sprint');
                        $sms = json_decode(settings('sprint_sms'), true);
                        $sms['m'] = 'Pesanan dengan kode ' . $this->data['order']->code . ' sedang di proses oleh penjual.';
                        $sms['d'] = $this->data['order']->customer_phone;
                        $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                        $this->main->insert('log_sms', array('content' => json_encode($sms), 'to' => $this->data['order']->customer_phone));

                    }
                }


                redirect('orders/view/' . encode($id));
                
            } elseif ($respon == 2) {
                $order_status = settings('order_rejected_status');
                $order = $this->main->get('orders', array('id' => $invoice->order));
                $this->main->update('order_invoice',array('order_status' => $order_status),array('id' => $id));
                $this->main->insert('order_history', array('order' => $invoice->order, 'invoice' => $invoice->id, 'order_status' => $order_status));

                if($order->payment_status == 'Paid'){
                    if($order->payment_method == 'transfer'){
                        $refund = round($order->total_plus_kode);
                    } else {
                        $refund = $order->total;
                    } 
                    $get_balance = $this->main->get('customer_balance', array('customer_id' => $order->customer));
                    if($get_balance){
                         $old_balance = $get_balance->balance;
                    } else {
                         $old_balance = 0;
                    }
                    $new_balance = $old_balance + $refund;
                    $this->main->insert('customer_balance_history', array('customer_id' => $order->customer, 'order_id' => $order->id, 'type' => 'IN', 'amount' => $refund, 'balance' => $new_balance));
                    if($get_balance){
                        $this->main->update('customer_balance', array('balance' => $new_balance), array('customer_id' => $order->customer));
                    } else {
                        $this->main->insert('customer_balance', array('customer_id' => $order->customer, 'balance' => $new_balance));
                    }
                }
                $this->main->update('orders',array('payment_status' => 'Reject'),array('id' => $invoice->order));

                $this->data['order'] = $this->orders->get($invoice->id);
                $this->data['products'] = $this->main->gets('order_product', ['invoice' => $invoice->id]);
                $this->data['merchant'] = $this->orders->merchant($this->data['order']->merchant);
                $this->data['refund'] = $refund;
                $this->load->helper('text');
                if($this->data['order']->customer_email){
                    $message = $this->load->view('email/rejected', $this->data, true);
                    $cronjob = array(
                        'from' => settings('send_email_from'),
                        'from_name' => settings('store_name'),
                        'to' => $this->data['order']->customer_email,
                        'subject' => 'Pesanan ' . $this->data['order']->code . ' Ditolak Penjual',
                        'message' => $message
                    );
                    $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));

                }
                else{
                    if($this->data['order']->customer_verif){
                        // $this->load->library('sprint');
                        $sms = json_decode(settings('sprint_sms'), true);
                        $sms['m'] = 'Pesanan anda dengan kode ' . $this->data['order']->code . ' ditolak oleh penjual.';
                        $sms['d'] = $this->data['order']->customer_phone;
                        $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms))); 
                        $this->main->insert('log_sms', array('content' => json_encode($sms), 'to' => $this->data['order']->customer_phone));
                    }
                }
                
                redirect('orders');
            }
        }
    }

    public function sent($order) {
        $id = decode($order);
        $invoice = $this->main->get('order_invoice', array('id' => $id));
        if ($invoice && $this->input->post('airway')) {
            $order_status = settings('order_shipped_status');
            $this->main->update('order_invoice', array('order_status' => $order_status, 'tracking_number' => $this->input->post('airway')), array('id' => $id));
            $this->main->insert('order_history', array('order' => $invoice->order, 'invoice' => $invoice->id, 'order_status' => $order_status));

            $this->data['order'] = $this->orders->get($invoice->id);
            $this->data['products'] = $this->main->gets('order_product', ['invoice' => $invoice->id]);
            $this->data['merchant'] = $this->orders->merchant($this->data['order']->merchant);

            $this->load->helper('text');
            if($this->data['order']->customer_email){
                $message = $this->load->view('email/shipping_confirmation', $this->data, true);
                $cronjob = array(
                    'from' => settings('send_email_from'),
                    'from_name' => settings('store_name'),
                    'to' => $this->data['order']->customer_email,
                    'subject' => 'Pesanan ' . $this->data['order']->code . ' Sudah Dikirim',
                    'message' => $message
                );
                $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
            }
            // $customer = $this->main->get('customers', array('id' => $this->data['order']->customer));
            if ($this->data['order']->customer_verif) {
                // $this->load->library('sprint');
                $sms = json_decode(settings('sprint_sms'), true);
                $sms['m'] = 'Pesanan ' . $this->data['order']->code . ' sudah dikirim dengan nomor resi ' . $this->data['order']->tracking_number;
                $sms['d'] = $this->data['order']->customer_phone;
                $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                $this->main->insert('log_sms', array('content' => json_encode($sms), 'to' => $this->data['order']->customer_phone));
            }

            redirect('orders/view/' . encode($id));
        }
    }

    public function update($order, $status) {
        $id = decode($order);
        $status = decode($status);
        $invoice = $this->main->get('order_invoice', array('id' => $id));
        $this->main->update('order_invoice', array('order_status' => $status), array('id' => $id));
        $this->main->insert('order_history', array('order' => $invoice->order, 'invoice' => $invoice->id, 'order_status' => $status));
        if ($status == settings('order_ready_pickup_status')) {
            $this->data['order'] = $this->orders->get($invoice->id);
            $this->data['products'] = $this->main->gets('order_product', ['invoice' => $invoice->id]);
            $this->data['merchant'] = $this->orders->merchant($this->data['order']->merchant);
            // $customer = $this->main->get('customers', array('id' => $this->data['order']->customer));
            $this->load->helper('text');
            if($this->data['order']->customer_email){
                $message = $this->load->view('email/ready_pickup', $this->data, true);
                $cronjob = array(
                    'from' => settings('send_email_from'),
                    'from_name' => settings('store_name'),
                    'to' => $this->data['order']->customer_email,
                    'subject' => 'Siap Diambil: ' . $this->data['order']->code,
                    'message' => $message
                );
                $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
            }

            if ($this->data['order']->customer_verif) {
                $this->load->library('sprint');
                $sms = json_decode(settings('sprint_sms'), true);
                $sms['m'] = 'Pesanan dengan kode ' . $this->data['order']->code . ' siap diambil. Lihat alamat pengambilan di detail pesanan Anda';
                $sms['d'] = $this->data['order']->customer_phone;
                $url = $sms['url'];
                unset($sms['url']);
                $sprint_response = $this->sprint->sms($url, $sms);
                $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                $this->main->insert('log_sms', array('content' => json_encode($sms), 'to' => $this->data['order']->customer_phone));
            }
        }
        if ($status == settings('order_complete_pickup_status')) {
            $this->main->update('order_invoice', array('date_received' => date('Y-m-d H:i:s'), 'due_date' => date('Y-m-d H:i:s', strtotime(' +2 day'))), array('id' => $invoice->id));
            $this->data['order'] = $this->orders->get($invoice->id);
            $this->data['products'] = $this->main->gets('order_product', ['invoice' => $invoice->id]);
            $this->data['merchant'] = $this->orders->merchant($this->data['order']->merchant);
            // $customer = $this->main->get('customers', array('id' => $this->data['order']->customer));
            $this->load->helper('text');
            if($this->data['order']->customer_email){
                $message = $this->load->view('email/pickup_completed', $this->data, true);
                $cronjob = array(
                    'from' => settings('send_email_from'),
                    'from_name' => settings('store_name'),
                    'to' => $this->data['order']->customer_email,
                    'subject' => 'Konfirmasi Pengambilan: ' . $this->data['order']->code,
                    'message' => $message
                );
                $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
            }
            else{
                if($this->data['order']->customer_verif){
                    // $this->load->library('sprint');
                    $sms = json_decode(settings('sprint_sms'), true);
                    $sms['m'] = 'Menurut penjual pesanan ' . $this->data['order']->code . ' sudah diambil oleh anda. Terimakasih, teruslah belanja di Tokomanamana.com! ';
                    $sms['d'] = $this->data['order']->customer_phone;
                    $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                    $this->main->insert('log_sms', array('content' => json_encode($sms), 'to' => $this->data['order']->customer_phone));

                }
            }
        }
        // if ($status == settings('order_cancel_status')) {
        //     $this->main->update('order_invoice', array('order_status' => $status), array('id' => $invoice->id));
        //     $this->data['order'] = $this->orders->get($invoice->id);
        //     $this->data['products'] = $this->main->gets('order_product', ['invoice' => $invoice->id]);
        //     $this->data['merchant'] = $this->orders->merchant($this->data['order']->merchant);
        //     $products = $this->main->gets('order_product', array('invoice' => $invoice->id));
        //     $order = $this->main->get('orders', array('id' => $invoice->order));
        //     if ($products) {
        //         if($this->data['user']->merchant_type == 'merchant'){
        //             foreach ($products->result() as $product) {
        //                 if($product->options){
        //                     $products_option = json_decode($product->options);
        //                     if(count($products_option) > 1 || !isset($products_option[0]->type)){

        //                         foreach($products_option AS $product_option){
        //                             $this->db->query("UPDATE product_option SET quantity = (quantity + $product_option->quantity) WHERE id = $product_option->id AND product = $product->product");
        //                         }

        //                     }
        //                 }
        //                 else{
        //                     $this->db->query("UPDATE products SET quantity = (quantity + $product->quantity) WHERE id = $product->product AND store_type = 'merchant' AND store_id = " .  $this->data['merchant']->id);       

        //                 }
     
                                 
        //             }
        //         }
        //         else if ($this->data['user']->merchant_type == 'principle_branch'){
        //             foreach ($products->result() as $product) {
        //                 if($product->options){
        //                     $products_option = json_decode($product->options);
        //                     if(count($products_option) > 1 || !isset($products_option[0]->type)){

        //                         foreach($products_option AS $product_option){
        //                             $this->db->query("UPDATE products_principal_stock SET quantity = (quantity + $product_option->quantity) WHERE product_id = $product->product AND option_id = $product_option->id AND branch_id = " . $this->data['merchant']->id);
        //                         }

        //                     }
        //                 }else{

        //                    // $this->db->query("UPDATE products SET quantity = (quantity + $product->quantity) WHERE id = $product->product AND store_type = 'principal'");   
        //                     $this->db->query("UPDATE products_principal_stock SET quantity = (quantity + $product->quantity) WHERE product_id = $product->product AND branch_id = " .  $this->data['merchant']->id);     
        //                 }
     
                                
        //             }

        //         }
                
        //     }
        //     if($order->payment_status == 'Paid'){
        //         if($order->payment_method == 'transfer'){
        //             $refund = round($order->total_plus_kode);
        //         } else {
        //             $refund = $order->total;
        //         } 
        //         $get_balance = $this->main->get('customer_balance', array('customer_id' => $order->customer));
        //         if($get_balance){
        //              $old_balance = $get_balance->balance;
        //         } else {
        //              $old_balance = 0;
        //         }
        //         $new_balance = $old_balance + $refund;
        //         $this->main->insert('customer_balance_history', array('customer_id' => $order->customer, 'order_id' => $order->id, 'type' => 'IN', 'amount' => $refund, 'balance' => $new_balance));
        //         if($get_balance){
        //             $this->main->update('customer_balance', array('balance' => $new_balance), array('customer_id' => $order->customer));
        //         } else {
        //             $this->main->insert('customer_balance', array('customer_id' => $order->customer, 'balance' => $new_balance));
        //         }
        //     }
        //     $this->main->update('orders',array('payment_status' => 'Cancel'),array('id' => $invoice->order));
        //     $this->data['refund'] = $refund;
        //     $this->load->helper('text');
        //     $message = $this->load->view('email/cancelled_by_merchant', $this->data, true);
        //     $cronjob = array(
        //         'from' => settings('send_email_from'),
        //         'from_name' => settings('store_name'),
        //         'to' => $this->data['order']->customer_email,
        //         'subject' => 'Pesanan ' . $this->data['order']->code . ' Dibatalkan Penjual',
        //         'message' => $message
        //     );
        //     $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));


        // }
        redirect('orders/view/' . encode($id));
    }

    public function request_token() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $order_id = $this->input->post('order_id');
        $phone = $this->input->post('phone');
        //$phone = '087777546456';
        $invoice = $this->main->get('order_invoice', array('id' => $order_id));
        $return = ['status' => 'error','message' => ''];
        $date = date('Y-m-d H:i:s');
        // $check_expired= $this->main->get('orders', array('id' => $order_id,'otp_expired <=' => $date));
        // if($check_expired){
                 $this->load->library('sprint');
            $sms = json_decode(settings('sprint_sms'), true);
            $code = rand(1000, 9999);
            // $currentDate = strtotime($date);
            // $futureDate = $currentDate+(60*2);
            // $formatDate = date('Y-m-d H:i:s', $futureDate);
            $sms['m'] = 'Hai, berikut kode verifikasi TokoManaMana Anda ' . $code;
            $sms['d'] = $phone;
            $url = $sms['url'];
            unset($sms['url']);
            $sprint_response = $this->sprint->sms($url, $sms);
            $this->main->update('orders', array('otp_code' => $code, 'otp_sent_time' => $date), array('id' => $invoice->order));
            $return['status'] = 'success';
            $return['message'] = 'Kode OTP berhasil dikirim!';
        //   } else {
        //     $return['status'] = 'failed';
        //     $return['message'] = 'Kode OTP sudah pernah dikirim. Harap tunggu 2 menit untuk request OTP baru!';
        // }
        echo json_encode($return);
    }

    public function check_token(){
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $order_id = $this->input->post('order_id');
        $code = $this->input->post('code');
        $invoice = $this->main->get('order_invoice', array('id' => $order_id));
        $checking = $this->main->get('orders', array('id' => $invoice->order,'otp_code' => $code));
        if($checking){
            //$datenow = date('Y-m-d H:i:s');
            //$check_expired= $this->main->get('orders', array('id' => $order_id,'otp_expired <=' => $datenow));
            // if($check_expired){
            //     $return['status'] = 'failed';
            //     $return['message'] = 'Kode OTP sudah expired!';
            // } else {
                $return['status'] = 'success';
                $return['message'] = 'Verifikasi berhasil!';
            //}
        } else {
            $return['status'] = 'failed';
            $return['message'] = 'Kode OTP tidak sesuai!';
        }
        echo json_encode($return);   
    }

    public function invoice($id) {
        $id = decode($id);
        $this->data['order'] = $this->orders->get($id);
        $this->data['products'] = $this->orders->get_products($id);
        $this->load->view('invoice', $this->data);
    }

    public function resi($id) {
        $id = decode($id);
        $this->data['order'] = $this->orders->get($id);
        $this->data['merchant'] = $this->orders->merchant($this->data['order']->merchant);
        $this->data['products'] = $this->orders->get_products($id);
        $this->load->view('resi', $this->data);
    }

}
