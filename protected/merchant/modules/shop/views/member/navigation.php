<div class="collection-leftsidebar sidebar col-sm-3 clearfix">
    <div class="sidebar-block collection-block">
        <div class="sidebar-title">
            <span>Akun Saya</span>
            <i class="fa fa-caret-down show_sidebar_content" aria-hidden="true"></i>
        </div>
        <div class="sidebar-content">
            <ul class="list-cat">
                <?php
                    $total_point = 0;
                    $this->load->model('Model');

                    $q_point = $this->Model->get_data('point', 'point', null, array('id_customer' => $this->session->userdata('user_id')));

                    if($q_point->num_rows() > 0){
                        $total_point = $q_point->row()->point;
                    }
                ?>
                <li class="<?php echo ($page == 'shop/member/history') ? 'active' : ''; ?>"><a href="<?php echo site_url('shop/member/history'); ?>"><?php echo lang('account_order_history'); ?></a> <?php echo ($total_active_order) ? '<span class="badge badge-notification" style="margin-top: 0;">' . $total_active_order . '</span>' : ''; ?></li>
                <li><a href="<?php echo site_url('shop/member/logout'); ?>">Keluar</a></li>


<!--<li class="<?php echo ($page == 'shop/member/payment_confirmation') ? 'active' : ''; ?>"><a href="<?php echo site_url('shop/member/payment_confirmation'); ?>"><?php echo lang('account_text_payment_confirmation'); ?></a></li>-->
            </ul>
        </div>
        <select class="form-control sidebar-content-select" onchange="window.location.href = this.value">
            <option value="<?php echo site_url('shop/member/history'); ?>" <?php echo ($page == 'shop/member/history') ? 'selected' : ''; ?>><?php echo lang('account_order_history'); ?></option>
        </select>
    </div>										
</div>