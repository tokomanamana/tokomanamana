<div class="my-account" id="history">
    <section class="collection-heading heading-content ">
        <div class="container">
            <div class="row">
                <div class="col-sm-9 col-sm-push-3">
                    <div class="collection-wrapper">
                        <h1 class="collection-title"><span><?php echo lang('account_order_history'); ?></span></h1>
                        <?php echo $breadcrumb; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container" style="margin-bottom: 30px;">
        <div class="row">
            <?php echo $this->load->view('navigation'); ?>
            <div class="collection-mainarea col-md-9 col-sm-8 col-xs-12">
                <?php if ($orders->num_rows() > 0) { ?>
                    <?php foreach ($orders->result() as $order) { 
                        $datenow = new DateTime();
                        $due_date = new DateTime($order->due_date);
                        $diff = $datenow->diff($due_date);
                        $hari = $diff->d ? $diff->d.' Hari ' : '';
                        $jam = $diff->h ? $diff->h.' Jam ' : '';
                        $menit = $diff->i ? $diff->i.' Menit ' : '';
                        $due_time = $hari.$jam.$menit;
                        ?>
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <span class="header-label">NO. TAGIHAN</span>
                                        <span><?php echo $order->code; ?></span>
                                        <span class="header-label"><?php echo get_date_time($order->date_added); ?></span>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <span class="header-label">TOTAL TAGIHAN</span>
                                        <span><?php echo rupiah($order->total); ?></span>
                                    </div>
                                    <div class="col-md-5 col-xs-12">
                                        <div style="display: table;width: 100%;border-spacing: 0;">
                                            <div style="width: auto; display: table-cell; vertical-align: top;">
                                                <span class="header-label">STATUS TAGIHAN</span>
                                                <?php echo lang($order->payment_status); ?>
                                                <?php if($diff->invert == 0 && $order->payment_status == 'Pending') {?>
                                                <span style="font-size:12px;margin:3px 0px">Batas waktu :</span>
                                                <span class="label label-success"><?php echo $due_time;?></span>
                                                <?php } ?>
                                            </div>
                                            <div style="width: 33%; display: table-cell; vertical-align: middle;">
                                                <a href="<?php echo site_url('shop/member/order_detail/' . encode($order->id)); ?>" class="detail">Lihat Detail</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <?php $invoices = $this->member->get_order_invoice($order->id); ?>
                                <?php if ($invoices->num_rows() > 0) foreach ($invoices->result() as $invoice) { 
                                     $due_datex = new DateTime($invoice->due_date);
                                     $diffx = $datenow->diff($due_datex);
                                     $harix = $diffx->d ? $diffx->d.' Hari ' : '';
                                     $jamx = $diffx->h ? $diffx->h.' Jam ' : '';
                                     $menitx = $diffx->i ? $diffx->i.' Menit ' : '';
                                     $due_timex = $harix.$jamx.$menitx;
                                    ?>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <span class="header-label">BARANG</span>
                                            <?php $products = $this->member->get_order_products($invoice->id); ?>
                                            <?php if ($products->num_rows() > 0) foreach ($products->result() as $product) { ?>
                                                <div class="product">
                                                    <div class="image">
                                                        <a href="<?php echo seo_url('shop/catalog/products/view/' . $product->product); ?>" target="_blank">
                                                            <img src="<?php echo get_image($product->image); ?>">
                                                        </a>
                                                    </div>
                                                    <div class="name"><?php echo $product->name; ?></div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <span class="header-label">PENJUAL</span>
                                            <span><?php echo $invoice->merchant_name; ?></span>
                                        </div>
                                        <div class="col-md-5 col-xs-12">
                                            <div style="display: table;width: 100%;border-spacing: 0;">
                                                <div style="width: auto; display: table-cell; vertical-align: top;">
                                                    <span class="header-label">STATUS PEMBELIAN</span>
                                                    <span><?php echo $invoice->order_status_name; ?></span>
                                                    <?php if($diffx->invert == 0 && $order->payment_status == 'Paid' && $invoice->order_status != 8) {?>
                                                        <span style="font-size:12px;margin:3px 0px">Batas waktu :</span>
                                                        <span class="label label-success"><?php echo $due_timex;?></span>
                                                    <?php } ?>
                                                </div>
                                                <div style="width: 33%; display: table-cell; vertical-align: middle;">
                                                    <!-- <a href="<?php echo site_url('shop/member/complain/' . encode($invoice->id)); ?>" class="detail">Komplain</a> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                    <div style="display: block; text-align: center;">
                        <div class="pagi-bar">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="block">
                        <p>Belum ada pesanan.</p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<div id="modal-tracking" class="modal" role="dialog" aria-hidden="true" tabindex="-1" data-width="800" style="display: none;">
    <div class="modal-dialog fadeIn animated">
        <div class="modal-content">
            <form action="#">
                <input type="hidden" name="id">
                <div class="modal-header">
                    <h1 class="modal-title">Lacak Pesanan</h1>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" style="background-color: #FFF; color: #000;">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
