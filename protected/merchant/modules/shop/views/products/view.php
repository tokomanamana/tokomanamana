<style>
    .flex-direction-nav a {
        height:50px !important;
    }
</style>
<section class="collection-heading heading-content">
    <div class="container">
        <div class="row">
            <div class="collection-wrapper">
                <h1 class="collection-title"></h1>
                <div class="breadcrumb-group">
                    <div class="breadcrumb clearfix">
                        <?php echo $breadcrumb; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="product-detail-content">
    <div class="detail-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="shopify-section">
                    <div class="detail-content-inner">
                        <div id="product" class="neque-porro-quisquam-est-qui-dolor-ipsum-quia-9 detail-content">
                            <div class="col-md-12 info-detail-pro clearfix">
                                <div class="col-md-5" id="product-image">
                                    <div class="show-image-load" style="display: none;">
                                        <div class="show-image-load-inner">
                                            <i class="fa fa-spinner fa-pulse fa-2x"></i>
                                        </div>
                                    </div>
                                    <!-- <div id="featuted-image" class="image featured"> -->
                                        <div id="slider" class="flexslider">
                                            <ul class="slides">
                                                <?php
                                                if ($images) {
                                                    $list_images = array();
                                                    $img_primary = array();
                                                    if ($is_option) {
                                                        echo $images;
                                                    } else {
                                                        ?>
                                                        <?php foreach ($images->result() as $image) { 
                                                            if($image->primary == "1") {
                                                                $img_primary[] = $image;
                                                            } else {
                                                                $list_images[] = $image;
                                                            }
                                                        ?>
                                                        <?php 
                                                            }
                                                            $newlist_img = array_merge($img_primary,$list_images);
                                                        ?>
                                                        <?php foreach ($newlist_img as $image) { ?>
                                                            <li>
                                                            <a href="<?php echo get_image($image->image); ?>" data-fancybox="gallery" data-options='{"loop":true,"smallBtn":false,"toolbar":false}'>
                                                                <img src="<?php echo get_image($image->image); ?>" alt="<?php echo $data->name; ?>" data-item="<?php echo $image->id; ?>"/>
                                                            </a>
                                                            </li>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php } ?>
                                                <!-- items mirrored twice, total of 12 -->
                                            </ul>
                                            </div>
                                            <div id="carousel" class="flexslider">
                                            <ul class="slides">
                                            <?php
                                                if ($images) {
                                                    $list_images = array();
                                                    $img_primary = array();
                                                    if ($is_option) {
                                                        echo $images;
                                                    } else {
                                                        ?>
                                                        <?php foreach ($images->result() as $image) { 
                                                            if($image->primary == "1") {
                                                                $img_primary[] = $image;
                                                            } else {
                                                                $list_images[] = $image;
                                                            }
                                                        ?>
                                                        <?php 
                                                            }
                                                            $newlist_img = array_merge($img_primary,$list_images);
                                                        ?>
                                                        <?php foreach ($newlist_img as $image) { ?>
                                                            <li>
                                                                <img src="<?php echo get_image($image->image); ?>" alt="<?php echo $data->name; ?>" data-item="<?php echo $image->id; ?>"/>
                                                            </li>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php } ?>
                                                <!-- items mirrored twice, total of 12 -->
                                            </ul>
                                        </div>
                                    <!-- </div> -->
                                </div>
                                <div class="col-md-7" id="product-information">
                                    <h1 itemprop="name" class="title"><?php echo $data->name; ?></h1>
                                    <div class="description" itemprop="description"><?php echo $data->short_description; ?></div>
                                    <div class="variants">
                                        <div class="product-options ">
                                            <div class="vendor-type">
                                                <?php if ($data->brand) { ?>
                                                    <span class="product_vendor"><span class="_title">Merek:</span> <?php echo $data->brand_name; ?></span>
                                                <?php } ?>
                                                <?php if ($data->merchant) { ?>
                                                    <span class="product_type"><span class="_title">Penjual:</span> <a href="<?php echo site_url('merchants/view/' . $data->merchant); ?>"><?php echo $data->merchant_name; ?></a></span>
                                                <?php } ?>
                                                <span class="product_sku"><span class="_title">Kode Produk: </span><?php echo $data->code; ?></span>
                                            </div>
                                            <div class="rating-star">
                                                <span class="spr-badge">
                                                    <span class="spr-starrating spr-badge-starrating">
                                                        <?php for ($i = 1; $i <= $rating->rating; $i++) { ?>
                                                            <i class="spr-icon spr-icon-star"></i>
                                                        <?php } ?>
                                                        <?php for ($i = $rating->rating + 1; $i <= 5; $i++) { ?>
                                                            <i class="spr-icon spr-icon-star-empty"></i>
                                                        <?php } ?>
                                                    </span>
                                                    <span class="spr-badge-caption"><?php echo ($rating->review) ? number($rating->review) : 'Tidak ada'; ?> ulasan</span>
                                                </span>
                                            </div>
                                            <?php
                                            if ($is_option) {
                                                echo '<div class="product-type" id="product-option">' . $options . '</div>';
                                            }
                                            ?>
                                            <div class="product-price">
                                                <?php
                                                    if($inPromo) {
                                                ?>
                                                <div style="margin:10px auto">
                                                    <span style="background-color:red;color:white;padding:5px 10px;"><?php echo $label_disc;?></span>
                                                    <span class="money" style="text-decoration: line-through;font-size:12px;color:#8b8f8b">
                                                    <?php echo rupiah($old_price); ?></span>
                                                </div>
                                                <?php
                                                    }
                                                ?>
                                                <h2 class="price"><?php echo rupiah($price); ?></h2>
                                                <div>Berat : <?php echo $data->weight; ?> gram</div>
                                                <?php if (isset($price_level) && $price_level->num_rows() > 0) { ?>
                                                    <?php $pl_1 = $price_level->row(0); $pl_2 = $price_level->row(1);?>
                                                    <div class="grochire">
                                                        Beli <?php echo $pl_1->min_qty; ?> lebih murah <b>@ <?php echo rupiah($pl_1->price); ?></b> <a data-toggle="popover" data-container="body" data-placement="bottom" data-html="true" href="javascript:void(0);" id="view-list-grochire" class="link">Lihat harga grosir</a>
                                                        <div class="popover">
                                                            <table class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Jumlah</th>
                                                                        <th>Harga / unit</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php foreach ($price_level->result() as $row => $pl) { ?>
                                                                        <tr>
                                                                            <td>
                                                                                <?php
                                                                                if (($row + 1) == $price_level->num_rows()) {
                                                                                    echo '>= ' . $pl->min_qty;
                                                                                } else {
                                                                                    echo $pl->min_qty . ' - ' . ($price_level->row($row + 1)->min_qty - 1);
                                                                                }
                                                                                ?></td>
                                                                            <td><?php
                                                                                echo rupiah($pl->price);
                                                                                ?></td>
                                                                        </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <div class="purchase-section multiple">
                                                <?php if ($data->merchant && $data->quantity < 1) { ?>
                                                    <p>Stok habis</p>
                                                <?php } else { ?>
                                                    <div class="quantity-wrapper clearfix">
                                                        <div class="wrapper">
                                                            <input type="text" name="qty" value="<?php echo $pl_2->min_qty;?>" min="<?php echo $pl_1->min_qty;?>" maxlength="3" class="item-quantity">
                                                        </div>
                                                    </div>
                                                    <div class="purchase">
                                                        <input type="hidden" name="product" value="<?php echo encode($data->id);?>">
                                                        <input type="hidden" name="min_qty" value="<?php echo $pl_1->min_qty;?>">
                                                        <button class="btn add-to-cart" id="btn-cart" type="button">Beli Sekarang</button>
                                                        <a href="javascript:void(0);" class="btn btn-default" onclick="addtoCart()"
                                                            style="height: 48px;width: 48px !important;padding-left: 10px;padding-right: 10px;margin-left:10px;padding:5px 0;text-align: center;background-color:#fff;">
                                                            <i class="fa fa-shopping-cart" style="font-size:16px;color:#000;"></i>
                                                        </a>
                                                    </div>
                                                    
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="supports-fontface">
                                        <div class="social-sharing is-clean">
                                            <div class="addthis_inline_share_toolbox_su95"></div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tabs-information" class="col-md-12">
                                <?php if ($data->description) { ?>
                                    <div class="information_content panel panel-default">
                                        <div class="panel-heading" role="tab" id="heading_des">
                                            <h4 class="panel-title" data-toggle="collapse" href="#collapse_des" aria-expanded="true" aria-controls="collapse_des">
                                                Deskripsi
                                                <i class="fa-icon fa fa-angle-up"></i>
                                            </h4>
                                        </div>
                                        <div id="collapse_des" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading_des">
                                            <div class="panel-body"><?php echo $data->description; ?></div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($features) { ?>
                                    <div class="information_content panel panel-default">
                                        <div class="panel-heading" role="tab" id="heading_spec">
                                            <h4 class="panel-title" data-toggle="collapse" href="#collapse_spec" aria-expanded="true" aria-controls="collapse_des">
                                                Spesifikasi
                                                <i class="fa-icon fa fa-angle-up"></i>
                                            </h4>
                                        </div>
                                        <div id="collapse_spec" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="collapse_spec">
                                            <div class="panel-body">
                                                <table class="table table-specification" style="width: 100%;">
                                                    <tbody>
                                                        <?php foreach ($features->result() as $feature) { ?>
                                                            <tr>
                                                                <td width="30%"><b><?php echo $feature->name; ?></b></td>
                                                                <td>
                                                                    <?php
                                                                    if ($feature->type == 'i' || $feature->type == 't') {
                                                                        echo nl2br($feature->value);
                                                                    } else {
                                                                        if ($feature->type == 's') {
                                                                            echo $feature->variant;
                                                                        } elseif ($feature->type == 'c') {
                                                                            $variants = $this->products->get_product_feature_variants($feature->product, $feature->id);
                                                                            if ($variants) {
                                                                                echo '<ul style="padding:0; margin: 0 0 0 16px">';
                                                                                foreach ($variants->result() as $variant)
                                                                                    echo '<li>' . $variant->value . '</li>';
                                                                                echo '</ul>';
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="information_content panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading_review">
                                        <h4 class="panel-title" data-toggle="collapse" href="#collapse_review" aria-expanded="true" aria-controls="collapse_review">Review<i class="fa-icon fa fa-angle-up"></i></h4>
                                    </div>
                                    <div id="collapse_review" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading_review">
                                        <div class="panel-body">
                                            <div id="customer_review">
                                                <div class="preview_content">
                                                    <div id="shopify-product-reviews">
                                                        <div class="spr-container">
                                                            <div class="spr-header">
                                                                <div class="spr-summary">
                                                                    <?php if ($rating->review) { ?>
                                                                        <span class="spr-starrating spr-summary-starrating">
                                                                            <?php for ($i = 1; $i <= $rating->rating; $i++) { ?>
                                                                                <i class="spr-icon spr-icon-star"></i>
                                                                            <?php } ?>
                                                                            <?php for ($i = $rating->rating + 1; $i <= 5; $i++) { ?>
                                                                                <i class="spr-icon spr-icon-star-empty"></i>
                                                                            <?php } ?>
                                                                        </span>
                                                                        <span class="spr-summary-caption">
                                                                            <span class="spr-summary-actions-togglereviews">Berdasarkan <?php echo number($rating->review); ?> ulasan</span>
                                                                        </span>
                                                                    <?php } else { ?>
                                                                        <span class="spr-summary-caption">
                                                                            <span class="spr-summary-actions-togglereviews">Tidak ada ulasan</span>
                                                                        </span>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                            <div class="spr-content">
                                                                <?php if ($reviews) { ?>
                                                                    <div class="spr-reviews">
                                                                        <?php foreach ($reviews->result() as $review) { ?>
                                                                            <div class="spr-review">
                                                                                <div class="spr-review-header">
                                                                                    <span class="spr-starratings spr-review-header-starratings">
                                                                                        <?php for ($i = 1; $i <= $review->rating; $i++) { ?>
                                                                                            <i class="spr-icon spr-icon-star"></i>
                                                                                        <?php } ?>
                                                                                        <?php for ($i = $review->rating + 1; $i <= 5; $i++) { ?>
                                                                                            <i class="spr-icon spr-icon-star-empty"></i>
                                                                                        <?php } ?>
                                                                                    </span>
                                                                                    <span class="spr-review-header-byline"><strong><?php echo $review->name; ?></strong> on <strong><?php echo get_date($review->date_added); ?></strong></span>
                                                                                </div>
                                                                                <div class="spr-review-content">
                                                                                    <p class="spr-review-content-body"><?php echo $review->description; ?></p>
                                                                                </div>
                                                                            </div>
                                                                        <?php } ?>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <?php if ($related_product) { ?>
                            <div id="related-product" class="shopify-section index-section index-section-proban">
                                <div>
                                    <section class="home_proban_layout">
                                        <div class="home_proban_wrapper">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="home_proban_inner">
                                                        <div class="page-title" style="border-color: #4C4A3F">
                                                            <div class="group_title">
                                                                <h2>Produk Terkait</h2>
                                                            </div>
                                                            <!-- <a href="">Lihat semua</a> -->
                                                        </div>
                                                        <div class="home_proban_content" id="promo-product">
                                                                <?php
                                                                    foreach ($related_product->result() as $product) {
                                                                    $sale_on = FALSE;
                                                                    if($product->promo == 1) {
                                                                        if ($product->merchant == 0) {
                                                                            $merchant = $this->session->userdata('list_merchant')[0];
                                                                            $price_group = $this->main->get('product_price', array('product' => $product->id, 'merchant_group' => $merchant['group']));
                                                                            if ($price_group) {
                                                                                $product->price = $price_group->price;
                                                                            }
                                                                        }
                                                                        $promo_data = json_decode($product->promo_data,true);
                                                                        if($promo_data['type'] == 'P') {
                                                                            $disc_price = round(($product->price * $promo_data['discount']) / 100);
                                                                            $label_disc = $promo_data['discount'].'% off';
                                                                        } else {
                                                                            $disc_price = $promo_data['discount'];
                                                                            $label_disc = 'SALE';
                                                                        }
                                                                        $end_price= $product->price - $disc_price;
                                                                        $today = date('Y-m-d');
                                                                        $today=date('Y-m-d', strtotime($today));
                                                                        $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                                                        $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                
                                                                        if (($today >= $DateBegin) && ($today <= $DateEnd)){
                                                                            $sale_on = TRUE;
                                                                        }
                                                                    }
                                                                ?>
                                                                <div class="col-sm-2 proban_product">
                                                                    <div class="row-container product list-unstyled clearfix">
                                                                    <?php if($sale_on) {?>
                                                                        <span class="product-discount-label" style="position:absolute;top:5px;right:10px;z-index:1;background-color:red;color:white;padding:5px 10px;"><?php echo $label_disc;?></span>
                                                                    <?php } ?>
                                                                        <div class="row-left">
                                                                            <a href="<?php echo seo_url('catalog/products/view/' . $product->id); ?>" class="hoverBorder container_item">
                                                                                <div class="hoverBorderWrapper">
                                                                                    <img src="<?php echo ($product->image) ? site_url('files/images/' . $product->image) : site_url('assets/frontend/images/noimage.jpg'); ?>" class="img-responsive front" alt="<?php echo $product->name; ?>">
                                                                                </div>
                                                                            </a>
                                                                        </div>

                                                                        <div class="row-right animMix">
                                                                            <div class="product-title"><a class="title-5" href="<?php echo seo_url('catalog/products/view/' . $product->id); ?>"><?php echo $product->name; ?></a></div>
                                                                            <div class="rating-star">
                                                                                <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                                                </span>
                                                                            </div>
                                                                            <div class="product-price">
                                                                            <?php if($sale_on) {?>
                                                                                <span class="money" style="text-decoration: line-through;font-size:9px;color:#8b8f8b"><?php echo rupiah($product->price); ?></span>
                                                                                <span class="money"><?php echo rupiah($end_price); ?></span>
                                                                            <?php } else { ?>
                                                                                <span class="price_sale"><span class="money"><?php echo rupiah($product->price); ?></span></span>
                                                                            <?php } ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                    }
                                                                ?>
                                                                
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <?php
                            }
                            ?>
                            <?php if($show_last_seen_product){?>
                            <div id="lastseen-product" class="shopify-section index-section index-section-proban">
                                <div>
                                    <section class="home_proban_layout">
                                        <div class="home_proban_wrapper">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="home_proban_inner">
                                                        <div class="page-title" style="border-color: #4C4A3F">
                                                            <div class="group_title">
                                                                <h2>Produk Terakhir diliat</h2>
                                                            </div>
                                                            <!-- <a href="">Lihat semua</a> -->
                                                        </div>
                                                        <div class="home_proban_content" id="promo-product">
                                                                <?php
                                                                    foreach ($last_seen_product->result() as $product) {
                                                                    $sale_on = FALSE;
                                                                    if($product->promo == 1) {
                                                                        if ($product->merchant == 0) {
                                                                            $merchant = $this->session->userdata('list_merchant')[0];
                                                                            $price_group = $this->main->get('product_price', array('product' => $product->id, 'merchant_group' => $merchant['group']));
                                                                            if ($price_group) {
                                                                                $product->price = $price_group->price;
                                                                            }
                                                                        }
                                                                        $promo_data = json_decode($product->promo_data,true);
                                                                        if($promo_data['type'] == 'P') {
                                                                            $disc_price = round(($product->price * $promo_data['discount']) / 100);
                                                                            $label_disc = $promo_data['discount'].'% off';
                                                                        } else {
                                                                            $disc_price = $promo_data['discount'];
                                                                            $label_disc = 'SALE';
                                                                        }
                                                                        $end_price= $product->price - $disc_price;
                                                                        $today = date('Y-m-d');
                                                                        $today=date('Y-m-d', strtotime($today));
                                                                        $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                                                        $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                
                                                                        if (($today >= $DateBegin) && ($today <= $DateEnd)){
                                                                            $sale_on = TRUE;
                                                                        }
                                                                    }
                                                                ?>
                                                                <div class="col-sm-2 proban_product">
                                                                    <div class="row-container product list-unstyled clearfix">
                                                                    <?php if($sale_on) {?>
                                                                        <span class="product-discount-label" style="position:absolute;top:5px;right:10px;z-index:1;background-color:red;color:white;padding:5px 10px;"><?php echo $label_disc;?></span>
                                                                    <?php } ?>
                                                                        <div class="row-left">
                                                                            <a href="<?php echo seo_url('catalog/products/view/' . $product->id); ?>" class="hoverBorder container_item">
                                                                                <div class="hoverBorderWrapper">
                                                                                    <img src="<?php echo ($product->image) ? site_url('files/images/' . $product->image) : site_url('assets/frontend/images/noimage.jpg'); ?>" class="img-responsive front" alt="<?php echo $product->name; ?>">
                                                                                </div>
                                                                            </a>
                                                                        </div>

                                                                        <div class="row-right animMix">
                                                                            <div class="product-title"><a class="title-5" href="<?php echo seo_url('catalog/products/view/' . $product->id); ?>"><?php echo $product->name; ?></a></div>
                                                                            <div class="rating-star">
                                                                                <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                                                </span>
                                                                            </div>
                                                                            <div class="product-price">
                                                                            <?php if($sale_on) {?>
                                                                                <span class="money" style="text-decoration: line-through;font-size:9px;color:#8b8f8b"><?php echo rupiah($product->price); ?></span>
                                                                                <span class="money"><?php echo rupiah($end_price); ?></span>
                                                                            <?php } else { ?>
                                                                                <span class="price_sale"><span class="money"><?php echo rupiah($product->price); ?></span></span>
                                                                            <?php } ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                    }
                                                                ?>
                                                                
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        <?php } ?>    

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
