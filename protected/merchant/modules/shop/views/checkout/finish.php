<section class="collection-heading heading-content ">
    <div class="container">
        <div class="cart-step">
            <ul>
                <li class="active">1. Belanjaan Saya</li>
                <li class="active">2. Alamat</li>
                <li class="active">3. Pengiriman</li>
                <li class="active">4. Pembayaran</li>
                <li class="active">5. Selesai</li>
            </ul>
        </div>
    </div>
</section>
<section class="cart-content checkout">
    <div class="container">
        <div class="finish">
            <div class="finish-header">
                <h2>Hai <?php echo $customer->fullname; ?>, terimakasih telah berbelanja di TokoManaMana.com</h2>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="order-info-label">NO. TAGIHAN</div>
                        <div class="order-info-content"><?php echo $order->code; ?></div>
                    </div>
                    <div class="col-xs-6 text-right">
                        <div class="order-info-label">STATUS</div>
                        <div class="order-info-content">
                            <?php
                            if ($order->payment_status == 'Paid') {
                                echo 'SUDAH DIBAYAR';
                            } elseif (in_array($order->payment_status,['Failed','Cancel'])) {
                                echo 'GAGAL';
                            } elseif ($order->payment_status=='Expired') {
                                echo 'KADALUARSA';
                            } else {
                                echo'BELUM DIBAYAR';
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php // if ($order->payment_method != 'credit_card') {  ?>
            <hr>
            <div class="finish-subheader">
                <?php
                if ($order->payment_status == 'Pending') {
                    $payment_method = $this->main->get('payment_methods', array('name' => $order->payment_method));
                    $payment = json_decode($order->payment_to);
                    if (in_array($order->payment_method, ['transfer', 'bca_va', 'mandiri_va', 'bni_va', 'bri_va'])) {
                        echo '<div class="text-center">Silahkan lakukan transfer sebesar:</div>
                            <div class="finish-amount">' . rupiah($order->total) . '</div>
                            <div class="text-center">ke rekening ' . ($order->payment_method != 'transfer' ? 'virtual' : '') . ' berikut ini:</div>';
                        if ($order->payment_method != 'transfer') {
                            echo '<div class="detail-bank">
                            <img src="' . base_url('../files/images/' . $payment_method->image) . '">
                            <div>' . $payment->account_name . '</div>
                            <strong>' . $payment->account_number . '</strong>
                            <div>' . $payment->account_holder_name . '</div>
                            </div>';
                        } else {
                            echo '<div class="list-bank">';
                            foreach ($payment as $pym) {
                                echo '<div class="detail-bank">
                                <img src="' . base_url('../files/images/' . $pym->image) . '">
                                <div><strong>' . $pym->account_number . '</strong></div>
                                <div>' . $pym->account_name . '</div>
                                <div>' . $pym->name . '</div>
                                <div>' . $pym->branch . '</div>
                                </div>';
                            }
                            echo '</div>';
                            echo '<div class="text-center" style="margin-top: 15px;"><button type="button" id="payment-confirmation" class="btn btn-primary">Konfirmasi Pembayaran</button></div>';
                        }
                    } elseif (in_array($order->payment_method, ['alfamart', 'kredivo'])) {
                        echo '<div class="text-center">Silahkan lakukan pembayaran sebesar:</div>
                            <div class="finish-amount">' . rupiah($order->total) . '</div>';
                        if ($order->payment_method == 'alfamart') {
                            echo '<div class="text-center">di Alfamart/Alfamidi terdekat</div>';
                            echo '<div class="detail-bank">
                            <img src="' . base_url('../files/images/' . $payment_method->image) . '">
                            <div>Kode Pembayaran:</div>
                            <strong>' . $payment->account_number . '</strong>
                            </div>';
                        } elseif ($order->payment_method == 'kredivo') {
                            echo '<div class="text-center">melalui Kredivo</div>';
                            echo '<div class="detail-bank">
                            <div class="text-center"><a class="btn" href="' . $payment->payment_url . '">BAYAR SEKARANG</a></div>
                            </div>';
                        }
                    }
                }
                if ($order->payment_status == 'Pending') {
                    echo '<div class="text-center">Transaksi otomatis batal jika pembayaran tidak dilakukan sampai <b>' . get_date_time($order->due_date) . '</b></div>';
                } elseif ($order->payment_status == 'Paid') {
                    echo '<div class="text-center"><div class="alert alert-success">Terima Kasih transaksi anda sudah berhasil</div></div>';
                } else {
                    echo '<div class="text-center"><div class="alert alert-danger">Mohon maaf transaksi anda gagal</div></div>';
                }
                ?>
            </div>
            <?php // }   ?>
            <div class="finish-detail">
                <div class="text-center text-bold">Rincian Belanja Anda</div>
                <div class="items">
                    <?php
                    $products = $this->main->gets('order_product_merchant', array('order' => $order->id));
                    foreach ($products->result() as $product) {
                        ?>
                        <div class="item">
                            <div class="item-left"><?php echo $product->name . ' ' . rupiah($product->price) . ' x ' . $product->quantity; ?></div>
                            <div class="item-right"><?php echo rupiah($product->total); ?></div>
                        </div>
                    <?php } ?>
                    <div class="item">
                        <div class="item-left">Ongkos Kirim</div>
                        <div class="item-right"><?php echo rupiah($order->shipping_cost); ?></div>
                    </div>
                </div>
                <div class="item">
                    <div class="item-left">Total Transaksi</div>
                    <div class="item-right"><?php echo rupiah($order->total); ?></div>
                </div>
            </div>
            <div class="finish-footer">
                <a class="btn btn-primary" href="<?php echo site_url('shop/member/order_detail/' . encode($order->id)); ?>">Lihat Tagihan</a>
            </div>
        </div>
    </div>
</section>
<?php if ($order->payment_method == 'transfer') { ?>
    <div id="modal-payment" class="modal" role="dialog" aria-hidden="true" tabindex="-1" style="display: none;">
        <div class="modal-dialog fadeIn animated">
            <div class="modal-content">
                <form action="#" class="form-horizontal">
                    <input type="hidden" name="id" value="<?php echo encode($order->id); ?>">
                    <div class="modal-header">
                        <h1 class="modal-title">Konfirmasi Pembayaran<br><small>Tagihan <?php echo $order->code; ?></small></h1>
                    </div>
                    <div class="modal-body">
                        <div id="message"></div>
                        <div class="form-group row">
                            <label class="control-label col-sm-3">Transfer ke</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="to" required="">
                                    <?php foreach ($payment as $id_bank => $pym) { ?>
                                        <option value="<?php echo $id_bank; ?>"><?php echo $pym->name . ' (' . $pym->account_number . ')'; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-3">Sebesar</label>
                            <div class="col-sm-9">
                                <input name="amount" type="text" class="form-control" required="" value="<?php echo $order->total; ?>" placeholder="Total transfer, input hanya angka">
                                Total Transaksi: <b><?php echo rupiah($order->total); ?></b>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-3">Dari Rekening Atas Nama</label>
                            <div class="col-sm-9">
                                <input name="from" type="text" class="form-control" required="" placeholder="Nama yang tertera di rekening pengirim">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-3">Catatan</label>
                            <div class="col-sm-9">
                                <textarea name="note" type="text" class="form-control" placeholder="Catatan khusus yang perlu kami perhatikan"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn" data-dismiss="modal" style="background-color: #FFF; color: #000;">Batal</button>
                        <button type="button" onclick="submit_payment();" class="btn">Konfirmasi</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>