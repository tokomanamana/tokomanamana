<section class="collection-heading heading-content ">
    <div class="container">
        <div class="cart-step">
            <ul>
                <li class="active">1. Belanjaan Saya</li>
                <li class="active">2. Alamat</li>
                <li>3. Pengiriman</li>
                <li>4. Pembayaran</li>
                <li>5. Selesai</li>
            </ul>
        </div>
        <div class="cart-title">2. Alamat</div>
    </div>
</section>
<section class="cart-content checkout">
    <div class="container">
        <?php if (isset($message)) { ?>
            <div class="alert alert-danger fade in alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                <?php echo $message; ?>
            </div>
        <?php } ?>
        <form class="form" action="" method="post">
            <div class="address">
                <div class="left">
                    <div class="header">
                        <div class="title">Pilih Alamat Pengiriman</div>
                        <!-- <button class="btn btn-default" type="button" id="add-address">Tambah Alamat</button> -->
                    </div>
                    <div class="body">
                        <div id="address-list">
                            <?php if ($list_address): foreach ($list_address->result() as $address): ?>
                                    <div class="payment-method-block">
                                        <div class="payment-method-detail">
                                            <input type="radio" value="<?php echo $address->id; ?>" onclick="selectPayment(this);" name="address" required id="address-<?php echo $address->id; ?>">
                                            <label for="address-<?php echo $address->id; ?>">
                                                <div class="payment-method-title"><?php echo $address->name; ?> - Utama</div>
                                                <div class="payment-method-subtitle"><?php echo $address->address . '<br>' . $address->district_name . ', ' . $address->city_name . ', ' . $address->postcode . '<br>' . $address->province_name; ?></div>
                                            </label>
                                        </div>
                                    </div>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
                <div class="right cart-summary">
                    <div class="header">
                        <div class="title">Rekap Belanjaan</div>
                    </div>
                    <div class="body">
                        <?php
                        foreach ($this->cart->contents() as $product) {
                            $id = explode('-', $product['id']);
                            ?>
                            <div class="cart-item">
                                <div class="cart-item-left">
                                    <div class="item-image">
                                        <a href="<?php echo seo_url('catalog/products/view/' . $id[0]); ?>">
                                            <img src="<?php echo get_image($product['image']); ?>">
                                        </a>
                                    </div>
                                </div>
                                <div class="cart-item-right">
                                    <div class="item-name"><a href="<?php echo seo_url('catalog/products/view/' . $id[0]); ?>"><?php echo $product['name']; ?></a></div>
                                    <div class="cart-price">
                                        <span class="price"><?php echo rupiah($product['price']); ?> <span class="x"> x <?php echo $product['qty']; ?></span></span>
                                    </div>
    <!--                                        <div class="cart-title"><a href="<?php echo seo_url('catalog/products/view/' . $id[0]); ?>"><?php echo $product['name']; ?></a></div>
                                    <div class="cart-price"><?php echo number($product['price']); ?><span class="x"> x <?php echo $product['qty']; ?></span></div>-->
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="cart-footer">
                <div class="action">
                    <a href="<?php echo site_url('cart'); ?>"><i class="fa fa-arrow-left"></i> Kembali ke Cart</a>
                    <button type="submit" class="btn">LANJUTKAN <i class="fa fa-arrow-right"></i></button>
                </div>
            </div>
        </form>
    </div>
</section>
<div id="modal-address" class="modal" role="dialog" aria-hidden="true" tabindex="-1" data-width="800" style="display: none;">
    <div class="modal-dialog fadeIn animated">
        <div class="modal-content">
            <form action="#">
                <div class="modal-header">
                    <h1>Tambah Alamat</h1>
                </div>
                <div class="modal-body">
                    <div id="message"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Simpan Alamat sebagai</label>
                                <input name="title" type="text" class="form-control" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('checkout_shipping_field_name'); ?></label>
                                <input name="name" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('checkout_shipping_field_phone'); ?></label>
                                <input name="phone" type="text" class="form-control" onkeypress="return isNumberKey(event)">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('checkout_shipping_field_postcode'); ?></label>
                                <input name="postcode" type="text" class="form-control" onkeypress="return isNumberKey(event)">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('checkout_shipping_field_province'); ?></label>
                                <select name="province" id="province" class="form-control">
                                    <option value="">Pilih provinsi</option>
                                    <?php if ($provincies) { ?>
                                        <?php foreach ($provincies->result() as $province) { ?>
                                            <option value="<?php echo $province->id; ?>"><?php echo $province->name; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('checkout_shipping_field_city'); ?></label>
                                <select name="city" id="city" class="form-control">
                                    <option value="">Pilih kota</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('checkout_shipping_field_district'); ?></label>
                                <select name="district" id="district" class="form-control">
                                    <option value="">Pilih kecamatan</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label"><?php echo lang('checkout_shipping_field_address'); ?></label>
                                <input name="address" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" style="background-color: #FFF; color: #000;">Batal</button>
                    <button type="button" onclick="submit_address();" class="btn">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>