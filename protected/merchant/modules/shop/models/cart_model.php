<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cart_model extends CI_Model {

    function get_cart_products($auth) {
        $this->db->select('c.*, p.id, p.code, p.name, p.price_old, p.price, p.discount, p.weight, p.short_description, pi.image')
                ->join('products p', 'p.id = c.product', 'left')
                ->join('product_image pi', 'p.id = pi.product AND primary = 1', 'left');
        $query = $this->db->get_where('cart c', array('auth' => $auth));
        return ($query->num_rows() > 0) ? $query : false;
    }

    function get_total_cart() {
        $this->db->select('SUM(c.quantity) total');
        $query = $this->db->get_where('cart c', array('auth' => $this->data['user']->id))->row();
        return ($query->total > 0) ? number($query->total) : 0;
    }

    function getTotalWeightCart() {
        $this->db->select('ROUND(SUM(p.weight),2) total')
                ->join('products p', 'p.id = c.product', 'left');
        $query = $this->db->get_where('cart c', array('auth' => $this->data['user']->id))->row();
        return ($query->total > 0) ? $query->total : 0;
    }

    function get_product($id1, $id2, $id3) {
        $this->db->select('p.id, p.name, p.short_description, p.code, pi.image')
                ->join('product_image pi', 'pi.product = p.id', 'left')
                ->where('p.id', $id1);
        if ($id2) {
            $this->db->select('p.weight, p.price_old, p.price, p.discount, p.quantity, p.merchant, m.name merchant_name')
                    ->join('merchants m', 'm.id = p.merchant', 'left')
                    ->where('p.merchant', $id2);
        } else {
            $this->db->select('pm.weight, pm.price_old, pm.price, pm.discount, pm.quantity, pm.merchant, m.name merchant_name')
                    ->join('product_merchant pm', 'p.id = pm.product', 'left')
                    ->join('merchants m', 'm.id = pm.merchant', 'left')
                    ->where('p.merchant', 0)
                    ->where('pm.merchant', $id3);
        }
        $query = $this->db->get('products p');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

}
