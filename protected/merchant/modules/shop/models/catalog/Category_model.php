<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Category_model extends CI_Model {

    function get_categories($parent = 0) {
        $this->db->select('id, name, image, icon')
                ->where('parent', $parent)
                ->order_by('sort_order')
                ->where('active', 1);
        $query = $this->db->get('categories');
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    function getCategory($id) {
        $this->db->select("c.*, GROUP_CONCAT(cp.path ORDER BY cp.level SEPARATOR ',') path", FALSE)
                ->join('category_path cp', 'cp.category = c.id', 'left')
                ->where('active', 1)
                ->where('c.id', $id);
        $query = $this->db->get('categories c');
        return ($query->num_rows() > 0) ? $query->row() : FALSE;
    }

    function getProducts($category, $merchant, $merchant_group, $sort = 'popular', $limit = 20, $start = 0) {
        $query_order = 'ORDER BY ';
        switch ($sort) {
            default:
            case 'popular':
                $query_order .= 'p.viewed DESC';
                break;
            case 'newest' :
                $query_order .= 'p.date_added DESC';
                break;
            case 'lowprice' :
                $query_order .= 'p.price ASC';
                break;
            case 'highprice' :
                $query_order .= 'p.price DESC';
                break;
            case 'nameAZ' :
                $query_order .= 'p.name ASC';
                break;
            case 'nameZA' :
                $query_order .= 'p.name DESC';
                break;
        }
        $query_limit = '';
        if ($limit) {
            $query_limit = 'LIMIT ' . $start . ',' . $limit;
        }
        $merchant = implode(',', $merchant);
        return $this->db->query("SELECT * FROM (
                    SELECT p.id, p.merchant, p.name, p.short_description, p.code, p.category, p.brand, IFNULL(pp.price, p.price) price, pi.image, pp.id ppid, p.viewed, p.date_added, p.quantity,p.promo,p.promo_data
                    FROM products p
                    LEFT JOIN product_price pp ON pp.product = p.id AND pp.merchant_group = $merchant_group
                    LEFT JOIN product_image pi ON pi.product = p.id AND pi.primary = 1
                    WHERE p.merchant = 0 AND p.status = 1
                    UNION
                    SELECT p.id, p.merchant, p.name, p.short_description, p.code, p.category, p.brand, p.price, pi.image, 0, p.viewed, p.date_added, p.quantity,p.promo,p.promo_data
                    FROM products p
                    LEFT JOIN product_image pi ON pi.product = p.id AND pi.primary = 1
                    WHERE merchant IN (SELECT id FROM merchants WHERE id IN ($merchant) AND status = 1) AND p.status = 1
                    ) p
                 WHERE p.category IN (SELECT category FROM category_path WHERE path = $category) $query_order $query_limit");
    }

    function get_category_path($category) {
        $this->db->select("c.id, c.name", FALSE)
                ->join('categories c', 'pc.path = c.id', 'left')
                ->where('category', $category)
                ->order_by('level', 'ASC');
        $query = $this->db->get('category_path pc');
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

}
