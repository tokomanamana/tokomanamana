<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Search_model extends CI_Model {

    function getProducts($word, $merchant, $merchant_group, $sort = 'popular', $limit = 20, $start = 0) {
        $query_order = 'ORDER BY ';
        switch ($sort) {
            default:
            case 'popular':
                $query_order .= 'p.viewed DESC';
                break;
            case 'newest' :
                $query_order .= 'p.date_added DESC';
                break;
            case 'lowprice' :
                $query_order .= 'p.price ASC';
                break;
            case 'highprice' :
                $query_order .= 'p.price DESC';
                break;
            case 'nameAZ' :
                $query_order .= 'p.name ASC';
                break;
            case 'nameZA' :
                $query_order .= 'p.name DESC';
                break;
        }
        $query_limit = '';
        if ($limit) {
            $query_limit = 'LIMIT ' . $start . ',' . $limit;
        }
        $merchant = implode(',', $merchant);
        $where = '';
        return $this->db->query("SELECT * FROM (
                    SELECT p.id, p.merchant, p.name, p.short_description, p.code, p.category, p.brand, IFNULL(pp.price, p.price) price, pi.image, pp.id ppid, p.viewed, p.date_added, p.meta_keyword, c.name cat_name
                    FROM products p
                    LEFT JOIN product_price pp ON pp.product = p.id AND pp.merchant_group = $merchant_group
                    LEFT JOIN product_image pi ON pi.product = p.id AND pi.primary = 1
                    LEFT JOIN categories c ON c.id = p.category
                    WHERE p.merchant = 0 AND p.status = 1
                    UNION
                    SELECT p.id, p.merchant, p.name, p.short_description, p.code, p.category, p.brand, p.price, pi.image, 0, p.viewed, p.date_added, p.meta_keyword, c.name cat_name
                    FROM products p
                    LEFT JOIN product_image pi ON pi.product = p.id AND pi.primary = 1
                    LEFT JOIN categories c ON c.id = p.category
                    WHERE merchant IN (SELECT id FROM merchants WHERE id IN ($merchant) AND status = 1)  AND p.status = 1
                    ) p
                 WHERE (p.code LIKE '%$word%' OR p.name LIKE '%$word%' OR p.meta_keyword REGEXP '$word' OR cat_name LIKE '%$word%') $where $query_order $query_limit");
    }

}
