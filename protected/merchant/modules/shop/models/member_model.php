<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Member_model extends CI_Model {

    function getOrders($customer, $limit = 10, $start = 0) {
        $this->db->select("o.*, sd.name shipping_district_name, sc.name shipping_city_name, sp.name shipping_province_name", FALSE)
                ->join('cities sc', 'sc.id = o.shipping_city', 'left')
                ->join('districts sd', 'sd.id = o.shipping_district', 'left')
                ->join('provincies sp', 'sp.id = o.shipping_province', 'left')
                ->order_by('o.id DESC')
                ->where('o.customer', $customer);
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get('orders_merchant o');
        return $query;
    }

    function get_order($id) {
        $this->db->select("o.*, sd.name shipping_district_name, sc.name shipping_city_name, sp.name shipping_province_name", FALSE)
                ->join('cities sc', 'sc.id = o.shipping_city', 'left')
                ->join('districts sd', 'sd.id = o.shipping_district', 'left')
                ->join('provincies sp', 'sp.id = o.shipping_province', 'left')
                ->order_by('o.id DESC')
                ->where('o.id', $id);
        $query = $this->db->get('orders_merchant o');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    function get_order_invoice($order) {
        $this->db->select('oi.*, m.name merchant_name, os.name order_status_name')
                ->join('merchants m', 'm.id = oi.merchant', 'left')
                ->join('setting_order_status os', 'os.id = oi.order_status', 'left')
                ->where('order', $order);
        return $this->db->get('order_invoice_merchant oi');
    }

    function get_order_products($invoice) {
        $this->db->select('op.*, pi.image')
                ->join('product_image pi', 'pi.product = op.product AND pi.primary = 1', 'left')
                ->where('invoice', $invoice);
        return $this->db->get('order_product_merchant op');
    }

    function getOrderProducts($order) {
        $this->db->select('op.*, pi.image')
                ->join('product_image pi', 'pi.product = op.product AND pi.primary = 1', 'left');
        $query = $this->db->get_where('order_product_merchant op', array('op.order' => $order));
        return ($query->num_rows() > 0) ? $query : false;
    }

    function get_order_histories($invoice) {
        $this->db->select('oh.*, os.name, os.description, os.color')
                ->join('setting_order_status os', 'os.id = oh.order_status', 'left')
                ->order_by('oh.id DESC');
        $query = $this->db->get_where('order_history_merchant oh', array('oh.invoice' => $invoice));
        return ($query->num_rows() > 0) ? $query : false;
    }

    function getTotalQuantityOrderProduct($order) {
        $this->db->select('SUM(quantity) total');
        $query = $this->db->get_where('order_product_merchant', array('order' => $order));
        return ($query->row()->total > 0) ? $query->row()->total : 0;
    }

    function getProfile($customer) {
        $this->db->select('c.address, c.name, c.phone, a.address, a.province, a.city, a.district, a.postcode, au.email')
                ->join('address a', 'c.address = a.id', 'left')
                ->join('auth_users au', 'c.auth = au.id', 'left');
        $query = $this->db->get_where('customers c', array('c.id' => $customer));
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    function get_total_shipping_cost($order) {
        $this->db->select('SUM(shipping_cost) shipping_cost')
                ->where('order', $order);
        return $this->db->get('order_invoice_merchant')->row()->shipping_cost;
    }

    function get_subtotal($order) {
        $this->db->select('SUM(subtotal) subtotal')
                ->where('order', $order);
        return $this->db->get('order_invoice_merchant')->row()->subtotal;
    }

    function addresses($customer) {
        $this->db->select('ca.*, p.name province_name, c.name city_name, d.name district_name')
                ->join('provincies p', 'p.id = ca.province')
                ->join('cities c', 'c.id = ca.city')
                ->join('districts d', 'd.id = ca.district')
                ->where('ca.customer', $customer);
        return $this->db->get('customer_address ca');
    }

    function get_orders_review($customer, $limit = 10, $start = 0) {
        $status = settings('order_finish_status');
        $this->db->select("o.id, o.code, o.date_added, GROUP_CONCAT(oi.id SEPARATOR ',') invoice")
                ->join('order_invoice oi', 'oi.order = o.id', 'left')
                ->where('oi.order_status', $status)
                ->where('o.customer', $customer)
                ->group_by('o.id')
                ->order_by('o.id', 'desc');
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        return $this->db->get('orders o');
    }

    function get_products_review($order, $customer, $invoice = array()) {
        $status = settings('order_finish_status');
        $this->db->select('op.id, op.product, op.name, op.code, pi.image, pr.rating_speed, pr.rating_service, pr.rating_accuracy, pr.description')
                ->join('product_image pi', 'pi.product = op.product AND pi.primary = 1', 'left')
                ->join('product_review pr', 'pr.product = op.product AND pr.customer = ' . $customer, 'left')
                ->where('op.order', $order)
                ->where_in('op.invoice', $invoice)
                ->order_by('op.id', 'desc');
        return $this->db->get('order_product op');
    }

    function merchant($id) {
        $this->db->select('m.*, p.name province, c.name city, d.name district')
                ->join('provincies p', 'p.id = m.province', 'left')
                ->join('cities c', 'c.id = m.city', 'left')
                ->join('districts d', 'd.id = m.district', 'left')
                ->where('m.id', $id);
        return $this->db->get('merchants m')->row();
    }

    function get_invoice_for_notification($customer) {
        $status = [settings('order_process_status'),settings('order_shipped_status'),settings('order_ready_pickup_status'),settings('order_delivered_status'),settings('order_complete_pickup_status')];
        $this->db->select("GROUP_CONCAT(code,'-',oi.order ORDER BY date_modified DESC SEPARATOR ',') invoice, order_status status")
                ->where('oi.customer', $customer)
                ->where_in('oi.order_status', $status)
                ->group_by('oi.order_status');
        $query = $this->db->get('order_invoice_merchant oi');
        return ($query->num_rows() > 0) ? $query : false;
    }
    
    function get_wishlist($customer, $merchant_group, $limit = 20, $start = 0){
        $this->db->select('p.id, p.name, IFNULL(pp.price, p.price) price, pi.image, p.date_added, w.id wishlist')
                ->join('products p','p.id = w.product','left')
                ->join('product_price pp','pp.product = p.id AND pp.merchant_group = '. $merchant_group,'left')
                ->join('product_image pi','pi.product = p.id AND pi.primary = 1','left')
                ->where('w.customer', $customer)
                ->where('p.status', 1)
                ->order_by('w.date_added desc')
                ->limit($limit, $start);
        return $this->db->get('wishlist w');
    }

}
