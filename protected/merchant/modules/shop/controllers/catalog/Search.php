<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->lang->load('shop/catalog', settings('language'));
        $this->load->model('catalog/search_model', 'search');
        $this->load->model('catalog/product_model', 'product');
        $this->load->library('pagination');
        $this->load->library('breadcrumb');
    }

    public function view() {
        $word = $this->input->get('word');
        
        $merchants = $this->session->userdata('list_merchant');
        $merchant_group = $merchants[0]['group'];
        foreach($merchants as $m){
            $list[] = $m['id'];
        }
        
        //pagination
        $config['base_url'] = site_url('search');
        $config['total_rows'] = $this->search->getProducts($word, $list, $merchant_group, '', '', '')->num_rows();
        $config['per_page'] = ($this->input->get('show')) ? $this->input->get('show') : 20;
        $this->pagination->initialize($config);
        $start = ($this->input->get('page')) ? ($this->input->get('page') - 1) * $config['per_page'] : 0;

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Cari "' . $word . '"', site_url('search'));
        $this->data['products'] = $this->search->getProducts($word, $list, $merchant_group, (($this->input->get('sort')) ? $this->input->get('sort') : 'popular'), $config['per_page'], $start);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['meta_description'] = 'Cari produk ' . $word . ' harga termurah.';
        $this->data['total_products'] = $config['total_rows'];
        $this->data['start'] = $start + 1;
        $this->data['to'] = ($this->data['total_products'] < $config['per_page']) ? $this->data['total_products'] : ($start * $config['per_page']);
        $this->output->set_title('Pencarian produk ' . $word . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->view('search', $this->data);
    }

    public function suggest()
    {
        $keyword = $this->input->get('keyword');
        $this->db->select('p.id,p.name');
        $this->db->where("p.status",'1');
            $this->db->group_start();
            $this->db->where("p.name LIKE '%$keyword%'");
            $this->db->or_where("p.meta_keyword REGEXP '$keyword'");
            $this->db->or_where("c.name LIKE '%$keyword%'");
            $this->db->group_end();
        $this->db->limit(5);
        $this->db->join('categories c','c.id = p.category','left');
        $this->db->order_by('p.viewed', 'desc');
        $search = $this->db->get('products p')->result();
        //$search = $this->products->as_dropdown('name')->order_by('viewed', 'desc')->limit(5)->get_all(['name LIKE' => "%$keyword%"]);
        //var_dump($this->db->last_query());exit();
        $arr = [];
        foreach ($search as $key => $value) {
            $arr[] = [
                'url' => seo_url('shop/catalog/products/view/' . $value->id),
                'name' => $value->name
            ];
        }
        echo json_encode($arr);
    }
}
