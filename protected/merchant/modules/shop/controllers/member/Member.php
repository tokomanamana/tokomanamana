<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Member extends CI_Controller {

    public function __construct() {
        parent::__construct();

        //$customer = $this->session->userdata('user');
        $merchant_user = $this->session->userdata('merchant_user');
        //print_r($customer);
        $this->load->library('form_validation');
        $this->load->model('member_model', 'member');
        $this->load->model('orders');
        $this->load->library('breadcrumb');
        $this->load->library('pagination');
        // if (!$this->input->is_ajax_request()) {
        //     if (!$this->ion_auth->logged_in()) {
        //         redirect('member/login?back=' . uri_string());
        //     }
        //     $this->load->library('breadcrumb');
        //     $this->load->library('pagination');

             $this->data['notification_invoice'] = $this->member->get_invoice_for_notification($merchant_user->merchant);
        // }
        $this->load->language('shop/member', settings('language'));

        //$this->data['notification_invoice'] = array();
    }

    public function history($tab = 'all') {
        //pagination
        $merchant_user = $this->session->userdata('merchant_user');
        $config['base_url'] = site_url('member/history/' . $tab . '/');
        $config['total_rows'] = $this->member->getOrders($merchant_user->merchant, '', '')->num_rows();
        $config['per_page'] = 8;
        $this->pagination->initialize($config);
        $start = ($this->input->get('page')) ? ($this->input->get('page') - 1) * $config['per_page'] : 0;

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Member', site_url('member'));
        $this->breadcrumb->add(lang('account_order_history'), site_url('member/history'));
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['orders'] = $this->member->getOrders($merchant_user->merchant, $config['per_page'], $start);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['meta_description'] = settings('meta_description');
        $this->data['page'] = 'member/history';

        $this->output->set_title(lang('account_order_history') . ' - ' . settings('meta_title'));
        $this->template->shop();
//        $this->load->js('assets/frontend/js/modules/member_history.js');
        $this->load->view('member/history', $this->data);
    }

    public function order_detail($id) {
        $data = $this->member->get_order(decode($id));
//        if ($this->input->post('cancel') && $data->payment_method == 'kredivo') {
//            $this->main->update('orders', array('payment_status' => 'Cancel'), array('id' => $data->id));
//            $this->main->update('order_invoice', array('order_status' => settings('order_cancel_status')), array('order' => $data->id));
//            $data = $this->member->get_order(decode($id));
//        }
        $this->data['page'] = 'member/history';
        $this->data['data'] = $data;
        $this->data['data']->payment_method_name = $this->main->get('payment_methods', ['name' => $this->data['data']->payment_method])->title;
        $this->data['payment'] = json_decode($this->data['data']->payment_to);
        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Member', site_url('member'));
        $this->breadcrumb->add(lang('account_order_history'), site_url('member/history'));
        $this->breadcrumb->add($this->data['data']->code, '#');
        $this->data['breadcrumb'] = $this->breadcrumb->output();

        $this->output->set_title('Pesanan:: ' . $this->data['data']->code . ' - ' . settings('meta_title'));
        $this->template->shop();
        $this->load->js('../assets/frontend/js/modules/member_order_detail.js');
        $this->load->view('member/order_detail', $this->data);
    }

   
    public function payment_confirmation() {
        $this->form_validation->set_rules('to', 'Rekening Tujuan', 'trim|required');
        $this->form_validation->set_rules('amount', 'Total Transfer', 'trim|required|numeric');
        $this->form_validation->set_rules('from', 'Nama Pengirim', 'trim|required');
        $this->form_validation->set_rules('note', 'Catatan', 'trim');
        if ($this->form_validation->run() == true) {
            $data = $this->input->post(null, true);
            $order = $this->main->get('orders', array('id' => decode($data['id'])));
            unset($data['id']);
            $payment_to = json_decode($order->payment_to, true);
            $data['to'] = $payment_to[$data['to']];
            $this->main->update('orders', array('payment_to' => json_encode($data), 'payment_status' => 'Confirmed', 'payment_date' => date('Y-m-d')), array('id' => $order->id));
            $this->main->update('order_invoice', array('order_status' => settings('order_payment_confirmed_status')), array('order' => $order->id));
            $return = array('status' => 'success', 'message' => '<div class="alert alert-success">Konfirmasi pembayaran telah kami terima. Pembayaran akan segera kami verifikasi.</div>', 'id' => encode($order->id));
        } else {
            $return = array('status' => 'error', 'message' => '<div class="alert alert-danger">' . validation_errors() . '</div>');
        }
        echo json_encode($return);
    }

    public function receipt_confirmation() {
        $id = $this->input->post('id');
        $invoice = $this->main->get('order_invoice', array('id' => decode($id)));
        $merchant = $this->main->get('merchants', array('id' => $invoice->merchant));
        $this->main->update('order_invoice', array('order_status' => settings('order_finish_status')), array('id' => decode($id)));
        $this->main->insert('order_history', array('order' => $invoice->order, 'invoice' => $invoice->id, 'order_status' => settings('order_finish_status')));

        //send balance to merchant
        $balance = $merchant->cash_balance + $invoice->total;
        $this->main->insert('merchant_balances', array('merchant' => $invoice->merchant, 'type' => 'in', 'invoice' => $invoice->id, 'amount' => $invoice->total, 'balance' => $balance));
        $this->main->update('merchants', array('cash_balance' => $balance), array('id' => $merchant->id));

        $this->load->helper('text');
        $this->data['invoice'] = $invoice;
        $this->data['order'] = $this->orders->get($this->data['invoice']->order);
        $this->data['merchant'] = $merchant;
        $this->data['products'] = $this->main->gets('order_product', array('invoice' => $this->data['invoice']->id));
        $customer = $this->main->get('customers', array('id' => $this->data['order']->customer));
        $message = $this->load->view('email/transaction/order_finish', $this->data, true);
        $cronjob = array(
            'from' => settings('send_email_from'),
            'from_name' => settings('store_name'),
            'to' => $customer->email,
            'subject' => 'Pesanan Selesai: ' . $this->data['order']->code,
            'message' => $message
        );
        $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));

        if ($customer->verification_phone) {
            $sms = json_decode(settings('sprint_sms'), true);
            $sms['m'] = 'Pesanan ' . $this->data['order']->code . ' telah selesai. Terimakasih telah berbelanja di ' . settings('store_name');
            $sms['d'] = $customer->phone;
            $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
        }
        echo 'success';
    }

    public function review() {
        $config['base_url'] = seo_url('member/review');
        $config['total_rows'] = $this->member->get_orders_review($this->data['user']->id, '', '')->num_rows();
        $config['per_page'] = 10;
        $this->pagination->initialize($config);
        $start = ($this->input->get('page')) ? ($this->input->get('page') - 1) * $config['per_page'] : 0;

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Member', site_url('member'));
        $this->breadcrumb->add(lang('account_review'), site_url('member/review'));
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['orders'] = $this->member->get_orders_review($this->data['user']->id, $config['per_page'], $start);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['meta_description'] = settings('meta_description');
        $this->data['page'] = 'member/review';

        $this->output->set_title(lang('account_review') . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->js('assets/frontend/js/modules/member_review.js');
        $this->load->view('member/review', $this->data);
    }

    public function save_review() {
        $this->form_validation->set_rules('rating_speed', 'Rating Kecepatan', 'trim|required');
        $this->form_validation->set_rules('rating_service', 'Rating Pelayanan', 'trim|required');
        $this->form_validation->set_rules('rating_accuracy', 'Rating Kecepatan', 'trim|required');
        $this->form_validation->set_rules('description', 'Isi Ulasan', 'trim|required');
        if ($this->form_validation->run() == true) {
            $data = $this->input->post(null, true);
            $order_product = $data['id'];
            unset($data['id']);
            $product = $this->main->get('order_product', array('id' => $order_product));
            $invoice = $this->main->get('order_invoice', array('id' => $product->invoice));
            $data['product'] = $product->product;
            $data['customer'] = $this->data['user']->id;
            $data['merchant'] = $invoice->merchant;
//            $this->main->insert('product_review', array('product' => $product, 'customer' => $this->data['user']->id, 'rating' => $data['rating'], 'description' => $data['description']));
            $this->main->insert('product_review', $data);
            $html = '<div class="row"><div class="col-sm-3">Kecepatan</div><div class="col-sm-9"><span class="spr-starratings spr-review-header-starratings">';
            for ($i = 1; $i <= $data['rating_speed']; $i++) {
                $html .= '<i class="spr-icon spr-icon-star"></i>';
            }
            for ($i = $data['rating_speed'] + 1; $i <= 5; $i++) {
                $html .= '<i class="spr-icon spr-icon-star-empty"></i>';
            }
            $html .= '</span></div></div>';
            $html .= '<div class="row"><div class="col-sm-3">Pelayanan</div><div class="col-sm-9"><span class="spr-starratings spr-review-header-starratings">';
            for ($i = 1; $i <= $data['rating_service']; $i++) {
                $html .= '<i class="spr-icon spr-icon-star"></i>';
            }
            for ($i = $data['rating_service'] + 1; $i <= 5; $i++) {
                $html .= '<i class="spr-icon spr-icon-star-empty"></i>';
            }
            $html .= '</span></div></div>';
            $html .= '<div class="row"><div class="col-sm-3">Akurasi</div><div class="col-sm-9"><span class="spr-starratings spr-review-header-starratings">';
            for ($i = 1; $i <= $data['rating_accuracy']; $i++) {
                $html .= '<i class="spr-icon spr-icon-star"></i>';
            }
            for ($i = $data['rating_accuracy'] + 1; $i <= 5; $i++) {
                $html .= '<i class="spr-icon spr-icon-star-empty"></i>';
            }
            $html .= '</span></div></div>';
            $html .= '<div class="spr-review-content">
                      <p class="spr-review-content-body">' . $data['description'] . '</p>
                      </div>';
            $return = array('html' => $html, 'status' => 'success');
        } else {
            $return = array('message' => '<div class="alert alert-danger fade in">' . validation_errors() . '</div>', 'status' => 'error');
        }

        echo json_encode($return);
    }

    public function get_cities($province, $city_selected = '') {
        $cities = $this->main->gets('cities', array('province' => $province), 'name ASC');
        $output = '<option value="">Pilih kota</option>';
        if ($cities) {
            foreach ($cities->result() as $city) {
                $output .= '<option value="' . $city->id . '" ' . (($city_selected && $city_selected == $city->id) ? 'selected' : '') . '>' . $city->type . ' ' . $city->name . '</option>';
            }
        }
        echo $output;
    }

    public function get_districts($city, $district_selected = '') {
        $districts = $this->main->gets('districts', array('city' => $city), 'name ASC');
        $output = '<option value="">Pilih kecamatan</option>';
        if ($districts) {
            foreach ($districts->result() as $district) {
                $output .= '<option value="' . $district->id . '" ' . (($district_selected && $district_selected == $district->id) ? 'selected' : '') . '>' . $district->name . '</option>';
            }
        }
        echo $output;
    }

    public function get_address() {
        $address = $this->main->get('customer_address', array('id' => $this->input->post('id')));
        echo json_encode($address);
    }

    public function save_address() {
        $this->form_validation->set_rules('title', 'Nama Alamat', 'trim|required');
        $this->form_validation->set_rules('name', 'lang:checkout_shipping_field_name', 'trim|required');
        $this->form_validation->set_rules('phone', 'lang:checkout_shipping_field_phone', 'trim|required');
        $this->form_validation->set_rules('address', 'lang:checkout_shipping_field_address', 'trim|required');
        $this->form_validation->set_rules('province', 'lang:checkout_shipping_field_province', 'required');
        $this->form_validation->set_rules('city', 'lang:checkout_shipping_field_city', 'required');
        $this->form_validation->set_rules('district', 'lang:checkout_shipping_field_district', 'required');
        $this->form_validation->set_rules('postcode', 'lang:checkout_shipping_field_postcode', 'trim|required');

        $return['status'] = 'error';
        if ($this->form_validation->run() == true) {
            $data = $this->input->post(null, true);
            if ($data['id']) {
                $address = $data['id'];
                $this->main->update('customer_address', $data, array('id' => $address));
                $return['type'] = 'edit';
            } else {
                $data['customer'] = $this->data['user']->id;
                $address = $this->main->insert('customer_address', $data);
                $return['type'] = 'add';
            }
            $province = $this->main->get('provincies', array('id' => $data['province']));
            $city = $this->main->get('cities', array('id' => $data['city']));
            $district = $this->main->get('districts', array('id' => $data['district']));
            $return['id'] = $address;
            $return['status'] = 'success';
            $return['html'] = '<tr id="address-' . $address . '">
                                    <td>
                                        <div style="font-weight: 800">' . $data['name'] . '</div>
                                        <div>' . $data['phone'] . '</div>
                                    </td>
                                    <td>
                                        <div style="font-weight: 800">' . $data['title'] . '</div>
                                        <div>' . nl2br($data['address']) . '</div>
                                    </td>
                                    <td>' . $province->name . ', ' . $city->name . ', ' . $district->name . ' ' . $data['postcode'] . '<br>Indonesia</td>
                                    <td><a href="javascript:void(0);" onclick="edit(\'' . $address . '\')">Ubah</a> | <a href="javascript:void(0);" onclick="remove(\'' . $address . '\')">Hapus</a></td>
                                </tr>';
        } else {
            $return['message'] = '<div class="alert alert-danger fade in">' . validation_errors() . '</div>';
        }

        echo json_encode($return);
    }

    public function remove_address($id) {
        $this->main->delete('customer_address', array('id' => $id));
    }

    public function check_birthday() {
        if (!checkdate($this->input->post('month'), $this->input->post('day'), $this->input->post('year'))) {
            $this->form_validation->set_message('check_birthday', 'Isian tanggal lahir tidak benar.');
            return false;
        } else {
            return true;
        }
    }

    public function check_email($email) {
        if ($email == $this->data['user']->email)
            return true;
        if ($this->ion_auth->email_check($email)) {
            $this->form_validation->set_message('check_email', 'Email sudah terdaftar oleh akun lain.');
            return false;
        } else {
            return true;
        }
    }

    public function check_password_old($password) {
        if ($this->ion_auth->hash_password_db($this->data['user']->id, $this->input->post('old_password'))) {
            return true;
        } else {
            $this->form_validation->set_message('check_password_old', 'Password Lama tidak sesuai.');
            return false;
        }
    }

    public function tracking($invoice) {
        $invoice = decode($invoice);
        $invoice = $this->main->get('order_invoice', array('id' => $invoice));
        if ($invoice) {
            $courier = explode('-', $invoice->shipping_courier);
            $awb = $invoice->tracking_number;
            if ($awb) {
                $this->load->library('rajaongkir');
                $tracking = $this->rajaongkir->waybill($awb, $courier[0]);
                $data['tracking'] = json_decode($tracking)->rajaongkir;
                $this->load->view('member/tracking', $data);
            } else {
                echo '<p>No. Resi tidak valid</p>';
            }
        }
    }

    public function verification_phone() {
        $customer = $this->main->get('customers', array('id' => $this->data['user']->id));
        if ($customer->verification_phone) {
            redirect('member');
        }

        $this->form_validation->set_rules('token', 'Kode Verifikasi', 'trim|required');
        if ($this->form_validation->run() == true) {
            $token = $this->input->post('token');
            if ($token == $customer->verification_code) {
                $this->main->update('customers', array('verification_phone' => 1, 'verification_code' => ''), array('id' => $customer->id));
                $this->session->set_flashdata('success', 'No. Handphone Anda berhasil diverifikasi.');
                $user = $this->ion_auth->user()->row();
                $this->session->set_userdata('user', $user);
                redirect('member');
            }
        } else {
            $this->data['message'] = validation_errors();

            $this->data['token_time_left'] = 0;
            if (!$customer->verification_code) {
                $this->request_token();
                $this->data['token_time_left'] = 120;
            } else {
                $verification_time = strtotime($customer->verification_sent_time);
                if ((strtotime(date('Y-m-d H:i:s')) - $verification_time) < 120) {
                    $this->data['token_time_left'] = strtotime(date('Y-m-d H:i:s')) - $verification_time;
                }
            }

            $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
            $this->breadcrumb->add('Member', site_url('member'));
            $this->breadcrumb->add('Verifikasi Akun', site_url('member/verification_phone'));
            $this->data['breadcrumb'] = $this->breadcrumb->output();

            $this->template->_init();
            $this->output->set_title('Verifikasi No. Handphone');
            $this->load->view('member/verification_phone', $this->data);
        }
    }

    public function request_token() {
        $this->load->library('sprint');
        $sms = json_decode(settings('sprint_sms'), true);
        $code = rand(1000, 9999);
        $sms['m'] = 'Hai, berikut kode verifikasi TokoManaMana Anda ' . $code;
        $sms['d'] = $this->data['user']->phone;
        $url = $sms['url'];
        unset($sms['url']);
        $this->load->library('sprint');
        $sprint_response = $this->sprint->sms($url, $sms);
        $sprint_response = explode('_', $sprint_response);
        if ($sprint_response[0] == 0) {
            $this->main->update('customers', array('verification_code' => $code, 'verification_sent_time' => date('Y-m-d H:i:s')), array('id' => $this->data['user']->id));
        }
    }

}
