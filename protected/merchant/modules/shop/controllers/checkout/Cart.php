<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->lang->load('shop/checkout', settings('language'));
        $this->load->model('catalog/product_model', 'product');
        $this->load->model('catalog/product_price_model', 'product_prices');
        $this->load->model('cart_model');
    }

    public function index() {
        $this->session->unset_userdata('coupon');
        $this->output->set_title(lang('cart_text') . ' - ' . settings('meta_title'));
        $this->template->shop();
        $this->load->js('../assets/backend/js/merchant/modules/shop/cart.js');
        $this->data['page'] = 'cart';

        $this->load->view('checkout/cart', $this->data);
    }

    public function add() {
        $data = $this->input->post(null, true);
        $id = decode($data['product']);
        $qty = $data['qty'];
        $min_qty = $data['min_qty'];
        $output = array('status' => 'error', 'message' => 'Qty minimum '.$min_qty);
        if ($qty >= $min_qty) {
            $product = $this->product->get_product($id);
            if ($product) {
                do {
                    $product_option = 0;
                    if (isset($data['option'])) {
                        $product_option = $this->product->get_product_option_id($product->id, $data['option']);
                        if (!$product_option) {
                            $output['message'] = 'Kesalahan memilih variasi produk.';
                            break;
                        }
//                    $product_option = $this->main->get('product_option', array('id' => $current_product_option));
                        $options = $this->product->get_option_groups($product->id, $product_option);
                        $option = $options->row();
//                    print_r($option);
//                    $product_option = $product_options->row();
                        $image = $this->product->get_product_option_images($product_option);
                        if ($image->num_rows() > 0) {
                            $product->image = $image->row()->image;
                        }
                    }
                    if ($product->length && $product->height && $product->width) {
                        $weight_volume = ($product->length * $product->height * $product->width) / 6;
                        if ($weight_volume > $product->weight)
                            $product->weight = round($weight_volume);
                    }
                    $old_price = $product->price;
                    if($product->promo == 1){
                        $promo_data = json_decode($product->promo_data,true);
                        if($promo_data['type'] == 'P') {
                            $disc_value = $promo_data['discount'];
                            $disc_price = round(($product->price * $promo_data['discount']) / 100);
                        } else {
                            $disc_value = NULL;
                            $disc_price = $promo_data['discount'];
                        }
                        $end_price= $product->price - $disc_price;
                        $today = date('Y-m-d');
                        $today=date('Y-m-d', strtotime($today));
                        $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                        $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
            
                        if (($today >= $DateBegin) && ($today <= $DateEnd)){
                            $price = $end_price;
                            $inPromo = true;
                        } else {
                            $price = $product->price;
                            $inPromo = false;
                        }
                    } else {
                        $price = $product->price;
                        $inPromo = false;
                    }

                    $rowid = md5($id . ($product_option ? $product_option : 0));
                    $item = $this->cart->get_item($rowid);
                    //$merchant = $this->session->userdata('list_merchant')[0];
                    $merchant_user = $this->session->userdata('merchant_user');
                    //$branch = $this->home->get_branch($merchant_user->merchant_group);
                    //$price = $product->price;
                    //jika produk tanaka
                    if ($product->merchant == 0) {
                        $price_level = $this->product->price_in_level($product->id, $merchant_user->merchant_group, ($item ? $item['qty'] + $qty : $qty));
                        if ($price_level) { //jika price level ada, maka produk tsb masuk ke harga grosir
                            $price = $price_level;
                            $price_level = 1;
                        } else {
                            $price_group = $this->main->get('product_price', array('product' => $product->id, 'merchant_group' => $merchant_user->merchant_group));
                            if ($price_group) {
                                $old_price = $price_group->price;
                                if($product->promo == 1){
                                    if($promo_data['type'] == 'P') {
                                        $disc_price_group = round(($price_group->price * $promo_data['discount']) / 100);
                                    } else {
                                        $disc_price_group = $promo_data['discount'];
                                    }
                                    $end_price_group= $price_group->price - $disc_price_group;
                        
                                    if (($today >= $DateBegin) && ($today <= $DateEnd)){
                                        $price = $end_price_group;
                                    } else {
                                        $price = $price_group->price;
                                    }
                                } else {
                                    $price = $price_group->price;
                                }
                            }
                        }
                    } else {
                        $price_level = 0;
                    }
                    $price += ($product_option) ? $option->price : 0;
                    $weight = $product->weight + ($product_option ? $option->weight : 0);
                    if ($item) {
                        $item['qty'] += $qty;
                        $item['price'] = $price;
                        $item['price_level'] = $price_level;
                        $item['weight'] = $weight * $item['qty'];
                        $this->cart->update($item);
                    } else {
                        $item = array(
                            'id' => $id,
                            'name' => $product->name,
                            'type' => $product->type,
                            'image' => $product->image,
                            'code' => $product->code,
                            'category' => $product->category,
                            'description' => $product->short_description,
                            'qty' => $qty,
                            'min_qty' => $min_qty,
                            'price' => $price,
                            'price_level' => $price_level,
                            'merchant' => $product->merchant,
                            'merchant_name' => $product->merchant_name,
                            'merchant_group' => ($product->merchant) ? 0 : $merchant_user->merchant_group,
                            'merchant_type' => '',
                            'weight' => $weight * $qty,
                            'shipping' => '',
                            'shipping_merchant' => $product->merchant,
                            'shipping_cost' => 0,
                            'options' => ($product_option) ? $options->result_array() : array()
                        );
                        if($product->type == 'p'){
                            $item['package_items'] = json_decode($product->package_items, true);
                        } else {
                            $item['package_items'] = array();
                        }
                        if($inPromo){
                            $item['inPromo'] = true;
                            $item['promo_data'] = $promo_data;
                            $item['disc_value'] = $disc_value;
                            $item['disc_price'] = $disc_price;
                            $item['old_price'] = $old_price;
                        } else {
                            $item['inPromo'] = false;
                            $item['promo_data'] = array();
                            $item['disc_value'] = NULL;
                            $item['disc_price'] = NULL;
                            $item['old_price'] = $price;
                        }
                        $this->cart->insert($item);
                        
                    }
                    $output['status'] = 'success';
                } while (0);
                //var_dump($this->cart->contents());exit();
//            $product_options = $this->product->get_product_options($id);
//            if ($product_options) {
//                foreach ($product_options->result() as $po) {
//                    if ($po->required && empty($options[$po->id])) {
//                        $output['error']['option'][] = (int) $po->id;
//                    }
//                }
//            }
//            if (!isset($output['error'])) {
//                $item_options = array();
//                if ($options) {
//                    foreach ($options as $option => $option_variant) {
//                        $option = $this->product->get_product_option_variant_full($option, $option_variant);
//                        if ($option) {
//                            $price_option += $option['price'];
//                            $weight_option += $option['weight'];
//                        }
//                        array_push($item_options, $option);
//                    }
//                }
//                $merchant = $this->session->userdata('list_merchant')[0];
//
//                $price = $price + $price_option;
//                $weight = $product->weight + $weight_option;
//            }
            }
        }
        echo json_encode($output);
    }

    public function update($rowid, $qty) {
        $item = $this->cart->get_item($rowid);
        if ($qty >= $item['min_qty']) {
            $product = $this->product->get_product($item['id']);
            $weight = $product->weight + ($product_option ? $option->weight : 0);
            if ($product) {
                $old_price = $product->price;
                if($product->promo == 1){
                    $promo_data = json_decode($product->promo_data,true);
                    if($promo_data['type'] == 'P') {
                        $disc_value = $promo_data['discount'];
                        $disc_price = round(($product->price * $promo_data['discount']) / 100);
                    } else {
                        $disc_value = NULL;
                        $disc_price = $promo_data['discount'];
                    }
                    $end_price= $product->price - $disc_price;
                    $today = date('Y-m-d');
                    $today=date('Y-m-d', strtotime($today));
                    $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                    $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
        
                    if (($today >= $DateBegin) && ($today <= $DateEnd)){
                        $price = $end_price;
                        $inPromo = true;
                    } else {
                        $price = $product->price;
                        $inPromo = false;
                    }
                } else {
                    $price = $product->price;
                    $inPromo = false;
                }
                $item['price'] = $price;
                $item['qty'] = $qty;
                $item['weight'] = $weight * $item['qty'];
                $item['rowid'] = $rowid;
                if ($product->merchant == 0) {
                    $merchant_user = $this->session->userdata('merchant_user');
                    $price_level = $this->product->price_in_level($product->id, $merchant_user->merchant_group, $qty);
                    if ($price_level) { //jika price level ada, maka produk tsb masuk ke harga grosir
                        $item['price'] = $price_level;
                        $item['price_level'] = 1;
                    } else {
                        $price_group = $this->main->get('product_price', array('product' => $product->id, 'merchant_group' => $merchant_user->merchant_group));
                        if ($price_group) {
                            $old_price = $price_group->price;
                            if($product->promo == 1){
                                if($promo_data['type'] == 'P') {
                                    $disc_price_group = round(($price_group->price * $promo_data['discount']) / 100);
                                } else {
                                    $disc_price_group = $promo_data['discount'];
                                }
                                $end_price_group= $price_group->price - $disc_price_group;
                    
                                if (($today >= $DateBegin) && ($today <= $DateEnd)){
                                    $price = $end_price_group;
                                } else {
                                    $price = $price_group->price;
                                }
                            } else {
                                $price = $price_group->price;
                            }
                            $item['price'] = $price;
                        }
                    }
                }
                if ($item['options']) {
                    foreach ($item['options'] as $option) {
                        $item['price'] += $option['price'];
                    }
                }
                $this->cart->update($item);
            }
        }
        redirect('shop/cart');
    }

    public function delete($rowid) {
        $this->cart->remove($rowid);
        redirect('shop/cart');
    }

    public function destroy() {
        $this->cart->destroy();
        redirect('shop/cart');
    }

}
