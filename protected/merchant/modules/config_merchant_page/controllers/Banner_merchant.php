<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Banner_merchant extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->data['menu'] = 'banner_merchant';
    }

    public function index() {
        if($this->aauth->is_loggedin()){
            $this->aauth->control('config_merchant_page/banner_merchant');
        }
        
        $this->data['errors'] = '';

        if($this->input->post('id')){
            $config['upload_path']          = FCPATH.'../files/images/banner_merchant';
            $config['allowed_types']        = 'jpg|jpeg|png';
            $config['file_name']            = 'banner_merchant_'.$this->input->post('id');
            $config['overwrite']            = TRUE;
            $config['max_size']             = 5120; //5MB

            $this->load->library('upload', $config);
            $id = $this->input->post('id');
            $old_filename=$_FILES['banner_merchant_file']['name'];
            $file_ext = pathinfo($old_filename,PATHINFO_EXTENSION);
            $file_name = 'banner_merchant_'.$this->input->post('id').'.png';

            if ( ! $this->upload->do_upload('banner_merchant_file'))
            {
                
                $error = array('error' => $this->upload->display_errors());
                $this->data['errors'] = $error;
            }
            else
            {   
                $image_data =   $this->upload->data();
                $config_img['image_library']='gd2';
                $config_img['source_image']= $image_data['full_path'];
                $config_img['new_image'] = FCPATH.'../files/images/banner_merchant/banner_merchant_'.$id.'.png';
                $config_img['maintain_ratio']= FALSE;
                $config_img['quality']= '100%';
                $config_img['width']= 1200;
                $config_img['height']= 235;
                $this->load->library('image_lib', $config_img);
                
                if( !$this->image_lib->resize()){
                    $error = array('error' => $this->image_lib->display_errors());
                    $this->data['errors'] = $error;
                }
                else{

                    if($file_ext != 'png' || $file_ext != 'PNG'){
                        unlink($image_data['full_path']);
                    }
                    $this->image_lib->clear();
                    $this->main->update('merchants', array('banner_merchant' => 'banner_merchant/'.$file_name),array('id' => $id));
                }
            }
        }

        $this->template->_init();
        $this->template->form();


        $this->data['data'] = $this->main->get('merchants', array('auth' => $this->data['user']->id));

        $this->load->view('view_banner', $this->data);
    }

    // public function save(){
    //     $config['upload_path']          = FCPATH.'../files/images/banner_merchant';
    //     $config['allowed_types']        = 'jpg|jpeg|png';
    //     $config['file_name']            = 'banner_merchant_'.$this->input->post('id');
    //     $config['overwrite']            = true;
    //     $config['max_size']             = 4096; //4mb

    //     $this->load->library('upload', $config);
    //     $id = $this->input->post('id');
    //     $old_filename=$_FILES['banner_merchant_file']['name'];
    //     $extension = substr($old_filename, strpos($old_filename, ".") + 1);  
    //     $file_name = 'banner_merchant_'.$this->input->post('id').'.'.$extension;

    //     if ( ! $this->upload->do_upload('banner_merchant_file'))
    //     {
            
    //             $error = array('error' => $this->upload->display_errors());

    //             $this->template->_init();
    //             $this->template->form();
    //             $this->data['data'] = $this->main->get('merchants', array('auth' => $this->data['user']->id));
    //             $this->data['errors'] = $error;
    //             $this->load->view('view_banner', $this->data);
    //     }
    //     else
    //     {   
    //            $this->main->update('merchants', array('banner_merchant' => 'banner_merchant/'.$file_name),array('id' => $id));
    //            return redirect('config_merchant_page/banner_merchant/index');
    //     }



    // }

   

}
