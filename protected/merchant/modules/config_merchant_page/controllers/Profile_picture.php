<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Profile_picture extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->data['menu'] = 'profile_picture';
    }

    public function index() {
        if($this->aauth->is_loggedin()){
            $this->aauth->control('config_merchant_page/profile_picture');
        }
        $this->data['errors'] = '';
        if($this->input->post('id')){
            $config['upload_path']          = FCPATH.'../files/images/profilepicture_merchant';
            $config['allowed_types']        = 'jpg|jpeg|png';
            $config['file_name']            = 'profilepicture_'.$this->input->post('id');
            $config['overwrite']            = TRUE;
            $config['max_size']             = 2048; //2MB

            $this->load->library('upload', $config);
            $id = $this->input->post('id');
            $old_filename=$_FILES['profilepicture_file']['name'];
            $file_ext = pathinfo($old_filename,PATHINFO_EXTENSION); 
            $file_name = 'profilepicture_'.$this->input->post('id').'.png';

            if ( ! $this->upload->do_upload('profilepicture_file'))
            {
                
                $error = array('error' => $this->upload->display_errors());
                $this->data['errors'] = $error;
            }
            else
            {   
                $image_data =   $this->upload->data();
                $config_img['image_library']='gd2';
                $config_img['source_image']= $image_data['full_path'];
                $config_img['new_image'] = FCPATH.'../files/images/profilepicture_merchant/profilepicture_'.$id.'.png';
                $config_img['maintain_ratio']= FALSE;
                $config_img['quality']= '100%';
                $config_img['width']= 120;
                $config_img['height']= 120;
                $this->load->library('image_lib', $config_img);
                
                if( !$this->image_lib->resize()){
                    $error = array('error' => $this->image_lib->display_errors());
                    $this->data['errors'] = $error;
                }
                else{

                    if($file_ext != 'png' || $file_ext != 'PNG'){
                        unlink($image_data['full_path']);
                    }

                    $this->image_lib->clear();
                    $this->main->update('merchants', array('merchant_profile_image' => 'profilepicture_merchant/'.$file_name),array('id' => $id));
                    $this->session->unset_userdata('merchant_user');

                }

            }
        }

        $this->template->_init();
        $this->template->form();
        


        $this->data['data'] = $this->main->get('merchants', array('auth' => $this->data['user']->id));

        $this->load->view('view_pp', $this->data);
    }  

}
