<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
               <h2>Profile Picture</h2>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <?php echo form_open_multipart('config_merchant_page/Profile_picture');?>
                    <input type="hidden" name="id" value="<?php echo $data->id?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="row">
                                <div style="margin: 10px;margin-left: 15px;"><h3>Profile Picture Halaman Merchant</h3></div>
                                <?php if ($data && $data->merchant_profile_image) { ?>
                                    <div class="col-md-3" id="image-preview">
                                        <div class="thumbnail" style="height: 120px;width: 120px; margin-left: auto;margin-right: auto;">
                                            <div class="thumb">
                                                <a href="<?php echo site_url('../files/images/' . $data->merchant_profile_image .'?'.time()); ?>" data-lightbox="pp_merchant" >
                                                <img style="" src="<?php echo site_url('../files/images/' . $data->merchant_profile_image .'?'.time()); ?>">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                <?php }else{ ?>
                                    <div class="col-md-3" id="image-preview">
                                        <div class="thumbnail" style="height: 120px;width: 120px; margin-left: auto;margin-right: auto;">
                                            <div class="thumb">
                                                <img src="<?php echo site_url('../files/images/profilepicture_merchant/default_image.jpeg'); ?>">
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="col-md-8" id="add-image">
                                    <div class="form-group">
                                        <label class="col-sm-6 control-label">Image Profile Picture <b style="color: red;">(*Max Size 2MB)</b> :</label>
                                    <div class="col-sm-7 control-label" >
                                        <input type="file" name="profilepicture_file" /> 
                                    </div>
                                    <div class="col-sm-7 control-label" style="padding:10px">
                                        <i style="font-weight: 400;"><?php echo lang('pp_warning'); ?></i>
                                    </div>
                                    <div class="col-sm-7 control-label" style="color: red; padding:10px;">
                                        <?php if($errors!= '') {
                                     foreach ($errors as $error) {
                                        echo $error;
                                     } 
                                    } ?>
                                    </div>
                                    
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                            <div class="heading-elements action-left">
                               
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>