<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('dashboard_model','dashboard');
        $this->data['menu'] = 'dashboard';
    }

    public function index() {
        $summaries = $this->dashboard->sales_summary();
        // var_dump($this->db->last_query()); die;
        $transaction = 0;
        $sales = 0;
        $today = date('Y-m-d');
        $past_date = date('Y-m-d', strtotime('-6 day'));
        $future_date = $today;
        foreach($summaries as $summary) {
            $date_summary = date('Y-m-d', strtotime($summary->date_summary));
            if($past_date <= $today && $future_date >= $today) {
                $transaction += $summary->transaction;
                $sales += $summary->sales;
            }
        }
        $this->data['past_date'] = $past_date;
        $this->data['future_date'] = $future_date;
        $this->data['transaction'] = $transaction;
        $this->data['sales'] = $sales;
        $this->data['last_orders'] = $this->dashboard->last_orders();
/*        var_dump($this->db->last_query());
        exit();*/
//        $this->data['last_search'] = $this->dashboard->last_search();
//        $this->data['top_search'] = $this->dashboard->top_search();
        $this->data['bestsellers'] = $this->dashboard->bestsellers();
        $this->data['mostviewed'] = $this->dashboard->mostviewed();
        // $branch = $this->main->gets('merchants', ['principal_id' => $this->data['user']->principle_id, 'type' => 'principle_branch']);
        $branch = $this->dashboard->get_branch($this->data['user']->principle_id);
        $this->data['branches'] = $branch;
        $balance = 0;
        foreach($branch->result() as $br) {
            $balance += $br->cash_balance;
        }
        $this->data['balance'] = $balance;
        
        $this->template->_init();
        $this->load->js('https://www.amcharts.com/lib/4/core.js');
        $this->load->js('https://www.amcharts.com/lib/4/charts.js');
        $this->load->js('https://www.amcharts.com/lib/4/themes/animated.js');
        $this->load->js('https://www.amcharts.com/lib/4/themes/kelly.js');
        $this->load->js('../assets/backend/js/plugins/visualization/d3/d3.min.js');
        $this->load->js('../assets/backend/js/plugins/visualization/c3/c3.min.js');
        $this->load->js('../assets/principle/js/modules/dashboard.js');
        $this->output->set_title('Dasbor');
        $this->load->view('dashboard', $this->data);
    }

    public function sales_summary() {
        $data = array('finish' => '0','progress' => '0','reject' => '0',);
        $month = date('m');
        $year = date('Y');
        $datas = $this->dashboard->sales_summary();
        $data = [];
        foreach($datas as $key => $as) {
            $data[] = $as;
        }
        
        echo json_encode($data);
    }

}
