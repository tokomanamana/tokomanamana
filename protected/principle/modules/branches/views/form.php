<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo ($data) ? lang('branch_edit_heading') : lang('branch_add_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('branches'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('branch_list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('branches/save'); ?>" method="post" class="form-horizontal" id="form">
                    <input type="hidden" name="id" value="<?php echo set_value('id', ($data) ? encode($data->id) : ''); ?>">
                    <input type="hidden" name="lat" id="lat" value="<?php echo ($data) ? $data->lat : ''; ?>">
                    <input type="hidden" name="lng" id="lng" value="<?php echo ($data) ? $data->lng : ''; ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#data" data-toggle="tab"><?php echo lang('branch_form_data_tabs'); ?></a></li>
                                    <li class=""><a href="#address" data-toggle="tab"><?php echo lang('branch_form_address_tabs'); ?></a></li>
                                    <li class=""><a href="#owner" data-toggle="tab"><?php echo lang('branch_form_owner_tabs'); ?></a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="data">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="name"><?php echo lang('branch_form_name_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="name" name="name" placeholder="<?php echo lang('branch_form_name_placeholder'); ?>" value="<?php echo ($data) ? $data->name : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('branch_form_description_label'); ?></label>
                                            <div class="col-md-9">
                                                <textarea cols="30" rows="2" class="form-control" id="description" name="description" placeholder="<?php echo lang('branch_form_description_placeholder') ?>"><?php echo ($data) ? $data->description : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="telephone"><?php echo lang('branch_form_telephone_label'); ?></label>
                                            <div class="col-md-4">
                                                <input type="text" onkeypress="if(event.which &lt; 48 || event.which &gt; 57 ) if(event.which != 8) return false;" class="form-control" name="telephone" value="<?php echo ($data) ? $data->telephone : ''; ?>" placeholder="<?php echo lang('branch_form_telephone_placeholder') ?>">
                                            </div>
                                        </div>
                                        <div class="form-group" style="display:none">
                                            <label class="col-md-3 control-label" for="warehouse_price"><?php echo lang('branch_form_w_price_label'); ?></label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="warehouse_price" value="<?php echo ($data) ? $data->warehouse_price : '0'; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group" style="display:none">
                                            <label class="col-md-3 control-label" for="warehouse_size"><?php echo lang('branch_form_w_size_label'); ?></label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="warehouse_size" value="<?php echo ($data) ? $data->warehouse_size : '0'; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('branch_form_status_label'); ?></label>
                                            <div class="col-md-9 status-brand">
                                                <input type="checkbox" name="status" value="1" data-on-text="<?php echo lang('enabled'); ?>" data-off-text="<?php echo lang('disabled'); ?>" class="switch" <?php echo ($data) ? (($data->status == 1) ? 'checked' : '') : 'checked'; ?>>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane " id="address">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('branch_form_address_label'); ?></label>
                                            <div class="col-md-9">
                                                <textarea cols="30" rows="2" class="form-control" id="address" name="address" placeholder="<?php echo lang('branch_form_address_placeholder') ?>"><?php echo ($data) ? $data->address : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="province"><?php echo lang('branch_form_province_label'); ?></label>
                                            <div class="col-md-4">
                                                <select class="bootstrap-select" name="province" id="province" data-live-search="true" data-width="100%">
                                                    <option value=""></option>
                                                    <?php if ($provinces) { ?>
                                                        <?php foreach ($provinces->result() as $province) { ?>
                                                            <option value="<?php echo $province->id; ?>" <?php echo ($data) ? ($data->province == $province->id) ? 'selected' : '' : ''; ?>><?php echo $province->name; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="city"><?php echo lang('branch_form_city_label'); ?></label>
                                            <div class="col-md-4">
                                                <select class="bootstrap-select" name="city" id="city" data-live-search="true" data-width="100%">
                                                    <option value=""></option>
                                                    <?php if ($cities) { ?>
                                                        <?php foreach ($cities->result() as $city) { ?>
                                                            <option value="<?php echo $city->id; ?>" <?php echo ($data) ? ($data->city == $city->id) ? 'selected' : '' : ''; ?>><?php echo $city->name; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="district"><?php echo lang('branch_form_district_label'); ?></label>
                                            <div class="col-md-4">
                                                <select class="bootstrap-select" name="district" id="district" data-live-search="true" data-width="100%">
                                                    <option value=""></option>
                                                    <?php if ($districts) { ?>
                                                        <?php foreach ($districts->result() as $district) { ?>
                                                            <option value="<?php echo $district->id; ?>" <?php echo ($data) ? ($data->district == $district->id) ? 'selected' : '' : ''; ?>><?php echo $district->name; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('maps'); ?></label>
                                            <div class="col-md-9">
                                                <script type="text/javascript">
                                                    var centreGot = false;
                                                </script>
                                                <?php echo $map['js']; ?>
                                                <input class="form-control" id="search-location">
                                                <?php echo $map['html']; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane " id="owner">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="fullname"><?php echo lang('branch_form_fullname_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control"  name="fullname" placeholder="<?php echo lang('branch_form_fullname_placeholder') ?>" value="<?php echo ($data) ? $data->fullname : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="email"><?php echo lang('branch_form_email_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="email" class="form-control" <?php echo (!$data) ? '' : 'disabled' ?> name="email" value="<?php echo ($data) ? $data->email : ''; ?>" placeholder="<?php echo lang('branch_form_email_placeholder') ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('branch_form_phone_label') ?></label>
                                            <div class="col-md-4">
                                                <input type="text" onkeypress="if(event.which &lt; 48 || event.which &gt; 57 ) if(event.which != 8) return false;" class="form-control" name="phone" placeholder="<?php echo lang('branch_form_phone_placeholder') ?>" value="<?php echo ($data) ? $data->phone : ''; ?>">
                                            </div>
                                        </div>
                                        <!-- <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('setting_profile_bank_name_label'); ?></label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="bank_name" placeholder="<?php echo lang('branch_form_bank_name_placeholder') ?>" value="<?php echo ($data) ? $data->bank_name : ''; ?>">
                                            </div>
                                        </div> -->
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('setting_profile_bank_name_label'); ?></label>
                                            <div class="col-md-4">
                                               <select class="bootstrap-select" name="bank_name" data-live-search="true" id="bank_name" data-width="100%">
                                                    <option></option>
                                                    <?php if ($bank_lists) {  ?>
                                                        <?php foreach ($bank_lists->result() as $bank_list) {
                                                            if($data->bank_name == 'BCA') $data->bank_name = 'BANK BCA'; ?>
                                                            <option value="<?php echo $bank_list->name;?>" <?php echo ($data) ? ($data->bank_name == $bank_list->name) ? 'selected' : '' : ''; ?>><?= $bank_list->name;?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('setting_profile_bank_account_name_label'); ?></label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="bank_account_name" placeholder="<?php echo lang('branch_form_bank_account_name_placeholder') ?>" value="<?php echo ($data) ? $data->bank_account_name : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('setting_profile_bank_account_number_label'); ?></label>
                                            <div class="col-md-4">
                                                <input type="number" class="form-control" name="bank_account_number" placeholder="<?php echo lang('branch_form_bank_account_number_placeholder') ?>" value="<?php echo ($data) ? $data->bank_account_number : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('setting_profile_bank_branch_label'); ?></label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="bank_branch" placeholder="<?php echo lang('branch_form_bank_branch_placeholder') ?>" value="<?php echo ($data) ? $data->bank_branch : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="password"><?php echo lang('branch_form_password_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="password" class="form-control" <?php echo (!$data) ? '' : '' ?> placeholder="<?php echo lang('branch_form_password_placeholder') ?>" name="password">
                                                <?php if ($data) { ?>
                                                    <span class="help-block"><?php echo lang('branch_form_password_update_help'); ?></span>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('branches'); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>