<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('heading'); ?></h2>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <?php if ($this->aauth->is_allowed('report/sales/edit')) { ?>
                        <a href="javascript:void(0);" class="btn btn-link btn-float has-text" id="btn-export"><i class="icon-download7 text-primary"></i><span>Export</span></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="row">
                    <form id="filter-form">
                        <div class="col-sm-5">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">Filter By:</label>
                                        <select id="filter-by" class="form-control bootstrap-select">
                                            <option value="today">Hari Ini</option>
                                            <option value="yesterday">Kemarin</option>
                                            <option value="this month">Bulan Ini</option>
                                            <option value="last month">Bulan Lalu</option>
                                            <option value="this year">Tahun Ini</option>
                                            <option value="99">Kostum</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">Dari:</label>
                                        <input type="text" class="form-control" readonly="" name="from" value="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 no-padding-right">
                                    <div class="form-group">
                                        <label class="control-label">Sampai:</label>
                                        <input type="text" class="form-control" readonly="" name="to" value="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="col-xs-6 col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Merek:</label>
                                    <select name="brands" class="form-control bootstrap-select">
                                        <option value="">Semua</option>
                                        <?php if ($brands = $this->main->gets('brands', array('principle_id'=>$user->principle_id), 'name asc')) foreach ($brands->result() as $brand) { ?>
                                                <option value="<?php echo $brand->id; ?>"><?php echo $brand->name; ?></option>
                                            <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Pengiriman:</label>
                                    <select name="shipping" class="form-control bootstrap-select">
                                        <option value="">Semua</option>
                                        <option value="jne">JNE</option>
                                        <option value="tiki">TIKI</option>
                                        <option value="pos">POS</option>
                                        <option value="pickup">Pickup</option>
                                    </select>
                                </div>
                            </div>
                            <!-- <div class="col-xs-6 col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Zona:</label>
                                    <select name="zone" class="form-control">
                                        <option value="">Semua</option>
                                        <?php if ($groups = $this->main->gets('merchant_groups', array(), 'name asc')) foreach ($groups->result() as $group) { ?>
                                                <option value="<?php echo $group->id; ?>"><?php echo $group->name; ?></option>
                                            <?php } ?>
                                    </select>
                                </div>
                            </div> -->
                        </div>

                        <div class="row">
                            <!-- <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Kota</label>
                                    <div class="col-sm-10">
                                        <select multiple="" name="city" class="select2 form-control">
                                            <?php if ($cities = $this->main->gets('cities')) foreach ($cities->result() as $city) { ?>
                                                    <option value="<?php echo $city->id; ?>"><?php echo $city->name; ?></option>
                                                <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div> -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Cabang: </label>
                                    <div class="col-sm-10">
                                        <select name="branch" class="form-control bootstrap-select">
                                            <option value="">Semua</option>
                                            <?php if($branches = $this->main->gets('merchants', ['type' => 'principle_branch', 'principal_id' => $user->principle_id])) : ?>
                                                <?php foreach($branches->result() as $branch) : ?>
                                                    <option value="<?= $branch->id ?>"><?= $branch->name ?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <button class="btn btn-default" type="button" id="filter">Filter</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="panel panel-flat">
            <table class="table table-hover" id="table-report" data-url="<?php echo site_url('reports/sales/data'); ?>">
                <thead>
                    <!-- <tr>
                        <th>Invoice ID</th>
                        <th class="default-sort" data-sort="desc">Tanggal</th>
                        <th>Nama Barang</th>
                        <th>Total</th>
                        <th>Qty</th>
                        <th>Pelanggan</th>
                        <th>Merchant</th>
                        <th>Brand</th>
                        <th>Zona</th>
                    </tr> -->
                    <tr>
                        <th class="default-sort" data-sort="desc">No. Invoice</th>
                        <th>Tanggal</th>
                        <th>Pelanggan</th>
                        <th>Total</th>
                        <th>Status</th>
                        <th>Pengiriman</th>
                        <th>Toko</th>
                        <th>Merek</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>