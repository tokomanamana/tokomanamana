<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reports_branch_model extends CI_Model {

    function sales($merchant, $start = 0, $length, $order = array(), $from, $to, $order_status = '', $shipping = '') {
        $this->db->select('o.id order, oi.id, oi.code invoice, o.code, cst.fullname customer, oi.total, oi.shipping_courier,m.id as merchant_id,m.name as merchant_name,o.date_added date, sos.name order_status')
                ->join('orders o', 'o.id = oi.order', 'left')
                ->join('customers cst', 'cst.id = o.customer', 'left')
                ->join('setting_order_status sos', 'sos.id = oi.order_status', 'left')
                ->join('merchants m', 'm.id = oi.merchant', 'left')
                //->where('oi.merchant', $merchant)
                ->where_in('oi.merchant', $merchant)
                ->where('o.payment_status', 'PAID')
                ->where('DATE(o.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'')
                ->limit($length, $start);
        if ($order_status)
            $this->db->where('oi.order_status', $order_status);
        if ($shipping)
            $this->db->like('oi.shipping_courier', $shipping);
        if ($order) {
            $col = $order['column'];
            switch ($order['column']) {
                case 0: $col = 'invoice';
                    break;
                case 1: $col = 'date';
                    break;
                case 2: $col = 'merchant_name';
                    break;
                case 3: $col = 'customer';
                    break;
                case 4: $col = 'total';
                    break;
                case 5: $col = 'order_process';
                    break;
                case 7: $col = 'shipping_courier';
                    break;
            }
            $this->db->order_by($col, $order['dir']);
        }
        return $this->db->get('order_invoice oi');
    }

    function sales_export($merchant, $from, $to, $order_status = '', $shipping = '') {
        $this->db->select('o.id order, oi.id, oi.code invoice, o.code, cst.fullname customer, oi.total, oi.shipping_courier,m.id as merchant_id,m.name as merchant_name, o.date_added date, sos.name order_status')
                ->join('orders o', 'o.id = oi.order', 'left')
                ->join('customers cst', 'cst.id = o.customer', 'left')
                ->join('setting_order_status sos', 'sos.id = oi.order_status', 'left')
                ->join('merchants m', 'm.id = oi.merchant', 'left')
                //->where('oi.merchant', $merchant)
                ->where_in('oi.merchant', $merchant)
                ->where('o.payment_status', 'PAID')
                ->where('DATE(o.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'');
        if ($order_status)
            $this->db->where('oi.order_status', $order_status);
        if ($shipping)
            $this->db->like('oi.shipping_courier', $shipping);
        return $this->db->get('order_invoice oi');
    }

    function sales_count($merchant, $from, $to, $order_status = '', $shipping = '') {
        $this->db->join('orders o', 'o.id = oi.order', 'left')
                //->where('oi.merchant', $merchant)
                ->where_in('oi.merchant', $merchant)
                ->where('o.payment_status', 'PAID')
                ->where('DATE(o.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'');
        if ($order_status)
            $this->db->where('oi.order_status', $order_status);
        if ($shipping)
            $this->db->like('oi.shipping_courier', $shipping);
        return $this->db->count_all_results('order_invoice oi');
    }

    function balances($principle, $start = 0, $length, $from, $to) {
        $this->db->select('b.*, oi.code invoice_code')
                ->join('order_invoice oi', 'oi.id = b.invoice', 'left')
                ->join('merchants m', 'm.id = b.merchant', 'left')
                ->where('m.principal_id',$principle)
                ->where('DATE(b.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'')
                ->limit($length, $start)
                ->order_by('b.date_added desc');
        return $this->db->get('merchant_balances b');
    }

    function balances_count($principle, $from, $to) {
        $this->db->join('merchants m', 'm.id = b.merchant', 'left')
                    ->where('m.principal_id',$principle)
                    ->where('DATE(b.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'');
        return $this->db->count_all_results('merchant_balances b');
    }

    function get_downline($merchant) {
        $this->db->select('m.id,m.name');
        $this->db->where('m.parent',$merchant);
        return $this->db->get('merchants m');
    }

    function balances_total($principle){
        $this->db->select('SUM(m.cash_balance) as balance');
        $this->db->where('m.principal_id',$principle);
        return $this->db->get('merchants m');
    }

    function balances_withdrawal($principle){
        $this->db->select('SUM(mbw.amount) as balance');
        $this->db->join('merchants m', 'm.id = mbw.merchant', 'left');
        $this->db->where('m.principal_id',$principle);
        $this->db->where('mbw.status','Pending');
        return $this->db->get('merchant_balance_withdrawal mbw');
    }

}
