<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['design'] = 'Desain';
$lang['banner'] = 'Banner';
$lang['profile_picture'] = 'Foto Profil';

$lang['brand_save_success_message'] = "Merek '%s' berhasil disimpan.";
$lang['brand_save_error_message'] = "Merek '%s' gagal disimpan.";
$lang['brand_delete_success_message'] = "Merek '%s' berhasil dihapus.";
$lang['brand_delete_error_message'] = "Merek '%s' gagal dihapus.";
$lang['page_friendly_url_exist_message'] = "Friendly URL sudah digunakan.";