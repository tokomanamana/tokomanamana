<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Design extends CI_Controller {

	public function __construct() {
        parent::__construct();

        $this->lang->load('design', settings('language'));
        $this->data['menu'] = 'design';
        $this->aauth->control('design');
    }

    private function message($type, $message) {
    	return '<div class="col-md-6">
                    <div class="alert alert-' . $type . '">
                    	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                       ' . $message . '
                    </div>
                </div>';
    }

    public function banner() {
    	$this->aauth->control('design/banner');
    	$this->data['menu'] = 'design_banner';
    	$this->data['errors'] = '';
    	$this->data['message'] = '';

    	if($_FILES) {
    		$principle_id = $this->data['user']->principle_id;
    		$config['upload_path']          = FCPATH . '../files/images/banner_principal';
            $config['allowed_types']        = 'jpg|jpeg|png';
            $config['file_name']            = 'banner_principal_'.$principle_id;
            $config['overwrite']            = true;
            $config['max_size']             = 4096; //4mb

            $this->load->library('upload', $config);
            $old_filename = $_FILES['banner_image']['name'];
            $extension = substr($old_filename, strpos($old_filename, ".") + 1);  
            $file_name = 'banner_principal_' . $principle_id . '.' . $extension;

            if ( ! $this->upload->do_upload('banner_image'))
            {
                $error = array('error' => $this->upload->display_errors());
                $this->data['message'] = $this->message('danger', $this->upload->display_errors());
            }
            else
            {   
            	$this->data['message'] = $this->message('success', 'Banner berhasil diperbarui!');
                $this->main->update('principles', array('banner' => 'banner_principal/' . $file_name), array('id' => $principle_id));
            }
    	}

    	$this->template->_init();
        $this->template->form();

        $this->breadcrumbs->unshift(lang('design'), '/');
        $this->breadcrumbs->push(lang('banner'), '/design/banner');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('banner'));
        $this->data['data'] = $this->main->get('principles', ['id' => $this->data['user']->principle_id]);
        $this->load->view('banner/view', $this->data);
    }

    public function profile_picture() {
    	$this->aauth->control('design/profile_picture');
    	$this->data['menu'] = 'design_profile_picture';
    	$this->data['message'] = '';
        $principle_id = $this->data['user']->principle_id;
        $principle = $this->main->get('principles', ['id' => $principle_id]);

    	if($_FILES) {
    		$today = date('Y_m_d');
    		$rand = rand(100, 200);
    		$file_name_img = 'profile_picture_' . $principle_id . '_' . $today . '_' . $rand;
    		$config['upload_path']          = FCPATH . '../files/images/profile_picture_principal';
            $config['allowed_types']        = 'jpg|jpeg|png';
            $config['file_name']            = $file_name_img;
            $config['overwrite']            = true;
            $config['max_size']             = 2048; //2mb

            $this->load->library('upload', $config);
            $old_filename = $_FILES['profile_picture']['name'];
            $extension = substr($old_filename, strpos($old_filename, ".") + 1);
            $file_name = $file_name_img . '.' . $extension;

            if ( ! $this->upload->do_upload('profile_picture'))
            {
                $error = array('error' => $this->upload->display_errors());
                $this->data['message'] = $this->message('danger', $this->upload->display_errors());
            }
            else
            {   
            	$this->data['message'] = $this->message('success', 'Banner berhasil diperbarui!');
                $this->main->update('principles', ['image' => 'profile_picture_principal/' . $file_name], ['id' => $principle_id]);
                redirect('design/profile_picture');
            }
    	}

    	$this->template->_init();
        $this->template->form();

        $this->breadcrumbs->unshift(lang('design'), '/');
        $this->breadcrumbs->push(lang('profile_picture'), '/design/profile_picture');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('profile_picture'));
        $this->data['data'] = $principle;
        $this->load->view('profile_picture/view', $this->data);
    }

}