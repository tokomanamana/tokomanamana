<?php

use PhpOffice\PhpSpreadsheet\Writer\Ods\Thumbnails;

defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->lang->load('auth', settings('language'));
        $this->load->library('form_validation');
        $this->load->helper('url');
    }

    public function login()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('identity', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            if ($this->form_validation->run() == true) {
                $data = $this->input->post(null, true);
                $remember = (bool) $this->input->post('remember');
                do {
                    $return = ['status' => 'error', 'message' => 'Login gagal, silahkan coba lagi'];
                    $merchant = $this->main->get('aauthp_users', array('email' => $data['identity']));
                    if (!$merchant) {
                        break;
                    }
                    if ($this->aauth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {
                        $return = array('message' => '', 'status' => 'success', 'redirect' => site_url($this->input->post('back')));
                    } else {
                        $return = array('message' => $this->aauth->print_errors(), 'status' => 'error');
                    }
                } while (0);
            } else {
                $return = array('message' => validation_errors(), 'status' => 'error');
            }
            echo json_encode($return);
        } else {
            $this->data['page'] = 'login';
            $this->output->set_title('Login Principal Application');
            $this->data['meta_description'] = 'Login Principal Tokomanamana';
            $this->template->_init('auth');
            $this->template->form();
            $this->load->js(site_url() . '../assets/principle/js/modules/auth/login.js');
            $this->load->view('login', $this->data);
        }
    }

    public function logout()
    {
        $this->aauth->logout();
        redirect('auth/login', 'refresh');
    }

    public function forgot_password()
    {
        if ($this->aauth->logged_in())
            redirect();

        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules('email', 'Alamat Email', 'required|valid_email');
            if ($this->form_validation->run() == true) {
                $forgotten = $this->aauth->forgotten_password($this->input->post('email'));
                if ($forgotten) {
                    $return = array('message' => 'Permintaan reset password telah diterima. Silahkan cek email Anda untuk langkah selanjutnya.', 'status' => 'success');
                } else {
                    $return = array('message' => 'Email yang Anda masukkan tidak terdaftar.', 'status' => 'error');
                }
            } else {
                $return = array('message' => validation_errors(), 'status' => 'error');
            }
            echo json_encode($return);
        } else {
            $this->template->_auth();
            $this->load->js('assets/js/modules/auth/forgot_password.min.js');

            $this->output->set_title('Lupa Password Bogatoko');
            $this->load->view('forgot_password');
        }
    }

    public function reset_password($code)
    {
        if ($this->aauth->logged_in())
            redirect();

        $reset = $this->aauth->forgotten_password_complete($code);
        $this->template->_auth();

        $this->output->set_title('Reset Password Bogatoko');
        if ($reset) {
            $this->load->view('forgot_password_complete');
        } else {
            $this->load->view('forgot_password_failed');
        }
    }

    //     public function register() {
    //         $this->load->library('session');
    //         $this->load->library('recaptcha');
    //         $this->load->helper(array('captcha','url'));
    //         if ($this->aauth->is_loggedin())
    //         redirect();

    //         $this->load->helper('captcha');

    //             $vals = array(
    //                 'word' => '',
    //                 'pool' => '0123456789',
    //                 'word_length' => 4,
    //                 'img_path'   => '../assets/frontend/captcha/',
    //                 'img_url'    => base_url() . '../assets/frontend/captcha/',
    //                 'img_width'  => '200',
    //                 'img_height' => 35,
    //                 'border' => 0,
    //                 'font_size' => 26, 
    //                 'expiration' => 7200,
    //                 'font_path' => FCPATH. 'assets/frontend/captcha/fonts/coolvetica_rg.ttf',
    //                 'colors' => array(
    //                     'background' => array(255, 255, 255),
    //                     'border' => array(230, 230, 230),
    //                     'text' => array(0, 0, 0),
    //                     'grid' => array(255, 255, 255)
    //                 )
    //             );

    //             // create captcha image
    //             $cap = create_captcha($vals);

    //             // store image html code in a variable
    //             $this->data['image'] = $cap['image'];




    //         if ($this->input->is_ajax_request()) {
    //             //$this->load->library('form_validation');
    //             //$this->form_validation->set_rules('email', 'lang:email', 'required|valid_email');
    //             //$this->form_validation->set_rules('password', 'lang:password', 'required|min_length[6]');
    //             //$this->form_validation->set_rules('confirm_password', 'lang:confirm_password', 'required|matches[password]');
    //             //$this->form_validation->set_rules('owner_name', 'lang:owner_name', 'required');
    //             // $this->form_validation->set_rules('owner_phone', 'lang:owner_phone', 'required|numeric');
    //             // $this->form_validation->set_rules('verified_phone', 'lang:verified_phone', 'required');
    //             // $this->form_validation->set_rules('owner_birthday', 'lang:owner_birthday_day_placeholder', 'required');
    //             // $this->form_validation->set_rules('owner_id', 'lang:owner_id', 'required');
    //             // $this->form_validation->set_rules('store_name', 'lang:store_name', 'required');
    //             // $this->form_validation->set_rules('store_telephone', 'lang:store_telephone', 'required|numeric');
    //             // $this->form_validation->set_rules('store_address', 'lang:store_address', 'required');
    //             // $this->form_validation->set_rules('store_province', 'lang:store_province', 'required');
    //             // $this->form_validation->set_rules('store_city', 'lang:store_city', 'required');
    //             // $this->form_validation->set_rules('store_district', 'lang:store_district', 'required');
    //             // $this->form_validation->set_rules('store_postcode', 'lang:store_postcode', 'required');
    //             // $this->form_validation->set_rules('bank_name', 'lang:bank_name', 'required');
    //             // $this->form_validation->set_rules('bank_branch', 'lang:bank_branch', 'required');
    //             // $this->form_validation->set_rules('bank_account_number', 'lang:bank_account_number', 'required');
    //             // $this->form_validation->set_rules('bank_account_name', 'lang:bank_account_name', 'required');
    //             // $this->form_validation->set_rules('agree', 'lang:terms', 'required', lang('terms_required'));
    //             // $this->form_validation->set_rules( 'captcha', 'captcha', 'trim|callback_validate_captcha|required' );
    //             //$this->form_validation->set_rules('g-recaptcha-response', 'lang:captcha', 'required|callback_check_recaptcha');

    //         $submit = $this->input->post('submit');
    //         if ($submit) {
    //             // var_dump($this->form_validation->run());
    //             // exit();
    //             //if ($this->form_validation->run() == true) {

    //                 $data = $this->input->post(null, true);
    //                 $data['document'] = json_encode($data['document']);
    //                 //var_dump($data);exit();
    //                 do {
    //                     $this->main->insert('principle_register', array(
    //                         'brand_name' => $data['brand_name'],
    //                         'company_name' => $data['company_name'],
    //                         'owner_name' => $data['pic_name'],
    //                         'owner_email' => $data['email'],
    //                         'owner_phone' => $data['owner_phone'],
    //                         'company_type' => $data['company_type'],
    //                         'main_category' => $data['main_category'],
    //                         'sku_total' => $data['sku_total'],
    //                         'revenue' => $data['revenue'],
    //                         'document' => $data['document'],
    //                         'instagram_acc' => $data['instagram_acc'],
    //                         'other_ecommerce' => $data['other_ecommerce'],
    //                         'range_price' => $data['range_price'],
    //                         'know_from' => $data['know_from'],
    //                         'custom_know_form' => $data['custom_know_form'],
    //                     ));
    //                     //$this->data['name'] = $data['owner_name'];
    //                     // $message = $this->load->view('email/register_confirmation', $this->data, TRUE);
    //                     // $cronjob = array(
    //                     //     'from' => settings('send_email_from'),
    //                     //     'from_name' => settings('store_name'),
    //                     //     'to' => $data['email'],
    //                     //     'subject' => 'Konfirmasi Pendaftaran Merchant ' . settings('store_name'),
    //                     //     'message' => $message
    //                     // );
    //                     // $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));

    // //                    send_mail('noreply@tokomanamana.com', $data['email'], 'Konfirmasi Pendaftaran Merchant ' . settings('store_name'), $message);
    //                     $return = array('message' => 'Pendaftaran anda akan segera diproses!', 'status' => 'success', 'redirect' => site_url('auth/login'));

    //                 } while (0);

    //                     /*echo '<script language="javascript">';
    //                     echo 'alert("Pendaftaran Sukses, Silahkan login")';
    //                     echo '</script>';

    //                     redirect(base_url('login'),refresh);*/

    //             } else {

    //             //     //var_dump('aaaa');exit();

    //             //     $return = array('message' => validation_errors(), 'status' => 'error');

    //             //         /*echo '<script language="javascript">';
    //             //         echo 'alert("Pendaftaran Gagal, Kode Captcha Salah")';
    //             //         echo '</script>';

    //             //         redirect(base_url('register'),refresh);*/
    //             }
    //             echo json_encode($return);
    //       } else {            
    //             $this->session->set_userdata('mycaptcha', $cap['word']);
    //             $this->data['page'] = 'register';

    //             // $this->data['page'] = 'register';
    //             $this->data['sku'] = $this->main->gets('principle_variable', ['type'=>'sku'], 'id asc');
    //             $this->data['revenue'] = $this->main->gets('principle_variable', ['type'=>'revenue'], 'id asc');
    //             $this->data['range'] = $this->main->gets('principle_variable', ['type'=>'range'], 'id asc');
    //             $this->data['from'] = $this->main->gets('principle_variable', ['type'=>'from'], 'id asc');
    //             // $this->data['provincies'] = $this->main->gets('provincies', [], 'name asc');
    //             // $this->data['widget'] = $this->recaptcha->getWidget();
    //             // $this->data['script'] = $this->recaptcha->getScriptTag();

    //             $this->template->_init('auth');
    //             $this->template->form();
    //             $this->output->set_title('Daftar Principal');
    //             $this->data['meta_description'] = 'Daftar menjadi penjual ditokomanamana, Tanpa ribet, cukup update stok, dan terima order disekitar lokasi anda.';
    //             $this->load->css('../assets/frontend_1/css/libs/bootstrap-datepicker3.min.css');
    //             $this->load->js('../assets/frontend_1/js/bootstrap-datepicker.min.js');
    //             $this->load->js('../assets/backend/js/plugins/forms/stepy.min.js');
    //             // $this->load->js('https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js');
    //             $this->load->js(base_url('../').'assets/backend/js/modules/principle/register.js');
    //             $this->load->view('register', $this->data);
    //         }
    //     }

    // NEW METHOD REGISTER
    public function register()
    {
        $this->load->library('session');
        $this->load->helper(array('captcha', 'url'));
        if ($this->aauth->is_loggedin())
            redirect();

        $this->load->helper('captcha');

        $vals = array(
            'word' => '',
            'pool' => '0123456789',
            'word_length' => 4,
            'img_path'   => '../assets/frontend/captcha/',
            'img_url'    => base_url() . '../assets/frontend/captcha/',
            'img_width'  => '200',
            'img_height' => 35,
            'border' => 0,
            'font_size' => 26,
            'expiration' => 7200,
            'font_path' => FCPATH . 'assets/frontend/captcha/fonts/coolvetica_rg.ttf',
            'colors' => array(
                'background' => array(255, 255, 255),
                'border' => array(230, 230, 230),
                'text' => array(0, 0, 0),
                'grid' => array(255, 255, 255)
            )
        );

        // create captcha image
        $cap = create_captcha($vals);

        // store image html code in a variable
        $this->data['image'] = $cap['image'];

        // $this->session->set_userdata('mycaptcha', $cap['word']);
        $this->data['captcha_word'] = $cap['word'];
        $this->data['page'] = 'register';

        // $this->data['page'] = 'register';
        $this->data['sku'] = $this->main->gets('principle_variable', ['type' => 'sku'], 'id asc');
        $this->data['revenue'] = $this->main->gets('principle_variable', ['type' => 'revenue'], 'id asc');
        $this->data['range'] = $this->main->gets('principle_variable', ['type' => 'range'], 'id asc');
        $this->data['from'] = $this->main->gets('principle_variable', ['type' => 'from'], 'id asc');
        // $this->data['provincies'] = $this->main->gets('provincies', [], 'name asc');
        // $this->data['widget'] = $this->recaptcha->getWidget();
        // $this->data['script'] = $this->recaptcha->getScriptTag();

        $this->template->_init('auth');
        $this->template->form();
        $this->output->set_title('Daftar Principal');
        $this->data['meta_description'] = 'Daftar menjadi penjual ditokomanamana, Tanpa ribet, cukup update stok, dan terima order disekitar lokasi anda.';
        $this->load->css('../assets/frontend_1/css/libs/bootstrap-datepicker3.min.css');
        $this->load->js('../assets/frontend_1/js/bootstrap-datepicker.min.js');
        $this->load->js('../assets/backend/js/plugins/forms/stepy.min.js');
        // $this->load->js('https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js');
        $this->load->js(base_url('../') . 'assets/backend/js/modules/principle/register.js');
        $this->load->view('register', $this->data);
    }

    public function save_register()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('session');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('brand_name', 'lang:brand_name', 'required|trim');
        $this->form_validation->set_rules('company_name', 'lang:company_name', 'required|trim');
        $this->form_validation->set_rules('pic_name', 'lang:pic_name', 'required|trim');
        $this->form_validation->set_rules('email', 'lang:email', 'required|trim|valid_email');
        $this->form_validation->set_rules('owner_phone', 'lang:owner_phone', 'required');
        $this->form_validation->set_rules('company_type', 'lang:company_type', 'required|trim');
        $this->form_validation->set_rules('main_category', 'lang:main_category', 'required|trim');
        $this->form_validation->set_rules('sku_total', 'lang:sku_total', 'required|trim');
        $this->form_validation->set_rules('revenue', 'lang:revenue', 'required|trim');
        $this->form_validation->set_rules('range_price', 'lang:range_price', 'required|trim');
        $this->form_validation->set_rules('other_ecommerce', 'lang:other_ecommerce', 'required|trim');
        $this->form_validation->set_rules('know_from', 'lang:know_from', 'required|trim');
        $this->form_validation->set_rules('agree', 'Syarat dan Ketentuan', 'required|trim');
        $this->form_validation->set_rules('captcha', 'Kode Captcha', 'required');
        $this->form_validation->set_error_delimiters('<p class="text-danger" style="font-size: 14px;">', '</p>');


        if ($this->form_validation->run() === TRUE) {
            $data = $this->input->post(null, true);

            $captcha_register = $data[decode('captcha_register')];
            $captcha_register = decode($captcha_register);

            if ($data['captcha'] == $captcha_register) {

                if(!$_FILES['brand_image']) {
                    $return = [
                        'status' => 'error',
                        'message' => "Gambar Brand harus diisi!"
                    ];
                    echo json_encode($return);
                    exit();
                }

                $company_name_ = $this->generateSeoURL($data['company_name']);
                $check_company_name = $this->main->get('seo_url', ['keyword' => $company_name_]);
                if($check_company_name) {
                    $return = [
                        'status' => 'error',
                        'message' => 'Nama Principal sudah digunakan'
                    ];
                    echo json_encode($return);
                    exit();
                }

                $brand_name_ = $this->generateSeoURL($data['brand_name']);
                $check_brand_name = $this->main->get('seo_url', ['keyword' => 'brands/' . $brand_name_]);
                if($check_brand_name) {
                    $return = [
                        'status' => 'error',
                        'message' => 'Nama Merek sudah digunakan'
                    ];
                    echo json_encode($return);
                    exit();
                }

                // $check_brand = $this->main->get('seo_url', ['keyword' => 'brands/' . $data['brand_name']]);
                // if($check_brand) {
                //     $return = [
                //         'status' => 'error',
                //         'message' => "Nama brands sudah digunakan!"
                //     ];
                //     echo json_encode($return);
                //     exit();
                // }

                $data_document = '';
                $count_document_required = 0;
                foreach ($data['document'] as $document) {
                    if(($document == 'haki') || ($document == 'npwp') || ($document == 'siup') || ($document == 'akta') || ($document == 'sppkp') || ($document == 'skt')) {
                        $count_document_required++;
                    } 
                    $data_document .= $document . ', ';
                }

                if($count_document_required == 6) {

                    $document_file = [];
                    foreach($_FILES['document_file']['name'] as $key => $value) {
                        $_FILES['file']['name'] = $_FILES['document_file']['name'][$key];
                        $_FILES['file']['type'] = $_FILES['document_file']['type'][$key];
                        $_FILES['file']['tmp_name'] = $_FILES['document_file']['tmp_name'][$key];
                        $_FILES['file']['error'] = $_FILES['document_file']['error'][$key];
                        $_FILES['file']['size'] = $_FILES['document_file']['size'][$key];
                        $type_file = explode('.', $_FILES['file']['name']);

                        if($type_file[1] == 'jpg' || $type_file[1] == 'pdf') {
                            $file_name = str_replace(' ', '-', $_FILES['document_file']['name'][$key]);
                            $file_name = strtolower($file_name);
                            $explode = explode('.', $file_name);

                            $company_name = $this->generateSeoURL($data['company_name']);
                            $company_name = $company_name . '-' . $key;

                            $full_file_name = $explode[0] . '-' . $company_name . '.' . $explode[1];
                            if(file_exists('../files/principal_register/' . $full_file_name)) {
                                unlink('../files/principal_register/' . $full_file_name);
                            }

                            $config['upload_path'] = '../files/principal_register/';
                            $config['allowed_types'] = '*';
                            $config['max_size'] = '2048';
                            $config['file_name'] = $full_file_name;

                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('file')) {
                                $return = [
                                    'status' => 'error',
                                    'message' => $this->upload->display_errors()
                                ];
                                echo json_encode($return);
                                exit();
                            } else {
                                $upload_data = $this->upload->data();
                                $fileName = $upload_data['file_name'];
                                $document_file[$key] = $fileName;
                            }
                        } else {
                            $return = [
                                'status' => 'error',
                                'message' => 'Jenis file harus JPG/PDF'
                            ];
                            echo json_encode($return);
                            exit();
                        }
                    }

                    $data_document_file = json_encode($document_file);

                    $config['upload_path']          = '../files/images/BRAND/';
                    $config['allowed_types']        = 'jpg|png';
                    $config['max_size']             = 2048;

                    $old_filename = $_FILES['brand_image']['name'];
                    // $brand_image_name = explode('.', $brand_image_name);
                    $today = date('Y_m_d');
                    $rand = rand(1, 10);
                    $extension = substr($old_filename, strpos($old_filename, ".") + 1);
                    // $brand_image_name_ = $brand_image_name[0] . '-' . $today . '.' . $brand_image_name[1];
                    // $config['file_name'] = $brand_image_name_;

                    $brand_image_name_ = 'brand_principal_' . $rand . '_' . $this->generateSeoURL($data['company_name']) . '_' . $today . '.' . $extension;
                    $config['file_name'] = $brand_image_name_;

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('brand_image')) {
                        $return = [
                            'status' => 'error',
                            'message' => $this->upload->display_errors()
                        ];
                        echo json_encode($return);
                        exit();
                    } else {
                        $upload_data = $this->upload->data();
                        $brand_image_name__ = $brand_image_name_;
                    }

                    $password = $this->aauth->hash_password($data['password'], null);

                    $this->main->insert('principle_register', array(
                        'brand_name' => $data['brand_name'],
                        'brand_image' => $brand_image_name__,
                        'company_name' => $data['company_name'],
                        'owner_name' => $data['pic_name'],
                        'owner_email' => $data['email'],
                        'owner_phone' => $data['owner_phone'],
                        'company_type' => $data['company_type'],
                        'main_category' => $data['main_category'],
                        'sku_total' => $data['sku_total'],
                        'revenue' => $data['revenue'],
                        'document' => $data_document,
                        'instagram_acc' => $data['instagram_acc'],
                        'other_ecommerce' => $data['other_ecommerce'],
                        'range_price' => $data['range_price'],
                        'know_from' => $data['know_from'],
                        'custom_know_form' => $data['custom_know_form'],
                        'status' => 0,
                        'document_file' => $data_document_file,
                        // 'username' => $data['username'],
                        'password' => $password
                    ));

                    if ($this->db->affected_rows() > 0) {
                        $check_principal = $this->main->get('principle_register', ['owner_email' => $data['email'], 'status' => 2]);
                        if($check_principal) {
                            $this->main->delete('principle_register', ['id' => $check_principal->id]);
                            $return = [
                                'status' => 'success',
                                'title' => 'Berhasil mendaftar!',
                                'message' => 'Silahkan tunggu email konfirmasi'
                            ];
                        } else{
                            $return = [
                                'status' => 'success',
                                'title' => 'Berhasil mendaftar!',
                                'message' => 'Silahkan tunggu email konfirmasi'
                            ];
                        }
                    } else {
                        $return = [
                            'status' => 'error',
                            'message' => 'Terjadi Suatu Kesalahan!'
                        ];
                    }
                } else {
                    $return = [
                        'status' => 'error',
                        'message' => 'Terdapat dokumen wajib yang tidak diisi!'
                    ];
                }
                
            } else {

                $this->load->helper(array('captcha', 'url'));
                $this->session->unset_userdata('mycaptcha_principal');

                $this->load->helper('captcha');

                $vals = array(
                    'word' => '',
                    'pool' => '0123456789',
                    'word_length' => 4,
                    'img_path'   => '../assets/frontend/captcha/',
                    'img_url'    => base_url() . '../assets/frontend/captcha/',
                    'img_width'  => '200',
                    'img_height' => 35,
                    'border' => 0,
                    'font_size' => 26,
                    'expiration' => 7200,
                    'font_path' => FCPATH . 'assets/frontend/captcha/fonts/coolvetica_rg.ttf',
                    'colors' => array(
                        'background' => array(255, 255, 255),
                        'border' => array(230, 230, 230),
                        'text' => array(0, 0, 0),
                        'grid' => array(255, 255, 255)
                    )
                );

                // create captcha image
                $cap = create_captcha($vals);

                // $this->session->set_userdata('mycaptcha_principal', $cap['word']);
                $captcha_word = encode($cap['word']);
                $new_captcha = $cap['image'];

                $return = [
                    'status' => 'error_captcha',
                    'message' => 'Kode captcha salah!',
                    'image' => $new_captcha,
                    'cap_word' => $captcha_word
                ];
            }
        } else {

            $return = [
                'status' => 'error',
                'message' => validation_errors()
            ];
        }

        echo json_encode($return);
    }

    public function check_phone()
    {
        $phone = $this->input->post('phone');
        $check_phone = $this->main->get('principles', ['owner_phone' => $phone]);
        if ($check_phone) {
            echo "false";
        } else {
            echo "true";
        }
    }

    public function request_token_principle()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $phone = $this->input->post('phone');
        //$phone = '087777546456';
        $check = $this->main->get('phone_verification', array('phone' => $phone));
        $return = ['status' => 'error', 'message' => ''];
        $date = date('Y-m-d H:i:s');
        $this->load->library('sprint');
        $sms = json_decode(settings('sprint_sms'), true);
        $code = rand(1000, 9999);
        $sms['m'] = 'Hai, berikut kode verifikasi No HP Principal Tokomanamana Anda ' . $code;
        $sms['d'] = $phone;
        $url = $sms['url'];
        unset($sms['url']);
        //send_mail('noreply@tokomanamana.com','Tokomanamana',$check_user->email, 'Kode verifikasi login merchant', $sms['m']);
        $sprint_response = $this->sprint->sms($url, $sms);
        // if($sprint_response) {
        //     if ($check) {
        //         $this->main->update('phone_verification', array('verification_code' => $code, 'verification_sent_time' => date('Y-m-d H:i:s')), array('phone' => $phone));
        //     } else {
        //         $this->main->insert('phone_verification', array(
        //             'phone' => $phone,
        //             'verification_code' => $code,
        //             'verification_sent_time' => $date,
        //         ));
        //     }
        //     $return['status'] = 'success';
        //     $return['message'] = 'Kode OTP berhasil dikirim!';
        // } else {
        //     $return['status'] = 'error';
        //     $return['message'] = 'OTP Gagal Dikirim';
        // }
        if ($check) {
            $this->main->update('phone_verification', array('verification_code' => $code, 'verification_sent_time' => date('Y-m-d H:i:s')), array('phone' => $phone));
        } else {
            $this->main->insert('phone_verification', array(
                'phone' => $phone,
                'verification_code' => $code,
                'verification_sent_time' => $date,
            ));
        }
        $return['status'] = 'success';
        $return['message'] = 'Kode OTP berhasil dikirim!';
        echo json_encode($return);
    }

    public function check_token_principal()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $phone = $this->input->post('phone');
        $code = $this->input->post('code');
        $checking = $this->main->get('phone_verification', array('phone' => $phone, 'verification_code' => $code));
        if ($checking) {
            $return['status'] = 'success';
            $return['message'] = 'Verifikasi berhasil!';
        } else {
            $return['status'] = 'failed';
            $return['message'] = 'Kode OTP tidak sesuai!';
        }
        echo json_encode($return);
    }

    public function check_email_ajax()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $email = $this->input->post('email');
        $check_email1 = $this->main->get('principle_register', ['owner_email' => $email, 'status !=', 2]);
        $check_email2 = $this->main->get('aauthp_users', ['email' => $email]);

        if ($check_email1 || $check_email2) {
            echo "false";
        } else {
            echo "true";
        }
    }

    public function check_company_name() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $company_name = $this->generateSeoURL($this->input->post('company_name'));
        $check_company_name = $this->main->get('seo_url', ['keyword' => $company_name]);
        if($check_company_name) {
            echo 'false';
        } else {
            echo 'true';
        }
    }

    public function check_brand_name() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $brand_name = $this->generateSeoURL($this->input->post('brand_name'));
        $check_brand_name = $this->main->get('seo_url', ['keyword' => 'brands/' . $brand_name]);
        if($check_brand_name) {
            echo 'false';
        } else {
            echo 'true';
        }
    }

    public function email_confirm($token = null)
    {
        $this->load->library('session');

        if ($token) {
            $principle = $this->main->get('principle_register', ['token_confirm' => $token, 'token_confirm_status' => 1]);
            if ($principle) {
                $today = date('Y-m-d H:i:s');
                $today = date('Y-m-d H:i:s', strtotime($today));
                if($principle->due_date_confirmation >= $today) {
                    $this->main->update('principle_register', ['status' => 1], ['id' => $principle->id]);
                    $insert_principle = [
                        'name' => $principle->company_name,
                        'status' => 1,
                        'viewed' => 0,
                        'owner_phone' => $principle->owner_phone
                    ];
                    $tb_principle = $this->main->insert('principles', $insert_principle);
                    if ($tb_principle) {
                        $username_authp = $this->generateSeoURL($principle->company_name);
                        $data_aauthp = [
                            'email' => $principle->owner_email,
                            'pass' => $principle->password,
                            'username' => $username_authp,
                            'phone' => $principle->owner_phone,
                            'principle_id' => $tb_principle,
                            'fullname' => $principle->owner_name
                        ];
                        $image_brands = 'BRAND/' . $principle->brand_image;
                        $data_brands = [
                            'name' => $principle->brand_name,
                            'status' => 1,
                            'principle_id' => $tb_principle,
                            'image' => $image_brands
                        ];
                        $aauthp = $this->main->insert('aauthp_users', $data_aauthp);
                        $this->main->update('principles', ['auth' => $aauthp], ['id' => $tb_principle]);
                        $this->main->update('principle_register', ['token_confirm_status' => 0], ['id' => $principle->id]);
                        $id_brand = $this->main->insert('brands', $data_brands);

                        $seo_url_ = $this->generateSeoURL($principle->brand_name);

                        $data_principal_seo_url = [
                            'query' => 'principal_home/view/' . $tb_principle,
                            'keyword' => $username_authp
                        ];

                        $data_brand_seo_url = [
                            'query' => 'catalog/brands/view/' . $id_brand,
                            'keyword' => 'brands/' . $seo_url_
                        ];

                        // create direktory
                        $dir = FCPATH .'../files/images/principal/'.$username_authp;
                        if (!is_dir($dir)) {
                            mkdir($dir,0777,TRUE);
                        }
                        // $dir_brands = FCPATH .'../files/images/principal/'.$username_authp.'/brands';
                        // if(!is_dir($dir_brands)) {
                        //     mkdir($dir_brands,0777,TRUE);
                        // }

                        // $srcfile_brands = FCPATH . '../files/images/BRAND/' . $principle->brand_image;
                        // $destfile_brands = $dir_brands . '/' . $principle->brand_image;
                        // shell_exec("cp -r $srcfile_brands $destfile_brands");


                        $this->main->insert('seo_url', $data_principal_seo_url);
                        $this->main->insert('seo_url', $data_brand_seo_url);
                        $this->main->insert('aauthp_user_to_group', ['user_id' => $aauthp, 'group_id' => 1]);
                        $this->session->set_flashdata('email_confirm_message', $this->message('Akun Berhasil Diaktifkan!.', 'success'));
                        redirect('auth/login');
                        
                    } else {
                        $this->session->set_flashdata('email_confirm_message', $this->message('Gagal Aktivasi Akun!.', 'failed'));
                        redirect('auth/login');
                    }
                } else {
                    $this->main->delete('phone_verification', ['phone' => $principle->owner_phone]);
                    $this->main->delete('principle_register', ['id' => $principle->id]);
                    $this->session->set_flashdata('email_confirm_message', $this->message('Konfirmasi Telah Kadaluarsa. Silahkan Daftar Ulang!.', 'failed'));
                    redirect('auth/login');
                }
            } else {
                $check_principal = $this->main->get('principle_register', ['token_confirm' => $token, 'status' => 1]);
                if($check_principal) {
                    $this->session->set_flashdata('email_confirm_message', $this->message('Akun Principal Sudah Diaktifkan!.', 'success'));
                    redirect('auth/login');
                } else {
                    $this->session->set_flashdata('email_confirm_message', $this->message('Akun Principal Tidak Tersedia!.', 'failed'));
                    redirect('auth/login');
                }
            }
        } else {
            $this->session->set_flashdata('email_confirm_message', $this->message('Token Konfirmasi Bermasalah!. Silahkan hubungi CS Tokomanamana', 'failed'));
            redirect('auth/login');
        }
    }

    private function message($str, $type) {
        if($type == 'success') {
            $return = '<div class="alert alert-success show" role="alert">
                ' . $str . '
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>';
        } elseif($type == 'failed') {
            $return = '<div class="alert alert-danger show" role="alert">
                ' . $str . '
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>';
        }
        return $return;
    }

    private function generateSeoURL($string, $wordLimit = 0){
        $separator = '-';
        
        if($wordLimit != 0){
            $wordArr = explode(' ', $string);
            $string = implode(' ', array_slice($wordArr, 0, $wordLimit));
        }
    
        $quoteSeparator = preg_quote($separator, '#');
    
        $trans = array(
            '&.+?;'                    => '',
            '[^\w\d _-]'            => '',
            '\s+'                    => $separator,
            '('.$quoteSeparator.')+'=> $separator
        );
    
        $string = strip_tags($string);
        foreach ($trans as $key => $val){
            $string = preg_replace('#'.$key.'#i'.(UTF8_ENABLED ? 'u' : ''), $val, $string);
        }
    
        $string = strtolower($string);
    
        return trim(trim($string, $separator));
    }
}
