<style>
    .wrapper {
        position: relative;
        width: 350px;
        height: 200px;
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    body {
        background-image: url('<?php echo site_url("../files/images/bg_register.jpeg") ?>');
        background-size: cover;
        background-attachment: fixed;
    }

    .signature-pad {
        position: absolute;
        left: 0;
        top: 0;
        width: 350px;
        height: 200px;
        background-color: #ccc;
    }

    .btn-clear-canvas {
        position: relative;
        width: 350px;
        height: 200px;
        text-align: right;
    }

    @media screen and (max-width: 480px) {
        .wrapper {
            width: 230px;
            height: 150px;
        }

        .signature-pad {
            width: 230px;
            height: 150px;
        }

        .btn-clear-canvas {
            width: 230px;
            height: 150px;
        }
    }

    .btn-is-disabled {
        pointer-events: none;
    }
</style>
<div class="content">
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <div class="panel panel-body">
                <h3>Form Pengajuan Ulang Data</h3>
                <form class="stepy form-horizontal " action="<?php echo site_url('auth/save_information_rejected'); ?>" method="post" id="send_data" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-md-12 control-label">Informasi Tambahan</label>
                        <div class="col-md-12">
                            <!-- <input type="text" class="form-control" required name="brand_name" placeholder="<?php echo lang('brand_name_placeholder'); ?>"> -->
                            <textarea class="form-control" name="other_information" placeholder="Masukkan informasi yang dibutuhkan" required></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12 control-label">Dokumen Tambahan</label>
                        <div class="col-md-12">
                            <div class="custom-file"></div>
                            <button class="btn btn-info add-file-btn" type="button" style="margin-top: 10px;">Tambah Berkas</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class='image col-md-6'>
                            <label class="control-label">Kode Captcha</label>
                            <br>
                            <div id="img_capt">
                                <?= $image ?>
                            </div>
                            <a href='#' class='refresh'></a>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Ketik Captcha</label>
                            <input type="text" name="captcha" class="form-control" placeholder="Masukkan Kode Captcha" required>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="<?= $id ?>">
                    <button type="submit" id="submit" class="btn btn-primary stepy-finish">Kirim <i class="icon-floppy-disk position-right"></i></button>
                </form>
            </div>
        </div>
    </div>

</div>