<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('heading'); ?></h2>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-sm-12">
                <form action="<?php echo site_url('settings/save'); ?>" class="form-horizontal" method="post" id="form">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="tabbable">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="general">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?php echo lang('principal_name'); ?> </label>
                                            <div class="col-sm-6">
                                                <input class="form-control" type="text" required="" name="name" value="<?php echo $data->name ?>" placeholder="<?php echo lang('principal_name_placeholder') ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?php echo lang('description'); ?> </label>
                                            <div class="col-sm-6">
                                                <textarea class="form-control" name="description" placeholder="<?php echo lang('description_placeholder') ?>"><?php echo $data->description ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('address_label'); ?></label>
                                            <div class="col-md-6">
                                                <textarea cols="30" rows="2" class="form-control" id="address" name="address" placeholder="<?php echo lang('address_placeholder') ?>"><?php echo $data->address; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('province_label'); ?></label>
                                            <div class="col-md-4">
                                                <select class="form-control"  name="province" id="province" data-live-search="true" data-width="100%">
                                                    <option value="">Pilih Provinsi</option>
                                                    <?php if ($provinces) { ?>
                                                        <?php foreach ($provinces->result() as $province) { ?>
                                                            <option value="<?php echo $province->id; ?>" <?php echo ($data->province == $province->id) ? 'selected' : ''; ?>><?php echo $province->name; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('city_label'); ?></label>
                                            <div class="col-md-4">
                                                <select class="form-control" name="city" id="city" data-live-search="true" data-width="100%">
                                                    <option value="">Pilih Kota</option>
                                                    <?php if ($cities) { ?>
                                                        <?php foreach ($cities->result() as $city) { ?>
                                                            <option value="<?php echo $city->id; ?>" <?php echo ($data->city == $city->id) ? 'selected' : ''; ?>><?php echo $city->name; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('district_label'); ?></label>
                                            <div class="col-md-4">
                                                <select class="form-control"  name="district" id="district" data-live-search="true" data-width="100%">
                                                    <option value="">Pilih Kecamatan</option>
                                                    <?php if ($districts) { ?>
                                                        <?php foreach ($districts->result() as $district) { ?>
                                                            <option value="<?php echo $district->id; ?>" <?php echo ($data->district == $district->id) ? 'selected' : ''; ?>><?php echo $district->name; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('meta_keyword'); ?></label>
                                            <div class="col-md-6">
                                                <input type="text" name="meta_keyword" class="tags-input" value="<?php echo ($data) ? $data->meta_keyword : ''; ?>" placeholder="Masukkan Kata Kunci">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>