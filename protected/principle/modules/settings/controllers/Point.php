<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Point extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->aauth->control('setting/point');
        //$this->lang->load('point', settings('language'));
        $this->load->model('point_model', 'point');
        $this->load->model('Model');

        $this->data['menu'] = 'setting_point';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('setting'), '/');
        $this->breadcrumbs->push(lang('point'), '/settings/point');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title('Point');
        $this->load->js('../assets/backend/js/modules/settings/point.js');
        $this->load->view('point/point', $this->data);
    }

    public function filter_kategori(){
        $m = $this->Model;
        $search = $this->input->post('search');

        $like['key'] = 'name';
        $like['value'] = $search;
        $where = array(
            'active' => '1',
            'parent' => '0'
        );
        $q = $m->get_data('', 'categories', null, $where, 'name', 'name asc, parent desc', 10, 0, null, $like);

        if($q->num_rows() > 0){
            $data = array();

            foreach ($q->result() as $key) {
                $data[count($data)] = array(
                    'id' => $key->id,
                    'name' => $key->name,
                    'tipe' => $key->parent == 0 ? 1 : 2
                );
            }
            
            die(json_encode(array('success' => '1', 'data' => $data)));
        }
        die(json_encode(array('success' => '0', 'msg' => 'Data Tidak Ada')));
    }

    public function filter_sub_kategori(){
        $m = $this->Model;
        $search = $this->input->post('search');
        $parent = $this->input->post('parent');

        $like['key'] = 'name';
        $like['value'] = $search;
        $where = array(
            'active' => '1'
        );

        if($parent != ''){
            $where['parent'] = $parent;
        }else{
            $where['parent!='] = '0';
        }

        $q = $m->get_data('', 'categories', null, $where, 'name', 'name asc, parent desc', 10, 0, null, $like);

        if($q->num_rows() > 0){
            $data = array();

            foreach ($q->result() as $key) {
                $where0 = array('active' => '1', 'id' => $key->parent);
                $q0 = $m->get_data('', 'categories', null, $where0);

                $data[count($data)] = array(
                    'id' => $key->id,
                    'name' => $key->name,
                    'tipe' => ($key->parent == 0 ? 1 : 2),
                    'has_parent' => ($q->num_rows() > 0 ? 1 : 0),
                    'parent' => ($q0->num_rows() > 0 ? $q0->row() : null)
                );
            }
            
            die(json_encode(array('success' => '1', 'data' => $data)));
        }
        die(json_encode(array('success' => '0', 'msg' => 'Data Tidak Ada')));
    }

    public function filter_item(){
        $m = $this->Model;
        $search = $this->input->post('search');
        $kategori = $this->input->post('kategori');
        $sub_kategori = $this->input->post('sub_kategori');

        $like['key'] = 'name';
        $like['value'] = $search;
        $where = array();
        $where0 = array();
        if($sub_kategori != ''){
            $where['category'] = $sub_kategori;
        }else if($kategori != ''){
            $q_sub_category = $m->get_data('', 'categories', null, array('parent' => $kategori), '', 'name asc');
            $where0[0] = "category=".$kategori;
            foreach ($q_sub_category->result() as $key) {
                $where0[0] .= " or category=".$key->id;
            }
        }
        $q = $m->get_data('', 'products', null, $where, '', 'name asc', 10, 0, $where0, $like);

        if($q->num_rows() > 0){
            $data = array();

            foreach ($q->result() as $key) {
                $q_kategori = $m->get_data('', 'categories', null, array('id' => $key->category));
                $kategorinya = null;
                $sub_kategorinya = null;

                $has_category = 0;
                $has_sub_category = 0;
                if($q_kategori->num_rows() > 0){
                    if($q_kategori->row()->parent != 0){
                        $has_sub_category = 1;
                        $has_category = 1;
                        $sub_kategorinya = $q_kategori->row();
                        $kategorinya = $m->get_data('', 'categories', null, array('id' => $q_kategori->row()->parent))->row();
                    }else{
                        $kategorinya = $q_kategori->row();
                        $has_category = 1;
                    }
                }
                $data[count($data)] = array(
                    'id' => $key->id,
                    'name' => $key->name,
                    'tipe' => 3,
                    'has_category' => $has_category,
                    'kategorinya' => $kategorinya,
                    'has_sub_category' => $has_sub_category,
                    'sub_kategorinya' => $sub_kategorinya
                );
            }
            
            die(json_encode(array('success' => '1', 'data' => $data)));
        }
        die(json_encode(array('success' => '0', 'msg' => 'Data Tidak Ada')));
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $m = $this->Model;
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $additional = $this->input->post('additional_data');
        $draw = intval($this->input->post('draw'));

        //die(json_encode(array('a' => $order)));

        $additional['merchant'] = isset($additional['merchant']) ? $additional['merchant'] : '';
        $additional['kategori'] = isset($additional['kategori']) ? $additional['kategori'] : '';
        $additional['sub_kategori'] = isset($additional['sub_kategori']) ? $additional['sub_kategori'] : '';
        $additional['item'] = isset($additional['item']) ? $additional['item'] : '';

        $additional['merchant'] = $additional['merchant'] == '1' ? 'c' : ($additional['merchant'] == '0' ? 'p' : 'cust');
        //die(json_encode(array('asd' => $additional)));


        $column[0] = 'sp.id_general '.$order['dir'];
        $column[1] = $additional['item'] != '' ? 'p.name '.$order['dir'] : 'c.name '.$order['dir'].', p.name '.$order['dir'];
        $column[2] = 'sp.tipe_item '.$order['dir'];
        $column[3] = 'sp.tipe_merchant '.$order['dir'];
        $column[4] = 'sp.point '.$order['dir'];

        $where = array(
            'sp.is_active' => '1',
            'sp.tipe_merchant' => $additional['merchant']
        );

        $output['data'] = array();

        if($additional['item'] != ''){
            $where['sp.tipe_item'] = 'i';
            $where['sp.id_general'] = $additional['item'];
            $join[0] = 'products p-p.id=sp.id_general';
            $datas = $m->get_data('sp.*, p.name as p_name', 'setting_point sp', $join, $where, '', $column[$order['column']], $length, $start);

            $datas_count = $m->get_data('sp.*, p.name as p_name', 'setting_point sp', $join, $where, '', '')->num_rows();
        }else if($additional['sub_kategori'] != ''){
            $join = array(
                0 => 'products p-p.id=sp.id_general',
                1 => 'categories c-c.id=sp.id_general'
            );

            $q_product = $m->get_data('id', 'products', null, array('category' => $additional['sub_kategori']));
            $where0 = "(sp.tipe_item='sk' and sp.id_general='".$additional['sub_kategori']."')";
            foreach ($q_product->result() as $key) {
                $where0 .= " or (sp.tipe_item='i' and sp.id_general='".$key->id."')";
            }
            //die(json_encode(array('a' => $where0)));
            $datas = $m->get_data('sp.*, c.name as c_name, p.name as p_name', 'setting_point sp', $join, $where, '', $column[$order['column']], $length, $start, array(0 => $where0));

            $datas_count = $m->get_data('sp.*, c.name as c_name, p.name as p_name', 'setting_point sp', $join, $where, '', '', 0, 0, array(0 => $where0))->num_rows();
        }else if($additional['kategori'] != ''){
            // $where['sp.tipe_item'] = 'k';
            // $where['sp.id_general'] = $additional['kategori'];
            $join = array(
                0 => 'products p-p.id=sp.id_general',
                1 => 'categories c-c.id=sp.id_general'
            );

            $q_sub_category = $m->get_data('id', 'categories', null, array('parent' => $additional['kategori']));
            $where0 = "(sp.tipe_item='k' and sp.id_general='".$additional['kategori']."')";
            foreach ($q_sub_category->result() as $key) {
                $where0 .= " or (sp.tipe_item='sk' and sp.id_general='".$key->id."')";

                $q_product = $m->get_data('id', 'products', null, array('category' => $key->id));
                foreach ($q_product->result() as $key0) {
                    $where0 .= " or (sp.tipe_item='i' and sp.id_general='".$key0->id."')";
                }
            }
            $datas = $m->get_data('sp.*, c.name as c_name, p.name as p_name', 'setting_point sp', $join, $where, '', $column[$order['column']], $length, $start, array(0 => $where0));

            $datas_count = $m->get_data('sp.*, c.name as c_name, p.name as p_name', 'setting_point sp', $join, $where, '', '', 0, 0, array(0 => $where0))->num_rows();
        }else{
            $join = array(
                0 => 'products p-p.id=sp.id_general',
                1 => 'categories c-c.id=sp.id_general'
            );
            $datas = $m->get_data("sp.*, p.name as p_name, c.name as c_name", 'setting_point sp', $join, $where, '', $column[$order['column']], $length, $start);

            $datas_count = $m->get_data("sp.*, p.name as p_name, c.name as c_name", 'setting_point sp', $join, $where)->num_rows();

            //die(json_encode(array('a' => $datas->result())));
        }
        
        //$datas = $this->point->get_all($start, $length, $search, $order, $additional);
        if ($datas) {
            $cnt = 0;
            foreach ($datas->result() as $data) {
                $name = $data->tipe_item == 'g' ? 'Global' : ($data->tipe_item == 'i' ? $data->p_name : $data->c_name);
                $tipe_item = $data->tipe_item == 'g' ? 'Global' : ($data->tipe_item == 'k' ? 'Kategori Barang' : ($data->tipe_item == 'sk' ? 'Sub Kategori Barang' : 'Barang'));
                $tipe_merchant = $data->tipe_merchant == 'p' ? 'Merchant' : ($data->tipe_merchant == 'c' ? 'Downline' : 'Customer');

                $output['data'][] = array(
                    $data->id_general.'<input type="hidden" id="id_general'.$cnt.'" value="'.$data->id_general.'"/>
                    <input type="hidden" id="id_setting_poin'.$cnt.'" value="'.$data->id.'"/>',
                    $name.'<input type="hidden" id="name'.$cnt.'" value="'.$name.'"/>',
                    $tipe_item.'<input type="hidden" id="tipe_item'.$cnt.'" value="'.$tipe_item.'"/>',
                    $tipe_merchant.'<input type="hidden" id="tipe_merchant'.$cnt.'" value="'.$tipe_merchant.'"/>',
                    $data->point.'<input type="hidden" id="point'.$cnt.'" value="'.$data->point.'"/>',
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#" onclick="edit_form('.$cnt.')">' . lang('button_edit') . '</a></li>
                    <li><a href="' . site_url('settings/point/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>
                    </ul>
                    </li>
                    </ul>',
                );
                $cnt++;
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $m->get_data('', 'setting_point', null, array('is_active' => '1'))->num_rows();
        $output['recordsFiltered'] = $datas_count;
        echo json_encode($output);
    }

    public function form($id = '') {
        $this->template->_init();
        $this->template->form();

        $this->data['data'] = array();
        if ($id) {
            $id = decode($id);
            $this->data['data'] = $this->main->get('point', array('id' => $id));
        }

        $this->breadcrumbs->unshift(lang('setting'), '/');
        $this->breadcrumbs->push(lang('point'), 'settings/point');
        $this->breadcrumbs->push(($this->data['data']) ? 'Edit Point' : 'Tambah Point', '/');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(($this->data['data']) ? 'Edit Point' : 'Tambah Point');
        $this->load->view('point/point_form', $this->data);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $m = $this->Model;

        $kategori = $this->input->post('kategori');
        $sub_kategori = $this->input->post('sub_kategori');
        $item = $this->input->post('item');
        $merchant = $this->input->post('merchant');
        $point = $this->input->post('point');

        $merchant = $merchant == '1' ? 'c' : ($merchant == '0' ? 'p' : 'cust');

        if($item != ''){
            $cek = $m->get_data('', 'setting_point', null, array('id_general' => $item, 'tipe_item' => 'i', 'tipe_merchant' => $merchant))->num_rows();

            if($cek > 0){
                die(json_encode(array('success' => '0', 'msg' => 'Data Sudah Ada')));
            }
            $data_ins = array(
                'id_general' => $item,
                'tipe_item' => 'i',
                'is_active' => '1',
                'tipe_merchant' => $merchant,
                'point' => $point
            );

            $m->insert_data('setting_point', $data_ins);
        }else if($sub_kategori != ''){
            $cek = $m->get_data('', 'setting_point', null, array('id_general' => $sub_kategori, 'tipe_item' => 'sk', 'tipe_merchant' => $merchant))->num_rows();

            if($cek > 0){
                die(json_encode(array('success' => '0', 'msg' => 'Data Sudah Ada')));
            }

            $data_ins = array(
                'id_general' => $sub_kategori,
                'tipe_item' => 'sk',
                'is_active' => '1',
                'tipe_merchant' => $merchant,
                'point' => $point
            );

            $m->insert_data('setting_point', $data_ins);
        }else if($kategori != ''){
            $cek = $m->get_data('', 'setting_point', null, array('id_general' => $kategori, 'tipe_item' => 'k', 'tipe_merchant' => $merchant))->num_rows();

            if($cek > 0){
                die(json_encode(array('success' => '0', 'msg' => 'Data Sudah Ada')));
            }

            $data_ins = array(
                'id_general' => $kategori,
                'tipe_item' => 'k',
                'is_active' => '1',
                'tipe_merchant' => $merchant,
                'point' => $point
            );

            $m->insert_data('setting_point', $data_ins);
        }else{
            $cek = $m->get_data('', 'setting_point', null, array('id_general' => '0', 'tipe_item' => 'g', 'tipe_merchant' => $merchant))->num_rows();

            if($cek > 0){
                die(json_encode(array('success' => '0', 'msg' => 'Data Sudah Ada')));
            }

            $data_ins = array(
                'id_general' => '0',
                'tipe_item' => 'g',
                'is_active' => '1',
                'tipe_merchant' => $merchant,
                'point' => $point
            );

            $m->insert_data('setting_point', $data_ins);
        }

        die(json_encode(array('success' => '1')));
    }

    public function save_edit(){
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $id_setting_poin = $this->input->post('id_setting_poin');
        $point = $this->input->post('point');

        $m = $this->Model;

        $m->update_data('setting_point', array('point' => $point), array('id' => $id_setting_poin));

        die(json_encode(array('success' => '1')));
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $data = $this->main->get('setting_point', array('id' => $id));

        $delete = $this->Model->delete_data1('setting_point', array('id' => $id));
        //$delete = $this->main->delete('point', array('id' => $id));
        if ($delete) {
            $return = array('message' => 'Data berhasil dihapus', 'status' => 'success');
        } else {
            $return = array('message' => lang('point_delete_error_message'), 'status' => 'error');
        }

        echo json_encode($return);
    }

    public function check_query($str) {
        if ($this->input->post('id')) {
            $check = $this->main->get('point', array('query' => $str, 'id !=' => decode($this->input->post('id'))));
        } else {
            $check = $this->main->get('point', array('query' => $str));
        }
        if ($check) {
            $this->form_validation->set_message('check_query', sprintf(lang('point_query_exist_message'), $str));
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function check_keyword($str) {
        if ($this->input->post('id')) {
            $check = $this->main->get('point', array('keyword' => $str, 'id !=' => decode($this->input->post('id'))));
        } else {
            $check = $this->main->get('point', array('keyword' => $str));
        }
        if ($check) {
            $this->form_validation->set_message('check_keyword', sprintf(lang('point_keyword_exist_message'), $str));
            return FALSE;
        } else {
            return TRUE;
        }
    }

}
