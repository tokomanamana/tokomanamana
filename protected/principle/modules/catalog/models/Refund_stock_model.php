<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Refund_stock_model extends CI_Model {

    function get_all($start = 0, $length, $search = '', $order = array(), $principal_id) {
        $this->_where_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select("p.code, p.name, m.name AS cabang, pph.quantity_revisi AS new_quantity, pph.date_added, pph.id")
                ->join('products p', 'p.id = pph.product_id', 'left')
                ->join('merchants m', 'm.id = pph.branch_id')
                ->where('pph.principal_id', $principal_id)
                ->where('quantity_revisi > 0');

        return $this->db->get('products_principal_history pph');
    }

    private function _get_alias_key($key) {
        switch ($key) {
            case 1: $key = 'p.code';
                break;
            case 2: $key = 'p.name';
                break;
            case 3: $key = 'm.name';
                break;
            case 4: $key = 'pph.quantity_revisi';
                break;
            case 5: $key = 'pph.date_added';
                break;
        }
        return $key;
    }

    function count_all($search = '', $principal_id) {
        $this->_where_like($search);
        $this->db->join('products p', 'p.id = pph.product_id', 'left')
                ->join('merchants m', 'm.id = pph.branch_id')
                ->where('pph.principal_id', $principal_id)
                ->where('quantity_revisi > 0');

        return $this->db->count_all_results('products_principal_history pph');
    }

    private function _where_like($search = '') {
        $columns = array('p.code', 'p.name');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $col) {
                $this->db->or_like("IFNULL($col,'')", $search);
            }
            $this->db->group_end();
        }
    }

}