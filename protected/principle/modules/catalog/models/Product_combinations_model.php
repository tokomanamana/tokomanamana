<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product_combinations_model extends CI_Model {

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->_where_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('p.id, code, name, cost, price, quantity, pi.image')
                ->join('product_image pi', 'p.id = pi.product', 'left')
                ->where('pi.primary', 1)
                ->limit($length, $start);

        return $this->db->get('products p');
    }

    private function _get_alias_key($key) {
        switch ($key) {
            case 2: $key = 'code';
                break;
            case 3: $key = 'name';
                break;
            case 4: $key = 'cost';
                break;
            case 5: $key = 'price';
                break;
            case 6: $key = 'quantity';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->_where_like($search);
        return $this->db->count_all_results('products');
    }

    private function _where_like($search = '') {
        $columns = array('name');
        if ($search) {
            foreach ($columns as $column) {
                $this->db->like('IFNULL(' . $column . ',"")', $search);
            }
        }
    }

    function get_store_quantity($merchant, $product = 0) {
        $this->db->select('ms.id store, ms.merchant, ms.name, IFNULL(ps.quantity,0) quantity', FALSE)
                ->join('product_store ps', 'ms.id = ps.store AND ps.product = ' . $product, 'left')
                ->where('ms.merchant', $merchant);
        $query = $this->db->get('merchant_store ms');
        return($query->num_rows() > 0) ? $query : false;
    }

    function get_categories() {
        $this->db->select("pcp.category id, GROUP_CONCAT(pc2.name ORDER BY pcp.level SEPARATOR ' > ') name", FALSE)
                ->join('categories pc1', 'pcp.category = pc1.id', 'left')
                ->join('categories pc2', 'pcp.path = pc2.id', 'left')
//                ->join('product_category_description pcd1', 'pcp.path = pcd1.id', 'left')
//                ->join('product_category_description pcd2', 'pcp.category = pcd2.id')
                ->order_by('pc2.name')
                ->group_by('pcp.category');

        return $this->db->get('category_path pcp');
    }

    function get_product_features($product) {
        $this->db->select('pf.product, pf.value, f.*')
                ->join('features f', 'pf.feature = f.id', 'left')
                ->where('pf.product', $product)
                ->group_by('pf.feature');
        return $this->db->get('product_feature pf');
    }

}
