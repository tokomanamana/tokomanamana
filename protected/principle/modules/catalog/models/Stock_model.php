<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Stock_model extends CI_Model {

    function get($id) {
        $this->db->select('p.*, keyword seo_url')
                ->join('seo_url su', "su.query = CONCAT('catalog/products/view/',p.id)", 'left')
                // ->join('brands b', 'b.id = p.brand', 'left')
                // ->where('b.principle_id',$this->data['user']->principle_id)
                ->where('p.id', $id);
        $query = $this->db->get('products p');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    function get_stock($id){
        $this->db->select('m.name, m.id, IFNULL((SELECT quantity FROM products_principal_stock pps WHERE pps.branch_id = m.id AND pps.product_id = '.$id.'),0) as quantity')
                ->where('m.principal_id', $this->data['user']->principle_id);

        return $this->db->get('merchants m');
    }

    function get_stock_option($id) {
        $this->db->select('m.name, m.id, IFNULL((SELECT pps.quantity FROM products_principal_stock pps LEFT JOIN product_option po ON po.id = pps.id_option WHERE pps.branch_id = m.id AND pps.product_id = '.$id.'),0) as quantity')
                ->where('m.principal_id', $this->data['user']->principle_id);

        return $this->db->get('merchants m');
    }

    function get_branch($principle) {
        $this->db->select('id, name')
                ->where('principal_id', $principle);
        return $this->db->get('merchants');
    }

    function get_cabang($id){
        $this->db->select('id, name, username')
                ->where('id',$id)
                ->where('type', 'principle_branch');

        return $this->db->get('merchants');
    }

    // function get_all($start = 0, $length, $search = '', $order = array()) {
    //     $this->_where_like($search);
    //     if ($order) {
    //         $order['column'] = $this->_get_alias_key($order['column']);
    //         $this->db->order_by($order['column'], $order['dir']);
    //     }
    //     $this->db->select('p.id, code, p.name, discount, quantity, price, p.category, p.status, c.name category')
    //             ->join('categories c', 'c.id = p.category', 'left')
    //             ->join('brands b', 'b.id = p.brand', 'left')
    //             ->where('b.principle_id',$this->data['user']->principle_id)
    //             ->limit($length, $start);

    //     return $this->db->get('products p');
    // }

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->_where_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('p.id, code, p.name, discount, quantity, price, p.category, p.status, c.name category, p.variation')
                ->join('categories c', 'c.id = p.category', 'left')
                ->join('brands b', 'b.id = p.brand', 'left')
                // ->where('b.principle_id',$this->data['user']->principle_id)
                ->where('p.store_type', 'principal')
                ->where('p.store_id', $this->data['user']->principle_id)
                ->where('p.category !=', '')
                ->limit($length, $start);

        return $this->db->get('products p');
    }

    private function _get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'code';
                break;
            case 1: $key = 'p.name';
                break;
            case 2: $key = 'price';
                break;
            case 3: $key = 'c.name';
                break;
            case 4: $key = 'p.status';
                break;
            case 5: $key = 'total_merchant';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->_where_like($search);
        $this->db->select('p.id, code, p.name, price, p.category, p.status, c.name category, (SELECT COUNT(id) FROM product_merchant pm WHERE pm.product = p.id) total_merchant')
                ->join("(SELECT `cp`.`category`, GROUP_CONCAT(c2.name ORDER BY cp.level SEPARATOR ' > ') name FROM `category_path` `cp` LEFT JOIN `categories` `c1` ON `cp`.`category` = `c1`.`id` LEFT JOIN `categories` `c2` ON `cp`.`path` = `c2`.`id` GROUP BY `cp`.`category`) c", 'c.category = p.category')
                ->join('brands b', 'b.id = p.brand', 'left')
                // ->where('b.principle_id',$this->data['user']->principle_id)
                ->where('p.store_type', 'principal')
                ->where('p.store_id', $this->data['user']->principle_id);
        return $this->db->count_all_results('products p');
    }

    private function _where_like($search = '') {
        $columns = array('code', 'p.name', 'price', 'c.name');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
            $this->db->group_end();
        }
    }

    function get_categories() {
        $this->db->select("pcp.category id, GROUP_CONCAT(pc2.name ORDER BY pcp.level SEPARATOR ' > ') name", FALSE)
                ->join('categories pc1', 'pcp.category = pc1.id', 'left')
                ->join('categories pc2', 'pcp.path = pc2.id', 'left')
                ->order_by('pc2.name')
                ->group_by('pcp.category');

        return $this->db->get('category_path pcp');
    }

    function get_package_item($q) {
        $this->db->select("p.id , p.name as value,p.length,p.width,p.height")
        ->join('brands b', 'b.id = p.brand', 'left');
        $this->db->where('b.principle_id',$this->data['user']->principle_id);
        $this->db->limit(6);
        $this->db->like('p.name',$q);
        $query = $this->db->get('products p');
        return $query->result();
    }

    function get_product_features($product) {
        $this->db->select('pf.product, pf.value, f.*')
                ->join('features f', 'pf.feature = f.id', 'left')
                ->where('pf.product', $product)
                ->group_by('pf.feature');
        return $this->db->get('product_feature pf');
    }

    function get_product_feature_variants($product, $feature) {
        $this->db->select('fv.*')
                ->join('feature_variant fv', 'pf.feature = fv.feature AND pf.value = fv.id', 'left')
                ->where('pf.product', $product)
                ->where('pf.feature', $feature);
        $query = $this->db->get('product_feature pf');
        return($query->num_rows() > 0) ? $query : false;
    }

    function get_product_feature_by_category($category, $product = 0) {
        $this->db->select('cf.category, f.*, fv.value variant')
                ->join('features f', 'f.id = cf.feature', 'left')
                ->join('feature_variant fv', 'fv.id = cf.feature', 'left')
                ->where('cf.category', $category)
                ->group_by('cf.feature')
                ->order_by('f.sort_order ASC');

        if ($product) {
            $this->db->select('pf.product, pf.value')
                    ->join('product_feature pf', 'pf.feature = cf.feature AND pf.product = ' . $product, 'left');
        }
        return $this->db->get('category_feature cf');
    }

    function get_features_by_category($category, $product = 0) {
        $this->db->select('f.*, pf.value')
                ->join('features f', 'cf.feature = f.id', 'left')
                ->join('product_feature pf', 'pf.feature = cf.feature AND pf.product = ' . $product, 'left')
                ->where('cf.category', $category)
                ->group_by('cf.feature')
                ->order_by('f.sort_order ASC');
        return $this->db->get('category_feature cf');
    }

    function get_prices($product = 0) {
        $this->db->select('pp.*, mg.id merchant_group, mg.name')
                ->join('product_price pp', 'mg.id = pp.merchant_group AND pp.product = ' . $product, 'left')
                ->order_by('mg.name');
        return $this->db->get('merchant_groups mg');
    }

    function price_level_groups($price_level = 0) {
        $this->db->select('pp.*, mg.id merchant_group, mg.name')
                ->join('product_price_level_groups pp', 'mg.id = pp.merchant_group AND pp.product_price_level = ' . $price_level, 'left')
                ->order_by('mg.name');
        return $this->db->get('merchant_groups mg');
    }

    function get_product_options($product, $ids = array()) {
        $this->db->select("po.*, GROUP_CONCAT(og.name, ': ', o.value ORDER BY og.sort_order ASC SEPARATOR ', ') option_combination")
                ->join('product_option_combination poc', 'po.id = poc.product_option', 'left')
                ->join('options o', 'poc.option = o.id', 'left')
                ->join('option_group og', 'og.id = o.group', 'left')
                ->group_by('po.id')
                ->where('po.product', $product);
        if($ids)
            $this->db->where_in('po.id',$ids);
        return $this->db->get('product_option po');
    }

    function get_product_option_image($product, $product_option) {
        $this->db->select('id, product, image, product_option')
                ->join('product_option_image poi', 'pi.id = poi.product_image AND poi.product_option = ' . $product_option, 'left')
                ->where('pi.product', $product);

        return $this->db->get('product_image pi');
    }

    function export($start = 0, $length, $search = '', $order = array()) {
        $this->_where_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('p.*')
                ->join('categories c', 'c.id = p.category', 'left')
                ->limit($length, $start)
                ->select('(SELECT COUNT(id) FROM product_merchant pm WHERE pm.product = p.id) total_merchant')
                ->join('brands b', 'b.id = p.brand', 'left')
                ->where('b.principle_id',$this->data['user']->principle_id)
                ->where('p.merchant', 0);


        return $this->db->get('products p');
    }

    function get_table_deskripsi($id){
        $this->db->select('dr.*, d.nama')
             ->join('deskripsi d', 'dr.id_deskripsi = d.id_deskripsi', 'left')
             ->where('dr.id_kategori', $id);
        return $this->db->get('deskripsi_relasi dr');
    }

    function check_stock($product) {
        return $this->db->query("SELECT * FROM products_principal_stock WHERE product_id = $product");
    }

    function get_variation_description($product_option) {
        $this->db->select('o.type, o.value')
                ->join('product_option_combination poc', 'poc.option = o.id', 'LEFT')
                ->where('poc.product_option', $product_option);
        return $this->db->get('options o');
    }

}
