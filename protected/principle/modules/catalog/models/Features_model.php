<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Features_model extends CI_Model {

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->where_like($search);
        if ($order) {
            $order['column'] = $this->get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('f.*, COUNT(fv.id) count_variant')
                ->join('feature_variant fv', 'f.id = fv.feature', 'left')
                ->group_by('f.id')
                ->limit($length, $start);

        return $this->db->get('features f');
    }

    function get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'name';
                break;
            case 1: $key = 'sort_order';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->where_like($search);
        return $this->db->count_all_results('features');
    }

    function where_like($search = '') {
        $columns = array('name');
        if ($search) {
            foreach ($columns as $column) {
                $this->db->like('IFNULL(' . $column . ',"")', $search);
            }
        }
    }

    function get_feature_variants($feature, $start = 0, $length, $search = '', $order = array()) {
        if ($order) {
//            $order['column'] = $this->get_alias_key($order['column']);
            $this->db->order_by('value', $order['dir']);
        }
        $this->db->where('feature', $feature)
                ->like('value', $search)
                ->limit($length, $start);

        return $this->db->get('feature_variant');
    }

    function count_feature_variants($feature, $search = '') {
        $this->db->where('feature', $feature)
                ->like('value', $search);
        return $this->db->count_all_results('feature_variant');
    }
    
    function parent_variants(){
        $this->db->where_not_in('type',array('i','t'))
                ->order_by('name asc');
        return $this->db->get('features');
    }

}
