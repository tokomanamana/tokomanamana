<?php

class Price_model extends CI_Model {
    
    function get_all_product($start = 0, $length, $search = '', $order = array()) {
        $this->_where_like_product($search);
        if ($order) {
            $order['column'] = $this->_get_alias_key_product($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('p.id, p.name, p.price, p.variation')
                // ->where('b.principle_id',$this->data['user']->principle_id)
                ->where('p.store_type', 'principal')
                ->where('p.store_id', $this->data['user']->principle_id)
                ->where('p.name !=', '')
                ->limit($length, $start);

        return $this->db->get('products p');
    }

    private function _get_alias_key_product($key) {
        switch ($key) {
            case 0: $key = 'p.name';
                break;
            case 1: $key = 'p.price';
                break;
        }
        return $key;
    }

    function count_all_products($search = '') {
        $this->_where_like_product($search);
        $this->db->select('p.id, p.name, p.price, p.variation')
                // ->where('b.principle_id',$this->data['user']->principle_id)
                ->where('p.store_type', 'principal')
                ->where('p.store_id', $this->data['user']->principle_id);
        return $this->db->count_all_results('products p');
    }

    private function _where_like_product($search = '') {
        $columns = array('p.name', 'p.price');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
            $this->db->group_end();
        }
    }

    public function get_branches($product) {
        $this->db->select('m.name, pps.price, pps.product_id, pps.branch_id, pps.id_option')
                ->join('merchants m', 'm.id = pps.branch_id', 'left')
                ->join('products p', 'p.id = pps.product_id')
                ->where('pps.product_id', $product)
                ->group_by('pps.product_id, pps.branch_id');
        $query = $this->db->get('products_principal_stock pps');
        return ($query->num_rows() > 0) ? $query->result() : FALSE;
    }

    function get_variation_description($product_option) {
        $this->db->select('o.type, o.value')
                ->join('product_option_combination poc', 'poc.option = o.id', 'LEFT')
                ->where('poc.product_option', $product_option);
        return $this->db->get('options o');
    }

}