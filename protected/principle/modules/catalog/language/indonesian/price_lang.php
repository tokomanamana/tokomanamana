<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['heading'] = 'Manajemen Harga';
$lang['heading_branch'] = 'Daftar Cabang';
$lang['heading_product'] = 'Daftar Produk';
$lang['form_heading'] = 'Edit Harga';

$lang['product_th'] = 'Nama Produk';
$lang['price_th'] = 'Harga Default';
$lang['branch_name_th'] = 'Nama Cabang';
$lang['price_branch_th'] = 'Harga Produk di Cabang';

$lang['search_branch_placeholder'] = 'Cari Cabang...';

$lang['price'] = 'Harga Jual';
$lang['price_level'] = 'Harga Grosir';
$lang['price_reseller'] = 'Harga Reseller';
$lang['price_default'] = 'Harga Default';
$lang['promo_type'] = 'Promo';

$lang['discount'] = 'Diskon';
$lang['promo_range_start'] = 'Tanggal Mulai Promo';
$lang['promo_range_end'] = 'Tanggal Berakhir Promo';