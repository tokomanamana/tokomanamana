<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['heading'] = 'Stok';
$lang['list_heading'] = 'Daftar Stok';
$lang['add_heading'] = 'Tambah Stok';
$lang['edit_heading'] = 'Edit Stok';

$lang['name'] = 'Nama';
$lang['price'] = 'Harga';
$lang['code'] = 'Kode';
$lang['type'] = 'Tipe';
$lang['status'] = 'Status';
$lang['stock_heading'] = 'Stok';
$lang['stock_old'] = 'Stok Lama';
$lang['stock_new'] = 'Stok Baru';
$lang['stock_total'] = 'Total Stok';
$lang['stock_sub_heading'] = 'Kelola Data Stok';
$lang['stock_list_heading'] = 'Daftar Stok';
$lang['stock_add_heading'] = 'Tambah Stok';
$lang['stock_edit_heading'] = 'Edit Stok';

$lang['stock_image_th'] = 'Gambar';
$lang['stock_name_th'] = 'Nama';
$lang['stock_seo_url_th'] = 'Friendly URL';

$lang['stock_form_general_tabs'] = 'Umum';
$lang['stock_form_seo_tabs'] = 'SEO';

$lang['stock_form_name_label'] = 'Nama';
$lang['stock_form_image_label'] = 'Gambar';
$lang['stock_form_description_label'] = 'Deskripsi';
$lang['stock_form_seo_url_label'] = 'Friendly URL';
$lang['stock_form_meta_title_label'] = 'Meta Judul';
$lang['stock_form_meta_description_label'] = 'Meta Deskripsi';
$lang['stock_form_meta_keyword_label'] = 'Meta Keyword';

$lang['stock_form_name_placeholder'] = 'Masukkan nama Stok';
$lang['stock_form_description_placeholder'] = 'Masukkan deskripsi dari Stok ini. Deskripsi akan ditampilkan di halaman yang menampilkan semua Stok dari Stok ini';

$lang['stock_save_success_message'] = "Stok '%s' berhasil disimpan.";
$lang['stock_save_error_message'] = "Stok '%s' gagal disimpan.";
$lang['stock_delete_success_message'] = "Stok '%s' berhasil dihapus.";
$lang['stock_delete_error_message'] = "Stok '%s' gagal dihapus.";
$lang['page_friendly_url_exist_message'] = "Friendly URL sudah digunakan.";