<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['option_heading'] = 'Variasi Produk';
$lang['option_list_heading'] = 'Daftar Variasi';
$lang['option_add_heading'] = 'Tambah Variasi';
$lang['option_edit_heading'] = 'Edit Variasi';

$lang['option_name_th'] = 'Nama';
$lang['option_sort_order_th'] = 'Urutan';
$lang['option_variant_value_th'] = 'Nilai';
$lang['option_variant_color_th'] = 'Warna';

$lang['option_form_name_label'] = 'Nama';
$lang['option_form_type_label'] = 'Jenis';

$lang['option_form_name_placeholder'] = 'Masukkan nama variasi';

$lang['option_save_success_message'] = "Variasi '%s' berhasil disimpan.";
$lang['option_save_error_message'] = "Variasi '%s' gagal disimpan.";
$lang['option_delete_success_message'] = "Variasi '%s' berhasil dihapus.";
$lang['option_delete_error_message'] = "Variasi '%s' gagal dihapus.";

//option variant 

$lang['option_variant_heading'] = 'Nilai Variasi';
$lang['option_variant_list_heading'] = 'Daftar Nilai Variasi';
$lang['option_variant_add_heading'] = 'Tambah Nilai Variasi';
$lang['option_variant_edit_heading'] = 'Edit Nilai Variasi';

$lang['option_variant_value_th'] = 'Nilai Variasi';

$lang['option_variant_form_option_label'] = 'Variasi';
$lang['option_variant_form_value_label'] = 'Nilai Variasi';
$lang['option_variant_form_color_label'] = 'Pilih Warna';

$lang['option_variant_form_value_placeholder'] = 'Masukkan nilai variasi dari variasi yang dipilih';