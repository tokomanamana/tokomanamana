<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['heading'] = 'Produk';
$lang['list_heading'] = 'Daftar Produk';
$lang['add_heading'] = 'Tambah Produk';
$lang['edit_heading'] = 'Edit Produk';

$lang['image'] = 'Gambar';
$lang['name'] = 'Nama';
$lang['code'] = 'SKU';
$lang['price_old'] = 'Harga Normal';
$lang['price'] = 'Harga Jual';
$lang['status'] = 'Aktif';
$lang['discount'] = 'Diskon';
$lang['category'] = 'Kategori';
$lang['merchant'] = 'Penjual';
$lang['quantity'] = 'Stok';
$lang['image'] = 'Gambar';
$lang['stock'] = 'Stok Produk';

$lang['general_tabs'] = 'Dasar';
$lang['feature_tabs'] = 'Fitur';
$lang['seo_tabs'] = 'SEO';
$lang['option_tabs'] = 'Variasi';
$lang['price_tabs'] = 'Harga';

$lang['name_product'] = 'Nama Produk';
$lang['code'] = 'Kode Produk';
$lang['barcode'] = 'Barcode';
$lang['name'] = 'Nama';
$lang['description'] = 'Deskripsi';
$lang['short_description'] = 'Deskripsi Singkat';
$lang['status'] = 'Status';
$lang['brand'] = 'Merek';
$lang['category'] = 'Kategori';
$lang['price'] = 'Harga';
$lang['weight'] = 'Berat Kemasan Pengiriman';
$lang['dimension'] = 'Dimensi (P x L x T)';
$lang['quantity'] = 'Stok';
$lang['seo_url'] = 'Friendly URL';
$lang['meta_title'] = 'Meta Judul';
$lang['meta_description'] = 'Meta Deskripsi';
$lang['meta_keyword'] = 'Meta Kata Kunci';
$lang['type'] = 'Jenis Produk';
$lang['type_standar'] = 'Produk Standar';
$lang['type_pack'] = 'Produk Paket';
$lang['type_digital'] = 'Produk Digital';
$lang['etalase'] = 'Etalase';
$lang['price_level'] = 'Harga Grosir';
$lang['price_reseller'] = 'Harga Reseller';
$lang['promo_range_start'] = 'Tanggal Mulai Promo';
$lang['promo_range_end'] = 'Tanggal Berakhir Promo';
$lang['stock_minimum'] = 'Minimal Kuantitas Pembelian';

$lang['feature_hidden_info'] = 'Silahkan pilih kategori dahulu untuk mengisi fitur.';

$lang['name_placeholder'] = 'Masukkan nama produk';
$lang['code_placeholder'] = 'ex: A001';
$lang['short_description_placeholder'] = 'Tuliskan deskripsi singkat sebagai referensi awal pembeli melihat produk ini';
$lang['description_placeholder'] = 'Tuliskan deskripsi lengkap tentang produk ini';
$lang['meta_title_placeholder'] = 'Tulis jika ingin menggunakan judul yang berbeda dari nama produk';
$lang['meta_description_placeholder'] = 'Tulis disini untuk membuat deskripsi yang berbeda dengan deskripsi singkat produk di hasil pencarian';
$lang['combination_select_attribut_placeholder'] = 'Pilih atribut yang akan dijadikan kombinasi produk';

$lang['add_image_button'] = 'Tambah Gambar';
$lang['add_feature_button'] = 'Tambah Fitur';
$lang['add_pack_button'] = 'Tambah Produk';

$lang['combination_add_button'] = 'Tambah Kombinasi';
$lang['combination_button'] = '<i class=" icon-grid5"></i> Kombinasi';

$lang['save_success_message'] = "Produk '%s' berhasil disimpan.";
$lang['save_error_message'] = "Produk '%s' gagal disimpan.";
$lang['delete_success_message'] = "Produk '%s' berhasil dihapus.";
$lang['delete_error_message'] = "Produk '%s' gagal dihapus.";
$lang['exist_code_message'] = "Kode produk sudah ada.";
$lang['empty_image_message'] = "Gambar kosong! Setidaknya tambahkan satu gambar.";
$lang['image_primary_not_set_message'] = "Pilih gambar default dahulu.";
$lang['default_option_not_set_message'] = "Pilih variasi default dahulu.";
$lang['friendly_url_exist_message'] = "Friendly URL sudah digunakan.";
$lang['empty_variation'] = "Tidak ada data variasi!";