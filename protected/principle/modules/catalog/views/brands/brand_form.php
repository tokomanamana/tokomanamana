<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo ($data) ? lang('brand_edit_heading') : lang('brand_add_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('catalog/brands'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('brand_list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('catalog/brands/save'); ?>" method="post" class="form-horizontal" id="form">
                    <input type="hidden" name="id" id="id_brand" value="<?php echo set_value('id', ($data) ? encode($data->id) : ''); ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#general" data-toggle="tab"><?php echo lang('brand_form_general_tabs'); ?></a></li>
                                    <li><a href="#seo" data-toggle="tab"><?php echo lang('brand_form_seo_tabs'); ?></a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="general">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="name"><?php echo lang('brand_form_name_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" required="" name="name" value="<?php echo ($data) ? $data->name : ''; ?>" onkeyup="convertToSlug(this.value);" placeholder="<?php echo lang('brand_form_name_placeholder') ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('brand_form_image_label'); ?></label>
                                            <div id="image-container">
                                            <?php if ($data && ($data->image)) { ?>
                                                <?php 
                                                $img = site_url('../files/images/principal/' . $data->image);
                                                if(file_exists(FCPATH . '../files/images/principal/' . $data->image)) {
                                                    $url = $img;
                                                } else {
                                                    $url = site_url('../files/images/' . $data->image);
                                                }
                                                ?>
                                                <div class="col-md-3" id="image-preview">
                                                    <div class="thumbnail">
                                                        <div class="thumb">
                                                            <img src="<?php echo $url ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <input type="hidden" name="image" id="image_name" value="<?= ($data) ? $data->image : '' ?>">
                                            </div>
                                            <div class="col-md-6" id="add-image">
                                                <button type="button" id="add-image-btn" class="btn btn-default" <?php echo ($data && $data->image) ? 'style="display:none;"' : ''; ?>><?php echo lang('button_add_image'); ?></button>
                                                <button type="button" id="btn-edit-image" class="btn btn-default" <?php echo (!$data || ($data && !$data->image)) ? 'style="display:none;"' : ''; ?>><?php echo lang('button_edit_image'); ?></button>
                                                <button type="button" id="btn-delete-image" class="btn btn-default" <?php echo (!$data || ($data && !$data->image)) ? 'style="display:none;"' : ''; ?>><?php echo lang('button_delete_image'); ?></button>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('brand_form_description_label'); ?></label>
                                            <div class="col-md-9">
                                                <textarea cols="30" rows="3" class="form-control" name="description" placeholder="<?php echo lang('brand_form_description_placeholder') ?>"><?php echo ($data) ? $data->description : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <!-- <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('brand_form_status_label'); ?></label>
                                            <div class="col-md-9 status-brand">
                                                <input type="checkbox" name="status" value="1" data-on-text="<?php echo lang('enabled'); ?>" data-off-text="<?php echo lang('disabled'); ?>" class="switch" <?php echo ($data) ? (($data->status == 1) ? 'checked' : '') : 'checked'; ?>>
                                            </div>
                                        </div> -->
                                    </div>
                                    <div class="tab-pane " id="seo">
                                        <!-- <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('brand_form_seo_url_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="seo_url" name="seo_url" value="<?php echo ($data) ? $data->seo_url : ''; ?>">
                                            </div>
                                        </div> -->
                                        <!-- <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('brand_form_meta_title_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" name="meta_title" class="form-control" placeholder="<?php echo lang('brand_form_meta_title_placeholder') ?>" value="<?php echo ($data) ? $data->meta_title : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('brand_form_meta_description_label'); ?></label>
                                            <div class="col-md-9">
                                                <textarea type="text" name="meta_description" class="form-control" placeholder="<?php echo lang('brand_form_meta_description_placeholder') ?>"><?php echo ($data) ? $data->meta_description : ''; ?></textarea>
                                            </div>
                                        </div> -->
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('brand_form_meta_keyword_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" name="meta_keyword" class="tags-input" placeholder="<?php echo lang('brand_form_meta_keyword_placeholder') ?>" value="<?php echo ($data) ? $data->meta_keyword : ''; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('catalog/brands'); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="username" id="username" value="<?php echo $username; ?>">
<input type="file" name="brand_image" id="brand_image" style="display: none;">
<div id="filemanager" class="modal" data-backdrop="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">File Manager</h5>
            </div>

            <div class="modal-body">
                <iframe  width="100%" height="550" frameborder="0" src="<?php echo base_url('filemanager/dialog.php?type=1&folder=' . $username . '/brands' . '&editor=false&field_id=image&relative_url=1'); ?>"></iframe>
            </div>
        </div>
    </div>
</div>