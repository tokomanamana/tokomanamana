<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo ($data) ? lang('option_edit_heading') : lang('option_add_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('catalog/options'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('option_list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('catalog/options/save'); ?>" method="post" class="form-horizontal" id="form">
                    <input type="hidden" name="id" value="<?php echo set_value('id', ($data) ? encode($data->id) : ''); ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="name"><?php echo lang('option_form_name_label'); ?></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" required="" id="name" name="name" placeholder="<?php echo lang('option_form_name_placeholder'); ?>" value="<?php echo ($data) ? $data->name : ''; ?>">
                                </div>
                            </div>
                            <?php if (!$data) { ?>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo lang('option_form_type_label'); ?></label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="type" required="" id="type">
                                            <option value="s" <?php echo ($data && ($data->type == 's')) ? 'selected' : ''; ?>>Select</option>
                                            <option value="r" <?php echo ($data && ($data->type == 'r')) ? 'selected' : ''; ?>>Radio</option>
                                            <option value="c" <?php echo ($data && ($data->type == 'c')) ? 'selected' : ''; ?>>Color</option>
                                        </select>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('catalog/options'); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>