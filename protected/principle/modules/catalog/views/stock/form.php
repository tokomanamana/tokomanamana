<style type="text/css">
    tr, td, th {
        text-align:center;
    }
    .option-header {
        padding-left: 5px;padding-bottom: 10px;
        font-size: 16px;
        font-weight: bold;
        padding-top: 15px;
    }
</style>
<div class="content-wrapper" id="product-form">
    
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <input type="hidden" name="id" id="id" value="<?php echo encode($data->id); ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="tabbable">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="general">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label><?php echo lang('code'); ?></label>
                                                    <input type="text" name="code" id="code" readonly=""  class="form-control" value="<?php echo ($data) ? $data->code : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><?php echo lang('name'); ?></label>
                                                    <input class="form-control big-input" readonly="" type="text" name="name" value="<?php echo $data->name; ?>" placeholder="<?php echo lang('name_placeholder'); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <?php if($data->variation) : ?>
                                <?php $variations = $this->main->gets('product_option', ['product' => $data->id]); ?>
                                <?php $var = 1; ?>
                                <?php $var_temp = 1; ?>
                                <?php foreach($variations->result() as $variation) : ?>
                                    <?php $desc = $this->stocks->get_variation_description($variation->id); ?>
                                    <div class="option-header">
                                        <span>
                                            <?php foreach($desc->result() as $des) : ?>
                                                <?php echo $des->type . ' ' . $des->value ?>
                                            <?php endforeach; ?>
                                        </span>
                                        <span style="float:right;margin-right: 20px;">Total Semua Stok : <?php echo $variation->quantity ?></span>
                                    </div>
                                    <div class="panel panel-flat">
                                        <table class="table table-hover table_stock" id="table" data-url="<?php echo site_url('catalog/stock/get_stock'); ?>">
                                            <thead>
                                                <tr>
                                                    <th style="text-align: left"><?php echo lang('name'); ?></th>
                                                    <th><?php echo lang('stock_old'); ?></th>
                                                    <th><?php echo lang('type'); ?></th>
                                                    <th><?php echo lang('stock_new'); ?></th>
                                                    <th><?php echo lang('stock_total'); ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <form action="<?php echo site_url('catalog/stock/save'); ?>" class="" method="post" id="form">
                                                <input type="hidden" name="id_option[<?php echo $var ?>]" value="<?php echo encode($variation->id) ?>">
                                                <?php
                                                $i = 1;
                                                foreach($dta->result() as $stk){ ?>
                                                    <?php
                                                    $stk->quantity = 0;
                                                    $product_branch_option = $this->main->get('products_principal_stock', ['branch_id' => $stk->id, 'product_id' => $data->id, 'id_option' => $variation->id]);
                                                    if($product_branch_option) {
                                                        $stk->quantity = $product_branch_option->quantity;
                                                    } 
                                                    ?>
                                                    <input type="hidden" name="id_produk" value="<?php echo encode($id_produk); ?>">
                                                    <input type="hidden" name="id_branch[<?php echo $var ?>][<?php echo $i; ?>]" value="<?php echo encode($stk->id); ?>">
                                                    <input type="hidden" id="tipe<?php echo $var_temp; ?>" name="tipe[<?php echo $var ?>][<?php echo $i; ?>]" value="in">
                                                    <tr>
                                                        <td style="text-align: left"><?php echo $stk->name; ?></td>
                                                        <td style="width:150px; color:<?php if($stk->quantity==0){ echo 'red'; } ?>">
                                                            <div id="stk_old<?php echo $var_temp; ?>"><?php echo number_format($stk->quantity); ?></div>
                                                        </td>
                                                        <td style="width:150px;">
                                                            <select class="bootstrap-select" onchange="tipe(<?php echo $var_temp; ?>)"  id="tipe_<?php echo $var_temp; ?>" data-width="100%">
                                                                <option value="in">Masuk</option>
                                                                <option value="out">Keluar</option>
                                                            </select>
                                                        </td>
                                                        <td style="width:150px;">
                                                            <input oninput="new_stock(<?php echo $var_temp; ?>)" type="number" name="stk_new[<?php echo $var ?>][<?php echo $i; ?>]" min="1" id="stk_new<?php echo $var_temp; ?>" class="form-control">
                                                        </td>
                                                        <td style="width:150px; font-weight:bold;">
                                                            <input style="color:<?php if($stk->quantity==0){ echo 'red'; } ?>" type="text" name="stk_total[<?php echo $var ?>][<?php echo $i; ?>]" id="stk_total<?php echo $var_temp; ?>" value="<?php echo number_format($stk->quantity); ?>" readonly="" class="form-control">
                                                        </td>
                                                    </tr>
                                                <?php $i++; $var_temp++; }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <?php $var++; ?>
                                <?php endforeach; ?>
                            <?php else : ?>
                                <div class="panel panel-flat">
                                    <table class="table table-hover table_stock" id="table" data-url="<?php echo site_url('catalog/stock/get_stock'); ?>">
                                        <thead>
                                            <tr>
                                                <th style="text-align: left"><?php echo lang('name'); ?></th>
                                                <th><?php echo lang('stock_old'); ?></th>
                                                <th><?php echo lang('type'); ?></th>
                                                <th><?php echo lang('stock_new'); ?></th>
                                                <th><?php echo lang('stock_total'); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i=1;
                                            foreach($dta->result() as $stk){ ?>
                                                <form action="<?php echo site_url('catalog/stock/save'); ?>" class="" method="post" id="form">
                                                <input type="hidden" name="id_produk" value="<?php echo encode($id_produk); ?>">
                                                <input type="hidden" name="id_branch[<?php echo $i; ?>]" value="<?php echo encode($stk->id); ?>">
                                                <input type="hidden" id="tipe<?php echo $i; ?>" name="tipe[<?php echo $i; ?>]" value="in">
                                                <tr>
                                                    <td style="text-align: left"><?php echo $stk->name; ?></td>
                                                    <td style="width:150px; color:<?php if($stk->quantity==0){ echo 'red'; } ?>"><div id="stk_old<?php echo $i; ?>"><?php echo number_format($stk->quantity); ?></div></td>
                                                    <td style="width:150px;">
                                                        <select class="bootstrap-select" onchange="tipe(<?php echo $i; ?>)"  id="tipe_<?php echo $i; ?>" data-width="100%">
                                                            <option value="in">Masuk</option>
                                                            <option value="out">Keluar</option>
                                                        </select>
                                                    </td>
                                                    <td style="width:150px;">
                                                        <input oninput="new_stock(<?php echo $i; ?>)" onkeypress="return event.charCode != 45 && event.charCode != 43" type="number" name="stk_new[<?php echo $i; ?>]" min="1" id="stk_new<?php echo $i; ?>" class="form-control">
                                                    </td>
                                                    <td style="width:150px; font-weight:bold;">
                                                        <input style="color:<?php if($stk->quantity==0){ echo 'red'; } ?>" type="text" name="stk_total[<?php echo $i; ?>]" id="stk_total<?php echo $i; ?>" value="<?php echo number_format($stk->quantity); ?>" readonly="" class="form-control">
                                                    </td>
                                                </tr>
                                            <?php $i++; }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('catalog/stock'); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <div class="btn-group dropup">
                                        <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                        <!-- <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button> -->
                                        <!-- <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="<?php echo site_url('catalog/stock'); ?>">Ke Produk</a></li>
                                        </ul> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>