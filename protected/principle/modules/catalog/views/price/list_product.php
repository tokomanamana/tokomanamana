<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('heading_product'); ?></h2>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <table class="table table-hover" id="table" data-url="<?php echo site_url('catalog/price/get_list_product'); ?>">
                <thead>
                    <tr>
                        <th class="default-sort" data-sort="asc"><?php echo lang('product_th'); ?></th>
                        <th><?= lang('price_th') ?></th>
                        <th class="no-sort text-center"><?= lang('actions') ?></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>