<style type="text/css">#description_ifr{width:99%!important}.product_title{margin-bottom:18px;margin-left:10px;margin-right:10px}.wajib{color:#fff;bottom:0;display:inline-block;margin-top:3px;margin-left:1px;margin-right:1px;position:relative;z-index:1;padding:2px 5px;background-color:#97c23c;font-size:10px;border-radius:2px}.circle{color:#fff;bottom:0;display:inline-block;margin-left:3px;position:relative;z-index:1;padding:4px;background-color:#97c23c;border-radius:50px}.nav-tabs{border-bottom:1px solid #97c23c}.nav-tabs>li.active>a{border:1px solid #97c23c;border-bottom-color:transparent}.page-title{width:auto !important;}#mceu_18{overflow-x: scroll;}.nav-tabs:before {content: 'Tabs .Produk';}#product-form #images{min-height: 163px;}h4{display: inline-block;}.info_price{font-size:small;display: inline-block;color:gray;}.inactive{background-color: #ccc;opacity: 0.3;}.panel-variasi-colour{margin-bottom: 10px !important;}.form-group-grosir{margin-left:0px !important;}#table-container{overflow:auto}.checkbox-status-variant{height:0;width:0;visibility:hidden;display:none}.label-status-variant{cursor:pointer;text-indent:-9999px;width:70px;height:35px;background:grey;display:block;border-radius:100px;position:relative}.label-status-variant:after{content:'';position:absolute;top:3px;left:4px;width:30px;height:30px;background:#fff;border-radius:90px;transition:.3s}.checkbox-status-variant:checked+.label-status-variant{background:#bada55}.checkbox-status-variant:checked+.label-status-variant:after{left:calc(100% - 4px);transform:translateX(-100%)}.checkbox-status-variant:active:after{width:70px}.custom-error-label{color:#f44336;}label{font-weight: 600;color: #878b8f}</style>
<div class="content-wrapper" id="product-form">
    <form action="<?php echo site_url('catalog/price/save') ?>" class="" method="post" id="form">
        <div class="page-header page-header-default">
            <div class="page-header-content">
                <div class="page-title">
                    <h1><?php echo lang('form_heading') ?></h1>
                </div>

                <div class="heading-elements">
                    <div class="heading-btn-group">
                        <a href="<?php echo site_url('catalog/price/branch/' . encode($id_product)); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span>Daftar Cabang</span></a>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" name="status_variation" id="status_variation" value="<?= $status_variation ?>">
        <input type="hidden" id="product_id" value="<?= encode($id_product) ?>">
        <input type="hidden" id="branch_id" value="<?= encode($id_branch) ?>">
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#price" data-toggle="tab">Harga</a></li>
                                    <li id="promo-tab"><a href="#promo" data-toggle="tab">Promo</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane form-horizontal active" id="price">
                                        <div class="form-group-price">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"><?= lang('price_default') ?></label>
                                                <div class="col-md-4">
                                                    <div class="input-group">
                                                        <select name="default_price" id="default_price" class="bootstrap-select">
                                                            <option value="0" <?= ($price_default == '0') ? 'selected' : '' ?>>Tidak</option>
                                                            <option value="1" <?= ($price_default == '1') ? 'selected' : '' ?>>Iya</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                        <div class="form-group-promo" id="promo_type_div">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"><?= lang('promo_type') ?></label>
                                                <div class="col-md-4">
                                                    <div class="input-group">
                                                        <select name="promo" id="promo_type" class="form-control">
                                                            <option value="0" <?= $promo_status == 0 ? 'selected' : '' ?>>Tidak</option>
                                                            <option value="1" <?= $promo_status == 1 ? 'selected' : '' ?>>Iya</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                        <?php if($status_variation) : ?>
                                            <div class="form-group-price price-variation">
                                            <?php foreach($data->result() as $product) : ?>
                                                <?php 
                                                    if($price_default) {
                                                        $product_option = $this->main->get('product_option', ['id' => $product->id_option]);
                                                        $price = $product_option->price;
                                                    } else {
                                                        $price = $product->price;
                                                    }
                                                ?>
                                                <input type="hidden" name="id[]" value="<?= encode($product->id) ?>">
                                                <div class="form-group">
                                                    <?php $variations = $this->price->get_variation_description($product->id_option); ?>
                                                    <label class="col-md-3 control-label">
                                                        <?php foreach($variations->result() as $variation) : ?>
                                                            <?= $variation->type . ' ' . $variation->value . ' ' ?>
                                                        <?php endforeach; ?>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">RP</span>
                                                            <input type="text" name="price_option[]" id="price-option-<?= encode($product->id_option) ?>" class="price-input-variation form-control number" value="<?= $price ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                            <hr>
                                            </div>
                                            <div class="form-group-price price-option-utama">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label"><?= lang('price') ?> Utama</label>
                                                    <div class="col-md-4">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">Rp</span>
                                                            <input type="text" name="price" id="price-input" class="form-control number" value="<?= isset($price) ? $price : '' ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            <hr>
                                            </div>
                                        <?php else : ?>
                                            <?php 
                                            if($price_default) {
                                                $product = $this->main->get('products', ['id' => $data->product_id]);
                                                $price = $product->price;
                                            } else {
                                                $price = $data->price;
                                            }
                                            ?>
                                            <input type="hidden" name="id" id="id" value="<?php echo encode($data->id) ; ?>">
                                            <div id="form-group-price">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label"><?php echo lang('price'); ?></label>
                                                    <div class="col-md-4">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">Rp</span>
                                                            <input type="text" name="price" id="price-input" class="form-control number" value="<?php echo $price ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                            </div>
                                        <?php endif; ?>
                                        <div class="form-group-grosir">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"><?= lang('price_level') ?></label>
                                                <input type="hidden" id="price-count" value="1">
                                                <div class="col-md-4">
                                                    <div class="input-group">
                                                        <select class="form-control" name="price_grosir" id="grosir-slc">
                                                            <option value="0" <?= ($price_level_status == 0) ? 'selected' : '' ?>>Tidak</option>
                                                            <option value="1" <?= ($price_level_status == 1) ? 'selected' : '' ?>>Iya</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <table id="price-list" class="table">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 80px">Min QTY</th>
                                                        <th style="width: 200px">Harga</th>
                                                        <th style="width: 50px"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if (isset($price_levels)) {
                                                        foreach ($price_levels as $price) {
                                                            ?>
                                                            <tr>
                                                                <td class="price-qty" width="10%"><input type="number" required min="2" name="price_level[<?php echo $price->id; ?>][qty]" class="form-control td-grosir-qty" value="<?php echo $price->min_qty; ?>"></td>
                                                                <td width="30%"><div class="input-group"><span class="input-group-addon">Rp</span><input type="text" required name="price_level[<?php echo $price->id; ?>][price]" class="form-control number-grosir number" value="<?php echo $price->price; ?>"></div></td>
                                                                <td><button type="button" class="btn btn-danger btn-xs remove-price">x</button></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    } else {?>
                                                    <?php } ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th colspan="4" class="text-center"><button type="button" style="float: left" id="add-price" class="btn btn-success">Tambah harga grosir</button></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <hr>
                                        </div>
                                        <div class="form-group-grosir">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"><?= lang('price_reseller') ?></label>
                                                <input type="hidden" id="price-count-reseller" value="1">
                                                <div class="col-md-4">
                                                    <div class="input-group">
                                                        <select class="form-control" name="price_reseller" id="reseller-slc">
                                                            <option value="0" <?= ($price_reseller_status == 0) ? 'selected' : '' ?>>Tidak</option>
                                                            <option value="1" <?= ($price_reseller_status == 1) ? 'selected' : '' ?>>Iya</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <table id="price-reseller-list" class="table">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 80px">Min QTY</th>
                                                        <th style="width: 200px">Harga</th>
                                                        <th style="width: 50px"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if (isset($price_reseller)) {
                                                        foreach ($price_reseller as $resell) {
                                                            ?>
                                                            <tr>
                                                                <td class="price-qty-reseller" width="10%"><input type="number" required min="2" name="price_reseller_list[<?php echo $resell->id; ?>][qty]" class="form-control td-reseller-qty" value="<?php echo $resell->min_qty; ?>"></td>
                                                                <td width="30%"><div class="input-group"><span class="input-group-addon">Rp</span><input type="text" required name="price_reseller_list[<?php echo $resell->id; ?>][price]" class="form-control number-reseller number" value="<?php echo $resell->price; ?>"></div></td>
                                                                <td><button type="button" class="btn btn-danger btn-xs remove-price-reseller">x</button></td>
                                                            </tr>
                                                            <?php
                                                        }

                                                    } ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th colspan="4" class="text-center"><button type="button" style="float: left" id="add-price-reseller" class="btn btn-success">Tambah harga reseller</button></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane form-horizontal" id="promo">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Jenis</label>
                                            <div class="col-md-3">
                                                <select class=" bootstrap-select" id="input_type_promo" name="promo_data[type]" required="">
                                                    <option id="fixed_amount" value="F" <?= (isset($promo_data['type']) ? ($promo_data['type'] == 'F') ? 'selected' : '' : '') ?>>Jumlah Tetap</option>
                                                    <option id="percentage" value="P" <?= (isset($promo_data['type']) ? ($promo_data['type'] == 'P') ? 'selected' : '' : '') ?>>Persentasi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Available <div class="wajib">Wajib</div></label>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control date" name="promo_data[date_start]" autocomplete="off" value="<?= (isset($promo_data['date_start']) ? ($promo_data['date_start'] ? $promo_data['date_start'] : '' ) : '' ) ?>" placeholder="">
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control date" name="promo_data[date_end]" autocomplete="off" value="<?= (isset($promo_data['date_end']) ? ($promo_data['date_end'] ? $promo_data['date_end'] : '' ) : '' ) ?>" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Discount <div class="wajib">Wajib</div></label>
                                            <div id="discount_div" class="col-md-3">
                                                <div class="input-group">
                                                    <span id="discount_addon"class="input-group-addon">Rp</span>
                                                    <input type="text" id="discount" class="form-control number" name="promo_data[discount]" value="<?= (isset($promo_data['discount']) ? ($promo_data['discount'] ? $promo_data['discount'] : '' ) : '' ) ?>">
                                                    <span id="discount_addon_percent"class="input-group-addon">%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="form-group">
                                            <label class="col-md-3 control-label">Gratis Ongkir</label>
                                            <div class="col-md-3">
                                            <select class="bootstrap-select" name="free_ongkir" id="ongkir-slc">
                                                <option value="0">Tidak</option>
                                                <option value="1">Iya</option>
                                            </select>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <!-- <a class="text-muted" href="<?php //echo site_url('catalog/products'); ?>"><i class="icon-trash"></i></a> -->
                                <a class="btn btn-default" href="<?php echo site_url('catalog/price/branch/' . encode($id_product)); ?>">Kembali</a>
                                <div class="pull-right">
                                    <div class="btn-group dropup">
                                        <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>