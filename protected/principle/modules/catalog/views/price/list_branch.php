<style>
    .option-header {
        padding-left: 5px;padding-bottom: 10px;
        font-size: 16px;
        font-weight: bold;
        padding-top: 15px;
    }
</style>
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('heading_branch') . ' ' . $product_name; ?></h2>
            </div>
            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('catalog/price'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('heading_product'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="form-group col-md-3">
                            <input type="text" name="search" id="search_form" class="form-control" placeholder="<?= lang('search_branch_placeholder') ?>">
                        </div>
                    </div>
                    <div class="content">
                    <div class="panel panel-flat">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th><?= lang('branch_name_th') ?></th>
                                    <th><?= lang('price_branch_th') ?></th>
                                    <th><?= lang('actions') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($branches) : ?>
                                    <?php foreach($branches as $branch) : ?>
                                        <?php 
                                        if(!$branch->id_option) {
                                            if($branch->price) {
                                                $price = $branch->price;
                                            } else {
                                                $product = $this->main->get('products', ['id' => $branch->product_id]);
                                                $price = $product->price;
                                            }
                                        }
                                        ?>
                                        <tr class="list-branch">
                                            <td class="list-name"><?= $branch->name ?></td>
                                            <td> <?= ($branch->id_option) ? '<i>Produk Bervariasi</i>' : 'Rp. ' . number($price) ?></td>
                                            <td>
                                                <?php if($this->aauth->is_allowed('catalog/edit_price')) : ?>
                                                    <a href="<?= site_url('catalog/price/edit_price/' . encode($branch->product_id) . '/' . encode($branch->branch_id)) ?>" class="btn btn-primary text-center">Edit Harga</a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else : ?>
                                    <tr class="list-branch">
                                        <td colspan="3" class="text-center">Tidak ada cabang</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    </div>
                    <div class="panel-footer">
                        <div class="heading-elements action-left">
                            <a class="btn btn-default" href="<?php echo site_url('catalog/price'); ?>">Kembali</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>