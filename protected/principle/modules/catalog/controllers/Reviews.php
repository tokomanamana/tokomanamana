<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Reviews extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->aauth->control('catalog/review');
        $this->lang->load('catalog/reviews', settings('language'));
        $this->load->model('reviews_model', 'reviews');
        $this->data['menu'] = 'catalog_review';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('review'), '/catalog/reviews');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('review_heading'));
        $this->load->view('reviews/list', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->reviews->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->product,
                    $data->customer,
                    $data->rating,
                    $data->title,
                    nl2br($data->description),
                    $this->_status_review($data->status),
                    $data->date,
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">' .
                    (($data->status == 0 && $this->aauth->is_allowed('catalog/review/verification')) ? '<li><a href="' . site_url('catalog/reviews/activate/' . encode($data->id)) . '" class="activate">' . lang('button_activate') . '</a></li>' : '') .
                    ($this->aauth->is_allowed('catalog/review/delete')?'<li><a href="' . site_url('catalog/reviews/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>':'').
                    '</ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->reviews->count_all();
        $output['recordsFiltered'] = $this->reviews->count_all($search);
        echo json_encode($output);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
$this->aauth->control('catalog/review/delete');
        $id = decode($id);
        $data = $this->main->get('product_review', array('id' => $id));
        $delete = $this->main->delete('product_review', array('id' => $id));
        if ($delete) {
            $this->main->delete('seo_url', array('query' => 'catalog/reviews/view/' . $id));
            $return = array('message' => sprintf(lang('review_delete_success_message'), $data->name), 'status' => 'success');
        } else {
            $return = array('message' => sprintf(lang('review_delete_error_message'), $data->name), 'status' => 'danger');
        }
        echo json_encode($return);
    }

    public function activate($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $update = $this->main->update('product_review', array('status' => 1), array('id' => $id));
        $return = array('message' => lang('message_activate_success'), 'status' => 'success');
        echo json_encode($return);
    }

    public function disabled($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $update = $this->main->update('product_review', array('status' => 2), array('id' => $id));
        $return = array('message' => lang('di non-aktifkan'), 'status' => 'success');
        echo json_encode($return);
    }

    private function _status_review($status) {
        switch ($status) {
            case 0: $text = '<i class="icon-cross2 text-danger-400"></i>';
                break;
            case 1: $text = '<i class="icon-checkmark3 text-success"></i>';
                break;
        }
        return $text;
    }

}
