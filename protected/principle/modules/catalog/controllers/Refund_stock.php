<?php

defined('BASEPATH') or exit('No direct script access allowed!');

require '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Refund_stock extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->aauth->control('catalog/refund_stock');

        $this->lang->load('catalog/refund_stock', settings('language'));
        $this->load->model('refund_stock_model', 'refund_stock');
        $this->load->helper('url');
        $this->data['menu'] = 'catalog_refund_stock';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();
        $this->load->js('../assets/principle/js/modules/catalog/products.js');
        $this->load->js('../assets/principle/js/modules/catalog/refund_stock.js');

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('heading'), '/catalog/refund_stock');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('heading'));
        
        $this->load->view('refund_stock/list', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));
        $filter = $this->input->post('filter');
        $principal_id = $this->data['user']->principle_id;

        $output['data'] = array();
        $datas = $this->refund_stock->get_all($start, $length, $search, $order, $principal_id);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->code,
                    $data->name,
                    $data->cabang,
                    $data->new_quantity,
                    $data->date_added,
                    '<button class="btn btn-primary" onclick="confirm(' . $data->id . ', ' . $data->new_quantity . ')">Konfirmasi</button>'
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->refund_stock->count_all('', $principal_id);
        $output['recordsFiltered'] = $this->refund_stock->count_all($search, $principal_id);
        echo json_encode($output);
    }

    public function confirm() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = $this->input->post('id');
        $new_stock = $this->input->post('stock');

        $data = [
            'quantity' => $new_stock,
            'quantity_revisi' => null
        ];
        $update = $this->main->update('products_principal_history', $data, ['id' => $id]);

        if($update) {
            $return = [
                'status' => 'success',
                'message' => 'Stok berhasil dikonfirmasi!'
            ];
        } else {
            $return = [
                'status' => 'error',
                'message' => 'Terjadi suatu kesalahan!'
            ];
        }

        echo json_encode($return);
    }

    public function cancel() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $id = $this->input->post('id');

        $data = [
            'status' => 3,
            'quantity_revisi' => null
        ];
        $update = $this->main->update('products_principal_history', $data, ['id' => $id]);

        if($update) {
            $return = [
                'status' => 'success',
                'message' => 'Stok Dibatalkan!'
            ];
        } else {
            $return = [
                'status' => 'error',
                'message' => 'Terjadi suatu kesalahan!'
            ];
        }
        echo json_encode($return);
    }

}
