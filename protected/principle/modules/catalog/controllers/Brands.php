<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Brands extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->aauth->control('catalog/brand');

        $this->lang->load('brands', settings('language'));
        $this->load->model('brands_model', 'brands');

        $this->data['menu'] = 'catalog_brand';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('brand'), '/catalog/brands');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('brand_heading'));
        $this->load->view('brands/brands', $this->data);
    }

    // public function index() {
    //     $this->template->_init();
    //     $this->template->form();

    //     $this->load->js('../assets/backend/js/modules/catalog/brand_form.js');
    //     $this->data['data'] = $this->main->get('brands', ['principle_id' => $this->data['user']->principle_id]);

    //     $this->breadcrumbs->unshift(lang('catalog'), '/');
    //     $this->breadcrumbs->push(lang('brand'), '/catalog/brands');

    //     $this->data['breadcrumbs'] = $this->breadcrumbs->show();

    //     $this->output->set_title(lang('brand_heading'));
    //     $this->load->view('brands/brand_form', $this->data);
    // }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->brands->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $img = '';
                $url = '';
                if($data->image) {
                    $img = site_url('../files/images/principal/' . $data->image);
                    if(file_exists(FCPATH . '../files/images/principal/' . $data->image)) {
                        $url = $img;
                    } else {
                        $url = site_url('../files/images/' . $data->image);
                    }
                }
                $output['data'][] = array(
                    ($data->image) ? '<a href="#"><img src="' . $url . '" class="img-circle img-xs"></a>' : '',
                    $data->name,
                    $data->seo_url,
                    // $this->status_($data->status),
                    // '<td class="text-center">
                    // <ul class="icons-list">
                    // <li class="dropdown">
                    // <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    // <ul class="dropdown-menu dropdown-menu-right">
                    // <li><a href="' . site_url('catalog/brands/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>
                    // <li><a href="' . site_url('catalog/brands/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>
                    // </ul>
                    // </li>
                    // </ul>
                    // </td>',
                    '<a href="' . site_url('catalog/brands/form/' . encode($data->id)) . '"><button type="button"   class="edit btn btn-info" style="background: #5BC0DE;">' . lang('button_edit') . '</button></a> ' . '<a href="' . site_url('catalog/brands/delete/' . encode($data->id)) . '" class="delete btn btn-danger" style="background: #D9534F">' . lang('button_delete') . '</a>'
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->brands->count_all();
        $output['recordsFiltered'] = $this->brands->count_all($search);
        echo json_encode($output);
    }

    public function form($id = '') {
        $this->template->_init();
        $this->template->form();

        $this->load->js('../assets/principle/js/modules/catalog/brand_form.js');

        $this->data['data'] = array();
        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('brand'), '/catalog/brands');

        if ($id) {
            $id = decode($id);
            $this->data['data'] = $this->brands->get($id);
            // $this->data['data']->description = tinymce_get_url_files($this->data['data']->description);
            $this->data['data']->description = str_replace("<br />", "\n", $this->data['data']->description); 
            $this->breadcrumbs->push(lang('brand_edit_heading'), '/catalog/brands/form/' . encode($id));
            $this->breadcrumbs->push($this->data['data']->name, '/');
        } else {
            $this->breadcrumbs->push(lang('brand_add_heading'), '/catalog/brands/form');
        }
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $principal = $this->main->get('principles', ['id' => $this->data['user']->principle_id]);
        $aauthp = $this->main->get('aauthp_users', ['id' => $principal->auth]);
        $this->data['username'] = $aauthp->username;

        $this->output->set_title(($this->data['data']) ? lang('brand_edit_heading') : lang('brand_add_heading'));
        $this->load->view('brands/brand_form', $this->data);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'lang:brand_form_name_label', 'trim|required');
        // $this->form_validation->set_rules('seo_url', 'lang:brand_form_seo_url_label', 'trim|alpha_dash');
        // $this->form_validation->set_rules('image', 'lang:brand_form_image_label', 'required');
        $this->form_validation->set_rules('description', 'lang:brand_form_description_label', 'required|trim');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);
            if($data['image'] == '') {
                $return = array('message' => 'Gambar harus diisi!', 'status' => 'error');
                echo json_encode($return);
                exit();
            } 

            // $data['description'] = tinymce_parse_url_files($data['description']);
            $data['meta_description'] = $data['description'];
            $data['description'] = str_replace("\n", "<br />",  $data['description']); 
            $data['principle_id'] = $this->data['user']->principle_id;
            // $data['status'] = (isset($data['status']) && $data['status'] == 1) ? $data['status'] : 0;
            $data['status'] = 0;
            $data['meta_title'] = $data['name'];

            if($data['id']) {
                $data['id'] = decode($data['id']);
                // $brand = $this->main->get('brands', ['id' => $data['id']]);
                // $keyword = $this->generateSeoURL($data['name']);
                // $query = 'catalog/brands/view/' . $data['id'];
                // if(check_url($keyword, $query)) {
                //     $return = array('message' => 'Seo URL sudah digunakan', 'status' => 'error');
                //     echo json_encode($return);
                //     exit();
                // }
                // $this->main->delete('seo_url', array('query' => $query));
                
                // $path = FCPATH.'../files/images/'.$brand->image;
                // if($path && is_readable($path)) unlink($path);
                // $this->main->insert('seo_url', ['keyword' => $keyword, 'query' => $query]);
                $brand = $this->main->get('brands', ['id' => $data['id']]);
                if($brand->image != $data['image']) {
                    $old_brand_image = FCPATH . '../files/images/' . $brand->image;
                    if(file_exists($old_brand_image)) {
                        unlink($old_brand_image);
                    }
                } 
                $this->main->update('brands', $data, ['id' => $data['id']]);
                $return = array('message' => sprintf(lang('brand_save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('catalog/brands'));
            } else {
                // $url = $this->generateSeoURL($data['name']);
                // $check_url = $this->main->get('seo_url', ['keyword' => $url]);
                // if($check_url) {
                //     $return = array('message' => 'Nama brands sudah digunakan!', 'status' => 'error');
                //     echo json_encode($return);
                //     exit();
                // } else {
                //     $insert = $this->main->insert('brands', $data);
                //     $query = 'catalog/brands/view/' . $insert;
                //     $this->main->insert('seo_url', ['query' => $query, 'keyword' => $url]);
                //     $return = array('message' => sprintf(lang('brand_save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('catalog/brands'));
                // }
                $insert = $this->main->insert('brands', $data);
                $return = array('message' => sprintf(lang('brand_save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('catalog/brands'));
            }
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    private function generateSeoURL($string, $wordLimit = 0){
        $separator = '-';
        
        if($wordLimit != 0){
            $wordArr = explode(' ', $string);
            $string = implode(' ', array_slice($wordArr, 0, $wordLimit));
        }
    
        $quoteSeparator = preg_quote($separator, '#');
    
        $trans = array(
            '&.+?;'                    => '',
            '[^\w\d _-]'            => '',
            '\s+'                    => $separator,
            '('.$quoteSeparator.')+'=> $separator
        );
    
        $string = strip_tags($string);
        foreach ($trans as $key => $val){
            $string = preg_replace('#'.$key.'#i'.(UTF8_ENABLED ? 'u' : ''), $val, $string);
        }
    
        $string = strtolower($string);
    
        return trim(trim($string, $separator));
    }

    public function save_new() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'lang:brand_form_name_label', 'trim|required');
        // $this->form_validation->set_rules('seo_url', 'lang:brand_form_seo_url_label', 'trim|alpha_dash');
        // $this->form_validation->set_rules('image', 'lang:brand_form_image_label', 'required');
        $this->form_validation->set_rules('description', 'lang:brand_form_description_label', 'required|trim');
        $principle_id = $this->data['user']->principle_id;

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);
            $data['id'] = decode($data['id']);
            $check_image = $this->main->get('brands', ['id' => $data['id']])->image;
            $data['image'] = $check_image;

            if($_FILES) {
                $today = date('Y_m_d');
                $rand = rand(100, 200);
                $full_file_name = 'brand_principal_' . $principle_id . '_' . $today . '_' . $rand;

                $config['upload_path']          = FCPATH . '../files/images/BRAND';
                $config['allowed_types']        = 'jpg|jpeg|png';
                $config['file_name']            = $full_file_name;
                $config['overwrite']            = true;
                $config['max_size']             = 2048;

                $this->load->library('upload', $config);
                $old_filename = $_FILES['image']['name'];
                $extension = substr($old_filename, strpos($old_filename, ".") + 1);
                $file_name = $full_file_name . '.' . $extension;
                if(!$this->upload->do_upload('image')) {
                    $return = array('message' => $this->upload->display_errors(), 'status' => 'error');
                    echo json_encode($return);
                    exit();
                } else {
                    if(file_exists('../files/images/' . $check_image)) {
                        unlink('../files/images/' . $check_image);
                    }
                    $data['image'] = 'BRAND/' . $file_name;
                }
            }

            // $data['description'] = tinymce_parse_url_files($data['description']);
            $data['principle_id'] = $principle_id;
            $data['status'] = 1;
            $data['meta_title'] = $data['name'];
            $data['meta_description'] = $data['description'];

            $this->main->update('brands', $data, ['id' => $data['id']]);
            $return = array('message' => sprintf(lang('brand_save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('catalog/brands'));
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $data = $this->main->get('brands', array('id' => $id));

        $delete = $this->main->delete('brands', array('id' => $id));
        if ($delete) {
            if($data->image) {
                $brand_image = FCPATH . '../files/images' . $data->image;
                if(file_exists($brand_image)) {
                    unlink($brand_image);
                }
            }
            $this->main->delete('seo_url', array('query' => 'catalog/brands/view/' . $data->id));
            $return = array('message' => sprintf(lang('brand_delete_success_message'), $data->name), 'status' => 'success');
        } else {
            $return = array('message' => lang('brand_delete_error_message'), 'status' => 'error');
        }

        echo json_encode($return);
    }

    private function status_($status) {
        if($status == 1) {
            $return = '<span style="padding:5px 10px;background:#5cb85c;color:white;border-radius:3px;">Aktif</span>';
        } else {
            $return = '<span style="padding:5px 10px;background:#d9534f;color:white;border-radius:3px;">Nonaktif</span>';
        }
        return $return;
    }

    public function image() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $data = $this->input->post(null, true);
        $return = [];
        $config['upload_path']          = FCPATH.'../files/images/BRAND/';
        $config['allowed_types']        = 'jpg|jpeg|png';
        $config['file_name']            = $data['image_name'];
        $config['overwrite']            = true;
        $config['max_size']             = 1024; //1MB

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('brand_image'))
        {
            $error = $this->upload->display_errors();
            $return = [
                'status' => 'error',
                'message' => $error
            ];
        }
        else
        {   
            // if($data['brand_image_temp']) {
            //     $brand_image = FCPATH . '../files/images/' . $data['brand_image_temp'];
            //     if(file_exists($brand_image)) {
            //         unlink($brand_image);
            //     }
            // }
            $return = [
                'status' => 'success',
                'message' => 'BRAND/' . $config['file_name']
            ];
        }
        echo json_encode($return);
    }

    public function delete_image() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $data = $this->input->post(null, true);
        $brand_image = FCPATH . '../files/images/' . $data['image_name'];
        if(file_exists($brand_image)) {
            unlink($brand_image);
        }
        echo 'success';
    }

}
