<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Price extends CI_Controller {
    
    public function __construct() {
        parent::__construct();

        $this->aauth->control('catalog/price');

        $this->lang->load('catalog/price', settings('language'));
        $this->load->model('price_model', 'price');
        $this->load->helper('url');
        $this->data['menu'] = 'catalog_price';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('heading'), '/catalog/price');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->output->set_title(lang('heading'));
        $this->load->view('price/list_product', $this->data);
    }

    public function get_list_product() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->price->get_all_product($start, $length, $search, $order);
        if($datas) {
            foreach($datas->result() as $data) {
                $output['data'][] = [
                    $data->name,
                    ($data->variation) ? '<i>Produk Bervariasi</i>' : 'Rp. ' . number($data->price),
                    ($this->aauth->is_allowed('catalog/price/edit_price') ? '<a href="' . site_url('catalog/price/branch/' . encode($data->id)) . '" class="btn btn-primary">Pilih Produk</a>' : '')
                ];
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->price->count_all_products();
        $output['recordsFiltered'] = $this->price->count_all_products($search);
        echo json_encode($output);
    }

    public function branch($id = '') {
        if($id == '') {
            redirect('catalog/price');
        }

        $id = decode($id);
        $product = $this->main->get('products', ['id' => $id]);
        if(!$product) {
            redirect('catalog/price');
        }

        $this->template->_init();

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('heading'), '/catalog/price');
        $this->breadcrumbs->push($product->name, '/catalog/price/branch/' . encode($id));

        $data_branch = $this->price->get_branches($id);

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->data['product_name'] = $product->name;
        $this->data['branches'] = $data_branch;
        $this->data['product_id'] = $product->id;
        $this->load->js('../assets/principle/js/modules/catalog/price_branch_list.js');
        $this->output->set_title(lang('heading_branch') . ' ' . $product->name);
        $this->load->view('price/list_branch', $this->data);
    }

    public function edit_price($product = '', $branch = '') {
        if($product == '' || $branch == '') {
            redirect('catalog/price');
        }

        $this->aauth->control('catalog/edit_price');

        $this->template->_init();
        $this->template->form();

        $product_id = decode($product);
        $branch_id = decode($branch);
        $this->data['id_product'] = $product_id;
        $this->data['id_branch'] = $branch_id;

        $product = $this->main->get('products', ['id' => $product_id]);
        if($product->variation) {
            $product_branch = $this->main->gets('products_principal_stock', ['product_id' => $product_id, 'branch_id' => $branch_id]);
        } else {
            $product_branch = $this->main->get('products_principal_stock', ['product_id' => $product_id, 'branch_id' => $branch_id]);
        }

        if(!$product_branch) {
            redirect('catalog/price');
        }

        $this->data['promo_data'] = [];

        if(!$product->variation) {
            $this->data['data'] = $product_branch;
            $this->data['status_variation'] = 0;
            if($product_branch->price) {
                $this->data['data']->price = $product_branch->price;
                $this->data['price_default'] = 0;
            } else {
                $this->data['data']->price = $product->price;
                $this->data['price_default'] = 1;
            }
            if($product_branch->id_price_level) {
                $price_level_data = json_decode($product_branch->id_price_level);
                $this->data['price_level_status'] = $price_level_data->status;
                $temp = [];
                foreach($price_level_data->data as $price_level) {
                    $get_price_level = $this->main->get('product_price_level', ['id' => $price_level]);
                    array_push($temp, $get_price_level);
                }
                $this->data['price_levels'] = $temp;
            } else {
                $this->data['price_level_status'] = 0;
            }
            if($product_branch->id_price_reseller) {
                $price_reseller_data = json_decode($product_branch->id_price_reseller);
                $this->data['price_reseller_status'] = $price_reseller_data->status;
                $temp = [];
                foreach($price_reseller_data->data as $price_reseller) {
                    $get_price_reseller = $this->main->get('product_price_reseller', ['id' => $price_reseller]);
                    array_push($temp, $get_price_reseller);
                }
                $this->data['price_reseller'] = $temp;
            } else {
                $this->data['price_reseller_status'] = 0;
            }
            if($product_branch->promo_data) {
                $promo_data = json_decode($product_branch->promo_data);
                $this->data['promo_status'] = $promo_data->status;
                $this->data['promo_data']['type'] = $promo_data->type;
                $this->data['promo_data']['date_start'] = $promo_data->date_start;
                $this->data['promo_data']['date_end'] = $promo_data->date_end;
                $this->data['promo_data']['discount'] = $promo_data->discount;
            } else {
                $this->data['promo_status'] = 0;
            }
        } else {
            $this->data['status_variation'] = 1;
            $this->data['data'] = $product_branch;
            if($product_branch->row(0)->price) {
                $this->data['price_default'] = 0;
            } else {
                $this->data['price_default'] = 1;
            }
            if($product_branch->row(0)->id_price_level) {
                $price_level_data = json_decode($product_branch->row(0)->id_price_level);
                $this->data['price_level_status'] = $price_level_data->status;
                $temp = [];
                foreach($price_level_data->data as $price_level) {
                    $get_price_level = $this->main->get('product_price_level', ['id' => $price_level]);
                    array_push($temp, $get_price_level);
                }
                $this->data['price_levels'] = $temp;
                $this->data['price'] = $product_branch->row(0)->price;
            } else {
                $this->data['price_level_status'] = 0;
            }
            if($product_branch->row(0)->id_price_reseller) {
                $price_reseller_data = json_decode($product_branch->row(0)->id_price_reseller);
                $this->data['price_reseller_status'] = $price_reseller_data->status;
                $temp = [];
                foreach($price_reseller_data->data as $price_reseller) {
                    $get_price_reseller = $this->main->get('product_price_reseller', ['id' => $price_reseller]);
                    array_push($temp, $get_price_reseller);
                }
                $this->data['price_reseller'] = $temp;
                $this->data['price'] = $product_branch->row(0)->price;
            } else {
                $this->data['price_reseller_status'] = 0;
            }
            if($product_branch->row(0)->promo_data) {
                $promo_data = json_decode($product_branch->row(0)->promo_data);
                $this->data['promo_status'] = $promo_data->status;
                $this->data['promo_data']['type'] = $promo_data->type;
                $this->data['promo_data']['date_start'] = $promo_data->date_start;
                $this->data['promo_data']['date_end'] = $promo_data->date_end;
                $this->data['promo_data']['discount'] = $promo_data->discount;
            } else {
                $this->data['promo_status'] = 0;
            }
        }

        $this->load->js('../assets/principle/js/modules/catalog/form_price.js');
        $this->output->set_title(lang('form_heading') . ' ' . $product->name);
        $this->load->view('price/form', $this->data);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->load->library('form_validation');

        if($this->input->post('default_price') == "0") {
            if($this->input->post('promo')){
                $promo = $this->input->post('promo');
                if($promo == 1){
                    $this->form_validation->set_rules('promo_data[discount]', 'lang:discount', 'trim|required|numeric|greater_than[0]');
                    $this->form_validation->set_rules('promo_data[date_start]', 'lang:promo_range_start', 'trim|required');
                    $this->form_validation->set_rules('promo_data[date_end]', 'lang:promo_range_end', 'trim|required');
                }
            }
            if($this->input->post('status_variation') == "0") {
                $this->form_validation->set_rules('price', 'lang:price', 'trim|required|numeric|greater_than[100]');
            }
        } 
        $this->form_validation->set_rules('default_price', 'Price Default', 'trim');

        if($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);
            $highest_qty = '';
            if($data['default_price'] == 0) {
                if($data['price_grosir'] == 1) {
                    if(!isset($data['price_level'])) {
                        $return = [
                            'status' => 'error',
                            'message' => 'Jika harga grosir aktif, maka harga grosir harus ada minimal 1'
                        ];
                        echo json_encode($return);
                        exit();
                    }
                    if(isset($data['price_level'])) {
                        $increment_grosir_check = 0;
                        $increment_grosir_qty_check = 0;
                        $count = 0;
                        $grosir_error = 0;
                        if($data['status_variation'] == 1) {
                            $price_temp = $data['price_option'][0];
                        } else {
                            $price_temp = $data['price'];
                        }
                        foreach($data['price_level'] as $price_l) {
                            if (isset($price_l['qty']) && isset($price_l['price'])) {
                            
                                if($count > 0){
                                    if($increment_grosir_check <= $price_l['price']){
                                        $grosir_error++;
                                        
                                    }
                                    if($price_l['qty'] <= $increment_grosir_qty_check){
                                        $grosir_error++;
                                    }
    
                                }
                                
                                if($price_l['price'] >= $price_temp){
                                    $grosir_error++;
                                    
                                }
                               
                                $increment_grosir_check = $price_l['price'];
                                $increment_grosir_qty_check = $price_l['qty'];
                                $count++;
                             
                            }
                            if($grosir_error > 0) {
                                $return = [
                                    'status' => 'error',
                                    'message' => 'Cek kembali harga grosir anda'
                                ];
                                echo json_encode($return);
                                exit();
                            } else {
                                $highest_qty = 0;
                                foreach($data['price_level'] as $price_l) {
                                    if($price_l['qty'] > $highest_qty){
    
                                        $highest_qty = $price_l['qty'];
                                   }
                                }
                            }
                        }
                    }
                }
                if($data['price_reseller'] == 1) {
                    if(!isset($data['price_reseller_list'])) {
                        $return = [
                            'status' => 'error',
                            'message' => 'Jika harga reseller aktif, maka harga reseller harus ada minimal 1'
                        ];
                        echo json_encode($return);
                        exit();
                    }
                    if(isset($data['price_reseller_list'])) {
                        $increment_reseller_check = 0;
                        $increment_reseller_qty_check = 0;
                        $count = 0;
                        $reseller_error = 0;
                        if($data['status_variation'] == 1) {
                            $price_temp = $data['price_option'][0];
                        } else {
                            $price_temp = $data['price'];
                        }
                        foreach ($data['price_reseller_list'] as $reseller) {
                            if (isset($reseller['qty']) && isset($reseller['price'])) {
                                if(!empty($highest_qty)){
                                    if($highest_qty != '') {
                                        if($reseller['qty'] <= $highest_qty){
                                            $return = array('message' => 'Jumlah produk reseller tidak boleh kurang atau sama dari jumlah produk harga grosir!', 'status' => 'error');
                                            echo json_encode($return);
                                            exit();
                                        }
                                    }
                                }
                                if($count > 0){
                                    if($increment_reseller_check <= $reseller['price']){
                                        $reseller_error++;
                                        
                                    }
                                    if($reseller['qty'] <= $increment_reseller_qty_check){
                                        $reseller_error++;
                                    }
    
                                }
                                
                                if($reseller['price'] >= $price_temp){
                                    $reseller_error++;
                                    
                                }
                                
                                
                               
                                $increment_reseller_check = $reseller['price'];
                                $increment_reseller_qty_check = $reseller['qty'];
                                $count++;
    
                            }
                            if($reseller_error > 0) {
                                $return = [
                                    'status' => 'error',
                                    'message' => 'Cek kembali harga reseller anda'
                                ];
                                echo json_encode($return);
                                exit();
                            }
                        }
                    }
                }
            }
            if($data['status_variation'] == '1') {
                if(isset($data['price_option'])) {
                    foreach($data['price_option'] as $price_opt) {
                        if($price_opt == '0') {
                            $return = [
                                'status' => 'error',
                                'message' => 'Harga variasi harus diisi!'
                            ];
                            echo json_encode($return);
                            exit();
                        }
                    }
                }
                $id_branch = 0;
                $id_product = 0;
                foreach($data['id'] as $key => $id_principal_stock) {
                    $id_principal_stock = decode($id_principal_stock);
                    $product_branch = $this->main->get('products_principal_stock', ['id' => $id_principal_stock]);
                    $id_branch = $product_branch->branch_id;
                    $id_product = $product_branch->product_id;
                    if($data['default_price'] == '1') {
                        if($product_branch->id_price_level) {
                            $price_level_data = json_decode($product_branch->id_price_level);
                            foreach($price_level_data->data as $price_level) {
                                $this->main->delete('product_price_level', ['id' => $price_level]);
                            }
                        }
                        if($product_branch->id_price_reseller) {
                            $price_reseller_data = json_decode($product_branch->id_price_reseller);
                            foreach($price_reseller_data->data as $price_reseller) {
                                $this->main->delete('product_price_reseller', ['id' => $price_reseller]);
                            }
                        }
                        $this->main->update('products_principal_stock', ['price' => '', 'id_price_level' => '', 'id_price_reseller' => '', 'promo_data' => ''], ['id' => $product_branch->id]);
                        $return = array('message' => 'Harga berhasil disimpan', 'status' => 'success', 'redirect' => site_url('catalog/price/branch/' . encode($product_branch->product_id)));
                    } else {
                        $level_prices = [];
                        $reseller_prices = [];
                        if (isset($data['price_level'])) {
                            $level_prices = $data['price_level'];
                            unset($data['price_level']);
                        }
                        if (isset($data['price_reseller_list'])) {
                            $reseller_prices = $data['price_reseller_list'];
                            unset($data['price_reseller_list']);
                        }
                        if (isset($data['promo_data'])) {
                            if($data['promo']) {
                                $data['promo_data']['status'] = 1;
                                $data['promo_data']['date_start'] = date('Y-m-d', strtotime($data['promo_data']['date_start']));
                                $data['promo_data']['date_end'] = date('Y-m-d', strtotime($data['promo_data']['date_end']));
                                $promo_data = json_encode($data['promo_data']);
                            } else {
                                $promo_data = '';
                            }
                        } else {
                            $promo_data = '';
                        }

                        if($product_branch->id_price_level) {
                            $price_level_data = json_decode($product_branch->id_price_level, true);
                            if($data['price_grosir']) {
                                if(count($level_prices) > 0) {
                                    $price_level_data['status'] = 1;
                                    foreach($price_level_data['data'] as $id) {
                                        $this->main->delete('product_price_level', ['id' => $id]);
                                    }
                                    $price_level_data['data'] = '';
                                    $temp = [];
                                    foreach($level_prices as $price) {
                                        $price_level = $this->main->insert('product_price_level', ['product' => $product_branch->product_id, 'min_qty' => $price['qty'], 'price' => $price['price']]);
                                        array_push($temp, $price_level);
                                    }
                                    $price_level_data['data'] = $temp;
                                    $data_price_level[$key] = json_encode($price_level_data);
                                } else {
                                    $data_price_level[$key] = json_encode($price_level_data);
                                }
                                
                            } else {
                                $price_level_data['status'] = 0;
                                $data_price_level[$key] = json_encode($price_level_data);
                            }
                        } else {
                            if($data['price_grosir'] == '1') {
                                if(count($level_prices) > 0) {
                                    $temp = [
                                        'data' => [],
                                        'status' => 1
                                    ];
                                    foreach($level_prices as $price) {
                                        $price_level = $this->main->insert('product_price_level', ['product' => $product_branch->product_id, 'min_qty' => $price['qty'], 'price' => $price['price']]);
                                        array_push($temp['data'], $price_level);
                                    }
                                    $data_price_level[$key] = json_encode($temp);
                                } else {
                                    $data_price_level[$key] = '';
                                }
                            } else {
                                $data_price_level[$key] = '';
                            }
                        }

                        if($product_branch->id_price_reseller) {
                            $price_reseller_data = json_decode($product_branch->id_price_reseller, true);
                            if($data['price_reseller'] == '1') {
                                if(count($reseller_prices) > 0) {
                                    $price_reseller_data['status'] = 1;
                                    foreach($price_reseller_data['data'] as $id) {
                                        $this->main->delete('product_price_reseller', ['id' => $id]);
                                    }
                                    $temp = [];
                                    foreach($reseller_prices as $reseller) {
                                        $price_reseller = $this->main->insert('product_price_reseller', array('product' => $product_branch->product_id, 'min_qty' => $reseller['qty'], 'price' => $reseller['price']));
                                        array_push($temp, $price_reseller);
                                    }
                                    $price_reseller_data['data'] = $temp;
                                    $data_price_reseller[$key] = json_encode($price_reseller_data);
                                } else {
                                    $data_price_reseller[$key] = json_encode($price_reseller_data);
                                }
                            } else {
                                $price_reseller_data['status'] = 0;
                                $data_price_reseller[$key] = json_encode($price_reseller_data);
                            }
                        } else {
                            if($data['price_reseller'] == '1') {
                                if(count($reseller_prices) > 0) {
                                    $temp = [
                                        'data' => [],
                                        'status' => 1
                                    ];
                                    foreach($reseller_prices as $reseller) {
                                        $price_reseller = $this->main->insert('product_price_reseller', array('product' => $product_branch->product_id, 'min_qty' => $reseller['qty'], 'price' => $reseller['price']));
                                        array_push($temp['data'], $price_reseller);
                                    }
                                    $data_price_reseller[$key] = json_encode($temp);
                                } else {
                                    $data_price_reseller[$key] = '';
                                }
                            } else {
                                $data_price_reseller[$key] = '';
                            }
                        }

                        $price_option = $data['price_option'][$key];

                        $this->main->update('products_principal_stock', ['price' => $price_option, 'promo_data' => $promo_data, 'id_price_level' => $data_price_level[0], 'id_price_reseller' => $data_price_reseller[0]], ['id' => $product_branch->id]);
                    }
                }
                $return = array('message' => 'Harga berhasil disimpan', 'status' => 'success', 'redirect' => site_url('catalog/price/branch/' . encode($id_product)));
            } else {
                $id = decode($this->input->post('id'));
                $product_branch = $this->main->get('products_principal_stock', ['id' => $id]);
                if($data['default_price']) {
                    if($product_branch->id_price_level) {
                        $price_level_data = json_decode($product_branch->id_price_level);
                        foreach($price_level_data->data as $price_level) {
                            $this->main->delete('product_price_level', ['id' => $price_level]);
                        }
                    }
                    if($product_branch->id_price_reseller) {
                        $price_reseller_data = json_decode($product_branch->id_price_reseller);
                        foreach($price_reseller_data->data as $price_reseller) {
                            $this->main->delete('product_price_reseller', ['id' => $price_reseller]);
                        }
                    }
                    $this->main->update('products_principal_stock', ['price' => '', 'id_price_level' => '', 'id_price_reseller' => '', 'promo_data' => ''], ['id' => $product_branch->id]);
                    $return = array('message' => 'Harga berhasil disimpan', 'status' => 'success', 'redirect' => site_url('catalog/price/branch/' . encode($product_branch->product_id)));
                } else {
                    do{
                        $level_prices = array();
                        $reseller_prices = array();

                        if (isset($data['price_level'])) {
                            $level_prices = $data['price_level'];
                            unset($data['price_level']);
                        }
                        if (isset($data['price_reseller_list'])) {
                            $reseller_prices = $data['price_reseller_list'];
                            unset($data['price_reseller_list']);
                        }
                        if (isset($data['promo_data'])) {
                            if($data['promo']) {
                                $data['promo_data']['status'] = 1;
                                $data['promo_data']['date_start'] = date('Y-m-d', strtotime($data['promo_data']['date_start']));
                                $data['promo_data']['date_end'] = date('Y-m-d', strtotime($data['promo_data']['date_end']));
                                $promo_data = json_encode($data['promo_data']);
                                // $this->main->update('products', ['promo' => 1], ['id' => $product_branch->product_id]);
                            } else {
                                $promo_data = '';
                            }
                        } else {
                            $promo_data = '';
                        }

                        if($product_branch->id_price_level) {
                            $price_level_data = json_decode($product_branch->id_price_level, true);
                            if($data['price_grosir']) {
                                if(count($level_prices) > 0) {
                                    $price_level_data['status'] = 1;
                                    foreach($price_level_data['data'] as $id) {
                                        $this->main->delete('product_price_level', ['id' => $id]);
                                    }
                                    $price_level_data['data'] = '';
                                    $temp = [];
                                    foreach($level_prices as $price) {
                                        $price_level = $this->main->insert('product_price_level', ['product' => $product_branch->product_id, 'min_qty' => $price['qty'], 'price' => $price['price']]);
                                        array_push($temp, $price_level);
                                    }
                                    $price_level_data['data'] = $temp;
                                    $data_price_level = json_encode($price_level_data);
                                } else {
                                    $data_price_level = json_encode($price_level_data);
                                }
                                
                            } else {
                                $price_level_data['status'] = 0;
                                $data_price_level = json_encode($price_level_data);
                            }
                        } else {
                            if($data['price_grosir'] == '1') {
                                if(count($level_prices) > 0) {
                                    $temp = [
                                        'data' => [],
                                        'status' => 1
                                    ];
                                    foreach($level_prices as $price) {
                                        $price_level = $this->main->insert('product_price_level', ['product' => $product_branch->product_id, 'min_qty' => $price['qty'], 'price' => $price['price']]);
                                        array_push($temp['data'], $price_level);
                                    }
                                    $data_price_level = json_encode($temp);
                                } else {
                                    $data_price_level = '';
                                }
                            } else {
                                $data_price_level = '';
                            }
                        }

                        if($product_branch->id_price_reseller) {
                            $price_reseller_data = json_decode($product_branch->id_price_reseller, true);
                            if($data['price_reseller'] == '1') {
                                if(count($reseller_prices) > 0) {
                                    $price_reseller_data['status'] = 1;
                                    foreach($price_reseller_data['data'] as $id) {
                                        $this->main->delete('product_price_reseller', ['id' => $id]);
                                    }
                                    $temp = [];
                                    foreach($reseller_prices as $reseller) {
                                        $price_reseller = $this->main->insert('product_price_reseller', array('product' => $product_branch->product_id, 'min_qty' => $reseller['qty'], 'price' => $reseller['price']));
                                        array_push($temp, $price_reseller);
                                    }
                                    $price_reseller_data['data'] = $temp;
                                    $data_price_reseller = json_encode($price_reseller_data);
                                } else {
                                    $data_price_reseller = json_encode($price_reseller_data);
                                }
                            } else {
                                $price_reseller_data['status'] = 0;
                                $data_price_reseller = json_encode($price_reseller_data);
                            }
                        } else {
                            if($data['price_reseller'] == '1') {
                                if(count($reseller_prices) > 0) {
                                    $temp = [
                                        'data' => [],
                                        'status' => 1
                                    ];
                                    foreach($reseller_prices as $reseller) {
                                        $price_reseller = $this->main->insert('product_price_reseller', array('product' => $product_branch->product_id, 'min_qty' => $reseller['qty'], 'price' => $reseller['price']));
                                        array_push($temp['data'], $price_reseller);
                                    }
                                    $data_price_reseller = json_encode($temp);
                                } else {
                                    $data_price_reseller = '';
                                }
                            } else {
                                $data_price_reseller = '';
                            }
                        }

                        $this->main->update('products_principal_stock', ['price' => $data['price'], 'promo_data' => $promo_data, 'id_price_level' => $data_price_level, 'id_price_reseller' => $data_price_reseller], ['id' => $product_branch->id]);
                        $return = array('message' => 'Harga berhasil disimpan', 'status' => 'success', 'redirect' => site_url('catalog/price/branch/' . encode($product_branch->product_id)));
                    } while(0);
                }
            }
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function change_default_price() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $product_id = decode($data['product_id']);
        $status_variation = $data['status_variation'];

        if($status_variation == '1') {
            $product_option = $this->main->gets('product_option', ['product' => $product_id]);
            $price_variation = [
                'data' => [],
                'variation' => 1
            ];
            foreach($product_option->result() as $product) {
                array_push($price_variation['data'], [
                    'price' => $product->price,
                    'id' => encode($product->id)
                ]);
            }
            echo json_encode($price_variation);
        } else {
            $product = $this->main->get('products', ['id' => $product_id]);
            $price = [
                'data' => $product->price,
                'variation' => 0
            ];
            echo json_encode($price);
        }
    }

}