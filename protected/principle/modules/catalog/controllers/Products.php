<?php

defined('BASEPATH') or exit('No direct script access allowed!');

require '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Products extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->aauth->control('catalog/product');

        $this->lang->load('catalog/products', settings('language'));
        $this->load->model('products_model', 'products');
        $this->load->model('product_price_model', 'product_prices');
        $this->data['menu'] = 'catalog_product_tanaka';
    }

    public function index()
    {
        $this->template->_init();
        $this->template->table();
        $this->load->js('../assets/principle/js/modules/catalog/products.js');

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('product'), '/catalog/products');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->main->delete('products', ['store_type' => 'principal', 'store_id' => $this->data['user']->principle_id, 'name' => '', 'short_description' => '', 'description' => '']);

        $this->output->set_title(lang('heading'));
        $this->load->view('products/list', $this->data);
    }

    public function get_list()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->products->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                if($data->variation) {
                    $product_option = $this->main->get('product_option', ['product' => $data->id, 'default' => 1]);
                    $data->price = $product_option->price;
                }
                $output['data'][] = array(
                    '<a href="#"><img src="' . site_url('../files/images/' . $data->product_image) . '" height="60" class=""></a>',
                    // '<a href="' . site_url('catalog/products/form/' . encode($data->id)) . '">' . $data->code . '</a>',
                    $data->code,
                    '<a href="' . site_url('catalog/products/form/' . encode($data->id)) . '">' . $data->name . '</a>',
                    number($data->price),
                    $data->category_name,
                    // $this->_status_product($data->status),
                    '<a href="#" class="status" data-id="' . encode($data->id) . '" data-status="' . $data->status . '"  >' . $this->status_product($data->status) . '</i>',
                    ($data->variation) ? '<i>Produk Bervariasi</i>' : '<input type="number" min="0" data-id="' . encode($data->id) . '"  class="quantity form-control" value="' . $data->quantity . '">',
                    // '<ul class="icons-list">
                    // <li class="dropdown">
                    // <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    // <ul class="dropdown-menu dropdown-menu-right">' .
                    //     ($this->aauth->is_allowed('catalog/product/edit') ? '<li><a href="' . site_url('catalog/products/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>' : '') .
                    //     ($this->aauth->is_allowed('catalog/product/delete') ? '<li><a href="' . site_url('catalog/products/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>' : '') .
                    //     '</ul>
                    // </li>
                    // </ul>',
                    ($this->aauth->is_allowed('catalog/product/edit') ? '<a href="' . site_url('catalog/products/form/' . encode($data->id)) . '"><button type="button"   class="edit btn btn-info" style="background: #5BC0DE;">' . lang('button_edit') . '</button></a> ' : '') . ($this->aauth->is_allowed('catalog/product/delete') ? '<a href="' . site_url('catalog/products/delete/' . encode($data->id)) . '" class="delete btn btn-danger" style="background: #D9534F">' . lang('button_delete') . '</a>' : '')
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->products->count_all();
        $output['recordsFiltered'] = $this->products->count_all($search);
        echo json_encode($output);
    }

    public function add()
    {
        $product = $this->main->insert('products', array('is_temp' => 1, 'user_added' => $this->data['user']->id, 'store_id' => $this->data['user']->principle_id, 'store_type' => 'principal'));
        redirect('catalog/products/form/' . encode($product));
    }

    // public function add () {
    //     if($this->session->userdata('image_id_session')) {
    //         $this->session->unset_userdata('image_id_session');
    //     }
    //     $this->data['brands'] = $this->main->gets('brands', array('principle_id'=>$this->data['user']->principle_id), 'name ASC');
    //     $this->data['categories'] = $this->products->get_categories();
    //     $this->data['option_groups'] = $this->main->gets('option_group', array(), 'sort_order asc');
    //     $this->data['merchant_groups'] = $this->main->gets('merchant_groups', array(), 'name asc');
    //     $this->data['prices'] = $this->products->get_prices(0);
    //     $this->data['images'] = null;
    //     $this->data['data'] = null;
    //     $this->data['images'] = null;
    //     $this->data['features'] = null;
    //     $this->data['price_levels'] = null;
    //     $this->data['product_options'] = null;
    //     $this->template->_init();
    //     $this->template->form();
    //     $this->load->js('https://code.jquery.com/ui/1.10.4/jquery-ui.js');
    //     $this->load->js(site_url().'../assets/backend/js/modules/catalog/product_form.js');
    //     $this->output->set_title(lang('heading'));
    //     $this->load->view('products/form', $this->data);
    // }

    public function form($id = null)
    {
        if (!$id) {
            redirect('catalog/products');
        }
        //        $id ? $this->aauth->control('catalog/product/edit') : $this->aauth->control('catalog/product/add');
        $id = decode($id);
        //        $this->data['data'] = $this->main->get('products',array('id'=>$id));
        $this->data['store_fronts'] = $this->main->gets('etalase', ['store_id' => $this->data['user']->principle_id, 'store_type' => 'principal']);
        $this->data['brands'] = $this->main->gets('brands', array('principle_id' => $this->data['user']->principle_id), 'name ASC');
        $principle = $this->main->get('principles', ['id' => $this->data['user']->principle_id])->auth;
        $auth = $this->main->get('aauthp_users', ['id' => $principle]);
        $this->data['username'] = $auth->username;
        $this->data['categories'] = $this->products->get_categories();
        $this->data['option_groups'] = $this->main->gets('option_group', array(), 'sort_order asc');
        $this->data['merchant_groups'] = $this->main->gets('merchant_groups', array(), 'name asc');
        $this->data['price_levels'] = array();
        //        $this->breadcrumbs->unshift(lang('catalog'), '/');
        //        $this->breadcrumbs->push(lang('product'), '/catalog/products');
        //
        //        if ($id) {
        //            $id = decode($id);
        $this->data['data'] = $this->products->get($id);
        $this->data['images'] = $this->main->gets('product_image', array('product' => $id));
        $this->data['features'] = $this->products->get_features_by_category($this->data['data']->category, $this->data['data']->id);
        if ($this->data['data']) {
            $this->data['data']->package_items = json_decode($this->data['data']->package_items);
            $this->data['data']->promo_data = json_decode($this->data['data']->promo_data);
            // $this->data['data']->description = tinymce_get_url_files($this->data['data']->description);
            $this->data['data']->description = str_replace("<br />", "\n", $this->data['data']->description); 
        } else {
            $this->data['data']->package_items = NULL;
            $this->data['data']->promo_data = NULL;
            $this->data['data']->description = NULL;
        }
        $product_branch = $this->main->gets('products_principal_stock', ['product_id' => $id]);
        if($product_branch) {
            $this->data['product_branch'] = 1;
        } else {
            $this->data['product_branch'] = 0;
        }
        $this->data['product_options'] = $this->products->get_product_options($this->data['data']->id);
        $this->data['price_levels'] = $this->main->gets('product_price_level', array('product' => $id), 'min_qty asc');
        $this->data['price_reseller'] = $this->main->gets('product_price_reseller', ['product' => $id], 'min_qty asc');

        //            $this->breadcrumbs->push(lang('edit_heading'), '/catalog/products/form/' . encode($id));
        //            $this->breadcrumbs->push($this->data['data']->name, '/');
        //        } else {
        //            $this->breadcrumbs->push(lang('add_heading'), '/catalog/products/form');
        //        }
        $this->data['prices'] = $this->products->get_prices(($id) ? $id : 0);
        //        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->template->_init();
        $this->template->form();
        $this->load->js('https://code.jquery.com/ui/1.10.4/jquery-ui.js');
        $this->load->js(site_url() . '../assets/principle/js/modules/catalog/product_form.js');
        $this->output->set_title(lang('heading'));
        $this->load->view('products/form', $this->data);
    }

    public function copy()
    {
        $this->data['data'] = array();
        $this->data['zonas'] = $this->main->gets('merchant_groups', array());
        $this->template->_init();
        $this->template->form();
        $this->load->js('https://code.jquery.com/ui/1.10.4/jquery-ui.js');
        $this->load->js('../assets/backend/js/modules/catalog/product_form.js');
        $this->output->set_title(lang('heading'));
        $this->load->view('products/product_copy', $this->data);
    }
    // public function image()
    // {
    //     $this->input->is_ajax_request() or exit('No direct post submit allowed!');

    //     $data = $this->input->post(null, true);
    //     switch ($data['act']) {
    //         case 'upload':
    //             $id = $this->main->insert('product_image', array('product' => decode($data['product']), 'image' => $data['image'], 'primary' => $data['primary']));
    //             echo $id;
    //             break;
    //         case 'delete':
    //             $this->main->delete('product_image', array('id' => $data['id']));
    //             $this->main->delete('product_option_image', array('product_image' => $data['id']));
    //             break;
    //         default:
    //             break;
    //     }
    // }

    public function image() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);

        switch ($data['act']) {
            case 'upload':
                $config['upload_path']          = FCPATH.'../files/images/principal/'.$data['username'];
                $config['allowed_types']        = 'jpg|jpeg|png';
                // $config['file_name']            = 'banner_merchant_'.$this->input->post('id');
                $config['overwrite']            = true;
                $config['max_size']             = 1024; //1MB

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('product_image'))
                {
                    $error = $this->upload->display_errors();
                    echo $error;
                }
                else
                {   
                    
                    $id = $this->main->insert('product_image', array('product' => decode($data['product']),'image' => $data['image_name'], 'primary' => $data['primary']));
                    log_message('debug',$id);
                    echo $id;
                }

                break;
        
            case 'delete':
                $image = $this->main->get('product_image',array('id' => $data['id']));
                $path = FCPATH.'../files/images/'.$image->image;
                if($path && is_readable($path)) unlink($path);
                $this->main->delete('product_image', array('id' => $data['id']));
                $this->main->delete('product_option_image', array('product_image' => $data['id']));
                break;
            default:break;
        }
    }

    // public function image() {
    //     $this->input->is_ajax_request() or exit('No direct post submit allowed!');
    //     $data = $this->input->post(null, true);
    //     switch ($data['act']) {
    //         case 'upload':
    //             if(empty($data['product'])){
    //                 $id = $this->main->insert('product_image', array('image' => $data['image'], 'primary' => $data['primary']));

    //                 $image_id_session =  $this->session->userdata('image_id_session');
    //                 if (!is_array($image_id_session)) $image_id_session= array();
    //                 $image_id_session[] = $id;
    //                 $this->session->set_userdata('image_id_session', $image_id_session);
    //                 echo $id;
    //                 break;
    //             }
    //             else{
    //                 $id = $this->main->insert('product_image', array('product' => $data['product'], 'image' => $data['image'], 'primary' => $data['primary']));

    //                 echo $id;
    //                 break;
    //             }


    //         case 'delete':
    //             $this->main->delete('product_image', array('id' => $data['id']));
    //             $this->main->delete('product_option_image', array('product_image' => $data['id']));
    //             break;
    //         default:
    //             break;
    //     }
    // }

    // public function option($act)
    // {
    //     $this->input->is_ajax_request() or exit('No direct post submit allowed!');

    //     switch ($act) {
    //         case 'generate':
    //             $options = $this->input->post('option');
    //             $product = $this->input->post('id');
    //             $options = array_filter($options);
    //             if ($options) {
    //                 $opt = array();
    //                 $options = array_values($options);
    //                 $options = $this->_create_combination($options);
    //                 $ids = array();
    //                 foreach ($options as $option) {
    //                     if (!$this->_check_combination($product, $option)) {
    //                         $product_option = $this->main->insert('product_option', array('product' => $product));
    //                         array_push($ids, $product_option);
    //                         $product_option_combination = array();
    //                         foreach ($option as $opt) {
    //                             array_push($product_option_combination, array('product_option' => $product_option, 'option' => $opt));
    //                         }
    //                         $this->db->insert_batch('product_option_combination', $product_option_combination);
    //                     }
    //                 }
    //                 if ($ids) {
    //                     $options = $this->products->get_product_options($product, $ids);
    //                     echo json_encode(array('status' => 'success', 'data' => $options->result()));
    //                 }
    //             } else {
    //                 $return = array('message' => 'Variasi tidak ada.', 'status' => 'error');
    //             }
    //             break;
    //         case 'get_image':
    //             $product_option = $this->input->post('product_option');
    //             $product = $this->input->post('product');
    //             $images = $this->products->get_product_option_image($product, $product_option);
    //             echo json_encode($images->result());
    //             break;
    //         case 'save_image':
    //             $product_option = $this->input->post('product_option');
    //             $this->main->delete('product_option_image', array('product_option' => $product_option));
    //             $images = $this->input->post('images');
    //             $images = array_filter($images);
    //             foreach ($images as $image) {
    //                 $this->main->insert('product_option_image', array('product_option' => $product_option, 'product_image' => $image));
    //             }
    //             break;
    //         case 'delete':
    //             $product_option = $this->input->post('product_option');
    //             $this->main->delete('product_option', array('id' => $product_option));
    //             $this->main->delete('product_option_combination', array('product_option' => $product_option));
    //             $this->main->delete('product_option_image', array('product_option' => $product_option));
    //             break;
    //         default:
    //             break;
    //     }
    // }
    public function option($act) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        switch ($act) {
            case 'generate':
                if(empty($this->input->post('id'))){
                    $options = $this->input->post('option');
                    if ($options) {
                        $count_variants = 0;
                        $variant_name = array();

                        $previous_variation = $this->products->get_variation_previously($product);
                        foreach($options as $option => $option_value) {
                            $count_variants++;
                            array_push($variant_name,$option);       
                        }

                        if($previous_variation != $variant_name[0]){
                             if($count_variants == 1){
                                if($variant_name[0] == 'Warna'){
                                     $change_variation_check = $this->products->get_change_variation_check($product,'Ukuran');
                                     if($change_variation_check){
                                      
                                        $this->products->delete_variants($product);
                                        $this->main->delete('products_principal_stock', ['product_id' => $product]);
                                     }
                                }
                                else if($variant_name[0] == 'Ukuran'){
                                     $change_variation_check = $this->products->get_change_variation_check($product,'Warna');
                                     if($change_variation_check){
                                      
                                        $this->products->delete_variants($product);
                                        $this->main->delete('products_principal_stock', ['product_id' => $product]);
                                     }
                                }

                            }
                            else if($count_variants == 2){
                                $size_and_color_check = $this->products->duplicate_variation_check($product);   
                                if($size_and_color_check <= 0){
                                  
                                    $this->products->delete_variants($product);
                                    $this->main->delete('products_principal_stock', ['product_id' => $product]);
                                }

                            }
                        }   

                        $options = array_filter($options);
                    
                        $opt = array();
                        $options = array_values($options);
                        $options = $this->_create_combination($options);
                        $ids = array();
                        foreach ($options as $key => $option) {
                            if (!$this->_check_combination($product, $option)) {
                                $product_option = $this->main->insert('product_option', array('product' => $product));
                                array_push($ids, $product_option);
                                $product_option_combination = array();
                                foreach ($option as $opt) {
                                    array_push($product_option_combination, array('product_option' => $product_option, 'option' => $opt));
                                }
                                $this->db->insert_batch('product_option_combination', $product_option_combination);
                            }
                        }
                        // if ($ids) {
                            $options = $this->products->get_product_options($product, $ids);
                            echo json_encode(array('status' => 'success', 'data' => $options->result()));
                        // }
                    } else {
                        $return = array('message' => 'Variasi tidak ada.', 'status' => 'error');
                    }
                }
                else{
                    $options = $this->input->post('option');
                    $product = decode($this->input->post('id'));
                    if ($options) {
                        $count_variants = 0;
                        $variant_name = array();
                
                        $previous_variation = $this->products->get_variation_previously($product);
                        foreach($options as $option => $option_value) {
                            $count_variants++;
                            array_push($variant_name,$option);       
                        }

                        if($previous_variation) {
                            if($previous_variation != $variant_name[0]){
                                if($count_variants == 1){
                                    if($variant_name[0] == 'Warna'){
                                         $change_variation_check = $this->products->get_change_variation_check($product,'Ukuran');
                                         if($change_variation_check){
                                          
                                            $this->products->delete_variants($product);
                                            $this->main->delete('products_principal_stock', ['product_id' => $product]);
                                         }
                                    }
                                    else if($variant_name[0] == 'Ukuran'){
                                         $change_variation_check = $this->products->get_change_variation_check($product,'Warna');
                                         if($change_variation_check){
                                          
                                            $this->products->delete_variants($product);
                                            $this->main->delete('products_principal_stock', ['product_id' => $product]);
                                         }
                                    }
    
                                }
                                else if($count_variants == 2){
                                    $size_and_color_check = $this->products->duplicate_variation_check($product);   
                                    if($size_and_color_check <= 0){
                                        $this->products->delete_variants($product);
                                        $this->main->delete('products_principal_stock', ['product_id' => $product]);
                                    }
    
                                }
                            }   
                        }

                        $options = array_filter($options);
                   
                        $opt = array();
                        $options = array_values($options);
                        $options = $this->_create_combination($options);
                        $ids = array();
                        foreach ($options as $option) {
                            if (!$this->_check_combination($product, $option)) {
                                $product_branch = $this->main->gets('products_principal_stock', ['product_id' => $product]);
                                if($product_branch) {
                                    $this->main->delete('products_principal_stock', ['product_id' => $product]);
                                }
                                $product_option = $this->main->insert('product_option', array('product' => $product));
                                array_push($ids, $product_option);
                                $product_option_combination = array();
                                foreach ($option as $opt) {
                                    array_push($product_option_combination, array('product_option' => $product_option, 'option' => $opt));
                                }
                                $this->db->insert_batch('product_option_combination', $product_option_combination);
                            }
                        }
                        // if ($ids) {
                            $options = $this->products->get_product_options($product);
                            echo json_encode(array('status' => 'success', 'data' => $options->result()));
                        // }
                    } else {
                        echo json_encode(array('message' => 'Variasi tidak ada.', 'status' => 'error'));
                    }
                }
                break;
            case 'get_image':
                $product_option = $this->input->post('product_option');
                $product = $this->input->post('product');
                $images = $this->products->get_product_option_image($product, $product_option);
                echo json_encode($images->result());
                break;
            case 'save_image':
                $product_option = $this->input->post('product_option');
                $this->main->delete('product_option_image', array('product_option' => $product_option));
                $images = $this->input->post('images');
                $images = array_filter($images);
                foreach ($images as $image) {
                    $this->main->insert('product_option_image', array('product_option' => $product_option, 'product_image' => $image));
                }
                break;
            case 'delete':
                $product_option = $this->input->post('product_option');
                $this->main->delete('product_option', array('id' => $product_option));
                $this->main->delete('product_option_combination', array('product_option' => $product_option));
                $this->main->delete('product_option_image', array('product_option' => $product_option));
                $this->main->delete('products_principal_stock', ['id_option' => $product_option]);
                $this->main->delete('products_principal_history', ['id_option' => $product_option]);
                break;
            default:break;
        }
    }

    private function _create_combination($options) {
        if (count($options) <= 1) {
            return count($options) ? array_map(function($v){return (array($v));}, $options[0]) : $options;
        }
        $res = array();
        $first = array_pop($options);
        foreach ($first as $option) {
            $tab = $this->_create_combination($options);
            foreach ($tab as $to_add) {
                $res[] = is_array($to_add) ? array_merge($to_add, array($option)) : array($to_add, $option);
            }
        }
        return $res;
    }

    private function _check_combination($product, $options) {
        $exists = array();
        $options_exist = $this->db->select('poc.*')
                ->join('product_option po', 'po.id = poc.product_option', 'left')
                ->where('po.product', $product)
                ->get('product_option_combination poc');
        if ($options_exist->num_rows() > 0) {
            foreach ($options_exist->result() as $oe) {
                $exists[$oe->product_option][] = $oe->option;
            }
        } else {
            return false;
        }
        foreach ($exists as $key => $oe) {
            if (count($oe) == count($options)) {
                $diff = false;
                for ($i = 0; isset($oe[$i]); $i++) {
                    if (!in_array($oe[$i], $options) || $key == false) {
                        $diff = true;
                    }
                }
                if (!$diff) {
                    return true;
                }
            }
        }
        return false;
    }

    // private function _create_combination($options)
    // {
    //     if (count($options) <= 1) {
    //         // return count($options) ? array_map(create_function('$v', 'return (array($v));'), $options[0]) : $options;
    //         return count($options) ? array_map(function($v){return array($v);}, $options[0]) : $options;
    //     }
    //     $res = array();
    //     $first = array_pop($options);
    //     foreach ($first as $option) {
    //         $tab = $this->_create_combination($options);
    //         foreach ($tab as $to_add) {
    //             $res[] = is_array($to_add) ? array_merge($to_add, array($option)) : array($to_add, $option);
    //         }
    //     }
    //     return $res;
    // }

    // private function _check_combination($product, $options)
    // {
    //     $exists = array();
    //     $options_exist = $this->db->select('poc.*')
    //         ->join('product_option po', 'po.id = poc.product_option', 'left')
    //         ->where('po.product', $product)
    //         ->get('product_option_combination poc');
    //     if ($options_exist->num_rows() > 0) {
    //         foreach ($options_exist->result() as $oe) {
    //             $exists[$oe->product_option][] = $oe->option;
    //         }
    //     } else {
    //         return false;
    //     }
    //     foreach ($exists as $key => $oe) {
    //         if (count($oe) == count($options)) {
    //             $diff = false;
    //             for ($i = 0; isset($oe[$i]); $i++) {
    //                 if (!in_array($oe[$i], $options) || $key == false) {
    //                     $diff = true;
    //                 }
    //             }
    //             if (!$diff) {
    //                 return true;
    //             }
    //         }
    //     }
    //     return false;
    // }

    public function get_feature_values($feature, $id)
    {
        $feature = $this->main->get('features', array('id' => $feature));
        $products = $this->main->gets('feature_variant', array('feature' => $feature->id));
        if ($feature->type == 'i') {
            $output = '<input type="text" name="feature[' . $id . '][value]" class="form-control">';
        } else {
            if ($products) {
                if ($feature->type == 't') {
                    $output = '<textarea name="feature[' . $id . '][value]" class="form-control">';
                } elseif ($feature->type == 's') {
                    $output = '<select name="feature[' . $id . '][value]" class="form-control">';
                    foreach ($products->result() as $feature) {
                        $output .= '<option value="' . $feature->id . '">' . $feature->value . '</option>';
                    }
                    $output .= '</select>';
                } elseif ($feature->type == 'c') {
                    $output = '';
                    foreach ($products->result() as $feature) {
                        $output .= '<div class="checkbox"><label><input type="checkbox" name="feature[' . $id . '][value][]" value="' . $feature->id . '">' . $feature->value . '</label></div>';
                    }
                }
            } else {
                header('HTTP/1.1 500 Internal Server Error');
                //            header('Content-Type: application/json; charset=UTF-8');
                //            die(json_encode(array('message' => 'ERROR', 'code' => 1337)));
            }
        }
        echo $output;
    }
    public function get_package_item()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $items = $this->products->get_package_item($this->input->get('query'));
        echo json_encode($items);
    }
    public function get_feature_form()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        if ($category = $this->input->post('category')) {
            $data['product'] = ($this->input->post('product')) ? $this->input->post('product') : 0;
            $data['features'] = $this->products->get_features_by_category($category, decode($data['product']));
            $this->load->view('products/feature_form', $data);
        } else {
            echo "";
        }
    }

    public function save()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->load->library('form_validation');
        $id = decode($this->input->post('id'));
        $product = $this->main->get('products', ['id' => $id]);

        // $code_check = '';
        // if ($id != '') {
        //     $code = $this->main->get('products', array('id' => $this->input->post('id')))->code;
        //     if ($code != $this->input->post('code')) {
        //         $code_check = '|is_unique[products.code]';
        //     }
        // }
        // $this->form_validation->set_rules('code', 'lang:code', 'trim|required|alpha_dash' . $code_check);

        if($this->input->post('promo')){
            $promo = $this->input->post('promo');
            if($promo==1){
                $this->form_validation->set_rules('promo_data[discount]', 'lang:discount', 'trim|required|numeric|greater_than[0]');
                $this->form_validation->set_rules('promo_data[date_start]', 'lang:promo_range_start', 'trim|required');
                $this->form_validation->set_rules('promo_data[date_end]', 'lang:promo_range_end', 'trim|required');
            }

        }

        if($this->input->post('variation') == 0){
            $this->form_validation->set_rules('price', 'lang:price', 'trim|required|numeric|greater_than_equal_to[100]');
        } 

        $this->form_validation->set_rules('price', 'lang:price', 'trim|required|numeric');
        $this->form_validation->set_rules('weight', 'lang:weight', 'trim|required|numeric');
        $this->form_validation->set_rules('name', 'lang:name', 'trim|required');
        $this->form_validation->set_rules('short_description', 'lang:short_description', 'trim|required');
        $this->form_validation->set_rules('category', 'lang:category', 'trim|required');
        // $this->form_validation->set_rules('quantity', 'lang:stock', 'trim|required|numeric');
        // $this->form_validation->set_rules('seo_url', 'lang:seo_url', 'trim|seo_url');



        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);
            $data['id'] = decode($data['id']);
            if($data['variation'] == 0 && $data['price'] <= 100) {
                $return = [
                    'message' => 'Harga tidak boleh dibawah Rp. 100',
                    'status' => 'error'
                ];
                echo json_encode($return);
                exit();
            }
            if($data['weight'] != 0) {
                if($data['length'] != 0 && $data['height'] != 0 && $data['width'] != 0) {
                    if($data['variation'] == 0) {
                        $product_temp = $this->main->get('products', ['id' => $data['id'], 'variation' => 1]);
                        if($product_temp) {
                            $product_branch_temp = $this->main->gets('products_principal_stock', ['product_id' => $data['id']]);
                            if($product_branch_temp) {
                                if($product_branch_temp->num_rows() > 0) {
                                    $this->main->delete('products_principal_stock', ['product_id' => $data['id']]);
                                }
                            }
                        }
                    }
                    if($data['variation'] == 1) {
                        $product_temp_ = $this->main->get('products', ['id' => $data['id'], 'variation' => 0]);
                        if($product_temp_) {
                            $product_branch_temp_ = $this->main->gets('products_principal_stock', ['product_id' => $data['id']]);
                            if($product_branch_temp_) {
                                if($product_branch_temp_->num_rows() > 0) {
                                    $this->main->delete('products_principal_stock', ['product_id' => $data['id']]);
                                }
                            }
                        }
                    }
                    if(isset($data['options'])) {
                        $status_default = 0;
                        foreach($data['options'] as $key => $opt) {
                            if($key == $data['default_option']) {
                                if($opt['status'] == 1) {
                                    $status_default = 1;
                                }
                            }
                        }
                        if($status_default == 0) {
                            $return = [
                                'message' => 'Variasi default harus berstatus aktif!',
                                'status' => 'error'
                            ];
                            echo json_encode($return);
                            exit();
                        }
                    }
                    // if($data['price_reseller'] == 1) {
                    //     if(!isset($data['price_reseller_list'])) {
                    //         $return = [
                    //             'status' => 'error',
                    //             'message' => 'Jika harga reseller aktif, maka harga reseller harus ada minimal 1'
                    //         ];
                    //         echo json_encode($return);
                    //         exit();
                    //     }
                    // }
                    // if($data['price_grosir'] == 1) {
                    //     if(!isset($data['price_level'])) {
                    //         $return = [
                    //             'status' => 'error',
                    //             'message' => 'Jika harga grosir aktif, maka harga grosir harus ada minimal 1'
                    //         ];
                    //         echo json_encode($return);
                    //         exit();
                    //     }
                    // }
                    do {
                        $features = array();
                        $prices = array();
                        $level_prices = array();
                        $reseller_prices = array();
                        $options = array();
                        $package_items = array();
                        $promo_data = array();
                        $table_description = array();

                        if (isset($data['prices'])) {
                            $prices = $data['prices'];
                            unset($data['prices']);
                        }
                        if (isset($data['price_level'])) {
                            $level_prices = $data['price_level'];
                            unset($data['price_level']);
                        }
                        if (isset($data['price_reseller_list'])) {
                            $reseller_prices = $data['price_reseller_list'];
                            unset($data['price_reseller_list']);
                        }
                        // if (isset($data['feature'])) {
                        //     $features = array_filter($data['feature']);
                        //     unset($data['feature']);
                        // }
                        if($data['variation'] == 1){
                            if (!isset($data['options'])) {
                                $return = array('message' => lang('empty_variation'), 'status' => 'error');
                                break;
                            }
                        }
                        if (isset($data['options'])) {
                            $options = $data['options'];
                            unset($data['options']);
                        }
                        if (isset($data['package_items'])) {
                            $data['package_items'] = json_encode($data['package_items']);
                        }
                        if (isset($data['table_description'])) {
                            $data['table_description'] = json_encode($data['table_description']);
                        }
                        if (isset($data['promo_data'])) {
                            $data['promo_data']['date_start'] = date('Y-m-d', strtotime($data['promo_data']['date_start']));
                            $data['promo_data']['date_end'] = date('Y-m-d', strtotime($data['promo_data']['date_end']));
                            $data['promo_data'] = json_encode($data['promo_data']);
                        }
                        if (!$product_images = $this->main->gets('product_image', array('product' => $data['id']))) {
                            $return = array('message' => lang('empty_image_message'), 'status' => 'error');
                            break;
                        }
                        if (!isset($data['image_primary'])) {
                            $return = array('message' => lang('image_primary_not_set_message'), 'status' => 'error');
                            break;
                        }

                        if ($this->main->gets('product_option', array('product' => $data['id']))) {
                            if (!isset($data['default_option'])) {
                                $return = array('message' => lang('default_option_not_set_message'), 'status' => 'error');
                                break;
                            }
                            $this->main->update('product_option', array('default' => 0), array('product' => $data['id']));
                            $this->main->update('product_option', array('default' => 1), array('product' => $data['id'], 'id' => $data['default_option']));
                            unset($data['default_option']);
                        }

                        // $url = 'catalog/products/view/' . $data['id'];
                        // if ($seo_url = $data['seo_url']) {
                        //     if (check_url($seo_url, $url)) {
                        //         $return = array('message' => lang('friendly_url_exist_message'), 'status' => 'error');
                        //         break;
                        //     }
                        // } else {
                        //     $seo_url = $url;
                        // }
                        // unset($data['seo_url']);

                        $this->main->update('product_image', array('primary' => 0), array('product' => $data['id']));
                        $this->main->update('product_image', array('primary' => 1), array('product' => $data['id'], 'id' => $data['image_primary']));
                        unset($data['image_primary']);

                        // $this->main->delete('seo_url', array('query' => $url));
                        // $this->main->insert('seo_url', array('query' => $url, 'keyword' => $seo_url));
                        $data['meta_title'] = $data['name'];
                        $data['meta_description'] = $data['short_description'];


                        $data['status'] = (isset($data['status']) && $data['status'] == 1) ? $data['status'] : 0;
                        // $data['description'] = tinymce_parse_url_files($data['description']);
                        $data['description'] = str_replace("\n", "<br />",  $data['description']); 
                        $data['price'] = parse_number($data['price']);

                        $this->main->delete('product_feature', array('product' => $data['id']));
                        $this->main->delete('product_price', array('product' => $data['id']));
                        $this->db->where_in('product_price_level', '(SELECT id FROM product_price_level WHERE product = ' . $data['id'] . ')', false)
                            ->delete('product_price_level_groups');
                        $this->main->delete('product_price_level', array('product' => $data['id']));

                        $this->main->update('products', $data, array('id' => $data['id']));

                        $check_product_branch = $this->main->gets('products_principal_stock', ['product_id' => $data['id']]);
                        if($check_product_branch) {
                            $category = $this->main->get('categories', ['id' => $data['category']]);
                            $category_name = $this->generateSeoURL($category->name);
                            $product_name = $this->generateSeoURL($data['name']);
                            foreach($check_product_branch->result() as $product_branch) {
                                $branch = $this->main->get('merchants', ['id' => $product_branch->branch_id]);
                                $branch_auth = $this->main->get('merchant_users', ['id' => $branch->auth]);
                                $branch_username = $this->generateSeoURL($branch_auth->username);
                                $keyword = $branch_username . '/' . $category_name . '/' . $product_name;
                                $this->main->update('seo_url', ['keyword' => $keyword], ['query' => 'catalog/products/view/' . $product_branch->product_id . '/' . $product_branch->branch_id]);
                            }
                        }

                        if (count($options) > 0) {
                            foreach ($options as $id => $option) {
                                $this->main->update('product_option', array('price' => $option['price'], 'quantity' => $option['stock'],'status' => $option['status']), array('id' => $id));
                            }
                        }

                        // $data_products = array();
                        // if (count($features) > 0) {
                        //     foreach ($features as $feature_id => $feature) {
                        //         if ($feature['value']) {
                        //             if (is_array($feature['value'])) {
                        //                 if (count($feature['value']) > 0) {
                        //                     foreach ($feature['value'] as $variant) {
                        //                         $data_products[] = array(
                        //                             'product' => $data['id'],
                        //                             'feature' => $feature['id'],
                        //                             'value' => $variant
                        //                         );
                        //                     }
                        //                 }
                        //             } else {
                        //                 $data_products[] = array(
                        //                     'product' => $data['id'],
                        //                     'feature' => $feature_id,
                        //                     'value' => $feature['value']
                        //                 );
                        //             }
                        //         }
                        //     }
                        //     if ($data_products)
                        //         $this->db->insert_batch('product_feature', $data_products);
                        // }

                        // $data_prices = array();
                        // if (count($prices) > 0) {
                        //     foreach ($prices as $merchant_group => $price) {
                        //         if (isset($price['price'])) {
                        //             $data_prices[] = array(
                        //                 'product' => $data['id'],
                        //                 'merchant_group' => $merchant_group,
                        //                 'value' => 0,
                        //                 'percent' => 0,
                        //                 'percent_value' => 0,
                        //                 'price' => $price['price']
                        //             );
                        //         }
                        //     }
                        //     if ($data_prices)
                        //         $this->db->insert_batch('product_price', $data_prices);
                        // }
                        // if (count($level_prices) > 0) {
                        //     foreach ($level_prices as $price) {
                        //         if (isset($price['qty']) && isset($price['price'])) {
                        //             $price_level_parent = $price['price'];
                        //             $price_level = $this->main->insert('product_price_level', array('product' => $data['id'], 'min_qty' => $price['qty'], 'price' => $price['price']));
                        //             $price_level_groups = array_filter($price['group']);
                        //             $data_price_level_groups = array();
                        //             if ($price_level_groups) {
                        //                 foreach ($price_level_groups as $group => $price) {
                        //                     if (isset($price['price'])) {
                        //                         $data_price_level_groups[] = array(
                        //                             'product_price_level' => $price_level,
                        //                             'merchant_group' => $group,
                        //                             'value' => 0,
                        //                             'percent' => 0,
                        //                             'percent_value' => 0,
                        //                             'price' => $price['price']
                        //                         );
                        //                     }
                        //                 }
                        //             }
                        //             if ($data_price_level_groups)
                        //                 $this->db->insert_batch('product_price_level_groups', $data_price_level_groups);
                        //         }
                        //     }
                        // }

                        if (count($level_prices) > 0) {
                            $this->main->delete('product_price_level', ['product' => $data['id']]);
                            foreach ($level_prices as $price) {
                                if (isset($price['qty']) && isset($price['price'])) {
                                    $price_level = $this->main->insert('product_price_level', array('product' => $data['id'], 'min_qty' => $price['qty'], 'price' => $price['price']));
                                }
                            }
                        }
                        if (count($reseller_prices) > 0) {
                            $this->main->delete('product_price_reseller', ['product' => $data['id']]);
                            foreach ($reseller_prices as $reseller) {
                                if (isset($reseller['qty']) && isset($reseller['price'])) {
                                    $price_reseller = $this->main->insert('product_price_reseller', array('product' => $data['id'], 'min_qty' => $reseller['qty'], 'price' => $reseller['price']));
                                }
                            }
                        }

                        $return = array('message' => sprintf(lang('save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('catalog/products'));
                    } while (0);
                } else {
                    $return = [
                        'message' => 'Dimensi harus diisi!',
                        'status' => 'error'
                    ];
                }
            } else {
                $return = [
                    'message' => 'Berat produk harus diisi!',
                    'status' => 'error'
                ];
            }
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }

        echo json_encode($return);
    }

    private function generateSeoURL($string, $wordLimit = 0){
        $separator = '-';
        
        if($wordLimit != 0){
            $wordArr = explode(' ', $string);
            $string = implode(' ', array_slice($wordArr, 0, $wordLimit));
        }
    
        $quoteSeparator = preg_quote($separator, '#');
    
        $trans = array(
            '&.+?;'                    => '',
            '[^\w\d _-]'            => '',
            '\s+'                    => $separator,
            '('.$quoteSeparator.')+'=> $separator
        );
    
        $string = strip_tags($string);
        foreach ($trans as $key => $val){
            $string = preg_replace('#'.$key.'#i'.(UTF8_ENABLED ? 'u' : ''), $val, $string);
        }
    
        $string = strtolower($string);
    
        return trim(trim($string, $separator));
    }

    // public function save() {
    //     $this->input->is_ajax_request() or exit('No direct post submit allowed!');

    //     $this->load->library('form_validation');

    //     $this->form_validation->set_rules('code', 'lang:code', 'trim|required|alpha_dash|is_unique[products.code]');
    //     $this->form_validation->set_rules('price', 'lang:price', 'trim|required|numeric');
    //     $this->form_validation->set_rules('weight', 'lang:weight', 'trim|required|numeric');
    //     $this->form_validation->set_rules('name', 'lang:name', 'trim|required');
    //     $this->form_validation->set_rules('short_description', 'lang:short_description', 'trim|required');
    //     $this->form_validation->set_rules('category', 'lang:category', 'trim|required');
    //     $this->form_validation->set_rules('seo_url', 'lang:seo_url', 'trim|seo_url');

    //     if ($this->form_validation->run() === true) {
    //         $data = $this->input->post(null, true);
    //         do {
    //             $product = $this->main->insert('products', array('is_temp' => 1, 'user_added' => $this->data['user']->id, 'store_type'=>'principal','store_id' =>$this->data['user']->principle_id));

    //             $features = array();
    //             $prices = array();
    //             $level_prices = array();
    //             $options = array();
    //             $package_items = array();
    //             $table_description = array();
    //             $promo_data = array();

    //             if (isset($data['prices'])) {
    //                 $prices = $data['prices'];
    //                 unset($data['prices']);
    //             }
    //             if (isset($data['price_level'])) {
    //                 $level_prices = $data['price_level'];
    //                 unset($data['price_level']);
    //             }
    //             if (isset($data['feature'])) {
    //                 $features = array_filter($data['feature']);
    //                 unset($data['feature']);
    //             }
    //             if (isset($data['options'])) {
    //                 $options = $data['options'];
    //                 unset($data['options']);
    //             }
    //             if (isset($data['package_items'])) {
    //                 $data['package_items'] = json_encode($data['package_items']);
    //             }
    //             if (isset($data['table_description'])) {
    //                 $data['table_description'] = json_encode($data['table_description']);
    //             }
    //             if (isset($data['promo_data'])) {
    //                 $data['promo_data']['date_start'] = date('Y-m-d',strtotime($data['promo_data']['date_start']));
    //                 $data['promo_data']['date_end'] = date('Y-m-d',strtotime($data['promo_data']['date_end']));
    //                 $data['promo_data'] = json_encode($data['promo_data']);
    //             }
    //             if(isset($data['coming_soon_time'])){
    //                 $data['coming_soon_time'] = date('Y-m-d', strtotime($data['coming_soon_time']));
    //             }

    //             $image_id_sessions =  $this->session->userdata('image_id_session');

    //             if($image_id_sessions){ 
    //                 foreach ($image_id_sessions as $image_id_session) {
    //                     $this->main->update('product_image', array('product' => $product), array('id' => $image_id_session));
    //                 }
    //             }else{
    //                 $return = array('message' => lang('empty_image_message'), 'status' => 'error');
    //                 break; 
    //             }

    //             if (!isset($data['image_primary'])) {
    //                 $return = array('message' => lang('image_primary_not_set_message'), 'status' => 'error');
    //                 break;
    //             }

    //             if ($this->main->gets('product_option', array('product' => $product))) {
    //                 if (!isset($data['default_option'])) {
    //                     $return = array('message' => lang('default_option_not_set_message'), 'status' => 'error');
    //                     break;
    //                 }
    //                 $this->main->update('product_option', array('default' => 0), array('product' => $product));
    //                 $this->main->update('product_option', array('default' => 1), array('product' => $product, 'id' => $data['default_option']));
    //                 unset($data['default_option']);
    //             }

    //             $data['meta_title'] = $data['name'];
    //             $data['meta_description'] = $data['short_description'];

    //             unset($data['image_primary']);

    //             $data['status'] = (isset($data['status']) && $data['status'] == 1) ? $data['status'] : 0;
    //             $data['description'] = tinymce_parse_url_files($data['description']);
    //             $data['price'] = parse_number($data['price']);

    //             $this->main->update('products', $data, array('id' => $product));

    //             if (count($options) > 0) {
    //                 foreach ($options as $id => $option) {
    //                     $this->main->update('product_option', array('price' => $option['price'], 'weight' => $option['weight']), array('id' => $id));
    //                 }
    //             }

    //             $data_products = array();
    //             if (count($features) > 0) {
    //                 foreach ($features as $feature_id => $feature) {
    //                     if ($feature['value']) {
    //                         if (is_array($feature['value'])) {
    //                             if (count($feature['value']) > 0) {
    //                                 foreach ($feature['value'] as $variant) {
    //                                     $data_products[] = array(
    //                                         'product' => $product,
    //                                         'feature' => $feature['id'],
    //                                         'value' => $variant
    //                                     );
    //                                 }
    //                             }
    //                         } else {
    //                             $data_products[] = array(
    //                                 'product' => $product,
    //                                 'feature' => $feature_id,
    //                                 'value' => $feature['value']
    //                             );
    //                         }
    //                     }
    //                 }
    //                 if ($data_products)
    //                     $this->db->insert_batch('product_feature', $data_products);
    //             }
    //             if (count($level_prices) > 0) {
    //                 foreach ($level_prices as $price) {
    //                     if (isset($price['qty']) && isset($price['price'])) {
    //                         $price_level_parent = $price['price'];
    //                         $price_level = $this->main->insert('product_price_level', array('product' => $product, 'min_qty' => $price['qty'], 'price' => $price['price']));
    //                     }
    //                 }
    //             }
    //             $return = array('message' => sprintf(lang('save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('catalog/products'));
    //         } while (0);
    //     } else {
    //         $return = array('message' => validation_errors(), 'status' => 'error');
    //     }

    //     echo json_encode($return);
    // }

    // public function save_edit() {
    //     $this->input->is_ajax_request() or exit('No direct post submit allowed!');

    //     $this->load->library('form_validation');

    //     $code_check = '';
    //     if ($this->input->post('id')) {
    //         $code = $this->main->get('products', array('id' => $this->input->post('id')))->code;
    //         if ($code != $this->input->post('code')) {
    //             $code_check = '|is_unique[products.code]';
    //         }
    //     }
    //     $this->form_validation->set_rules('code', 'lang:code', 'trim|required|alpha_dash' . $code_check);
    //     $this->form_validation->set_rules('price', 'lang:price', 'trim|required|numeric');
    //     $this->form_validation->set_rules('weight', 'lang:weight', 'trim|required|numeric');
    //     $this->form_validation->set_rules('name', 'lang:name', 'trim|required');
    //     $this->form_validation->set_rules('short_description', 'lang:short_description', 'trim|required');
    //     $this->form_validation->set_rules('category', 'lang:category', 'trim|required');
    //     $this->form_validation->set_rules('seo_url', 'lang:seo_url', 'trim|seo_url');

    //     if ($this->form_validation->run() === true) {
    //         $data = $this->input->post(null, true);
    //         do {
    //             $features = array();
    //             $prices = array();
    //             $level_prices = array();
    //             $options = array();
    //             $package_items = array();
    //             $table_description = array();
    //             $promo_data = array();

    //             if (isset($data['prices'])) {
    //                 $prices = $data['prices'];
    //                 unset($data['prices']);
    //             }
    //             if (isset($data['price_level'])) {
    //                 $level_prices = $data['price_level'];
    //                 unset($data['price_level']);
    //             }
    //             if (isset($data['feature'])) {
    //                 $features = array_filter($data['feature']);
    //                 unset($data['feature']);
    //             }
    //             if (isset($data['options'])) {
    //                 $options = $data['options'];
    //                 unset($data['options']);
    //             }
    //             if (isset($data['package_items'])) {
    //                 $data['package_items'] = json_encode($data['package_items']);
    //             }
    //             if (isset($data['table_description'])) {
    //                 $data['table_description'] = json_encode($data['table_description']);
    //             }
    //             if (isset($data['promo_data'])) {
    //                 $data['promo_data']['date_start'] = date('Y-m-d',strtotime($data['promo_data']['date_start']));
    //                 $data['promo_data']['date_end'] = date('Y-m-d',strtotime($data['promo_data']['date_end']));
    //                 $data['promo_data'] = json_encode($data['promo_data']);
    //             }
    //             if(isset($data['coming_soon_time'])){
    //                 $data['coming_soon_time'] = date('Y-m-d', strtotime($data['coming_soon_time']));
    //             }
    //             if (!$product_images = $this->main->gets('product_image', array('product' => $data['id']))) {
    //                 $return = array('message' => lang('empty_image_message'), 'status' => 'error');
    //                 break;
    //             }
    //             if (!isset($data['image_primary'])) {
    //                 $return = array('message' => lang('image_primary_not_set_message'), 'status' => 'error');
    //                 break;
    //             }

    //             if ($this->main->gets('product_option', array('product' => $data['id']))) {
    //                 if (!isset($data['default_option'])) {
    //                     $return = array('message' => lang('default_option_not_set_message'), 'status' => 'error');
    //                     break;
    //                 }
    //                 $this->main->update('product_option', array('default' => 0), array('product' => $data['id']));
    //                 $this->main->update('product_option', array('default' => 1), array('product' => $data['id'], 'id' => $data['default_option']));
    //                 unset($data['default_option']);
    //             }

    //             $this->main->update('product_image', array('primary' => 0), array('product' => $data['id']));
    //             $this->main->update('product_image', array('primary' => 1), array('product' => $data['id'], 'id' => $data['image_primary']));
    //             unset($data['image_primary']);

    //             $data['status'] = (isset($data['status']) && $data['status'] == 1) ? $data['status'] : 0;
    //             $data['description'] = tinymce_parse_url_files($data['description']);
    //             $data['price'] = parse_number($data['price']);

    //             $this->main->delete('product_feature', array('product' => $data['id']));
    //             $this->main->delete('product_price', array('product' => $data['id']));
    //             $this->db->where_in('product_price_level', '(SELECT id FROM product_price_level WHERE product = ' . $data['id'] . ')', false)
    //                     ->delete('product_price_level_groups');
    //             $this->main->delete('product_price_level', array('product' => $data['id']));

    //             $this->main->update('products', $data, array('id' => $data['id']));

    //             if (count($options) > 0) {
    //                 foreach ($options as $id => $option) {
    //                     $this->main->update('product_option', array('price' => $option['price'], 'weight' => $option['weight']), array('id' => $id));
    //                 }
    //             }

    //             $data_products = array();
    //             if (count($features) > 0) {
    //                 foreach ($features as $feature_id => $feature) {
    //                     if ($feature['value']) {
    //                         if (is_array($feature['value'])) {
    //                             if (count($feature['value']) > 0) {
    //                                 foreach ($feature['value'] as $variant) {
    //                                     $data_products[] = array(
    //                                         'product' => $data['id'],
    //                                         'feature' => $feature['id'],
    //                                         'value' => $variant
    //                                     );
    //                                 }
    //                             }
    //                         } else {
    //                             $data_products[] = array(
    //                                 'product' => $data['id'],
    //                                 'feature' => $feature_id,
    //                                 'value' => $feature['value']
    //                             );
    //                         }
    //                     }
    //                 }
    //                 if ($data_products)
    //                     $this->db->insert_batch('product_feature', $data_products);
    //             }

    //             if (count($level_prices) > 0) {
    //                 foreach ($level_prices as $price) {
    //                     if (isset($price['qty']) && isset($price['price'])) {
    //                         $price_level_parent = $price['price'];
    //                         $price_level = $this->main->insert('product_price_level', array('product' => $data['id'], 'min_qty' => $price['qty'], 'price' => $price['price']));
    //                     }
    //                 }
    //             }

    //             $return = array('message' => sprintf(lang('save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('catalog/products'));
    //         } while (0);
    //     } else {
    //         $return = array('message' => validation_errors(), 'status' => 'error');
    //     }

    //     echo json_encode($return);
    // }

    public function copy_save()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $check_zona = $this->main->gets('product_price', array('merchant_group' => $data['from_zona']));
        $this->db->select('id');
        $this->db->where('merchant_group', $data['to_zona']);
        $check_zona_to = $this->db->get('product_price');
        $check_price_level = $this->main->gets('product_price_level_groups', array('merchant_group' => $data['from_zona']));
        $this->db->select('id');
        $this->db->where('merchant_group', $data['to_zona']);
        $check_price_level_to = $this->db->get('product_price_level_groups');
        if ($check_zona->num_rows() > 0) {
            foreach ($check_zona->result_array() as $price) {
                if (isset($price['value']) || isset($price['percent'])) {
                    $data_prices[] = array(
                        'product' => $price['product'],
                        'merchant_group' => $data['to_zona'],
                        'value' => $price['value'],
                        'percent' => $price['percent'],
                        'percent_value' => $price['percent_value'],
                        'price' => $price['price']
                    );
                }
            }
            if ($check_zona_to->num_rows() > 0) {
                foreach ($check_zona_to->result_array() as $key => $value) {
                    $data_prices[$key]['id'] = $value['id'];
                }
                //var_dump($data_prices);exit();
                $this->db->update_batch('product_price', $data_prices, 'id');
            } else {

                //var_dump('new');exit();
                $this->db->insert_batch('product_price', $data_prices);
            }
        }
        if ($check_price_level->num_rows() > 0) {
            foreach ($check_price_level->result_array() as $price) {
                if (isset($price['value']) || isset($price['percent'])) {
                    $data_price_level_groups[] = array(
                        'product_price_level' => $price['product_price_level'],
                        'merchant_group' => $data['to_zona'],
                        'value' => $price['value'],
                        'percent' => $price['percent'],
                        'percent_value' => $price['percent_value'],
                        'price' => $price['price']
                    );
                }
            }
            if ($check_price_level_to->num_rows() > 0) {
                foreach ($check_price_level_to->result_array() as $key => $value) {
                    $data_price_level_groups[$key]['id'] = $value['id'];
                }
                $this->db->update_batch('product_price_level_groups', $data_price_level_groups, 'id');
            } else {
                $this->db->insert_batch('product_price_level_groups', $data_price_level_groups);
            }
        }
        $return = array('message' => 'Copy Zona Produk Berhasil', 'status' => 'success');

        echo json_encode($return);
    }

    public function view($id)
    {
        $this->load->model('catalog/categories_model', 'categories');

        $this->template->_init();
        $this->load->js('../assets/backend/js/modules/catalog/product_view.js');

        $id = decode($id);
        $this->data['product'] = $this->products->get($id);
        $this->data['product_images'] = $this->main->gets('product_image', array('product' => $id));
        $this->data['product_features'] = $this->products->get_product_feature_by_category($this->data['product']->category, $id);
        $this->data['brand'] = $this->main->get('brands', array('id' => $this->data['product']->brand));
        $this->data['category'] = $this->categories->get($this->data['product']->category);
        $this->data['merchant'] = $this->main->get('merchants', array('id' => $this->data['product']->merchant));

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('product'), '/catalog/products');
        $this->breadcrumbs->push($this->data['product']->name, '/catalog/products/view/' . encode($id));

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title('Produk ' . $this->data['product']->name);
        $this->load->view('products/product_view', $this->data);
    }

    public function other()
    {
        $this->data['menu'] = 'catalog_product_other';

        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('product'), '/catalog/products');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('heading'));
        $this->load->view('products/other/list', $this->data);
    }

    public function get_list_other()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->products->get_all(-1, $start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    '<a href="' . site_url('catalog/products/view/' . encode($data->id)) . '">' . $data->code . '</a>',
                    $data->name,
                    number($data->price_old),
                    number($data->price) . '<small class="display-block text-muted">Diskon ' . number($data->discount) . '%</small>',
                    number($data->quantity),
                    $data->category,
                    $this->_status_product($data->status),
                    $data->merchant_name,
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">' .
                        (($data->status == 2) ? '<li><a href="' . site_url('catalog/products/activate/' . encode($data->id)) . '" class="activate">' . lang('button_activate') . '</a></li>' : '') .
                        ($this->aauth->is_allowed('catalog/product/edit') ? '<li><a href="' . site_url('catalog/products/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>' : '') .
                        ($this->aauth->is_allowed('catalog/product/delete') ? '<li><a href="' . site_url('catalog/products/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>' : '') .
                        '</ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->products->count_all();
        $output['recordsFiltered'] = $this->products->count_all($search);
        echo json_encode($output);
    }

    public function delete($id)
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $data = $this->main->get('products', array('id' => $id));
        $delete = $this->main->delete('products', array('id' => $id));
        $product_branch = $this->main->gets('products_principal_stock', ['product_id' => $id]);
        if ($delete) {
            if($product_branch) {
                foreach($product_branch->result() as $product) {
                    $this->main->delete('seo_url', ['query' => 'catalog/products/view/' . $product->product_id . '/' . $product->branch_id]);
                }
            }
            $this->main->delete('products_principal_stock', array('product_id' => $id));
            $this->main->delete('products_principal_history', ['product_id' => $id]);
            $this->main->delete('product_image', array('product' => $id));
            $this->main->delete('product_merchant', array('product' => $id));
            // $this->db->where_in('product_option', 'SELECT id FROM product_option WHERE product = ' . $id, false)->delete('product_option_variant');
            $this->main->delete('product_option', array('product' => $id));
            $this->main->delete('product_price', array('product' => $id));
            $this->main->delete('product_price_level', array('product' => $id));
            $this->db->where_in('product_review', 'SELECT id FROM product_review WHERE product = ' . $id, false)->delete('product_review_likedislike');
            $this->main->delete('product_review', array('product' => $id));
            $return = array('message' => sprintf(lang('delete_success_message'), $data->name), 'status' => 'success');
        } else {
            $return = array('message' => sprintf(lang('delete_error_message'), $data->name), 'status' => 'danger');
        }
        echo json_encode($return);
    }

    public function activate($id)
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $data = $this->main->get('products', array('id' => $id));
        $update = $this->main->update('products', array('status' => 1), array('id' => $id));
        if ($update) {
            $return = array('message' => 'Produk ' . $data->name . ' berhasil di aktifasi.', 'status' => 'success');
        } else {
            $return = array('message' => 'Produk ' . $data->name . ' gagal di aktifasi.', 'status' => 'danger');
        }
        echo json_encode($return);
    }

    public function disabled($id)
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $data = $this->main->get('products', array('id' => $id));
        $update = $this->main->update('products', array('status' => 2), array('id' => $id));
        if ($update) {
            $return = array('message' => 'Produk ' . $data->name . ' berhasil di non-aktifkan.', 'status' => 'success');
        } else {
            $return = array('message' => 'Produk ' . $data->name . ' gagal di non-aktifkan.', 'status' => 'danger');
        }
        echo json_encode($return);
    }

    public function get_product_code()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $merchant = $this->main->get('merchants', array('id' => $this->input->post('merchant')));
        echo product_code($merchant->name, $merchant->id);
    }

    private function _status_product($status)
    {
        switch ($status) {
            case 0:
                $text = '<i class="icon-cross2 text-danger-400"></i>';
                break;
            case 1:
                $text = '<i class="icon-checkmark3 text-success"></i>';
                break;
            case 2:
                $text = '<i class="icon-cross2 text-danger-400"></i>';
        }
        return $text;
    }

    private function status_product($status) {

        switch ($status) {
            case 0:
                $text = '<i style="color:#D9534F;text-decoration:underline;">Aktifkan produk ini</i>';
                break;
            case 1:
                $text = '<i style="color:#5CB85C;text-decoration:underline;">Aktif</i>';
        }
        return $text;
    }

    public function get_option($id)
    {
        $data = $this->main->get('options', array('id' => $id));
        $variants = $this->main->gets('option_variant', array('option' => $data->id))->result();
        echo json_encode(array('option' => $data, 'variants' => $variants));
    }

    // public function export()
    // {
    //     $this->aauth->control('catalog/product/edit');
    //     $spreadsheet = new Spreadsheet();
    //     $spreadsheet->setActiveSheetIndex(0);
    //     $sheet = $spreadsheet->getActiveSheet();
    //     $sheet->fromArray(array('ID', 'Nama', 'Desk. Singkat', 'Kode', 'Dimensi (PxLxT)', 'Berat (gram)', 'Harga Normal', 'Harga Zona', 'Harga Level'), NULL, 'A1');
    //     $data = $this->input->get(null, true);
    //     $order = explode(',', $data['order']);
    //     $products = $this->products->export($data['start'], $data['length'], $data['search'], array('column' => $order[0], 'dir' => $order[1]));
    //     $i = 2;
    //     if ($products->num_rows() > 0) {
    //         foreach ($products->result() as $product) {
    //             $row_data = array(
    //                 $product->id,
    //                 $product->name,
    //                 $product->short_description,
    //                 $product->code,
    //                 $product->length . 'x' . $product->width . 'x' . $product->height,
    //                 $product->weight,
    //                 $product->price,
    //             );
    //             $row_price_zone = '';
    //             $price_zone = $this->db->select('mg.id, mg.name, pp.value, pp.percent')->join('product_price pp', 'pp.merchant_group = mg.id AND pp.product = ' . $product->id, 'left')->get('merchant_groups mg');
    //             if ($price_zone->num_rows() > 0) {
    //                 foreach ($price_zone->result() as $pz) {
    //                     $row_price_zone .= $pz->id . '-' . $pz->name . '=F:' . $pz->value . ',P:' . $pz->percent . ';';
    //                 }
    //             }
    //             $row_data[] = $row_price_zone;
    //             $row_price_level = '';
    //             $price_level = $this->main->gets('product_price_level', array('product' => $product->id), 'min_qty asc');
    //             if ($price_level) {
    //                 foreach ($price_level->result() as $pl) {
    //                     $row_price_level .= $pl->min_qty . '=' . $pl->price;
    //                     $groups = $this->db->select('mg.id, mg.name, pplg.product_price_level, IFNULL(pplg.value,0) value, IFNULL(pplg.percent,0) percent')->join('product_price_level_groups pplg', 'pplg.merchant_group = mg.id AND pplg.product_price_level = ' . $pl->id, 'left')->order_by('mg.name')->get('merchant_groups mg');
    //                     if ($groups->num_rows() > 0) {
    //                         $row_price_level .= '=>';
    //                         foreach ($groups->result() as $group) {
    //                             $row_price_level .= $group->id . '-' . $group->name . '=F:' . number_format($group->value, 0, '', '') . ',P:' . (($this->is_decimal($group->percent)) ? number_format($group->percent, 1, '.', '') : 0) . ';';
    //                         }
    //                     }
    //                     $row_price_level .= ';';
    //                 }
    //             }
    //             $row_data[] = $row_price_level;
    //             $sheet->fromArray($row_data, null, 'A' . $i);
    //             $i++;
    //         }
    //     }
    //     header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    //     header('Content-Disposition: attachment;filename="Produk Tanaka.xlsx"');
    //     header('Cache-Control: max-age=0');
    //     header('Cache-Control: max-age=1');
    //     header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    //     header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    //     header('Cache-Control: cache, must-revalidate');
    //     header('Pragma: public');
    //     $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
    //     $writer->save('php://output');
    //     exit;
    // }

    public function export() {
        $this->aauth->control('catalog/product/edit');
        $filename = "product_stock";
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        \PhpOffice\PhpSpreadsheet\Shared\File::setUseUploadTempDirectory(true);
        $sheet->fromArray(array('Kode Produk', 'Nama','Tipe','Deskripsi Pendek','Deskripsi','Brand', 'Kategori','Etalase','Berat','Panjang','Lebar','Tinggi','Harga Jual','Tanggal', 'Stok'), NULL, 'A1');
        $data = $this->input->get(null, true);
        $order = explode(',', $data['order']);
        $fullFileName = "${filename}_".date('Y-m-d');
        // $products = $this->products->export($data['start'], $data['length'], $data['search'], array('column' => $order[0], 'dir' => $order[1]));
        $products = $this->products->export_data($this->data['user']->principle_id);
        $i = 2;
        if ($products) {
            foreach ($products->result() as $data) {
                $row_data = array(
                    $data->code,
                    $data->name,
                    $data->type,
                    $data->short_description,
                    $data->description,
                    $data->brand_name,
                    $data->category_name,
                    $data->etalase_name,
                    $data->weight,
                    $data->length,
                    $data->width,
                    $data->height,
                    number_format($data->price,2,',','.'),
                    $data->date_added,
                    $data->quantity
                );
                $sheet->fromArray($row_data, null, 'A' . $i);
                $i++;
            }
        };
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $fullFileName . '.xlsx"');
        header('Cache-Control: max-age=0');
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        ob_end_clean();
        $writer->save('php://output');
    }

    // public function import()
    // {
    //     $this->aauth->control('catalog/product/edit');
    //     $res = array('status' => 'error', 'message' => 'File tidak ada');
    //     if (isset($_FILES['file']['name'])) {
    //         $this->load->library('upload');
    //         $config['upload_path'] = '../files/import/';
    //         $config['allowed_types'] = 'xlsx';
    //         $config['overwrite'] = TRUE;

    //         $this->upload->initialize($config);
    //         if ($this->upload->do_upload('file')) {
    //             $file = $this->upload->data();
    //             $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load('../files/import/' . $file['file_name']);
    //             $sheet = $spreadsheet->getSheet(0);
    //             $datas = array();
    //             for ($row = 2; $row <= $sheet->getHighestRow(); $row++) {
    //                 $data = $sheet->rangeToArray('A' . $row . ':' . $sheet->getHighestColumn() . $row, NULL, TRUE, FALSE);
    //                 //                    array_push($datas, $data);
    //                 $dimention = array_filter(explode('x', $data[0][4]));
    //                 $id = $data[0][0];
    //                 $product = array(
    //                     'name' => $data[0][1],
    //                     'short_description' => $data[0][2],
    //                     'code' => $data[0][3],
    //                     'length' => isset($dimention[0]) ? $dimention[0] : 0,
    //                     'width' => isset($dimention[1]) ? $dimention[1] : 0,
    //                     'height' => isset($dimention[2]) ? $dimention[2] : 0,
    //                     'weight' => $data[0][5],
    //                     'price' => $data[0][6]
    //                 );
    //                 $this->main->update('products', $product, array('id' => $id));
    //                 $price_zone = $data[0][7];
    //                 $price_zone = ($price_zone) ? array_filter(explode(';', $price_zone)) : array();
    //                 $price_level = $data[0][8];
    //                 $price_levels = ($price_level) ? array_filter(explode(';;', $price_level)) : array();
    //                 $this->db->delete('product_price', array('product' => $id));
    //                 $this->db->where_in('product_price_level', '(SELECT id FROM product_price_level WHERE product = ' . $id . ')', false)
    //                     ->delete('product_price_level_groups');
    //                 $this->db->delete('product_price_level', array('product' => $id));

    //                 $data_price_zone = array();
    //                 if (count($price_zone) > 0) {
    //                     foreach ($price_zone as $price) {
    //                         $price = array_filter(explode('=', $price));
    //                         if ($price) {
    //                             $group = array_filter(explode('-', $price[0]));
    //                             $price = array_filter(explode(',', isset($price[1]) ? $price[1] : ''));
    //                             if ($group && isset($price)) {
    //                                 $data_price = array(
    //                                     'product' => $id,
    //                                     'merchant_group' => $group[0],
    //                                     'value' => 0,
    //                                     'percent' => 0,
    //                                     'percent_value' => 0,
    //                                 );
    //                                 foreach ($price as $p) {
    //                                     $p = explode(':', $p);
    //                                     if ($p[0] == 'F') {
    //                                         $data_price['value'] = ($p[1]) ? $p[1] : 0;
    //                                     }
    //                                     if ($p[0] == 'P') {
    //                                         $data_price['percent'] = ($p[1]) ? $p[1] : 0;
    //                                         $data_price['percent_value'] = ($p[1]) ? $product['price'] * $p[1] / 100 : 0;
    //                                     }
    //                                 }
    //                                 $data_price['price'] = $product['price'] + $data_price['value'] + $data_price['percent_value'];
    //                                 $data_price_zone[] = $data_price;
    //                             }
    //                         }
    //                     }
    //                     if ($data_price_zone)
    //                         $this->db->insert_batch('product_price', $data_price_zone);
    //                 }

    //                 $data_price_level = array();
    //                 if (count($price_levels) > 0) {
    //                     foreach ($price_levels as $price) {
    //                         $price = explode('=>', $price);
    //                         $qty = explode('=', $price[0]);
    //                         $price_parent = $qty[1];
    //                         $qty = $qty[0];
    //                         $price_level = $this->main->insert('product_price_level', array('product' => $id, 'min_qty' => $qty, 'price' => $price_parent));
    //                         $group_price = array_filter(explode(';', $price[1]));
    //                         //                            print_r($group_price);
    //                         $data_price = array();
    //                         if ($group_price) {
    //                             foreach ($group_price as $price) {
    //                                 //                                    print_r($price);
    //                                 //                                    echo '<br>';
    //                                 $price = explode('=', $price);
    //                                 $merchant_group = explode('-', $price[0]);
    //                                 $price = explode(',', $price[1]);
    //                                 $data_price = array(
    //                                     'product_price_level' => $price_level,
    //                                     'merchant_group' => $merchant_group[0],
    //                                     'value' => 0,
    //                                     'percent' => 0,
    //                                     'percent_value' => 0,
    //                                     'price' => $price_parent
    //                                 );
    //                                 foreach ($price as $p) {
    //                                     $p = explode(':', $p);
    //                                     if ($p[0] == 'F') {
    //                                         $data_price['value'] = ($p[1]) ? $p[1] : 0;
    //                                     }
    //                                     if ($p[0] == 'P') {
    //                                         $data_price['percent'] = ($p[1]) ? $p[1] : 0;
    //                                         $data_price['percent_value'] = ($p[1]) ? $price_parent * $p[1] / 100 : 0;
    //                                     }
    //                                 }
    //                                 $data_price['price'] = $price_parent + $data_price['value'] + $data_price['percent_value'];
    //                                 $this->main->insert('product_price_level_groups', $data_price);
    //                             }
    //                         }
    //                     }
    //                 }
    //             }
    //             $res = array('status' => 'success', 'message' => 'Import berhasil disimpan');
    //         } else {
    //             $res['message'] = $this->upload->display_errors();
    //         }
    //     }
    //     echo json_encode($res);
    // }

    // public function import()
    // {
    //     $res = array('status' => 'error', 'message' => 'File tidak ada');
    //     if (isset($_FILES['file']['name'])) {
    //         $brand = $this->main->get('brands', ['principle_id' => $this->data['user']->principle_id]);
    //         $this->load->library('upload');
    //         $config['upload_path'] = '../files/import/';
    //         $config['allowed_types'] = 'xlsx';
    //         $config['overwrite'] = TRUE;
    //         $this->upload->initialize($config);
    //         if ($this->upload->do_upload('file')) {
    //             $file = $this->upload->data();
    //             $input_file = '../files/import/' . $file['file_name'];
    //             try {
    //                 $input_file_type = \PhpOffice\PhpSpreadsheet\IOFactory::identify($input_file);
    //                 $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($input_file_type);
    //                 $reader->setReadDataOnly(true);
    //                 $reader->setReadEmptyCells(false);
    //                 $spreadsheet = $reader->load($input_file);
    //             } catch(Exception $e) {
    //                 die('Error loading file "'.pathinfo($input_file,PATHINFO_BASENAME).'": '.$e->getMessage());
    //             }
    //             // $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load('../files/import/' . $file['file_name']);
    //             $sheet = $spreadsheet->getSheet(0);

    //             for ($row = 7; $row <= $sheet->getHighestDataRow(); $row++) {
    //                 $data = $sheet->rangeToArray('A' . $row . ':' . 'S'. $row, NULL, TRUE, FALSE);
    //                 $brand_other = '';
    //                 $count_empty_cell = 0;
    //                 $res = array('status' => 'success', 'message' => 'Import berhasil disimpan', 'redirect' => site_url('catalog/products'));

    //                 //check if row is empty
    //                 for($j=0;$j<=17;$j++){  
    //                     if($data[0][$j] == '' || $data[0][$j] == NULL){
    //                         $count_empty_cell++;
    //                     }
    //                 }
    //                 if($count_empty_cell>=17) break;
    //                 else{
    //                     //check if empty cell
    //                     for($j=1;$j<=16;$j++){
    //                         if($j==3 || $j==5 || $j==10 || $j==13 || $j==16) continue;
                           
    //                         if($data[0][$j] == '' || $data[0][$j] == NULL){
    //                             $res = array('status' => 'error', 'message' => 'Ada data wajib yang tidak terisi!');
    //                             break 2;
    //                         }
    //                     }

    //                     if($data[0][14]=='AKTIF')
    //                         $status_produk=1;
    //                     else if ($data[0][14]=='NONAKTIF')
    //                         $status_produk=0;

    //                     // if(!is_numeric($data[0][4])){
    //                     //     $brand_other = $data[0][4];
    //                     //     $data[0][4] = 0;
    //                     // }
    //                     if(!$data[0][10]) {
    //                         $data[0][10] = '';
    //                     }

    //                     $product = array(
    //                         'store_type' => 'principal',
    //                         'store_id' => $this->data['user']->principle_id,
    //                         'meta_title'=> $data[0][1],
    //                         'meta_description' => $data[0][2],
    //                         'code' => $data[0][0],
    //                         'name' => $data[0][1],
    //                         'short_description' => $data[0][2],
    //                         'description' => '<p>'.$data[0][3].'</p>',
    //                         'brand' => $brand->id,
    //                         'category' => $data[0][4],
    //                         'etalase' => $data[0][5],
    //                         'weight' => $data[0][6],
    //                         'length' => $data[0][7],
    //                         'width' => $data[0][8],
    //                         'height' => $data[0][9],
    //                         'guarantee' => $data[0][10],
    //                         'price' => $data[0][11],
    //                         'quantity' => $data[0][12],
    //                         'meta_keyword' => $data[0][13],
    //                         'status' => $status_produk,
    //                         'brand_other' => $brand_other
    //                     );

    //                     $this->db->insert('products', $product);
    //                     $insert_id = $this->db->insert_id();

                        
    //                     $primary = 1;
    //                     $count = 1;
    //                     for($i=15;$i<=17;$i++){
    //                         if($data[0][$i] == '' || $data[0][$i] == NULL) continue;
                            
    //                         $image_download = $this->file_get_contents_curl($data[0][$i]); 
    //                         $image_name = 'import_product_image_'.$insert_id.'_'.$count.'.jpg';
    //                         $folder_name = '../files/images/principal/' . $this->data['user']->username . '/' . $image_name;
                              
    //                         file_put_contents($folder_name, $image_download ); 

    //                         $product_image = array(
    //                             'product' => $insert_id,
    //                             'image' => 'principal/'.$this->data['user']->username.'/'.$image_name,
    //                             'primary' => $primary

    //                         );
    //                         $this->db->insert('product_image',$product_image);

    //                         $primary = 0;
    //                         $count++;
    //                     }
    //                 }
    //             }
                
    //         } else {
    //             $res['message'] = $this->upload->display_errors();
    //         }
    //     }
    //     echo json_encode($res);
    // }

    public function import()
    {
        $res = array('status' => 'error', 'message' => 'File tidak ada');
        if (isset($_FILES['file']['name'])) {
            $this->load->library('upload');
            $config['upload_path'] = '../files/import/';
            $config['allowed_types'] = 'xlsx';
            $config['overwrite'] = TRUE;
            $this->upload->initialize($config);
            if ($this->upload->do_upload('file')) {
                $file = $this->upload->data();
                $input_file = '../files/import/' . $file['file_name'];
                try {
                    $input_file_type = \PhpOffice\PhpSpreadsheet\IOFactory::identify($input_file);
                    $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($input_file_type);
                    $reader->setReadDataOnly(true);
                    $reader->setReadEmptyCells(false);
                    $spreadsheet = $reader->load($input_file);
                } catch(Exception $e) {
                    die('Error loading file "'.pathinfo($input_file,PATHINFO_BASENAME).'": '.$e->getMessage());
                }
                // $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load('../files/import/' . $file['file_name']);
                $sheet = $spreadsheet->getSheet(0);

                for ($row = 7; $row <= $sheet->getHighestDataRow(); $row++) {
                    $data = $sheet->rangeToArray('A' . $row . ':' . 'S'. $row, NULL, TRUE, FALSE);
                    $brand_other = '';
                    $count_empty_cell = 0;
                    $res = array('status' => 'success', 'message' => 'Import berhasil disimpan', 'redirect' => site_url('catalog/products'));

                    //check if row is empty
                    for($j=0;$j<=18;$j++){  
                        if($data[0][$j] == '' || $data[0][$j] == NULL){
                            $count_empty_cell++;
                        }
                    }
                    if($count_empty_cell>=18) break;
                    else{
    
                        //check if empty cell
                        for($j=1;$j<=16;$j++){
                            if($j==3 || $j==4 || $j==6 || $j==11 || $j==14) continue;
                           
                            if($data[0][$j] == '' || $data[0][$j] == NULL){
                                $res = array('status' => 'error', 'message' => 'Ada data wajib yang tidak terisi!');
                                break 2;
                            }
                        }

                        if($data[0][15]=='AKTIF')
                            $status_produk=1;
                        else if ($data[0][15]=='NONAKTIF')
                            $status_produk=0;

                        if(!is_numeric($data[0][4])){
                            $brand_other = $data[0][4];
                            $data[0][4] = 0;
                        }


                        $product = array(
                            'store_type' => 'principal',
                            'store_id' => $this->data['user']->principle_id,
                            'meta_title'=> $data[0][1],
                            'meta_description' => $data[0][2],
                            'code' => $data[0][0],
                            'name' => $data[0][1],
                            'short_description' => $data[0][2],
                            'description' => '<p>'.$data[0][3].'</p>',
                            'brand' => $data[0][4],
                            'category' => $data[0][5],
                            'etalase' => $data[0][6],
                            'weight' => $data[0][7],
                            'length' => $data[0][8],
                            'width' => $data[0][9],
                            'height' => $data[0][10],
                            'guarantee' => $data[0][11],
                            'price' => $data[0][12],
                            'quantity' => $data[0][13],
                            'meta_keyword' => $data[0][14],
                            'status' => $status_produk,
                            'brand_other' => $brand_other
                        );

                        $this->db->insert('products', $product);
                        $insert_id = $this->db->insert_id();

                        $primary = 1;
                        $count = 1;
                        for($i=16;$i<=18;$i++){
                            if($data[0][$i] == '' || $data[0][$i] == NULL) continue;
                            
                            $image_download = $this->file_get_contents_curl($data[0][$i]); 
                            $image_name = 'import_product_image_'.$insert_id.'_'.$count.'.jpg';
                            $folder_name = '../files/images/principal/' . $this->data['user']->username . '/' . $image_name;
                              
                            file_put_contents($folder_name, $image_download ); 

                            $product_image = array(
                                'product' => $insert_id,
                                'image' => 'principal/'.$this->data['user']->username.'/'.$image_name,
                                'primary' => $primary

                            );
                            $this->db->insert('product_image',$product_image);

                            $primary = 0;
                            $count++;
                        }
                    }
                }
                
            } else {
                $res['message'] = $this->upload->display_errors();
            }
        }
        echo json_encode($res);
    }

    public function file_get_contents_curl($url) { 
        $ch = curl_init(); 
      
        curl_setopt($ch, CURLOPT_HEADER, 0); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_URL, $url); 
      
        $data = curl_exec($ch); 
        curl_close($ch); 
      
        return $data; 
    } 

    private function is_decimal($val)
    {
        return is_numeric($val) && floor($val) != $val;
    }

    public function update_status_product()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        if ($data['id']) {
            $data['id'] = decode($data['id']);
            $this->main->update('products', $data, array('id' => $data['id']));
        } else {
            return false;
        }
        echo $data['status'];
    }

    public function update_quantity_product()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        if ($data['id']) {
            $id = decode($data['id']);
            unset($data['id']);
            $this->main->update('products', $data, array('id' => $id));
        } else {
            return 'error';
        }
        echo $data['id'];
    }

    public function get_table_description()
    {
        $id = $this->input->post('id');
        $data = $this->products->get_table_deskripsi($id)->result();
        echo json_encode($data);
    }
    public function download_template_import(){
        $this->load->helper('download');
        force_download('../files/Template/Template_impor_produk_principal.xlsx',NULL);
    }

    // public function download_lists() {
    //     $filename = "lists_principal_import";
    //     $spreadsheet = new Spreadsheet();
    //     $spreadsheet->setActiveSheetIndex(0);
    //     $sheet = $spreadsheet->getActiveSheet();
    //     $sheet->setTitle('List Kategori');
    //     $sheet->getColumnDimension('B')->setAutoSize(true);
    //     \PhpOffice\PhpSpreadsheet\Shared\File::setUseUploadTempDirectory(true);
    //     $sheet->fromArray(array('ID','Kategori'), NULL, 'A1');
    //     $fullFileName = "${filename}_".date('Y-m-d');
    //     $data_categories = $this->main->gets('categories',[],'id ASC');
    //     $i = 2;
    //     if ($data_categories->num_rows() > 0) {
    //         foreach ($data_categories->result() as $data_category) {
    //             $row_data = array(
    //                 $data_category->id,
    //                 $data_category->name
                    
    //             );
    //             $sheet->fromArray($row_data, null, 'A' . $i);
    //             $i++;
    //         }
    //     };
    //     $spreadsheet->createSheet(1);
    //     $spreadsheet->setActiveSheetIndex(1);
    //     $sheet = $spreadsheet->getActiveSheet();
    //     $sheet->setTitle('List Etalase');
    //     $sheet->getColumnDimension('B')->setAutoSize(true);
    //     $sheet->fromArray(array('ID','Etalase'), NULL, 'A1');
    //     $data_etalases = $this->main->gets('etalase',['store_id' => $this->data['user']->principle_id, 'store_type' => 'principal'],'id ASC');
    //     $i = 2;
    //     if ($data_etalases) {
    //         foreach ($data_etalases->result() as $data_etalase) {
    //             $row_data = array(
    //                 $data_etalase->id,
    //                 $data_etalase->name
                    
    //             );
    //             $sheet->fromArray($row_data, null, 'A' . $i);
    //             $i++;
    //         }
    //     };

    //     header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    //     header('Content-Disposition: attachment;filename="' . $fullFileName . '.xlsx"');
    //     header('Cache-Control: max-age=0');
    //     $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
    //     ob_end_clean();
    //     $writer->save('php://output');
    // }

    public function download_lists() {
        $filename = "lists_principal_import";
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('List Kategori');
        $sheet->getColumnDimension('B')->setAutoSize(true);
        \PhpOffice\PhpSpreadsheet\Shared\File::setUseUploadTempDirectory(true);
        $sheet->fromArray(array('ID','Kategori'), NULL, 'A1');
        $fullFileName = "${filename}_".date('Y-m-d');
        $data_categories = $this->main->gets('categories',[],'id ASC');
        $i = 2;
        if ($data_categories->num_rows() > 0) {
            foreach ($data_categories->result() as $data_category) {
                $row_data = array(
                    $data_category->id,
                    $data_category->name
                    
                );
                $sheet->fromArray($row_data, null, 'A' . $i);
                $i++;
            }
        };
        $spreadsheet->createSheet(1);
        $spreadsheet->setActiveSheetIndex(1);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('List Brand');
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->fromArray(array('ID','Brand'), NULL, 'A1');
        $data_brands = $this->main->gets('brands',['principle_id' => $this->data['user']->principle_id],'id ASC');
        $i = 2;
        if ($data_brands->num_rows() > 0) {
            foreach ($data_brands->result() as $data_brand) {
                $row_data = array(
                    $data_brand->id,
                    $data_brand->name
                    
                );
                $sheet->fromArray($row_data, null, 'A' . $i);
                $i++;
            }
        };
        $spreadsheet->createSheet(2);
        $spreadsheet->setActiveSheetIndex(2);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('List Etalase');
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->fromArray(array('ID','Etalase'), NULL, 'A1');
        $data_etalases = $this->main->gets('etalase',['store_id' => $this->data['user']->merchant],'id ASC');
        $i = 2;
        if($data_etalases){
            if ($data_etalases->num_rows() > 0) {
                foreach ($data_etalases->result() as $data_etalase) {
                    $row_data = array(
                        $data_etalase->id,
                        $data_etalase->name
                        
                    );
                    $sheet->fromArray($row_data, null, 'A' . $i);
                    $i++;
                }
            }
        }


        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $fullFileName . '.xlsx"');
        header('Cache-Control: max-age=0');
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        ob_end_clean();
        $writer->save('php://output');
    }

    public function check_option() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $id = decode($data['id']);
        $option = $data['option'];
        $option_product = $this->products->get_option_groups($id);
        $product_branch = $this->main->gets('products_principal_stock', ['product_id' => $id]);
        if($option_product) {
            if($product_branch) {
                if(count($option) == count($option_product->result())) {
                    if(count($option) == 2) {
                        $return = 'true';
                    } else {
                        foreach($option as $index => $value) {
                            if($index == $option_product->row()->type) {
                                $return = 'true';
                            } else {
                                $return = 'false';
                            }
                        }
                    }
                } else {
                    $return = 'false';
                }
            } else {
                $return = 'true';
            }
        } else {
            $return = 'true';
        }
        echo $return;
    }
}
