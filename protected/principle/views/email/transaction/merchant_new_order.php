<div bgcolor="#FFFFFF" style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;width:100%!important;height:100%;font-size:14px;color:#404040;margin:0;padding:0">
    <table style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:0" bgcolor="transparent">
        <tbody>
            <tr style="margin:0;padding:0">
                <td style="margin:0;padding:0"></td>
                <td style="display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0" bgcolor="#FFFFFF">
                    <div style="max-width:600px;display:block;border-collapse:collapse;margin:0 auto;border:1px solid #e7e7e7">
                        <table style="max-width:100%;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:20px 20px 0;" bgcolor="transparent">
                            <tbody>
                                <tr style="margin:0;padding:0">
                                    <td>
                                        <img src="<?php echo site_url('../assets/frontend/images/logo.png'); ?>" alt="" style="margin:10px 0">
                                    </td>
                                </tr>

                                <tr style="margin:0;padding:0">
                                    <td style="margin:0;padding:0">
                                        <h5 style="line-height:32px;font-weight:700;font-size:17px;margin:0 0 20px;padding:0"><?php echo $merchant->name; ?>, Anda mendapatkan pesanan baru</h5>
                                        <p style="font-weight:normal;font-size:14px;line-height:1.6;margin:0 0 10px;padding:0">
                                            Selamat, Anda mendapatkan pesanan baru dari <?php echo $customer->fullname; ?> dengan detil sebagai berikut:
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table style="width:100%;margin-bottom:24px;padding:0 20px;font-size: 14px;">
                            <tbody>
                                <tr>
                                    <td style="width: 30%;border-bottom: 1px solid #eee;padding: 5px 0;">ID Pemesanan</td>
                                    <td style="width: 1%;border-bottom: 1px solid #eee;padding: 5px 0;">:</td>
                                    <td style="border-bottom: 1px solid #eee;padding: 5px 0;"><?php echo $invoice->code; ?></td>
                                </tr>
                                <tr>
                                    <td style="border-bottom: 1px solid #eee;padding: 5px 0;">Tanggal Pemesanan</td>
                                    <td style="border-bottom: 1px solid #eee;padding: 5px 0;">:</td>
                                    <td style="border-bottom: 1px solid #eee;padding: 5px 0;"><?php echo get_date($invoice->date_added); ?></td>
                                </tr>
                                <tr>
                                    <td style="border-bottom: 1px solid #eee;padding: 5px 0;">Total Pesanan</td>
                                    <td style="border-bottom: 1px solid #eee;padding: 5px 0;">:</td>
                                    <td style="border-bottom: 1px solid #eee;padding: 5px 0;"><?php echo rupiah($invoice->total); ?></td>
                                </tr>
                                <tr>
                                    <td style="padding: 5px 0;">Pengiriman</td>
                                    <td style="padding: 5px 0;">:</td>
                                    <td style="padding: 5px 0;"><?php echo strtoupper(str_replace('-', ' ', $invoice->shipping_courier)); ?></td>
                                </tr>
                            </tbody>
                        </table>
                        <table style="width:100%;margin-bottom:24px;padding:0 20px;">
                            <tbody>
                                <tr>
                                    <td>
                                        <div style="border: 1px solid #e7e7e7;border-radius: 5px;padding: 0px 20px;">
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <h2 style="font-size:16px;font-weight:600;margin:0 0 16px;padding:0 20px;">Rincian Pesanan</h2>
                        <span style="width:30px;border:2px solid #FF5722;display:inline-block;margin-left:20px;"></span>
                        <div style="padding:20px;background-color:#fdfdf5;margin-bottom:20px">
                            <div style="margin:0;padding:0">
                                <ol style="font-weight:normal;font-size:14px;line-height:1.6;margin:0 0 20px;padding:0 0 0 21px">
                                    <?php foreach ($products->result() as $product) { ?>
                                        <li style="font-size:13px;margin:0 0 15px;padding:0">
                                            <b style="margin:0;padding:0"><?php echo $product->name; ?></b>
                                            <br style="margin:0;padding:0">
                                            Jumlah: <?php echo $product->quantity; ?> Buah (@ <?php echo rupiah($product->price); ?>) 
                                        </li>
                                    <?php } ?>
                                </ol>

                                <div style="font-size:13px;line-height:18px;margin:0 0 15px;padding:0">
                                    <div style="margin:0 0 5px;padding:0"><b style="margin:0;padding:0">Tujuan Pengiriman:</b></div>
                                    <?php echo $order->shipping_name; ?> <br style="margin:0;padding:0">
                                    <?php echo nl2br($order->shipping_address); ?> <br style="margin:0;padding:0">
                                    <?php echo $this->main->get('districts', array('id' => $order->shipping_district))->name; ?>, <?php echo $this->main->get('cities', array('id' => $order->shipping_city))->name; ?>, <?php echo $order->shipping_postcode; ?> <br style="margin:0;padding:0">
                                    <?php echo $this->main->get('provincies', array('id' => $order->shipping_province))->name; ?> <br style="margin:0;padding:0">

                                    Telp: <?php echo $order->shipping_phone; ?>
                                </div>

                                <div style="margin:20px 0 0;padding:0">
                                    <table style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0" bgcolor="transparent">
                                        <tbody style="margin:0;padding:0">
                                            <tr style="margin:0;padding:0">
                                                <td style="width:50%;font-weight:700;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0" valign="top">Total Harga Produk:</td>
                                                <td style="text-align:right;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0" align="right" valign="top"><?php echo rupiah($invoice->subtotal); ?></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <div style="border-bottom-width:1px;border-bottom-color:#eee;border-bottom-style:solid;margin:0;padding:0"></div>
                                    <table style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0" bgcolor="transparent">
                                        <tbody style="margin:0;padding:0">
                                            <tr style="margin:0;padding:0">
                                                <td style="width:50%;font-weight:700;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0" valign="top">Ongkos kirim:</td>
                                                <td style="text-align:right;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0" align="right" valign="top"><?php echo rupiah($invoice->shipping_cost); ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div style="border-bottom-width:1px;border-bottom-color:#eee;border-bottom-style:solid;margin:0;padding:0"></div>
                                    <table style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0" bgcolor="transparent">
                                        <tbody style="margin:0;padding:0">
                                            <tr style="margin:0;padding:0">
                                                <td style="width:50%;font-weight:700;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0" valign="top">Total Pembayaran:</td>
                                                <td style="text-align:right;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0" align="right" valign="top"><?php echo rupiah($invoice->total); ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <p style="font-weight:normal;font-size:14px;line-height:1.6;margin:0 0 10px;padding:0 20px">
                            Mohon konfirmasi terima/tolak pesanan paling lambat tanggal <b><?php echo get_date_time($invoice->due_date); ?></b>
                        </p>
                        <div style="padding:0 20px">
                            <p style="font-size:14px;color:#999;padding:24px 0 10px;margin:0;border-top: 1px solid #E0E0E0;">
                                Email dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.
                            </p>
                        </div>
                    </div>
                </td>
                <td style="margin:0;padding:0"></td>
            </tr>
        </tbody>
    </table>
</div>