<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo "{$title}"; ?></title>

        <link rel="canonical" href="<?php echo current_url(); ?>" />
        <link rel="shortcut icon" href="<?php echo site_url('assets/frontend/images/tokomanamana.ico'); ?>">
        <meta property="og:title" content="<?php echo "{$title}"; ?>" />
        <meta property="og:site_name" content="<?php echo settings('store_name'); ?>" />
        <meta property="og:url" content="<?php echo current_url(); ?>" />
        <?php if (isset($meta_description)) { ?>
            <meta name="description" content="<?php echo $meta_description; ?>"/>
            <meta property="og:description" content="<?php echo $meta_description; ?>" />
            <meta property="twitter:description" content="<?php echo $meta_description; ?>" />
        <?php } ?>
        <?php
        foreach ($css as $file) {
            echo "\n    ";
            echo '<link href="' . $file .'?timestamp='.time().'" rel="stylesheet" type="text/css" />';
        } echo "\n";
        ?>
        <script type="text/javascript">
            var site_url = '<?php echo site_url(); ?>';
            var base_url = '<?php echo base_url(); ?>';
            var current_url = '<?php echo current_url(); ?>';
            var decimal_digit = '<?php echo settings('number_of_decimal'); ?>';
            var decimal_separator = '<?php echo settings('number_separator_decimal'); ?>';
            var thousand_separator = '<?php echo settings('number_separator_thousand'); ?>';
        </script>
    </head>

    <body class="login-container">

        <div class="page-container <?php echo ($page == 'register') ? 'p-5' : ''; ?>">
            <div class="page-content">
                <div class="content-wrapper">
                    <?php echo $output; ?>
                </div>
            </div>
        </div>
        <?php
        foreach ($js as $file) {
            echo "\n    ";
            echo '<script src="' . $file . '"></script>';
        } echo "\n";
        ?>
    </body>
</html>
