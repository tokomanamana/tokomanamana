<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');


if (!function_exists('menu_active')) {

    function menu_active($menu, $active) {
        if (substr($menu, 0, strlen($active)) == $active) {
            return 'active';
        } else {
            return FALSE;
        }
    }

}

function check_url($keyword, $query = '') {
    $CI = get_instance();
    $CI->load->database();

    if ($query)
        $CI->db->where('query !=', $query);
    $query = $CI->db->where('keyword', $keyword)->get('seo_url');
    return($query->num_rows() > 0) ? true : false;
}

function invoice_code() {
    $CI = get_instance();
    $CI->load->database();
    $query = $CI->db->select('MAX(SUBSTR(code, 12)) code')
            ->where('YEAR(date_added)', date('Y'))
            ->where('MONTH(date_added)', date('m'))
            ->get('warehouse_invoice');
    if ($query->num_rows() > 0) {
        $increment = $query->row()->code;
    } else {
        $increment = 0;
    }
    $increment = ($increment == 0) ? 1 : $increment + 1;
    if ($increment < 10) {
        $increment = '000' . $increment;
    } elseif ($increment < 100) {
        $increment = '00' . $increment;
    } elseif ($increment < 1000) {
        $increment = '0' . $increment;
    }
    $code = 'INV/' . date('Ym') . '/' . $increment;
    return $code;
}
function rent_code() {
    $CI = get_instance();
    $CI->load->database();
    $query = $CI->db->select('MAX(SUBSTR(code, 12)) code')
            ->where('YEAR(date_added)', date('Y'))
            ->where('MONTH(date_added)', date('m'))
            ->get('warehouse_rent');
    if ($query->num_rows() > 0) {
        $increment = $query->row()->code;
    } else {
        $increment = 0;
    }
    $increment = ($increment == 0) ? 1 : $increment + 1;
    if ($increment < 10) {
        $increment = '000' . $increment;
    } elseif ($increment < 100) {
        $increment = '00' . $increment;
    } elseif ($increment < 1000) {
        $increment = '0' . $increment;
    }
    $code = 'RNT/' . date('Ym') . '/' . $increment;
    return $code;
}

function tinymce_get_url_files($text) {
    return str_replace('files/', base_url('../files/'), $text);
}

function tinymce_parse_url_files($text) {
    return str_replace(base_url('../files/'), 'files/', $text);
}
