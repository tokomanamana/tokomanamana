<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Crons extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

   public function create_history_usage() {
        // cek jika tanggal mulai sama dengan hari ini
        $history = $this->db->where('rent_date <= CURDATE()')->where('status', '1')->get('warehouse_rent');
        //var_dump($history->result());exit();
        if ($history->num_rows() > 0) {
            // create daily history usage
            foreach ($history->result() as $hst) {
                $dimension = 0;
                // get data branch
                $branch = $this->main->get('merchants', array('id' => $hst->warehouse_id));
                $ppm = $branch->warehouse_price;
                //get data product inventory
                $this->db->select('wp.*,p.length,p.width,p.height');
                $this->db->join('products p','p.id = wp.product_id','left');
                $products = $this->main->gets('warehouse_product wp', array('wp.rent_id' => $hst->id));
                if($products->num_rows() > 0){
                    foreach($products->result() as $product){
                        $count = ($product->length / 100) * ($product->width / 100) * ($product->height / 100);
                        $count = number_format($count, 2);
                        $dimension = number_format($dimension + $count,2);
                    }
                }
                // check data invoice history today
                $inv_history = $this->db->where('history_date = CURDATE()')->where('rent_id', $hst->id)->get('warehouse_invoice_history');
                if($inv_history->num_rows() == 0){
                    // Jika blum ada invoice history
                    $this->main->insert('warehouse_invoice_history', array('rent_id' => $hst->id, 'history_date' => date('Y-m-d'), 'dimension' => $dimension,'dimension_round' => ceil($dimension),'ppm'=>$ppm,'total'=>$ppm*ceil($dimension)));
                } else {
                    // Jika sudah ada invoice history
                    $this->main->update('warehouse_invoice_history', array('dimension' => $dimension,'dimension_round' => ceil($dimension),'ppm'=>$ppm,'total'=>$ppm*ceil($dimension)), array('rent_id' => $hst->id));
                }
                //var_dump(($inv_history->result()));exit();
            }
        }

   }

   public function confirm_branch_stok_in() {
       $get_histories = $this->main->gets('products_principal_history', ['status' => 1, 'type' => 'in']);
       if($get_histories) {
           foreach($get_histories->result() as $stock_history) {
                $product_id = $stock_history->product_id;
                $product = $this->main->get('products', ['id' => $product_id, 'store_type' => 'principal']);
                $category = $this->main->get('categories', ['id' => $product->category]);
                $branch = $this->main->get('merchants', ['id' => $stock_history->branch_id, 'type' => 'principle_branch']);
                $new_quantity = $stock_history->quantity;
                if($product && $category && $branch) {
                    if($product->variation) {
                        $product_branch_variation = $this->main->get('products_principal_stock', ['branch_id' => $stock_history->branch_id, 'product_id' => $product_id, 'id_option' => $stock_history->id_option]);
                        $product_option = $this->main->get('product_option', ['id' => $stock_history->id_option]);
    
                        // create seo url
                        if(!$this->main->get('products_principal_stock', ['branch_id' => $stock_history->branch_id, 'product_id' => $product_id])) {
                            $branch_profile = $this->main->get('merchant_users', ['id' => $branch->auth]);
                            $branch_name = $this->generateSeoURL($branch_profile->username);
                            $category_name = $this->generateSeoURL($category->name);
                            $product_name = $this->generateSeoURL($product->name);
                            $seo_url = $branch_name . '/' . $category_name . '/' . $product_name;
                            $data_url = [
                                'query' => 'catalog/products/view/' . $product_id . '/' . $stock_history->branch_id,
                                'keyword' => $seo_url
                            ];
                            $this->main->insert('seo_url', $data_url);
                        }
    
                        if($product_branch_variation) {
                            $old_quantity = $product_branch_variation->quantity;
                            $quantity = $old_quantity + $new_quantity;
                            $this->main->update('products_principal_stock', ['quantity' => $quantity], ['product_id' => $product_id, 'branch_id' => $stock_history->branch_id, 'id_option' => $stock_history->id_option]);
                        } else {
                            $data = [
                                'branch_id' => $stock_history->branch_id,
                                'product_id' => $product_id,
                                'quantity' => $new_quantity,
                                'id_option' => $stock_history->id_option
                            ];
                            $this->main->insert('products_principal_stock', $data);
                        }
    
                        $quantity_product = $product_option->quantity;
                        $new_quantity_product = $quantity_product - $new_quantity;
                        $this->main->update('product_option', ['quantity' => $new_quantity_product], ['id' => $product_option->id]);
                        $this->main->update('products_principal_history', ['status' => 2], ['id' => $stock_history->id]);
                    } else {
                        $product_branch = $this->main->get('products_principal_stock', ['product_id' => $product_id, 'branch_id' => $stock_history->branch_id]);
                        if($product_branch) {
                            $old_quantity = $product_branch->quantity;
                            $quantity = $old_quantity + $new_quantity;
                            $this->main->update('products_principal_stock', ['quantity' => $quantity], ['product_id' => $product_id, 'branch_id' => $stock_history->branch_id]);
                        } else {
                            $branch_profile = $this->main->get('merchant_users', ['id' => $branch->auth]);
                            $branch_name = $this->generateSeoURL($branch_profile->username);
                            $category_name = $this->generateSeoURL($category->name);
                            $product_name = $this->generateSeoURL($product->name);
                            $seo_url = $branch_name . '/' . $category_name . '/' . $product_name;
                            $data_url = [
                                'query' => 'catalog/products/view/' . $product_id . '/' . $stock_history->branch_id,
                                'keyword' => $seo_url
                            ];
                            $data = [
                                'branch_id' => $stock_history->branch_id,
                                'product_id' => $product_id,
                                'quantity' => $new_quantity
                            ];
                            $this->main->insert('products_principal_stock', $data);
                            if(!$this->main->get('seo_url', ['query' => $data_url['query']])) {
                                $this->main->insert('seo_url', $data_url);
                            }
                        }
                        $quantity_product = $product->quantity;
                        $new_quantity_product = $quantity_product - $new_quantity;
                        $this->main->update('products', ['quantity' => $new_quantity_product], ['id' => $product_id, 'store_type' => 'principal']);
                        $this->main->update('products_principal_history', ['status' => 2], ['id' => $stock_history->id]);
                    }
                }
           }
       }
   }

   private function generateSeoURL($string, $wordLimit = 0){
    $separator = '-';
    
    if($wordLimit != 0){
        $wordArr = explode(' ', $string);
        $string = implode(' ', array_slice($wordArr, 0, $wordLimit));
    }

    $quoteSeparator = preg_quote($separator, '#');

    $trans = array(
        '&.+?;'                    => '',
        '[^\w\d _-]'            => '',
        '\s+'                    => $separator,
        '('.$quoteSeparator.')+'=> $separator
    );

    $string = strip_tags($string);
    foreach ($trans as $key => $val){
        $string = preg_replace('#'.$key.'#i'.(UTF8_ENABLED ? 'u' : ''), $val, $string);
    }

    $string = strtolower($string);

    return trim(trim($string, $separator));
}
}
