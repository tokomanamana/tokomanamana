<?php

$lang['dashboard'] = 'Dasbor';
$lang['catalog'] = 'Katalog';
$lang['branch'] = 'Cabang';
$lang['product'] = 'Produk';
$lang['catalog_product'] = 'Produk';
$lang['catalog_brand'] = 'Merek';
$lang['catalog_stock'] = 'Manajemen Stok';
$lang['catalog_stock_history'] = 'Riwayat Stok';
$lang['catalog_refund_stock'] = 'Konfirmasi Stok';
$lang['product_tanaka'] = 'Produk Tanaka';
$lang['product_other'] = 'Produk Lainnya';
$lang['review'] = 'Ulasan';
$lang['category'] = 'Kategori';
$lang['brand'] = 'Merek';
$lang['feature'] = 'Fitur';
$lang['option'] = 'Variasi';
$lang['order'] = 'Pesanan';
$lang['sale'] = 'Penjualan';
$lang['warehouse'] = 'Gudang';
$lang['warehouse_rent'] = 'Sewa Gudang';
$lang['report'] = 'Laporan';
$lang['report_sales'] = 'Penjualan';
$lang['report_balances'] = 'Saldo';
$lang['report_sales_branch'] = 'Penjualan Cabang';
$lang['report_merchant'] = 'Merchant';
$lang['customer'] = 'Pelanggan';
$lang['address'] = 'Alamat';
$lang['design'] = 'Desain';
$lang['page'] = 'Halaman';
$lang['banner'] = 'Banner';
$lang['marketing'] = 'Marketing';
$lang['coupon'] = 'Kupon';
$lang['subscriber'] = 'Berlangganan Email';
$lang['mail'] = 'Mail Blast';
$lang['point'] = 'Point';
$lang['setting'] = 'Pengaturan';
$lang['filemanager'] = 'File Manager';
$lang['seo_url'] = 'SEO Url';
$lang['principle'] = 'Principal';
$lang['merchant'] = 'Merchant';
$lang['merchant_branch'] = 'Cabang';
$lang['merchant_group'] = 'Kelompok Merchant';
$lang['merchant_withdrawal'] = 'Penarikan Dana';
$lang['user'] = 'Pengguna';
$lang['user_group'] = 'Grup Pengguna';
$lang['user_permission'] = 'Hak Akses';
$lang['blog'] = 'Blog';
$lang['blog_post'] = 'Post';
$lang['blog_category'] = 'Kategori';
$lang['payment_method'] = 'Metode Pembayaran';
$lang['actions'] = 'Opsi';
$lang['profile_picture'] = 'Foto Profil';
$lang['catalog_price'] = 'Manajemen Harga';

$lang['button_save'] = '<i class="icon-floppy-disk position-left"></i> Simpan';
$lang['button_save_new'] = '<i class="icon-floppy-disk position-left"></i> Simpan dan Tambah Baru';
$lang['button_cancel'] = '<i class="icon-cancel-square2 position-left"></i> Batal';
$lang['button_view'] = '<i class="icon-search4"></i> Lihat';
$lang['button_delete'] = '<i class="icon-trash"></i> Hapus';
$lang['button_edit'] = '<i class="icon-pencil"></i> Edit';
$lang['button_activate'] = '<i class="icon-checkmark3"></i> Aktifkan';
$lang['button_disabled'] = '<i class="icon-cross2"></i> Non-Aktifkan';
$lang['button_add_image'] = '<i class="icon-plus3 position-left"></i> Tambah Gambar';
$lang['button_edit_image'] = '<i class="icon-pencil position-left"></i> Ubah Gambar';
$lang['button_delete_image'] = '<i class="icon-cross2 position-left"></i> Hapus Gambar';

$lang['message_save_success'] = 'Data berhasil disimpan.';
$lang['message_delete_success'] = 'Data berhasil dihapus.';
$lang['message_activated_success'] = 'Data berhasil diaktifkan.';
$lang['message_disabled_success'] = 'Data berhasil di non-aktifkan.';
$lang['message_save_error'] = 'Data gagal disimpan.';
$lang['message_delete_error'] = 'Data gagal dihapus.';
$lang['message_activated_error'] = 'Data gagal diaktifkan.';
$lang['message_disabled_error'] = 'Data gagal di non-aktifkan.';

$lang['yes'] = 'Ya';
$lang['no'] = 'Tidak';
$lang['enabled'] = 'Aktif';
$lang['disabled'] = 'Nonaktif';

$lang['January'] = 'Januari';
$lang['February'] = 'Februari';
$lang['March'] = 'Maret';
$lang['April'] = 'April';
$lang['May'] = 'Mei';
$lang['June'] = 'Juni';
$lang['July'] = 'Juli';
$lang['August'] = 'Agustus';
$lang['September'] = 'September';
$lang['October'] = 'Oktober';
$lang['November'] = 'Nopember';
$lang['December'] = 'Desember';