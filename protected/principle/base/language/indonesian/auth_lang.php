<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Errors
$lang['error_csrf'] = 'Form yang dikirim tidak lulus pemeriksaan keamanan kami.';

// Login
$lang['login_heading'] = 'Login Admin Application';
$lang['login_subheading'] = 'Silakan login dengan email dan password anda.';
$lang['login_identity_label'] = 'Email';
$lang['login_password_label'] = 'Kata Sandi';
$lang['login_remember_label'] = 'Ingat saya';
$lang['login_submit_btn'] = 'Login';
$lang['login_forgot_password'] = 'Lupa kata sandi?';

$lang['account_setting_title'] = 'Pengaturan Akun';

$lang['account_fullname_label'] = 'Nama Lengkap';
$lang['account_phone_label'] = 'No. Handphone';
$lang['account_email_label'] = 'Email';
$lang['account_old_password_label'] = 'Password Lama';
$lang['account_new_password_label'] = 'Password Baru';

$lang['account_new_password_help'] = '*untuk merubah password isi password lama dan password baru';

$lang['account_old_password_warning_message'] = 'Untuk merubah password silahkan isi password lama Anda.';
$lang['account_old_password_error_message'] = 'Password lama yang dimasukkan salah.';
$lang['account_update_success_message'] = 'Akun berhasil diperbaharui.';

// Index
$lang['index_heading'] = 'Pengguna';
$lang['index_subheading'] = 'Di bawah ini list dari para Pengguna.';
$lang['index_fname_th'] = 'Nama Awal';
$lang['index_lname_th'] = 'Nama Akhir';
$lang['index_email_th'] = 'Email';
$lang['index_groups_th'] = 'Grup';
$lang['index_status_th'] = 'Status';
$lang['index_action_th'] = 'Aksi';
$lang['index_active_link'] = 'Aktif';
$lang['index_inactive_link'] = 'Tidak Aktif';
$lang['index_create_user_link'] = 'Buat Pengguna baru';
$lang['index_create_group_link'] = 'Buat grup baru';

// Deactivate User
$lang['deactivate_heading'] = 'Deaktivasi Pengguna';
$lang['deactivate_subheading'] = 'Anda yakin akan melakukan deaktivasi akun Pengguna \'%s\'';
$lang['deactivate_confirm_y_label'] = 'Ya:';
$lang['deactivate_confirm_n_label'] = 'Tidak:';
$lang['deactivate_submit_btn'] = 'Kirim';
$lang['deactivate_validation_confirm_label'] = 'konfirmasi';
$lang['deactivate_validation_user_id_label'] = 'ID Pengguna';

// Create User
$lang['create_user_heading'] = 'Buat Pengguna';
$lang['create_user_subheading'] = 'Silakan masukan informasi Pengguna di bawah ini.';
$lang['create_user_fname_label'] = 'Nama Awal:';
$lang['create_user_lname_label'] = 'Nama Akhir:';
$lang['create_user_company_label'] = 'Nama Perusahaan:';
$lang['create_user_identity_label'] = 'Identitas:';
$lang['create_user_email_label'] = 'Surel:';
$lang['create_user_phone_label'] = 'Telepon:';
$lang['create_user_password_label'] = 'Kata Sandi:';
$lang['create_user_password_confirm_label'] = 'Konfirmasi Kata Sandi:';
$lang['create_user_submit_btn'] = 'Buat Pengguna';
$lang['create_user_validation_fname_label'] = 'Nama Awal';
$lang['create_user_validation_lname_label'] = 'Nama Akhir';
$lang['create_user_validation_identity_label'] = 'Identitas';
$lang['create_user_validation_email_label'] = 'Alamat Surel';
$lang['create_user_validation_phone_label'] = 'Telepon';
$lang['create_user_validation_company_label'] = 'Nama Perusahaan';
$lang['create_user_validation_password_label'] = 'Kata Sandi';
$lang['create_user_validation_password_confirm_label'] = 'Konfirmasi Kata Sandi';

// Edit User
$lang['edit_user_heading'] = 'Ubah Pengguna';
$lang['edit_user_subheading'] = 'Silakan masukan informasi Pengguna di bawah ini.';
$lang['edit_user_fname_label'] = 'Nama Awal:';
$lang['edit_user_lname_label'] = 'Nama Akhir:';
$lang['edit_user_company_label'] = 'Nama Perusahaan:';
$lang['edit_user_email_label'] = 'Surel:';
$lang['edit_user_phone_label'] = 'Telepon:';
$lang['edit_user_password_label'] = 'Kata Sandi: (jika mengubah sandi)';
$lang['edit_user_password_confirm_label'] = 'Konfirmasi Kata Sandi: (jika mengubah sandi)';
$lang['edit_user_groups_heading'] = 'Anggota dari Grup';
$lang['edit_user_submit_btn'] = 'Simpan Pengguna';
$lang['edit_user_validation_fname_label'] = 'Nama Awal';
$lang['edit_user_validation_lname_label'] = 'Nama Akhir';
$lang['edit_user_validation_email_label'] = 'Alamat Surel';
$lang['edit_user_validation_phone_label'] = 'Telepon';
$lang['edit_user_validation_company_label'] = 'Nama Perusahaan';
$lang['edit_user_validation_groups_label'] = 'Nama Grup';
$lang['edit_user_validation_password_label'] = 'Kata Sandi';
$lang['edit_user_validation_password_confirm_label'] = 'Konfirmasi Kata Sandi';

// Create Group
$lang['create_group_title'] = 'Buat Grup';
$lang['create_group_heading'] = 'Buat Grupp';
$lang['create_group_subheading'] = 'Silakan masukan detail Grup di bawah ini.';
$lang['create_group_name_label'] = 'Nama Grup:';
$lang['create_group_desc_label'] = 'Deskripsi:';
$lang['create_group_submit_btn'] = 'Buat Grup';
$lang['create_group_validation_name_label'] = 'Nama Grup';
$lang['create_group_validation_desc_label'] = 'Deskripsi';

// Edit Group
$lang['edit_group_title'] = 'Ubah Grup';
$lang['edit_group_saved'] = 'Grup Tersimpan';
$lang['edit_group_heading'] = 'Ubah Grup';
$lang['edit_group_subheading'] = 'Silakan masukan detail Grup di bawah ini.';
$lang['edit_group_name_label'] = 'Nama Grup:';
$lang['edit_group_desc_label'] = 'Deskripsi:';
$lang['edit_group_submit_btn'] = 'Simpan Grup';
$lang['edit_group_validation_name_label'] = 'Nama Grup';
$lang['edit_group_validation_desc_label'] = 'Deskripsi';

// Change Password
$lang['change_password_heading'] = 'Ganti Kata Sandi';
$lang['change_password_old_password_label'] = 'Kata Santi Lama:';
$lang['change_password_new_password_label'] = 'Kata Sandi Baru (paling sedikit %s karakter):';
$lang['change_password_new_password_confirm_label'] = 'Konfirmasi Kata Sandi:';
$lang['change_password_submit_btn'] = 'Ubah';
$lang['change_password_validation_old_password_label'] = 'Kata Sandi Lama';
$lang['change_password_validation_new_password_label'] = 'Kata Sandi Baru';
$lang['change_password_validation_new_password_confirm_label'] = 'Konfirmasi Kata Sandi Baru';

// Forgot Password
$lang['forgot_password_heading'] = 'Lupa Kata Sandi';
$lang['forgot_password_subheading'] = 'Silakan masukkan %s anda, agar kami dapat mengirim surel untuk mereset Kata Sandi Anda.';
$lang['forgot_password_email_label'] = '%s:';
$lang['forgot_password_submit_btn'] = 'Kirim';
$lang['forgot_password_validation_email_label'] = 'Alamat Surel';
$lang['forgot_password_username_identity_label'] = 'Nama Pengguna';
$lang['forgot_password_email_identity_label'] = 'Surel';
$lang['forgot_password_email_not_found'] = 'Tidak ada data dari surel tersebut.';

// Reset Password
$lang['reset_password_heading'] = 'Ganti Kata Sandi';
$lang['reset_password_new_password_label'] = 'Kata Sandi Baru (paling sedikit %s karakter):';
$lang['reset_password_new_password_confirm_label'] = 'Konfirmasi Kata Sandi:';
$lang['reset_password_submit_btn'] = 'Ubah';
$lang['reset_password_validation_new_password_label'] = 'Kata Sandi Baru';
$lang['reset_password_validation_new_password_confirm_label'] = 'Konfirmasi Kata Sandi Baru';

// Principal
$lang['account'] = 'Informasi Akun';
$lang['store'] = 'Informasi Toko';
$lang['bank'] = 'Informasi Bank';

//
$lang['brand_name'] = 'Nama Brand';
$lang['brand_image'] = 'Gambar Brand';
$lang['company_name'] = 'Nama Perusahaan';
$lang['pic_name'] = 'Nama Lengkap pihak yang dapat dihubungi';
$lang['main_category'] = 'Apa Kategori Utama Produk yang Anda Jual?';
$lang['sku_total'] = 'Berapa Jenis Produk (SKU) yang ingin Anda Jual di Tokomanamana?';
$lang['revenue'] = 'Berapa Revenue Anda per bulan ?';
$lang['document'] = 'Apakah Anda Memiliki Dokumen Sah Berikut ini?';

$lang['instagram_acc'] = 'Akun toko di Instagram (jika ada)';
$lang['other_ecommerce'] = 'Apakah Anda Menjual Produk Secara Online di Website/ E-Commerce Lainnya? Jika Ya, Dimana?';


$lang['range_price'] = 'Berapa Range Harga Produk Anda ?';
$lang['know_from'] = 'Dari Mana Anda mendapatkan Informasi Mengenai Official brand Tokomanamana?';
//
$lang['email'] = 'Email';
$lang['password'] = 'Password';
$lang['confirm_password'] = 'Konfirmasi Password';
$lang['owner_name'] = 'Nama Pemilik';
$lang['owner_phone'] = 'No. Handphone';
$lang['verified_phone'] = 'Verifikasi No Hp';
$lang['owner_birthday'] = 'Tanggal Lahir';
$lang['owner_id'] = 'No. KTP';
$lang['store_name'] = 'Nama Toko';
$lang['store_telephone'] = 'No. Telepon';
$lang['store_address'] = 'Alamat Toko';
$lang['store_province'] = 'Provinsi';
$lang['store_city'] = 'Kota';
$lang['store_district'] = 'Kecamatan';
$lang['store_postcode'] = 'Kode Pos';
$lang['bank_name'] = 'Nama Bank';
$lang['bank_branch'] = 'Cabang';
$lang['bank_account_number'] = 'No. Rekening';
$lang['bank_account_name'] = 'Nama Rekening';
$lang['terms_principal'] = 'Syarat & Ketentuan mendaftar sebagai Principal Tokomanamana';
$lang['terms'] = 'Syarat dan Ketentuan';
$lang['terms_prefix'] = 'Dengan ini saya menyetujui';
$lang['remember'] = 'Ingat saya';
$lang['login'] = 'Login';

$lang['brand_name_placeholder'] = 'Masukkan nama brand';
$lang['company_name_placeholder'] = 'Masukkan nama perusahaan';
$lang['pic_name_placeholder'] = 'Masukkan nama lengkap pihak yang dapat dihubungi';
$lang['main_category_placeholder'] = 'Masukkan kategori utama produk';
$lang['instagram_acc_placeholder'] = 'Masukkan akun Instagram (Jika ada)';
$lang['other_ecommerce_placeholder'] = 'Apakah Anda Menjual Produk Secara Online di Website/ E-Commerce Lainnya?';
$lang['email_placeholder'] = 'Masukkan email perusahaan';
$lang['owner_phone_placeholder'] = 'Masukkan no. handphone';