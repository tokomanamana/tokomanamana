<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

if (!function_exists('encode')) {

    function encode($string) {
        return encrypt_decrypt('encrypt', $string);
    }

}

if (!function_exists('decode')) {

    function decode($string) {
        return encrypt_decrypt('decrypt', $string);
    }

}

if (!function_exists('encrypt_decrypt')) {

    function encrypt_decrypt($action, $string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'BogaSoftware';
        $secret_iv = 'Boga Software';

        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }

}

if (!function_exists('number')) {

    function number($val) {
        $value = number_format($val, settings('number_of_decimal'), settings('number_separator_decimal'), settings('number_separator_thousand'));
        return $value;
    }

}

if (!function_exists('parse_number')) {

    function parse_number($number) {
        $dec_point = settings('number_separator_decimal');
        if (empty($dec_point)) {
            $locale = localeconv();
            $dec_point = $locale['decimal_point'];
        }
        return floatval(str_replace($dec_point, '.', preg_replace('/[^\d' . preg_quote($dec_point) . ']/', '', $number)));
    }

}

if (!function_exists('get_date')) {

    function get_date($date) {
        $format = settings('date_format');
        $timestamp = strtotime($date);
        return date($format, $timestamp);
    }

}
if (!function_exists('get_date_time')) {

    function get_date_time($date) {
        $format = settings('date_format') . ' H:i';
        $timestamp = strtotime($date);
        return date($format, $timestamp);
    }

}

if (!function_exists('get_date_mysql')) {

    function get_date_mysql($date) {
        return date_format(date_create_from_format(settings('date_format'), $date), 'Y-m-d');
    }

}

function get_date_indo_full($fulldate) {
    $date = substr($fulldate, 8, 2);
    $month = get_month(substr($fulldate, 5, 2));
    $year = substr($fulldate, 0, 4);
    $time = substr($fulldate, 10);
    return $date . ' ' . $month . ' ' . $year . ', ' . $time;
}

if (!function_exists('get_month')) {

    function get_month($month) {
        $CI = get_instance();
        if (is_numeric($month)) {
            $month = date('F', strtotime('2017-' . $month . '-01 00:00:00'));
        }
        return $CI->lang->line($month);
    }

}

if (!function_exists('order_code')) {

    function order_code() {
        $CI = get_instance();
        $CI->load->database();
        $format = settings('order_format');

        $dd = date('d');
        $mm = date('m');
        $yy = date('y');
        $yyyy = date('Y');

        $in = $CI->db->where('DATE(date_added)', date('Y-m-d'))
                ->count_all_results('orders');
        $in = ($in == 0) ? 1 : $in + 1;
        if ($in < 10) {
            $in = '000' . $in;
        } elseif ($in < 100) {
            $in = '00' . $in;
        } elseif ($in < 1000) {
            $in = '0' . $in;
        }
        $code = str_replace('[IN]', $in, $format);
        $code = str_replace('[DD]', $dd, $code);
        $code = str_replace('[MM]', $mm, $code);
        $code = str_replace('[YY]', $yy, $code);
//        $code = str_replace('[MY]', $my, $code);
        return $code;
    }

}

if (!function_exists('product_code')) {

    function product_code($merchant_name, $merchant_id) {
        $CI = get_instance();
        $initial = strtoupper(substr($merchant_name, 0, 2) . substr($merchant_name, -1));
        $code = $merchant_id;
        if ($code < 10) {
            $code = '000' . $code;
        } elseif ($code < 100) {
            $code = '00' . $code;
        } elseif ($code < 1000) {
            $code = '0' . $code;
        }
        $prefix = $initial . $code . '-';
        $CI->load->database();
        $CI->db->select('MAX(SUBSTR(code,' . (strlen($prefix) + 1) . ')) count')->where('merchant', $merchant_id)->where('SUBSTR(code,1,' . strlen($prefix) . ')', $prefix);
        $query = $CI->db->get('products');
        if ($query->num_rows() > 0)
            $count = $query->row()->count;

        $count = ($count == NULL) ? 1 : $count + 1;
        if ($count < 10) {
            $count = '000' . $count;
        } elseif ($count < 100) {
            $count = '00' . $count;
        } elseif ($count < 1000) {
            $count = '0' . $count;
        }

        return $prefix . $count;
    }

}

if (!function_exists('create_url')) {

    function create_url($name, $merchant_id = '') {
        $CI = get_instance();
        $slug = url_title($name, '-', TRUE);
        if ($merchant_id) {
            $merchant = $CI->main->get('merchants', array('id' => $merchant_id));
            $slug = $merchant->username . '/' . $slug;
        }
        if ($CI->main->get('seo_url', array('keyword' => $slug))) {
            $CI->load->helper('string');
            $slug = $slug . random_string('alpha', '4');
        }
        return $slug;
    }

}

if (!function_exists('settings')) {

    function settings($key) {
        if ($key) {
            $CI = get_instance();
            if($CI->session->has_userdata('tnkSettings')){
                return $CI->session->userdata('tnkSettings')[$key];
            }else{
                $CI->load->database();
                $settings = $CI->db->get('settings');
                foreach ($settings->result() as $setting) {
                    $data[$setting->key] = $setting->value;
                }
                $CI->session->set_userdata('tnkSettings', $data);
                return $data[$key];
            }
        } else {
            return false;
        }
    }

}

if (!function_exists('remove_space')) {

    function remove_space($string) {
        return str_replace(' ', '', $string);
    }

}

if (!function_exists('send_mail')) {

    function send_mail($from, $from_name, $to, $subject, $message) {
        $CI = get_instance();
//        $CI->config->load('smtp');
        $CI->load->library('email');
        $CI->email->initialize(array(
            'protocol' => 'smtp',
            'smtp_host' => settings('smtp_host'),
            'smtp_user' => settings('smtp_user'),
            'smtp_pass' => settings('smtp_password'),
            'smtp_port' => settings('smtp_port'),
            'smtp_crypto' => settings('smtp_crypto'),
            'crlf' => "\r\n",
            'newline' => "\r\n",
            'mailtype' => 'html'
        ));
        $CI->email->from($from, $from_name);
        $CI->email->to($to);
        $CI->email->subject($subject);
        $CI->email->message($message);

        if ($CI->email->send())
            return true;
        else {
            log_message('error', $CI->email->print_debugger());
            return false;
        }
    }

}

function rupiah($val) {
    $value = number($val);
    return 'Rp ' . $value;
}

function stripHTMLtags($str) {
    $t = preg_replace('/<[^<|>]+?>/', '', htmlspecialchars_decode($str));
    $t = htmlentities($t, ENT_QUOTES, "UTF-8");
    return $t;
}

/* End of file common_helper.php */
/* Location: ./system/helpers/common_helper.php */
