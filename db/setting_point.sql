-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 14, 2019 at 09:19 AM
-- Server version: 5.6.24
-- PHP Version: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tmm0`
--

-- --------------------------------------------------------

--
-- Table structure for table `setting_point`
--

CREATE TABLE IF NOT EXISTS `setting_point` (
  `id` int(11) NOT NULL,
  `id_general` int(11) NOT NULL,
  `tipe_item` enum('g','k','sk','i') NOT NULL COMMENT 'g=global, k=kategori, sk=subkategori, i=item',
  `is_active` enum('0','1') NOT NULL,
  `tipe_merchant` enum('g','p','c') NOT NULL COMMENT 'g=global, p=parent, c=child',
  `point` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting_point`
--

INSERT INTO `setting_point` (`id`, `id_general`, `tipe_item`, `is_active`, `tipe_merchant`, `point`) VALUES
(1, 0, 'g', '1', 'p', 10),
(2, 0, 'g', '1', 'c', 30),
(5, 4, 'k', '1', 'p', 91),
(6, 711, 'i', '1', 'p', 5),
(7, 20, 'sk', '1', 'p', 50),
(9, 4, 'k', '1', 'c', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `setting_point`
--
ALTER TABLE `setting_point`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `setting_point`
--
ALTER TABLE `setting_point`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
