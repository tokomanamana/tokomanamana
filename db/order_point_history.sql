-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 14, 2019 at 09:19 AM
-- Server version: 5.6.24
-- PHP Version: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tmm0`
--

-- --------------------------------------------------------

--
-- Table structure for table `order_point_history`
--

CREATE TABLE IF NOT EXISTS `order_point_history` (
  `id` bigint(20) NOT NULL,
  `tgl` int(11) NOT NULL,
  `bulan` int(11) NOT NULL,
  `tahun` int(11) NOT NULL,
  `id_order_invoice` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `merchant` int(11) NOT NULL,
  `customer` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `point` int(11) NOT NULL,
  `is_downline` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_point_history`
--

INSERT INTO `order_point_history` (`id`, `tgl`, `bulan`, `tahun`, `id_order_invoice`, `code`, `merchant`, `customer`, `product`, `qty`, `point`, `is_downline`) VALUES
(1, 14, 2, 2019, 7, 'INV/201808/0007', 67, 11, 274, 1, 30, 0),
(2, 14, 2, 2019, 8, 'INV/201808/0008', 67, 11, 274, 1, 30, 0),
(3, 14, 2, 2019, 23, 'INV/201809/0014', 67, 11, 274, 1, 30, 0),
(4, 14, 2, 2019, 60, 'INV/201809/0051', 67, 11, 274, 50, 30, 0),
(5, 14, 2, 2019, 64, 'INV/201809/0055', 67, 11, 274, 1, 30, 0),
(6, 14, 2, 2019, 71, 'INV/201809/0062', 67, 11, 274, 1, 30, 0),
(7, 14, 2, 2019, 79, 'INV/201810/0001', 70, 1, 201, 1, 30, 0),
(8, 14, 2, 2019, 87, 'INV/201810/0009', 67, 43, 232, 1, 30, 0),
(9, 14, 2, 2019, 95, 'INV/201810/0017', 67, 11, 232, 5, 30, 0),
(10, 14, 2, 2019, 104, 'INV/201810/0026', 67, 29, 60, 1, 30, 0),
(11, 14, 2, 2019, 134, 'INV/201810/0056', 67, 11, 232, 5, 30, 0),
(12, 14, 2, 2019, 136, 'INV/201810/0058', 70, 1, 232, 1, 30, 0),
(13, 14, 2, 2019, 138, 'INV/201810/0060', 67, 11, 232, 5, 30, 0),
(14, 14, 2, 2019, 156, 'INV/201810/0078', 72, 11, 274, 2, 30, 0),
(15, 14, 2, 2019, 156, 'INV/201810/0078', 72, 11, 232, 1, 30, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `order_point_history`
--
ALTER TABLE `order_point_history`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `order_point_history`
--
ALTER TABLE `order_point_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
