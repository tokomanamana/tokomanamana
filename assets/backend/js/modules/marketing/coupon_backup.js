$('#couponModal').on('hidden.bs.modal', function () {
    $("#coupon_table").dataTable().fnDestroy();
    // do something…
})
function seeCoupon(id) {
    var t = $('#coupon_table').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "type" : "POST",
            "data": {id:id},
            "url" : site_url + 'marketing/coupons/get_coupon_list',
            "dataSrc": function ( json ) {
                //Make your callback here.
                $('#couponModal').modal('show');
                return json.data;
            }       
        },
    });
}