function change_product(id) {

	$.ajax({
		url: site_url + 'orders/get_detail_product',
		data: {id: id},
		method: 'post',
		success: function(data) {
			data = JSON.parse(data);
			$("#name_product").val(data.name);
			$("#code_product").val(data.code);
			$("#price").val(data.price);
			$("#quantity").val(data.quantity);
			$("#total_price").val(data.total);
		}
	})

}

$(function() {
	$('input[name=quantity]').keyup(function(){
	    var qty = parseInt($(this).val());
	    if(isNaN(qty) || qty < 1){
	        $(this).val(0);
	    }
	})

	$('input[name=ongkos_kirim]').on('keyup', function() {
		let subtotal = $('input[name=subtotal]').val();
		let ongkos_kirim = parseInt($(this).val());
		if(isNaN(ongkos_kirim)) {
			$(this).val(0);
			ongkos_kirim = 0;
		}
		$('input[name=total]').val(parseInt(subtotal) + parseInt(ongkos_kirim));
		$('input[name=total_]').val(parseInt(subtotal) + parseInt(ongkos_kirim));
    });
    
    $('#submitInvoice').on('click', function() {
        swal({
            title: 'Masukkan Password',
            text: 'Harap masukkan password untuk merubah invoice',
            type: 'input',
            showCancelButton: true,
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: 'Submit',
            cancelButtonText: lang.button.cancel,
            closeOnConfirm: false,
            inputPlaceholder: "Password",
            inputType: "password"
        }, function(inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("Password tidak boleh kosong!");
                return false
            }
            $.ajax({
                url: site_url + '/orders/check_account',
                data: {
                    password: inputValue
                },
                type: 'post',
                success: function(data) {
                    if (data == 'true') {
                        swal.close();
                        $('#form').submit()
                    } else {
                        swal.showInputError("Password salah!");
                        return false
                    }
                }
            })
        })
    })
});