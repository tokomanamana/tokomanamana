$(function () {
    $('body').on('click', '#edit-image-btn', function () {
        $('#image-preview').remove();
    });
    $('body').on('click', '#delete-image-btn', function () {
        $('#image').val('');
        $('#image-preview').remove();
        $('#add-image-btn').show();
        $('#edit-image-btn').hide();
        $('#delete-image-btn').hide();
    });
});
$('.remove_etalase').on('click', function (e) {
        var url = $(this).attr('href');
        console.log(url);
        e.preventDefault();
        swal({
            title: lang.message.confirm.delete,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: lang.button.delete,
            cancelButtonText: lang.button.cancel,
            closeOnConfirm: false
        }, function () {
            $.ajax({
                url: url,
                success: function (data) {
                    data = JSON.parse(data);
                    table.ajax.reload();
                    swal(data.message, '', data.status);
                }
            });
        });
    });
function responsive_filemanager_callback(field_id) {
    if (field_id) {
        var url = $('#' + field_id).val();
        var html = '<div class="col-md-3" id="image-preview">';
        html += '<div class="thumbnail"><div class="thumb">';
        html += '<img src="' + site_url + '../files/images/' + url + '">';
        html += '</div></div></div>';
        $(html).insertBefore('#add-image');
        $('#add-image-btn').hide();
        $('#edit-image-btn').show();
        $('#delete-image-btn').show();
    }
}