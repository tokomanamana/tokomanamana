$(function () {
    $('#province').on('change', function () {
        $('#district').html('<option value=""></option>').selectpicker('refresh');
        if ($(this).val() == "") {
            $('#city').html('<option value=""></option>').selectpicker('refresh');
        } else {
            $.ajax({
                type: "POST",
                url: site_url + 'merchants/get_cities',
                data: {id: $(this).val()},
                success: function (data) {
                    $('#city').html(data).selectpicker('refresh');
                }
            });
        }
    });
    $('#city').on('change', function () {
        if ($(this).val() == "") {
            $('#district').html('<option value=""></option>').selectpicker('refresh');
        } else {
            $.ajax({
                type: "POST",
                url: site_url + 'merchants/get_districts',
                data: {id: $(this).val()},
                success: function (data) {
                    $('#district').html(data).selectpicker('refresh');
                }
            });
        }
    });
    $('a[href=#address]').on('click', function () {
        setTimeout(function () {
            x = map.getZoom();
            c = map.getCenter();
            google.maps.event.trigger(map, 'resize');
            map.setZoom(x);
            map.setCenter(c);
        }, 50);
    });
    $('#buttonSubmit').on('click', function() {
        swal({
            title: 'Masukkan Password',
            text: 'Harap masukkan password untuk merubah toko',
            type: 'input',
            showCancelButton: true,
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: 'Submit',
            cancelButtonText: lang.button.cancel,
            closeOnConfirm: false,
            inputPlaceholder: "Password",
            inputType: "password"
        }, function(inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("Password tidak boleh kosong!");
                return false
            }
            $.ajax({
                url: site_url + '/orders/check_account',
                data: {
                    password: inputValue
                },
                type: 'post',
                success: function(data) {
                    if (data == 'true') {
                        swal.close();
                        $('#form').submit()
                    } else {
                        swal.showInputError("Password salah!");
                        return false
                    }
                }
            })
        })
    })
});

function setLocation(lat, lng) {
    $('#lat').val(lat);
    $('#lng').val(lng);
}
