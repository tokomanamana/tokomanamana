var catIds = [{"id":"0","title":"All Categories","value":"0","children":[]}];
var zonaIds = [{"id":"0","title":"All Zona","value":"0","children":[]}];
var cityIds = [{"id":"city-0", "title":"All City", "value":"0", "children": []}];
var voucherId = $('input:hidden[id=decodeid]').val();
var catIdActive = $('input:hidden[name=catIds]').val();
var zonaIdActive = $('input:hidden[name=zonaIds]').val();
var cityIdActive = $('input:hidden[name=cityIds]').val();
var productIdActive = $('input:hidden[name=product_id]').val();
var productIds = [{"id": "product-0", "title": "All Products", "value": "0", "children": []}];
var product_type = $('#product_type').val();
var category_type = $('#category_type').val();
catIdActive = catIdActive ? catIdActive.split(',') : [];
productIdActive = productIdActive ? productIdActive.split(',') : [];
//zonaIdActive =  zonaIdActive ? zonaIdActive.split(',') : [];
cityIdActive = cityIdActive ? cityIdActive.split(',') : [];
$.ajax({
    url: site_url + 'marketing/coupons/getCategories',
    type: 'get',
    data: {},
    success: function (response) {
        response = JSON.parse(response);
        jQuery.each(response.data, function(index, item) {
            let catId = {id: item.id,title: item.name, value: item.id,children:item.children}
            catIds[0].children.push(catId);
            // do something with `item` (or `this` is also `item` if you like)
        });
        $('#treeSelectorCat').treeSelector(catIds, catIdActive, function (e, values) {
            //console.info('onChange', e, values);
            $('input:hidden[name=catIds]').val(values);
          },{
          
            // children checked/unchecked if true
            checkWithParent: true,
          
            // title with 'title1 - title 2' if true
            titleWithParent: false,
          
            // when item click, only view leaf title if true
            notViewClickParentTitle: false,
          
            // disable the plugin
            disabled: false,
          
            // placeholder if empty
            emptyOptonPlaceholder: 'no options'
            
          })
    }
});
$.ajax({
  url: site_url + 'marketing/coupons/getCities',
  type: 'get',
  data: {},
  success: function(response) {
    response = JSON.parse(response);
    jQuery.each(response.data, function(index, item) {
      let cityId = {id: item.id,title: item.name, value: item.id,children:item.children}
      cityIds[0].children.push(cityId);
    })
    $('#treeSelectorCity').treeSelector(cityIds, cityIdActive, function (e, values) {
      //console.info('onChange', e, values);
      $('input:hidden[name=cityIds]').val(values);
    },{
    
      // children checked/unchecked if true
      checkWithParent: true,
    
      // title with 'title1 - title 2' if true
      titleWithParent: false,
    
      // when item click, only view leaf title if true
      notViewClickParentTitle: false,
    
      // disable the plugin
      disabled: false,
    
      // placeholder if empty
      emptyOptonPlaceholder: 'Pilih Kota'
      
    })
    $('#treeSelectorCity .treeSelector-wrapper').prepend('<input type="text" class="form-control" id="search_city" placeholder="Cari Kota..." style="margin: 10px 0px">')
    $('#search_city').on('keyup', function() {
      let $this = $(this);
      let exp = new RegExp($this.val(), 'i');
      $('#treeSelectorCity .treeSelector-li-title-box span').each(function() {
        var $self = $(this);
        if(!exp.test($self.text())) {
          $self.parent().hide();
        } else {
          $self.parent().show();
        }
      })
    })
  }
})

$.ajax({
  url: site_url + 'marketing/coupons/getProducts',
  type: 'get',
  data: {},
  success: function (response) {
      response = JSON.parse(response);
      jQuery.each(response.data, function(index, item) {
          let productId = {id: item.id,title: item.name, value: item.value}
          productIds[0].children.push(productId);
          // do something with `item` (or `this` is also `item` if you like)
      });
      $('#treeSelectorProducts').treeSelector(productIds, productIdActive, function (e, values) {
          //console.info('onChange', e, values);
          $('input:hidden[name=product_id]').val(values);
        },{
        
          // children checked/unchecked if true
          checkWithParent: true,
        
          // title with 'title1 - title 2' if true
          titleWithParent: false,
        
          // when item click, only view leaf title if true
          notViewClickParentTitle: false,
        
          // disable the plugin
          disabled: false,
        
          // placeholder if empty
          emptyOptonPlaceholder: 'no options'
          
        })
        $('#treeSelectorProducts .treeSelector-wrapper').prepend('<input type="text" class="form-control" id="search_product" placeholder="Cari Produk..." style="margin: 10px 0px">')
        $('#search_product').on('keyup', function() {
          let $this = $(this);
          let exp = new RegExp($this.val(), 'i');
          $('#treeSelectorProducts .treeSelector-li-title-box span').each(function() {
            var $self = $(this);
            if(!exp.test($self.text())) {
              $self.parent().hide();
            } else {
              $self.parent().show();
            }
          })
        })
  }
});

$(function() {
  if (category_type == 1) {
    $('.category-wrapper').show();
    $('.product-wrapper').hide();
    $('#for_product option[value=0]').removeAttr('disabled');
  } else if(product_type == 1) {
    $('.product-wrapper').show();
    $('.category-wrapper').hide();
    $('#for_product option[value=0]').attr('disabled', true);
  } else {
    $('.product-wrapper').hide();
    $('.category-wrapper').hide();
  }
  $('#select_type').on('change', function() {
    let value = $(this).val();
    if(value == 'category') {
      $('.category-wrapper').show();
      $('.product-wrapper').hide();
      $('#for_product option[value=0]').removeAttr('disabled')
    } else if(value == 'product') {
      $('.category-wrapper').hide();
      $('.product-wrapper').show();
      $('#for_product').val(1);
      $('#for_product option[value=0]').attr('disabled', true);
    } else {
      $('.category-wrapper').hide();
      $('.product-wrapper').hide();
    }
  })
})

// $.ajax({
//   url: site_url + 'marketing/coupons/getZona',
//   type: 'get',
//   data: {},
//   success: function (response) {
//       response = JSON.parse(response);
//       jQuery.each(response.data, function(index, item) {
//           let zonaId = {id: item.id,title: item.name, value: item.id,children:[]}
//           zonaIds[0].children.push(zonaId);
//           // do something with `item` (or `this` is also `item` if you like)
//       });
//       $('#treeSelectorZona').treeSelector(zonaIds, zonaIdActive, function (e, values) {
//           console.info('onChange', e, values);
//           $('input:hidden[name=zonaIds]').val(values);
//         },{
        
//           // children checked/unchecked if true
//           checkWithParent: true,
        
//           // title with 'title1 - title 2' if true
//           titleWithParent: false,
        
//           // when item click, only view leaf title if true
//           notViewClickParentTitle: false,
        
//           // disable the plugin
//           disabled: false,
        
//           // placeholder if empty
//           emptyOptonPlaceholder: 'no options'
          
//         })
//   }
// });

function chose_image() {
    $('#gambar_input').click();
}

function change_image(val) {
  if (val.files && val.files[0]) {
    
    var FR= new FileReader();
    
    FR.addEventListener("load", function(e) {
      $('#img').show();
      document.getElementById("img").src       = e.target.result;
      $('#base64_image').val(e.target.result);
    }); 
    
    FR.readAsDataURL( val.files[0] );
  }
}