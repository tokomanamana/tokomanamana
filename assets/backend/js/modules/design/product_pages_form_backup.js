$(function() {
	$('#set_by').on('change', function() {
		$('#checkbox_value').html('');
		let value = $(this).val();
		if(value == 'category') {
			$.ajax({
				url: site_url + 'design/product_pages/get_product_category',
				data: {},
				success: function(data) {
					data = JSON.parse(data);
					$.each(data, function(i, val) {
						$('#checkbox_value').append('<div class="checkbox"><label><input type="checkbox" name="product[]" value="' + val.id + '">' + val.category_name + '</label></div>');
					});
				}
			});
		} else {
			$.ajax({
				url: site_url + 'design/product_pages/get_all_product',
				data: {},
				success: function(data) {
					data = JSON.parse(data);
					$.each(data, function(i, val) {
						$('#checkbox_value').append('<div class="checkbox"><label><input type="checkbox" name="product[]" value="' + val.id + '">' + val.name + '</label></div>');
					});
				}
			})
		}
	});
	$('#search_product').on('keyup', function(e) {
		var $this = $(this);
    	var exp = new RegExp($this.val(), 'i');
    	$('#checkbox_value .checkbox label').each(function() {
    		var $self = $(this);
		      if(!exp.test($self.text())) {
		        $self.parent().hide();
		      } else {
		        $self.parent().show();
		      }
    	})
	})
});

function delete_image() {
	swal({
	  title: "Apakah ingin dihapus?",
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonClass: "btn-danger",
	  confirmButtonText: "Ya, hapus!",
	  cancelButtonText: "Batal!",
	  closeOnConfirm: false,
	  closeOnCancel: true
	},
	function(isConfirm) {
	  if (isConfirm) {
	  	swal("Berhasil!", "Gambar berhasil dihapus!.", "success");
	  	let html = `<div class="col-md-3">
                                            <input type="file" class="form-control" name="image">
                                        </div>`;
	    $('#image').html('');
	    $('#image').append(html);
	  }
	});
}