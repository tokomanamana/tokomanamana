$(function () {
    
    // $('.listbox').bootstrapDualListbox({
    //     nonSelectedListLabel: 'Tidak dipilih',
    //     selectedListLabel: 'Dipilih',
    //     infoText: 'Total {0}',
    //     infoTextEmpty: '',
    //     filterPlaceHolder: 'Cari...',
    // });
    $('#add-image-btn').on('click', function() {
        $('#banner_image').click();
    });
    $('#btn-edit-image').on('click', function() {
        $('#banner_image').click();
    });
    $('#btn-delete-image').on('click', function() {
        swal({
            title: 'Apakah ingin menghapus gambar?',
			// text: 'Gambar akan dihapus secara permanen',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#DD6B55',
			cancelButtonText: 'Tidak',
			confirmButtonText: 'Iya',
			closeOnConfirm: true
        }, function(res) {
            if(res == true) {
				// let image_name = $('#image_name').val();
				// $.ajax({
				// 	type: 'POST',
				// 	url: site_url + 'design/banners/delete_image',
				// 	data: {
				// 		image_name: image_name
				// 	},
				// 	success: function(data) {
				// 		let html = '<input type="hidden" name="image" id="image_name" value="">';
				// 		$('#image-container').html(html);
				// 		$('#add-image-btn').show();
                //         $('#btn-edit-image').hide();
                //         $('#btn-delete-image').hide();

				// 		$('#banner_image').val('');
				// 	}
                // })
                let html = '<input type="hidden" name="image" id="image_name" value="">';
                $('#image-container').html(html);
                $('#add-image-btn').show();
                $('#btn-edit-image').hide();
                $('#btn-delete-image').hide();

                $('#banner_image').val('');
			}
        })
    })
});

$(document).on('change', '#banner_image', function() {
    let id_brand = $('#id_brand').val();
    let image = $("#banner_image").prop('files')[0] ;
    let image_name = $("#banner_image").prop('files')[0].name ;
    let last_dot = image_name.lastIndexOf('.');
    let extension = image_name.substring(last_dot + 1);
    let random_number = Math.floor(Math.random() * 100000);
    let file_name = 'banner_home_' + random_number + '_' + Date.now();
    let full_file_name = file_name + '.' + extension;
    let image_name_temp = $('#image_name').val();

    let data = new FormData();
    data.append('banner_image', image);
    data.append('id_brand', id_brand);
    data.append('image_name', full_file_name);
    data.append('image_name_temp', image_name_temp);

    $.ajax({
    type: "POST",
    url: site_url + 'design/banners/image',
    data: data,
    cache: false,
    contentType: false,
    processData: false,

    success: function(data) 
    {
        data = JSON.parse(data);
        if(data.status == 'error'){
            let error = data.message.substring(3,data.indexOf('</p>'));
            swal({
                title: 'Peringatan',
                text: error,
                type: "warning",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: 'Ok',
                closeOnConfirm: true
            });
        }
        else{
            let html = `<div class="col-md-3" id="image-preview">
                    <div class="thumbnail">
                        <div class="thumb">
                            <img src="${site_url + '../files/images/' + data.message}">
                        </div>
                    </div>
                </div>
                <input type="hidden" name="image" id="image_name" value="${data.message}">`;
            $('#image-container').html(html);
            $('#add-image-btn').hide();
            $('#btn-edit-image').show();
            $('#btn-delete-image').show();
        }
    }
});
})