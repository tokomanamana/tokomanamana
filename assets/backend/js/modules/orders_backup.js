$(function () {
    $(".datepicker").datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
    });

    $('.custom-select').select2({
        minimumResultsForSearch: Infinity
    });
    
    var table = $('#table_order').DataTable({
        stateSave: ($('#table_order').data('state-save')) ? $('#table_order').data('state-save') : true,
        processing: true,
        serverSide: true,
        ajax: {
            url: $('#table_order').attr('data-url'),
            type: 'post',
            data: function (d) {
                d.filter = get_form_data($('#filter-form'));
                return d;
            }
        },
        autoWidth: false,
        columnDefs: [
            {
                orderable: false,
                targets: 'no-sort'
            },
            {
                className: 'text-center',
                targets: 'text-center'
            },
            {
                className: 'text-right',
                targets: 'text-right'
            }
        ],
        order: $('th.default-sort').length? [[$('th.default-sort').index(), $('th.default-sort').attr('data-sort')]]:false,
        dom: '<"datatable-header"fBl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            processing: lang.table.processing,
            zeroRecords: lang.table.zero_records,
            info: lang.table.info,
            infoEmpty: lang.table.info_empty,
            infoFiltered: lang.table.info_filtered,
            search: '<span>' + lang.table.search + ':</span> _INPUT_',
            searchPlaceholder: lang.table.search_placeholder,
            lengthMenu: '<span>' + lang.table.length_menu + ':</span> _MENU_',
            paginate: {'first': lang.table.first, 'last': lang.table.last, 'next': '&rarr;', 'previous': '&larr;'}
        },
    });

    table.on('click', '.paid', function (e) {
        var url = $(this).attr('href');
        var split = url.split("/");
        var id = split[6];
        e.preventDefault();
        // swal({
        //     title: lang.message.confirm.paid,
        //     type: "warning",
        //     showCancelButton: true,
        //     confirmButtonColor: "#EF5350",
        //     confirmButtonText: lang.button.yes,
        //     cancelButtonText: lang.button.cancel,
        //     closeOnConfirm: false
        // }, function () {
        //     $.ajax({
        //         url: url,
        //         success: function (data) {
        //             data = JSON.parse(data);
        //             table.ajax.reload();
        //             swal(data.message, '', data.status);
        //         }
        //     });
        // });
        $.ajax({
            url: site_url + '/orders/send_otp',
            data: {
                id: id
            },
            type: 'post',
            success: function(data) {
                if(data == 'true'){
                    console.log('OTP Code sent successfully');
                    swal({
                        title: 'Konfirmasi Kode OTP',
                        text: 'Kode OTP berhasil terkirim. Silahkan Masukkan Kode OTP Super Admin!',
                        type: 'input',
                        showCancelButton: true,
                        showCancelButton: true,
                        confirmButtonColor: "#EF5350",
                        confirmButtonText: 'Submit',
                        cancelButtonText: lang.button.cancel,
                        closeOnConfirm: false,
                        inputPlaceholder: "Kode OTP",
                        // inputType: "password"
                    }, function(inputValue) {
                        if (inputValue === false) return false;
                        if (inputValue === "") {
                            swal.showInputError("Kode OTP tidak boleh kosong!");
                            return false
                        }
                        $.ajax({
                            url: site_url + '/orders/check_otp_code',
                            data: {
                                id : id,
                                otp_code: inputValue
                            },
                            type: 'post',
                            success: function(data) {
                                if (data == 'true') {
                                    swal.close();
                                    $.ajax({
                                        url: url,
                                        success: function (data) {
                                            data = JSON.parse(data);
                                            table.ajax.reload();
                                            swal(data.message, '', data.status);
                                        }
                                    });
                                } else {
                                    swal.showInputError("Kode OTP salah!");
                                    return false
                                }
                            }
                        })
                    })
                } else {
                    swal("Terjadi Kesalahan", "Kode OTP gagal terkirim", "error");
                }
                                
            }
        })
    });

    table.on('click', '.delete', function (e) {
        var url = $(this).attr('href');
        var split = url.split("/");
        var id = split[6];
        e.preventDefault();
        // swal({
        //     title: lang.message.confirm.paid,
        //     type: "warning",
        //     showCancelButton: true,
        //     confirmButtonColor: "#EF5350",
        //     confirmButtonText: lang.button.yes,
        //     cancelButtonText: lang.button.cancel,
        //     closeOnConfirm: false
        // }, function () {
        //     $.ajax({
        //         url: url,
        //         success: function (data) {
        //             data = JSON.parse(data);
        //             table.ajax.reload();
        //             swal(data.message, '', data.status);
        //         }
        //     });
        // });
        $.ajax({
            url: site_url + 'orders/send_otp',
            data: {
                id: id
            },
            type: 'post',
            success: function(data) {
                if(data == 'true'){
                    console.log('OTP Code sent successfully');
                    swal({
                        title: 'Konfirmasi Kode OTP',
                        text: 'Kode OTP berhasil terkirim. Silahkan Masukkan Kode OTP Super Admin!',
                        type: 'input',
                        showCancelButton: true,
                        showCancelButton: true,
                        confirmButtonColor: "#EF5350",
                        confirmButtonText: 'Submit',
                        cancelButtonText: lang.button.cancel,
                        closeOnConfirm: false,
                        inputPlaceholder: "Kode OTP",
                        // inputType: "password"
                    }, function(inputValue) {
                        if (inputValue === false) return false;
                        if (inputValue === "") {
                            swal.showInputError("Kode OTP tidak boleh kosong!");
                            return false
                        }
                        $.ajax({
                            url: site_url + '/orders/check_otp_code',
                            data: {
                                id : id,
                                otp_code: inputValue
                            },
                            type: 'post',
                            success: function(data) {
                                if (data == 'true') {
                                    swal.close();
                                    $.ajax({
                                        url: url,
                                        success: function (data) {
                                            data = JSON.parse(data);
                                            table.ajax.reload();
                                            swal(data.message, '', data.status);
                                        }
                                    });
                                } else {
                                    swal.showInputError("Kode OTP salah!");
                                    return false
                                }
                            }
                        })
                    })
                } else {
                    swal("Terjadi Kesalahan", "Kode OTP gagal terkirim", "error");
                }
                                
            }
        })
    })

    $('<div class="datatable_content" style="display: inline-block; float: right;"></div>').appendTo('.datatable-header');

    $('.dataTables_length').appendTo('.datatable_content');
    $('.dataTables_length').css('float', 'none');

    // load_filter();

    $('#filter').on('click', function () {
        table.draw();
    });

    $('#filter-by').on('change', function () {
        var a = $(this).val();
        var now = new Date();
        $('input[name=from]').datepicker('destroy');
        $('input[name=to]').datepicker('destroy');
        $('input[name=from]').attr('readonly', 'readonly');
        $('input[name=to]').attr('readonly', 'readonly');
        if (a == 'today') { //hari ini
            $('input[name=from]').val(formatDate(now));
            $('input[name=to]').val(formatDate(now));
        } else if (a == 'yesterday') { //kemarin
            var date = new Date();
            date.setDate(now.getDate() - 1);
            $('input[name=from]').val(formatDate(date));
            $('input[name=to]').val(formatDate(date));
        } else if (a == 'this month') { //bulan ini
            var date1 = new Date(now.getFullYear(), now.getMonth(), 1);
            var date2 = new Date(now.getFullYear(), now.getMonth() + 1, 0);
            $('input[name=from]').val(formatDate(date1));
            $('input[name=to]').val(formatDate(date2));
        } else if (a == 'last month') { //bulan lalu
            var date1 = new Date(now.getFullYear(), now.getMonth() - 1, 1);
            var date2 = new Date(now.getFullYear(), now.getMonth(), 0);
            $('input[name=from]').val(formatDate(date1));
            $('input[name=to]').val(formatDate(date2));
        } else if (a == 'this year') { //tahun ini
            var date1 = new Date(now.getFullYear(), 0, 1);
            var date2 = new Date(now.getFullYear(), 12, 0);
            $('input[name=from]').val(formatDate(date1));
            $('input[name=to]').val(formatDate(date2));
        } else if (a == 99) {
            $('input[name=from]').datepicker({format: 'yyyy-mm-dd'});
            $('input[name=from]').removeAttr('readonly');
            $('input[name=to]').datepicker({format: 'yyyy-mm-dd'});
            $('input[name=to]').removeAttr('readonly');
        }
    });

    getFilterOrder();

})

function load_filter() {
    $.ajax({
        url: site_url + 'orders/orders/get_all_merchant',
        data: {},
        success: function (data) {
            $('.datatable-header .datatable_content').append(data);
        }
    });
}

// function set_datatable() {
//     var table = $('#table').DataTable();
//     table.draw();
// }

function getFilterOrder () {
    let filter = JSON.parse(localStorage.getItem('filterOrder'));
    let monthSales = JSON.parse(localStorage.getItem('monthSales'))
    if(filter) {
        if (filter[0] !== null || filter[0] !== undefined) {
            let field = $('form#filter-form');
            if (field.length > 0) {
                field.find('input[name=from]').val(filter[1]);
                field.find('input[name=to]').val(filter[2]);
                field.find('select[name=payment_status]').val(filter[0]);
                field.find('#filter-by').val(filter[3]);
                $('#filter').trigger('click');
                localStorage.removeItem('filterOrder');
            }
        }
    }
    if (monthSales) {
        if (monthSales[0] !== null || monthSales[0] !== undefined) {
            let field = $('form#filter-form')
            if (field.length > 0) {
                field.find('input[name=from]').val(monthSales[1])
                field.find('input[name=to]').val(monthSales[2])
                field.find('select[name=payment_status]').val(monthSales[0])
                field.find('#filter-by').val(monthSales[3])
                $('#filter').trigger('click')
                localStorage.removeItem('monthSales')
            }
        }
    }
}

function formatDate(date) {
    var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}
function get_form_data($form){
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });
    return indexed_array;
}