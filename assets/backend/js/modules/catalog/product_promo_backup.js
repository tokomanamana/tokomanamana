$('form#promo-settings').on('click', 'button', function (e) {
    e.preventDefault();
    let value = $('form#promo-settings').find('select').val();
    $.ajax({
        url: site_url + 'catalog/products/set_sort_promo',
        data: { code: value },
        method: 'POST',
        type: 'POST',
        success: function (res) {
            let retrn = JSON.parse(res);
            if (retrn.status = 'success') {
                swal('Berhasil.', '', 'success');
            } else {
                swal('Gagal.', retrn.desc, 'error');
            }
        }
    })
});

$(function() {
    $('#promo').select2({
        minimumResultsForSearch: Infinity
    });
})