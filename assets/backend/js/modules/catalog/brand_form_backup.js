$(function () {
    // $('body').on('click', '#edit-image-btn', function () {
    //     $('#image-preview').remove();
    //     $('#image').val('');
    // });
    $('body').on('click', '#delete-image-btn', function () {
        $('#image').val('');
        $('#image-preview').remove();
        $('#add-image-btn').show();
        $('#edit-image-btn').hide();
        $('#delete-image-btn').hide();
    });
    $('.status-brand .bootstrap-switch-container').on('click', function() {
        alert('k');
    });
    $('#add-image-btn').on('click', function() {
        $('#brand_image').click();
    });
    $('#btn-edit-image').on('click', function() {
        $('#brand_image').click();
    })
    $('#btn-delete-image').on('click', function() {
        swal({
            title: 'Apakah ingin menghapus gambar?',
			// text: 'Gambar akan dihapus secara permanen',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#DD6B55',
			cancelButtonText: 'Tidak',
			confirmButtonText: 'Iya',
			closeOnConfirm: true
        }, function(res) {
            if(res == true) {
				// let image_name = $('#image_name').val();
				// $.ajax({
				// 	type: 'POST',
				// 	url: site_url + 'catalog/brands/delete_image',
				// 	data: {
				// 		image_name: image_name
				// 	},
				// 	success: function(data) {
				// 		let html = '<input type="hidden" name="image" id="image_name" value="">';
				// 		$('#image-container').html(html);
				// 		$('#add-image-btn').show();
                //         $('#btn-edit-image').hide();
                //         $('#btn-delete-image').hide();

				// 		$('#brand_image').val('');
				// 	}
                // })
                let html = '<input type="hidden" name="image" id="image_name" value="">';
                $('#image-container').html(html);
                $('#add-image-btn').show();
                $('#btn-edit-image').hide();
                $('#btn-delete-image').hide();

                $('#brand_image').val('');
			}
        })
    })
});
function responsive_filemanager_callback(field_id) {
    if (field_id) {
        var url = $('#' + field_id).val();
        var username = $('#username').val();
        var final_url = 'principal/' + url;
        var html = '<div class="col-md-3" id="image-preview">';
        html += '<div class="thumbnail"><div class="thumb">';
        html += '<img src="' + site_url + '../files/images/' + final_url + '">';
        html += '</div></div></div>';
        $(html).insertBefore('#add-image');
        $('#add-image-btn').hide();
        $('#edit-image-btn').show();
        $('#delete-image-btn').show();
    }
}

$(document).on('change', '#brand_image', function() {
    let id_brand = $('#id_brand').val();
    let image = $("#brand_image").prop('files')[0] ;
    let image_name = $("#brand_image").prop('files')[0].name ;
    let last_dot = image_name.lastIndexOf('.');
    let extension = image_name.substring(last_dot + 1);
    let random_number = Math.floor(Math.random() * 100000);
    let file_name = 'brand_office_' + random_number + '_' + Date.now();
    let full_file_name = file_name + '.' + extension;
    let image_name_temp = $('#image_name').val();

    let data = new FormData();
    data.append('brand_image', image);
    data.append('id_brand', id_brand);
    data.append('image_name', full_file_name);
    data.append('image_name_temp', image_name_temp);

    $.ajax({
    type: "POST",
    url: site_url + 'catalog/brands/image',
    data: data,
    cache: false,
    contentType: false,
    processData: false,

    success: function(data) 
    {
        data = JSON.parse(data);
        if(data.status == 'error'){
            let error = data.message.substring(3,data.indexOf('</p>'));
            swal({
                title: 'Peringatan',
                text: error,
                type: "warning",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: 'Ok',
                closeOnConfirm: true
            });
        }
        else{
            let html = `<div class="col-md-3" id="image-preview">
                    <div class="thumbnail">
                        <div class="thumb">
                            <img src="${site_url + '../files/images/' + data.message}">
                        </div>
                    </div>
                </div>
                <input type="hidden" name="image" id="image_name" value="${data.message}">`;
            $('#image-container').html(html);
            $('#add-image-btn').hide();
            $('#btn-edit-image').show();
            $('#btn-delete-image').show();
        }
    }
});
})