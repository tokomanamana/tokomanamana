$(function () {
    $('.number').number(true, 0, ',', '.');
    $('.number').attr('autocomplete', 'off');
    $('.bootstrap-select').selectpicker();
    $('.btn-edit').on('click', function () {
        $('.modal').modal('show');
        if (!$("form-group").hasClass("has-errors") && !$("div").hasClass("alert")) {
            $("form")[0].reset();
        }
        $.post(site_url + 'catalog/products/get_combination', {id: $(this).attr('data-id')}, function (data) {

        });
    });
    $('.btn-delete').on('click', function () {
        var id = $(this).attr('data-id');
        swal({
            title: lang.message.confirm.delete_combination,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: lang.button.delete,
            cancelButtonText: lang.button.cancel,
            closeOnConfirm: false,
            closeOnCancel: false
        }, function () {
            $('#row-' + id).remove();
            $.post(site_url + 'catalog/products/delete_combination', {id: id});
            if (!$("input:radio[name='default']").is(":checked")) {
                $("input:radio[name='default']").prop('checked', true);
                $.post(site_url + 'catalog/products/default_combination', {id: $("input:radio[name='default']:checked").val()});
            }
            swal("Deleted!", lang.message.confirm.delete_combination_success, "success");
        });
    });
    $("input:radio[name='default']").on('change', function () {
        var id = $(this).val();
        $('#default-' + id).prop('checked', true);
        $.post(site_url + 'catalog/products/default_combination', {id: id});
    });
    $("#form").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        // Different components require proper error label placement
        errorPlacement: function (error, element) {
            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                } else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function (label) {
            label.remove();
            label.closest('.form-group').removeClass('has-error');
        },
        rules: {
            code: {
                required: true
            }
        },
        submitHandler: function (form) {
            $(form).ajaxSubmit({
                success: function (data) {
                    var response = JSON.parse(data);
                    if (response.status == 'success') {
                        swal({
                            title: "Good job!",
                            text: response.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success"
                        }, function () {
                            $('.modal').modal('hide');
                            $('#table tbody').append(response.data);
                        });
                    } else {
                        swal({
                            title: "Oops...",
                            text: response.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                        });
                    }
                }
            });
            return false;
        }
    });
});
function responsive_filemanager_callback(field_id) {
    if (field_id) {
        var id = Math.random().toString(36).slice(2);
        var url = $('#' + field_id).val();
        var html = '<div class="col-lg-3 col-sm-4" id="' + id + '">';
        html += '<div class="thumbnail"><div class="thumb">';
        html += '<img src="' + site_url + '../files/images/' + url + '">';
        html += '<input type="hidden" name="images[' + id + ']" value="' + url + '">';
        html += '</div>';
        html += '<div class="row">';
        html += '<div class="col-md-8">';
        html += '<div class="radio"><label><input type="radio" value="' + id + '" name="image_primary" id="image_primary' + id + '">Gambar Utama</label></div>';
        html += '</div>';
        html += '<div class="col-md-4">';
        html += '<div class="radio"><a href="javascript:delete_image(\'' + id + '\')" class="text-muted pull-right"><i class="icon-trash"></i></a></div>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        $(html).insertBefore('#add-image');
        if (!$("input:radio[name='image_primary']").is(":checked")) {
            $('#image_primary' + id).prop('checked', true);
        }
    }
}

function delete_image(id) {
    swal({
        title: lang.message.confirm.delete_image,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: lang.button.delete,
        cancelButtonText: lang.button.cancel,
        closeOnConfirm: false,
        closeOnCancel: false
    }, function () {
        $('#' + id).remove();
        if (!$("input:radio[name='image_primary']").is(":checked")) {
            $("input:radio[name='image_primary']").prop('checked', true);
        }
        swal("Deleted!", lang.message.confirm.delete_image_success, "success");
    });
}

function add_feature() {
    var id = Math.random().toString(36).slice(2);
    var feature_row_html = feature_add_html.replace(/idf/g, id);
    $('#feature table tbody').append(feature_row_html);
    $('.bootstrap-select').selectpicker();
    $('#feature-select-' + id).on('change', function () {
        var feature = $(this).val();
        value_feature(feature, id);
    });
}

function delete_feature(id) {
    $('#row-' + id).remove();
}

function value_feature(feature, id) {
    $.ajax(site_url + 'catalog/products/get_feature_values/' + feature + '/' + id).done(function (response) {
        $('#feature-value-' + id).html(response);
    });
}