$(function () {
    // $('body').on('click', '#edit-image-btn', function () {
    //     $('#image-preview').remove();
    // });
    // $('body').on('click', '#delete-image-btn', function () {
    //     $('#image').val('');
    //     $('#image-preview').remove();
    //     $('#add-image-btn').show();
    //     $('#edit-image-btn').hide();
    //     $('#delete-image-btn').hide();
    // });
    // $('body').on('click', '#edit-banner-btn', function () {
    //     $('#banner-preview').remove();
    // });
    // $('body').on('click', '#delete-banner-btn', function () {
    //     $('#banner').val('');
    //     $('#banner-preview').remove();
    //     $('#add-banner-btn').show();
    //     $('#edit-banner-btn').hide();
    //     $('#delete-banner-btn').hide();
    // });
    $('.listbox').bootstrapDualListbox({
        nonSelectedListLabel: 'Tidak dipilih',
        selectedListLabel: 'Dipilih',
        infoText: 'Total {0}',
        infoTextEmpty: '',
        filterPlaceHolder: 'Cari...',
    });
    $('body').on('click', '#add-image-btn', function() {
        $('#icon_image').click()
    })
    $('body').on('click', '#btn-edit-image', function() {
        $('#icon_image').click()
    })
    $('body').on('click', '#btn-delete-image', function() {
        swal({
            title: 'Apakah ingin menghapus gambar?',
			// text: 'Gambar akan dihapus secara permanen',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#DD6B55',
			cancelButtonText: 'Tidak',
			confirmButtonText: 'Iya',
			closeOnConfirm: true
        }, function(res) {
            if (res == true) {
                $('#btn-delete-image').hide()
                $('#btn-edit-image').hide()
                $('#image-preview').hide()
                $('#add-image-btn').show()
                $('#image_name').val('')
                $('#icon_image').val('')
            }
        })
    })
    $('body').on('click', '#add-banner-btn', function() {
        $('#banner_image').click()
    })
    $('body').on('click', '#btn-edit-banner', function() {
        $('#banner_image').click()
    })
    $('body').on('click', '#btn-delete-banner', function() {
        swal({
            title: 'Apakah ingin menghapus gambar?',
            type: 'warning',
            showCancelButton: true,
			confirmButtonColor: '#DD6B55',
			cancelButtonText: 'Tidak',
			confirmButtonText: 'Iya',
			closeOnConfirm: true
        }, function(res) {
            if (res == true) {
                $('#btn-delete-banner').hide()
                $('#btn-edit-banner').hide()
                $('#banner-preview').hide()
                $('#add-banner-btn').show()
                $('#banner').val('')
                $('#banner_image').val('')
            }
        })
    })
});
// function responsive_filemanager_callback(field_id) {
//     if (field_id) {
//         var url = $('#' + field_id).val();
//         var html = '<div class="col-sm-1" id="'+field_id+'-preview">';
//         html += '<div class="thumbnail"><div class="thumb">';
//         html += '<img src="' + site_url + '../files/images/' + url + '">';
//         html += '</div></div></div>';
//         $(html).insertBefore('#add-'+field_id);
//         $('#add-' + field_id + '-btn').hide();
//         $('#edit-' + field_id + '-btn').show();
//         $('#delete-' + field_id + '-btn').show();
//     }
// }

function change_image(val) {
    if (val.files && val.files[0]) {
    
        const filename = $("#icon_image").prop('files')[0].name;
        const ext = getFileExtension(filename)
        if (ext == 'jpg' || ext == 'jpeg' || ext == 'png') {
            var FR= new FileReader();
            
            FR.addEventListener("load", function(e) {
                $('#image-preview').show()
                document.getElementById("img").src       = e.target.result;
                $('#image_name').val(filename)
            }); 
            
            FR.readAsDataURL( val.files[0] );

            $('#add-image-btn').hide()
            $('#btn-edit-image').show()
            $('#btn-delete-image').show()
        } else {
            swal({
                title: 'Peringatan',
                text: 'Hanya boleh file bergambar',
                type: "warning",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: 'Ok',
                closeOnConfirm: true
            });
        }
      
    }
}

function change_banner(val) {
    if (val.files && val.files[0]) {
        const filename = $("#banner_image").prop('files')[0].name;
        const ext = getFileExtension(filename)
        if (ext == 'jpg' || ext == 'jpeg' || ext == 'png') {
            var FR= new FileReader();
            
            FR.addEventListener("load", function(e) {
                $('#banner-preview').show()
                document.getElementById("img_banner").src       = e.target.result;
                $('#banner').val(filename)
            }); 
            
            FR.readAsDataURL( val.files[0] );

            $('#add-banner-btn').hide()
            $('#btn-edit-banner').show()
            $('#btn-delete-banner').show()
        } else {
            swal({
                title: 'Peringatan',
                text: 'Hanya boleh file bergambar',
                type: "warning",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: 'Ok',
                closeOnConfirm: true
            });
        }
    }
}

function getFileExtension(filename) {
    return filename.split('.').pop();
}