$(function () {
    $('.select-type').on('change',function(){
        var val = $(this).val();
        var i = $(this).data('footer');
        if(val == 'text'){
            $('#footer-'+i+' .footer-content-list').hide();
            $('#footer-'+i+' .footer-content-text').show();
            $('#footer-'+i+' .footer-content-text textarea').removeAttr('disabled');
        }else{
            $('#footer-'+i+' .footer-content-list').show();
            $('#footer-'+i+' .footer-content-text').hide();
            $('#footer-'+i+' .footer-content-text textarea').attr('disabled','disabled');
        }
    })
    $('.btn-add-link').on('click', function () {
        var i = $(this).data('footer');
        var j = $(this).data('last-link');
        var html = '<div class="row mb-5" id="footer-' + i + '-link-' + j + '">' +
                '<div class="col-sm-4">' +
                '<input type="text" class="form-control" name="footer[' + i + '][content][' + j + '][title]" placeholder="Title">' +
                '</div>' +
                '<div class="col-sm-7">' +
                '<input type="text" class="form-control" name="footer[' + i + '][content][' + j + '][url]" placeholder="URL">' +
                '</div>' +
                '<div class="col-sm-1">' +
                '<a href="javascript:delete_footer_link(' + i + ',' + j + ')"><i class="icon-x"></i></a>' +
                '</div>' +
                '</div>';
        $('#footer-' + i + ' .footer-content-list .button').before(html);
        j = j+1;
        $('#footer-' + i + ' .footer-content-list .btn-add-link').data('last-link', j);
    });
});

function delete_footer_link(i, j) {
    $('#footer-' + i + '-link-' + j).remove();
}