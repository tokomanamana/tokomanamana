$(function() {
	jQuery.datetimepicker.setLocale('id');
	$('.datetimepicker').datetimepicker({
		timepicker: true,
		format: 'Y-m-d H:i:s',
		lang: 'id',
		minDate:'-1970/01/01',
		allowTimes:[
		  '00:00:00', '01:00:00', '02:00:00', '03:00:00', 
		  '04:00:00', '05:00:00', '06:00:00', '07:00:00', 
		  '08:00:00', '09:00:00', '10:00:00', '11:00:00',
		  '12:00:00', '13:00:00', '14:00:00', '15:00:00',
		  '16:00:00', '17:00:00', '18:00:00', '19:00:00',
		  '20:00:00', '21:00:00', '22:00:00', '23:00:00',
		  '23:59:59'
		 ]
	});
	$('#btn-delete-image').on('click', function() {
		swal({
			title: 'Apakah ingin menghapus gambar?',
			text: 'Gambar akan dihapus secara permanen',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#DD6B55',
			cancelButtonText: 'Tidak',
			confirmButtonText: 'Iya',
			closeOnConfirm: true
		}, function(res) {
			if(res == true) {
				let image_name = $('#image').val();
				$.ajax({
					type: 'POST',
					url: site_url + 'settings/promotions/delete_image',
					data: {
						image_name: image_name
					},
					success: function(data) {
						let html = '<input type="hidden" name="gambar" id="image" value="">';
						$('#image_container').html(html);
						$('#upload-image-btn').show();
						$('#btn-delete-image').hide();

						$('#countdown_image').val('');
					}
				})
			}
		});
	})
});

function upload_image_btn() {
	$('#countdown_image').click();
}

$(document).on('change', '#countdown_image', function() {
    let id_countdown = $('#id_countdown').val();
    let image = $("#countdown_image").prop('files')[0] ;
    let image_name = $("#countdown_image").prop('files')[0].name ;
    let last_dot = image_name.lastIndexOf('.');
    let extension = image_name.substring(last_dot + 1);
    let random_number = Math.floor(Math.random() * 100000);
    let file_name = 'banner_countdown_' + random_number + '_' + Date.now();
    let full_file_name = file_name + '.' + extension;

    let data = new FormData();
    data.append('countdown_image', image);
    data.append('id_countdown', id_countdown);
    data.append('image_name', full_file_name);

    $.ajax({
    type: "POST",
    url: site_url + 'settings/promotions/image',
    data: data,
    cache: false,
    contentType: false,
    processData: false,

    success: function(data) 
    {
        data = JSON.parse(data);
        if(data.status == 'error'){
            let error = data.message.substring(3,data.indexOf('</p>'));
            swal({
                title: 'Peringatan',
                text: error,
                type: "warning",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: 'Ok',
                closeOnConfirm: true
            });
        }
        else{
            let html = `<div class="col-md-3" id="image-preview">
                    <div class="thumbnail">
                        <div class="thumb">
                            <img src="${site_url + '../files/images/' + data.message}">
                        </div>
                    </div>
                </div>
                <input type="hidden" name="gambar" id="image" value="${data.message}">`;
            $('#image_container').html(html);
			$('#upload-image-btn').hide();
			$('#btn-delete-image').show();
        }
    }
});
})