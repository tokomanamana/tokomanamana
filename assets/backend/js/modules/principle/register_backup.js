jQuery.validator.addMethod("letters_spaceonly", function(value, element) 
{
return this.optional(element) || /^[a-z ]+$/i.test(value);
}, "Harap masukan huruf dan spasi saja.");
jQuery.validator.addMethod("exactlength", function(value, element, param) {
    return this.optional(element) || value.length == param;
   }, $.validator.format("Harap masukkan {0} karakter."));

 function countdown(duration) {
    var countdown = setInterval(function () {
        if (--duration) {
            $('#btn-token').text('Kirim Ulang (' + duration + 's)');
        } else {
            clearInterval(countdown);
            $('#btn-token').attr('style', '');
            $('#btn-token').text('Request OTP');
        }
    }, 1000);
}

function agreeSnk() {
    $("input[name=agree]").prop("checked", true);
    $('#terms').trigger('click.dismiss.bs.modal');
}
function openSnKmodal() {
    $('#terms').modal('show');;
}
function closeSnKmodal() {
    $('#terms').trigger('click.dismiss.bs.modal');
}
function changeKnowFrom() {
    var val = $("select[name=know_from]").val();
    if(val == '19') {
        $('#custom_know_form').show();
    } else {
        $('#custom_know_form').hide();
    }
}
$(function () {
    $('#verification-code').hide();
    //var currentDate =  new Date(new Date().setFullYear(new Date().getFullYear() - 20));
    $.fn.datepicker.defaults.startView = 2;
    $.fn.datepicker.defaults.defaultViewDate= {year:1980, month:0, day:1};
    $('.datepicker').datepicker();
    $('#province').on('change', function () {
        $.get(site_url+'auth/cities/'+$(this).val(),function(res){
            $('#city').html(res);
            $('#district').html('<option></option>');
        });
    });
    $('#city').on('change', function () {
        $.get(site_url+'auth/districts/'+$(this).val(),function(res){
            $('#district').html(res);
        });
    });
    $.fn.stepy.defaults.legend = false;
    $.fn.stepy.defaults.backLabel = '<span><i class="icon-arrow-left13 position-left"></i> Kembali</span>';
    $.fn.stepy.defaults.nextLabel = '<span>Lanjutkan <i class="icon-arrow-right14 position-right"></i></span>';
    $(".stepy").stepy({
        validate: true,
        block: true,
        next: function (index) {
        //    if (!$(".stepy").validate(validate)) {
        //        return false
        //    }
        }
    });
    $('.stepy-step').find('.button-next').addClass('btn btn-primary');
    $('.stepy-step').find('.button-back').addClass('btn btn-default');

    $("#register").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        rules: {
            email: {
                required: true,
                email: true,
                remote: {
                    url: site_url+'auth/check_email_ajax/',
                    type: "post"
                }
            },
            company_name: {
                required: true,
                // remote: {
                //     url: site_url + 'auth/check_company_name',
                //     type: "post"
                // }
            },
            brand_name: {
                required: true,
                // remote: {
                //     url: site_url + 'auth/check_brand_name',
                //     type: "post"
                // }
            },
            owner_name: {
                required: true,
                letters_spaceonly: true
            },
            owner_phone: {
                required: true,
                digits: true
            },
            verified_phone: {
                required: true
            },
            password: {
                minlength: 8
            },
            password_confirm: {
                minlength: 8,
                equalTo: '#password'
            },
            know_from: {
                required: true
            },
            custom_know_form: {
                required: true
            },
            agree: {
                required: true
            },
            captcha: {
                required: true,
                digits: true
            },
            document: {
                required: true
            },
            brand_image: {
                required: true
            }
        },
        messages: {
            email: {
                remote: "Maaf, email ini sudah terdaftar!" 
            },
            brand_name: {
                remote: 'Maaf, nama brand sudah digunakan!'
            }
            // owner_id: {
            //     remote: "Maaf, No KTP ini sudah terdaftar" 
            // }
        }, 
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        // Different components require proper error label placement
        errorPlacement: function (error, element) {
            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    // error.appendTo(element.parent().parent().parent().parent());
                    error.appendTo(element.parent().parent().parent().parent().parent().parent().parent().children('.error-checkbox-document'));
                } else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            }


            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            } else if(element.parent().hasClass('custom-file')) {
                error.appendTo(element.parent().parent().children('.error-custom-file'));
            } else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function (label) {
            label.remove();
            label.closest('.form-group').removeClass('has-error');
        },
        submitHandler: function (form) {
            //$('#base64_img').val(encodeURIComponent(signaturePad.toDataURL().replace(/^data:image\/(png|jpg);base64,/, '')));
            // $(form).ajaxSubmit({
            //     success: function (data) {
            //         responseForm(data);
            //     }
            // });
            // //grecaptcha.reset()
            // return false;
        }
    });
    // $('.checkbox-inline').on('click', function () {
    //     $('.upload-files').html('');
    //     let value = [];
    //     let total = 0;
    //     $('.checkbox-inline :checkbox:checked').each(function (i, val) {
    //         value[i] = val;
    //         total++;
    //     });
    //     let data_html = '';
    //     data_html += '<div class="col-md-12 control-label">';
    //     data_html += 'Upload File (Max 2MB & JPG/PDF) *'
    //     data_html += '</div>';
    //     data_html += '<div class="custom-file col-md-12">';
    //     $.each(value, function(i) {
    //         data_html += '<input type="file" name="document_image[]" required>';
    //     })
    //     data_html += '</div>';
    //     $('.upload-files').html(data_html);

    // });
    $('#submit').on('click', function (e) {
        e.preventDefault();
        let link = $("#register").attr('action');
        let form_data = new FormData($(this).parents('form')[0]);
        $.ajax({
            url: link,
            method: 'post',
            data: form_data,
            xhr: function () {
                let myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status == 'success') {
                    swal({
                            html: true,
                            title: data.title + ' ' + data.message,
                            type: data.status,
                            showConfirmButton: true
                        },
                        function () {
                            document.location.href = site_url + 'auth/login';
                        });
                } else if (data.status == 'error_captcha') {
                    swal({
                        html: true,
                        title: data.message,
                        type: 'error'
                    });
                    $("#img_capt").html(data.image);
                    $('#register input[type=hidden]').val(data.cap_word);
                } else {
                    swal({
                        html: true,
                        title: data.message,
                        type: data.status
                    });
                }
            }
        });
    });
});
function setLocation(lat, lng) {
    $('#lat').val(lat);
    $('#lng').val(lng);
}
$(".toggle-password").click(function() {

    //$(this).toggleClass("icon-eye fa-eye-slash");
    var input = $('#password');
    if (input.attr("type") == "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
    }
  });
$(document).on("click", ".openOTPModal", function () {
    var mobileNumber = $('#owner_phone').val();
    $(".modal-body #inputNoHP").val(mobileNumber);
});
function requestOTPButton() {
    var phone_num = $('#owner_phone').val();
    if (phone_num == '') {
        swal({
            title: "Oops...",
            text: 'No Hp tidak boleh kosong',
            confirmButtonColor: "#EF5350",
            type: "error",
            html: true
        });
        return false;
    }
    $.post(
        site_url + 'auth/check_phone/', {
            'phone': phone_num
        },
        function (res) {
            if (res == 'false') {
                swal({
                    title: "Oops...",
                    text: 'No Hp ini sudah pernah digunakan',
                    confirmButtonColor: "#EF5350",
                    type: "error",
                    html: true
                });
            } else {
                $.post(
                    site_url + 'auth/request_token_principle/', {
                        'phone': phone_num
                    },
                    function (data) {
                        data = JSON.parse(data);
                        if (data.status == 'success') {
                            swal({
                                title: "Good job!",
                                text: data.message,
                                confirmButtonColor: "#66BB6A",
                                type: "success",
                                html: true
                            });
                            $('#verification-code').show();
                            $('#btn-token').attr('style', 'pointer-events: none;width: 100%;');
                            $('#btn-token').text('Kirim Ulang (120s)');
                            countdown(120);
                        } else {
                            swal({
                                title: "Oops...",
                                text: data.message,
                                confirmButtonColor: "#EF5350",
                                type: "error",
                                html: true
                            });
                        }
                    }
                )
            }
        });
}

function submitOTP() {
    $.post(
        site_url + 'auth/check_token_principal/', {
            'phone': $('#owner_phone').val(),
            'code': $('#otpCode').val()
        },
        function (data) {
            data = JSON.parse(data);
            if (data.status == 'success') {
                // $('#verif_icon').hide();
                // $('#verif_icon_ok').show();
                // $('#verified_phone').val('1');
                // $('#requestOTPModal').modal('hide');
                $('#btn-token').remove();
                $('#btn-token-submit').attr("disabled",true);
                $('#btn-token-submit').prop("disabled",true);
                $('#btn-token-submit').text("OTP Verified");
                $('#btn-token-submit').css({"background-color":"#28a745","color":"white","pointer-events":"none","cursor":"default"});
                $('.stepy-navigator').css("display","block");
            } else {
                swal({
                    title: "Oops...",
                    text: data.message,
                    confirmButtonColor: "#EF5350",
                    type: "error",
                    html: true
                });
            }
        }
    )
}

function document_principal(e) {
    let value = $(e).val();
    if($(e).prop('checked') == true) {
        let html = '<input type="file" class="uploader" name="document_file[' + value + ']" required accept="image/jpeg,application/pdf" onchange="file_validation(this)">';
        $('#' + value + '_document .custom-file').append(html);
    } else {
        
        $('#' + value + '_document .error-custom-file').children().remove();
        $('#' + value + '_document .custom-file input').remove();
    }
    $('.document-principal .control-label .upload-text').html('Upload File (Max 2MB & JPG/PDF) *');
}

function file_validation(e) {
    let image = $(e).val();
    image = image.split('.', 2);
    if(image[1] == 'jpg' || image[1] == 'pdf') {
    } else{
        swal({
            title: 'File yang harus diupload JPG/PDF!',
            type: 'error'
        });
        $(e).val('');
    }
}