$('#branchModal').on('hidden.bs.modal', function () {
    $("#branches_table").dataTable().fnDestroy();
    // do something…
})

function seeBranches(id) {
    var t = $('#branches_table').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "type" : "POST",
            "data": {id:id},
            "url" : site_url + 'principle/get_branches',
            "dataSrc": function ( json ) {
                //Make your callback here.
                $('#branchModal').modal('show');
                return json.data;
            }       
        },
    });
}

$(function() {
    var table = $('#table_principal').DataTable({
        stateSave: ($('#table_principal').data('state-save')) ? $('#table_principal').data('state-save') : true,
        processing: true,
        serverSide: true,
        ajax: {
            url: $('#table_principal').attr('data-url'),
            type: 'post',
            data: function (d) {
                d.additional_data = datatable_data;
                return d;
            }
        },
        autoWidth: false,
        columnDefs: [
            {
                orderable: false,
                targets: 'no-sort'
            },
            {
                className: 'text-center',
                targets: 'text-center'
            },
            {
                className: 'text-right',
                targets: 'text-right'
            }
        ],
        order: $('th.default-sort').length? [[$('th.default-sort').index(), $('th.default-sort').attr('data-sort')]]:false,
        dom: '<"datatable-header"fBl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            processing: lang.table.processing,
            zeroRecords: lang.table.zero_records,
            info: lang.table.info,
            infoEmpty: lang.table.info_empty,
            infoFiltered: lang.table.info_filtered,
            search: '<span>' + lang.table.search + ':</span> _INPUT_',
            searchPlaceholder: lang.table.search_placeholder,
            lengthMenu: '<span>' + lang.table.length_menu + ':</span> _MENU_',
            paginate: {'first': lang.table.first, 'last': lang.table.last, 'next': '&rarr;', 'previous': '&larr;'}
        },
    });
    table.on('click', '.delete', function(e) {
        var url = $(this).attr('href');
        e.preventDefault();
        
        swal({
            title: 'Masukkan Password',
            text: 'Harap masukkan password untuk menghapus principal',
            type: 'input',
            showCancelButton: true,
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: 'Submit',
            cancelButtonText: lang.button.cancel,
            closeOnConfirm: false,
            inputPlaceholder: "Password",
            inputType: "password"
        }, function(inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("Password tidak boleh kosong!");
                return false
            }
            $.ajax({
                url: site_url + '/orders/check_account',
                data: {
                    password: inputValue
                },
                type: 'post',
                success: function(data) {
                    if (data == 'true') {
                        swal.close();
                        $.ajax({
                            url: url,
                            success: function (data) {
                                data = JSON.parse(data);
                                table.ajax.reload();
                                swal(data.message, '', data.status);
                            }
                        });
                    } else {
                        swal.showInputError("Password salah!");
                        return false
                    }
                }
            })
        })
    })
})