$(function () {
    $('.add-file-btn').on('click', function () {
        let input_file = '<input type="file" name="other_document_image[]">';
        $('.custom-file').append(input_file);
    });
    $("#submit").click(function (e) {
        e.preventDefault();
        let link = $("#send_data").attr('action');
        let form_data = new FormData($(this).parents('form')[0]);
        $.ajax({
            url: link,
            method: 'post',
            data: form_data,
            xhr: function () {
                let myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.status == 'success') {
                    swal({
                            html: true,
                            title: data.message,
                            type: data.status,
                            showConfirmButton: true
                        },
                        function () {
                            document.location.href = site_url + 'auth/login';
                        });
                } else if (data.status == 'error_captcha') {
                    // $("#img_capt").html('');
                    swal({
                        html: true,
                        title: data.message,
                        type: 'error'
                    });
                    $("#img_capt").html(data.image);
                } else {
                    swal({
                        html: true,
                        title: data.message,
                        type: data.status
                    });
                }
            }
        });
    })
});