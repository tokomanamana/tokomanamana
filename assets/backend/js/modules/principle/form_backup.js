$(function () {
    $('body').on('click', '#edit-image-btn', function () {
        $('#image-preview').remove();
    });
    $('body').on('click', '#delete-image-btn', function () {
        $('#image').val('');
        $('#image-preview').remove();
        $('#add-image-btn').show();
        $('#edit-image-btn').hide();
        $('#delete-image-btn').hide();
    });
    $('#approve').on('click', function () {
        // swal({
        //     title: "Apakah ingin disetujui?",
        //     type: "info",
        //     showCancelButton: true,
        //     confirmButtonClass: "btn-danger",
        //     confirmButtonText: "Iya",
        //     cancelButtonText: "Batal",
        //     closeOnConfirm: true
        // },
        // function () {
        //     let id = $('#approve').data('id');
            // $.ajax({
            //     url: site_url + 'principle/request/approved',
            //     data: {
            //         id: id
            //     },
            //     method: 'post',
            //     success: function (data) {
            //         data = JSON.parse(data);
            //         swal({
            //             title: data.message,
            //             type: data.status
            //         },
            //         function () {
            //             document.location.href = site_url + 'principle/request';
            //         });
            //     }
            // })
        // });
        let id = $('#approve').data('id');
        swal({
            title: 'Masukkan Password',
            text: 'Harap masukkan password untuk menyetujui principal',
            type: 'input',
            showCancelButton: true,
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: 'Submit',
            cancelButtonText: lang.button.cancel,
            closeOnConfirm: false,
            inputPlaceholder: "Password",
            inputType: "password"
        }, function(inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("Password tidak boleh kosong!");
                return false
            }
            $.ajax({
                url: site_url + '/orders/check_account',
                data: {
                    password: inputValue
                },
                type: 'post',
                success: function(data) {
                    if (data == 'true') {
                        // swal.close();
                        $.ajax({
                            url: site_url + 'principle/request/approved',
                            data: {
                                id: id
                            },
                            method: 'post',
                            success: function (data) {
                                data = JSON.parse(data);
                                swal({
                                    title: data.message,
                                    type: data.status
                                },
                                function () {
                                    document.location.href = site_url + 'principle/request';
                                });
                            }
                        })
                    } else {
                        swal.showInputError("Password salah!");
                        return false
                    }
                }
            })
        })
    });
    $("#reject").on('click', function () {
        // swal({
        //     title: "Apakah tidak setuju?",
        //     type: "error",
        //     showCancelButton: true,
        //     confirmButtonClass: "btn-danger",
        //     confirmButtonText: "Iya",
        //     cancelButtonText: "Batal",
        //     closeOnConfirm: true
        // },
        // function() {
        //     $('#modal_rejected').modal('show');
        // })
        swal({
            title: 'Masukkan Password',
            text: 'Harap masukkan password untuk menolak principal',
            type: 'input',
            showCancelButton: true,
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: 'Submit',
            cancelButtonText: lang.button.cancel,
            closeOnConfirm: false,
            inputPlaceholder: "Password",
            inputType: "password"
        }, function(inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("Password tidak boleh kosong!");
                return false
            }
            $.ajax({
                url: site_url + '/orders/check_account',
                data: {
                    password: inputValue
                },
                type: 'post',
                success: function(data) {
                    if (data == 'true') {
                        swal.close();
                        $('#modal_rejected').modal('show');
                    } else {
                        swal.showInputError("Password salah!");
                        return false
                    }
                }
            })
        })
    });
    $('#resending').on('click', function() {
        let id = $('#resending').data('id');
        // swal({
        //     title: 'Apakah ingin mengirim ulang email?',
        //     type: 'info',
        //     showCancelButton: true,
        //     confirmButtonClass: 'btn-danger',
        //     confirmButtonText: 'Iya',
        //     cancelButtonText: 'Batal',
        //     closeOnConfirm: true
        // }, function() {
        //     let id = $('#resending').data('id');
        //     $.ajax({
        //         url: site_url + 'principle/request/resend_email',
        //         data: {
        //             id: id
        //         },
        //         method: 'POST',
        //         success: function(data) {
        //             data = JSON.parse(data);
        //             swal({
        //                 title: data.message,
        //                 type: data.status
        //             },
        //             function () {
        //                 document.location.href = site_url + 'principle/request';
        //             });
        //         }
        //     })
        // })
        swal({
            title: 'Masukkan Password',
            text: 'Harap masukkan password untuk merubah principal',
            type: 'input',
            showCancelButton: true,
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: 'Submit',
            cancelButtonText: lang.button.cancel,
            closeOnConfirm: false,
            inputPlaceholder: "Password",
            inputType: "password"
        }, function(inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("Password tidak boleh kosong!");
                return false
            }
            $.ajax({
                url: site_url + '/orders/check_account',
                data: {
                    password: inputValue
                },
                type: 'post',
                success: function(data) {
                    if (data == 'true') {
                        // swal.close();
                        $.ajax({
                            url: site_url + 'principle/request/resend_email',
                            data: {
                                id: id
                            },
                            method: 'POST',
                            success: function(data) {
                                data = JSON.parse(data);
                                swal({
                                    title: data.message,
                                    type: data.status
                                },
                                function () {
                                    document.location.href = site_url + 'principle/request';
                                });
                            }
                        })
                    } else {
                        swal.showInputError("Password salah!");
                        return false
                    }
                }
            })
        })
    });
    $('input[name=documents_not_complete]').on('click', function() {
        if($(this).prop('checked') == true) {
            $('#documents_not_complete').attr('style', 'display: block;margin-top: 10px;');
        } else {
            $('#documents_not_complete').attr('style', 'display: none;margin-top: 10px;');
        }
    });
    $('input[name=documents_not_valid]').on('click', function() {
        if($(this).prop('checked') == true) {
            $('#documents_not_valid').attr('style', 'display: block;margin-top: 10px;');
        } else {
            $('#documents_not_valid').attr('style', 'display: none;margin-top: 10px;');
        }
    })
    $('#buttonSubmit').on('click', function() {
        swal({
            title: 'Masukkan Password',
            text: 'Harap masukkan password untuk merubah principal',
            type: 'input',
            showCancelButton: true,
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: 'Submit',
            cancelButtonText: lang.button.cancel,
            closeOnConfirm: false,
            inputPlaceholder: "Password",
            inputType: "password"
        }, function(inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("Password tidak boleh kosong!");
                return false
            }
            $.ajax({
                url: site_url + '/orders/check_account',
                data: {
                    password: inputValue
                },
                type: 'post',
                success: function(data) {
                    if (data == 'true') {
                        swal.close();
                        $('#form').submit()
                    } else {
                        swal.showInputError("Password salah!");
                        return false
                    }
                }
            })
        })
    })
});
function responsive_filemanager_callback(field_id) {
    if (field_id) {
        var url = $('#' + field_id).val();
        var html = '<div class="col-md-3" id="image-preview">';
        html += '<div class="thumbnail"><div class="thumb">';
        html += '<img src="' + site_url + '../files/images/' + url + '">';
        html += '</div></div></div>';
        $(html).insertBefore('#add-image');
        $('#add-image-btn').hide();
        $('#edit-image-btn').show();
        $('#delete-image-btn').show();
    }
}