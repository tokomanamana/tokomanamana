$(function () {
    $('#province').on('change', function () {
        $('#district').html('<option value=""></option>').selectpicker('refresh');
        if ($(this).val() == "") {
            $('#city').html('<option value=""></option>').selectpicker('refresh');
        } else {
            $.ajax({
                type: "POST",
                url: site_url + 'warehouses/get_cities',
                data: {id: $(this).val()},
                success: function (data) {
                    $('#city').html(data).selectpicker('refresh');
                }
            });
        }
    });
    $('#city').on('change', function () {
        if ($(this).val() == "") {
            $('#district').html('<option value=""></option>').selectpicker('refresh');
        } else {
            $.ajax({
                type: "POST",
                url: site_url + 'warehouses/get_districts',
                data: {id: $(this).val()},
                success: function (data) {
                    $('#district').html(data).selectpicker('refresh');
                }
            });
        }
    });
    $('a[href=#address]').on('click', function () {
        setTimeout(function () {
            x = map.getZoom();
            c = map.getCenter();
            google.maps.event.trigger(map, 'resize');
            map.setZoom(x);
            map.setCenter(c);
        }, 50);
    });
});

function setLocation(lat, lng) {
    $('#lat').val(lat);
    $('#lng').val(lng);
}
