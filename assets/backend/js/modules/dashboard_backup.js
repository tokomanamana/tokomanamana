$(function () {
    // am4core.useTheme(am4themes_animated);
    // var chart = am4core.create("chartdiv", am4charts.PieChart);
    // chart.legend = new am4charts.Legend();
    //  // Add data
    //  chart.data = [];
    //   $.ajax({
    //     url: site_url + 'dashboard/chart_order_stats',
    //     success: function (data) {
    //         data = JSON.parse(data);
    //         chart.data = [ {
    //             "name": "Selesai",
    //             "total": data.finish,
    //         }, {
    //             "name": "Belum Selesai",
    //             "total": data.progress
    //         },{
    //             "name": "Ditolak",
    //             "total": data.reject
    //         }];
    //     }
    // });
    // // Add and configure Series
    // var pieSeries = chart.series.push(new am4charts.PieSeries());
    // var colorSet = new am4core.ColorSet();
    // colorSet.list = ["#388E3C", "#0288d1", "#F44336"].map(function(color) {
    // return new am4core.color(color);
    // });
    // pieSeries.colors = colorSet;
    // pieSeries.dataFields.value = "total";
    // pieSeries.dataFields.category = "name";
    // pieSeries.slices.template.stroke = am4core.color("#fff");
    // pieSeries.slices.template.strokeWidth = 2;
    // pieSeries.slices.template.strokeOpacity = 1;
    // pieSeries.labels.template.disabled = true;
    // pieSeries.ticks.template.disabled = true;
    
    // // This creates initial animation
    // pieSeries.hiddenState.properties.opacity = 1;
    // pieSeries.hiddenState.properties.endAngle = -90;
    // pieSeries.hiddenState.properties.startAngle = -90;

});
function copyLink() {
    var copyText = document.getElementById("myInput");
    copyText.select();
    document.execCommand("copy");

    swal('Copy link berhasil', '', 'success');
}

$(function() {
    $('.custom-select').select2({
        minimumResultsForSearch: Infinity
    });
    var tableOrders = $('#table-order').DataTable({
        stateSave: false,
        processing: true,
        serverSide: true,
        searching: false,
        ajax: {
            url: $('#table-order').data('url'),
            type: 'post',
            data: function (data) {
                data.filter = get_form_data($('#filter-order'));
                return data;
            }
        },
        autoWidth: false,
        columnDefs: [
            {
                orderable: false,
                targets: 'no-sort'
            },
            {
                className: 'text-center',
                targets: 'text-center'
            },
            {
                className: 'text-right',
                targets: 'text-right'
            }
        ],
        language: {
            processing: lang.table.processing,
            zeroRecords: lang.table.zero_records,
            lengthMenu: '',
        },
        scrollX: true,
        paging: false,
        info: false
    });

    $('#filter-order .filter').on('click', function () {
        tableOrders.ajax.reload();
    });

    $('#filter-order .filter-by').on('change', function () {
        var a = $(this).val();
        var now = new Date();
        $('#filter-order input[name=from]').datepicker('destroy');
        $('#filter-order input[name=to]').datepicker('destroy');
        $('#filter-order input[name=from]').attr('readonly', 'readonly');
        $('#filter-order input[name=to]').attr('readonly', 'readonly');
        if (a == 'today') { //hari ini
            $('#filter-order input[name=from]').val(formatDate(now));
            $('#filter-order input[name=to]').val(formatDate(now));
        } else if (a == 'yesterday') { //kemarin
            var date = new Date();
            date.setDate(now.getDate() - 1);
            $('#filter-order input[name=from]').val(formatDate(date));
            $('#filter-order input[name=to]').val(formatDate(date));
        } else if (a == 'this month') { //bulan ini
            var date1 = new Date(now.getFullYear(), now.getMonth(), 1);
            var date2 = new Date(now.getFullYear(), now.getMonth() + 1, 0);
            $('#filter-order input[name=from]').val(formatDate(date1));
            $('#filter-order input[name=to]').val(formatDate(date2));
        } else if (a == 'last month') { //bulan lalu
            var date1 = new Date(now.getFullYear(), now.getMonth() - 1, 1);
            var date2 = new Date(now.getFullYear(), now.getMonth(), 0);
            $('#filter-order input[name=from]').val(formatDate(date1));
            $('#filter-order input[name=to]').val(formatDate(date2));
        } else if (a == 'this year') { //tahun ini
            var date1 = new Date(now.getFullYear(), 0, 1);
            var date2 = new Date(now.getFullYear(), 12, 0);
            $('#filter-order input[name=from]').val(formatDate(date1));
            $('#filter-order input[name=to]').val(formatDate(date2));
        } else if (a == 99) {
            $('#filter-order input[name=from]').datepicker({format: 'yyyy-mm-dd'});
            $('#filter-order input[name=from]').removeAttr('readonly');
            $('#filter-order input[name=to]').datepicker({format: 'yyyy-mm-dd'});
            $('#filter-order input[name=to]').removeAttr('readonly');
        }
    });

    var tableUserList = $('#table-user-list').DataTable({
        stateSave: false,
        processing: true,
        serverSide: true,
        searching: false,
        ajax: {
            url: $('#table-user-list').data('url'),
            type: 'post',
            data: function (data) {
                data.filter = get_form_data($('#filter-user-list'));
                return data;
            }
        },
        autoWidth: false,
        columnDefs: [
            {
                orderable: false,
                targets: 'no-sort'
            },
            {
                className: 'text-center',
                targets: 'text-center'
            },
            {
                className: 'text-right',
                targets: "text-right"
            }
        ],
        language: {
            processing: lang.table.processing,
            zeroRecords: lang.table.zero_records,
            lengthMenu: '',
        },
        scrollX: true,
        paging: false,
        info: false
    });

    $('#filter-user-list .filter').on('click', function () {
        tableUserList.ajax.reload();
    });

    $('#filter-user-list .filter-by').on('change', function () {
        var a = $(this).val();
        var now = new Date();
        $('#filter-user-list input[name=from]').datepicker('destroy');
        $('#filter-user-list input[name=to]').datepicker('destroy');
        $('#filter-user-list input[name=from]').attr('readonly', 'readonly');
        $('#filter-user-list input[name=to]').attr('readonly', 'readonly');
        if (a == 'all') {
            var n = new Date(2017, 0, 1, 00, 00, 00, 0);
            var date1 = new Date(n.getFullYear(), n.getMonth(), n.getDate());
            var date2 = new Date(now.getFullYear(), now.getMonth(), 0);
            $('#filter-user-list input[name=from]').val(formatDate(0));
            $('#filter-user-list input[name=to]').val(formatDate(date2));
        } else if (a == 'today') { //hari ini
            $('#filter-user-list input[name=from]').val(formatDate(now));
            $('#filter-user-list input[name=to]').val(formatDate(now));
        } else if (a == 'yesterday') { //kemarin
            var date = new Date();
            date.setDate(now.getDate() - 1);
            $('#filter-user-list input[name=from]').val(formatDate(date));
            $('#filter-user-list input[name=to]').val(formatDate(date));
        } else if (a == 'this month') { //bulan ini
            var date1 = new Date(now.getFullYear(), now.getMonth(), 1);
            var date2 = new Date(now.getFullYear(), now.getMonth() + 1, 0);
            $('#filter-user-list input[name=from]').val(formatDate(date1));
            $('#filter-user-list input[name=to]').val(formatDate(date2));
        } else if (a == 'last month') { //bulan lalu
            var date1 = new Date(now.getFullYear(), now.getMonth() - 1, 1);
            var date2 = new Date(now.getFullYear(), now.getMonth(), 0);
            $('#filter-user-list input[name=from]').val(formatDate(date1));
            $('#filter-user-list input[name=to]').val(formatDate(date2));
        } else if (a == 'this year') { //tahun ini
            var date1 = new Date(now.getFullYear(), 0, 1);
            var date2 = new Date(now.getFullYear(), 12, 0);
            $('#filter-user-list input[name=from]').val(formatDate(date1));
            $('#filter-user-list input[name=to]').val(formatDate(date2));
        } else if (a == 99) {
            $('#filter-user-list input[name=from]').datepicker({format: 'yyyy-mm-dd'});
            $('#filter-user-list input[name=from]').removeAttr('readonly');
            $('#filter-user-list input[name=to]').datepicker({format: 'yyyy-mm-dd'});
            $('#filter-user-list input[name=to]').removeAttr('readonly');
        }
    });

    var tableBestseller = $('#table-bestseller').DataTable({
        stateSave: false,
        processing: true,
        serverSide: true,
        searching: false,
        ajax: {
            url: $('#table-bestseller').data('url'),
            type: 'post',
            data: function (data) {
                data.filter = get_form_data($('#filter-bestseller'));
                return data;
            }
        },
        autoWidth: false,
        columnDefs: [
            {
                orderable: false,
                targets: 'no-sort'
            },
            {
                className: 'text-center',
                targets: 'text-center'
            },
            {
                className: 'text-right',
                targets: 'text-right'
            }
        ],
        language: {
            processing: lang.table.processing,
            zeroRecords: lang.table.zero_records,
            lengthMenu: '',
        },
        scrollX: true,
        paging: false,
        info: false
    });

    $('#filter-bestseller .filter').on('click', function () {
        tableBestseller.ajax.reload();
    });

    $('#filter-bestseller .export').on('click', function () {
        filter = get_form_data($('#filter-bestseller'));
        $.ajax({
            url: site_url + 'dashboard/export_bestseller',
            data: {
                from: filter.from,
                to: filter.to
            },
            type: 'POST',
            dataType: 'JSON',
            success: function (data) {
                var $a = $('<a>');
                $a.attr('href', data.file);
                $('body').append($a);
                $a.attr('download', data.name);
                $a[0].click();
                $a.remove();
            }
        })
    });

    $('#filter-bestseller .filter-by').on('change', function () {
        var a = $(this).val();
        var now = new Date();
        $('#filter-bestseller input[name=from]').datepicker('destroy');
        $('#filter-bestseller input[name=to]').datepicker('destroy');
        $('#filter-bestseller input[name=from]').attr('readonly', 'readonly');
        $('#filter-bestseller input[name=to]').attr('readonly', 'readonly');
        if (a == 'today') { //hari ini
            $('#filter-bestseller input[name=from]').val(formatDate(now));
            $('#filter-bestseller input[name=to]').val(formatDate(now));
        } else if (a == 'yesterday') { //kemarin
            var date = new Date();
            date.setDate(now.getDate() - 1);
            $('#filter-bestseller input[name=from]').val(formatDate(date));
            $('#filter-bestseller input[name=to]').val(formatDate(date));
        } else if (a == 'this month') { //bulan ini
            var date1 = new Date(now.getFullYear(), now.getMonth(), 1);
            var date2 = new Date(now.getFullYear(), now.getMonth() + 1, 0);
            $('#filter-bestseller input[name=from]').val(formatDate(date1));
            $('#filter-bestseller input[name=to]').val(formatDate(date2));
        } else if (a == 'last month') { //bulan lalu
            var date1 = new Date(now.getFullYear(), now.getMonth() - 1, 1);
            var date2 = new Date(now.getFullYear(), now.getMonth(), 0);
            $('#filter-bestseller input[name=from]').val(formatDate(date1));
            $('#filter-bestseller input[name=to]').val(formatDate(date2));
        } else if (a == 'this year') { //tahun ini
            var date1 = new Date(now.getFullYear(), 0, 1);
            var date2 = new Date(now.getFullYear(), 12, 0);
            $('#filter-bestseller input[name=from]').val(formatDate(date1));
            $('#filter-bestseller input[name=to]').val(formatDate(date2));
        } else if (a == 99) {
            $('#filter-bestseller input[name=from]').datepicker({format: 'yyyy-mm-dd'});
            $('#filter-bestseller input[name=from]').removeAttr('readonly');
            $('#filter-bestseller input[name=to]').datepicker({format: 'yyyy-mm-dd'});
            $('#filter-bestseller input[name=to]').removeAttr('readonly');
        }
    });

    var tableMostviewed = $('#table-mostviewed').DataTable({
        stateSave: false,
        processing: true,
        serverSide: true,
        searching: false,
        ajax: {
            url: $('#table-mostviewed').data('url'),
            type: 'post',
            data: function (data) {
                data.filter = get_form_data($('#filter-mostviewed'));
                return data;
            }
        },
        autoWidth: false,
        columnDefs: [
            {
                orderable: false,
                targets: 'no-sort'
            },
            {
                className: 'text-center',
                targets: 'text-center'
            },
            {
                className: 'text-right',
                targets: 'text-right'
            }
        ],
        language: {
            processing: lang.table.processing,
            zeroRecords: lang.table.zero_records,
            lengthMenu: '',
        },
        scrollX: true,
        paging: false,
        info: false
    });

    $('#filter-mostviewed .filter').on('click', function () {
        tableMostviewed.ajax.reload();
    });

    $('#filter-mostviewed .export').on('click', function () {
        filter = get_form_data($('#filter-mostviewed'));
        $.ajax({
            url: site_url + 'dashboard/export_mostviewed',
            data: {
                from: filter.from,
                to: filter.to
            },
            type: 'POST',
            dataType: 'JSON',
            success: function (data) {
                var $a = $('<a>');
                $a.attr('href', data.file);
                $('body').append($a);
                $a.attr('download', data.name);
                $a[0].click();
                $a.remove();
            }
        })
    });

    $('#filter-mostviewed .filter-by').on('change', function () {
        var a = $(this).val();
        var now = new Date();
        $('#filter-mostviewed input[name=from]').datepicker('destroy');
        $('#filter-mostviewed input[name=to]').datepicker('destroy');
        $('#filter-mostviewed input[name=from]').attr('readonly', 'readonly');
        $('#filter-mostviewed input[name=to]').attr('readonly', 'readonly');
        if (a == 'today') { //hari ini
            $('#filter-mostviewed input[name=from]').val(formatDate(now));
            $('#filter-mostviewed input[name=to]').val(formatDate(now));
        } else if (a == 'yesterday') { //kemarin
            var date = new Date();
            date.setDate(now.getDate() - 1);
            $('#filter-mostviewed input[name=from]').val(formatDate(date));
            $('#filter-mostviewed input[name=to]').val(formatDate(date));
        } else if (a == 'this month') { //bulan ini
            var date1 = new Date(now.getFullYear(), now.getMonth(), 1);
            var date2 = new Date(now.getFullYear(), now.getMonth() + 1, 0);
            $('#filter-mostviewed input[name=from]').val(formatDate(date1));
            $('#filter-mostviewed input[name=to]').val(formatDate(date2));
        } else if (a == 'last month') { //bulan lalu
            var date1 = new Date(now.getFullYear(), now.getMonth() - 1, 1);
            var date2 = new Date(now.getFullYear(), now.getMonth(), 0);
            $('#filter-mostviewed input[name=from]').val(formatDate(date1));
            $('#filter-mostviewed input[name=to]').val(formatDate(date2));
        } else if (a == 'this year') { //tahun ini
            var date1 = new Date(now.getFullYear(), 0, 1);
            var date2 = new Date(now.getFullYear(), 12, 0);
            $('#filter-mostviewed input[name=from]').val(formatDate(date1));
            $('#filter-mostviewed input[name=to]').val(formatDate(date2));
        } else if (a == 99) {
            $('#filter-mostviewed input[name=from]').datepicker({format: 'yyyy-mm-dd'});
            $('#filter-mostviewed input[name=from]').removeAttr('readonly');
            $('#filter-mostviewed input[name=to]').datepicker({format: 'yyyy-mm-dd'});
            $('#filter-mostviewed input[name=to]').removeAttr('readonly');
        }
    });
})

function get_form_data($form){
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });
    return indexed_array;
}

function formatDate(date) {
    var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}

function see_filtered_order(filter) {
    let from = $('#filter-order input[name="from"]').val();
    let to = $('#filter-order input[name="to"]').val();
    let desc = $('#filter-order .filter-by').find(':selected').val();
    let filterOrder = [filter, from, to, desc] ;
    localStorage.setItem('filterOrder', JSON.stringify(filterOrder));
    window.location.href = site_url + 'orders';
}

function see_month_sales() {
    var now = new Date();
    var date1 = new Date(now.getFullYear(), now.getMonth(), 1);
    var date2 = new Date(now.getFullYear(), now.getMonth() + 1, 0);
    var from = formatDate(date1)
    var to = formatDate(date2)
    var filter = 'Paid'
    var desc = 'this month'
    var filterOrder = [filter, from, to, desc];
    localStorage.setItem('monthSales', JSON.stringify(filterOrder))
    window.location.href = site_url + 'orders'
}