$(function () {
    $('body').on('change', '.quantity', function (e) {
        e.preventDefault();
//    console.log($(this).val());
        var el = $(this);
        el.attr('disabled', 'disabled');
        $.ajax({
            type: "POST",
            url: current_url + '/update_quantity_merchant',
            data: {quantity: el.val(), id: el.data('id')},
            success: function (data) {
                el.removeAttr('disabled');
                el.data('id', data);
            }
        });
    });

    $(':input[type="number"]').keydown(function (e) { 
       if(!((e.keyCode > 95 && e.keyCode < 106)
      || (e.keyCode > 47 && e.keyCode < 58) 
      || e.keyCode == 8)) {
        return false;
    }
    });
    
//    $.post(site_url + 'products/search_products/' + text + '/' + page, function (data) {
//        data = JSON.parse(data);
//        if (data.status == 'success') {
//            $('#list-product').append(data.content);
//            page = data.page;
//            if (data.page == data.page_max) {
//                $('#load-more').hide();
//            } else {
//                $('#load-more').show();
//            }
//        }
//    });
    
    $('body').on('click','.remove',function (e){

        e.preventDefault();
        var el = $(this);
        var $tr = $(this).closest('tr');
        
        swal({
          title: 'Yakin produk akan dibuang?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#DD6B55',
          confirmButtonText: 'Hapus',
          cancelButtonText: 'Batal'
        },function (isConfirm) {
          if (isConfirm) {
            // handle Confirm button click
            $.ajax({
                type: "POST",
                url: current_url + '/delete_merchant',
                data: {id: el.data('id')},
                success: function (data) {
                   $tr.find('td').fadeOut(500,function(){ 
                            $tr.remove();                    
                    }); 
                }
            });
          } else {
            // result.dismiss can be 'cancel', 'overlay', 'esc' or 'timer'
          }
        });
         // console.log(el.data('id'));
     });

      $('body').on('click','.status',function (e){
        e.preventDefault();
        var el = $(this);
        var id= $('.status').attr('id');
        swal({
          title: 'Ubah Status Produk ini?',
          type: 'info',
          showCancelButton: true,
          confirmButtonText: 'Ubah',
          cancelButtonText: 'Batal'
        },function (isConfirm) {
          if (isConfirm) {
            
            console.log(id);
            // handle Confirm button click
            if (el.data('status') == 0) {
                data = {id: el.data('id'), status: 1 };
            } else if (el.data('status') == 1)  {
                data = {id: el.data('id'), status: 0 };
            }

                $.ajax({
                    type: "POST",
                    url: current_url + '/update_status_merchant',
                    data: data,
                    success: function (data) {      
                        if(data==1){
                          el.data('status',1);
                          
                         
                          el.html('<i class="status" style="color:green;text-decoration:underline;">Aktif</i>');
                          
                        }
                        else if(data==0){
                           
                          el.data('status',0);
                          
                          
                            el.html('<i class="status" style="color:red;text-decoration:underline;">Aktifkan produk ini</i>');
                          
                        }
                    }
                });

            }
            
           
          else {
            // result.dismiss can be 'cancel', 'overlay', 'esc' or 'timer'
            
          }
        });
         
     });

    $('#table').DataTable().on('draw', function (e) {
        e.preventDefault();
        $('[data-popup=popover]').popover();
    });
    $('body').on('change', '.pricing_level', function (e) {
        e.preventDefault();
        var checked = 0;
        if($(this).is(':checked')){
            checked = 1;
        }
        $.ajax({
            type: "POST",
            url: current_url + '/update_quantity',
            data: {id: $(this).data('id'), pricing_level: checked},
            success: function (data) {
                
            }
        });
    });
    $('#btn-export').on('click', function () {
        window.open(site_url + 'products/productv2/export', '_blank');
    });
   $('#btn-import').on('click', function () {
    $('#modal-import').modal('show');
    $('#modal-import form')[0].reset();
  });
  $('#submit-import').on('click', function () {
      var data = new FormData();
      var file = $('#file')[0].files[0];
      data.append('file', file);
      $.ajax({
          url: site_url+'products/productv2/import',
          data: data,
          cache: false,
          contentType: false,
          processData: false,
          method: 'POST',
          type: 'POST',
          success: function (res) {
              $('#modal-import').modal('hide');
              responseForm(res);
          }
      });
  });

});