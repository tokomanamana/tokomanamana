$(function () {
    $('#discount').on('change', function () {
        get_price_discount()
    });
    $('#price').on('change', function () {
        get_price_discount()
    });
});

function get_price_discount() {
    var price = $('#price').val();
    var discount = $('#discount').val();
    var price_discount = price * discount / 100;
    $('#price-discount').val(price - price_discount);
}