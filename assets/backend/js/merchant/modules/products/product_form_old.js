$(function () {
    let prod_type = $('#type').val();
    let promo = $('#promo-slc').val();
    let option = $('#option-slc').val();
    let preorder = $('#preorder').val();
    let grosir = $('#grosir-slc').val();
    let reseller = $('#reseller-slc').val();
    let preorder_time = $('#preorder-time-id').val();
    let count_checkbox_colour = 0
    let count_checkbox_size = 0
    let colour_status = 0;
    let size_status = 0;
    let colour_size_on = 0;


     $(':input[type="number"]').keydown(function (e) { 
       if(!((e.keyCode > 95 && e.keyCode < 106)
      || (e.keyCode > 47 && e.keyCode < 58) 
      || e.keyCode == 8)) {
        return false;
    }
    });


    //variation panels
    $('.variation-body-colour').addClass('inactive');
    $('.variation-body-size').addClass('inactive');

    $('.variation-swich-colour').on('switchChange.bootstrapSwitch', function (e) {
        if(e.target.checked){
             $('.variation-body-colour').removeClass('inactive'); 
             $('.checkbox-colour').prop('disabled', false);
             $('.checkbox-colour').css('cursor','pointer');
             colour_status = 1;
             $('#option-generate').prop('disabled',false); 
             if(size_status === 1){
                colour_size_on = 1;
             }

        }
        else{
            $('.variation-body-colour').addClass('inactive'); 
            // $('.checkbox-colour').prop('checked', false);
            $('.checkbox-colour').prop('disabled', true);
            $('.checkbox-colour').css('cursor','not-allowed');
            colour_status = 0;
            colour_size_on = 0;
            $('#option-generate').prop('disabled',true); 
            $('.checkbox-colour').prop('checked', false);
        }
       
    });

      $('.variation-swich-size').on('switchChange.bootstrapSwitch', function (e) {
        if(e.target.checked){
             $('.variation-body-size').removeClass('inactive'); 
             $('.checkbox-size').prop('disabled', false);
             $('.checkbox-size').css('cursor','pointer');
             size_status = 1;
             $('#option-generate').prop('disabled',false);
             if(colour_status === 1){
                colour_size_on = 1;
             }

        }
        else{
            $('.variation-body-size').addClass('inactive'); 
            $('.checkbox-size').prop('disabled', true);
            $('.checkbox-size').css('cursor','not-allowed');
            size_status = 0;
            colour_size_on = 0;
            $('#option-generate').prop('disabled',true); 
            $('.checkbox-size').prop('checked', false);
        }
       
    });

    $('.checkbox-colour-count').on('change',function(){
        count_checkbox_colour = $(".checkbox-colour-count input:checkbox:checked").length;
        if(colour_status === 1){
            if(count_checkbox_colour > 0){
               $('#option-generate').prop('disabled',false); 
            }
        }
      

    })
    $('.checkbox-size-count').on('change',function(){
        count_checkbox_size = $(".checkbox-size-count input:checkbox:checked").length;
        if(size_status === 1){
            if(count_checkbox_size > 0){
               $('#option-generate').prop('disabled',false); 
            }
        }
    
    })

    // $('#option-generate').on('click',function(e){
    //     e.preventDefault();
    //     if(colour_size_on === 1){
    //         if(count_checkbox_colour > 0 && count_checkbox_size > 0){
    //             $('#option-generate').submit();
    //         }
    //         else{
    //              swal({
    //                 title: 'Jika variasi warna dan ukuran aktif anda perlu mengisi dua-duanya!',
    //                 type: "warning",
    //                 showCancelButton: false,
    //                 confirmButtonColor: "#DD6B55",
    //                 confirmButtonText: 'Ok',
    //                 closeOnConfirm: false,
    //             });
    //              return false;
    //         }
    //     }
    //     else{
    //         $('#option-generate').submit();
    //     }

    // })

    //Controls visibility of options or inputs on load page
    if(prod_type == 's') {
        $("#package-tab").hide();
    } else {
        $("#package-tab").show();
    }
    if(promo == '0') {
        $("#promo-tab").hide();
    } else {
        $("#promo-tab").show();
    }
    if(option == '0') {
        $("#option-tab").hide();
        $(".variant_price").removeAttr('min');
    } else {
        $("#option-tab").show();
        $('#form-group-price').hide();
        $('.form-group-stock').hide();
    }
    if(brand == 'other'){
        var input = '<input type="text" name="brand_other" id="brand_other"  class="form-control" value="">';
        $('#brand_other_div').html(input);
    }
    if(grosir == '0'){
        $('#price-list').hide();
        $('.td-grosir-qty').prop('required',false);
        $('.td-grosir-qty').removeAttr('min');
        $('.number-grosir').prop('required',false);
        $('.number-grosir').removeAttr('min');
    }
    else{
       $('#price-list').show();
    }
    if(reseller == '0'){
        $('#price-reseller-list').hide();
        $('.td-reseller-qty').prop('required',false);
        $('.td-reseller-qty').removeAttr('min');
        $('.number-reseller').prop('required',false);
        $('.number-reseller').removeAttr('min');
    }
    else{
       $('#price-reseller-list').show();
    }
    if((grosir === '1' && option === '1') || (reseller === '1' && option === '1')){
        $('.variant_price').prop({ readOnly: true });
        $('#form-group-price').show();
         $('#price-input').on('change',function(){
            $('.variant_price').val($('#price-input').val());
        });
        
    }
    else if((grosir === '0' && option === '1') || (reseller === '0' && option === '1')){
        $('#form-group-price').hide();
    }
    // if($('#reseller-slc').val() === '1' && option === '1'){
    //     $('.variant_price').prop({ readOnly: true });
    //     $('#form-group-price').show();
    // }
    // else if($('#reseller-slc').val() === '0' && option === '1'){
    //     $('#form-group-price').hide();
    // }
    if(preorder == '0') {
        $("#preorder-time").hide();
        $("#coming-soon-time").hide();
    } else if(preorder == '1') {
        $("#preorder-time").show();
        $("#label-time").html('Waktu Pre-Order');
        $("#coming-soon-time").hide();
    } else {
        $("#preorder-time").hide();
        $("#coming-soon-time").show();
        $("#label-time").html('Waktu Coming Soon');
    }

    //Datepicker
    $( "#coming-soon-time-id" ).datepicker();

    $('.page-header .page-title').css({width: $('.page-header .page-title').width() - $('.page-header .heading-elements').width() - 50});
    // $('#category').on('change', function () {

    //     $.ajax({
    //         type: "POST",
    //         url: site_url + 'products/productv2/get_feature_form',
    //         data: {category: $(this).val(), product: $('#id').val()},
    //         success: function (data) {
                
    //             $('#feature-form').html(data);
    //         }
    //     });

    //     $.ajax({
    //         type: "POST",
    //         url: site_url + 'products/productv2/get_variation_form',
    //         data: {category: $(this).val()},
    //         success: function (data) {
                
    //             $('#variation-form').html(data);
    //         }
    //     });
    // });
    
    
    $('#type').on('change', function() {
        if ( this.value == 's'){
            $("#package-tab").hide();
        } else {
            $("#package-tab").show();
            $('#a_package > .circle').remove();
            $('<div class="circle"></div>').appendTo('#a_package');
        } 
    });
    $('#a_package').on('click', function() {
        $('#a_package > .circle').remove();
        
    });
    $('#a_promo').on('click', function() {
        $('#a_promo > .circle').remove();
        
    });
    $('#a_option').on('click', function() {
        $('#a_option > .circle').remove();  
    });
    $('#promo-slc').on('change', function() {
        if ( this.value == '0'){
            $("#promo-tab").hide();
            $("#discount").prop('required',false);
            $("#start_date").prop('required',false);
            $("#end_date").prop('required',false);
        } else {
            $("#promo-tab").show();
            $('#a_promo > .circle').remove();
            $('<div class="circle"></div>').appendTo('#a_promo');
            $("#discount").prop('required',true);
            $("#start_date").prop('required',true);
            $("#end_date").prop('required',true);
        } 
    });
    $('#option-slc').on('change', function() {
        if ( this.value == '0'){
            option = this.value;
            $("#option-tab").hide();
            $('#form-group-price').show();
            $('.form-group-stock').show();
            $(".variant_price").removeAttr('min');
     
        } else {
            option = this.value;
            $("#option-tab").show();
            $('#a_option > .circle').remove();
            $('<div class="circle"></div>').appendTo('#a_option');
            $('#form-group-price').hide();
            $('.form-group-stock').hide();
            $('#stock_normal').val(0);
            $(".variant_price").prop('min',100);
            if(grosir === '0' && reseller === '0'){
                $('.variant_price').prop({ readOnly: false });
            }
            else if((grosir === '1' && reseller === '1') || (grosir === '0' && reseller === '1') || (grosir === '1' && reseller === '0')){
                $('#form-group-price').show();
                $('.variant_price').val($('#price-input').val());
                $('.variant_price').prop({ readOnly: true });
            }
           
        } 
    });
    $('#grosir-slc').on('change', function() {
        if ( this.value == '0'){
            grosir = this.value;
            $('#price-list').hide();
            $('.td-grosir-qty').prop('required',false);
            $('.td-grosir-qty').removeAttr('min');
            $('.number-grosir').prop('required',false);
            
            if(option === '1' && reseller === '0'){
                $('#form-group-price').hide();
                $('.variant_price').prop({ readOnly: false });
            }

   
        } else {
            grosir = this.value;
            $('.td-grosir-qty').prop('required',true);
            $('.td-grosir-qty').prop('min',2);
            $('.number-grosir').prop('required',true);
            if($('.price-qty').length <= 0){
                add_new_grosir_row();
                $('.remove-price').hide();
            }
         
            if(option === '1'){
                $('.variant_price').prop({ readOnly: true });
                $('#price-input').on('change',function(){
                    $('.variant_price').val($('#price-input').val());
                });
                 swal({
                    title: 'Peringatan',
                    text: 'Menambah harga grosir akan mengubah semua harga varian menjadi harga utama! Lanjut?',
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: 'Ok',
                    cancelButtonText: 'Batal',
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function (inputValue) {
                    if(inputValue === true){

                       $('#price-list').show();

                       $('#form-group-price').show();
                       $('.variant_price').prop({ readOnly: true });
                       $('.variant_price').val($('#price-input').val());

                      
                    }else{
                        grosir = 0;
                        $('#grosir-slc').val(0);
                    }
                    
                });
            }
            else{
                $('#price-list').show();
            }
            
        } 
    });

   //to check price grosir if only one data or not if yes hide delete button
   if($('.price-qty').length >= 1){
        $('.remove-price').first().hide();
   }
 
    $('#reseller-slc').on('change', function() {

        if ( this.value == '0'){
            reseller = this.value;
            $('#price-reseller-list').hide();
            $('.td-reseller-qty').prop('required',false);
            $('.td-reseller-qty').removeAttr('min');
            $('.number-reseller').prop('required',false);
            
            if(option === '1' && grosir === '0'){
                $('#form-group-price').hide();
                $('.variant_price').prop({ readOnly: false });
            }
   
        } else {
            reseller = this.value;
            $('.td-reseller-qty').prop('required',true);
            $('.td-reseller-qty').attr('min',2);
            $('.number-reseller').prop('required',true);

            if($('.price-qty-reseller').length <= 0){
                add_new_reseller_row();
                $('.remove-price-reseller').hide();
            }

            if(option === '1'){
                $('.variant_price').prop({ readOnly: true });
                $('#price-input').on('change',function(){
                    $('.variant_price').val($('#price-input').val());
                });
                 swal({
                    title: 'Peringatan',
                    text: 'Menambah harga reseller akan mengubah semua harga varian menjadi harga utama! Lanjut?',
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: 'Ok',
                    cancelButtonText: 'Batal',
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function (inputValue) {
                    if(inputValue === true){
                        $('#price-reseller-list').show();
                        $('#form-group-price').show();
                        $('.variant_price').prop({ readOnly: true });
                        $('.variant_price').val($('#price-input').val());


                    }else{

                        reseller = 0;
                        $('#reseller-slc').val(0);
                    }
                    
   
                });
            }
            else{
                $('#price-reseller-list').show();
            }

        } 
    });

    //to check price resell if only one data or not if yes hide delete button
   if($('.price-qty-reseller').length >= 1){
        $('.remove-price-reseller').first().hide();
   }


    $('#preorder').on('change', function() {
        if ( this.value == '0'){
            $("#preorder-time").hide();
            $("#coming-soon-time").hide();
        } else if(this.value == '1'){
            $("#preorder-time").show();
            $("#coming-soon-time").hide();
            $("#label-time").html('Waktu Pre-Order');
            $("#preorder-time-id").prop('min',3);
            if(preorder_time == 0){
                $("#preorder-time-id").prop("value", 3);
            }
        } else {
            $("#preorder-time").hide();
            $("#coming-soon-time").show();
            $("#label-time").html('Waktu Coming Soon');
        }
    });


   
    var selected = $('#promo_type').children(":selected").attr("id");
    if(selected == 'percentage'){
        $("#discount_addon").hide();
        $("#discount_addon_percent").show();
        $('#discount').prop('type', 'number');
        $("#discount").prop('min',1);
        $("#discount").prop('max',100);
    }
    else{
         $('#discount_addon_percent').hide();
    }
    $('#promo_type').change(function(){
        if(this.options[this.selectedIndex].id == 'percentage'){
            $("#discount_addon").hide();
             $("#discount_addon_percent").show();
             $('#discount').val(0);
             $('#discount').prop('type', 'number');
             $("#discount").prop('min',1);
             $("#discount").prop('max',100);
        }
        else{
            $('#discount').val(0);
            $('#discount_addon').show();
            $("#discount_addon_percent").hide();
            $('#discount').prop('type', 'text');
            $("#discount").prop('max',false);
            $("#discount").prop('min','100');

        }
    });

       
    $(document).on('keyup change',".td-grosir-qty",function(e){
       let increment_check = parseInt($(this).closest('tr').prev().children('td').children('.td-grosir-qty').val());
       let this_qty = parseInt($(this).val()); 
       let attr_name_qty = $(this).attr('name')
       let count_label_grosir_qty = $('.label_grosir_qty').length;
       if(count_label_grosir_qty === 0){
            if(this_qty <= increment_check){
                $(this).after('<label id="" class="custom-error-label label_grosir_qty" for="'+ attr_name_qty +'">Kuantitas minimal harus lebih tinggi dari sebelumnya!</label>')
            }
        }
        if(this_qty > increment_check){
            $('.label_grosir_qty').remove();
        }
        e.preventDefault();
        return false;
    })  

    $(document).on('keyup change',".td-reseller-qty",function(e){
       let increment_check_ = parseInt($(this).closest('tr').prev().children('td').children('.td-reseller-qty').val());
       let this_qty_ = parseInt($(this).val()); 
       let attr_name_qty_ = $(this).attr('name')
       let count_label_reseller_qty = $('.label_reseller_qty').length;
       if(count_label_reseller_qty === 0){
            if(this_qty_ <= increment_check_){
                $(this).after('<label id="" class="custom-error-label label_reseller_qty" for="'+ attr_name_qty_ +'">Kuantitas minimal harus lebih tinggi dari sebelumnya!</label>')
            }
        }
        if(this_qty_ > increment_check_){
            $('.label_reseller_qty').remove();
        }
        e.preventDefault();
        return false;
    })   

    $(document).on('keyup',".number-grosir",function(e){
        let grosir_value = parseInt($(this).val())
        let price_value = parseInt($('#price-input').val())
        let attr_name = $(this).attr('name')
        let count_label_grosir = $('.label_grosir').length;
        let increment_check_price = parseInt($(this).closest('tr').prev().children('.grosir_price_col').children('.input-group').children('.number-grosir').val());
        if(count_label_grosir === 0){
            if(price_value <= grosir_value){
                $(this).closest('.input-group').addClass('has-error');
                $(this).parent().after('<label id="" class="custom-error-label label_grosir" for="'+ attr_name +'">Harga Grosir harus lebih rendah dari Harga Utama!</label>')
            }
            if(grosir_value >= increment_check_price){
                 $(this).closest('.input-group').addClass('has-error');
                $(this).parent().after('<label id="" class="custom-error-label label_grosir" for="'+ attr_name +'">Harga Grosir harus lebih rendah dari sebelumnya!</label>')
            }

        }
        else{
            if(price_value > grosir_value){
                $('.label_grosir').remove();
                 $(this).closest('.input-group').removeClass('has-error');
            }
            if(grosir_value < increment_check_price){
                $('.label_grosir').remove();
                 $(this).closest('.input-group').removeClass('has-error');
            }
        }

        e.preventDefault();
        return false;
    })

    $(document).on('keyup',".number-reseller",function(e){
        let reseller_value = parseInt($(this).val())
        let price_value_ = parseInt($('#price-input').val())
        let attr_name_ = $(this).attr('name')
        let count_label_reseller = $('.label_reseller').length;
        let increment_check_price_ = parseInt($(this).closest('tr').prev().children('.reseller_price_col').children('.input-group').children('.number-reseller').val());
        if(count_label_reseller === 0){
            if(price_value_ <= reseller_value){
                $(this).closest('.input-group').addClass('has-error');
                $(this).parent().after('<label id="" class="custom-error-label label_reseller" for="'+ attr_name_ +'">Harga Reseller harus lebih rendah dari Harga Utama!</label>')
            }
            if(reseller_value >= increment_check_price_){
                 $(this).closest('.input-group').addClass('has-error');
                $(this).parent().after('<label id="" class="custom-error-label label_reseller" for="'+ attr_name_ +'">Harga Reseller harus lebih rendah dari sebelumnya!</label>')
            }

        }
        else{
            if(price_value_ > reseller_value){
                $('.label_reseller').remove();
                 $(this).closest('.input-group').removeClass('has-error');
            }
            if(reseller_value < increment_check_price_){
                $('.label_reseller').remove();
                 $(this).closest('.input-group').removeClass('has-error');
            }
        }
        e.preventDefault();
        return false;
    })

   
    // $(document).on("keydown.autocomplete",".autocompleteTxt",function(e){
    //     $(this).autocomplete({
    //         source: function( request, response ) {
    //             $.ajax({
    //                 url: site_url +'products/productv2/get_package_item',
    //                 data: {
    //                     query: request.term
    //                 },
    //                 success: function( data ) {
    //                     data = $.parseJSON(data);
    //                     response( data );
    //                 }
    //             });
    //             },
    //         minLength: 1,
    //         matchContains: true,
    //         selectFirst: false,
    //         select: function( event, ui ) {
    //             currentId = $(this).attr('id').replace('packageItem', '');
    //             $( '#packageItem'+currentId ).val( ui.item.label );
    //             $( '#packageItemId'+currentId ).val( ui.item.id );
    //         },
    //         change: function( event, ui ) {
    //             currentId = $(this).attr('id').replace('packageItem', '');
    //             $( "#packageItemId"+currentId ).val( ui.item? ui.item.id : 0 );
    //         } 
    //     });
    // });

    $(document).on('change','.pick-product-package',function(){

       let product_id = $(this).find(':selected').data('id');
       let main_id = $(this).data('main_id');
       $('#packageItemId' + main_id ).val(product_id);
    });

    $(document).on('click', '#add-package-item',function () {
        const id = Math.random().toString(36).slice(2);
        let html = '<div class="row" id="rowID-' + id + '">';
        let i = 0;
        $.ajax({
            url : site_url + 'products/productv2/products_package',
            method : "POST",
            async : true,
            dataType : 'json',
            success: function(data){
                html += '<div class="col-md-6">';
                html += '<div class="form-group">';
                html += '<label class="col-md-3 control-label">Item Name <div class="wajib">Wajib</div></label>';
                html += '<div class="col-md-9">';
                html += '<select id="packageItem' + id + '" data-main_id="'+ id +'" class="pick-product-package form-control" required="" name="package_items[' + id + '][product_name]">';
                html += '<option value=""></option>';
                for(i; i<data.length; i++){
                    html += '<option class="product_item_option" value='+data[i].name+' data-id='+data[i].id+'>'+data[i].name+'</option>';
                }
                // html += '<option value="test">test</option>';
                html += '</select>';
                // html += '<input id="packageItem' + id + '" class="autocompleteTxt form-control" required="" type="text" name="package_items[' + id + '][product_name]">';
                html += '<input id="packageRowId' + id + '" class="form-control" type="hidden" name="package_items[' + id + '][row_id]" value="'+id+'">';
                html += '<input id="packageItemId' + id + '" class="form-control" type="hidden" name="package_items[' + id + '][product_id]">';
                html += '</div></div></div>';
                html += '<div class="col-md-2">';
                html += '<div class="form-group">';
                html += '<label class="col-md-3 control-label">Qty</label>';
                html += '<div class="col-md-9">';
                html += '<input class="form-control" type="number" min="0" required="" name="package_items[' + id + '][qty]">';
                html += '</div></div></div>';
                html += '<div class="col-md-2">'
                html += '<button type="button" class="btn btn-danger btn-xs remove-item" data-id="' + id + '">x</button>';
                html += '</div></div>';
                $('#package-lists').append(html);

            }
        });
        
        
    });

    $(document).on('change','#file_main_image',function(){  
        let username = $('#username').val();
        let image_name = 'merchant/'+ username + '/' + $("#file_main_image").prop('files')[0].name ;
        let image = $("#file_main_image").prop('files')[0] ;
        let primary = 0;
        let product = $('input[name=id]').val();
        let data = new FormData();
        data.append('product_image', image);
        data.append('username', username);
        data.append('image_name', image_name);
        data.append('act', 'upload');
        data.append('product', product);
    
        if (!$("input:radio[name='image_primary']").is(":checked")) {
            primary = 1;
        }
        data.append('primary', primary);
        $.ajax({
            type: "POST",
            url: site_url + 'products/productv2/image',
            data: data,
            cache: false,
            contentType: false,
            processData: false,

            success: function(data) 
            {
                if(isNaN(data)){
                    let error = data.substring(3,data.indexOf('</p>'));
                    swal({
                        title: 'Peringatan',
                        text: error,
                        type: "warning",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: 'Ok',
                        closeOnConfirm: true
                    });
                }
                else{
                    var html = '<div class="image" id="img' + data + '">';
                    html += '<div class="image-bg" style="background-image: url(\'' + base_url + '../files/images/' + image_name + '\')"></div>';
                    html += '<div class="option">';
                    html += '<div class="radio"><label><input type="radio" value="' + data + '" name="image_primary" id="image_primary' + data + '">Default</label></div>';
                    html += '<div><a href="javascript:delete_image(\'' + data + '\')" class="text-muted pull-right"><i class="icon-trash"></i></a></div>';
                    html += '</div>';
                    html += '</div>';
                    $(html).insertBefore('#add-image');
                    if (primary)
                        $('#image_primary' + data).prop('checked', true);
                }
            }
        });
    });

    

    //ajax to get products for package type
    
    // // $.ajax({
    // //     type: 'POST',
    // //     url: site_url + 'products/productv2/products_package/',function(data){
    // //     data:'username='+username+'&msg='+msg,
    // //     success: function (data){
    // //        $.each(data, function(i, item) {
    // //          $('#info').append("<p> you are:"+data[i].username+"</p> <p> your message  is:"+data[i].mesg);
    // //        });​

    // //     }
    // // });
    //   $.post(
    //         site_url + 'products/productv2/products_package/',function(data){
    //             data = JSON.parse(data);
    //             image = data.image;
    //             new_captcha = data.new_captcha;
    //             $('.image').html('<label class="control-label">Captcha Code</label>'+ image +  '<a href="#" class ="refresh"></a>');
    //     });
    $('#add-price').on('click', function () {
            add_new_grosir_row();
    });
    $('body').on('click', '.remove-price', function () {

        $(this).parents('tr').remove();
    });
    $('#add-price-reseller').on('click', function () {
        add_new_reseller_row();
    });
    $('body').on('click', '.remove-price-reseller', function () {
        $(this).parents('tr').remove();
    });
    $('body').on('click', '.remove-item', function () {
        var id = $(this).attr("data-id");
        $('#rowID-'+id).remove();
    });
    $('#add-image').on('click', function () {
        // $('#filemanager').modal('show');
         $('#file_main_image').click()
    });
    $('#option-generate').on('click', function () {
        if ($('.options:checked').length > 0) {
            var html = '';
            var id = $('#id').val();
            var grosir_aktif = $('#grosir-slc').val();
            if(colour_size_on === 1){
            
                if(count_checkbox_colour <= 0 || count_checkbox_size <= 0){
                     
                    swal({
                        title: 'Jika variasi warna dan ukuran aktif anda perlu mengisi dua-duanya!',
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: 'Ok',
                        closeOnConfirm: false,
                    });
                     return false;
                }
            }
            $.ajax({
                url: site_url + 'products/productv2/option/generate',
                type: 'post',
                data: $('.options:checked, #id'),
                success: function (data) {
                    count_checkbox_size = 0
                    count_checkbox_colour = 0
                    $('#combination-list tbody').empty();
                    if (data) {
                        data = JSON.parse(data);
                        if (data.status == 'error') {
                            error_message(data.message);
                        } else {
                            var variant_status = '';
                            if(grosir_aktif === '1'){
                                 $.each(data.data, function (i, opt) {
                                    if(opt.status==='1'){variant_status = '<td><input type="hidden" name="options[' + opt.id + '][status]" value="0"><input type="checkbox" class="checkbox-status-variant" name="options[' + opt.id + '][status]" id="variant_status' + opt.id + '" value="1" checked><label class="label-status-variant" for="variant_status' + opt.id + '">Toggle</label></td>'}
                                    else{variant_status = '<td><input type="hidden" name="options[' + opt.id + '][status]" value="0" ><input type="checkbox" class="checkbox-status-variant" name="options[' + opt.id + '][status]" id="variant_status' + opt.id + '" value="1"><label class="label-status-variant" for="variant_status' + opt.id + '">Toggle</label></td>'}
                                    html += '<tr id="option-' + opt.id + '">' +
                                    '<td><input type="radio" class="form-control" checked="" name="default_option" value="' + opt.id + '"></td>' +
                                    '<td>' + opt.option_combination + '</td>' +
                                    '<td><div class="input-group"><span class="input-group-addon">Rp</span><input type="text" class="form-control number variant_price" required="" min="100" name="options[' + opt.id + '][price]" value="' + $('#price-input').val() + '"readonly></div></td>' +
                                    '<td><input type="number" class="form-control" required="" name="options[' + opt.id + '][stock]" value="' + opt.quantity + '"></td>' +
                                    variant_status +
                                    '<td>' +
                                    '<button type="button" class="btn btn-default btn-xs" onclick="set_image_option(' + id + ',' + opt.id + ')"><i class="icon-image2"></i></button>' +
                                    '<button type="button" class="btn btn-danger btn-xs" onclick="delete_option(' + opt.id + ')"><i class="icon-x"></i></button>' +
                                    '</td>' +
                                    '</tr>';
                                });
                                $('#combination-list tbody').append(html);
                                $('.number-grosir').number(true, decimal_digit, decimal_separator, thousand_separator);
                                $('.number-grosir').attr('autocomplete', 'off');

                            }
                            else{

                                $.each(data.data, function (i, opt) {
                                    if(opt.status==='1'){variant_status = '<td><input type="hidden" name="options[' + opt.id + '][status]" value="0"><input type="checkbox" class="checkbox-status-variant" name="options[' + opt.id + '][status]" id="variant_status' + opt.id + '" value="1" checked><label class="label-status-variant" for="variant_status' + opt.id + '">Toggle</label></td>'}
                                    else{variant_status = '<td><input type="hidden" name="options[' + opt.id + '][status]" value="0" ><input type="checkbox" class="checkbox-status-variant" name="options[' + opt.id + '][status]" id="variant_status' + opt.id + '" value="1"><label class="label-status-variant" for="variant_status' + opt.id + '">Toggle</label></td>'}
                                    html += '<tr id="option-' + opt.id + '">' +
                                    '<td><input type="radio" class="form-control" checked="" name="default_option" value="' + opt.id + '"></td>' +
                                    '<td>' + opt.option_combination + '</td>' +
                                    '<td><div class="input-group"><span class="input-group-addon">Rp</span><input type="text" class="form-control number variant_price" required="" min="100" name="options[' + opt.id + '][price]" value="' + opt.price + '"></div></td>' +
                                    '<td><input type="number" class="form-control" required="" name="options[' + opt.id + '][stock]" value="' + opt.quantity + '"></td>' +
                                    variant_status +
                                    '<td>' +
                                    '<button type="button" class="btn btn-default btn-xs" onclick="set_image_option(' + id + ',' + opt.id + ')"><i class="icon-image2"></i></button>' +
                                    '<button type="button" class="btn btn-danger btn-xs" onclick="delete_option(' + opt.id + ')"><i class="icon-x"></i></button>' +
                                    '</td>' +
                                    '</tr>';
                                });
                                $('#combination-list tbody').append(html);
                                $('.number-grosir').number(true, decimal_digit, decimal_separator, thousand_separator);
                                $('.number-grosir').attr('autocomplete', 'off');
                            }
                        }
                    }
                    $('.options').prop('checked',false);
                    $.uniform.update('.options');
                }
            });
                        // complete: function(){
                        //     $('#combination-list tbody').empty();
                        //     $.ajax({
                        //         url: site_url + 'products/productv2/get_new_variation_list',
                        //         type: 'post',
                        //         data: $('.options:checked, #id'),
                        //         success:function (data){
                        //             if (data) {
                        //                 data = JSON.parse(data);
                        //                 if (data.status == 'error') {
                        //                     error_message(data.message);
                        //                 }else{
                        //                     $.each(data.data, function (i, opt) {
                        //                     html += '<tr id="option-' + opt.id + '">' +
                        //                             '<td><input type="radio" class="form-control" name="default_option" value="' + opt.id + '"></td>' +
                        //                             '<td>' + opt.option_combination + '</td>' +
                        //                             '<td><input type="text" class="number form-control" name="options[' + opt.id + '][price]" value="' + opt.price + '"></td>' +
                        //                             '<td><input type="text" class="number form-control" name="options[' + opt.id + '][stock]" value="' + opt.stock + '"></td>' +
                        //                             '<td>' +
                        //                             '<button type="button" class="btn btn-default btn-xs" onclick="set_image_option(' + id + ',' + opt.id + ')"><i class="icon-image2"></i></button>' +
                        //                             '<button type="button" class="btn btn-danger btn-xs" onclick="delete_option(' + opt.id + ')"><i class="icon-x"></i></button>' +
                        //                             '</td>' +
                        //                             '</tr>';
                        //                     });
                        //                     $('#combination-list tbody').append(html);
                        //                     $('.number').number(true, decimal_digit, decimal_separator, thousand_separator);
                        //                     $('.number').attr('autocomplete', 'off');
                        //                 }

                        //             }
                        //         }

                        //     })
                        // },
           
        }
    });

    $('#submit').on('click',function(e){
        
        let count_error = $('.validation-error-label').length;
        console.log(count_error)
        if(count_error > 0){
            swal({
                title: 'Error',
                type: "warning",
                text: 'Cek kembali data-data yang anda masukan!',
                confirmButtonColor: "#DD6B55",
                confirmButtonText: 'Ok',
                closeOnConfirm: true,
            })
        }else{
            $(this).submit();
        }

    })


    option_select();
});

function add_new_grosir_row(){
    var count = $('#price-count').val();
    var id = Math.random().toString(36).slice(2);
    var html = '<tr>';
    html += '<td class="price-qty" width="10%"><input type="number" min="2" required name="price_level[' + id + '][qty]" class="form-control td-grosir-qty" ></td>';
    html += '<td class="grosir_price_col" width="30%"><div class="input-group"><span class="input-group-addon">Rp</span><input type="text" required min="100" name="price_level[' + id + '][price]" class="form-control number number-grosir" value="100"></div></td>';
    html += '<td><button type="button" class="btn btn-danger btn-xs remove-price">x</button></td></tr>';
    count = parseInt(count) + 1;
    $('#price-count').val(count);
    $('#price-list tbody').append(html);
    $('.number-grosir').number(true, decimal_digit, decimal_separator, thousand_separator);
    $('.number-grosir').attr('autocomplete', 'off');
}

function add_new_reseller_row(){
    var count = $('#price-count-reseller').val();
    var id = Math.random().toString(36).slice(2);
    var html = '<tr>';
    html += '<td class="price-qty-reseller" width="10%"><input type="number" required min="2" name="price_reseller_list[' + id + '][qty]" class="form-control td-reseller-qty" ></td>';
    html += '<td class="reseller_price_col" width="30%"><div class="input-group"><span class="input-group-addon">Rp</span><input type="text" required min="100" name="price_reseller_list[' + id + '][price]" class="form-control number number-reseller" value="100"></div></td>';
    html += '<td><button type="button" class="btn btn-danger btn-xs remove-price-reseller">x</button></td></tr>';
    count = parseInt(count) + 1;
    $('#price-count-reseller').val(count);
    $('#price-reseller-list tbody').append(html);
    $('.number-reseller').number(true, decimal_digit, decimal_separator, thousand_separator);
    $('.number-reseller').attr('autocomplete', 'off');
}

function other_brand(){
    var brand = $('#brand option:selected').text();
    var input = '<input type="text" name="brand_other" id="brand_other"  class="form-control" value="" required>';
    if(brand == 'Other'){
        $('#brand_other_div').html(input);
        $('#brand_other').focus();
    } else {
        $('#brand_other_div').html(' ');
    }
    
        
}

function responsive_filemanager_callback(field_id) {
    if (field_id) {
        var url = $('#' + field_id).val();
        var final_url = 'merchant/' + url;
        var username = $('#username').val();
        var primary = 0;
        if (!$("input:radio[name='image_primary']").is(":checked")) {
            primary = 1;
        }
        $.ajax({
            url: site_url + 'products/productv2/image',
            type: 'post',
            data: {act: 'upload', image: final_url, username: username, product: $('input[name=id]').val(), primary: primary},
            success: function (id) {
//                var id = Math.random().toString(36).slice(2);
                var html = '<div class="image" id="img' + id + '">';
                html += '<div class="image-bg" style="background-image: url(\'' + base_url + '../files/images/' + final_url + '\')"></div>';
                html += '<div class="option">';
                html += '<div class="radio"><label><input type="radio" value="' + id + '" name="image_primary" id="image_primary' + id + '">Default</label></div>';
                html += '<div><a href="javascript:delete_image(\'' + id + '\')" class="text-muted pull-right"><i class="icon-trash"></i></a></div>';
                html += '</div>';
                html += '</div>';
                $(html).insertBefore('#add-image');
                if (primary)
                    $('#image_primary' + id).prop('checked', true);
            }
        });
    }
}


function delete_image(id) {
    swal({
        title: lang.message.confirm.delete_image,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: lang.button.delete,
        cancelButtonText: lang.button.cancel,
        closeOnConfirm: false,
        closeOnCancel: true
    }, function () {
        $.ajax({
            url: site_url + 'products/productv2/image',
            type: 'post',
            data: {act: 'delete', id: id},
            success: function () {
                $('#img' + id).remove();
                if (!$("input:radio[name='image_primary']").is(":checked")) {
                    $("input:radio[name='image_primary']").prop('checked', true);
                }
                swal.close()
            }
        });
    });
}

function option_select() {
    $('#option-image .image').on('click', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).children('input').prop('checked', false);
        } else {
            $(this).addClass('active');
            $(this).children('input').prop('checked', true);
        }
    });
}

function set_image_option(product, product_option) {
    $.ajax({
        url: site_url + 'products/productv2/option/get_image',
        type: 'post',
        data: {product: product, product_option: product_option},
        success: function (res) {
            res = JSON.parse(res);
            if (res.length > 0) {
                var html = '';
                $.each(res, function (i, img) {
                    html += '<div class="image ' + (img.product_option ? 'active' : '') + '"><input type="checkbox" name="images[]" value="' + img.id + '" ' + (img.product_option ? 'checked' : '') + '><img src="' + base_url + '../files/images/' + img.image + '"></div>';
                });
                $('#option-image .modal-body').html(html);
                $('input[name=product_option]').val(product_option);
                $('#option-image').modal('show');
                option_select();
            } else {
                swal('Tidak ada gambar produk.', '', 'error');
            }
        }
    });

}

function save_image_option() {
    $.ajax({
        url: site_url + 'products/productv2/option/save_image',
        type: 'post',
        data: $('#option-image input:checked, input[name=product_option], #id'),
        success: function () {
            $('#option-image').modal('hide');
        }
    });
}

function delete_option(id) {
    swal({
        title: lang.message.confirm.delete,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: lang.button.delete,
        cancelButtonText: lang.button.cancel,
        closeOnConfirm: false,
        closeOnCancel: true
    }, function () {
        $.ajax({
            url: site_url + 'products/productv2/option/delete',
            type: 'post',
            data: {product_option: id},
            success: function () {
                $('#option-' + id).remove();
                swal.close()
            }
        });
    });
}

function get_table_deskripsi($idkat){
    var id = $('#category').val();
    var tr = '';
    var div = '';
    $('#table_desk tbody').html('');
    $('#add_param_table_desk').html('');
    $.ajax({
        method: 'post',
        url: site_url + 'products/productv2/get_table_description',
        data: {
            id: id
        },
        success: function(data) {
            data = JSON.parse(data);
            var i;
            for(i in data){
                var key = Math.random().toString(36).slice(2);
                tr += '<tr>';
                tr += '<td style="width:200px">'+ data[i].nama +'</td>';
                tr += '<input type="hidden" name="table_description['+key+'][row_id]" id="tabledeskrowid'+key+'"  class="form-control" value="'+key+'">';
                tr += '<input type="hidden" name="table_description['+key+'][id_deskripsi]" id="tabledescriptionid'+key+'"  class="form-control" value="'+data[i].id_deskripsi+'">';
                tr += '<input type="hidden" name="table_description['+key+'][name]" id="tabledescriptionname'+key+'"  class="form-control" value="'+data[i].nama+'">';
                tr += '<td><input type="text" name="table_description['+key+'][value]" id="tabledescription'+key+'"  class="form-control" placeholder="Silahkan input '+data[i].nama+' produk Anda" required>';
                tr += '</td>';
                tr += '</tr>';
            }
            $(".add_param").remove();
            div += '<br><a onclick="add_param()" class="add_param btn btn-success">Tambah Spesifikasi</a><br><br>';
            $('#table_desk tbody').append(tr);
            $('#add_param_table_desk').html(div);
        }
    });
}

function add_param(){
    var tr = '';
    var i = $('#count_btn').val();
    var key = Math.random().toString(36).slice(2);
    i++;
    tr += '<tr id="tr'+i+'">';
    tr += '<td style="width:200px">';
    tr += '<input type="hidden" value="'+key+'" name="table_description['+key+'][row_id]" id="param'+i+'"  class="form-control" placeholder="Parameter" required>';
    tr += '<input type="hidden" name="table_description['+key+'][id_deskripsi]" id="tabledescriptionid'+key+'"  class="form-control" value="0">';
    tr += '<input type="text" name="table_description['+key+'][name]" id="param'+i+'"  class="form-control" placeholder="Nama Spesifikasi" required>';
    tr += '</td>'; 
    tr += '<td><input type="text" name="table_description['+key+'][value]" id="value'+i+'"  class="form-control" placeholder="Value" required></td>';
    tr += '<td style="width:60px"><a onclick="delete_param('+i+')" class="btn btn-danger"><i class="icon-trash"></></a></td>';
    tr += '</tr>';
    
    $('#param_other_table_desk').append(tr);
    $('#count_btn').val(i);
}

function delete_param(id){
    $('#tr'+id).remove();
    var i = $('#count_param').val();
    i--;
    $('#count_param').val(i);
}

//function add_option() {
//    var id = $('#list-option').val();
//    if (id == '') {
//        error_message('Variasi belum dipilih!');
//        return;
//    }
//    $.ajax({
//        type: 'post',
//        url: site_url + 'catalog/products/get_option/' + id,
//        success: function (data) {
//            data = JSON.parse(data);
//            id = Math.random().toString(36).slice(2);
////            console.log(data);
//            var option = data.option;
//            var variants = data.variants;
//            var html = '<div class="tab-pane" id="option-' + id + '">';
//            html += '<input type="hidden" name="options[' + id + '][product_option]" value="">';
//            html += '<input type="hidden" name="options[' + id + '][option]" value="' + option.id + '">';
//            html += '<div class="form-group">';
//            html += '<label class="col-md-3 control-label">Required?</label>';
//            html += '<div class="col-md-9"><input type="checkbox" name="options[' + id + '][required]" value="1" data-on-text="Yes" data-off-text="No" class="switch" checked="checked"></div>';
//            html += '</div>';
//            html += '  <div class="table-responsive">';
//            html += '   <table id="option-variant-' + id + '" class="table">';
//            html += '     <thead>';
//            html += '       <tr>';
//            html += '         <th style="width: 50%">Variasi</th>';
//            html += '         <th style="width: 25%">Harga</th>';
//            html += '         <th style="width: 20%">Berat</th>';
//            html += '         <th style="width: 5%"></th>';
//            html += '       </tr>';
//            html += '     </thead>';
//            html += '     <tbody></tbody>';
//            html += '     <tfoot>';
//            html += '       <tr>';
//            html += '         <th colspan="4" class="text-center"><button type="button" onclick="add_option_variant(\'' + id + '\');" class="btn btn-default">Tambah Variasi</button></th>';
//            html += '       </tr>';
//            html += '     </tfoot>';
//            html += '   </table>';
//            html += '  </div>';
//
//            html += '<select id="option-variants-' + id + '" style="display: none;">'
//            $.each(variants, function (i, d) {
//                html += '<option value="' + d.id + '">' + d.value + '</option>';
//            });
//            html += '</select>';
//            html += '</div>';
//
//            $('#option .tab-content').append(html);
//            $(".switch").bootstrapSwitch();
//            $('#option .nav').append('<li><a href="#option-' + id + '" data-toggle="tab"><span class="label label-danger pull-right" onclick="$(\'a[href=\\\'#option-' + id + '\\\']\').parent().remove(); $(\'#option-' + id + '\').remove(); $(\'#option .nav a:first\').tab(\'show\')"><i class="icon-x"></i></span> ' + option.name + '</a></li>');
//            $('#option .nav a[href=\'#option-' + id + '\']').tab('show');
//        }
//    });
//}
//
//function add_option_variant(id) {
//    var variant_id = Math.random().toString(36).slice(2);
//    var html = '<tr id="option-variant-row-' + variant_id + '">';
//    html += ' <td><select name="options[' + id + '][variant][' + variant_id + '][option_variant]" class="form-control">';
//    html += $('#option-variants-' + id).html();
//    html += '</select><input type="hidden" name="options[' + id + '][variant][' + variant_id + '][product_option_variant]" value=""></td>';
//    html += '<td><input type="text" class="form-control number" name="options[' + id + '][variant][' + variant_id + '][price]" value="0"></td>';
//    html += '<td><div class="input-group"><input type="number" name="options[' + id + '][variant][' + variant_id + '][weight]" class="form-control" value="0"><span class="input-group-addon">gram</span></div></td>';
//    html += '<td><button type="button" class="btn btn-danger" onclick="$(\'#option-variant-row-' + variant_id + '\').remove();"><i class="icon-x"></i></td>';
//    html += '</tr>';
//    $('#option-' + id + ' tbody').append(html);
//    $('.number').number(true, decimal_digit, decimal_separator, thousand_separator);
//    $('.number').attr('autocomplete', 'off');
//}