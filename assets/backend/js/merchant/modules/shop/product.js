$(document).ready(function() {
    $("#product #featuted-image").owlCarousel({
        navigation: true,
        pagination: false,
        autoPlay: false,
        items: 1,
        slideSpeed: 200,
        paginationSpeed: 800,
        rewindSpeed: 1000,
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [979, 1],
        itemsTablet: [768, 1],
        itemsTabletSmall: [540, 1],
        itemsMobile: [360, 1],
        navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
    });
    $(".information_content .panel-title").click(function() {
        if ($(this).find("i").hasClass("fa-angle-up")) {
            $(this).find("i").removeClass("fa-angle-up");
            $(this).find("i").addClass("fa-angle-down");
        } else {
            $(this).find("i").removeClass("fa-angle-down");
            $(this).find("i").addClass("fa-angle-up");
        }
    });
    //    $('#gallery-images div.image').on('click', function () {
    //        var $this = $(this);
    //        var parent = $this.parents('#gallery-images');
    //        parent.find('.image').removeClass('active');
    //        $this.addClass('active');
    //    });
    $('.swatch-element.color').click(function() {
        if (!$(this).hasClass('active')) {
            $('.swatch-element.active').removeClass('active');
            $(this).addClass('active');
        }
    });
    $("#featuted-image a.fancybox").fancybox({
        openEffect: 'none',
        closeEffect: 'none',
        loop: true,
        smallBtn: false,
        toolbar: false
    });
    $('#product-option select, #product-option input').on('change', function() {
        change_option();
    });
    $('input[name=qty]').keyup(function(){
        var qty = parseInt($(this).val());
        if(isNaN(qty) || qty < 1){
            $(this).val(1);
        }else{
            $(this).val(qty);
        }
    })
    $('#btn-cart').click(function() {
        $.ajax({
            url: site_url + 'shop/add_to_cart',
            type: 'post',
            data: $('#product-information input[type=text], #product-information input[type=radio]:checked, #product-information select, #product-information input[type=hidden]'),
            beforeSend: function() {
                $('#btn-cart').button('loading');
            },
            complete: function() {
                $('#btn-cart').button('reset');
            },
            success: function(data) {
                data = JSON.parse(data);
                //                $('.swatch').removeClass('has-error');
                if (data.status == 'error') {
                    $('.product-price').before('<div class="alert alert-danger">' + data.message + '</div>');
                    //                    $.each(data.error.option, function (i, d) {
                    //                        $('#option-' + d).addClass('has-error');
                    //                    });
                } else {
                    window.location.href = site_url + 'shop/cart';
                }
            }
        })
    });
    $('#view-list-grochire').popover({
        html: true,
        content: function() {
            return $('.grochire .popover').html();
        }
    });
});
$(window).load(function() {
    // The slider being synced must be initialized first
    $('#carousel').flexslider({
      animation: "slide",
      controlNav: false,
      animationLoop: false,
      slideshow: false,
      itemWidth: 100,
      itemMargin: 5,
      asNavFor: '#slider'
    });

    $('#slider').flexslider({
      animation: "slide",
      controlNav: false,
      animationLoop: false,
      slideshow: false,
      sync: "#carousel"
    });
});
function formatRupiah(angka, prefix){
    angka = angka.toString();
	var number_string = angka.replace(/[^,\d]/g, '').toString(),
	split   		= number_string.split(','),
	sisa     		= split[0].length % 3,
	rupiah     		= split[0].substr(0, sisa),
	ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
	// tambahkan titik jika yang di input sudah menjadi angka ribuan
	if(ribuan){
		separator = sisa ? '.' : '';
		rupiah += separator + ribuan.join('.');
	}
 
	rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
	return prefix == undefined ? rupiah : (rupiah ? 'Rp ' + rupiah : '');
}
function addtoCart() {
    $.ajax({
        url: site_url + 'shop/catalog/products/add_to_cart_ajax',
        type: 'post',
        data: $('#product-information input[type=text], #product-information input[type=radio]:checked, #product-information select, #product-information input[type=hidden]'),
        success: function(data) {
            data = JSON.parse(data);
            if (data.status == 'error') {
                $('.product-price').before('<div class="alert alert-danger">' + data.message + '</div>');
            } else {
                var length = Object.keys( data.items ).length;
                var total = 0;
                var total_qty = 0;
                var html = '<div class="items control-container">';
                if(length > 0) {
                    $('#cart-target .cart-info').attr('style','display:block');
                    var redir_url = "'"+site_url+'cart'+"'";
                    $.each(data.items , function(index, val) {
                        var id = val.id.split("-");
                        total = total + (val.price * val.qty);
                        total_qty = total_qty + val.qty;
                        html += '<div class="row"><div class="cart-left">';
                        html += '<a class="cart-image" href="catalog/products/view/'+id[0]+'">';
                        html += '<img src="'+site_url+'files/images/'+val.image+'" alt="" title=""></a></div>';
                        html += '<div class="cart-right">';
                        html += '<div class="cart-title"><a href="">'+val.name+'</a></div>';
                        html += '<div class="cart-price"><span class="money">'+formatRupiah(val.price,'.')+'</span><span class="x"> x '+val.qty+'</span></div>';
                        html += '</div></div>';
                    });
                    html += '</div> <div class="subtotal"><span>Subtotal:</span><span class="cart-total-right money">'+formatRupiah(total,'.')+'</span></div>';
                    html += '<div class="action"><button class="btn" style="font-size:10px" onclick="window.location = '+redir_url+'">Lihat Keranjang<i class="fa fa-caret-right"></i></button><button class="btn float-right" style="font-size:10px" onclick="window.location = '+redir_url+'">Beli Sekarang<i class="fa fa-caret-right"></i></button></div>';
                } else {
                    html += '<p style="padding: 15px;">Keranjang masih kosong.</p>';
                }
                html += '</div>';
                $('#cart-target .badge-cart').html(total_qty);
                $('#cart-button .badge-cart').html(total_qty);
                $('#cart-target-mobile .number').html(total_qty);
                $('#cart-target .cart-info .cart-content').html(html);
                $('#cart-target-mobile .cart-info .cart-content').html(html);
            }
        }
    })
}
function change_option() {
    $.ajax({
        url: site_url + 'shop/catalog/products/change_option',
        type: 'post',
        data: $('#product-option input:checked, #product-option select, input[name=product]'),
        success: function(res) {
            res = JSON.parse(res);
            $('#product #featuted-image').owlCarousel().trigger('remove.owl.carousel');
            $('#product .show-image-load').after('<div id="featuted-image" class="image featured">' + res.images + '</div>');
            $("#product #featuted-image").owlCarousel({
                navigation: true,
                pagination: false,
                autoPlay: false,
                items: 1,
                slideSpeed: 200,
                paginationSpeed: 800,
                rewindSpeed: 1000,
                itemsDesktop: [1199, 1],
                itemsDesktopSmall: [979, 1],
                itemsTablet: [768, 1],
                itemsTabletSmall: [540, 1],
                itemsMobile: [360, 1],
                navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
            });
            $("#featuted-image a.fancybox").fancybox({
                openEffect: 'none',
                closeEffect: 'none',
                loop: true,
                smallBtn: false,
                toolbar: false
            });
            $('#product-option').html(res.options);
            $('#product-option select, #product-option input').on('change', function() {
                change_option();
            });
        }
    });
}
function addWishlist(id,uri) {
    $.ajax({
        method: 'post',
        url: site_url + 'shop/catalog/products/add_wishlist_ajax',
        data: {
            id: id
        },
        success: function(data) {
            data = JSON.parse(data);
           if(data.status_code == 'login_first'){
                window.location.href = site_url + 'shop/member/login?back='+uri;
           } else if (data.status_code == 'add_wishlist_success') {
            $('.add-to-wishlist .wish-list').attr('style','background-color:#000');
            $('.add-to-wishlist .fa-heart').attr('style','color:#fff');
           } else {
            $('.add-to-wishlist .wish-list').attr('style','background-color:#fff');
            $('.add-to-wishlist .fa-heart').attr('style','color:#000');
           }
        }
    });
}
function likeDislike(type, id) {
    var likes = $('#likedislike' + id).find('.likes');
    var dislikes = $('#likedislike' + id).find('.dislikes');
    if (type == 1) {
        likes.text(parseInt(likes.text()) + 1);
        $('#likedislike' + id).find('.like').addClass('current');
    } else if (type == 2) {
        dislikes.text(parseInt(dislikes.text()) + 1);
        $('#likedislike' + id).find('.dislike').addClass('current');
    }
    $('#likedislike' + id).find('div').removeAttr('onclick');
    $.ajax({
        method: 'post',
        url: base_url + 'catalog/products/submitLikeDislike',
        data: {
            id: id,
            type: type
        },
        success: function(data) {
            $('#likedislike' + id).find('.notif').html(data);
        }
    });
}
