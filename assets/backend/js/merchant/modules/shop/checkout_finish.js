$(document).ready(function () {
    $('#payment-confirmation').click(function () {
        $('#modal-payment').modal('show');
        $('#message').html('');
    });
});
function submit_payment() {
    $.ajax({
        url: site_url + 'auth/member/payment_confirmation',
        type: 'post',
        data: $('#modal-payment input, select, textarea'),
        success: function (response) {
            response = JSON.parse(response);
            if (response.status == 'success') {
                window.location.href = site_url+'member/order_detail/'+response.id;
            } else {
                $('#message').html(response.message);
            }
        }
    })
}