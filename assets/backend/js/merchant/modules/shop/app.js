var slideIndex = 0;
$(function () {
    showDivs(slideIndex);

    $('#search_box').keyup(function (e) {
        var input = $(this).val();
        $.ajax({
            type: "GET",
            url: site_url + "shop/catalog/search/suggest",
            data: {keyword: input},
            async: true,
            success: function (data) {
                console.log(data);
                var outWords = $.parseJSON(data);
                $('#search_suggest').html('');
                $('#search_suggest').show();

                for (x = 0; x < outWords.length; x++) {
                    $('#search_suggest').prepend('<a href="' + outWords[x].url + '">' + outWords[x].name + '</a>');
                }

            }
        });
    });
    $('#search_box').focusout(function () {
        setTimeout(function () {
            $('#search_suggest').hide();
        }, 1000);
    });
//    var is_mobile = window.matchMedia("(max-width: 767px)");
//    if (is_mobile) {
//        $('#top').addClass('sticky');
//    }
    $('.navigation_mobile .arrow').on('click', function () {
        if ($(this).attr('class') == 'arrow class_test') {
            $('.navigation_mobile .arrow').removeClass('class_test');
            $('.navigation_mobile').removeClass('active');
            $('.navigation_mobile').find('.menu-mobile-container').hide("slow");
        } else {
            $('.navigation_mobile .arrow').removeClass('class_test');
            $(this).addClass('class_test');
            $('.navigation_mobile').each(function () {
                if ($(this).find('.arrow').attr('class') == 'arrow class_test') {
                    $(this).find('.menu-mobile-container').show("slow");
                    $(this).addClass('active');
                } else {
                    $(this).find('.menu-mobile-container').hide("slow");
                    $(this).removeClass('active');
                }
            });
        }
    });
    $('#btn-subscribe').on('click', function () {
        var email = $('#email-subscribe').val();
        if (is_email(email)) {
            $.ajax({
                type: "post",
                url: site_url + "shop/auth/subscription",
                data: {email: email},
                success: function (data) {
                    $('.footer_newsoc_inner').parent().prepend(data);
                }
            });
        }
        return false;
    })
});

function is_email(email) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(email);
}

function my_location() {
    $('#modal-location').find('.modal-body').load(site_url + 'location/modal_location');
    $('#modal-location').modal('show');
}

function plusDivs(n) {
    showDivs(slideIndex += n);
}

function currentDiv(n) {
    showDivs(slideIndex = n);
}

function showDivs(n) {
    var i;
    var x = $('.hssEach');
    if (n == x.length) {
        slideIndex = 0;
    }
    if (n < 0) {
        slideIndex = (x.length - 1)
    }
    $('#slide-id').text(slideIndex + 1);
    x.each(function () {
        $(this).hide();
        if ($(this).index() == slideIndex) {
            $(this).show();
        }
    });
}
//function set_location(latitude, longitude) {
//    $.ajax({
//        type: 'post',
//        url: site_url + 'location/set',
//        data: {lat: latitude, lng: longitude},
//        success: function (data) {
//            var location = {
//                lat: latitude,
//                lng: longitude,
//            }
//            localStorage.setItem('tmmLoc', location);
//        }
//    });
//}