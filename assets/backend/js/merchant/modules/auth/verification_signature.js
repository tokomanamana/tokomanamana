var canvas = document.getElementById('signature-pad');

// Adjust canvas coordinate space taking into account pixel ratio,
// to make it look crisp on mobile devices.
// This also causes canvas to be cleared.
function resizeCanvas() {
    // When zoomed out to less than 100%, for some very strange reason,
    // some browsers report devicePixelRatio as less than 1
    // and only part of the canvas is cleared then.
    var ratio =  Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
}

window.onresize = resizeCanvas;
resizeCanvas();

var signaturePad = new SignaturePad(canvas, {
  backgroundColor: '#ccc' // necessary for saving image as JPEG; can be removed is only saving as PNG or SVG
});
document.getElementById('clear-canvas').addEventListener('click', function () {
    signaturePad.clear();
  });
function agreeSnk() {
    $("input[name=agree]").prop("checked", true);
    $('#terms').trigger('click.dismiss.bs.modal');
}
function openSnKmodal() {
    $('#terms').modal('show');;
}
function closeSnKmodal() {
    $('#terms').trigger('click.dismiss.bs.modal');
}
$('#verif_submit').on('click', function(e){
    if(!signaturePad.isEmpty()){
        $('#base64_img').val(encodeURIComponent(signaturePad.toDataURL().replace(/^data:image\/(png|jpg);base64,/, '')));
    }
});