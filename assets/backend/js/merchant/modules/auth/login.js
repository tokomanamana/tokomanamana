$(function () {
    $('#login').submit(function () {
        $('#login').ajaxSubmit({
            success: function (data) {
                data = JSON.parse(data);
                if (data.status == 'success') {
                    window.setTimeout(function () {
                        window.location = data.redirect;
                    }, 1000);
                } else {
                    swal({
                        title: "Oops...",
                        text: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error",
                        html: true
                    });
                }
            }
        });
        return false;
    });
    $('#phone_login').submit(function () {
        $('#phone_login').ajaxSubmit({
            success: function (data) {
                data = JSON.parse(data);
                if (data.status == 'success') {
                    window.setTimeout(function () {
                        window.location = data.redirect;
                    }, 1000);
                } else {
                    swal({
                        title: "Oops...",
                        text: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error",
                        html: true
                    });
                }
            }
        });
        return false;
    });
});
function request_otp() {
    //alert('aaa');
    var phone = $('form input[name=phoneNum]').val();
    //console.log(site_url);
    if (!validationPhone(phone)) {
        $('#via_phone .message').html('<div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Format No. Handphone salah!</div></div>');
    } else {
        $.ajax({
            url: site_url + 'auth/request_otp_login',
            type: 'post',
            data: {phone: phone},
            success: function (res) {
                res = JSON.parse(res);
                if (res.status == 'error') {
                    $('#via_phone .message').html('<div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Tidak ada akun terkait dengan No. Handphone Anda!</div></div>');
                } else {
                    swal(res.msg, '', 'success');
                    $('#btn-token').attr('disabled', 'disabled');
                    $('#btn-token').text('Kirim Ulang (120s)');
                    countdown(120);
                }
            }
        })
    }
}
function validationPhone(phone) {
    if (phone == null || phone == '') {
        return false;
    }
    var number = /^[0-9]+$/;
    if (!phone.match(number)) {
        return false;
    }
    if (phone.length < 6) {
        return false;
    }
    return true;
}
function countdown(duration) {
    var countdown = setInterval(function () {
        if (--duration) {
            $('#btn-token').text('Kirim Ulang (' + duration + 's)');
        } else {
            clearInterval(countdown);
            $('#btn-token').removeAttr('disabled');
            $('#btn-token').text('Kirim Kode');
        }
    }, 1000);
}
$(".toggle-password").click(function() {
    $(this).toggleClass("fa-eye icon-eye-slash");
    var input = $('#password');
    if (input.attr("type") == "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
    }
    });