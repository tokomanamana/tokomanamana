$(function () {
    countdown(time_left_verification);
    $('#request-token').on('click', function () {
        $.ajax({
            url: site_url + 'auth/request_token',
            success: function (data) {
                $('#wait').show();
                $('#wait span').text('120');
                countdown(120);
                $('#request-token').hide();
            }
        })
    });
});
function countdown(duration) {
    var countdown = setInterval(function () {
        duration = duration-1;
        if (duration>0) {
            $('#wait span').text(duration);
        } else {
            clearInterval(countdown);
            $('#wait').hide();
            $('#request-token').show();
        }
    }, 1000);
}