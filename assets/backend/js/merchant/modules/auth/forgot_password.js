$(function () {
    $('#forgot-password').submit(function () {
        $('#forgot-password').ajaxSubmit({
            success: function (data) {
                data = JSON.parse(data);
                showNotify(data.message, data.status,data.redirect);
            }
        });
        return false;
    });

     $('#forgot-password_success').submit(function () {
        $('#forgot-password_success').ajaxSubmit({
            success: function (data) {
                data = JSON.parse(data);
                showNotify(data.message, data.status,data.redirect);
            }
        });
        return false;
    });





    $("#forgot-password_success").validate({
        // ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        rules: {
            old_password: {
                required: true,
                minlength: 6,
            },
            new_password: {
                minlength: 6
            },
            new_password_confirm: {
                equalTo: '#new_password'
            },
        },
        messages: {
           

        },
        onkeyup: true, 
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        // Different components require proper error label placement
        errorPlacement: function (error, element) {
            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                } else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            }


            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function (label) {
            label.remove();
            label.closest('.form-group').removeClass('has-error');
        },
        submitHandler: function (form) {
            return false;
        }
    });
});

function showNotify(message, status,redirect) {
    if(status==='error'){
        swal({
        title: "Oops...",
        text: message,
        type: "warning",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: 'OK',
        closeOnConfirm: true,
    })

    }else{
        setTimeout(function() {
            swal({
            title: "Success!",
            text: message,
            type: "success",
            confirmButtonColor: "#97C23C",
            confirmButtonText: 'OK',
            closeOnConfirm: true
            },function(){

                window.location = redirect;
            });
        },1000);
            

    }
    
}