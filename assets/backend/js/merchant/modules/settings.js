$(function () {
    $('#province').on('change', function () {
        $('#district').html('<option value=""></option>');
        if ($(this).val() == "") {
            $('#city').html('<option value=""></option>');
        } else {
            $.ajax({
                type: "POST",
                url: site_url + 'settings/get_cities',
                data: {id: $(this).val()},
                success: function (data) {
                    $('#city').html(data);
                }
            });
        }
    });
    $('#city').on('change', function () {
        if ($(this).val() == "") {
            $('#district').html('<option value=""></option>');
        } else {
            $.ajax({
                type: "POST",
                url: site_url + 'settings/get_districts',
                data: {id: $(this).val()},
                success: function (data) {
                    $('#district').html(data);
                }
            });
        }
    });
    $('a[href=#address]').on('click', function () {
        setTimeout(function () {
            x = map.getZoom();
            c = map.getCenter();
            google.maps.event.trigger(map, 'resize');
            map.setZoom(x);
            map.setCenter(c);
        }, 50);
    });
});

function setLocation(lat, lng) {
    $('#lat').val(lat);
    $('#lng').val(lng);
}