$(document).ready(function () {
    $('#province').change(function (e) {
        e.preventDefault();
        var id = $(this).val();
        getCities(id);
    });
    $('#city').change(function (e) {
        e.preventDefault();
        var id = $(this).val();
        getDistricts(id);
    });
    $('#change_password').change(function () {
        if (this.checked) {
            $('.password').attr('required', 'required');
            $('#password').show();
        } else {
            $('.password').removeAttr('required');
            $('#password').hide();
        }
    });
});
function getCities(province) {
    $('#district option').remove();
    $("#district").append('<option value="">Pilih kecamatan</option>');
    if (province == '' || province == 0) {
        $('#city option').remove();
        $("#city").append('<option value="">Pilih kota</option>');
    } else {
        $.get(base_url + 'checkout/getCities/' + province, function (data) {
            $('#city').html(data);
        });
    }
}
function getDistricts(city) {
    if (city == '' || city == 0) {
        $('#district option').remove();
        $("#district").append('<option value="">Pilih kecamatan</option>');
    } else {
        $.get(base_url + 'checkout/getDistricts/' + city, function (data) {
            $('#district').html(data);
        });
    }
}