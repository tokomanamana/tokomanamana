$(document).ready(function () {
    $('#province').change(function (e) {
        e.preventDefault();
        var id = $(this).val();
        getCities(id);
    });
    $('#city').change(function (e) {
        e.preventDefault();
        var id = $(this).val();
        getDistricts(id);
    });
    $('#district').change(function (e) {
        e.preventDefault();
        var id = $(this).val();
        getShippings(id);
    });
//    $('#submit').click(function (e) {
//        e.preventDefault();
//        $('#form-shipping_payment').submit(function (e) {
//            if (jQuery(':radio[name=shipping]', '#select-shipping').length) {
//                return true;
//            } else {
//                alert('Jasa pengiriman tidak mencakupi wilayah kota Anda. Hubungi kami untuk bantuan.');
//                return false;
//            }
//        });
//    })
    $('#form-shipping_payment').submit(function (e) {
        if (jQuery(':radio[name=shipping]', '#select-shipping').length) {
            return true;
        } else {
            alert('Jasa pengiriman tidak mencakupi wilayah kota Anda. Hubungi kami untuk bantuan.');
            return false;
        }
    })
});
function getCities(province) {
    $('#district option').remove();
    $("#district").append('<option value="">Pilih kecamatan</option>');
    if (province == '' || province == 0) {
        $('#city option').remove();
        $("#city").append('<option value="">Pilih kota</option>');
        $('#select-shipping').children().remove();
        $('#select-shipping').html('<p>Pilih provinsi terlebih dahulu.</p>');
    } else {
        $.get(base_url + 'checkout/getCities/' + province, function (data) {
            $('#city').html(data);
            $('#select-shipping').children().remove();
            $('#select-shipping').html('<p>Pilih kota terlebih dahulu.</p>');
        });
    }
}
function getDistricts(city) {
    if (city == '' || city == 0) {
        $('#district option').remove();
        $("#district").append('<option value="">Pilih kecamatan</option>');
        $('#select-shipping').children().remove();
        $('#select-shipping').html('<p>Pilih kota terlebih dahulu.</p>');
    } else {
        $.get(base_url + 'checkout/getDistricts/' + city, function (data) {
            $('#district').html(data);
            $('#select-shipping').children().remove();
            $('#select-shipping').html('<p>Pilih kecamatan terlebih dahulu.</p>');
        });
    }
}
function getShippings(district) {
    $('#shipping-cost').html(rp(0));
    $('#shipping-cost-promo').html(rp(0));
    if (district == '' || district == 0) {
        $('#select-shipping').children().remove();
        $('#select-shipping').html('<p>Pilih kecamatan terlebih dahulu.</p>');
    } else {
        $.get(base_url + 'checkout/getShippings/' + district, function (data) {
            $('#select-shipping').html(data);
        });
    }
}
function selectShipping(el) {
    $(el).parents('#select-shipping').find('.cart-shipping-block').removeClass('cart-shipping-block-active');
    $(el).parents('.cart-shipping-block').addClass('cart-shipping-block-active');
    var shipping_cost = parseFloat($(el).attr('data-cost'));
//    var shipping_cost_promo = parseFloat($(el).attr('data-promo'));
    var total = parseFloat($('#total').val());
    $('#shipping-cost').html(rp(shipping_cost));
//    $('#shipping-cost-promo').html('-' + rp(shipping_cost_promo));
    $('#grand-total').html(rp(total + shipping_cost));
}
function selectPayment(el) {
    $(el).parents('#select-payment').find('.cart-shipping-block').removeClass('cart-shipping-block-active');
    $(el).parents('.cart-shipping-block').addClass('cart-shipping-block-active');
}
function rp(number) {
    return "Rp " + parseFloat(number).toFixed(0).replace(/./g, function (c, i, a) {
        return i > 0 && c !== "," && (a.length - i) % 3 === 0 ? "." + c : c;
    });
}