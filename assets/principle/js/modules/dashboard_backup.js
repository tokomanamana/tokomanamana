$(function () {
    am4core.useTheme(am4themes_animated);
    var chart = am4core.create("chartdiv", am4charts.XYChart);
    $.ajax({
        url: site_url + 'dashboard/sales_summary',
        success: function (data) {
            data = JSON.parse(data);
            chart_data = [];
            let test = new Date();
            let this_date = test.getDate();
            let past_date = this_date - 6;
            let future_date = this_date + 1;
            let value_data = [];
            let value_data2 = [];
            for(let i = 0; i < 7; i++) {
                if(data[i]) {
                    value_data[data[i].date] = data[i].sales;
                    value_data2[data[i].date] = data[i].transaction;
                }
            }
            for(let i = past_date; i < future_date; i++) {
                let date = new Date();
                let value = [];
                let value2 = [];
                date.setDate(i);
                if(value_data[i] && value_data2[i]) {
                    value.push((value_data[i]));
                    value2.push((value_data2[i]));
                } else {
                    value.push(0);
                    value2.push(0);
                }
                chart_data.push({
                    date: date,
                    value: value,
                    value2: value2
                });
            }
            chart.data = chart_data;
        }
    });


    chart.width = am4core.percent(100);
    chart.height = am4core.percent(100);

    // Create axes
    var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    dateAxis.renderer.minGridDistance = 50;
    dateAxis.renderer.grid.template.location = 0.5;
    dateAxis.startLocation = 0.5;
    dateAxis.endLocation = 0.5;

    // Create value axis1
    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.title.text = "Pendapatan";

    // Create series1
    var series1 = chart.series.push(new am4charts.LineSeries());
    series1.dataFields.valueY = "value";
    series1.dataFields.dateX = "date";
    series1.strokeWidth = 3;
    series1.tensionX = 0.8;
    series1.bullets.push(new am4charts.CircleBullet());
    series1.connect = false;
    series1.yAxis = valueAxis;
    series1.name = 'Pendapatan';

    series1.tooltipText = "{name}: [bold]{valueY}[/]";

    // Create value axis2
    var valueAxis2 = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis2.renderer.opposite = true;
    valueAxis2.title.text = "Jumlah Transaksi";

    // Create series2
    var series2 = chart.series.push(new am4charts.LineSeries());
    series2.dataFields.valueY = "value2";
    series2.dataFields.dateX = "date";
    series2.strokeWidth = 3;
    series2.tensionX = 0.8;
    series2.bullets.push(new am4charts.CircleBullet());
    series2.connect = false;
    series2.yAxis = valueAxis2;
    series2.name = 'Jumlah Transaksi';

    series2.tooltipText = "{name}: [bold]{valueY}[/]";

    chart.cursor = new am4charts.XYCursor();

    chart.legend = new am4charts.Legend();

});
function copyLink() {
    var copyText = document.getElementById("myInput");
    copyText.select();
    document.execCommand("copy");

    swal('Copy link berhasil', '', 'success');
}