$(function() {
    let price_default = $('#default_price').val();
    let grosir = $('#grosir-slc').val();
    let reseller = $('#reseller-slc').val();
    let promo_type = $('#promo_type').val();
    let status_variation = $('#status_variation').val();

    if(price_default == "1") {
        $('#promo_type_div').hide();
        $('#price-input').attr('disabled', true);
        $('.form-group-grosir').hide();
        $('.price-input-variation').attr('disabled', true);
    } else {
        $('#promo_type_div').show();
        $('.form-group-grosir').show();
    }

    $('#default_price').on('change', function() {
        if(this.value == "1") {
            $('#promo_type_div').hide();
            $('#price-input').attr('disabled', true);
            $('.form-group-grosir').hide();
            $('#promo-tab').hide();
            $('#promo_type').val(0);
            promo_type = 0;
            $('#promo_type option[value=0]').attr('selected', true);
            $.ajax({
                url: site_url + 'catalog/price/change_default_price',
                method: 'POST',
                data: {
                    status_variation: status_variation,
                    product_id: $('#product_id').val(),
                    branch_id: $('#branch_id').val()
                },
                success: function(data) {
                    data = JSON.parse(data);
                    if(data.variation == 1) {
                        $.each(data.data, function(i, val) {
                            $('#price-option-' + val.id).val(val.price);
                        });
                    } else {
                        $('#price-input').val(data.data);
                    }
                }
            })
            if(status_variation == "1") {
                $('.price-input-variation').attr('disabled', true);
                $('.price-option-utama').hide();
            }
        } else {
            $('#promo_type_div').show();
            $('#price-input').removeAttr('disabled');
            $('.form-group-grosir').show();
            if(status_variation == '1') {
                if($('#grosir-slc').val() == '1' || $('#reseller-slc').val() == "1") {
                    $('.price-option-utama').show();
                    $('.price-input-variation').attr('disabled', true);
                } else {
                    $('.price-option-utama').hide();
                    $('.price-input-variation').removeAttr('disabled');
                }
            }
        }
    });

    if(promo_type == "1") {
        $('#promo-tab').show();
    } else {
        $('#promo-tab').hide();
    }

    $('#promo_type').on('change', function() {
        if(this.value == "1") {
            $('#promo-tab').show();
        } else {
            $('#promo-tab').hide();
        }
    });

    if(grosir == "1") {
        $('#price-list').show();
        if(status_variation == "1") {
            $('.price-input-variation').prop({ readOnly: true });
        }
    } else {
        $('#price-list').hide();
        if(status_variation == "1") {
            $('.price-option-utama').hide();
            $('#price-input').attr('disabled', true);
        }
    }

    $('#grosir-slc').on('change', function() {
        if(status_variation == "1") {
            if(this.value == "1") {
                swal({
                    title: 'Peringatan',
                    text: 'Menambah harga grosir akan mengubah semua harga varian menjadi harga utama! Lanjut?',
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: 'Ok',
                    cancelButtonText: 'Batal',
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function (inputValue) {
                    if(inputValue === true){
    
                       $('#price-list').show();
    
                       $('.price-input-variation').prop({ readOnly: true });
                       $('.price-input-variation').val($('#price-input').val());
    
                       if($('.price-qty').length <= 0){
                            add_new_grosir_row();
                        }

                        $('.price-option-utama').show();
                        $('#price-input').removeAttr('disabled');
                        
                    }else{
                        grosir = 0;
                        $('#grosir-slc').val(0);
                    }
                    
                });
            } else {
                $('#price-list').hide();
                $('.price-option-utama').hide();
                $('#price-input').attr('disabled', true);
                $('.price-input-variation').prop({ readOnly: false });
                $('.price-input-variation').removeAttr('disabled');
            }
        } else {
            if(this.value == "1") {
                $('#price-list').show();
            } else {
                $('#price-list').hide();
            }
        }
    });

    if(reseller == "1") {
        $('#price-reseller-list').show();
        if(status_variation == "1") {
            $('.price-input-variation').prop({ readOnly: true });
        }
    } else {
        $('#price-reseller-list').hide();
        if(status_variation == "1") {
            $('.price-option-utama').hide();
            $('#price-input').attr('disabled', true);
        }
    }

    $('#reseller-slc').on('change', function() {
        if(status_variation == "1") {
            if(this.value == "1") {
                swal({
                    title: 'Peringatan',
                    text: 'Menambah harga reseller akan mengubah semua harga varian menjadi harga utama! Lanjut?',
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: 'Ok',
                    cancelButtonText: 'Batal',
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function (inputValue) {
                    if(inputValue === true){
    
                       $('#price-reseller-list').show();
    
                       $('.price-input-variation').prop({ readOnly: true });
                       $('.price-input-variation').val($('#price-input').val());
    
                       if($('.price-qty').length <= 0){
                            add_new_reseller_row();
                        }

                        $('.price-option-utama').show();
                        $('#price-input').removeAttr('disabled');
                        
                    }else{
                        grosir = 0;
                        $('#grosir-slc').val(0);
                    }
                    
                });
            } else {
                $('#price-reseller-list').hide();
                $('.price-option-utama').hide();
                $('#price-input').attr('disabled', true);
                $('.price-input-variation').prop({ readOnly: false });
                $('.price-input-variation').removeAttr('disabled');
            }
        } else {
            if(this.value == '1') {
                $('#price-reseller-list').show();
            } else {
                $('#price-reseller-list').hide();
            }
        }
    });

    if(status_variation == "1") {
        $('#price-input').on('keyup', function() {
            $('.price-input-variation').val($(this).val());
        })
    }

    var input_type_promo = $('#input_type_promo').children(":selected").attr("id");
    if(input_type_promo == 'percentage'){
        $("#discount_addon").hide();
        $("#discount_addon_percent").show();
        $('#discount').prop('type', 'number');
        $("#discount").prop('min',0);
        $("#discount").prop('max',100);
    }
    else{
         $('#discount_addon_percent').hide();
    }
    $('#input_type_promo').change(function(){
        if(this.options[this.selectedIndex].id == 'percentage'){
            $("#discount_addon").hide();
             $("#discount_addon_percent").show();
             $('#discount').val(0);
             $('#discount').prop('type', 'number');
             $("#discount").prop('min',0);
             $("#discount").prop('max',100);
        }
        else{
            $('#discount').val(0);
            $('#discount_addon').show();
            $("#discount_addon_percent").hide();
            $('#discount').prop('type', 'text');
            $("#discount").prop('max',false);
            $("#discount").prop('min',false);

        }
    });

    $( "#coming-soon-time-id" ).datepicker();

    $('#add-price').on('click', function () {
        add_new_grosir_row();
    });

    $('body').on('click', '.remove-price', function () {
        $(this).parents('tr').remove();
    });

    $('#add-price-reseller').on('click', function () {
        add_new_reseller_row();
    });
    $('body').on('click', '.remove-price-reseller', function () {
        $(this).parents('tr').remove();
    });
});

function add_new_grosir_row(){
    var count = $('#price-count').val();
    var id = Math.random().toString(36).slice(2);
    var html = '<tr>';
    html += '<td class="price-qty" width="10%"><input type="number" min="2" required name="price_level[' + id + '][qty]" class="form-control td-grosir-qty"></td>';
    html += '<td width="30%"><div class="input-group"><span class="input-group-addon">Rp</span><input type="text" required name="price_level[' + id + '][price]" class="form-control number-grosir"></div></td>';
    html += '<td><button type="button" class="btn btn-danger btn-xs remove-price">x</button></td></tr>';
    count = parseInt(count) + 1;
    $('#price-count').val(count);
    $('#price-list tbody').append(html);
    $('.number-grosir').number(true, decimal_digit, decimal_separator, thousand_separator);
    $('.number-grosir').attr('autocomplete', 'off');
}

function add_new_reseller_row(){
    var count = $('#price-count-reseller').val();
    var id = Math.random().toString(36).slice(2);
    var html = '<tr>';
    html += '<td class="price-qty-reseller" width="10%"><input type="number" required min="2" name="price_reseller_list[' + id + '][qty]" class="form-control td-reseller-qty"></td>';
    html += '<td width="30%"><div class="input-group"><span class="input-group-addon">Rp</span><input type="text" required name="price_reseller_list[' + id + '][price]" class="form-control number-reseller"></div></td>';
    html += '<td><button type="button" class="btn btn-danger btn-xs remove-price-reseller">x</button></td></tr>';
    count = parseInt(count) + 1;
    $('#price-count-reseller').val(count);
    $('#price-reseller-list tbody').append(html);
    $('.number-reseller').number(true, decimal_digit, decimal_separator, thousand_separator);
    $('.number-reseller').attr('autocomplete', 'off');
}

$(document).on('keyup change',".td-grosir-qty",function(e){
    let increment_check = parseInt($(this).closest('tr').prev().children('td').children('.td-grosir-qty').val());
    let this_qty = parseInt($(this).val()); 
    let attr_name_qty = $(this).attr('name')
    let count_label_grosir_qty = $('.label_grosir_qty').length;
    if(count_label_grosir_qty === 0){
         if(this_qty <= increment_check){
             $(this).after('<label id="" class="custom-error-label label_grosir_qty" for="'+ attr_name_qty +'">Kuantitas minimal harus lebih tinggi dari sebelumnya!</label>')
         }
     }
     if(this_qty > increment_check){
         $('.label_grosir_qty').remove();
     }
     e.preventDefault();
     return false;
 })  

 $(document).on('keyup change',".td-reseller-qty",function(e){
    let increment_check_ = parseInt($(this).closest('tr').prev().children('td').children('.td-reseller-qty').val());
    let this_qty_ = parseInt($(this).val()); 
    let attr_name_qty_ = $(this).attr('name')
    let count_label_reseller_qty = $('.label_reseller_qty').length;
    if(count_label_reseller_qty === 0){
         if(this_qty_ <= increment_check_){
             $(this).after('<label id="" class="custom-error-label label_reseller_qty" for="'+ attr_name_qty_ +'">Kuantitas minimal harus lebih tinggi dari sebelumnya!</label>')
         }
     }
     if(this_qty_ > increment_check_){
         $('.label_reseller_qty').remove();
     }
     e.preventDefault();
     return false;
 })   

 $(document).on('keyup',".number-grosir",function(e){
     let grosir_value = parseInt($(this).val())
     let price_value = parseInt($('#price-input').val())
     let attr_name = $(this).attr('name')
     let count_label_grosir = $('.label_grosir').length;
     let increment_check_price = parseInt($(this).closest('tr').prev().children('.grosir_price_col').children('.input-group').children('.number-grosir').val());
     if(count_label_grosir === 0){
         if(price_value <= grosir_value){
             $(this).closest('.input-group').addClass('has-error');
             $(this).parent().after('<label id="" class="custom-error-label label_grosir" for="'+ attr_name +'">Harga Grosir harus lebih rendah dari Harga Utama!</label>')
         }
         if(grosir_value >= increment_check_price){
              $(this).closest('.input-group').addClass('has-error');
             $(this).parent().after('<label id="" class="custom-error-label label_grosir" for="'+ attr_name +'">Harga Grosir harus lebih rendah dari sebelumnya!</label>')
         }

     }
     else{
         if(price_value > grosir_value){
             $('.label_grosir').remove();
              $(this).closest('.input-group').removeClass('has-error');
         }
         if(grosir_value < increment_check_price){
             $('.label_grosir').remove();
              $(this).closest('.input-group').removeClass('has-error');
         }
     }

     e.preventDefault();
     return false;
 })

 $(document).on('keyup',".number-reseller",function(e){
     let reseller_value = parseInt($(this).val())
     let price_value_ = parseInt($('#price-input').val())
     let attr_name_ = $(this).attr('name')
     let count_label_reseller = $('.label_reseller').length;
     let increment_check_price_ = parseInt($(this).closest('tr').prev().children('.reseller_price_col').children('.input-group').children('.number-reseller').val());
     if(count_label_reseller === 0){
         if(price_value_ <= reseller_value){
             $(this).closest('.input-group').addClass('has-error');
             $(this).parent().after('<label id="" class="custom-error-label label_reseller" for="'+ attr_name_ +'">Harga Reseller harus lebih rendah dari Harga Utama!</label>')
         }
         if(reseller_value >= increment_check_price_){
              $(this).closest('.input-group').addClass('has-error');
             $(this).parent().after('<label id="" class="custom-error-label label_reseller" for="'+ attr_name_ +'">Harga Reseller harus lebih rendah dari sebelumnya!</label>')
         }

     }
     else{
         if(price_value_ > reseller_value){
             $('.label_reseller').remove();
              $(this).closest('.input-group').removeClass('has-error');
         }
         if(reseller_value < increment_check_price_){
             $('.label_reseller').remove();
              $(this).closest('.input-group').removeClass('has-error');
         }
     }
     e.preventDefault();
     return false;
 })