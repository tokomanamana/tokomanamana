// $(function() {
//     var table = $('#table-list-branch').DataTable({
//         stateSave: false,
//         processing: true,
//         serverSide: true,
//         ajax: {
//             url: $('#table-list-branch').data('url'),
//             type: 'post',
//             data: function (d) {
//                 d.data = get_form_data($('#form-list-branch'));
//                 return d;
//             }
//         },
//         autoWidth: false,
//         columnDefs: [
//             {
//                 orderable: false,
//                 targets: 'no-sort'
//             },
//             {
//                 className: 'text-center',
//                 targets: 'text-center'
//             },
//             {
//                 className: 'text-right',
//                 targets: 'text-right'
//             }
//         ],
//         order: $('th.default-sort').length ? [[$('th.default-sort').index(), $('th.default-sort').attr('data-sort')]] : false,
//         dom: '<"datatable-scroll"t><"datatable-footer"lp>',
//         language: {
//             processing: lang.table.processing,
//             zeroRecords: lang.table.zero_records,
//             lengthMenu: '<span>' + lang.table.length_menu + ':</span> _MENU_',
//             paginate: {'first': lang.table.first, 'last': lang.table.last, 'next': '&rarr;', 'previous': '&larr;'}
//         },
//     });
// })

// function get_form_data($form){
//     var unindexed_array = $form.serializeArray();
//     var indexed_array = {};

//     $.map(unindexed_array, function(n, i){
//         indexed_array[n['name']] = n['value'];
//     });

//     return indexed_array;
// }
$(document).ready(function() {
    $('.list-branch .list-name').each(function() {
        $(this).attr('search_data', $(this).text().toLowerCase());
    });
    $('#search_form').on('keyup', function() {
        let data_search = $(this).val().toLowerCase();
        $('.list-branch').each(function() {
            if($(this).find('.list-name').filter('[search_data *= ' + data_search + ']').length > 0 || data_search.length < 1) {
                $(this).show();
            } else {
                $(this).hide();
            }
        })
    });
});