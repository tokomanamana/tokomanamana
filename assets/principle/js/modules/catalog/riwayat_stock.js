$(function () {
	var table = $('#table_x').DataTable({
        stateSave: false,
        processing: true,
        serverSide: true,
        ajax: {
            url: $('#table_x').data('url'),
            type: 'post',
            data: function (d) {
                d.filter = get_form_data($('#filter-form'));
                return d;
            }
        },
        autoWidth: false,
        columnDefs: [
            {
                orderable: false,
                targets: 'no-sort'
            },
            {
                className: 'text-center',
                targets: 'text-center'
            },
            {
                className: 'text-right',
                targets: 'text-right'
            }
        ],
        order: $('th.default-sort').length ? [[$('th.default-sort').index(), $('th.default-sort').attr('data-sort')]] : false,
        dom: '<"datatable-scroll"t><"datatable-footer"lp>',
        language: {
            processing: lang.table.processing,
            zeroRecords: lang.table.zero_records,
            lengthMenu: '<span>' + lang.table.length_menu + ':</span> _MENU_',
            paginate: {'first': lang.table.first, 'last': lang.table.last, 'next': '&rarr;', 'previous': '&larr;'}
        },
    });
    $('#filter').on('click', function () {
        table.ajax.reload();
    });
    $('#filter-by').on('change', function () {
        var a = $(this).val();
        var now = new Date();
        $('input[name=from]').datepicker('destroy');
        $('input[name=to]').datepicker('destroy');
        $('input[name=from]').attr('readonly', 'readonly');
        $('input[name=to]').attr('readonly', 'readonly');
        if (a == 'today') { //hari ini
            $('input[name=from]').val(formatDate(now));
            $('input[name=to]').val(formatDate(now));
        } else if (a == 'yesterday') { //kemarin
            var date = new Date();
            date.setDate(now.getDate() - 1);
            $('input[name=from]').val(formatDate(date));
            $('input[name=to]').val(formatDate(date));
        } else if (a == 'this month') { //bulan ini
            var date1 = new Date(now.getFullYear(), now.getMonth(), 1);
            var date2 = new Date(now.getFullYear(), now.getMonth() + 1, 0);
            $('input[name=from]').val(formatDate(date1));
            $('input[name=to]').val(formatDate(date2));
        } else if (a == 'last month') { //bulan lalu
            var date1 = new Date(now.getFullYear(), now.getMonth() - 1, 1);
            var date2 = new Date(now.getFullYear(), now.getMonth(), 0);
            $('input[name=from]').val(formatDate(date1));
            $('input[name=to]').val(formatDate(date2));
        } else if (a == 'this year') { //tahun ini
            var date1 = new Date(now.getFullYear(), 0, 1);
            var date2 = new Date(now.getFullYear(), 12, 0);
            $('input[name=from]').val(formatDate(date1));
            $('input[name=to]').val(formatDate(date2));
        } else if (a == 99) {
            $('input[name=from]').datepicker({format: 'yyyy-mm-dd'});
            $('input[name=from]').removeAttr('readonly');
            $('input[name=to]').datepicker({format: 'yyyy-mm-dd'});
            $('input[name=to]').removeAttr('readonly');
        }
    });
});

function relog(){
	var table = $('#table_x').DataTable();
	if($('#tahun').val() != '99'){
		table.ajax.reload();
	}
}

function formatDate(date) {
    var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}

function get_form_data($form){
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}