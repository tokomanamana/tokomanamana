
$('input[name=datefrom]').datepicker({format: 'yyyy-mm-dd'});
$(document).on("keydown.autocomplete",".autocompleteTxt",function(e){
    $(this).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: site_url +'catalog/products/get_package_item',
                data: {
                    query: request.term
                },
                success: function( data ) {
                    data = $.parseJSON(data);
                    response( data );
                }
            });
            },
        minLength: 1,
        matchContains: true,
        selectFirst: false,
        select: function( event, ui ) {
            currentId = $(this).attr('id').replace('warehouseItem', '');
            var dimension = (ui.item.width/100)*(ui.item.length/100)*(ui.item.height/100);
            $( '#warehouseItem'+currentId ).val( ui.item.label );
            $( '#warehouseItemId'+currentId ).val( ui.item.id );
            $( '#productDimension'+currentId).html(parseFloat(dimension).toFixed(2));
            $( '#warehouseDimension'+currentId).html(parseFloat(dimension).toFixed(2));
            calculateSum();
        },
        change: function( event, ui ) {
            currentId = $(this).attr('id').replace('warehouseItem', '');
            $( "#warehouseItemId"+currentId ).val( ui.item? ui.item.id : 0 );
        } 
    });
});
$('.bootstrap-select').change(function() {    
    var item=$(this);
    $('.info-div').hide();
    $('#warehouse-info').html('');
    $.ajax({
        url: site_url +'warehouses/get_detail_warehouse',
        data: { m_id: item.val() },
        type: "POST",
        success: function( data ) {
            $('.info-div').show();
            data = $.parseJSON(data);
            var html = `
            <h5>Info Gudang</h5>
            <div class="row">
            <div class="col-md-6">
                <label class="col-md-3 control-label">Harga /hari /m3</label>
                <div class="col-md-9">
                <input type="text" class="form-control" disabled="" value=
                `+data.warehouse_price+`>
                <input type="hidden" name="w_price" value=`+data.warehouse_price+`>
                </div>
            </div>
            <div class="col-md-6">
                <label class="col-md-3 control-label">Ukuran (m3)</label>
                <div class="col-md-9">
                <input type="text" class="form-control" disabled="" value='`+data.length+` x `+data.width+` x `+data.height+`'>
                <input type="hidden" name="w_size" value=`+(data.length*data.width*data.height)+`>
                </div>
            </div>
            </div>
            <div style="margin-bottom:60px;"></div>
            <hr>
            `;
            $('#warehouse-info').append(html);
        }
    });
});
$(document).on('click', '#add-warehouse-item',function () {
    var id = Math.random().toString(36).slice(2);
    var html = '<div class="row" id="rowID-' + id + '">';
    html += '<div class="col-md-6">';
    html += '<div class="form-group">';
    html += '<label class="col-md-3 control-label">Item Name</label>';
    html += '<div class="col-md-9">';
    html += '<input id="warehouseItem' + id + '" class="autocompleteTxt form-control" type="text" name="warehouse_items[' + id + '][product_name]">';
    html += '<input id="warehouseRowId' + id + '" class="form-control" type="hidden" name="warehouse_items[' + id + '][row_id]" value="'+id+'">';
    html += '<input id="warehouseItemId' + id + '" class="form-control" type="hidden" name="warehouse_items[' + id + '][product_id]">';
    html += '</div></div></div>';
    html += '<div class="col-md-2">';
    html += '<div class="form-group">';
    html += '<label class="col-md-3 control-label">Qty</label>';
    html += '<div class="col-md-9">';
    html += '<input class="form-control" id="warehouseQty' + id + '" onchange="changeQty(`'+id+'`)" min="1" value="1" type="number" name="warehouse_items[' + id + '][qty]">';
    html += '</div></div></div>';
    html += '<div class="col-md-2">'
    html += '<div class="form-group">';
    html += '<div class="col-md-12 control-label">Dimensi : <label id="warehouseDimension' + id + '" class="txt">0</label> m3</div>';
    html += '</div></div>';
    html += '<div class="col-md-2">';
    html += '<button type="button" class="btn btn-danger btn-xs remove-item" data-id="' + id + '">x</button>';
    html += '</div>';
    html += '</div>';
    html += '<label id="productDimension' + id + '" style="display:none;">0</label>';
    //html += '<div class="col-md-6"><label class="col-md-3 control-label">Dimensi : </label><label class="col-md-3 control-label">123</label></div>';
    $('#item-lists').append(html);
});
$('body').on('click', '.remove-item', function () {
    var id = $(this).attr("data-id");
    $('#rowID-'+id).remove();
    calculateSum();
});

function changeQty(id){
    var dimension =  $('#productDimension'+id).html();
    var qty =  $('#warehouseQty'+id).val();
    $('#warehouseDimension'+id).html(parseFloat(dimension*qty).toFixed(2));
    calculateSum();
}

function calculateSum() {
    var sum = 0;
    //iterate through each textboxes and add the values
    $(".txt").each(function() {
    

        //add only if the value is number
        if(!isNaN($(this).html()) && $(this).html().length!=0) {
            sum += parseFloat($(this).html());
        }

    });
    //.toFixed() method will roundoff the final sum to 2 decimal places
    $("#sum").html(sum.toFixed(2));
}