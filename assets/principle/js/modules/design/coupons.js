var catIds = [{"id":"0","title":"All Categories","value":"0","children":[]}];
var zonaIds = [{"id":"0","title":"All Zona","value":"0","children":[]}];
var voucherId = $('input:hidden[id=decodeid]').val();
var catIdActive = $('input:hidden[name=catIds]').val();
var zonaIdActive = $('input:hidden[name=zonaIds]').val();
catIdActive = catIdActive ? catIdActive.split(',') : [];
//zonaIdActive =  zonaIdActive ? zonaIdActive.split(',') : [];
$.ajax({
    url: site_url + 'marketing/coupons/getCategories',
    type: 'get',
    data: {},
    success: function (response) {
        response = JSON.parse(response);
        jQuery.each(response.data, function(index, item) {
            let catId = {id: item.id,title: item.name, value: item.id,children:item.children}
            catIds[0].children.push(catId);
            // do something with `item` (or `this` is also `item` if you like)
        });
        $('#treeSelectorCat').treeSelector(catIds, catIdActive, function (e, values) {
            //console.info('onChange', e, values);
            $('input:hidden[name=catIds]').val(values);
          },{
          
            // children checked/unchecked if true
            checkWithParent: true,
          
            // title with 'title1 - title 2' if true
            titleWithParent: false,
          
            // when item click, only view leaf title if true
            notViewClickParentTitle: false,
          
            // disable the plugin
            disabled: false,
          
            // placeholder if empty
            emptyOptonPlaceholder: 'no options'
            
          })
    }
});

// $.ajax({
//   url: site_url + 'marketing/coupons/getZona',
//   type: 'get',
//   data: {},
//   success: function (response) {
//       response = JSON.parse(response);
//       jQuery.each(response.data, function(index, item) {
//           let zonaId = {id: item.id,title: item.name, value: item.id,children:[]}
//           zonaIds[0].children.push(zonaId);
//           // do something with `item` (or `this` is also `item` if you like)
//       });
//       $('#treeSelectorZona').treeSelector(zonaIds, zonaIdActive, function (e, values) {
//           console.info('onChange', e, values);
//           $('input:hidden[name=zonaIds]').val(values);
//         },{
        
//           // children checked/unchecked if true
//           checkWithParent: true,
        
//           // title with 'title1 - title 2' if true
//           titleWithParent: false,
        
//           // when item click, only view leaf title if true
//           notViewClickParentTitle: false,
        
//           // disable the plugin
//           disabled: false,
        
//           // placeholder if empty
//           emptyOptonPlaceholder: 'no options'
          
//         })
//   }
// });

function chose_image() {
    $('#gambar_input').click();
}

function change_image(val) {
  if (val.files && val.files[0]) {
    
    var FR= new FileReader();
    
    FR.addEventListener("load", function(e) {
      $('#img').show();
      document.getElementById("img").src       = e.target.result;
      $('#base64_image').val(e.target.result);
    }); 
    
    FR.readAsDataURL( val.files[0] );
  }
}