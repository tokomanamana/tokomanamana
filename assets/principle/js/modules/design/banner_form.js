$(function () {
    
    $('.listbox').bootstrapDualListbox({
        nonSelectedListLabel: 'Tidak dipilih',
        selectedListLabel: 'Dipilih',
        infoText: 'Total {0}',
        infoTextEmpty: '',
        filterPlaceHolder: 'Cari...',
    });
});
