$(function () {
    $.fn.stepy.defaults.legend = false;
    $.fn.stepy.defaults.backLabel = '<i class="icon-arrow-left13 position-left"></i> Kembali';
    $.fn.stepy.defaults.nextLabel = 'Lanjutkan <i class="icon-arrow-right14 position-right"></i>';
    $(".stepy").stepy({
        validate: true,
        block: true,
//        next: function (index) {
//            if (!$(".stepy").validate(validate)) {
//                return false
//            }
//        }
    });
    $('.stepy-step').find('.button-next').addClass('btn btn-primary');
    $('.stepy-step').find('.button-back').addClass('btn btn-default');

    Dropzone.autoDiscover = false
    var myDropzone = new Dropzone('div#myId', {
        url: base_url + "products/other/image_upload",
        autoProcessQueue: false,
        uploadMultiple: true,
        parallelUploads: 8,
        maxFiles: 8,
//        previewsContainer: "#showhere",
//        clickable: "#showhere",
        dictDefaultMessage: '',
        maxFilesize: 2, // MB
        acceptedFiles: '.jpg,.png,jpeg',
        addRemoveLinks: true,
        init: function () {
            var myDropzone = this;
            document.querySelector('#submit').addEventListener("click", function (e) {
                if (myDropzone.files.length == 0) {
                    error_message('Gambar produk harus ada.');
                } else {
                    myDropzone.processQueue();
                }
            });
            this.on("addedfile", function (file) {
//                console.log(file);
                var unique_field_id = new Date().getTime();
                file.id = unique_field_id;
                file.previewElement.appendChild(Dropzone.createElement('<div class="radio"><label><input type="radio" value="' + file.name + '" ' + ((myDropzone.files.length == 1) ? 'checked' : '') + ' name="image_primary" id="image_primary' + file.id + '">Gambar Utama</label></div>'));
            });
            this.on("removedfile", function (file) {
                if (!$("input:radio[name='image_primary']").is(":checked")) {
                    $("input:radio[name='image_primary']").prop('checked', true);
                }
            });
            this.on("thumbnail", function (file, done) {
                if (file.width < 600 || file.height < 600) {
                    error_message('Dimensi gambar minimum 600px.');
                    this.removeFile(file);
                }
            });
            this.on("maxfilesexceeded", function (file) {
                this.removeFile(file);
            });
            this.on("error", function (file, done) {
                error_message(done);
                if (!file.accepted)
                    this.removeFile(file);
            });
            this.on('sendingmultiple', function (e) {
                $('body').block({
                    message: '<i class="icon-spinner spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            });
            this.on('sending', function (file, xhr, formData) {
                $('body').block({
                    message: '<i class="icon-spinner spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
                formData.append("primary", $("input[name='image_primary']:checked").val());
                formData.append("name", $("#name").val());
                formData.append("id_temp", $("#id_temp").val());
            });
        },
//        accept: function (file, done) {
//            var myDropzone = this;
//            file.rejectDimensions = function () {
//                done("Dimensi gambar minimum 600px.");
//            };
//            if (myDropzone.files.length == 9) {
//                done("Jumlah gambar maksimum 8.");
//            }
////            console.log(myDropzone.files.length);
//        }
    });
//    var mocks = $('#myId').data('dropzone');
//    if (mocks.length > 0) {
//        for (var i = 0; i < mocks.length; i++) {
//            var mock = mocks[i];
//            mock.accepted = true;
//            myDropzone.emit('addedfile', mock);
//            myDropzone.createThumbnailFromUrl(mock, site_url + mock.url);
//            myDropzone.emit('complete', mock);
//            myDropzone.files.push(mock);
//        }
//    }
    $("#form-product").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        // Different components require proper error label placement
        errorPlacement: function (error, element) {
            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                } else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            }


            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function (label) {
            label.remove();
            label.closest('.form-group').removeClass('has-error');
        },
        rules: rules_form,
//        messages: messages_form,
        submitHandler: function (form) {
//            myDropzone.processQueue();
//            if ($('#id').val().length > 0) {
//                $(form).ajaxSubmit({
//                    success: function (data) {
//                        responseForm(data);
//                    }
//                });
//                return false;
//            } else {
            myDropzone.on('successmultiple', function (e) {
                $(form).ajaxSubmit({
                    success: function (data) {
                        responseForm(data);
                    }
                });
                return false;
            });
//            }
        }
    });
    $('#category').on('change', function () {
        $.ajax({
            type: "POST",
            url: base_url + 'products/other/get_feature_form',
            data: {category: $(this).val(), product: $('#id').val()},
            success: function (data) {
                $('#feature-form').html(data);
            }
        });
    });
});