function change_airway(e, id) {
    $(e).parent().parent().parent().attr('method', 'post');
    $(e).parent().parent().parent().attr('action', site_url + 'orders/sent/' + id);
    $(e).parent().html('<div class="input-group"><input type="text" class="form-control" name="airway" required value="' + $(e).parent().text().trim() + '"><span class="input-group-btn"><button type="submit" class="btn btn-primary"><i class="icon-checkmark"></i></button></span></div>');
}
$(document).on("click", ".openOTPModal", function () {
    var mobileNumber = $(this).data('mobilenumber');
    $(".modal-body #inputNoHP").val( mobileNumber );
});

function requestOTPButton(){
    $.post(
        site_url + 'orders/request_token/',{
            'order_id': $('#otpOrderID').val(),
            'phone': $('#inputNoHP').val()
        },function(data) {
            data = JSON.parse(data);
            if(data.status == 'success'){
                swal({
                    title: "Good job!",
                    text: data.message,
                    confirmButtonColor: "#66BB6A",
                    type: "success",
                    html: true
                });
                $('#btn-token').attr('disabled', 'disabled');
                $('#btn-token').text('Kirim Ulang (120s)');
                countdown(120);
            } else {
                swal({
                    title: "Oops...",
                    text: data.message,
                    confirmButtonColor: "#EF5350",
                    type: "error",
                    html: true
                });
            }
        }
    )
 }

 function submitOTP(){
    var url_success = $('.openOTPModal').data('urlsuccess');
    $.post(
        site_url + 'orders/check_token/',{
            'order_id': $('#otpOrderID').val(),
            'code': $('#otpCode').val()
        },function(data) {
            data = JSON.parse(data);
            if(data.status == 'success'){
                window.location.href = url_success;
            } else {
                swal({
                    title: "Oops...",
                    text: data.message,
                    confirmButtonColor: "#EF5350",
                    type: "error",
                    html: true
                });
            }
        }
    )
 }
 function countdown(duration) {
    var countdown = setInterval(function () {
        if (--duration) {
            $('#btn-token').text('Kirim Ulang (' + duration + 's)');
        } else {
            clearInterval(countdown);
            $('#btn-token').removeAttr('disabled');
            $('#btn-token').text('Request OTP');
        }
    }, 1000);
}