$('#stockModal').on('hidden.bs.modal', function () {
    $("#stock_table").dataTable().fnDestroy();
    // do something…
})
function seeStock(id) {
    var t = $('#stock_table').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "type" : "POST",
            "data": {id:id},
            "url" : site_url + 'merchants/get_stock_list',
            "dataSrc": function ( json ) {
                //Make your callback here.
                $('#stockModal').modal('show');
                return json.data;
            }       
        },
    });
}