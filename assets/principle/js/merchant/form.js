$(function () {
    $('.number').number(true, decimal_digit, decimal_separator, thousand_separator);
    $('.number').attr('autocomplete', 'off');
    tinymce.init({
        selector: ".tinymce",
        plugins: "align textcolor colorpicker link image table media placeholder advlist code table autoresize lists",
        browser_spellcheck: true,
        toolbar: [
            'fontselect,fontsizeselect,bold,italic,underline,forecolor,backcolor,blockquote,align,formatselect',
            'link,bullist,numlist,table,image,media,code'
        ],
        menubar: false,
        statusbar: false,
        relative_urls: false,
        convert_urls: false,
        branding: false,
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        }
    });
    $(".styled").uniform({
        radioClass: 'choice'
    });
    $('.select').select2({
        placeholder: function () {
            $(this).data('placeholder');
        }
    });
    $(".switch").bootstrapSwitch();
    $('.tags-input').tagsinput();
    $('.date').datepicker({
        format: 'yyyy-mm-dd',
        zIndex: 10001
    });
    $("#form").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        // Different components require proper error label placement
        errorPlacement: function (error, element) {
            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                } else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            }


            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function (label) {
            label.remove();
            label.closest('.form-group').removeClass('has-error');
        },
//        messages: messages_form,
        submitHandler: function (form) {
            $(form).ajaxSubmit({
                success: function (data) {
                    responseForm(data);
                }
            });
            return false;
        }
    });
});