$(document).ready(function () {
    // $('input[type=number]').keyup(function () {
    //     var qty = parseInt($(this).val());
    //     if (isNaN(qty) || qty < 1) {
    //         $(this).val(0);
    //         $('#btn_down_qty').attr('disabled', true);
    //     } else {
    //         $(this).val(qty);
    //     }

    //     if(qty <= 1) {
    //         $('#btn_down_qty').attr('disabled', true);
    //     } else {
    //         $('#btn_down_qty').removeAttr('disabled');
    //     }
    // })
});

function delete_cart(el, id, merchant, option = false) {
    id = id;
    id_merchant = merchant;
    Swal.fire({
        title: 'Apakah ingin menghapus pesanan ?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#d9534f',
        confirmButtonText: 'Ya, hapus!',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: site_url + 'catalog/products/delete_cart/',
                method: 'post',
                data: {
                    id: id,
                    id_merchant: id_merchant,
                    option: option
                },
                beforeSend: function () {
                    $("#tags-load").css('display', 'block');
                },
                complete: function () {
                    $("#tags-load").css('display', 'none');
                },
                success: function (data) {
                    data = JSON.parse(data);
                    // console.log(data.views);

                    var length = Object.keys(data.views).length;

                    var html = '';
                    html += '<div id="cart-page">';
                    html += '<div class="container">';
                    if (length > 0) {
                        html += '<div class="cart-product-mobile-container">';
                        $.each(data.views, function (index, val) {
                            if (val.option) {
                                let options = JSON.parse(val.option);
                                $.each(options, function (index, value) {
                                    val.min_buy = parseInt(val.min_buy);
                                    value.quantity = parseInt(value.quantity);
                                    value.product_quantity = parseInt(value.product_quantity);
                                    if (value.product_quantity < value.quantity || value.quantity < val.min_buy) {
                                        html += '<div class="cart-item" id="cart-mobile-option-' + value.id_cart_option + '" style="border-color:#d9534f;border-bottom:1px solid #d9534f;">';
                                    } else {
                                        html += '<div class="cart-item" id="cart-mobile-option-' + value.id_cart_option + '">';
                                    }
                                    html += '<div class="row" style="width: 100%;" id="cart-content-mobile-' + value.id_cart_option + '">';
                                    html += '<div class="cart-product-mobile">';
                                    html += '<div class="item-image-mobile">';
                                    html += '<a href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '">';
                                    html += '<img src="' + site_url + 'files/images/' + value.image + '">';
                                    html += '</a>';
                                    html += '</div>';
                                    html += '<div class="item-desc-mobile" style="position:relative;">';
                                    html += '<div class="item-name">';
                                    html += '<a href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '">';
                                    html += '<strong>' + val.name + '</strong>';
                                    html += '</a>';
                                    html += '</div>';
                                    html += '<div class="cart-line">';
                                    let data_option = JSON.parse(value.data_option);
                                    $.each(data_option, function (index, opt) {
                                        html += '<div class="option" style="height: 20px;">' + opt.value + '</div>';
                                        html += '<div class="divider"></div>';
                                    })
                                    html += '<div class="info weight">' + value.total_weight + ' gram</div>';
                                    html += '</div>';
                                    html += '<br>';
                                    html += '<span class="price" style="font-size: 16px; color: #a7c22a;">' + formatRupiah(value.price, ".") + '</span>';
                                    html += '<div class="cart-line">';
                                    html += '<span class="merchant">Penjual: <a href="' + site_url + 'merchant_home/view/' + val.merchant_id + '"><b>' + val.merchant_name + '</b></a></span>';
                                    html += '</div>';
                                    html += '<div class="alertEmptyStock" id="alert-mobile-option-' + value.id_cart_option + '" style="color:#d9534f;position:absolute;top:115%;">';
                                    if (value.product_quantity < value.quantity) {
                                        html += 'Maaf, Stok Produk tidak mencukupi!';
                                    } else if (value.quantity < val.min_buy) {
                                        html += 'Minimal pembelian ' + val.min_buy + ' barang';
                                    }
                                    html += '</div>';
                                    html += '</div>';
                                    html += '</div>';
                                    html += '<div class="cart-quantity-mobile">';
                                    html += '<div class="quantity-field" style="margin-right: 15px;margin-top:-11px; ">';
                                    if (value.quantity <= 1 || value.product_quantity == 1) {
                                        html += '<button id="btn_down_qty-mobile-option-' + value.id_cart_option + '" onclick="down_qty(\' ' + value.id_cart_option + '\', true);update_cart($(\'#qty-mobile-option-' + value.id_cart_option + '\').val(), $(\'#qty-mobile-option-' + value.id_cart_option + '\').data(\'store\'), $(\'#qty-mobile-option-' + value.id_cart_option + '\').data(\'product\'), $(\'#qty-mobile-option-' + value.id_cart_option + '\').data(\'idcart\'), \'' + value.id_encode + '\')" disabled>';
                                    } else {
                                        html += '<button id="btn_down_qty-mobile-option-' + value.id_cart_option + '" onclick="down_qty(\' ' + value.id_cart_option + '\', true);update_cart($(\'#qty-mobile-option-' + value.id_cart_option + '\').val(), $(\'#qty-mobile-option-' + value.id_cart_option + '\').data(\'store\'), $(\'#qty-mobile-option-' + value.id_cart_option + '\').data(\'product\'), $(\'#qty-mobile-option-' + value.id_cart_option + '\').data(\'idcart\'), \'' + value.id_encode + '\')">';
                                    }
                                    html += '<i class="fa fa-minus"></i>';
                                    html += '</button>';
                                    html += '<input type="number" id="qty-mobile-option-' + value.id_cart_option + '" name="qty_mobile" value="' + value.quantity + '" min="1" maxlength="3" class="item-quantity" data-store="' + val.input_merchant_id + '" data-product="' + val.input_product_id + '" data-idcart="' + val.id + '" onkeyup="update_cart($(this).val(), $(this).data(\'store\'), $(this).data(\'product\'), $(this).data(\'idcart\'), \'' + value.id_encode + '\');qty_form(\'' + value.id_cart_option + '\', true)" data-qty="' + value.product_quantity + '">';
                                    if (value.quantity >= value.product_quantity) {
                                        html += ' <button id="btn_up_qty-mobile-option-' + value.id_cart_option + '" onclick="up_qty(\' ' + value.id_cart_option + '\', true);update_cart($(\'#qty-mobile-option-' + value.id_cart_option + '\').val(), $(\'#qty-mobile-option-' + value.id_cart_option + '\').data(\'store\'), $(\'#qty-mobile-option-' + value.id_cart_option + '\').data(\'product\'), $(\'#qty-mobile-option-' + value.id_cart_option + '\').data(\'idcart\'), \'' + value.id_encode + '\')" class="plus" disabled>';
                                    } else {
                                        html += ' <button id="btn_up_qty-mobile-option-' + value.id_cart_option + '" onclick="up_qty(\' ' + value.id_cart_option + '\', true);update_cart($(\'#qty-mobile-option-' + value.id_cart_option + '\').val(), $(\'#qty-mobile-option-' + value.id_cart_option + '\').data(\'store\'), $(\'#qty-mobile-option-' + value.id_cart_option + '\').data(\'product\'), $(\'#qty-mobile-option-' + value.id_cart_option + '\').data(\'idcart\'), \'' + value.id_encode + '\')" class="plus">';
                                    }
                                    html += '<i class="fa fa-plus"></i>';
                                    html += '</button>';
                                    html += '</div>';
                                    html += '<a class="btn-delete-cart" href="javascript:void(0)" onclick="delete_cart(this, $(this).data(\'id\'), $(this).data(\'merchant\'), \'' + value.id_encode + '\')" data-id="' + val.id + '" data-merchant="' + val.input_merchant_id + '"><i class="fa fa-trash" style="font-size:24px;"></i></a>';
                                    html += '</div>';
                                    html += '</div>';
                                    html += '</div>';
                                })
                            } else {
                                val.quantity = parseInt(val.quantity);
                                val.min_buy = parseInt(val.min_buy);
                                val.product_quantity = parseInt(val.product_quantity);
                                if (val.stock_empty == 1 || val.quantity < val.min_buy) {
                                    html += '<div class="cart-item" id="cart-mobile-' + val.id + '" style="border-color:#d9534f;border-bottom:1px solid #d9534f;">';
                                } else {
                                    html += '<div class="cart-item" id="cart-mobile-' + val.id + '">';
                                }
                                html += '<div class="row" style="width: 100%;" id="cart-content-mobile-' + val.id + '">';
                                html += '<div class="cart-product-mobile">';
                                html += '<div class="item-image-mobile">';
                                html += '<a href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '">';
                                html += '<img src="' + site_url + 'files/images/' + val.image + '">';
                                html += '</a>';
                                html += '</div>';
                                html += '<div class="item-desc-mobile" style="position:relative;">';
                                html += '<div class="item-name">';
                                html += '<a href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '">';
                                html += '<strong>' + val.name + '</strong>';
                                html += '</a>';
                                html += '</div>';
                                html += '<div class="cart-line">';
                                html += '<div class="info weight">' + val.weight + ' gram</div>';
                                html += '</div>';
                                html += '<br>';
                                html += '<span class="price" style="font-size: 16px; color: #a7c22a;">' + formatRupiah(val.price, ".") + '</span>';
                                html += '<div class="cart-line">';
                                html += '<span class="merchant">Penjual: <a href="' + site_url + 'merchant_home/view/' + val.merchant_id + '"><b>' + val.merchant_name + '</b></a></span>';
                                html += '</div>';
                                html += '<div class="alertEmptyStock" id="alert-mobile-' + val.id + '" style="color:#d9534f;position:absolute;top:115%;">';
                                if (val.stock_empty == 1) {
                                    html += 'Maaf, Stok Produk tidak mencukupi!';
                                } else if (val.quantity < val.min_buy) {
                                    html += 'Minimal pembelian ' + val.min_buy + ' barang';
                                }
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += '<div class="cart-quantity-mobile">';
                                html += '<div class="quantity-field" style="margin-right: 15px;margin-top:-11px; ">';
                                if (val.quantity <= 1 || val.product_quantity == 1) {
                                    html += '<button id="btn_down_qty-mobile-' + val.id + '" onclick="down_qty(\' ' + val.id + '\');update_cart($(\'#qty-mobile-' + val.id + '\').val(), $(\'#qty-mobile-' + val.id + '\').data(\'store\'), $(\'#qty-mobile-' + val.id + '\').data(\'product\'), $(\'#qty-mobile-' + val.id + '\').data(\'idcart\'))" disabled>';
                                } else {
                                    html += '<button id="btn_down_qty-mobile-' + val.id + '" onclick="down_qty(\' ' + val.id + '\');update_cart($(\'#qty-mobile-' + val.id + '\').val(), $(\'#qty-mobile-' + val.id + '\').data(\'store\'), $(\'#qty-mobile-' + val.id + '\').data(\'product\'), $(\'#qty-mobile-' + val.id + '\').data(\'idcart\'))">';
                                }
                                html += '<i class="fa fa-minus"></i>';
                                html += '</button>';
                                html += '<input type="number" id="qty-mobile-' + val.id + '" name="qty_mobile" value="' + val.quantity + '" min="1" maxlength="3" class="item-quantity" data-store="' + val.input_merchant_id + '" data-product="' + val.input_product_id + '" data-idcart="' + val.id + '" onkeyup="update_cart($(this).val(), $(this).data(\'store\'), $(this).data(\'product\'), $(this).data(\'idcart\'));qty_form($(this).data(\'idcart\'))" data-qty="' + val.product_quantity + '">';
                                if (val.quantity >= val.product_quantity) {
                                    html += ' <button id="btn_up_qty-mobile-' + val.id + '" onclick="up_qty(\' ' + val.id + '\');update_cart($(\'#qty-mobile-' + val.id + '\').val(), $(\'#qty-mobile-' + val.id + '\').data(\'store\'), $(\'#qty-mobile-' + val.id + '\').data(\'product\'), $(\'#qty-mobile-' + val.id + '\').data(\'idcart\'))" class="plus" disabled>';
                                } else {
                                    html += ' <button id="btn_up_qty-mobile-' + val.id + '" onclick="up_qty(\' ' + val.id + '\');update_cart($(\'#qty-mobile-' + val.id + '\').val(), $(\'#qty-mobile-' + val.id + '\').data(\'store\'), $(\'#qty-mobile-' + val.id + '\').data(\'product\'), $(\'#qty-mobile-' + val.id + '\').data(\'idcart\'))" class="plus">';
                                }
                                html += '<i class="fa fa-plus"></i>';
                                html += '</button>';
                                html += '</div>';
                                html += '<a class="btn-delete-cart" href="javascript:void(0)" onclick="delete_cart(this, $(this).data(\'id\'), $(this).data(\'merchant\'))" data-id="' + val.id + '" data-merchant="' + val.input_merchant_id + '"><i class="fa fa-trash" style="font-size:24px;"></i></a>';
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                            }
                        });
                        html += '</div>';
                        html +=
                            `<div class="cart-footer-mobile">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="total">
                                        <span>Total Harga</span>
                                        <span class="grand-total">${formatRupiah(data.grand_total, ".")}</span>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="action">
                                        <!-- <a href="javascript:void(0)" onclick="checkStock()" class="btn">Beli Sekarang</a> -->
                                        <a href="javascript:void(0)" onclick="check_verifikasi()" class="btn btn-buy-mobile">Beli Sekarang</a>
                                    </div>
                                </div>
                            </div>
                        </div>`;

                        html += '<div class="cart-product">';
                        $.each(data.views, function (index, val) {
                            if (val.option) {
                                let options = JSON.parse(val.option);
                                val.min_buy = parseInt(val.min_buy);
                                $.each(options, function (index, value) {
                                    value.quantity = parseInt(value.quantity);
                                    value.product_quantity = parseInt(value.product_quantity);
                                    if (value.product_quantity < value.quantity || value.quantity < val.min_buy) {
                                        html += '<div class="cart-item" id="cart-option-' + value.id_cart_option + '" style="border-color:#d9534f;border-bottom:1px solid #d9534f;">';
                                    } else {
                                        html += '<div class="cart-item" id="cart-option-' + value.id_cart_option + '">';
                                    }
                                    html += '<div class="row" style="width: 100%;" id="cart-content-' + value.id_cart_option + '">';
                                    html += '<div class="col-sm-8 col-xs-8 cart-left">';
                                    html += '<div class="item-image">';
                                    html += '<a href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '">';
                                    html += '<img src="' + site_url + 'files/images/' + value.image + '">';
                                    html += '</a>';
                                    html += '</div>';
                                    html += '<div class="item-desc">';
                                    html += '<div class="item-name">';
                                    html += '<a href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '">';
                                    html += '<strong>' + val.name + '</strong>';
                                    html += '</a>';
                                    html += '</div>';
                                    html += '<div class="cart-line">';
                                    let data_option = JSON.parse(value.data_option);
                                    $.each(data_option, function (index, opt) {
                                        html += '<div class="option" style="height: 20px;">' + opt.value + '</div>';
                                        html += '<div class="divider"></div>';
                                    })
                                    html += '<div class="info weight">' + value.total_weight + ' gram</div>';
                                    html += '</div>';
                                    html += '<br>';
                                    html += '<span class="price" style="font-size: 16px; color: #a7c22a;">' + formatRupiah(value.price, ".") + '</span>';
                                    html += '<div class="cart-line">';
                                    html += '<span class="merchant">Penjual: <a href="' + site_url + 'merchant_home/view/' + val.merchant_id + '"><b>' + val.merchant_name + '</b></a></span>';
                                    html += '</div>';
                                    html += '<div class="alertEmptyStock" id="alert-option-' + value.id_cart_option + '" style="color:#d9534f;">';
                                    if (value.product_quantity < value.quantity) {
                                        html += 'Maaf, Stok Produk tidak mencukupi!';
                                    } else if (value.quantity < val.min_buy) {
                                        html += 'Minimal pembelian ' + val.min_buy + ' barang';
                                    }
                                    html += '</div>';
                                    html += '</div>';
                                    html += '</div>';
                                    html += '<div class="center col-sm-4 col-xs-4">';
                                    html += '<div class="quantity-field" style="margin-right: 15px;margin-top:-11px; ">';
                                    if (value.quantity <= 1 || value.product_quantity == 1) {
                                        html += '<button id="btn_down_qty-option-' + value.id_cart_option + '" onclick="down_qty(\' ' + value.id_cart_option + '\', true);update_cart($(\'#qty-option-' + value.id_cart_option + '\').val(), $(\'#qty-option-' + value.id_cart_option + '\').data(\'store\'), $(\'#qty-option-' + value.id_cart_option + '\').data(\'product\'), $(\'#qty-option-' + value.id_cart_option + '\').data(\'idcart\'), \'' + value.id_encode + '\')" disabled>';
                                    } else if (value.quantity <= val.min_buy) {
                                        html += '<button id="btn_down_qty-option-' + value.id_cart_option + '" onclick="down_qty(\' ' + value.id_cart_option + '\', true);update_cart($(\'#qty-option-' + value.id_cart_option + '\').val(), $(\'#qty-option-' + value.id_cart_option + '\').data(\'store\'), $(\'#qty-option-' + value.id_cart_option + '\').data(\'product\'), $(\'#qty-option-' + value.id_cart_option + '\').data(\'idcart\'), \'' + value.id_encode + '\')" disabled>';
                                    } else {
                                        html += '<button id="btn_down_qty-option-' + value.id_cart_option + '" onclick="down_qty(\' ' + value.id_cart_option + '\', true);update_cart($(\'#qty-option-' + value.id_cart_option + '\').val(), $(\'#qty-option-' + value.id_cart_option + '\').data(\'store\'), $(\'#qty-option-' + value.id_cart_option + '\').data(\'product\'), $(\'#qty-option-' + value.id_cart_option + '\').data(\'idcart\'), \'' + value.id_encode + '\')">';
                                    }
                                    html += '<i class="fa fa-minus"></i>';
                                    html += '</button>';
                                    html += '<input type="number" id="qty-option-' + value.id_cart_option + '" name="qty" value="' + value.quantity + '" min="1" maxlength="3" class="item-quantity" data-store="' + val.input_merchant_id + '" data-product="' + val.input_product_id + '" data-idcart="' + val.id + '" data-min_buy="' + val.min_buy + '" onkeyup="update_cart($(this).val(), $(this).data(\'store\'), $(this).data(\'product\'), $(this).data(\'idcart\'), \'' + value.id_encode + '\');qty_form(\'' + value.id_cart_option + '\', true)" data-qty="' + value.product_quantity + '">';
                                    if (value.quantity >= value.product_quantity || value.product_quantity == 1) {
                                        html += ' <button id="btn_up_qty-option-' + value.id_cart_option + '" onclick="up_qty(\'' + value.id_cart_option + '\', true);update_cart($(\'#qty-option-' + value.id_cart_option + '\').val(), $(\'#qty-option-' + value.id_cart_option + '\').data(\'store\'), $(\'#qty-option-' + value.id_cart_option + '\').data(\'product\'), $(\'#qty-option-' + value.id_cart_option + '\').data(\'idcart\'), \'' + value.id_encode + '\')" class="plus" disabled>';
                                    } else {
                                        html += ' <button id="btn_up_qty-option-' + value.id_cart_option + '" onclick="up_qty(\' ' + value.id_cart_option + '\', true);update_cart($(\'#qty-option-' + value.id_cart_option + '\').val(), $(\'#qty-option-' + value.id_cart_option + '\').data(\'store\'), $(\'#qty-option-' + value.id_cart_option + '\').data(\'product\'), $(\'#qty-option-' + value.id_cart_option + '\').data(\'idcart\'), \'' + value.id_encode + '\')" class="plus">';
                                    }
                                    html += '<i class="fa fa-plus"></i>';
                                    html += '</button>';
                                    html += '</div>';
                                    html += '<a class="btn-delete-cart" href="javascript:void(0)" onclick="delete_cart(this, $(this).data(\'id\'), $(this).data(\'merchant\'), \' ' + value.id_encode + '\')" data-id="' + val.id + '" data-merchant="' + val.input_merchant_id + '"><i class="fa fa-trash" style="font-size:24px;"></i></a>';
                                    html += '</div>';
                                    html += '</div>';
                                    html += '</div>';
                                })
                            } else {
                                val.min_buy = parseInt(val.min_buy);
                                val.quantity = parseInt(val.quantity);
                                val.product_quantity = parseInt(val.product_quantity);
                                if (val.stock_empty == 1 || val.quantity < val.min_buy) {
                                    html += '<div class="cart-item" id="cart-' + val.id + '" style="border-color:#d9534f;border-bottom:1px solid #d9534f;">';
                                } else {
                                    html += '<div class="cart-item" id="cart-' + val.id + '">';
                                }
                                html += '<div class="row" style="width: 100%;" id="cart-content-' + val.id + '">';
                                html += '<div class="col-sm-8 col-xs-8 cart-left">';
                                html += '<div class="item-image">';
                                html += '<a href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '">';
                                html += '<img src="' + site_url + 'files/images/' + val.image + '">';
                                html += '</a>';
                                html += '</div>';
                                html += '<div class="item-desc">';
                                html += '<div class="item-name">';
                                html += '<a href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '">';
                                html += '<strong>' + val.name + '</strong>';
                                html += '</a>';
                                html += '</div>';
                                html += '<div class="cart-line">';
                                html += '<div class="info">' + val.code + '</div>';
                                html += '<div class="info weight">' + val.weight + ' gram</div>';
                                html += '</div>';
                                html += '<br>';
                                html += '<span class="price" style="font-size: 16px; color: #a7c22a;">' + formatRupiah(val.price, ".") + '</span>';
                                html += '<div class="cart-line">';
                                html += '<span class="merchant">Penjual: <a href="' + site_url + 'merchant_home/view/' + val.merchant_id + '"><b>' + val.merchant_name + '</b></a></span>';
                                html += '</div>';
                                html += '<div class="alertEmptyStock" id="alert-' + val.id + '" style="color:#d9534f;">';
                                if (val.stock_empty == 1) {
                                    html += 'Maaf, Stok Produk tidak mencukupi!';
                                } else if (val.quantity < val.min_buy) {
                                    html += 'Minimal pembelian ' + val.min_buy + ' barang';
                                }
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += '<div class="center col-sm-4 col-xs-4">';
                                html += '<div class="quantity-field" style="margin-right: 15px;margin-top:-11px; ">';
                                if (val.quantity <= 1 || val.product_quantity == 1) {
                                    html += '<button id="btn_down_qty-' + val.id + '" onclick="down_qty(\' ' + val.id + '\');update_cart($(\'#qty-' + val.id + '\').val(), $(\'#qty-' + val.id + '\').data(\'store\'), $(\'#qty-' + val.id + '\').data(\'product\'), $(\'#qty-' + val.id + '\').data(\'idcart\'))" disabled>';
                                } else if (val.quantity <= val.min_buy) {
                                    html += '<button id="btn_down_qty-' + val.id + '" onclick="down_qty(\' ' + val.id + '\');update_cart($(\'#qty-' + val.id + '\').val(), $(\'#qty-' + val.id + '\').data(\'store\'), $(\'#qty-' + val.id + '\').data(\'product\'), $(\'#qty-' + val.id + '\').data(\'idcart\'))" disabled>';
                                } else {
                                    html += '<button id="btn_down_qty-' + val.id + '" onclick="down_qty(\' ' + val.id + '\');update_cart($(\'#qty-' + val.id + '\').val(), $(\'#qty-' + val.id + '\').data(\'store\'), $(\'#qty-' + val.id + '\').data(\'product\'), $(\'#qty-' + val.id + '\').data(\'idcart\'))">';
                                }
                                html += '<i class="fa fa-minus"></i>';
                                html += '</button>';
                                html += '<input type="number" id="qty-' + val.id + '" name="qty" value="' + val.quantity + '" min="1" maxlength="3" class="item-quantity" data-store="' + val.input_merchant_id + '" data-product="' + val.input_product_id + '" data-idcart="' + val.id + '" data-min_buy="' + val.min_buy + '" onkeyup="update_cart($(this).val(), $(this).data(\'store\'), $(this).data(\'product\'), $(this).data(\'idcart\'));qty_form($(this).data(\'idcart\'))" data-qty="' + val.product_quantity + '">';
                                if (val.quantity >= val.product_quantity) {
                                    html += ' <button id="btn_up_qty-' + val.id + '" onclick="up_qty(\'' + val.id + '\');update_cart($(\'#qty-' + val.id + '\').val(), $(\'#qty-' + val.id + '\').data(\'store\'), $(\'#qty-' + val.id + '\').data(\'product\'), $(\'#qty-' + val.id + '\').data(\'idcart\'))" class="plus" disabled>';
                                } else {
                                    html += ' <button id="btn_up_qty-' + val.id + '" onclick="up_qty(\' ' + val.id + '\');update_cart($(\'#qty-' + val.id + '\').val(), $(\'#qty-' + val.id + '\').data(\'store\'), $(\'#qty-' + val.id + '\').data(\'product\'), $(\'#qty-' + val.id + '\').data(\'idcart\'))" class="plus">';
                                }
                                html += '<i class="fa fa-plus"></i>';
                                html += '</button>';
                                html += '</div>';
                                html += '<a class="btn-delete-cart" href="javascript:void(0)" onclick="delete_cart(this, $(this).data(\'id\'), $(this).data(\'merchant\'))" data-id="' + val.id + '" data-merchant="' + val.input_merchant_id + '"><i class="fa fa-trash" style="font-size:24px;"></i></a>';
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                            }
                        });
                        html += '</div>';
                        html += '<div class="cart-footer">';
                        html += '<div class="total">';
                        html += '<span>Total Harga</span>';
                        html += '<span class="grand-total">' + formatRupiah(data.grand_total, ".") + '</span>';
                        html += '</div>';
                        html += '<div class="action">';
                        html += '<a href="' + site_url + '" class="link-back"><i class="fa fa-arrow-left"></i> Lanjut Belanja</a>';
                        html += '<a>';
                        html += '<a href="javascript:void(0)" style="border-radius: 5px;" class="btn" onclick="check_verifikasi()">Beli Sekarang <i class="fa fa-arrow-right"></i></a>';
                        html += '</div>';
                    } else {
                        html += '<div class="text-center">';
                        html += '<img src="' + site_url + 'files/images/keranjang-kosong.jpeg" alt="Keranjang Kosong" style="width: 40%; margin-top: -20px;">';
                        html += '<div style="font-size: 20px;font-weight: bold;margin-bottom: 5px;">Keranjang belanja anda kosong.</div>';
                        html += '<div style="font-size: 18px;margin-bottom: 20px;">Yuk belanja sekarang!</div>';
                        html += '<a href="' + site_url + '" class="btn btn-block tombol-belanja" style="border-radius: 5px;">Mulai Belanja</a>';
                        html += '</div>';
                    }
                    html += '</div>';
                    html += '</div>';

                    // var html_header = '<div class="items control-container" style="max-height: 150px; overflow: auto">';
                    // if(length > 0) {
                    //     $.each(data.views, function(index, val) {
                    //         html_header += '<div class="row" style="padding: 10px 10px;"><div class="cart-left">';
                    //         html_header += '<a class="cart-image" href="catalog/products/view/'+val.product_id+'/'+val.merchant_id+'">';
                    //         html_header += '<img src="'+site_url+'files/images/'+val.image+'" alt="" title=""></a></div>';
                    //         html_header += '<div class="cart-right">';
                    //         html_header += '<div class=""><a href="catalog/products/view/' + val.product_id + '/'+ val.merchant_id +'" style="color:#000;">'+val.name+'</a></div>';
                    //         html_header += '<div class="cart-price"><span class="money">'+formatRupiah(val.price,'.')+'</span><span class="x"> x '+val.quantity+'</span></div>';
                    //         html_header += '</div></div>';
                    //     });
                    //     html_header += '</div> <div class="subtotal" style="border-top: 1px solid #EBEBEB"><span>Subtotal:</span><span class="cart-total-right money">'+formatRupiah(data.grand_total,'.')+'</span></div>';
                    //     html_header += '<div class="action"><a href="'+site_url+'cart" class="btn" style="font-size:10px; width: 100%; onclick="window.location = '+site_url+'cart;">Lihat Semua</a></div>';
                    // } else {
                    //     html_header += '<div class="items control-container"><center><p style="font-size: 13px; font-weight: bold; padding: 15px;">Keranjang belanja Anda kosong. Yuk, belanja sekarang!</p></center></div>';
                    // }
                    // html_header += '</div>';
                    var html_header = '';
                    if (length > 0) {
                        html_header += '<div style="max-height: 200px; overflow: auto">';
                        html_header += '<div class="carts-items">';
                        $.each(data.views, function (index, val) {
                            if (val.option) {
                                let options = JSON.parse(val.option);
                                $.each(options, function (index, value) {
                                    html_header += '<div class="row" id="cart-header-option-' + value.id_cart_option + '" style="padding: 10px 10px;border-bottom:1px solid #EEE;">';
                                    html_header += '<div class="cart-left col-md-4" style="display: inline-block;">';
                                    html_header += '<a class="cart-image" href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '">';
                                    html_header += '<img style="width: 50px;" src="' + site_url + 'files/images/' + value.image + '" alt="" title="">';
                                    html_header += '</a>';
                                    html_header += '</div>';
                                    html_header += '<div class="cart-right col-md-8" style="display: inline-block;margin-left:-25px;">';
                                    html_header += '<div class="">';
                                    html_header += '<a href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '" style="color:#000;">';
                                    html_header += val.name;
                                    html_header += '</a>';
                                    html_header += '</div>';
                                    html_header += '<div class="cart-price">';
                                    html_header += '<span class="money" style="color: #97C23C;">' + formatRupiah(value.price, '.') + '</span>';
                                    html_header += '<span class="x" style="color: #97C23C;">x ' + value.quantity + '</span>'
                                    html_header += '</div>';
                                    html_header += '</div>';
                                    html_header += '</div>';
                                })
                            } else {
                                html_header += '<div class="row" id="cart-header-' + val.id + '" style="padding: 10px 10px;border-bottom:1px solid #EEE;">';
                                html_header += '<div class="cart-left col-md-4" style="display: inline-block;">';
                                html_header += '<a class="cart-image" href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '">';
                                html_header += '<img style="width: 50px;" src="' + site_url + 'files/images/' + val.image + '" alt="" title="">';
                                html_header += '</a>';
                                html_header += '</div>';
                                html_header += '<div class="cart-right col-md-8" style="display: inline-block;margin-left:-25px;">';
                                html_header += '<div class="">';
                                html_header += '<a href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '" style="color:#000;">';
                                html_header += val.name;
                                html_header += '</a>';
                                html_header += '</div>';
                                html_header += '<div class="cart-price">';
                                html_header += '<span class="money" style="color: #97C23C;">' + formatRupiah(val.price, '.') + '</span>';
                                html_header += '<span class="x" style="color: #97C23C;">x ' + val.quantity + '</span>'
                                html_header += '</div>';
                                html_header += '</div>';
                                html_header += '</div>';
                            }
                        });
                        html_header += '</div>';
                        html_header += '</div>';
                        html_header += '<div class="subtotal" style="color: black;padding: 15px 20px;font-size:14px;border-top:1px solid #EEE;">';
                        html_header += '<span>Subtotal:</span><span class="cart-total-right money" style="float: right;">' + formatRupiah(data.grand_total, '.') + '</span>'
                        html_header += '</div>';
                        html_header += '<div class="action" style="padding: 15px 20px;margin-top:-10px;">';
                        html_header += '<a href="' + site_url + 'cart" class="btn btn-show-all" style="font-size:10px; width: 100%;border-radius:3px;">Lihat Semua</a>'
                        html_header += '</div>';
                    } else {
                        html_header += '<img src="' + site_url + 'files/images/keranjang-kosong.jpeg" alt="Keranjang Kosong" style="width: 70%;margin-top: 5px;display:block;margin-left: auto; margin-right: auto;">';
                        html_header += '<p style="font-size: 13px; font-weight: bold; padding: 15px;text-align:center;">Keranjang belanja Anda kosong. Yuk, belanja sekarang!</p>';
                    }

                    $("#cart-target .items-cart-right .badge").html(data.total_qty);
                    $('.icon-cart .cart_text .number').html(data.total_qty);
                    $('#cart-target-mobile .number').html(data.total_qty);
                    $('#cart-target .cart-info .cart-content').html(html_header);
                    $('#cart-target-mobile .cart-info .cart-content').html(html_header);
                    $('.cart-footer-mobile .total .grand-total').html(formatRupiah(data.grand_total, 'Rp. '));

                    // console.log(data.cartt);
                    // console.log(data.views);
                    // $(".cart-product").html(' ');
                    // $(".cart-product").append(data.cartt);
                    $("#cart-section").html('');
                    $("#cart-section").append(html);

                    // new
                    $('.cart_icon .cart-dropdown').html(html_header);
                    $('.cart_icon #icon_cart .badge').html(data.total_qty);
                    $('.mobile-navigation .nav-bottom #cart-button .badge').html(data.total_qty);

                    Swal.fire({
                        type: 'success',
                        title: data.message,
                        timer: 1000,
                        showConfirmButton: false,
                        customClass: 'swal-class'
                    });
                }
            });
        }
    })
}

function cartUpdateQty(qty, product) {
    $.post(base_url + 'update_cart/', {
        qty: qty,
        product: product
    }, function (data) {
        location.reload();
    });
    // $.post(site_url + 'update_cart/', { qty: qty, product: product }, function (data) {
    //     data = JSON.parse(data);
    //     if (data.status == 'failed') {
    //         $.each(data.data, function (i, item) {
    //             $('#cart-' + item.rowid).attr("style", "border-color:red;border-bottom:1px solid red;'");
    //             $('#alert-' + item.rowid).html('Maaf, Stock Produk tidak mencukupi!');
    //         });
    //     } else {
    //         location.reload();
    //     }
    // });
}

function update_cart(qty, store, product, id_cart, product_option = null) {
    // break;
    if (qty == 0 || qty == "") {
        qty = 1;
    }
    $.ajax({
        url: site_url + 'catalog/products/update_cart_ajax/',
        type: 'post',
        data: {
            qty: qty,
            store: store,
            product: product,
            id_cart: id_cart,
            product_option: product_option
        },
        // beforeSend: function () {
        //     $("#tags-load").css('display', 'block');
        // },
        // complete: function () {
        //     $("#tags-load").css('display', 'none');
        // },
        success: function (data) {
            data = JSON.parse(data);
            if (data.status == 'error') {
                Swal.fire({
                    type: 'error',
                    title: 'Terjadi Kesalahan',
                    text: data.message
                });
            } else {
                $.ajax({
                    url: site_url + 'checkout/check_stock',
                    data: {
                        product_option: product_option,
                        qty: qty
                    },
                    type: 'POST',
                    success: function (response) {
                        let data = JSON.parse(response);
                        $('.cart-item').attr("style", '');
                        $('.alertEmptyStock').html('');
                        if (data.status == 'failed') {
                            // if(data.option == 'true') {
                            //     $(data.data).each(function (index, value) {
                            //         $('#cart-option-' + value).attr("style", "border-color:#d9534f;border-bottom:1px solid #d9534f;");
                            //         $('#alert-option-' + value).html('Maaf, Stok Produk tidak mencukupi!');
                            //         $('#cart-mobile-option-' + value).attr("style", "border-color:#d9534f;border-bottom:1px solid #d9534f;");
                            //         $('#alert-mobile-option-' + value).html('Maaf, Stok Produk tidak mencukupi!');
                            //     });
                            // } else {
                            //     $(data.data).each(function (index, value) {
                            //         $('#cart-' + value).attr("style", "border-color:#d9534f;border-bottom:1px solid #d9534f;");
                            //         $('#alert-' + value).html('Maaf, Stok Produk tidak mencukupi!');
                            //         $('#cart-mobile-' + value).attr("style", "border-color:#d9534f;border-bottom:1px solid #d9534f;");
                            //         $('#alert-mobile-' + value).html('Maaf, Stok Produk tidak mencukupi!');
                            //     });
                            // }
                            $(data.data).each(function (index, value) {
                                if (value.status == 'failed') {
                                    if (value.option == 'true') {
                                        $('#cart-option-' + value.id).attr("style", "border-color:#d9534f;border-bottom:1px solid #d9534f;");
                                        $('#alert-option-' + value.id).html('Maaf, Stok Produk tidak mencukupi!');
                                        $('#cart-mobile-option-' + value.id).attr("style", "border-color:#d9534f;border-bottom:1px solid #d9534f;");
                                        $('#alert-mobile-option-' + value.id).html('Maaf, Stok Produk tidak mencukupi!');
                                    } else {
                                        $('#cart-' + value.id).attr("style", "border-color:#d9534f;border-bottom:1px solid #d9534f;");
                                        $('#alert-' + value.id).html('Maaf, Stok Produk tidak mencukupi!');
                                        $('#cart-mobile-' + value.id).attr("style", "border-color:#d9534f;border-bottom:1px solid #d9534f;");
                                        $('#alert-mobile-' + value.id).html('Maaf, Stok Produk tidak mencukupi!');
                                    }
                                } else if (value.status == 'failed_min_buy') {
                                    if (value.option == 'true') {
                                        $('#cart-option-' + value.id).attr("style", "border-color:#d9534f;border-bottom:1px solid #d9534f;");
                                        $('#alert-option-' + value.id).html('Minimal pembelian ' + value.min_buy + ' barang');
                                    } else {
                                        $('#cart-' + value.id).attr("style", "border-color:#d9534f;border-bottom:1px solid #d9534f;");
                                        $('#alert-' + value.id).html('Minimal pembelian ' + value.min_buy + ' barang');
                                    }
                                }
                            })
                        } else {
                            // document.location.href = site_url + 'checkout';
                        }
                    }
                });
                if (data.items.option_status == true) {
                    $("#cart-option-" + data.items.id + " .cart-line .weight").html(data.items.total_weight + ' gram');
                    $("#cart-mobile-option-" + data.items.id + " .cart-line .weight").html(data.items.total_weight + ' gram');
                    $("#cart-option-" + data.items.id + " .price").html(formatRupiah(data.items.price, '.'));
                    $("#cart-mobile-option-" + data.items.id + " .price").html(formatRupiah(data.items.price, '.'));
                    $("#cart-header-option-" + data.items.id + " .cart-price .x").html("x " + data.items.quantity);
                } else {
                    $("#cart-" + data.items.id + " .cart-line .weight").html(data.items.total_weight + ' gram');
                    $("#cart-mobile-" + data.items.id + " .cart-line .weight").html(data.items.total_weight + ' gram');
                    $("#cart-" + data.items.id + " .price").html(formatRupiah(data.items.price, '.'));
                    $("#cart-mobile-" + data.items.id + " .price").html(formatRupiah(data.items.price, '.'));
                    $("#cart-header-" + data.items.id + " .cart-price .x").html("x " + data.items.quantity);
                }

                $("#cart-header-" + data.items.id + " .cart-price .money").html(formatRupiah(data.items.price, '.'));
                $("#cart-target .items-cart-right .badge").html(data.total_qty);
                $(".cart-footer .total .grand-total, .cart-footer-mobile .total .grand_total").html(formatRupiah(data.grand_total, '.'));
                $(".cart-content .subtotal .money").html(formatRupiah(data.grand_total, '.'));
                $('.cart-footer-mobile .total .grand-total').html(formatRupiah(data.grand_total, 'Rp. '));

                // new
                $('.cart_icon #icon_cart .badge').html(data.total_qty);
                $('.cart_icon .cart-dropdown .subtotal .money').html(formatRupiah(data.grand_total, '.'));
                $('.mobile-navigation .nav-bottom #cart-button .badge').html(data.total_qty);
                // Swal.fire({
                //   type: 'success',
                //   title: data.message,
                //   timer: 1000,
                //   showConfirmButton: false,
                //   customClass: 'swal-class'
                // });
            }
        }
    });
}

function checkStock() {
    $.post(site_url + 'checkout/check_stock/', {}, function (data) {
        data = JSON.parse(data);
        $('.cart-item').attr("style", '');
        $('.alertEmptyStock').html('');
        if (data.status == 'failed') {
            $.each(data.data, function (i, item) {
                $('#cart-' + item.rowid).attr("style", "border-color:#d9534f;border-bottom:1px solid #d9534f;'");
                $('#alert-' + item.rowid).html('Maaf, Stock Produk tidak mencukupi!');
            });
        } else {
            window.location.href = site_url + 'checkout';
        }
    });
}

function check_verifikasi() {
    $.ajax({
        url: site_url + 'checkout/check_verifikasi',
        data: {},
        success: function (response) {
            if (response == 'success') {
                $.ajax({
                    url: site_url + 'checkout/check_address',
                    data: {},
                    success: function (response) {
                        if (response == 'success') {
                            $.ajax({
                                url: site_url + 'checkout/check_stock',
                                data: {},
                                success: function (response) {
                                    let data = JSON.parse(response);
                                    $('.cart-item').attr("style", '');
                                    $('.alertEmptyStock').html('');
                                    if (data.status == 'failed') {
                                        $(data.data).each(function (index, value) {
                                            if (value.status == 'failed') {
                                                if (value.option == 'true') {
                                                    $('#cart-option-' + value.id).attr("style", "border-color:#d9534f;border-bottom:1px solid #d9534f;");
                                                    $('#alert-option-' + value.id).html('Maaf, Stok Produk tidak mencukupi!');
                                                    $('#cart-mobile-option-' + value.id).attr("style", "border-color:#d9534f;border-bottom:1px solid #d9534f;");
                                                    $('#alert-mobile-option-' + value.id).html('Maaf, Stok Produk tidak mencukupi!');
                                                } else {
                                                    $('#cart-' + value.id).attr("style", "border-color:#d9534f;border-bottom:1px solid #d9534f;");
                                                    $('#alert-' + value.id).html('Maaf, Stok Produk tidak mencukupi!');
                                                    $('#cart-mobile-' + value.id).attr("style", "border-color:#d9534f;border-bottom:1px solid #d9534f;");
                                                    $('#alert-mobile-' + value.id).html('Maaf, Stok Produk tidak mencukupi!');
                                                }
                                            } else if (value.status == 'failed_min_buy') {
                                                if (value.option == 'true') {
                                                    $('#cart-option-' + value.id).attr("style", "border-color:#d9534f;border-bottom:1px solid #d9534f;");
                                                    $('#alert-option-' + value.id).html('Minimal pembelian ' + value.min_buy + ' barang');
                                                } else {
                                                    $('#cart-' + value.id).attr("style", "border-color:#d9534f;border-bottom:1px solid #d9534f;");
                                                    $('#alert-' + value.id).html('Minimal pembelian ' + value.min_buy + ' barang');
                                                }
                                            }
                                        })
                                    } else {
                                        document.location.href = site_url + 'checkout';
                                    }
                                }
                            });
                        } else {
                            Swal.fire({
                                type: 'error',
                                title: 'Belum ada alamat!',
                                text: 'Silahkan tambah alamat terlebih dahulu!',
                                customClass: 'swal-class',
                                showCancelButton: true,
                                confirmButtonText: 'Tambah Sekarang',
                                cancelButtonText: "Nanti Saja",
                                closeOnConfirm: false,
                                closeOnCancel: false,
                                confirmButtonColor: '#a7c22a',
                                cancelButtonColor: '#d9534f'
                            }).then((result) => {
                                if (result.value) {
                                    window.location.href = site_url + 'member/address';
                                }
                            });
                        }
                    }
                });
            } else if (response == 'error_not_phone') {
                Swal.fire({
                    type: 'error',
                    title: 'No HP Belum diisi',
                    text: 'Silahkan Isi No HP Terlebih Dahulu',
                    customClass: 'swal-class',
                    showCancelButton: true,
                    confirmButtonText: 'Isi Sekarang',
                    cancelButtonText: "Nanti Saja",
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    confirmButtonColor: '#a7c22a',
                    cancelButtonColor: '#d9534f'
                }).then((result) => {
                    if (result.value) {
                        window.location.href = site_url + 'member/profile';
                    }
                });
            } else {
                Swal.fire({
                    type: 'error',
                    title: 'No HP Belum diverifikasi',
                    text: 'Silahkan Verifikasi No HP Terlebih Dahulu',
                    customClass: 'swal-class',
                    showCancelButton: true,
                    confirmButtonText: 'Verifikasi Sekarang',
                    cancelButtonText: "Nanti Saja",
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    confirmButtonColor: '#a7c22a',
                    cancelButtonColor: '#d9534f'
                }).then((result) => {
                    if (result.value) {
                        window.location.href = site_url + 'member/profile';
                    }
                });
            }
        }
    });
}

function formatRupiah(angka, prefix) {
    angka = angka.toString();
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split = number_string.split(','),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp ' + rupiah : '');
}

let up_qty = (id, option = false) => {
    id = id.replace(' ', '');
    if (option) {
        let input_qty_option = $('#qty-option-' + id).val();
        let input_qty_mobile_option = $('#qty-mobile-option-' + id).val();
        let new_qty_option = $('#qty-option-' + id).val(Number(input_qty_option) + 1);
        let new_qty_mobile_option = $('#qty-mobile-option-' + id).val(Number(input_qty_mobile_option) + 1);
        let product_qty_option = $('#qty-option-' + id).data('qty');
        let min_qty_option = $('#qty-option-' + id).data('min_buy');
        if (min_qty_option > 1) {
            if (new_qty_option.val() >= min_qty_option) {
                $('input[name=qty]').css('border', 'solid #DDDDDD');
                $('input[name=qty]').css('border-width', '0 2px');
                $('input[name=qty_mobile]').css('border', 'solid #DDDDDD');
                $('input[name=qty_mobile]').css('border-width', '0 2px');
                $('#err_qty').css('display', 'none');
            }
            if (new_qty_option.val() > min_qty_option) {
                $('#btn_down_qty-option-' + id).removeAttr('disabled');
            }
            if (new_qty_option.val() == product_qty_option) {
                $('#btn_up_qty-option-' + id).attr('disabled', true);
            }
            if (new_qty_option.val() <= min_qty_option) {
                $('#btn_down_qty-option-' + id).attr('disabled', true);
            }
        } else {
            if (new_qty_option.val() > 0 || new_qty_mobile_option.val() > 0) {
                $('input[name=qty]').css('border', 'solid #DDDDDD');
                $('input[name=qty]').css('border-width', '0 2px');
                $('input[name=qty_mobile]').css('border', 'solid #DDDDDD');
                $('input[name=qty_mobile]').css('border-width', '0 2px');
                $('#err_qty').css('display', 'none');
            }
            if (new_qty_option.val() > 1) {
                $('#btn_down_qty-option-' + id).removeAttr('disabled');
            }
            if (new_qty_mobile_option.val() > 1) {
                $('#btn_down_qty-mobile-option-' + id).removeAttr('disabled');
            }

            if (new_qty_option.val() == product_qty_option) {
                $('#btn_up_qty-option-' + id).attr('disabled', true);
            }
            if (new_qty_mobile_option.val() == product_qty_option) {
                $('#btn_up_qty-mobile-option-' + id).attr('disabled', true);
            }
        }
    } else {
        let input_qty = $('#qty-' + id).val();
        let input_qty_mobile = $('#qty-mobile-' + id).val();
        let new_qty = $('#qty-' + id).val(Number(input_qty) + 1);
        let new_qty_mobile = $('#qty-mobile-' + id).val(Number(input_qty_mobile) + 1);
        let product_qty = $('#qty-' + id).data('qty');
        let min_qty = $('#qty-' + id).data('min_buy');
        if (min_qty > 1) {
            if (new_qty.val() >= min_qty) {
                $('input[name=qty]').css('border', 'solid #DDDDDD');
                $('input[name=qty]').css('border-width', '0 2px');
                $('input[name=qty_mobile]').css('border', 'solid #DDDDDD');
                $('input[name=qty_mobile]').css('border-width', '0 2px');
                $('#err_qty').css('display', 'none');
            }
            if (new_qty.val() > min_qty) {
                $('#btn_down_qty-' + id).removeAttr('disabled');
            }
            if (new_qty.val() == product_qty) {
                $('#btn_up_qty-' + id).attr('disabled', true);
            }
            if (new_qty.val() <= min_qty) {
                $('#btn_down_qty-' + id).attr('disabled', true);
            }
        } else {
            if (new_qty.val() > 0 || new_qty_mobile.val() > 0) {
                $('input[name=qty]').css('border', 'solid #DDDDDD');
                $('input[name=qty]').css('border-width', '0 2px');
                $('input[name=qty_mobile]').css('border', 'solid #DDDDDD');
                $('input[name=qty_mobile]').css('border-width', '0 2px');
                $('#err_qty').css('display', 'none');
            }
            if (new_qty.val() > 1) {
                $('#btn_down_qty-' + id).removeAttr('disabled');
            }
            if (new_qty_mobile.val() > 1) {
                $('#btn_down_qty-mobile-' + id).removeAttr('disabled');
            }

            if (new_qty.val() == product_qty) {
                $('#btn_up_qty-' + id).attr('disabled', true);
            }
            if (new_qty_mobile.val() == product_qty) {
                $('#btn_up_qty-mobile-' + id).attr('disabled', true);
            }
        }
    }

}

let down_qty = (id, option = false) => {
    id = id.replace(' ', '');
    if (option) {
        let input_qty_option = $('#qty-option-' + id).val();
        let input_qty_mobile_option = $('#qty-mobile-option-' + id).val();
        let new_qty_option = $('#qty-option-' + id).val(Number(input_qty_option) - 1);
        let new_qty_mobile_option = $('#qty-mobile-option-' + id).val(Number(input_qty_mobile_option) - 1);
        let product_qty_option = $('#qty-option-' + id).data('qty');
        let min_qty_option = $('#qty-option-' + id).data('min_buy');
        if (min_qty_option > 1) {
            if (new_qty_option.val() <= min_qty_option) {
                $('#btn_down_qty-option-' + id).attr('disabled', true);
            }
            if (new_qty_option.val() > min_qty_option) {
                $('#btn_down_qty-option-' + id).removeAttr('disabled');
            }
            if (new_qty_option.val() < product_qty_option) {
                $('#btn_up_qty-option-' + id).removeAttr('disabled');
            }
        } else {
            if (new_qty_option.val() < 2) {
                $('#btn_down_qty-option-' + id).attr('disabled', true);
            }

            if (new_qty_mobile_option.val() < 2) {
                $('#btn_down_qty-mobile-option-' + id).attr('disabled', true);
            }

            if (new_qty_option.val() < product_qty_option) {
                $('#btn_up_qty-option-' + id).removeAttr('disabled');
            }
            if (new_qty_mobile_option.val() < product_qty_option) {
                $('#btn_up_qty-mobile-option-' + id).removeAttr('disabled');
            }

            if (new_qty_option.val() >= product_qty_option) {
                $('#btn_down_qty-option-' + id).removeAttr('disabled');
                $('#btn_up_qty-option-' + id).attr('disabled', true);
            }
            if (new_qty_mobile_option.val() >= product_qty_option) {
                $('#btn_up_qty-mobile-option-' + id).attr('disabled', true);
                $('#btn_down_qty-mobile-option-' + id).removeAttr('disabled');
            }

            if (product_qty_option == 1 && new_qty_option.val() == 1) {
                $('#btn_down_qty-option-' + id).attr('disabled', true);
                $('#btn_up_qty-option-' + id).attr('disabled', true);
            }
            if (product_qty_option == 1 && new_qty_mobile_option.val() == 1) {
                $('#btn_down_qty-mobile-option-' + id).attr('disabled', true);
                $('#btn_up_qty-mobile-option-' + id).attr('disabled', true);
            }

            if (new_qty_option.val() == product_qty_option) {
                $('#btn_up_qty-option-' + id).attr('disabled', true);
            }
            if (new_qty_mobile_option.val() == product_qty_option) {
                $('#btn_up_qty-mobile-option-' + id).attr('disabled', true);
            }
        }
    } else {
        let input_qty = $('#qty-' + id).val();
        let input_qty_mobile = $('#qty-mobile-' + id).val();
        let new_qty = $('#qty-' + id).val(Number(input_qty) - 1);
        let new_qty_mobile = $('#qty-mobile-' + id).val(Number(input_qty_mobile) - 1);
        let product_qty = $('#qty-' + id).data('qty');
        let min_qty = $('#qty-' + id).data('min_buy');
        if (min_qty > 1) {
            if (new_qty.val() <= min_qty) {
                $('#btn_down_qty-' + id).attr('disabled', true);
            }
            if (new_qty.val() > min_qty) {
                $('#btn_down_qty-' + id).removeAttr('disabled');
            }
            if (new_qty.val() < product_qty) {
                $('#btn_up_qty-' + id).removeAttr('disabled');
            }
        } else {
            if (new_qty.val() < 2) {
                $('#btn_down_qty-' + id).attr('disabled', true);
            }

            if (new_qty_mobile.val() < 2) {
                $('#btn_down_qty-mobile-' + id).attr('disabled', true);
            }

            if (new_qty.val() < product_qty) {
                $('#btn_up_qty-' + id).removeAttr('disabled');
            }
            if (new_qty_mobile.val() < product_qty) {
                $('#btn_up_qty-mobile-' + id).removeAttr('disabled');
            }

            if (new_qty.val() >= product_qty) {
                $('#btn_down_qty-' + id).removeAttr('disabled');
                $('#btn_up_qty-' + id).attr('disabled', true);
            }
            if (new_qty_mobile.val() >= product_qty) {
                $('#btn_up_qty-mobile-' + id).attr('disabled', true);
                $('#btn_down_qty-mobile-' + id).removeAttr('disabled');
            }

            if (product_qty == 1 && new_qty.val() == 1) {
                $('#btn_down_qty-' + id).attr('disabled', true);
                $('#btn_up_qty-' + id).attr('disabled', true);
            }
            if (product_qty == 1 && new_qty_mobile.val() == 1) {
                $('#btn_down_qty-mobile-' + id).attr('disabled', true);
                $('#btn_up_qty-mobile-' + id).attr('disabled', true);
            }

            if (new_qty.val() == product_qty) {
                $('#btn_up_qty-' + id).attr('disabled', true);
            }
            if (new_qty_mobile.val() == product_qty) {
                $('#btn_up_qty-mobile-' + id).attr('disabled', true);
            }
        }
    }
}

function qty_form(id, option = false) {
    if (option) {
        let qty = parseInt($('#qty-option-' + id).val());
        let qty_mobile = parseInt($('#qty-mobile-option-' + id).val());
        let product_qty = parseInt($('#qty-option-' + id).data('qty'));
        let product_qty_mobile = parseInt($('#qty-mobile-option-' + id).data('qty'));
        let min_qty = parseInt($('#qty-option-' + id).data('min_buy'));
        if (min_qty > 1) {
            if (isNaN(qty) || qty < 1) {
                $('#btn_down_qty-option-' + id).attr('disabled', true);
                qty = $('#qty-option-' + id).val(1);
                if (qty == product_qty) {
                    $('#btn_up_qty-option-' + id).attr('disabled', true);
                } else {
                    $('#btn_up_qty-option-' + id).removeAttr('disabled');
                }
            } else {
                $('#qty-option-' + id).val(qty);
            }
            if (min_qty >= qty) {
                $('#btn_down_qty-option-' + id).attr('disabled', true);
            }
            if (qty > min_qty) {
                $('#btn_down_qty-option-' + id).removeAttr('disabled');
            }
            if (qty >= product_qty) {
                $('#btn_up_qty-option-' + id).attr('disabled', true);
            }
            if (qty < product_qty) {
                $('#btn_up_qty-option-' + id).removeAttr('disabled');
            }
        } else {
            if (isNaN(qty) || qty < 1) {
                $('#btn_down_qty-option-' + id).attr('disabled', true);
                qty = $('#qty-option-' + id).val(1);
                if (qty == product_qty) {
                    $('#btn_up_qty-option-' + id).attr('disabled', true);
                } else {
                    $('#btn_up_qty-option-' + id).removeAttr('disabled');
                }
            } else {
                $('#qty-option-' + id).val(qty);
            }

            if (qty_mobile < 1 || isNaN(qty_mobile)) {
                $('#btn_down_qty-mobile-option-' + id).attr('disabled', true);
                qty_mobile = $('#qty-mobile-option-' + id).val(1);
                if (qty_mobile == product_qty_mobile) {
                    $('#btn_up_qty-mobile-option-' + id).attr('disabled', true);
                } else {
                    $('#btn_up_qty-mobile-option-' + id).removeAttr('disabled');
                }
            } else {
                $('#qty-mobile-option-' + id).val(qty_mobile);
            }

            if (qty == 1) {
                $('#btn_down_qty-option-' + id).attr('disabled', true);
                $('#btn_up_qty-option-' + id).removeAttr('disabled');
            } else if (qty > 1) {
                $('#btn_down_qty-option-' + id).removeAttr('disabled');
            }

            if (qty_mobile == 1) {
                $('#btn_down_qty-mobile-option-' + id).attr('disabled', true);
                $('#btn_up_qty-mobile-option-' + id).removeAttr('disabled');
            } else if (qty_mobile > 1) {
                $('#btn_down_qty-mobile-option-' + id).removeAttr('disabled');
            }

            if (qty >= product_qty) {
                $('#btn_up_qty-option-' + id).attr('disabled', true);
            } else {
                $('#btn_up_qty-option-' + id).removeAttr('disabled');
            }

            if (qty_mobile >= product_qty_mobile) {
                $('#btn_up_qty-mobile-option-' + id).attr('disabled', true);
                console.log($('#btn_up_qty-mobile-option-' + id));
            } else {
                $('#btn_up_qty-mobile-option-' + id).removeAttr('disabled');
            }

            if (product_qty == 1 || product_qty_mobile == 1) {
                $('#btn_up_qty-option-' + id).attr('disabled', true);
                $('#btn_up_qty-mobile-option-' + id).attr('disabled', true);
            }
            if (product_qty == 1 && qty > 1) {
                $('#btn_down_qty-option-' + id).removeAttr('disabled');
            }
            if (product_qty_mobile == 1 && qty_mobile > 1) {
                $('#btn_down_qty-mobile-option-' + id).removeAttr('disabled');
            }
        }
    } else {
        let qty = parseInt($('#qty-' + id).val());
        let qty_mobile = parseInt($('#qty-mobile-' + id).val());
        let product_qty = parseInt($('#qty-' + id).data('qty'));
        let min_qty = parseInt($('#qty-' + id).data('min_buy'));
        if (min_qty > 1) {
            if (isNaN(qty) || qty < 1) {
                $('#btn_down_qty-' + id).attr('disabled', true);
                qty = $('#qty-' + id).val(1);
                if (qty == product_qty) {
                    $('#btn_up_qty-' + id).attr('disabled', true);
                } else {
                    $('#btn_up_qty-' + id).removeAttr('disabled');
                }
            } else {
                $('#qty-' + id).val(qty);
            }

            if (min_qty >= qty) {
                $('#btn_down_qty-' + id).attr('disabled', true);
            }
            if (qty > min_qty) {
                $('#btn_down_qty-' + id).removeAttr('disabled');
            }
            if (qty >= product_qty) {
                $('#btn_up_qty-' + id).attr('disabled', true);
            }
            if (qty < product_qty) {
                $('#btn_up_qty-' + id).removeAttr('disabled');
            }
        } else {
            if (isNaN(qty) || qty < 1) {
                $('#btn_down_qty-' + id).attr('disabled', true);
                qty = $('#qty-' + id).val(1);
                if (qty == product_qty) {
                    $('#btn_up_qty-' + id).attr('disabled', true);
                } else {
                    $('#btn_up_qty-' + id).removeAttr('disabled');
                }
            } else {
                $('#qty-' + id).val(qty);
            }

            if (qty_mobile < 1 || isNaN(qty_mobile)) {
                $('#btn_down_qty-mobile-' + id).attr('disabled', true);
                qty_mobile = $('#qty-mobile-' + id).val(1);
                if (qty_mobile == product_qty) {
                    $('#btn_up_qty-mobile-' + id).attr('disabled', true);
                } else {
                    $('#btn_up_qty-mobile-' + id).removeAttr('disabled');
                }
            } else {
                $('#qty-mobile-' + id).val(qty_mobile);
            }

            if (qty == 1) {
                $('#btn_down_qty-' + id).attr('disabled', true);
                $('#btn_up_qty-' + id).removeAttr('disabled');
            } else if (qty > 1) {
                $('#btn_down_qty-' + id).removeAttr('disabled');
            }

            if (qty_mobile == 1) {
                $('#btn_down_qty-mobile-' + id).attr('disabled', true);
                $('#btn_up_qty-mobile-' + id).removeAttr('disabled');
            } else if (qty_mobile > 1) {
                $('#btn_down_qty-mobile-' + id).removeAttr('disabled');
            }

            if (qty >= product_qty) {
                $('#btn_up_qty-' + id).attr('disabled', true);
            } else {
                $('#btn_up_qty-' + id).removeAttr('disabled');
            }

            if (qty_mobile >= product_qty) {
                $('#btn_up_qty-mobile-' + id).attr('disabled', true);
            } else {
                $('#btn_up_qty-mobile-' + id).removeAttr('disabled');
            }

            if (product_qty == 1) {
                $('#btn_up_qty-' + id).attr('disabled', true);
                $('#btn_up_qty-mobile-' + id).attr('disabled', true);
            }
            if (product_qty == 1 && qty > 1) {
                $('#btn_down_qty-' + id).removeAttr('disabled');
            }
            if (product_qty == 1 && qty_mobile > 1) {
                $('#btn_down_qty-mobile-' + id).removeAttr('disabled');
            }
        }
    }
}