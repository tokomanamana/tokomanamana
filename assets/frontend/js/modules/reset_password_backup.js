$(function() {
    $('#reset_password_form').submit(function(e) {
        e.preventDefault();
        let link = current_url;
        $.ajax({
            url: link,
            data: $(this).serialize(),
            type: 'post',
            success: function(data) {
                data = JSON.parse(data);
                if(data.status == 'error') {
                    $('#reset_password_form .message_validation').html(data.message);
                } else {
                    Swal.fire({
                        title: data.message,
                        type: data.status,
                        timer: 1000,
                        showConfirmButton: false
                    }).then((result) => {
                        document.location.href = site_url + 'member/login';
                    });
                }
            }
        })
    });
})
function password_reset_show(e, type) {
    if(type == 'password') {
        let input = $('#reset_password_form input[name=password]');
        if (input.attr("type") == "password") {
          input.attr("type", "text");
        } else {
          input.attr("type", "password");
        }
    } else {
        let input = $('#reset_password_form input[name=confirm_password]');
        if (input.attr("type") == "password") {
          input.attr("type", "text");
        } else {
          input.attr("type", "password");
        }
    }
}