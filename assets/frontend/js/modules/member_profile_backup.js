$(document).ready(function () {
    $('#province').change(function (e) {
        e.preventDefault();
        var id = $(this).val();
        getCities(id);
    });
    $('#city').change(function (e) {
        e.preventDefault();
        var id = $(this).val();
        getDistricts(id);
    });
    $('#change_password').change(function () {
        if (this.checked) {
            $('.password').attr('required', 'required');
            $('#password').show();
        } else {
            $('.password').removeAttr('required');
            $('#password').hide();
        }
    });
    $('#save_email_customer').submit(function(e) {
        const loader = '<div class="lds-ring" style="margin-top:50px; margin-bottom:20px;"><div></div><div></div><div></div><div></div></div>';
        $('#add_email_modal .error_validation').html('');
        $('#add_email_modal input').css('border', '1px solid #CCC');
        e.preventDefault();
        let link = $(this).attr('action');
        $.ajax({
            url: link,
            type: 'post',
            data: $(this).serialize(),
            success: function(data) {
                data = JSON.parse(data);
                if(data.status == 'error') {
                    $.each(data.message, function(i, value) {
                        $('#error_' + value.type).html(value.message);
                        $('#add_email_modal input[name=' + value.type + ']').css('border', '1px solid #D9534F');
                    });
                } else if(data.status == 'error_otp') {
                    $('#add_email_modal .message_error_email').html(data.message);
                } else {
                    $('#myTabContent').html(loader);
                    Swal.fire({
                        type: data.status,
                        title: data.message,
                        timer: 1000,
                        showConfirmButton: false
                    });
                    $('#myTabContent').load(window.location.href + ' #myTabContent');
                    $('#add_email_modal').modal('hide');
                    $('#add_email_modal #button_edit_email_verification').data('otp', true);
                    $('#add_email_modal #button_edit_email_verification').text('Minta kode verifikasi');
                    $('.notification_profile_container').load(window.location.href + ' #notification_profile');
                    clear_interval();
                }
            }
        });
    });
    $('#save_name_customer').submit(function(e) {
        const loader = '<div class="lds-ring" style="margin-top:50px; margin-bottom:20px;"><div></div><div></div><div></div><div></div></div>';
        e.preventDefault();
        let link = $(this).attr('action');
        $.ajax({
            url: link,
            type: 'post',
            data: $(this).serialize(),
            success: function(data) {
                data = JSON.parse(data);
                if(data.status == 'error') {
                    $('#add_name_modal #error_name').html(data.message);
                    $('#add_name_modal input[name=name]').css('border', '1px solid #D9534F');
                } else {
                    $('#myTabContent').html(loader);
                    Swal.fire({
                        type: data.status,
                        title: data.message,
                        timer: 1000,
                        showConfirmButton: false
                    });
                    $('#myTabContent').load(window.location.href + ' #myTabContent');
                    $('#add_name_modal').modal('hide');
                    $('.notification_profile_container').load(window.location.href + ' #notification_profile');
                }
            }
        });
    });
    $('#save_birthday').submit(function(e) {
        const loader = '<div class="lds-ring" style="margin-top:50px; margin-bottom:20px;"><div></div><div></div><div></div><div></div></div>';
        e.preventDefault();
        let link = $(this).attr('action');
        $.ajax({
            url: link,
            type: 'post',
            data: $(this).serialize(),
            success: function(data) {
                if(data == 'success') {
                    $('#myTabContent').html(loader);
                    Swal.fire({
                        type: data,
                        title: 'Tanggal lahir berhasil disimpan!',
                        timer: 1000,
                        showConfirmButton: false
                    });
                    $('#myTabContent').load(window.location.href + ' #myTabContent');
                    $('#birthday_modal').modal('hide');
                    $('.notification_profile_container').load(window.location.href + ' #notification_profile');
                } else {
                    let alert = `<div class="alert alert-danger" role="alert">
                                  Tanggal tidak valid!
                                </div>`;
                    $('#birthday_modal .message_error').html(alert);
                }
            }
        });
    });
    $('#save_gender').submit(function(e) {
        const loader = '<div class="lds-ring" style="margin-top:50px; margin-bottom:20px;"><div></div><div></div><div></div><div></div></div>';
        e.preventDefault();
        let link = $(this).attr('action');
        $.ajax({
            url: link,
            type: 'post',
            data: $(this).serialize(),
            success: function(data) {
                if(data == 'success') {
                    $('#myTabContent').html(loader);
                    Swal.fire({
                        type: data,
                        title: 'Jenis kelamin berhasil disimpan!',
                        timer: 1000,
                        showConfirmButton: false
                    });
                    $('#myTabContent').load(window.location.href + ' #myTabContent');
                    $('#gender_modal').modal('hide');
                    $('.notification_profile_container').load(window.location.href + ' #notification_profile');
                } else {
                    let alert = `<div class="alert alert-danger" role="alert">
                                  Terjadi suatu kesalahan!
                                </div>`;
                    $('#gender_modal .message_error').html(alert);
                }
            }
        });
    });
    $('#add_email_modal #button_edit_email_verification').on('click', function(e) {
        let loader = '<div class="lds-ring" style="display:block;left:47%;"><div style="border:1px solid black;border-color:black transparent transparent transparent"></div><div style="border:1px solid black;border-color:black transparent transparent transparent"></div><div style="border:1px solid black;border-color:black transparent transparent transparent"></div><div style="border:1px solid black;border-color:black transparent transparent transparent"></div></div>';
        let otp = $(this).data('otp');
        if(otp == true) {
            $.ajax({
                url: site_url + 'auth/member/get_code_verification',
                data: '',
                success: function(data) {
                    data = JSON.parse(data);
                    if(data.status == 'success') {
                        clear_interval();
                        $('#add_email_modal #button_edit_email_verification').html(loader);
                        set_interval();
                        $('#add_email_modal .message_send_verification').html(data.message);
                    } else if(data.status == 'error_send') {
                        clear_interval();
                        $('#add_email_modal #button_edit_email_verification').html(loader);
                        set_interval();
                        $('#add_email_modal .message_send_verification').html(data.message);
                    } else {
                        $('#add_email_modal .message_send_verification').html(data.message);
                    }
                }
            })
        } else {
            return false;
        }
    });
    $('#AddBank select').selectize();
    $('#add_phone_modal #input_phone').on('keyup', function(event) {
        const key = event.keyCode;
        return ((key >= 48 && key <= 57) || // Allow number line
            (key >= 96 && key <= 105) // Allow number pad
        );
    });
    $('#add_phone_modal input').on('blur keyup', function() {
        let name = $(this).attr('name');
        $(this).css('border', '1px solid #CCC');
        $('#add_phone_modal #error_' + name).html('');
    });
    $('#add_phone_modal #save_phone_customer').submit(function(e) {
        const loader = '<div class="lds-ring" style="margin-top:50px; margin-bottom:20px;"><div></div><div></div><div></div><div></div></div>';
        e.preventDefault();
        $('#add_phone_modal .message_save_phone').html('');
        let link = $(this).attr('action');
        $.ajax({
            url: link,
            data: $(this).serialize(),
            type: 'POST',
            success: function(data) {
                data = JSON.parse(data);
                if(data.status == 'error') {
                    $.each(data.message, function(index, value) {
                        $('#add_phone_modal #error_' + value.type).html(value.message);
                        $('#add_phone_modal input[name=' + value.type + ']').css('border', '1px solid #D9534F');
                    })
                } else if (data.status == 'error_update') {
                    $('#add_phone_modal .message_save_phone').html(data.message);
                } else {
                    $('#myTabContent').html(loader);
                    $('#add_phone_modal').modal('hide');
                    Swal.fire({
                        title: data.message,
                        type: data.status,
                        timer: 1000,
                        showConfirmButton: false
                    });
                    $('#myTabContent').append(loader);
                    $('#myTabContent').load(window.location.href + ' #myTabContent');
                    $('.notification_profile_container').load(window.location.href + ' #notification_profile');
                    $('#add_phone_modal').load(window.location.href + ' #add_phone_modal');
                    $('#message_not_phone_').load(window.location.href + ' #message_not_phone_');
                    $('#add_email_modal #save_email_customer .message_edit').load(window.location.href + ' #message_edit_email');

                    $('#add_email_modal .modal-content #modal_email_body').load(window.location.href + ' #save_email_customer', function() {
                        $('#save_email_customer').submit(function(e) {
                            const loader = '<div class="lds-ring" style="margin-top:50px; margin-bottom:20px;"><div></div><div></div><div></div><div></div></div>';
                            $('#add_email_modal .error_validation').html('');
                            $('#add_email_modal input').css('border', '1px solid #CCC');
                            e.preventDefault();
                            let link = $(this).attr('action');
                            $.ajax({
                                url: link,
                                type: 'post',
                                data: $(this).serialize(),
                                success: function(data) {
                                    data = JSON.parse(data);
                                    if(data.status == 'error') {
                                        $.each(data.message, function(i, value) {
                                            $('#error_' + value.type).html(value.message);
                                            $('#add_email_modal input[name=' + value.type + ']').css('border', '1px solid #D9534F');
                                        });
                                    } else if(data.status == 'error_otp') {
                                        $('#add_email_modal .message_error_email').html(data.message);
                                    } else {
                                        $('#myTabContent').html(loader);
                                        Swal.fire({
                                            type: data.status,
                                            title: data.message,
                                            timer: 1000,
                                            showConfirmButton: false
                                        });
                                        $('#myTabContent').load(window.location.href + ' #myTabContent');
                                        $('#add_email_modal').modal('hide');
                                        $('#add_email_modal #button_edit_email_verification').data('otp', true);
                                        $('#add_email_modal #button_edit_email_verification').text('Minta kode verifikasi');
                                        $('.notification_profile_container').load(window.location.href + ' #notification_profile');
                                        clear_interval();
                                    }
                                }
                            });
                        });
                        $('#add_email_modal #button_edit_email_verification').on('click', function(e) {
                            let loader = '<div class="lds-ring" style="display:block;left:47%;"><div style="border:1px solid black;border-color:black transparent transparent transparent"></div><div style="border:1px solid black;border-color:black transparent transparent transparent"></div><div style="border:1px solid black;border-color:black transparent transparent transparent"></div><div style="border:1px solid black;border-color:black transparent transparent transparent"></div></div>';
                            let otp = $(this).data('otp');
                            if(otp == true) {
                                $.ajax({
                                    url: site_url + 'auth/member/get_code_verification',
                                    data: '',
                                    success: function(data) {
                                        data = JSON.parse(data);
                                        if(data.status == 'success') {
                                            clear_interval();
                                            $('#add_email_modal #button_edit_email_verification').html(loader);
                                            set_interval();
                                            $('#add_email_modal .message_send_verification').html(data.message);
                                        } else if(data.status == 'error_send') {
                                            clear_interval();
                                            $('#add_email_modal #button_edit_email_verification').html(loader);
                                            set_interval();
                                            $('#add_email_modal .message_send_verification').html(data.message);
                                        } else {
                                            $('#add_email_modal .message_send_verification').html(data.message);
                                        }
                                    }
                                })
                            } else {
                                return false;
                            }
                        });
                    });
                    
                }
            }
        })
    });
    $('#input_code_verification_email_modal #form_input_code_verification_email').submit(function(e) {
        const loader = '<div class="lds-ring" style="margin-top:50px; margin-bottom:20px;"><div></div><div></div><div></div><div></div></div>';
        e.preventDefault();
        $('#input_code_verification_email_modal .message_email_verification').html('');
        $('#input_code_verification_email_modal #error_code_verification').html('');
        $('#input_code_verification_email_modal input[name=code_verification]').css('border', '1px solid #CCC');
        let link = $(this).attr('action');
        $.ajax({
            url: link,
            data: $(this).serialize(),
            type: 'POST',
            success: function(data) {
                data = JSON.parse(data);
                if(data.status == 'error_validation') {
                    $('#input_code_verification_email_modal #error_code_verification').html(data.message);
                    $('#input_code_verification_email_modal input[name=code_verification]').css('border', '1px solid #D9534F');
                } else if (data.status == 'error_verification') {
                    $('#input_code_verification_email_modal .message_email_verification').html(data.message);
                } else {
                    $('#input_code_verification_email_modal').modal('hide');
                    Swal.fire({
                        title: data.message,
                        type: data.status,
                        timer: 1000,
                        showConfirmButton: false
                    }).then((result) => {
                        $('#myTabContent').html(loader);
                        $('#myTabContent').load(window.location.href + ' #myTabContent');
                        $('.notification_profile_container').load(window.location.href + ' #notification_profile');
                    });
                }
            }
        });
        $('#input_code_verification_email_modal input').on('blur keyup', function() {
            let name = $(this).attr('name');
            $(this).css('border', '1px solid #CCC');
            $('#input_code_verification_email_modal #error_' + name).html('');
        });
    })
});
function formatState (bank_code) {
  if (!bank_code.id) {
    return bank_code.text;
  }
  var baseUrl = "/user/pages/images/flags";
  var $bank_code = $(
    '<span>' + bank_code.text + '</span>'
  );
  return $bank_code;
};
let x1;
let x2;
function set_interval() {
    let duration = 120;
    x1 = setInterval(function() {
        if(--duration) {
            $('#add_email_modal #button_edit_email_verification').text('Kirim Ulang (' + duration + ' s)');
            $('#add_email_modal #button_edit_email_verification').data('otp', false);
        } else {
            clearInterval(x1);
            $('#add_email_modal #button_edit_email_verification').text('Minta kode verifikasi');
            $('#add_email_modal #button_edit_email_verification').data('otp', true);
        }
    }, 1000);
}
function clear_interval() {
    clearInterval(x1);
    $('#add_email_modal #button_edit_email_verification').text('Minta kode verifikasi');
    $('#add_email_modal #button_edit_email_verification').data('otp', true);
}
function set_interval2() {
    let duration = 120;
    x2 = setInterval(function() {
        if(--duration) {
            $('#add_phone_modal #button_send_otp_no_phone').text('Kirim Ulang (' + duration + ' s)');
            $('#add_phone_modal #button_send_otp_no_phone').data('otp', false);
        } else {
            clearInterval(x2);
            $('#add_phone_modal #button_send_otp_no_phone').text('Minta kode verifikasi');
            $('#add_phone_modal #button_send_otp_no_phone').data('otp', true);
        }
    }, 1000);
}
function clear_interval2() {
    clearInterval(x2);
    $('#add_phone_modal #button_send_otp_no_phone').text('Minta kode verifikasi');
    $('#add_phone_modal #button_send_otp_no_phone').data('otp', true);
}
function getCities(province) {
    $('#district option').remove();
    $("#district").append('<option value="">Pilih kecamatan</option>');
    if (province == '' || province == 0) {
        $('#city option').remove();
        $("#city").append('<option value="">Pilih kota</option>');
    } else {
        $.get(base_url + 'checkout/getCities/' + province, function (data) {
            $('#city').html(data);
        });
    }
}
function getDistricts(city) {
    if (city == '' || city == 0) {
        $('#district option').remove();
        $("#district").append('<option value="">Pilih kecamatan</option>');
    } else {
        $.get(base_url + 'checkout/getDistricts/' + city, function (data) {
            $('#district').html(data);
        });
    }
}
function verification_email(id, type) {
    const loader = '<div class="lds-ring" style="margin-top:50px; margin-bottom:20px;"><div></div><div></div><div></div><div></div></div>';
    let title = '';
    if(type == 'sending') {
        title = 'Verifikasi Sekarang ?';
    } else {
        title = 'Verifikasi Ulang ?';
    }
    Swal.fire({
      title: title,
      type: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Batal'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: site_url + 'auth/member/verification_email',
                data: {id: id},
                type: 'post',
                beforeSend: function() {
                    $("#tags-load").css('display', 'block');
                },
                complete: function() {
                    $("#tags-load").css('display', 'none');
                },
                success: function(data) {
                    data = JSON.parse(data);
                    if(data.status == 'error') {
                        Swal.fire({
                            title: data.message,
                            type: data.status
                        });
                    } else {
                        $('#myTabContent').html(loader);
                        Swal.fire({
                            title: data.message,
                            type: data.status,
                            timer: 1000,
                            showConfirmButton: false
                        });
                        $('#myTabContent').append(loader);
                        $('#myTabContent').html(data.load);
                    }
                }
            });
        }
    })
}
function add_email() {
    const loader = '<div class="lds-ring" style="margin-top:50px; margin-bottom:20px;"><div></div><div></div><div></div><div></div></div>';
    $('#add_email_modal').modal('show');
    $('#error_email').html('');
    $('#add_email_modal input').css('border', '1px solid #CCC');
    $('#add_email_modal form')[0].reset();
    $('#add_email_modal .message_error_email').html('');
    $('#add_email_modal .message_send_verification').html('');
    $.ajax({
        url: site_url + 'auth/member/get_email',
        data: {},
        type: 'get',
        success: function(data) {
            data = JSON.parse(data);
            if(data.type == 'edit') {
                $('#add_email_modal input[name=email]').val(data.value);
            }
        }
    });
}
function add_name() {
    const loader = '<div class="lds-ring" style="margin-top:50px; margin-bottom:20px;"><div></div><div></div><div></div><div></div></div>';
    $('#add_name_modal').modal('show');
    $('#error_name').html('');
    $('#add_name_modal input[name=name]').css('border', '1px solid #CCC');
    $('#add_name_modal form')[0].reset();
    $.ajax({
        url: site_url + 'auth/member/get_name',
        data: {},
        type: 'get',
        success: function(data) {
            data = JSON.parse(data);
            if(data.type == 'edit') {
                $('#add_name_modal input[name=name]').val(data.value);
            }
        }
    })
}
function birthday_modal() {
    // const loader = '<div class="lds-ring" style="margin-top:50px; margin-bottom:20px;"><div></div><div></div><div></div><div></div></div>';
    $('#birthday_modal').modal('show');
    // $('#birthday_modal form')[0].reset();
    $('#birthday_modal .message_error').html('');
}
function gender_modal() {
    $('#gender_modal').modal('show');
    $('#gender_modal .message_error').html('');
}
function addBank() {
    $('#AddBank').modal('show');
    $('#AddBank form')[0].reset();
    $('#AddBank .selectize-input .item').remove();
    $('#AddBank .selectize-input #bank_code-selectized').attr('placeholder', 'Pilih Bank');
    $('#AddBank .selectize-input #bank_code-selectized').attr('style', 'width: 60px;');
}
function cek_norek(){
    var norek = $('#bank_account_number').val();
    var bank = $('#bank_code').val();
    var bank_acc_name = $('#bank_account_name').val();
    var bank_branch = $('#bank_branch').val();
    if(norek != '' && bank != 0 && bank_acc_name != '' && bank_branch != ''){
        $('#btn-submit-bank').prop('disabled', false);
    } else {
        $('#btn-submit-bank').prop('disabled', true);
    }
}

function validate(evt) {
  var theEvent = evt || window.event;

  // Handle paste
  if (theEvent.type === 'paste') {
      key = event.clipboardData.getData('text/plain');
  } else {
  // Handle key press
      var key = theEvent.keyCode || theEvent.which;
      key = String.fromCharCode(key);
  }
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}

function submit_bank(){
    const loader = '<div class="lds-ring" style="margin-top:50px; margin-bottom:20px;"><div></div><div></div><div></div><div></div></div>';
    $.ajax({
        url: site_url + 'auth/member/save_norek',
        type: 'post',
        data: $('#AddBank input, select'),
        success: function (response) {
            response = JSON.parse(response);
            if (response.status == 'success') {
                $('#myTabContent').html(loader);
                Swal.fire({
                  type: 'success',
                  title: 'Rekening Bank Berhasil Ditambah',
                  showConfirmButton: false,
                  timer: 1500
                }).then(function(){ 
                   $('#myTabContent').load(window.location.href + ' #myTabContent', function() {
                       $('#biodata').removeClass('active');
                       $('#norek').addClass('active');
                       $('#norek').addClass('in');
                   });
                });
            } else {
                Swal.fire({
                  type: 'error',
                  title: 'Terjadi Kesalahan!',
                  text: response.message,
                  customClass: 'swal-class'
                });
                return false;
            }
        }
    })
}

function delete_account_bank(id) {
    const loader = '<div class="lds-ring" style="margin-top:50px; margin-bottom:20px;"><div></div><div></div><div></div><div></div></div>';
    Swal.fire({
        title: 'Apakah ingin menghapus rekening bank ?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, hapus!',
        cancelButtonText: 'Batal'
    }).then((result) => {
        if(result.value) {
            $.ajax({
                url: site_url + 'auth/member/delete_account_bank',
                data: {id: id},
                type: 'post',
                success: function(data) {
                    data = JSON.parse(data);
                    if(data.status == 'success') {
                        $('#myTabContent').html(loader);
                        Swal.fire({
                          type: 'success',
                          title: data.message,
                          showConfirmButton: false,
                          timer: 1500
                        }).then(function(){ 
                           $('#myTabContent').load(window.location.href + ' #myTabContent', function() {
                               $('#biodata').removeClass('active');
                               $('#norek').addClass('active');
                               $('#norek').addClass('in');
                           });
                        });
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: 'Terjadi Kesalahan!',
                            text: data.message,
                            customClass: 'swal-class'
                        });
                        return false;
                    }
                }
            })
        }
    })
}
function add_phone(type = null) {
    if(type == 'verif') {
        $('#add_phone_modal').modal('show');
        $('#add_phone_modal .message_save_phone').html('');
        $.ajax({
            url: site_url + 'auth/member/get_code_verification',
            data: {},
            success: function(data) {
                data = JSON.parse(data);
                if(data.status == 'error_send') {
                    clear_interval2();
                    $('#add_phone_modal .message_save_phone').html(data.message);
                    set_interval2();
                } else {
                    clear_interval2();
                    $('#add_phone_modal .message_save_phone').html(data.message);
                    set_interval2();
                }
            }
        })
    } else {
        $('#add_phone_modal').modal('show');
    }
}
function request_verification_code(e, type = null) {
    let loader = '<div class="lds-ring" style="display:block;left:47%;"><div style="border:1px solid black;border-color:black transparent transparent transparent"></div><div style="border:1px solid black;border-color:black transparent transparent transparent"></div><div style="border:1px solid black;border-color:black transparent transparent transparent"></div><div style="border:1px solid black;border-color:black transparent transparent transparent"></div></div>';
    let otp = $(e).data('otp');
    if(type == 'verif') {
        if(otp == true) {
            $('#add_phone_modal input').css('border', '1px solid #ccc');
            $('#add_phone_modal .message_save_phone').html('');
            $('#add_phone_modal .error_validation').html('');
            $.ajax({
                url: site_url + 'auth/member/get_code_verification',
                data: {},
                success: function(data) {
                    data = JSON.parse(data);
                    if (data.status == 'error_send') {
                        clear_interval2();
                        $('#add_phone_modal .message_save_phone').html(data.message);
                        $('#add_phone_modal #button_send_otp_no_phone').html(loader);
                        set_interval2();
                    } else if (data.status == 'error') {
                        $('#add_phone_modal .message_save_phone').html(data.message);
                    } else {
                        clear_interval2();
                        $('#add_phone_modal .message_save_phone').html(data.message);
                        $('#add_phone_modal #button_send_otp_no_phone').html(loader);
                        set_interval2();
                    }
                }
            });
        } else {
            return false;
        }
    } else {
        let phone = $('#add_phone_modal input[name=phone]').val();
        if(otp == true) {
            $('#add_phone_modal input').css('border', '1px solid #ccc');
            $('#add_phone_modal .message_save_phone').html('');
            $('#add_phone_modal .error_validation').html('');
            $.ajax({
                url: site_url + 'auth/member/get_code_verification_new_phone',
                data: {
                    phone: phone
                },
                type: 'post',
                success:function(data) {
                    data = JSON.parse(data);
                    if(data.status == 'error_validation') {
                        $('#add_phone_modal input[name=phone]').css('border', '1px solid #D9534F');
                        $('#add_phone_modal #error_phone').html(data.message);
                    } else if (data.status == 'error') {
                        clear_interval2()
                        $('#add_phone_modal .message_save_phone').html(data.message);
                        $('#add_phone_modal #button_send_otp_no_phone').html(loader);
                        set_interval2();
                    } else {
                        clear_interval2()
                        $('#add_phone_modal .message_save_phone').html(data.message);
                        $('#add_phone_modal #button_send_otp_no_phone').html(loader);
                        set_interval2();
                    }
                }
            });
        } else {
            return false;
        }
    }
}
function change_picture(id, e) {
    const loader = '<div class="lds-ring" style="margin-top:50px; margin-bottom:20px;"><div></div><div></div><div></div><div></div></div>';
    data = new FormData();
    data.append('file', $("#profile_picture")[0].files[0]);
    var imgname  =  $('#profile_picture').val();
    if(imgname != '') {
        var ext =  imgname.substr( (imgname.lastIndexOf('.') +1) );
        if(ext=='jpg' || ext=='jpeg' || ext=='png') {
            $.ajax({
                url: site_url + 'auth/member/change_picture',
                type: 'POST',
                data: data,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                beforeSend: function() {
                    $('#myTabContent').html(loader);
                },
                success: function(data) {
                    $('#myTabContent').html(loader);
                    data = JSON.parse(data);
                    if(data.status == 'success') {
                        Swal.fire({
                            type: data.status,
                            title: data.message,
                            timer: 1000,
                            showConfirmButton: false
                        }).then((result) => {
                            $('#myTabContent').load(window.location.href + ' #myTabContent');
                            $('.navigation-mobile-inner #profile_picture_mobile').load(window.location.href + ' #profile_picture_mobile');
                        });
                    } else {
                        Swal.fire({
                            type: data.status,
                            title: data.message,
                            timer: 1000,
                            showConfirmButton: false
                        }).then((result) => {
                            $('#myTabContent').load(window.location.href + ' #myTabContent');
                            $('.navigation-mobile-inner #profile_picture_mobile').load(window.location.href + ' #profile_picture_mobile');
                        });
                    }
                }
            });
        } else {
            Swal.fire({
                type: 'error',
                title: 'Jenis ekstensi tidak didukung!',
                timer: 1000,
                showConfirmButton: false
            });
        }
    } else {
        return false;
    }
}

function input_code_verification_email() {
    $('#input_code_verification_email_modal').modal('show');
}

function copy_code(val, e) {
    var tempInput = document.createElement("input");
    tempInput.style = "position: absolute; left: -10000px; top: -10000px";
    tempInput.value = val;
    document.body.appendChild(tempInput);
    tempInput.select();
    document.execCommand("copy");
    document.body.removeChild(tempInput);
    $(e).html('<i class="fa fa-check" aria-hidden="true"></i> Tersalin')
}