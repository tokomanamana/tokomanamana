$(document).ready(function () {
    $('input[name=rating_speed]').change(function () {
        $('#star-speed-amount').text($(this).val());
    });
    $('input[name=rating_service]').change(function () {
        $('#star-service-amount').text($(this).val());
    });
    $('input[name=rating_accuracy]').change(function () {
        $('#star-accuracy-amount').text($(this).val());
    });
});
function kecepatan(nomor){
    var src = site_url + '/assets/frontend/images/';
    if (nomor == 1){
        $('#icon-satu').html('<label class="star-1" id="icon-satu" for="star-speed-1"><img style="width:50px" src="'+src+'smiley_no.png"></label>');
        $('#icon-dua').html('<label class="star-2" for="star-speed-2"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
        $('#icon-tiga').html('<label class="star-3" for="star-speed-3"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
        $('#icon-empat').html('<label class="star-4" for="star-speed-4"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
        $('#icon-lima').html('<label class="star-5" for="star-speed-5"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
    }
    else if (nomor == 2){
        $('#icon-satu').html('<label class="star-1" id="icon-satu" for="star-speed-1"><img style="width:50px" src="'+src+'smiley_no.png"></label>');
        $('#icon-dua').html('<label class="star-2" id="icon-dua" for="star-speed-2"><img style="width:50px" src="'+src+'smiley_no.png"></label>');
        $('#icon-tiga').html('<label class="star-3" for="star-speed-3"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
        $('#icon-empat').html('<label class="star-4" for="star-speed-4"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
        $('#icon-lima').html('<label class="star-5" for="star-speed-5"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
    }
    else if (nomor == 3){
        $('#icon-satu').html('<label class="star-1" id="icon-satu" for="star-speed-1"><img style="width:50px" src="'+src+'smiley_.png"></label>');
        $('#icon-dua').html('<label class="star-2" id="icon-dua" for="star-speed-2"><img style="width:50px" src="'+src+'smiley_.png"></label>');
        $('#icon-tiga').html('<label class="star-3" id="icon-tiga" for="star-speed-3"><img style="width:50px" src="'+src+'smiley_.png"></label>');
        $('#icon-empat').html('<label class="star-4" for="star-speed-4"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
        $('#icon-lima').html('<label class="star-5" for="star-speed-5"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
    }
    else if (nomor == 4){
        $('#icon-satu').html('<label class="star-1" id="icon-satu" for="star-speed-1"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
        $('#icon-dua').html('<label class="star-2" id="icon-dua" for="star-speed-2"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
        $('#icon-tiga').html('<label class="star-3" id="icon-tiga" for="star-speed-3"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
        $('#icon-empat').html('<label class="star-4" for="star-speed-4"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
        $('#icon-lima').html('<label class="star-5" for="star-speed-5"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
    }
    else if (nomor == 5){
        $('#icon-satu').html('<label class="star-1" id="icon-satu" for="star-speed-1"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
        $('#icon-dua').html('<label class="star-2" id="icon-dua" for="star-speed-2"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
        $('#icon-tiga').html('<label class="star-3" id="icon-tiga" for="star-speed-3"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
        $('#icon-empat').html('<label class="star-4" for="star-speed-4"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
        $('#icon-lima').html('<label class="star-5" for="star-speed-5"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
    }
}
function pelayanan(nomor){
    var src = site_url + '/assets/frontend/images/';
    if (nomor == 1){
        $('#layan-satu').html('<label class="star-1" id="layan-satu" for="star-service-1"><img style="width:50px" src="'+src+'smiley_no.png"></label>');
        $('#layan-dua').html('<label class="star-2" id="layan-dua" for="star-service-2"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
        $('#layan-tiga').html('<label class="star-3" id="layan-tiga" for="star-service-3"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
        $('#layan-empat').html('<label class="star-4" id="layan-empat" for="star-service-4"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
        $('#layan-lima').html('<label class="star-5" id="layan-lima" for="star-service-5"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
    }
    else if (nomor == 2){
        $('#layan-satu').html('<label class="star-1" id="layan-satu" for="star-service-1"><img style="width:50px" src="'+src+'smiley_no.png"></label>');
        $('#layan-dua').html('<label class="star-2" id="layan-dua" for="star-service-2"><img style="width:50px" src="'+src+'smiley_no.png"></label>');
        $('#layan-tiga').html('<label class="star-3" id="layan-tiga" for="star-service-3"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
        $('#layan-empat').html('<label class="star-4" id="layan-empat" for="star-service-4"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
        $('#layan-lima').html('<label class="star-5" id="layan-lima" for="star-service-5"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
    }
    else if (nomor == 3){
        $('#layan-satu').html('<label class="star-1" id="layan-satu" for="star-service-1"><img style="width:50px" src="'+src+'smiley_.png"></label>');
        $('#layan-dua').html('<label class="star-2" id="layan-dua" for="star-service-2"><img style="width:50px" src="'+src+'smiley_.png"></label>');
        $('#layan-tiga').html('<label class="star-3" id="layan-tiga" for="star-service-3"><img style="width:50px" src="'+src+'smiley_.png"></label>');
        $('#layan-empat').html('<label class="star-4" id="layan-empat" for="star-service-4"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
        $('#layan-lima').html('<label class="star-5" id="layan-lima" for="star-service-5"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
    }
    else if (nomor == 4){
        $('#layan-satu').html('<label class="star-1" id="layan-satu" for="star-service-1"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
        $('#layan-dua').html('<label class="star-2" id="layan-dua" for="star-service-2"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
        $('#layan-tiga').html('<label class="star-3" id="layan-tiga" for="star-service-3"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
        $('#layan-empat').html('<label class="star-4" id="layan-empat" for="star-service-4"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
        $('#layan-lima').html('<label class="star-5" id="layan-lima" for="star-service-5"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
    }
    else if (nomor == 5){
        $('#layan-satu').html('<label class="star-1" id="layan-satu" for="star-service-1"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
        $('#layan-dua').html('<label class="star-2" id="layan-dua" for="star-service-2"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
        $('#layan-tiga').html('<label class="star-3" id="layan-tiga" for="star-service-3"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
        $('#layan-empat').html('<label class="star-4" id="layan-empat" for="star-service-4"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
        $('#layan-lima').html('<label class="star-5" id="layan-lima" for="star-service-5"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
    }
}
function akurasi(nomor){
    var src = site_url + '/assets/frontend/images/';
    if (nomor == 1){
        $('#akur-satu').html('<label class="star-1" id="akur-satu" for="star-accuracy-1"><img style="width:50px" src="'+src+'smiley_no.png"></label>');
        $('#akur-dua').html('<label class="star-2" id="akur-dua" for="star-accuracy-2"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
        $('#akur-tiga').html('<label class="star-3" id="akur-tiga" for="star-accuracy-3"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
        $('#akur-empat').html('<label class="star-4" id="akur-empat" for="star-accuracy-4"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
        $('#akur-lima').html('<label class="star-5" id="akur-lima" for="star-accuracy-5"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
    }
    else if (nomor == 2){
        $('#akur-satu').html('<label class="star-1" id="akur-satu" for="star-accuracy-1"><img style="width:50px" src="'+src+'smiley_no.png"></label>');
        $('#akur-dua').html('<label class="star-2" id="akur-dua" for="star-accuracy-2"><img style="width:50px" src="'+src+'smiley_no.png"></label>');
        $('#akur-tiga').html('<label class="star-3" id="akur-tiga" for="star-accuracy-3"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
        $('#akur-empat').html('<label class="star-4" id="akur-empat" for="star-accuracy-4"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
        $('#akur-lima').html('<label class="star-5" id="akur-lima" for="star-accuracy-5"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
    }
    else if (nomor == 3){
        $('#akur-satu').html('<label class="star-1" id="akur-satu" for="star-accuracy-1"><img style="width:50px" src="'+src+'smiley_.png"></label>');
        $('#akur-dua').html('<label class="star-2" id="akur-dua" for="star-accuracy-2"><img style="width:50px" src="'+src+'smiley_.png"></label>');
        $('#akur-tiga').html('<label class="star-3" id="akur-tiga" for="star-accuracy-3"><img style="width:50px" src="'+src+'smiley_.png"></label>');
        $('#akur-empat').html('<label class="star-4" id="akur-empat" for="star-accuracy-4"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
        $('#akur-lima').html('<label class="star-5" id="akur-lima" for="star-accuracy-5"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
    }
    else if (nomor == 4){
        $('#akur-satu').html('<label class="star-1" id="akur-satu" for="star-accuracy-1"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
        $('#akur-dua').html('<label class="star-2" id="akur-dua" for="star-accuracy-2"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
        $('#akur-tiga').html('<label class="star-3" id="akur-tiga" for="star-accuracy-3"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
        $('#akur-empat').html('<label class="star-4" id="akur-empat" for="star-accuracy-4"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
        $('#akur-lima').html('<label class="star-5" id="akur-lima" for="star-accuracy-5"><img style="width:50px" src="'+src+'smiley_blank.png"></label>');
    }
    else if (nomor == 5){
        $('#akur-satu').html('<label class="star-1" id="akur-satu" for="star-accuracy-1"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
        $('#akur-dua').html('<label class="star-2" id="akur-dua" for="star-accuracy-2"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
        $('#akur-tiga').html('<label class="star-3" id="akur-tiga" for="star-accuracy-3"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
        $('#akur-empat').html('<label class="star-4" id="akur-empat" for="star-accuracy-4"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
        $('#akur-lima').html('<label class="star-5" id="akur-lima" for="star-accuracy-5"><img style="width:50px" src="'+src+'smiley_ok.png"></label>');
    }
}
function add(id) {
    $('#modal-review form')[0].reset();
    $('#modal-review input[name=id]').val(id);
    $('#modal-review #star-amount').text('0');
    $('#modal-review').modal('show');
}
function submit_review() {
    var id = $('#modal-review input[name=id]').val();
    $.ajax({
        url: site_url + 'auth/member/save_review',
        type: 'post',
        data: $('#modal-review input[type=radio]:checked, #modal-review textarea, #modal-review input[type=hidden]'),
        success: function (response) {
            response = JSON.parse(response);
            if (response.status == 'success') {
                $('#review-'+id+' .col-sm-7').html(response.html);
                $('#modal-review').modal('hide');
            } else {
                $('#message').html(response.message);
            }
        }
    })
}