$(document).ready(function () {
    getCheapestService();
    // let elements = $('.choose-shipping');
    // for(i = 0; i < elements.length; i++) {
    //     if(elements[i].value == 'grab') {
    //         elements[i].checked = true;
    //     } else if(elements[i].value == 'pickup') {
    //         elements[i].checked = true;
    //     }

    // }
    // $('.choose-shipping-mobile').prop('selectedIndex',0);
    $('#province').change(function (e) {
        e.preventDefault();
        var id = $(this).val();
        getCities(id);
    });
    $('#city').change(function (e) {
        e.preventDefault();
        var id = $(this).val();
        getDistricts(id);
    });
    // $('.choose-shipping').change(function (e) {
    //     var tag = $(this).prop('tagName');
    //     if (tag == 'SELECT') {
    //         $('#shipping-cost-' + $(this).find(':selected').data('id')).val($(this).find(':selected').data('value'));
    //     } else {
    //         $('#shipping-cost-' + $(this).data('id')).val($(this).data('value'));
    //     }
    //     totalShippingCost();
    // });
    $('#form-shipping_payment').submit(function (e) {
        if (jQuery(':radio[name=shipping]', '#select-shipping').length) {
            return true;
        } else {
            alert('Jasa pengiriman tidak mencakupi wilayah kota Anda. Hubungi kami untuk bantuan.');
            return false;
        }
    });
    // $('#add-address').click(function () {
    //     $('#modal-address').modal('show');
    //     $('#message').html('');
    //     $('#modal-address form')[0].reset();
    //     $('#city').html('<option value="">Pilih kota</option>');
    //     $('#district').html('<option value="">Pilih kecamatan</option>');
    // });
    $('.info-store').popover({
        html: true,
        content: function () {
            var e = $(this);
            var rowid = e.data('id');
            var id = $('#shipping-' + rowid).val();
            return $.ajax({
                url: site_url + 'checkout/info_store',
                type: 'post',
                data: {
                    id: id
                },
                dataType: 'html',
                async: false
            }).responseText;
        }
    });
    $('.payment_method_detail').on('click', function(e) {
        $('.fa-chevron-down').attr('style', 'transform:rotate(0deg);transition:.3s;');
        let input_test = $('.test');
        let id = $(this).data('id');
        let icon = $('#tes-'+id).val();
        $('input[name=payment]').removeAttr('checked');
        let test = $('.collapse').collapse('hide');
        input_test.each(function(i, val) {
            if($(val).attr('id') != 'tes-' + id) {
                $(val).val(99);
            }
        })
        if(icon == 99) {
            $('#icon-' + id).attr('style', 'transform:rotate(-180deg);transition:.3s;');
            $('#tes-'+id).val(0);
        } else {
            $('#icon-' + id).attr('style', 'transform:rotate(0deg);transition:.3s;');
            $('#tes-'+id).val(99);
        }
    })
});
function getCheapestService () {
    let id = [];
    let harga = [];
    let el = $('.cart-item-right-').find('.full-ekspedisi');
    let elm = $('.cart-item-right-').find('.full-ekspedisi-mobile');
    if (el.length > 0) {
        $('.choose-shipping').each(function () {
            if (id.includes($(this).data('id'))) {
                
            } else {
                id.push($(this).data('id'));
            }
        })
    
        for (i = 0; i < id.length; i++) {
            $('input[type=radio].choose-shipping[data-id="'+ id[i] +'"]').each(function () {
                if ($(this).data('value') !== 0) {
                    harga.push(Number($(this).data('value')));
                }
            })
            let min = Math.min( ... harga);
            let option = $('input:radio[data-id="'+ id[i] +'"]').filter('[data-value="'+ min +'"]');
            if (option.length > 0) {
                option.prop('checked', true)
                $('#shipping-cost-'+ id[i]).val(min);
                totalShippingCost();
            }
            harga = [];
        }
    } else if(elm.length > 0) {
        $('select.choose-shipping-mobile option').each(function () {
            if (id.includes($(this).data('id'))) {

            } else {
                id.push($(this).data('id'));
            }
        })

        for (i = 0; i < id.length; i++) {
            $('option[data-id="'+ id[i] +'"]').each(function () {
                if ($(this).data('value') !== 0) {
                    harga.push(Number($(this).data('value')));
                }
            })
            let min = Math.min( ... harga);
            let option = $('#shipping-courier-mobile-'+ id[i] +' option').filter('[data-value="'+ min +'"]');
            if (option.length > 0) {
                option.attr('selected', 'selected').trigger('change');
                $('#shipping-cost-' + id[i]).val(min);
                totalShippingCost();
            } else {
                option.each(function () {
                    $(this).removeAttr('selected');
                })
            }
            
            harga = [];
        }
    }
}
function add_address() {
    $('#modal-address').modal('show');
    $('#message').html('');
    $('#modal-address form')[0].reset();
    $('#city').html('<option value="">Pilih kota</option>');
    $('#district').html('<option value="">Pilih kecamatan</option>');
}
function submitCoupon(){
    $.ajax({
        url: site_url + 'checkout/add_coupon',
        type: 'post',
        data: {'couponCode' : $('.couponCode').val()},
        success: function (response) {
                $('#showDisc').attr('style','display:none;');
                response = JSON.parse(response);
                if (response.status == 'success') {
                    if(response.data.coupon_type == 'branch'){
                        if(response.data.autofill == 1){
                           var r = true;
                        } else {
                            var r = confirm("Semua barang akan dikirim dari cabang terdekat. Lanjutkan??");
                        }
                         if (r == true) {
                            if(response.ekspedisi == 1){
                                if(response.pickup == 1){
                                    var html = '<div class="alert alert-danger  alert-dismissable">'+
                                        '<a href="javascript:void(0)" onclick="resetCoupon()" class="close" aria-label="close">×</a>'+
                                        '<strong>Voucher "'+response.data.code+'" tidak berlaku untuk Pickup</strong>'+
                                    '</div>';
                                } else {
                                    var html = '<div class="alert alert-success  alert-dismissable">'+
                                        '<a href="javascript:void(0)" onclick="resetCoupon()" class="close" aria-label="close">×</a>'+
                                        '<strong>Voucher "'+response.data.code+'" sedang digunakan</strong>'+
                                    '</div>';   
                                }
                            } else {
                                var html = '<div class="alert alert-success  alert-dismissable">'+
                                        '<a href="javascript:void(0)" onclick="resetCoupon()" class="close" aria-label="close">×</a>'+
                                        '<strong>Voucher "'+response.data.code+'" sedang digunakan</strong>'+
                                    '</div>';  
                            }
                            
                            var ttl_shopping = response.data.total_shopping;
                            if(ttl_shopping <= 0){
                                ttl_shopping = 0;
                            };
                            if(response.data.coupon_type == "branch"){
                                $('.sent_from').attr('style','display:block;');
                                $('.sent_merchant').attr('style','display:none;');
                            }
                            $('#showDisc').attr('style','display:block;');
                            $('#coupon_disc').html('-' + formatRupiah(response.data.total_disc, 'Rp '));
                            $('#totalShopping').html(formatRupiah(ttl_shopping, 'Rp '));
                         } else {
                            $.ajax({
                                url: site_url + 'checkout/reset_coupon',
                                type: 'post',
                                data: {'reset' : true},
                                success: function (response) {
                                    $('#showDisc').attr('style','display:none;');
                                    response = JSON.parse(response);
                                    if (response.status == 'success') {
                                        // var html = '<input type="text" class="couponCode" name="couponCode" style="float:left;height:40px;width:60%;" value=""><a href="javascript:void(0)" onclick="submitCoupon()" class="btn btn-default btn-sm">Submit</a>';
                                        var html ='<span class="voucher-summary-title">Kode Voucher</span><input type="text" class="couponCode" name="couponCode" value="" placeholder="Ketik kode voucher">';
                                        if(response.coupon_type == "branch"){
                                            $('.sent_from').attr('style','display:none;');
                                            $('.sent_merchant').attr('style','display:block;');
                                        } 
                                        $('#totalShopping').html(formatRupiah(response.totalShopping, 'Rp '));
                                        $('#coupon-input').html(html);
                                    }
                                }
                            })
                         }
                    } else {
                        if(response.ekspedisi == 1){
                            if(response.pickup == 1){
                                var html = '<div class="alert alert-danger  alert-dismissable">'+
                                    '<a href="javascript:void(0)" onclick="resetCoupon()" class="close" aria-label="close">×</a>'+
                                    '<strong>Voucher "'+response.data.code+'" tidak berlaku untuk Pickup</strong>'+
                                '</div>';
                            } else {
                                var html = '<div class="alert alert-success  alert-dismissable">'+
                                    '<a href="javascript:void(0)" onclick="resetCoupon()" class="close" aria-label="close">×</a>'+
                                    '<strong>Voucher "'+response.data.code+'" sedang digunakan</strong>'+
                                '</div>';   
                            }
                        } else {
                            var html = '<div class="alert alert-success  alert-dismissable">'+
                                    '<a href="javascript:void(0)" onclick="resetCoupon()" class="close" aria-label="close">×</a>'+
                                    '<strong>Voucher "'+response.data.code+'" sedang digunakan</strong>'+
                                '</div>';  
                        }
                        var ttl_shopping = response.data.total_shopping;
                        if(ttl_shopping <= 0){
                            ttl_shopping = 0;
                        };
                        // if(response.data.coupon_type == "branch"){
                        //     $('.sent_from').attr('style','display:block;');
                        //     $('.sent_merchant').attr('style','display:none;');
                        // }
                        $('#showDisc').attr('style','display:block;');
                        $('#coupon_disc').html('-' + formatRupiah(response.data.total_disc, 'Rp '));
                        $('#totalShopping').html(formatRupiah(ttl_shopping, 'Rp '));
                    }
                    $('#coupon-alert').attr('style', 'display:none;');
                } else {
                    // var html = '<input type="text" class="couponCode" name="couponCode" style="float:left;height:40px;width:60%;" value=""><a href="javascript:void(0)" onclick="submitCoupon()" class="btn btn-default btn-sm">Submit</a>';
                    var html ='<span class="voucher-summary-title">Kode Voucher</span><input type="text" class="couponCode" name="couponCode" value="" placeholder="Ketik kode voucher">';
                    $('#coupon-alert').attr('style', 'display:block;');
                    $('#coupon-alert').html(response.message);
                }
                $('#coupon-input').html(html);
        }
    })
}
function formatRupiah(angka, prefix){
    var number_string = angka.toString(),
    split           = number_string.split(','),
    sisa            = split[0].length % 3,
    rupiah          = split[0].substr(0, sisa),
    ribuan          = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if(ribuan){
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp ' + rupiah : '');
}
function resetCoupon() {
    Swal.fire({
        title: 'Apakah ingin menghapus kode kupon?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#d9534f',
        confirmButtonText: 'Iya, hapus',
        reverseButtons: true,
        cancelButtonText: 'Batal'
    }).then(res => {
        if(res.value) {
            $.ajax({
                url: site_url + 'checkout/reset_coupon',
                type: 'post',
                data: {'reset' : true},
                success: function (response) {
                    $('#showDisc').attr('style','display:none;');
                    response = JSON.parse(response);
                    if (response.status == 'success') {
                        var html = '<span class="voucher-summary-title">Kode Voucher</span><input type="text" class="couponCode" name="couponCode" value="" placeholder="Ketik kode voucher">';
                        $('#totalShopping').html(formatRupiah(response.totalShopping, 'Rp '));
                        $('#coupon-input').html(html);
                    }
                }
            })
        }
    })
    // var r = confirm("Hapus kode kupon??");
    // if (r == true) {
    //     $.ajax({
    //         url: site_url + 'checkout/reset_coupon',
    //         type: 'post',
    //         data: {'reset' : true},
    //         success: function (response) {
    //             $('#showDisc').attr('style','display:none;');
    //             response = JSON.parse(response);
    //             if (response.status == 'success') {
    //                 var html = '<input type="text" class="couponCode" name="couponCode" style="float:left;height:40px;width:60%;" value=""><a href="javascript:void(0)" onclick="submitCoupon()" class="btn btn-default btn-sm">Submit</a>';
    //                 $('#totalShopping').html(formatRupiah(response.totalShopping, 'Rp. '));
    //                 $('#coupon-input').html(html);
    //             }
    //         }
    //     })
    // } 
}
// function submitPaymentForm(){
//     var dataForm = $('.form').serializeArray();
//     var payment = null;
//     jQuery.each( dataForm, function( i, field ) {
//         if(field.name == 'payment') {
//             payment = field.value;
//         }
//     });
//     if (payment == 'kredivo') {
//        $('#KredivoModal').modal('show');
//     } else {
//         $('.form').submit();
//     }
// }
function submitPaymentForm(){
    var dataForm = $('.form').serializeArray();
    var payment = null;
    jQuery.each( dataForm, function( i, field ) {
        if(field.name == 'payment') {
            payment = field.value;
        } 
    });
    if(payment != null) {
        if (payment == 'kredivo') {
           $('#KredivoModal').modal('show');
        } else {
            $('.form').submit();
        }
    } else {
        Swal.fire({
          type: 'error',
          title: 'Metode pembayaran harus dipilih!'
        })
    }
}
function agreeKredivo(){
    $('.form').submit();
}
function submit_address() {
    let loader = '<div class="lds-ring" style="margin-top:20px; margin-bottom:20px;"><div></div><div></div><div></div><div></div></div>';
    $.ajax({
        url: site_url + 'checkout/add_address',
        type: 'post',
        data: $('#modal-address input, #modal-address select'),
        beforeSend: function() {
            $('#tags-load').attr('style', 'display:block;');
        },
        complete: function() {
            $('#tags-load').attr('style', 'display:none;');
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.status == 'success') {
                $("#shipping-cost-text").text('Rp 0');
                $('.shipping-cost-class').val(0);
                $('#address-list').html(loader);
                $("#primary_address").html(loader);
                $('#address-list').append(response.html);
                $("#tags-load").css('display', 'none');
                $('#modal-address').modal('hide');
                let id = response.id_address;
                $.ajax({
                    url: site_url + 'checkout/change_address',
                    type: 'post',
                    data: {
                        id: id
                    },
                    success: function(data) {
                        data = JSON.parse(data);
                        $('.info-shop').html(' ');
                        if(data.status == 'success'){
                            $('.info-shop').append(loader);
                            $("#primary_address").html(' ');
                            $('#primary_address').append(data.new_address);
                            $('#address-district').val(data.new_district);
                            document.cookie = "district_add = " + data.new_district;
                            $('.info-shop').append(data.load_info_product);
                            $('.payment-method-block').removeClass('payment-method-block-active');
                            $('.payment-method-block:last-child').addClass('payment-method-block-active');
                        } else {
                            $('#address-list').html(data.html);
                        }
                    }
                });
            } else {
                // $("#tags-load").css('display', 'none');
                $('#tags-load').attr('style', 'display:none;');
                $('#message').html(response.message);
            }
        }
    });
}
function totalShippingCost() {
    var shipping_cost = 0;
    $('input[name^="shipping_cost"]').each(function () {
        var cost = parseFloat($(this).val());
        if (!isNaN(cost)) {
            shipping_cost += cost;
        }
    });
    $('#shipping-cost-text').text(rp(shipping_cost));
    $('#total-text').text(rp(parseFloat($('#total').val()) + shipping_cost));
}
function getCities(province) {
    $('#district option').remove();
    $("#district").append('<option value="">Pilih kecamatan</option>');
    if (province == '' || province == 0) {
        $('#city option').remove();
        $("#city").append('<option value="">Pilih kota</option>');
        $('#select-shipping').children().remove();
        $('#select-shipping').html('<p>Pilih provinsi terlebih dahulu.</p>');
    } else {
        $.get(site_url + 'checkout/getCities/' + province, function (data) {
            $('#city').html(data);
            $('#select-shipping').children().remove();
            $('#select-shipping').html('<p>Pilih kota terlebih dahulu.</p>');
        });
    }
}
function getDistricts(city) {
    if (city == '' || city == 0) {
        $('#district option').remove();
        $("#district").append('<option value="">Pilih kecamatan</option>');
        $('#select-shipping').children().remove();
        $('#select-shipping').html('<p>Pilih kota terlebih dahulu.</p>');
    } else {
        $.get(site_url + 'checkout/getDistricts/' + city, function (data) {
            $('#district').html(data);
            $('#select-shipping').children().remove();
            $('#select-shipping').html('<p>Pilih kecamatan terlebih dahulu.</p>');
        });
    }
}
function getShippings(el) {
//    console.log(el);
    var id = $(el).find(':selected').data('id');
    var merchant = $(el).find(':selected').val();
    var district_from = $(el).find(':selected').data('district-from');
    var district_to = $(el).find(':selected').data('district-to');
    var weight = $(el).find(':selected').data('weight');
    var courier = $(el).find(':selected').data('courier');
//    console.log(merchant);
//    console.log(district);
//    console.log(weight);
//    console.log(courier);
//    $('#shipping-cost').html(rp(0));
//    $('#shipping-cost-promo').html(rp(0));
//    if (district == '' || district == 0) {
//        $('#select-shipping').children().remove();
//        $('#select-shipping').html('<p>Pilih kecamatan terlebih dahulu.</p>');
//    } else {
    $("#shipping-cost-" + id).val('');
    // totalShippingCost();
    let loader = '<div class="lds-ring" style="margin-top:20px; margin-bottom:20px;"><div></div><div></div><div></div><div></div></div>';
    $("#full-ekspedisi-" + id).html(' ');
    $("#full-ekspedisi-mobile-" + id).html(' ');
    $.ajax({
        type: 'post',
        url: site_url + 'checkout/getShippings/',
        data: {
            from: district_from,
            to: district_to,
            weight: weight,
            courier: courier,
            id: id
        },
        success: function (data) {
            data = JSON.parse(data);
            $("#full-ekspedisi-" + id).append(loader);
            $("#full-ekspedisi-mobile-" + id).append(loader);
            $("#full-ekspedisi-" + id).append(data.full_ekspedisi);
            $("#full-ekspedisi-mobile-" + id).append(data.full_ekspedisi_mobile);
        }
    })

}
// function selectPayment(el) {
//     $('.payment-method-block').removeClass('payment-method-block-active');
//     $(el).parents('.payment-method-block').addClass('payment-method-block-active');
// }
function selectPayment(el) {
    $('.payment-method-block').removeClass('payment-method-block-active');
    $(el).parents('.payment-method-block').addClass('payment-method-block-active');
    // if($(el).val() != 'transfer') {
    //     $('.payment_transfer').attr('style', 'height: 0px;opacity: 0;');
    //     $('.payment_transfer .payment_method_transfer input').removeAttr('required');
    //     $('.payment_transfer .payment_method_transfer input').attr('checked', false);
    // }
    if($(el).val() == 'transfer') {
        let name_transfer = $(el).data('nametransfer');
        $('#parent_transfer').val(name_transfer);
    } else {
        $('#parent_transfer').val('');
    }
}
function rp(number) {
    return "Rp " + parseFloat(number).toFixed(0).replace(/./g, function (c, i, a) {
        return i > 0 && c !== "," && (a.length - i) % 3 === 0 ? "." + c : c;
    });
}
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;

    return true;
}
function selectAddress(el, id, total) {
    let loader = '<div class="lds-ring" style="margin-top:20px; margin-bottom:20px;"><div></div><div></div><div></div><div></div></div>';
    // $('#ekspedisi-').html(' ');
    $(".full-ekspedisi").html(' ');
    $('#primary_address').html(loader);
    $("#shipping-cost-text").text('Rp 0');
    $('.full-ekspedisi-mobile').html(' ');
    $('#total-text').text(rp(total));
    $("#otherAddress").collapse('hide');
    $('.shipping-cost-class').val(0);
    $.ajax({
        type: 'post',
        url: site_url + 'checkout/change_address/',
        data: { id: id },
        success: function (data) {
            data = JSON.parse(data);
            $('.info-shop').html(' ');
            if(data.status == 'success'){
                // $('#address-list').append(data.html);
                $('.info-shop').append(loader);
                // $(".full-ekspedisi").append(loader);
                // $('.full-ekspedisi-mobile').append(loader);
                $("#primary_address").html(' ');
                $('#primary_address').append(data.new_address);
                $('#address-district').val(data.new_district);
                document.cookie = "district_add = " + data.new_district;
                $('.info-shop').append(data.load_info_product);
                // $('#ekspedisi-').append(data.reload_ekspedisi);
                // $(".full-ekspedisi").append(data.reload_full_ekspedisi);
                // $('.full-ekspedisi-mobile').append(data.reload_full_ekspedisi_mobile);
                $('.payment-method-block').removeClass('payment-method-block-active');
                $(el).parents('.payment-method-block').addClass('payment-method-block-active');
            } else {
                $('#address-list').html(data.html);
            }
            
        }
    });
}
function ganti_pengiriman(value, id) {
    $("#shipping-cost-" + id).val(value);
    totalShippingCost(value);
}
function check_ongkir(value) {
    value.each(function(index, value) {
        var typebtn = $("#button-lanjutkan").attr('type');
        if(typebtn == 'confirm'){
            $("#button-lanjutkan").attr('type', 'confirm');
        } else {
            if($(this).val() == '') {
                Swal.fire({
                  type: 'error',
                  title: 'Terjadi Kesalahan!',
                  text: 'Silahkan Pilih Pengiriman!',
                  customClass: 'swal-class'
                });
                $("#button-lanjutkan").attr('type', 'button');
            } else { //if($(this).val() == 99){
                Swal.fire({
                  type: 'warning',
                  title: 'Konfirmasi Alamat',
                  text: 'Pastikan Alamat Pengiriman Sudah Benar!',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Ya, sudah benar!'
                }).then((result) => {
                  if (result.value) {
                    $("#button-lanjutkan").attr('type', 'confirm');
                    $("#button-lanjutkan").click();
                  }
                })
            } //else {
            //     $("#button-lanjutkan").attr('type', 'confirm');
            // }
        }
        
    });
}