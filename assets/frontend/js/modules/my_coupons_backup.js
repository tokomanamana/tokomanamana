function copy_code(val, e) {
    $('.btn-copied').text('Salin Kode');
    $('.btn-copied').css('color', '#555555');
    var tempInput = document.createElement("input");
    tempInput.style = "position: absolute; left: -10000px; top: -10000px";
    tempInput.value = val;
    document.body.appendChild(tempInput);
    tempInput.select();
    document.execCommand("copy");
    document.body.removeChild(tempInput);
    $(e).html('<i class="fa fa-check" aria-hidden="true"></i> Tersalin');
    $(e).css('color', '#5CB85C');
}

function copy_code_modal(e) {
    $('.btn-copied').text('Salin Kode');
    $('.btn-copied').css('color', '#555555');
    var copyText = document.querySelector('#code_coupon_modal');
    copyText.select();
    document.execCommand("copy");
    $(e).html('<i class="fa fa-check" aria-hidden="true"></i> Tersalin');
    $(e).css('color', '#5CB85C');
}

function openModal(id) {
    $('#modalCoupon').modal('show');
    $.ajax({
        url: site_url + 'auth/member/get_coupon',
        data: {
            id: id
        },
        type: 'POST',
        success: function(data) {
            res = JSON.parse(data);
            if (res.status == 'success') {
                $('#modalCoupon .btn-copied').text('Salin Kode');
                $('#modalCoupon .btn-copied').css('color', '#555555');
                $('#modalCoupon #coupon_name_modal').html(res.data.name);
                $('#modalCoupon .total-discount').html('Rp ' + formatRupiah(res.data.discount));
                $('#modalCoupon .date-end-content').html(res.data.date_end);
                if (res.data.apply == 0) {
                    $('#modalCoupon #coupon-app-info').html('Voucher bisa digunakan di aplikasi dan web Tokomanamana');
                } else if(res.data.apply == 1) {
                    $('#modalCoupon #coupon-app-info').html('Voucher bisa digunakan di aplikasi Tokomanamana');
                } else if(res.data.apply == 2) {
                    $('#modalCoupon #coupon-app-info').html('Voucher bisa digunakan di web Tokomanamana');
                }
                $('#modalCoupon #list-category').html(res.data.categories);
                $('#modalCoupon #list-city').html(res.data.cities);
                if (res.data.min_order > 0) {
                    $('#modalCoupon .coupon-min-order').html('Rp ' + formatRupiah(res.data.min_order));
                    $('#modalCoupon .coupon-min-order').parent().show();
                } else {
                    $('#modalCoupon .coupon-min-order').parent().hide();
                }

                if (res.data.max_amount > 0) {
                    $('#modalCoupon .coupon-max-amount').html('Rp ' + formatRupiah(res.data.max_amount));
                    $('#modalCoupon .coupon-max-amount').parent().show();
                } else {
                    $('#modalCoupon .coupon-max-amount').parent().hide();
                }

                $('#modalCoupon .coupon-code-modal').html(res.data.code);
                $('#modalCoupon #code_coupon_modal').val(res.data.code);
                $('#modalCoupon #button_copy_code').attr('data-code', res.data.code);
            }
        }
    })
}

const formatRupiah = (number) => {
    const formatNumbering = new Intl.NumberFormat("id-ID");
    return formatNumbering.format(number);
}