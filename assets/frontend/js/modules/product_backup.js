$(document).ready(function () {
    let placeholderText = "";
    let min_buy = $('#min_buy').val();
    
    $(window).on('load', function() {
        $('input[name=qty]').val(min_buy);
    });

    if ($(window).width() <= 480) {
        placeholderText = "Nama kecamatan";
    } else {
        placeholderText = "Ketik 3 huruf awal kecamatan Anda";
    }

    $('.district').select2({
        placeholder: placeholderText,
        ajax: {
            url: site_url + 'catalog/products/get_districts',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });
    $("#product #featuted-image").owlCarousel({
        navigation: true,
        pagination: false,
        autoPlay: false,
        items: 1,
        slideSpeed: 200,
        paginationSpeed: 800,
        rewindSpeed: 1000,
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [979, 1],
        itemsTablet: [768, 1],
        itemsTabletSmall: [540, 1],
        itemsMobile: [360, 1],
        navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
    });
    $(".information_content .panel-title").click(function () {
        if ($(this).find("i").hasClass("fa-angle-up")) {
            $(this).find("i").removeClass("fa-angle-up");
            $(this).find("i").addClass("fa-angle-down");
        } else {
            $(this).find("i").removeClass("fa-angle-down");
            $(this).find("i").addClass("fa-angle-up");
        }
    });
    //    $('#gallery-images div.image').on('click', function () {
    //        var $this = $(this);
    //        var parent = $this.parents('#gallery-images');
    //        parent.find('.image').removeClass('active');
    //        $this.addClass('active');
    //    });
    $('.swatch-element.color').click(function () {
        if (!$(this).hasClass('active')) {
            $('.swatch-element.active').removeClass('active');
            $(this).addClass('active');
        }
    });
    $("#featuted-image a.fancybox").fancybox({
        openEffect: 'none',
        closeEffect: 'none',
        loop: true,
        smallBtn: false,
        toolbar: false
    });
    $('#product-option select, #product-option input').on('change', function () {
        change_option();
    });

    $('input[name=qty]').keyup(function (e) {
        e.preventDefault();
        var qty = parseInt($(this).val());
        let product_qty = parseInt($(this).data('qty'));
        let type = $(this).data('type');
        let merchant = $(this).data('merchant');
        // console.log($.isNumeric(qty));
        if (isNaN(qty) || qty < 1) {
            $('#qty').attr('style', 'border:1px solid #d9534f');
            $('#err_qty').attr('style', 'display:block; color:#d9534f;margin-top:-22px;margin-bottom:25px;font-weight:550;');
            $('#err_qty').text('Jumlah produk tidak boleh kosong!');
            $('.error-mobile').attr('style', 'color: #d9534f;display:block;width:100%');
            $('.error-mobile').text('Jumlah produk tidak boleh kosong!');
            $('#btn_up_qty').removeAttr('disabled');
            $('#btn_down_qty').attr('disabled', true);
            $(this).val(0);
            if(min_buy > 1) {
                $('#min_buy_notification').css('color', '#d9534f');
            }
        } else if (qty > product_qty) {
            $('#qty').attr('style', 'border:1px solid #d9534f');
            $('#err_qty').attr('style', 'display:block; color:#d9534f;margin-top:-22px;margin-bottom:25px;font-weight:550;');
            $('.error-mobile').attr('style', 'color: #d9534f;display:block;width:100%');
            $('.error-mobile').text('Maksimal pembelian ' + product_qty + ' produk!');
            $('#err_qty').text('Maksimal pembelian ' + product_qty + ' produk!');
            $('#btn_up_qty').attr('disabled', true);
        } else {
            $(this).val(qty);
            $('#qty').attr('style', 'border: solid #DDDDDD; border-width: 0 2px;');
            $('#err_qty').attr('style', 'display:none; color:red;');
            $('.error-mobile').attr('style', 'color: #d9534f;display:none;width:100%');
        }

        if(qty <= min_buy) {
            $('#btn_down_qty').attr('disabled', true);
        } 
        if(qty > min_buy) {
            $('#btn_down_qty').removeAttr('disabled');
        }
        if(qty < min_buy) {
            $('#qty').attr('style', 'border:1px solid #d9534f');
            $('#min_buy_notification').css('color', '#d9534f');
        }
        if((qty >= min_buy) && (qty <= product_qty)) {
            $('#qty').attr('style', 'border: solid #DDDDDD; border-width: 0 2px;');
            $('#min_buy_notification').css('color', 'black');
        }

        // if (qty > 1) {
        //     $('#btn_down_qty').removeAttr('disabled');
        // }
        if (qty < product_qty) {
            $('#btn_up_qty').removeAttr('disabled');
        }
        if (qty == product_qty) {
            $('#btn_up_qty').attr('disabled', true);
        }
        // if (qty <= 1) {
        //     $('#btn_down_qty').attr('disabled', true);
        // }
        if(type) {
            $.ajax({
                url: site_url + 'ma/auth/check_user_merchant',
                success: function (data) {
                    data = JSON.parse(data);
                    if(data.status == 'login') {
                        merchant = true;
                    } else {
                        merchant = false;
                    }
                },
                async: false
            });
            let options = $('input[name=product_option').val();
            let last_qty = 0;
            let grochire_qty = $('.grochire-qty');
            $.each(grochire_qty, function(index, value) {
                $(value).removeClass('active');
                if(qty >= $(value).data('qty') && merchant) {
                    last_qty = $(value).data('qty');
                } else if (qty >= $(value).data('qty') && $(value).data('type') != 'reseller') {
                    last_qty = $(value).data('qty');
                }
            });
            $.each(grochire_qty, function(i, val) {
                if(last_qty == $(val).data('qty') && merchant && $(val).data('type') == 'reseller') {
                    $(val).addClass('active');
                    change_qty($(val).data('id'), $(val).data('qty'), $(val).data('type'), $(val).data('product_qty'), true, options, $(val).data('store_id'));
                    let see_others_reseller = $(val).hasClass('other-reseller-item');
                    if(see_others_reseller == true) {
                        let link_other_reseller = $('#see_others_grochire_reseller');
                        $('.other-reseller-item').css('display', 'grid');
                        link_other_reseller.text('Sembunyikan');
                        link_other_reseller.data('type', 'hide');
                    } else {
                        let link_other_reseller = $('#see_others_grochire_reseller');
                        $('.other-reseller-item').css('display', 'none');
                        link_other_reseller.text('Tampilkan Semua');
                        link_other_reseller.data('type', 'show');
                    }
                    $('input[name=type_merchant]').val('true');
                } else if (last_qty == $(val).data('qty') && $(val).data('type') != 'reseller') {
                    $(val).addClass('active');
                    change_qty($(val).data('id'), $(val).data('qty'), $(val).data('type'), $(val).data('product_qty'), true, options, $(val).data('store_id'));
                    let see_others_grochire = $(val).hasClass('see-other-grochire');
                    if(see_others_grochire == true) {
                        let link_other_grochire = $('#see_others_grochire_price_link');
                        $('.see-other-grochire').css('display', 'grid');
                        link_other_grochire.text('Sembunyikan');
                        link_other_grochire.data('type', 'hide');
                    } else {
                        let link_other_grochire = $('#see_others_grochire_price_link');
                        $('.see-other-grochire').css('display', 'none');
                        link_other_grochire.text('Tampilkan Semua');
                        link_other_grochire.data('type', '<show></show>');
                    }
                    $('input[name=type_merchant]').val('false');
                }
            })
        }
    })

    // $('.grochire-qty').on('click', function () {
    //     // if($(this).data('type') == 'grochire') {
    //     //     $('.grochire-qty').removeClass('active');
    //     // }
    //     let data_option = $('input[name=product_option]').val();
    //     if($(this).data('type') == 'grochire') {
    //         $('.grochire-qty').removeClass('active');
    //         change_qty($(this).data('id'), $(this).data('qty'), $(this).data('type'), $(this).data('product_qty'), false, data_option);
    //         $(this).addClass('active');
    //     }
    //     $('input[name=type_merchant]').val('false');
    // });

    // $('.reseller-item').on('click', function() {
    //     let item = $(this);
    //     let data_option = $('input[name=product_option]').val();
    //     $.ajax({
    //         url: site_url + 'catalog/products/check_user_merchant',
    //         data: {},
    //         success: function(data) {
    //             data = JSON.parse(data);
    //             if(data.status == 'not_login') {
    //                 if(item.data('login')) {
    //                     $('#modal_login_merchant').modal('show');
    //                 } else {
    //                     if(item.data('mobile')) {
    //                         window.location.href = site_url + 'member/login';
    //                     } else {
    //                         $('#modal_login').modal('show');
    //                     }
    //                 }
    //                 $('input[name=type_merchant]').val('false');
    //             } else {
    //                 $.ajax({
    //                     url: site_url + 'ma/auth/check_user_merchant',
    //                     data: {
    //                         callback: data.callback
    //                     },
    //                     type: 'POST',
    //                     success: function(data) {
    //                         data = JSON.parse(data);
    //                         if(data.status == 'not_login') {
    //                             if(item.data('login')) {
    //                                 $('#modal_login_merchant').modal('show');
    //                             } else {
    //                                 $('#modal_login').modal('show');
    //                             }
    //                             $('input[name=type_merchant]').val('false');
    //                         } else {
    //                             $('.grochire-qty').removeClass('active');
    //                             change_qty(item.data('id'), item.data('qty'), item.data('type'), item.data('product_qty'), false, data_option);
    //                             item.addClass('active');
    //                             $('input[name=type_merchant]').val('true');
    //                         }
    //                     }
    //                 })
    //             }
    //         }
    //     });
    // });

    $('#btn-cart').click(function () {
        var qty = $('#qty').val();
        var store = $('#store').val();
        var product_qty = $('input[name=qty]').data('qty');
        if (isNaN(qty) || qty < 1) {
            $('#qty').attr('style', 'border:1px solid #d9534f');
            $('#err_qty').attr('style', 'display:block; color:#d9534f;margin-top:-22px;margin-bottom:25px;font-weight:550;');
            $('#err_qty').text('Jumlah produk tidak boleh kosong!');
            $('.error-mobile').attr('style', 'color: #d9534f;display:block;width:100%');
            $('.error-mobile').text('Jumlah produk tidak boleh kosong!');
            return false;
        } else if (qty > product_qty) {
            $('#qty').attr('style', 'border:1px solid #d9534f');
            $('#err_qty').attr('style', 'display:block; color:#d9534f;margin-top:-22px;margin-bottom:25px;font-weight:550;');
            $('#err_qty').text('Maksimal pembelian ' + product_qty + ' produk!');
            $('.error-mobile').attr('style', 'color: #d9534f;display:block;width:100%');
            $('.error-mobile').text('Maksimal pembelian ' + product_qty + ' produk!');
            return false;
        } else if ((qty < min_buy) && min_buy > 1) {
            $('#qty').attr('style', 'border:1px solid #d9534f');
            $('#min_buy_notification').css('color', '#D9534F');
            return false;
        } else {
            $.ajax({
                url: site_url + 'catalog/products/add_to_cart_ajax_new',
                type: 'post',
                data: $('#product-information input[type=text], #product-information input[type=radio]:checked, #product-information select, #product-information input[type=hidden], #product-information input[type=number]'),
                beforeSend: function () {
                    $('#btn-cart').button('loading');
                },
                complete: function () {
                    $('#btn-cart').button('reset');
                },
                success: function (data) {
                    data = JSON.parse(data);
                    //                $('.swatch').removeClass('has-error');
                    if (data.status == 'error') {
                        // $('.product-price').before('<div class="alert alert-danger">' + data.message + '</div>');
                        //                    $.each(data.error.option, function (i, d) {
                        //                        $('#option-' + d).addClass('has-error');
                        //                    });
                        // Swal.fire({
                        //     type: 'error',
                        //     title: data.title,
                        //     message: data.message
                        // });
                        let html = `<div class="alert alert-danger" style="border-radius:5px;"><button type="button" style="margin-top:-4px;" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button> ${data.message}</div>`;
                        $('#notification').attr('style', 'display:block;');
                        $('#notification').html(html);
                    } else {
                        window.location.href = site_url + 'cart';
                    }
                }
            })
        }

    });
    $('#view-list-grochire').popover({
        html: true,
        content: function () {
            return $('.grochire .popover').html();
        }
    });
    $('.btn-cart-not-login').on('click', function (e) {
        e.preventDefault();
        back_data = $(this).data('back');
        // console.log(back_data);
        $('#back-modal').val(back_data);
    });
    $('.btn-add-to-wishlist').on('click', (e) => {
        e.preventDefault();
    });
    $('.heading-information li a').on('click', function(e) {
        e.preventDefault();
        let hash = this.hash;

        $('html, body').animate({
            scrollTop: $(hash).offset().top - 200
        }, 500, 'swing');
    });
    $(this).scroll(function() {
        if($(this).scrollTop() > 592) {
            $('.heading-information').css('position', 'sticky')
            .css('top', 120)
            .css('background', 'white')
            .css('z-index', 2)
            .css('box-shadow', '0px 2px 7px 0px rgba(0,0,0,0.175)')
            .css('border-radius', '5px')
            .css('border', 'none')
            .css('display', 'block');
        } else {
            $('.heading-information').css('border-top', '1px solid #CCC')
            .css('border-bottom', '1px solid #CCC')
            .css('padding', '15px 0')
            .css('position', 'static')
            .css('border-radius', 'none')
            .css('z-index', '0')
            .css('box-shadow', 'none');
        }

        let documentTop = $(window).scrollTop();

        let description_information = $('#description_information');
        let compare_information = $('#compare_information');
        let review_information = $('#review_information');
        let spesification_information = $('#spesification_information');
        let other_products = $('#other_products');

        if(description_information.offset()) {
            if(documentTop >= description_information.offset().top - 201) {
                $('.heading-information ul li a').removeClass('active');
                $('.heading-information ul li a[href=#description_information]').addClass('active');
            } else {
                $('.heading-information ul li a').removeClass('active');
                $('.heading-information ul li a[href=#description_information]').addClass('active');
            }
        }

        if(spesification_information.offset()) {
            if(documentTop >= spesification_information.offset().top - 201) {
                $('.heading-information ul li a').removeClass('active');
                $('.heading-information ul li a[href=#spesification_information]').addClass('active');
            } 
        }
        if(review_information.offset()) {
            if(documentTop >= review_information.offset().top - 201) {
                $('.heading-information ul li a').removeClass('active');
                $('.heading-information ul li a[href=#review_information]').addClass('active');
            } 
        }
        if(compare_information.offset()) {
            if(documentTop >= compare_information.offset().top - 201) {
                $('.heading-information ul li a').removeClass('active');
                $('.heading-information ul li a[href=#compare_information]').addClass('active');
            }
        }
        if(other_products.offset()) {
            if(documentTop >= other_products.offset().top - 201) {
                $('.heading-information ul li a').removeClass('active');
                $('.heading-information ul li a[href=#other_products]').addClass('active');
            }
        } 
    });
    // $('#see_others_grochire_price_link').on('click', function(e) {
    //     let type = $(this).data('type');
    //     if(type == 'show') {
    //         $('.see-other-grochire').css('display', 'grid');
    //         $(this).text('Sembunyikan');
    //         $(this).data('type', 'hide');
    //     } else {
    //         $('.see-other-grochire').css('display', 'none');
    //         $(this).text('Tampilkan Semua');
    //         $(this).data('type', 'show');
    //     }
    // });
    // $('#see_others_grochire_reseller').on('click', function(e) {
    //     let type = $(this).data('type');
    //     if(type == 'show') {
    //         $('.other-reseller-item').css('display', 'grid');
    //         $(this).text('Sembunyikan');
    //         $(this).data('type', 'hide');
    //     } else {
    //         $('.other-reseller-item').css('display', 'none');
    //         $(this).text('Tampilkan Semua');
    //         $(this).data('type', 'show');
    //     }
    // });
});

function see_others_grochire(e) {
    let type = $(e).data('type');
    if(type == 'show') {
        $('.see-other-grochire').css('display', 'grid');
        $(e).text('Sembunyikan');
        $(e).data('type', 'hide');
    } else {
        $('.see-other-grochire').css('display', 'none');
        $(e).text('Tampilkan Semua');
        $(e).data('type', 'show');
    }
}

function see_others_reseller(e) {
    let type = $(e).data('type');
    if(type == 'show') {
        $('.other-reseller-item').css('display', 'grid');
        $(e).text('Sembunyikan');
        $(e).data('type', 'hide');
    } else {
        $('.other-reseller-item').css('display', 'none');
        $(e).text('Tampilkan Semua');
        $(e).data('type', 'show');
    }
}

function change_grochire(e) {
    let data_option = $('input[name=product_option]').val();
    if($(e).data('type') == 'grochire') {
        $('.grochire-qty').removeClass('active');
        change_qty($(e).data('id'), $(e).data('qty'), $(e).data('type'), $(e).data('product_qty'), false, data_option, $(e).data('store_id'));
        $(e).addClass('active');
    }
    $('input[name=type_merchant]').val('false');
}

function change_reseller(e) {
    const item = $(e);
    let data_option = $('input[name=product_option]').val();
    $.ajax({
        url: site_url + 'catalog/products/check_user_merchant',
        data: {},
        success: function(data) {
            data = JSON.parse(data);
            if(data.status == 'not_login') {
                if(item.data('login')) {
                    $('#modal_login_merchant').modal('show');
                } else {
                    if(item.data('mobile')) {
                        window.location.href = site_url + 'member/login';
                    } else {
                        $('#modal_login').modal('show');
                    }
                }
                $('input[name=type_merchant]').val('false');
            } else {
                $.ajax({
                    url: site_url + 'ma/auth/check_user_merchant',
                    data: {
                        callback: data.callback
                    },
                    type: 'POST',
                    success: function(data) {
                        data = JSON.parse(data);
                        if(data.status == 'not_login') {
                            if(item.data('login')) {
                                $('#modal_login_merchant').modal('show');
                            } else {
                                $('#modal_login').modal('show');
                            }
                            $('input[name=type_merchant]').val('false');
                        } else {
                            $('.grochire-qty').removeClass('active');
                            change_qty(item.data('id'), item.data('qty'), item.data('type'), item.data('product_qty'), false, data_option, item.data('store_id'));
                            item.addClass('active');
                            $('input[name=type_merchant]').val('true');
                        }
                    }
                })
            }
        }
    });
}

function get_districts(city, district) {
    if (city == '' || city == 0) {
        $('#district option').remove();
        $("#district").append('<option value="">Pilih kecamatan</option>');
    } else {
        $.get(site_url + 'catalog/products/get_districts/' + city + (district ? '/' + district : ''), function (data) {
            $('#district').html(data);
        });
    }
}

function districts() {
    var data = $('.district').select2('data')
    var id = data[0].id;
    var qty = $('#qty').val();
    var weight = $('#berat').val();
    var tr = "";
    var cost = [];
    $('#table_cek_ongkir tbody').empty();

    $.ajax({
        method: 'post',
        url: site_url + 'catalog/products/cek_estimasi_harga',
        data: {
            id: id,
            qty: qty,
            weight: weight
        },
        success: function (data) {
            data = JSON.parse(data);
            var i;
            for (i in data[3]) {
                //console.log(data[3][i]);
                if (data[3][i] == 'pickup') {
                    tr += '<tr>';
                    tr += '<td style="font-size:12px;">Ambil Sendiri</td>';
                    tr += '<td colspan="2" style="font-size:12px;color:red">* Tidak support kredivo</td>';
                    tr += '</tr>';
                }
            }
            tr += '<tr>';
            tr += '<td style="font-size:12px;">Reguler</td>';
            tr += '<td style="font-size:12px;">' + data[0][1].cost[0].etd + ' hari' + '</td>';
            tr += '<td style="font-size:12px;"><b>' + data[1] + ' - ' + data[2] + '</b></td>';
            tr += '</tr>';
            /*tr += '<tr>';
            tr += '<td>OKE</td>';
            tr =+ '</tr>';*/
            $('#table_cek_ongkir tbody').append(tr);
        }
    });

}
$(window).load(function () {
    // The slider being synced must be initialized first
    $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 100,
        itemMargin: 5,
        asNavFor: '#slider'
    });

    $('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
    });
});

function formatRupiah(angka, prefix) {
    angka = angka.toString();
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split = number_string.split(','),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp ' + rupiah : '');
}

/*
function addtoCart() {
    $.ajax({
        url: site_url + 'catalog/products/add_to_cart_ajax_new',
        type: 'post',
        data: $('#product-information input[type=text], #product-information input[type=radio]:checked, #product-information select, #product-information input[type=hidden]'),
        beforeSend: function () {
            $("#tags-load").css('display', 'block');
        },
        complete: function () {
            $("#tags-load").css('display', 'none');
        },
        success: function (data) {
            data = JSON.parse(data);
            if (data.status == 'error') {
            // $('.product-price').before('<div class="alert alert-danger">' + data.message + '</div>');
            Swal.fire({
              type: 'error',
              title: 'Terjadi Kesalahan',
              text: data.message
            });
          } else {
            var length = Object.keys( data.items ).length;
            var total = 0;
            var total_qty = 0;
            var html = '<div class="items control-container" style="max-height: 150px; overflow: auto">';
            if(length > 0) {
              $('#cart-target .cart-info').attr('style','display:none');
              var redir_url = "'" + site_url + "cart" + "'";
              $.each(data.items , function(index, val) {
                // console.log(val);
                total += (Number(val.price * val.qty));
                total_qty += Number(val.qty);
                html += '<div class="row" style="padding: 10px 10px;"><div class="cart-left">';
                html += '<a class="cart-image" href="catalog/products/view/'+val.product_id+'/'+val.merchant_id+'">';
                html += '<img src="'+site_url+'files/images/'+val.image+'" alt="" title=""></a></div>';
                html += '<div class="cart-right">';
                html += '<div class=""><a href="catalog/products/view/' + val.product_id + '/'+ val.merchant_id +'" style="color:#000;">'+val.name+'</a></div>';
                html += '<div class="cart-price"><span class="money">'+formatRupiah(val.price,'.')+'</span><span class="x"> x '+val.qty+'</span></div>';
                html += '</div></div>';
              });
              html += '</div> <div class="subtotal" style="border-top: 1px solid #EBEBEB"><span>Subtotal:</span><span class="cart-total-right money">'+formatRupiah(total,'.')+'</span></div>';
              html += '<div class="action"><a href="cart" class="btn" style="font-size:10px; width: 100%; onclick="window.location = '+redir_url+';">Lihat Semua</a></div>';
            } else {
              html += '<div class="items control-container"><center><p style="font-size: 13px; font-weight: bold; padding: 15px;">Keranjang belanja Anda kosong. Yuk, belanja sekarang!</p></center></div>';
            }
            html += '</div>';
            $('#cart-target .badge').html(total_qty);
            $('#cart-button .badge').html(total_qty);
            $('.icon-cart .cart_text .number').html(total_qty);
            $('#cart-target-mobile .number').html(total_qty);
            $('#cart-target .cart-info .cart-content').html(html);
            $('#cart-target-mobile .cart-info .cart-content').html(html);
            Swal.fire({
              type: 'success',
              title: 'Produk Berhasil Ditambah!',
              timer: 1000,
              showConfirmButton: false,
              customClass: 'swal-class'
            });
          }
        }
    })
}
*/

// NEW METHOD
function addtoCarts() {
    let product_qty = $('input[name=qty]').data('qty');
    let qty = $('input[name=qty]').val();
    let min_buy = $('#min_buy').val();
    if (isNaN(qty) || qty < 1) {
        $('#qty').attr('style', 'border:1px solid #d9534f');
        $('#err_qty').attr('style', 'display:block; color:#d9534f;margin-top:-22px;margin-bottom:25px;font-weight:550;');
        $('#err_qty').text('Jumlah produk tidak boleh kosong!');
        $('.error-mobile').attr('style', 'color: #d9534f;display:block;width:100%');
        $('.error-mobile').text('Jumlah produk tidak boleh kosong!');
        return false;
    } else if (qty > product_qty) {
        $('#qty').attr('style', 'border:1px solid #d9534f');
        $('#err_qty').attr('style', 'display:block; color:#d9534f;margin-top:-22px;margin-bottom:25px;font-weight:550;');
        $('#err_qty').text('Maksimal pembelian ' + product_qty + ' produk!');
        $('.error-mobile').attr('style', 'color: #d9534f;display:block;width:100%');
        $('.error-mobile').text('Maksimal pembelian ' + product_qty + ' produk!');
        return false;
    } else if ((qty < min_buy) && min_buy > 1) {
        $('#qty').attr('style', 'border:1px solid #d9534f');
        $('#min_buy_notification').css('color', '#D9534F');
        return false;
    } else {
        $.ajax({
            url: site_url + 'catalog/products/add_to_cart_ajax_new',
            type: 'post',
            data: $('#product-information input[type=text], #product-information input[type=radio]:checked, #product-information select, #product-information input[type=hidden], #product-information input[type=number]'),
            beforeSend: function () {
                $("#tags-load").css('display', 'block');
            },
            complete: function () {
                $("#tags-load").css('display', 'none');
            },
            success: function (data) {
                data = JSON.parse(data);
                if (data.status == 'error') {
                    // $('.product-price').before('<div class="alert alert-danger">' + data.message + '</div>');
                    // Swal.fire({
                    //     type: 'error',
                    //     title: data.title,
                    //     text: data.message
                    // });
                    let html = `<div class="alert alert-danger" style="border-radius:5px;"><button type="button" style="margin-top:-4px;" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button> ${data.message}</div>`;
                    $('#notification').attr('style', 'display:block;');
                    $('#notification').html(html);
                } else {
                    var length = Object.keys(data.items).length;
                    var total = 0;
                    var total_qty = 0;
                    var html = '<div class="items control-container" style="max-height: 150px; overflow: auto">';
                    if (length > 0) {
                        var html = '<div style="max-height: 200px; overflow: auto">';
                        html += '<div class="carts-items">';
                        $.each(data.items, function (index, val) {
                            if(val.option) {
                                let data_cart_option = JSON.parse(val.option);
                                $.each(data_cart_option, function(index, value) {
                                    total += (Number(value.price * value.quantity));
                                    total_qty += Number(value.quantity);
                                    html += '<div class="row" id="cart-header-option-' + val.id_encrypt + '" style="padding: 10px 10px;border-bottom:1px solid #EEE;">';
                                    html += '<div class="cart-left col-md-4" style="display: inline-block;">';
                                    html += '<a class="cart-image" href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '">';
                                    html += '<img style="width: 50px;" src="' + site_url + 'files/images/' + value.image + '" alt="" title="">';
                                    html += '</a>';
                                    html += '</div>';
                                    html += '<div class="cart-right col-md-8" style="display: inline-block;margin-left:-25px;">';
                                    html += '<div class="">';
                                    html += '<a href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '" style="color:#000;">';
                                    html += val.name;
                                    html += '</a>';
                                    html += '</div>';
                                    html += '<div class="cart-price">';
                                    html += '<span class="money" style="color: #97C23C;">' + formatRupiah(value.price, '.') + '</span>';
                                    html += '<span class="x" style="color: #97C23C;">x ' + value.quantity + '</span>'
                                    html += '</div>';
                                    html += '</div>';
                                    html += '</div>';
                                });
                            } else {
                                total += (Number(val.price * val.quantity));
                                total_qty += Number(val.quantity);
                                html += '<div class="row" id="cart-header-' + val.id_encrypt + '" style="padding: 10px 10px;border-bottom:1px solid #EEE;">';
                                html += '<div class="cart-left col-md-4" style="display: inline-block;">';
                                html += '<a class="cart-image" href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '">';
                                html += '<img style="width: 50px;" src="' + site_url + 'files/images/' + val.image + '" alt="" title="">';
                                html += '</a>';
                                html += '</div>';
                                html += '<div class="cart-right col-md-8" style="display: inline-block;margin-left:-25px;">';
                                html += '<div class="">';
                                html += '<a href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '" style="color:#000;">';
                                html += val.name;
                                html += '</a>';
                                html += '</div>';
                                html += '<div class="cart-price">';
                                html += '<span class="money" style="color: #97C23C;">' + formatRupiah(val.price, '.') + '</span>';
                                html += '<span class="x" style="color: #97C23C;">x ' + val.quantity + '</span>'
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                            }
                        });
                        html += '</div>';
                        html += '</div>';
                        html += '<div class="subtotal" style="color: black;padding: 15px 20px;font-size:14px;border-top:1px solid #EEE;">';
                        html += '<span>Subtotal:</span><span class="cart-total-right money" style="float: right;">' + formatRupiah(total, '.') + '</span>'
                        html += '</div>';
                        html += '<div class="action" style="padding: 15px 20px;margin-top:-10px;">';
                        html += '<a href="' + site_url + 'cart" class="btn btn-show-all" style="font-size:10px; width: 100%;border-radius:3px;">Lihat Semua</a>'
                        html += '</div>';
                    } else {
                        var html = '<p style="font-size: 13px; font-weight: bold; padding: 15px;text-align:center;">Keranjang belanja Anda kosong. Yuk, belanja sekarang!</p>'
                    }

                    $('#cart-target .badge').html(total_qty);
                    $('#cart-button .badge').html(total_qty);
                    $('.icon-cart .cart_text .number').html(total_qty);
                    $('#cart-target-mobile .number').html(total_qty);
                    $('#cart-target .cart-info .cart-content').html(html);
                    $('#cart-target-mobile .cart-info .cart-content').html(html);

                    // new
                    $('.cart_icon #icon_cart .badge').html(total_qty);
                    $('.cart_icon .cart-dropdown').html(html);
                    Swal.fire({
                        type: 'success',
                        title: 'Produk Berhasil Ditambah!',
                        timer: 1000,
                        showConfirmButton: false,
                        customClass: 'swal-class'
                    });
                }
            }
        })
    }
}

// addToCart product relation
function addtoCart(id, store_id) {
    $.ajax({
        url: site_url + 'catalog/products/add_to_cart_ajax_new',
        type: 'post',
        data: {
            product: id,
            store: store_id,
            qty: 1
        },
        beforeSend: function() {
            $("#tags-load").css('display', 'block');
        },
        complete: function() {
            $("#tags-load").css('display', 'none');
        },
        success: function(data) {
            data = JSON.parse(data);
            if (data.status == 'error') {
                Swal.fire({
                    type: 'error',
                    title: 'Terjadi Kesalahan',
                    text: data.message
                });
            } else {
                var length = Object.keys(data.items).length;
                var total = 0;
                var total_qty = 0;
                if(length > 0) {
                    var html = '<div style="max-height: 200px; overflow: auto">';
                    html += '<div class="carts-items">';
                    $.each(data.items, function(index, val) {
                        if(val.option) {
                            let cart_item_option = JSON.parse(val.option);
                            $.each(cart_item_option, function(index, value) {
                                total += (Number(value.price * value.quantity));
                                total_qty += Number(value.quantity);
                                html += '<div class="row" id="cart-header-option-' + val.id_encrypt + '" style="padding: 10px 10px;border-bottom:1px solid #EEE;">';
                                html += '<div class="cart-left col-md-4" style="display: inline-block;">';
                                html += '<a class="cart-image" href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '">';
                                html += '<img style="width: 50px;" src="' + site_url + 'files/images/' + value.image + '" alt="" title="">';
                                html += '</a>';
                                html += '</div>';
                                html += '<div class="cart-right col-md-8" style="display: inline-block;margin-left:-25px;">';
                                html += '<div class="">';
                                html += '<a href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '" style="color:#000;">';
                                html += val.name;
                                html += '</a>';
                                html += '</div>';
                                html += '<div class="cart-price">';
                                html += '<span class="money" style="color: #97C23C;">' + formatRupiah(value.price, '.') + '</span>';
                                html += '<span class="x" style="color: #97C23C;">x ' + value.quantity + '</span>'
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                            })
                        } else {
                            total += (Number(val.price * val.quantity));
                            total_qty += Number(val.quantity);
                            html += '<div class="row" id="cart-header-' + val.id_encrypt + '" style="padding: 10px 10px;border-bottom:1px solid #EEE;">';
                            html += '<div class="cart-left col-md-4" style="display: inline-block;">';
                            html += '<a class="cart-image" href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '">';
                            html += '<img style="width: 50px;" src="' + site_url + 'files/images/' + val.image + '" alt="" title="">';
                            html += '</a>';
                            html += '</div>';
                            html += '<div class="cart-right col-md-8" style="display: inline-block;margin-left:-25px;">';
                            html += '<div class="">';
                            html += '<a href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '" style="color:#000;">';
                            html += val.name;
                            html += '</a>';
                            html += '</div>';
                            html += '<div class="cart-price">';
                            html += '<span class="money" style="color: #97C23C;">' + formatRupiah(val.price, '.') + '</span>';
                            html += '<span class="x" style="color: #97C23C;">x ' + val.quantity + '</span>'
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                        }
                    });
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="subtotal" style="color: black;padding: 15px 20px;font-size:14px;border-top:1px solid #EEE;">';
                    html += '<span>Subtotal:</span><span class="cart-total-right money" style="float: right;">' + formatRupiah(total, '.') + '</span>'
                    html += '</div>';
                    html += '<div class="action" style="padding: 15px 20px;margin-top:-10px;">';
                    html += '<a href="' + site_url + 'cart" class="btn btn-show-all" style="font-size:10px; width: 100%;border-radius:3px;">Lihat Semua</a>'
                    html += '</div>';
                } else {
                    var html = '<p style="font-size: 13px; font-weight: bold; padding: 15px;text-align:center;">Keranjang belanja Anda kosong. Yuk, belanja sekarang!</p>'
                }

                $('#cart-target .badge').html(total_qty);
                $('#cart-button .badge').html(total_qty);
                $('.icon-cart .cart_text .number').html(total_qty);
                $('#cart-target-mobile .number').html(total_qty);
                $('#cart-target .cart-info .cart-content').html(html);
                $('#cart-target-mobile .cart-info .cart-content').html(html);

                // new
                $('.cart_icon #icon_cart .badge').html(total_qty);
                $('.cart_icon .cart-dropdown').html(html);
                Swal.fire({
                    type: 'success',
                    title: 'Produk Berhasil Ditambah!',
                    timer: 1000,
                    showConfirmButton: false,
                    customClass: 'swal-class'
                });
            }
        }
    })
}

function change_option() {
    $('#notification').attr('style', 'display:none;');
    let min_buy = $('#min_buy').val();
    $.ajax({
        url: site_url + 'catalog/products/change_option',
        type: 'post',
        data: $('#product-option input:checked, #product-option select, input[name=product], input[name=store]'),
        beforeSend: function () {
            $("#tags-load").css('display', 'block');
        },
        complete: function () {
            $("#tags-load").css('display', 'none');
        },
        success: function (res) {
            res = JSON.parse(res);
            // if(res.status == 'FALSE') {
            //  let html = `<div class="alert alert-danger" style="border-radius:5px;"><button type="button" style="margin-top:-4px;" class="close" data-dismiss="alert" aria-label="Close">
            //             <span aria-hidden="true">&times;</span>
            //           </button> Variasi tersebut tidak tersedia! Silahkan pilih variasi lain!</div>`;
            //     $('#notification').attr('style', 'display:block;');
            //     $('#notification').html(html);
            // } 
            $('.slides-big').html(res.images);
            $('.slides-small').html(res.images_small);
            // $('#product-option select, #product-option input').on('change', function() {
            //     change_option();
            // });
            $('.error-mobile').attr('style', 'color: #d9534f;display:none;width:100%');
            $('#slider').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                sync: "#carousel"
            });
            $('#mobile-slider').flexslider({
                animation: "slide",
                controlNav: false,
                directionNav: false,
                animationLoop: false,
                slideshow: false,
            });
            $('#carousel').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                itemWidth: 100,
                itemMargin: 5,
                asNavFor: '#slider'
            });
            $('#product-option').html(res.options);
            // $('#product-option-container').load(window.location.href + ' #product-option');
            // console.log(res.old_price);
            $('.disc-product-price .money').html(formatRupiah(parseInt(res.old_price), '.'));
            $('.product-price .price').html(formatRupiah(parseInt(res.price), '.'));
            if (res.in_promo == 1) {
                $(".product-options .col-md-12 .money").html(formatRupiah(parseInt(res.old_price), '.'));
            }
            $('.grochire-qty').removeClass('active');
            let grochire_qty = $('.grochire-detail').find('.grochire-qty[data-qty=1]');
            grochire_qty.addClass('active');
            if(min_buy > 1) {
                $('input[name=qty]').val(min_buy);
            } else {
                $('input[name=qty]').val(1);
            }
            $('#btn_down_qty').attr('disabled', true);

            $('#product-option select, #product-option input').on('change', function () {
                change_option();
            });
            $('input[name=qty]').data('qty', Number(res.product_option.quantity));
            if(res.product_option.quantity == 1) {
                $('#btn_up_qty').attr('disabled', true);
            } else {
                $('#btn_up_qty').removeAttr('disabled');
            }
            if(res.product_option.quantity < 10) {
                $('.purchase-section-desktop').attr('style', 'float: left;display: inline-block;margin-top:-20px;');
                $('.purchase-section-mobile').attr('style', 'float: left;display: inline-block;margin-top:-20px;');
            } else {
                $('.purchase-section-desktop').attr('style', 'float: left;display: inline-block;');
                $('.purchase-section-mobile').attr('style', 'float: left;display: inline-block;');
            }
            if(res.product_option.quantity > 0) {
                let html = '';
                let html_mobile = '';
                if(res.product_option.quantity == 1) {
                    html += `<span style="margin-top:20px;display:inline-block;" class="col-sm-12">
                                <b>Stok hampir habis!</b> Tersisa ${res.product_option.quantity}
                            </span>`;
                    html_mobile += `<span style="margin-top:20px;display:inline-block;margin-bottom:20px;" class="col-sm-12">
                        <b>Stok hampir habis!</b> Tersisa ${res.product_option.quantity}
                    </span>`;
                } else if(res.product_option.quantity < 5) {
                    html += `<span style="margin-top:20px;display:inline-block;" class="col-sm-12">
                                <b>Stok hampir habis!</b> Tersisa < 5
                            </span> `;
                    html_mobile += `<span style="margin-top:20px;display:inline-block;margin-bottom:20px;" class="col-sm-12">
                                <b>Stok hampir habis!</b> Tersisa < 5
                            </span> `;
                } else if(res.product_option.quantity < 10) {
                    html += `<span style="margin-top:20px;display:inline-block;" class="col-sm-12">
                                <b>Stok hampir habis!</b> Tersisa < 10
                            </span> `;
                    html_mobile += `<span style="margin-top:20px;display:inline-block;margin-bottom:20px;" class="col-sm-12">
                                <b>Stok hampir habis!</b> Tersisa < 10
                            </span> `;       
                }
                $('#quantity_notification').html(html);
                $('#quantity_notification_mobile').html(html_mobile);
            } else {
                $('#quantity_notification').html('');
            }
            if(res.product_option.quantity == 0) {
                let html = `<h3 style="color: #d9534f;font-size:20px;margin-top:30px;">Stok habis</h3>`;
                $('#purchase-section-desktop-wrapper').css('display', 'none');
                $('.purchase-section-desktop h3').css('display', 'none');
                $('.purchase-section-desktop').append(html);
            } else {
                $('.purchase-section-desktop h3').css('display', 'none');
                $('#purchase-section-desktop-wrapper').css('display', 'block');
            }
            if(res.price_level_option.length > 0) {
                const price_level_option = res.price_level_option;
                $('.grochire-div').css('display', 'block');
                // price_level_option.map((value) => {
                //     let find_grochire_qty = $('.grochire-detail .grochire-qty[data-qty=' + value.min_qty + ']')
                //     find_grochire_qty.find('span:nth-child(even)').html(formatRupiah(value.price, '.'));
                // });
                let num_row_price_level = 0 ;
                if(price_level_option[price_level_option.length - 1].num_rows > 2) {
                    num_row_price_level = 2;
                } else {
                    num_row_price_level = price_level_option[price_level_option.length - 1].num_rows;
                }
                const count_row = Math.floor(12 / (num_row_price_level + 1));
                let html_grochire = '<span style="font-size: 14px;margin-top: 10px;display: block;margin-bottom: 2px;font-weight: 500;">Harga Grosir</span>';
                let check_dropdown = 0;
                html_grochire += `<div class="col-md-${count_row} col-sm-${count_row} col-xs-${count_row} grochire-qty active" data-id="${res.product_id}" data-qty="${Number(1)}" data-type="grochire" data-store_id="${res.store_id}" onclick="change_grochire(this)">
                                    <span>1</span>
                                    <span>${formatRupiah(res.price, '.')}</span>
                                </div>`;
                for(let i = 0; i < price_level_option.length; i++) {
                    if(Number(price_level_option[i].min_qty) <= Number(res.product_option.quantity)) {
                        check_dropdown++;
                        if(i <= 1) {
                            html_grochire += `<div class="col-md-${count_row} col-sm-${count_row} col-xs-${count_row} grochire-qty" data-id="${res.product_id}" data-qty="${price_level_option[i].min_qty}" data-type="grochire" data-product_qty="${res.product_option.quantity}" data-store_id="${res.store_id}" onclick="change_grochire(this)">
                                                <span>${i + 1 == price_level_option.length ? `>= ${price_level_option[i].min_qty}` : `${price_level_option[i].min_qty} - ${Number(price_level_option[i + 1].min_qty) - 1}`}</span>
                                                <span>${formatRupiah(price_level_option[i].price, '.')}</span>
                                            </div>`;
                        } else {
                            html_grochire += `<div class="col-md-${count_row} col-sm-${count_row} col-xs-${count_row} grochire-qty see-other-grochire" data-id="${res.product_id}" data-qty="${price_level_option[i].min_qty}" data-type="grochire" data-product_qty="${res.product_option.quantity}" data-store_id="${res.store_id}" onclick="change_grochire(this)" style="display: none;">
                                                <span>${i + 1 == price_level_option.length ? `>= ${price_level_option[i].min_qty}` : `${price_level_option[i].min_qty} - ${Number(price_level_option[i + 1].min_qty) - 1}`}</span>
                                                <span>${formatRupiah(price_level_option[i].price, '.')}</span>
                                            </div>`;
                        }
                    }
                }
                if(check_dropdown > 2) {
                    html_grochire += `<div class="col-md-12 col-sm-12" style="text-align: center;margin-top: 2px;">
                                        <a href="javascript:void(0)" id="see_others_grochire_price_link" data-type="show" onclick="see_others_grochire(this)">Tampilkan Semua</a>
                                    </div>`;
                }
                $('.grochire-div').html(html_grochire);
            } else {
                $('.grochire-div').css('display', 'none');
            }
            if(res.price_reseller_option.length > 0) {
                $('.reseller-div').css('display', 'block');
                const price_reseller_option = res.price_reseller_option;
                let num_row_reseller_price = 0 ;
                if(price_reseller_option[price_reseller_option.length - 1].num_rows > 2) {
                    num_row_reseller_price = 2;
                } else {
                    num_row_reseller_price = price_reseller_option[price_reseller_option.length - 1].num_rows;
                }
                const count_row_reseller = Math.floor(12 / (num_row_reseller_price));
                let check_dropdown_reseller = 0;
                let html_reseller = '<span style="font-size: 14px;margin-top: 10px;display: block;margin-bottom: 2px;font-weight: 500;">Harga Reseller</span>';
                for(let i = 0; i < price_reseller_option.length; i++) {
                    if(Number(price_reseller_option[i].min_qty) <= Number(res.product_option.quantity)) {
                        check_dropdown_reseller++;
                        if(i <= 1) {
                            html_reseller += `<div class="col-md-${count_row_reseller} col-sm-${count_row_reseller} col-xs-${count_row_reseller} grochire-qty reseller-item" data-id="${res.product_id}" data-qty="${price_reseller_option[i].min_qty}" data-type="reseller" data-product_qty="${res.product_option.quantity}" data-login="${price_reseller_option[i].login}" data-mobile="false" data-store_id="${res.store_id}" onclick="change_reseller(this)">
                                            <span>${i + 1 == price_reseller_option.length ? `>= ${price_reseller_option[i].min_qty}` : `${price_reseller_option[i].min_qty} - ${Number(price_reseller_option[i + 1].min_qty) - 1}`}</span>
                                            <span>${formatRupiah(price_reseller_option[i].price, '.')}</span>
                                        </div>`;
                        } else {
                            html_reseller += `<div class="col-md-${count_row_reseller} col-sm-${count_row_reseller} col-xs-${count_row_reseller} grochire-qty reseller-item other-reseller-item" data-id="${res.product_id}" data-qty="${price_reseller_option[i].min_qty}" data-type="reseller" data-product_qty="${res.product_option.quantity}" data-login="${price_reseller_option[i].login}" data-mobile="false" data-store_id="${res.store_id}" onclick="change_reseller(this)" style="display: none;">
                                            <span>${i + 1 == price_reseller_option.length ? `>= ${price_reseller_option[i].min_qty}` : `${price_reseller_option[i].min_qty} - ${Number(price_reseller_option[i + 1].min_qty) - 1}`}</span>
                                            <span>${formatRupiah(price_reseller_option[i].price, '.')}</span>
                                        </div>`;
                        }
                    }
                }
                if(check_dropdown_reseller > 2) {
                    html_reseller += `<div class="col-md-12 col-sm-12" style="text-align: center;margin-top: 2px;">
                                <a href="javascript:void(0)" id="see_others_grochire_reseller" data-type="show" onclick="see_others_reseller(this)">Tampilkan Semua</a>
                            </div>`;
                }
                $('.reseller-div').html(html_reseller);
            } else {
                $('.reseller-div').css('display', 'none');
            }
            $('#err_qty').css('display', 'none');
            $('input[name=qty]').css('border', 'solid #DDDDDD');
            $('input[name=qty]').css('border-width', '0 2px');
            let all_grochire_qty = $('.grochire-qty');
            $('input[name=product_option]').val(res.product_option.id);
            grochire_qty.find('span:nth-child(even)').html(formatRupiah(res.price, '.'));
            // console.log(res.images_carousel);
            // $('#product #featuted-image').owlCarousel().trigger('remove.owl.carousel');
            // $('#product .show-image-load').after('<div id="featuted-image" class="image featured">' + res.images + '</div>');
            // $("#product #featuted-image").owlCarousel({
            //     navigation: true,
            //     pagination: false,
            //     autoPlay: false,
            //     items: 1,
            //     slideSpeed: 200,
            //     paginationSpeed: 800,
            //     rewindSpeed: 1000,
            //     itemsDesktop: [1199, 1],
            //     itemsDesktopSmall: [979, 1],
            //     itemsTablet: [768, 1],
            //     itemsTabletSmall: [540, 1],
            //     itemsMobile: [360, 1],
            //     navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
            // });
            // $("#featuted-image a.fancybox").fancybox({
            //     openEffect: 'none',
            //     closeEffect: 'none',
            //     loop: true,
            //     smallBtn: false,
            //     toolbar: false
            // });
            
        }
    });
}

function addWishlist(id, store, uri) {
    $.ajax({
        method: 'post',
        url: site_url + 'catalog/products/add_wishlist_ajax',
        data: {
            id: id,
            store: store
        },
        beforeSend: function () {
            $("#tags-load").css('display', 'block');
        },
        complete: function () {
            $("#tags-load").css('display', 'none');
        },
        success: function (data) {
            data = JSON.parse(data);
            if (data.status_code == 'login_first') {
                // window.location.href = site_url + 'member/login?back='+uri;
                $('#modal-guest').modal('show');
            } else if (data.status_code == 'add_wishlist_success') {
                $('.add-to-wishlist .wish-list').attr('style', 'background-color:#FFFFFF;width: 40px !important; height: 40px !important;border-radius: 50%;font-size: 1.5rem !important;');
                $('.add-to-wishlist .fa-heart').attr('style', 'color:red;font-size: 1.5rem !important;line-height: 40px;');
            } else if (data.status_code == 'remove_wishlist_success') {
                $('.add-to-wishlist .wish-list').attr('style', 'width: 40px !important; height: 40px !important;border-radius: 50%;font-size: 1.5rem !important;');
                $('.add-to-wishlist .fa-heart').attr('style', 'color:#fff;-webkit-text-stroke: 1px #000;font-size: 1.5rem !important;line-height: 40px;');
            } else {
                $('.add-to-wishlist .wish-list').attr('style', 'width: 40px !important; height: 40px !important;border-radius: 50%;font-size: 1.5rem !important;');
                $('.add-to-wishlist .fa-heart').attr('style', 'color:#fff;-webkit-text-stroke: 1px #000;font-size: 1.5rem !important;line-height: 40px;');
            }
        }
    });
}

function add_to_wishlist_new(id, store, customer) {
    $.ajax({
        url: site_url + 'catalog/products/add_wishlist_new',
        data: {
            product: id,
            store: store,
            customer: customer
        },
        type: 'POST',
        success: function (data) {
            data = JSON.parse(data);
            if (data.status == 'add') {
                $('.add-to-wishlist-mobile .wish-list i').attr('style', 'color: #d9534f');
                $('.add-to-wishlist-desktop .wish-list i').attr('style', 'color: #d9534f');
                Swal.fire({
                    type: 'success',
                    title: 'Wishlist Berhasil Ditambah',
                    timer: 1000,
                    showConfirmButton: false
                });
                $('.profile-list .profile-dropdown .profile-body .wishlist-link .badge').html(data.total);
                $('.profile-list .profile-dropdown .profile-body .wishlist-link .badge').attr('style', 'position: absolute;right:25px;');
            } else {
                $('.add-to-wishlist-mobile .wish-list i').attr('style', 'color: #CCC');
                $('.add-to-wishlist-desktop .wish-list i').attr('style', 'color: #CCC');
                Swal.fire({
                    type: 'success',
                    title: 'Wishlist Berhasil Dihapus',
                    timer: 1000,
                    showConfirmButton: false
                });
                $('.profile-list .profile-dropdown .profile-body .wishlist-link .badge').html(data.total);
                $('.profile-list .profile-dropdown .profile-body .wishlist-link .badge').attr('style', 'position: absolute;right:25px;');
            }
        }
    })
}

function likeDislike(type, id) {
    var likes = $('#likedislike' + id).find('.likes');
    var dislikes = $('#likedislike' + id).find('.dislikes');
    if (type == 1) {
        likes.text(parseInt(likes.text()) + 1);
        $('#likedislike' + id).find('.like').addClass('current');
    } else if (type == 2) {
        dislikes.text(parseInt(dislikes.text()) + 1);
        $('#likedislike' + id).find('.dislike').addClass('current');
    }
    $('#likedislike' + id).find('div').removeAttr('onclick');
    $.ajax({
        method: 'post',
        url: base_url + 'catalog/products/submitLikeDislike',
        data: {
            id: id,
            type: type
        },
        success: function (data) {
            $('#likedislike' + id).find('.notif').html(data);
        }
    });
}
let up_qty = (qty, type = false, merchant = false) => {
    let min_buy = $('#min_buy').val();
    let input_qty = $('input[name=qty]').val();
    let new_qty = $('input[name=qty]').val(Number(input_qty) + 1);
    if(min_buy > 1) {
        if (new_qty.val() > 0) {
            $('.error-mobile').attr('style', 'color: #d9534f;display:none;width:100%');
            $('#err_qty').css('display', 'none');
        }
        if(new_qty.val() >= min_buy) {
            $('input[name=qty]').css('border', 'solid #DDDDDD');
            $('input[name=qty]').css('border-width', '0 2px');
            $('.error-mobile').attr('style', 'color: #d9534f;display:none;width:100%');
            $('#err_qty').css('display', 'none');
            $('#min_buy_notification').css('color', 'black');
        }
        
        if(new_qty.val() > min_buy) {
            $('#btn_down_qty').removeAttr('disabled');
        }
        if (qty == new_qty.val() || input_qty == qty) {
            $('#btn_up_qty').attr('disabled', true);
        } else {
            $('#btn_up_qty').attr('disabled', false);
        }
    } else {
        if (new_qty.val() > 0) {
            $('input[name=qty]').css('border', 'solid #DDDDDD');
            $('input[name=qty]').css('border-width', '0 2px');
            $('.error-mobile').attr('style', 'color: #d9534f;display:none;width:100%');
            $('#err_qty').css('display', 'none');
        }
        if (new_qty.val() > 1) {
            $('#btn_down_qty').removeAttr('disabled');
        }
    
        if (qty == new_qty.val() || input_qty == qty) {
            $('#btn_up_qty').attr('disabled', true);
        } else {
            $('#btn_up_qty').attr('disabled', false);
        }
    }
    if(type) {
        $.ajax({
            url: site_url + 'ma/auth/check_user_merchant',
            success: function (data) {
                data = JSON.parse(data);
                if(data.status == 'login') {
                    merchant = true;
                } else {
                    merchant = false;
                }
            },
            async: false
        });
        let last_qty = 0;
        let grochire_qty = $('.grochire-qty');
        let options = $('input[name=product_option').val();
        $.each(grochire_qty, function(index, value) {
            $(value).removeClass('active');
            if(new_qty.val() >= $(value).data('qty') && merchant && $(value).data('type') == 'reseller') {
                last_qty = $(value).data('qty');
            } else if (new_qty.val() >= $(value).data('qty') && $(value).data('type') != 'reseller') {
                last_qty = $(value).data('qty');
            }
        });
        $.each(grochire_qty, function(i, val) {
            if(last_qty == $(val).data('qty') && merchant && $(val).data('type') == 'reseller') {
                $(val).addClass('active');
                change_qty($(val).data('id'), $(val).data('qty'), $(val).data('type'), $(val).data('product_qty'), true, options, $(val).data('store_id'));
                let see_others_reseller = $(val).hasClass('other-reseller-item');
                if(see_others_reseller == true) {
                    let link_other_reseller = $('#see_others_grochire_reseller');
                    $('.other-reseller-item').css('display', 'grid');
                    link_other_reseller.text('Sembunyikan');
                    link_other_reseller.data('type', 'hide');
                }
                $('input[name=type_merchant]').val('true');
            } else if (last_qty == $(val).data('qty') && $(val).data('type') != 'reseller') {
                $(val).addClass('active');
                change_qty($(val).data('id'), $(val).data('qty'), $(val).data('type'), $(val).data('product_qty'), true, options, $(val).data('store_id'));
                let see_others_grochire = $(val).hasClass('see-other-grochire');
                if(see_others_grochire == true) {
                    let link_other_grochire = $('#see_others_grochire_price_link');
                    $('.see-other-grochire').css('display', 'grid');
                    link_other_grochire.text('Sembunyikan');
                    link_other_grochire.data('type', 'hide');
                }
                $('input[name=type_merchant]').val('false');
            }
        })
    }
}

let down_qty = (qty, type = false, merchant = false) => {
    qty = Number(qty);
    let input_qty = $('input[name=qty]').val();
    let new_qty = $('input[name=qty]').val(Number(input_qty) - 1);
    let min_buy = $('#min_buy').val();
    if(min_buy > 1) {
        if(Number(new_qty.val()) <= Number(min_buy)) {
            $('#btn_down_qty').attr('disabled', true);
        }
        if((Number(new_qty.val()) > Number(min_buy)) && Number(qty) > Number(new_qty.val())) {
            $('#btn_up_qty').removeAttr('disabled');
        }
        if((Number(new_qty.val()) >= Number(min_buy)) && Number(qty) >= Number(new_qty.val())) {
            $('input[name=qty]').css('border', 'solid #DDDDDD');
            $('input[name=qty]').css('border-width', '0 2px');
            $('#err_qty').css('display', 'none');
            $('.error-mobile').attr('style', 'color: #d9534f;display:none;width:100%');
        }
    } else {
        if (Number(new_qty.val()) < 2) {
            $('#btn_down_qty').attr('disabled', true);
        }
        if (qty > Number(new_qty.val())) {
            $('#btn_up_qty').removeAttr('disabled');
        }
        if (qty >= Number(new_qty.val())) {
            $('input[name=qty]').css('border', 'solid #DDDDDD');
            $('input[name=qty]').css('border-width', '0 2px');
            $('#err_qty').css('display', 'none');
            $('.error-mobile').attr('style', 'color: #d9534f;display:none;width:100%');
        }
    }
    if(type) {
        $.ajax({
            url: site_url + 'ma/auth/check_user_merchant',
            success: function (data) {
                data = JSON.parse(data);
                if(data.status == 'login') {
                    merchant = true;
                } else {
                    merchant = false;
                }
            },
            async: false
        });
        let options = $('input[name=product_option').val();
        let last_qty = 0;
        let grochire_qty = $('.grochire-qty');
        $.each(grochire_qty, function(index, value) {
            $(value).removeClass('active');
            if(new_qty.val() >= $(value).data('qty') && merchant) {
                last_qty = $(value).data('qty');
            } else if (new_qty.val() >= $(value).data('qty') && $(value).data('type') != 'reseller') {
                last_qty = $(value).data('qty');
            }
        });
        $.each(grochire_qty, function(i, val) {
            if(last_qty == $(val).data('qty') && merchant && $(val).data('type') == 'reseller') {
                $(val).addClass('active');
                change_qty($(val).data('id'), $(val).data('qty'), $(val).data('type'), $(val).data('product_qty'), true, options, $(val).data('store_id'));
                let see_others_reseller = $(val).hasClass('other-reseller-item');
                if(see_others_reseller == true) {
                    let link_other_reseller = $('#see_others_grochire_reseller');
                    $('.other-reseller-item').css('display', 'grid');
                    link_other_reseller.text('Sembunyikan');
                    link_other_reseller.data('type', 'hide');
                } else {
                    let link_other_reseller = $('#see_others_grochire_reseller');
                    $('.other-reseller-item').css('display', 'none');
                    link_other_reseller.text('Tampilkan Semua');
                    link_other_reseller.data('type', 'show');
                }
                $('input[name=type_merchant]').val('true');
            } else if (last_qty == $(val).data('qty') && $(val).data('type') != 'reseller') {
                $(val).addClass('active');
                change_qty($(val).data('id'), $(val).data('qty'), $(val).data('type'), $(val).data('product_qty'), true, options, $(val).data('store_id'));
                let see_others_grochire = $(val).hasClass('see-other-grochire');
                if(see_others_grochire == true) {
                    let link_other_grochire = $('#see_others_grochire_price_link');
                    $('.see-other-grochire').css('display', 'grid');
                    link_other_grochire.text('Sembunyikan');
                    link_other_grochire.data('type', 'hide');
                } else {
                    let link_other_grochire = $('#see_others_grochire_price_link');
                    $('.see-other-grochire').css('display', 'none');
                    link_other_grochire.text('Tampilkan Semua');
                    link_other_grochire.data('type', '<show></show>');
                }
                $('input[name=type_merchant]').val('false');
            }
        })
    }
}

function change_store(id, type, type_product) {
    if (type_product == 'last_seen') {
        if (type == 'over') {
            $('#text-ellipsis-city-last_seen-' + id).attr('style', 'margin-top:-18px;display:block;transition:.3s;');
        } else {
            $('#text-ellipsis-city-last_seen-' + id).attr('style', 'margin-top:0px;display:block;transition:.3s;')
        }
    } else if (type_product == 'related') {
        if (type == 'over') {
            $('#text-ellipsis-city-related-' + id).attr('style', 'margin-top:-18px;display:block;transition:.3s;');
        } else {
            $('#text-ellipsis-city-related-' + id).attr('style', 'margin-top:0px;display:block;transition:.3s;');
        }
    }
}

function add_wishlist(e) {
    let product = $(e).data('product');
    let store = $(e).data('store');
    let customer = $(e).data('customer');

    $.ajax({
        url: site_url + 'catalog/products/add_wishlist_new',
        data: {
            product: product,
            store: store,
            customer: customer
        },
        type: 'POST',
        success: function (data) {
            data = JSON.parse(data);
            if (data.status == 'add') {
                $(e).attr('style', 'color:#d9534f;');
                Swal.fire({
                    type: 'success',
                    title: 'Wishlist Berhasil Ditambah',
                    timer: 1000,
                    showConfirmButton: false
                });
                $('.profile-list .profile-dropdown .profile-body .wishlist-link .badge').html(data.total);
                $('.profile-list .profile-dropdown .profile-body .wishlist-link .badge').attr('style', 'position: absolute;right:25px;');
            } else {
                $(e).attr('style', 'color:#BBB;');
                Swal.fire({
                    type: 'success',
                    title: 'Wishlist Berhasil Dihapus',
                    timer: 1000,
                    showConfirmButton: false
                });
                $('.profile-list .profile-dropdown .profile-body .wishlist-link .badge').html(data.total);
                $('.profile-list .profile-dropdown .profile-body .wishlist-link .badge').attr('style', 'position: absolute;right:25px;');
            }
        }
    })
}

function change_qty (id, qty, type, product_qty, type_change = false, option, store_id) {
    $.ajax({
        method: 'post',
        url: site_url + 'catalog/products/get_grochire',
        data: {
            id: id,
            qty: qty,
            type: type,
            option: option,
            store_id: store_id
        },
        success: function (data) {
            data = JSON.parse(data);
            if (data.status == 'success') {
                if(type_change == false) {
                    $('input[name=qty]').val(qty);
                    if(qty > 1) {
                        $('#btn_down_qty').removeAttr('disabled');
                    } else {
                        $('#btn_down_qty').attr('disabled', true);
                    }
                    if(product_qty == qty) {
                        $('#btn_up_qty').attr('disabled', true);
                    } else {
                        $('#btn_up_qty').removeAttr('disabled');
                    }
                }
                $('h2.price').text('');
                $('h2.price').text('Rp ' + formatRupiah(data.price));
            }
        }
    });
}